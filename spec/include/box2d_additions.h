/**
 *
 *  Copyright (c) 2020 Louis Marchand
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 *
 */

/**
 *  \file box2d_additions.h
 *
 *  Header file containing every internal mecanism to make Box2D callbacks
 *  work in the Eiffel World.
 */


#ifndef BOX_2D_ADDITIONS_H
#define BOX_2D_ADDITIONS_H
#include <Box2D.h>
#include "eif_eiffel.h"


/**
 * Used to box a Eiffel {B2D_WORLD} object.
 *
 * Note that `EiffelObject` can be NULL.
 */
typedef struct EiffelWorldBoxed {
	EIF_OBJECT EiffelObject;
} EiffelWorldBoxed;


/**
 * Contain a single element of the `EiffelObjectChain`
 */
typedef struct EiffelObjectNode {
	EIF_OBJECT Object;
	EiffelObjectNode * Next;
} EiffelObjectNode;	

/**
 * Contain Adopted Eiffel Object that will be Wean when the
 * {B2D_WORLD} is collected.
 */
typedef struct EiffelObjectChain {
	EiffelObjectNode * First;
} EiffelObjectChain;

/**
 * Add `object` to `chain`.
 *
 * @param chain The EiffelObjectChain to add  `object`
 * @param object The object to add in `chain` (should be protected)
 */
void AddToEiffelObjectChain(EiffelObjectChain * chain, EIF_OBJECT object);

/**
 * Remove and Wean `object` from `chain`.
 *
 * @param chain The EiffelObjectChain that the  `object` should be removed.
 * @param object The object to remove from `chain` (Should still be protected).
 */
void RemoveFromEiffelObjectChain(EiffelObjectChain * chain, EIF_OBJECT object);

/**
 * Remove and Wean every objects from `chain`.
 *
 * @param chain The EiffelObjectChain to empty
 */
void WeanAllEiffelObjectChain(EiffelObjectChain * chain);

/**
 * The nombre of element in `chain`
 *
 * @param chain The EiffelObjectChain to count
 */
int CountEiffelObjectChain(EiffelObjectChain * chain);

/**
 * The listener used in callbacks when contact happen in {B2D_WORLD}.`step'
 */
class B2dCallbacks : public b2ContactListener, public b2ContactFilter,
					public b2QueryCallback, public b2RayCastCallback{
private: 

	/**
	 * Callback when a contact begin.
	 *
	 * Launch the Eiffel method {B2D_WORLD}.`begin_contact_callback'.
	 *
	 * @param contact The contact that begin.
	 */
    void BeginContact(b2Contact* contact);

	/**
	 * Callback when a contact end.
	 *
	 * Launch the Eiffel method {B2D_WORLD}.`end_contact_callback'.
	 *
	 * @param contact The contact that ended.
	 */
    void EndContact(b2Contact* contact);
	/**
	 * Callback before a contact is solved.
	 *
	 * Launch the Eiffel method {B2D_WORLD}.`pre_solve_contact_callback'.
	 *
	 * @param contact The contact that need to be solved.
	 * @param oldManifest The manifest before the contact happened
	 */
	void PreSolve(b2Contact *contact, const b2Manifold *oldManifold);

	/**
	 * Callback after a contact is solved.
	 *
	 * Launch the Eiffel method {B2D_WORLD}.`post_solve_contact_callback'.
	 *
	 * @param contact The contact that need to be solved.
	 * @param impulse The sub-step forces of the impact
	 */
	void PostSolve(b2Contact *contact, const b2ContactImpulse *impulse);

	/**
	 * Callback when two fixtures collide.
	 *
	 * Launch the Eiffel method {B2D_WORLD}.`filter_collision_callback'.
	 *
	 * @return True if the collision should create a contact, False if not.
	 *
	 * @param fixtureA The first fixture in the collision
	 * @param fixtureB The second fixture in the collision
	 */
	bool ShouldCollide (b2Fixture *fixtureA, b2Fixture *fixtureB);

	/**
	 * Callback when an Axis Align Bounding Box (AABB) request have a result.
	 *
	 * Launch the Eiffel method {B2D_WORLD}.`axis_align_bounding_box_callback'.
	 *
	 * @return True if the callback process should stop.
	 *
	 * @param fixture The fixture found at the requested location
	 */
	bool ReportFixture (b2Fixture *fixture);

	/**
	 * Callback when a ray cast is requested and have a result.
	 *
	 * Launch the Eiffel method {B2D_WORLD}.`ray_cast_callback'.
	 *
	 * @return -1 to ignore the fixture, 0 to stop the process, 1 to continue
	 * 			and a fraction to clip the ray.
	 *
	 * @param fixture The fixture found
	 * @param point The point of origin of the ray
	 * @param normal The direction of the ray
	 * @param fraction The length of the ray
	 */
	float ReportFixture (b2Fixture *fixture, const b2Vec2 &point,
									const b2Vec2 &normal, float fraction);

	/**
	 * Used to know the method of what {B2D_WORLD} Eiffel object callback need
	 * to launch.
	 */
	EiffelWorldBoxed * eiffelWorld;

	/**
	 * The `ShouldCollide` is activated.
	 */
	bool MustShouldCollide;

	/**
	 * The `BeginContact` is activated.
	 */
	bool MustBeginContact;

	/**
	 * The `EndContact` is activated.
	 */
	bool MustEndContact;

	/**
	 * The `PreSolve` is activated.
	 */
	bool MustPreSolve;

	/**
	 * The `PostSolve` is activated.
	 */
	bool MustPostSolve;

	/**
	 * The `begin_contact_callback` Eiffel Callback method
	 */
	void (* beginContactCallback)(EIF_REFERENCE, EIF_POINTER, EIF_REFERENCE, EIF_REFERENCE);

	/**
	 * The `end_contact_callback` Eiffel Callback method
	 */
	void (* endContactCallback)(EIF_REFERENCE, EIF_POINTER, EIF_REFERENCE, EIF_REFERENCE);

	/**
	 * The `pre_solve_contact_callback` Eiffel Callback method
	 */
	void (* preSolveContactCallback)(EIF_REFERENCE, EIF_POINTER, EIF_POINTER, EIF_REFERENCE, EIF_REFERENCE);

	/**
	 * The `post_solve_contact_callback` Eiffel Callback method
	 */
	void (* postSolveContactCallback)(EIF_REFERENCE, EIF_POINTER, EIF_POINTER, EIF_REFERENCE, EIF_REFERENCE);

	/**
	 * The `filter_collision_callback` Eiffel Callback method
	 */
	bool (* filterCollisionCallback)(EIF_REFERENCE, EIF_REFERENCE, EIF_REFERENCE);

	/**
	 * The `axis_align_bounding_box_callback` Eiffel Callback method
	 */
	bool (* axisAlignBoundingBoxCallback)(EIF_REFERENCE, EIF_REFERENCE);

	/**
	 * The `ray_cast_callback` Eiffel Callback method
	 */
	float (* rayCastCallback)(EIF_REFERENCE, EIF_REFERENCE, EIF_POINTER, EIF_POINTER, EIF_REAL_32);

	/**
	 * The default Box2D collision filter
	 */
	b2ContactFilter *b2DefaultFilter;

public:

	/**
	 * The default Box2D collision filter execution.
	 *
	 * @return True if the fixtures must collide. False if not.
	 *
	 * @param fixtureA The first fixture to collide.
	 * @param fixtureB The second fixture to collide.
	 */
	bool DefaultFilter(b2Fixture* fixtureA, b2Fixture* fixtureB);

	/**
	 * Assign `beginContactCallback` with the value of `callback`
	 *
	 * @param callback The method to assign
	 */
	void setBeginContactCallback(EIF_POINTER callback);

	/**
	 * Assign `endContactCallback` with the value of `callback`
	 *
	 * @param callback The method to assign
	 */
	void setEndContactCallback(EIF_POINTER callback);

	/**
	 * Assign `preSolveContactCallback` with the value of `callback`
	 *
	 * @param callback The method to assign
	 */
	void setPreSolveContactCallback(EIF_POINTER callback);

	/**
	 * Assign `postSolveContactCallback` with the value of `callback`
	 *
	 * @param callback The method to assign
	 */
	void setPostSolveContactCallback(EIF_POINTER callback);

	/**
	 * Assign `filterCollisionCallback` with the value of `callback`
	 *
	 * @param callback The method to assign
	 */
	void setFilterCollisionCallback(EIF_POINTER callback);

	/**
	 * Assign `axisAlignBoundingBoxCallback` with the value of `callback`
	 *
	 * @param callback The method to assign
	 */
	void setAxisAlignBoundingBoxCallback(EIF_POINTER callback);

	/**
	 * Assign `rayCastCallback` with the value of `callback`
	 *
	 * @param callback The method to assign
	 */
	void setRayCastCallback(EIF_POINTER callback);

	/**
	 * Constructor of `B2dCallbacks`.
	 */
	B2dCallbacks();

	/**
	 * Destructor of `B2dCallbacks`.
	 */
	~B2dCallbacks();

	/**
	 * Assign `EiffelObject`.
	 *
	 * @param EiffelObject Value to assign.
	 */
	void SetEiffelWorld(EIF_OBJECT EiffelObject);

	/**
	 * Set `EiffelObject` to NULL.
	 */
	void ClearEiffelWorld();

	/**
	 * Assing `MustShouldCollide`.
	 *
	 * @param flag The value to assign
	 */
	void SetMustShouldCollide(bool flag);


	/**
	 * Assing `MustBeginContact`.
	 *
	 * @param flag The value to assign
	 */
	void SetMustBeginContact(bool flag);

	/**
	 * Assing `MustEndContact`.
	 *
	 * @param flag The value to assign
	 */
	void SetMustEndContact(bool flag);

	/**
	 * Assing `MustPreSolve`.
	 *
	 * @param flag The value to assign
	 */
	void SetMustPreSolve(bool flag);

	/**
	 * Assing `MustPostSolve`.
	 *
	 * @param flag The value to assign
	 */
	void SetMustPostSolve(bool flag);


};

#endif /* BOX_2D_ADDITIONS_H */
