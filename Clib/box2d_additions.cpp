/**
 *
 *  Copyright (c) 2020 Louis Marchand
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 *
 */

/**
 *  \file box2d_additions.cpp
 *
 *  Source file containing every internal mecanism to make Box2D callbacks
 *  work in the Eiffel World.
 */


#include "box2d_additions.h"
#include <stdio.h>

/**
 * Add `aObject` to `aChain`.
 *
 * @param aChain The EiffelObjectChain to add  `aObject`
 * @param aObject The object to add in `aChain` (should be protected)
 */
void AddToEiffelObjectChain(EiffelObjectChain * aChain, EIF_OBJECT aObject){
	EiffelObjectNode * lNode = new EiffelObjectNode();
	lNode->Object = aObject;
	lNode->Next = aChain->First;
	aChain->First = lNode;
}

/**
 * Remove and Wean `aObject` from `Chain`.
 *
 * @param aChain The EiffelObjectChain that the  `aObject` should be removed.
 * @param aObject The object to remove from `aChain` (Should still be protected).
 */
void RemoveFromEiffelObjectChain(EiffelObjectChain * aChain, EIF_OBJECT aObject){
	EiffelObjectNode * lOld;
	EiffelObjectNode * lNode = aChain->First;
	if (lNode->Object == aObject) {
		aChain->First = lNode->Next;
		eif_wean(aObject);
		delete lNode;
	} else {
		while (lNode->Next) {
			lOld = lNode;
			lNode = lNode->Next;
			if (lNode->Object == aObject){
				lOld->Next = lNode->Next;
				delete lNode;
				lNode = lOld;
				eif_wean(aObject);
			}
		}
	}
}

/**
 * Remove and Wean every objects from `aChain`.
 *
 * @param aChain The EiffelObjectChain to empty
 */
void WeanAllEiffelObjectChain(EiffelObjectChain * aChain){
	EiffelObjectNode * lNext;
	EiffelObjectNode * lNode = aChain->First;
	while (lNode) {
		lNext = lNode->Next;
		eif_wean(lNode->Object);
		delete lNode;
		lNode = lNext;
	}
	aChain->First = NULL;
}

/**
 * The nombre of element in `aChain`
 *
 * @param aChain The EiffelObjectChain to count
 */
int CountEiffelObjectChain(EiffelObjectChain * aChain){
	EiffelObjectNode * lNode = aChain->First;
	int lCount = 0;
	while (lNode) {
		lCount = lCount + 1;
		lNode = lNode->Next;
	}
	return lCount;
}

/**
 * Constructor of `B2dCallbacks`.
 */
B2dCallbacks::B2dCallbacks(){
	eiffelWorld = new EiffelWorldBoxed();
	SetMustBeginContact(false);
	SetMustEndContact(false);
	SetMustPreSolve(false);
	SetMustPostSolve(false);
	SetMustShouldCollide(false);
	b2DefaultFilter = new b2ContactFilter();
}

/**
 * Destructor of `B2dCallbacks`.
 */
B2dCallbacks::~B2dCallbacks(){
	delete eiffelWorld;
	delete b2DefaultFilter;
}

/**
 * Assign `EiffelObject`.
 *
 * @param aEiffelObject Value to assign.
 */
void B2dCallbacks::SetEiffelWorld(EIF_OBJECT aEiffelObject){
	eiffelWorld->EiffelObject = aEiffelObject;
}

/**
 * Set `EiffelObject` to NULL.
 */
void B2dCallbacks::ClearEiffelWorld(){
	eiffelWorld->EiffelObject = NULL;
}

/**
 * Assign `beginContactCallback` with the value of `a_callback`
 *
 * @param a_callback The method to assign
 */
void B2dCallbacks::setBeginContactCallback(EIF_POINTER a_callback){
	beginContactCallback = (void (*)(EIF_REFERENCE, EIF_POINTER, 
									EIF_REFERENCE, EIF_REFERENCE)) a_callback;
}

/**
 * Assign `endContactCallback` with the value of `a_callback`
 *
 * @param a_callback The method to assign
 */
void B2dCallbacks::setEndContactCallback(EIF_POINTER a_callback){
	endContactCallback = (void (*)(EIF_REFERENCE, EIF_POINTER,
							EIF_REFERENCE, EIF_REFERENCE)) a_callback;
}

/**
 * Assign `preSolveContactCallback` with the value of `a_callback`
 *
 * @param a_callback The method to assign
 */
void B2dCallbacks::setPreSolveContactCallback(EIF_POINTER a_callback){
	preSolveContactCallback = (void (*)(EIF_REFERENCE, EIF_POINTER, 
					EIF_POINTER, EIF_REFERENCE, EIF_REFERENCE)) a_callback;
}

/**
 * Assign `postSolveContactCallback` with the value of `a_callback`
 *
 * @param a_callback The method to assign
 */
void B2dCallbacks::setPostSolveContactCallback(EIF_POINTER a_callback){
	postSolveContactCallback = (void (*)(EIF_REFERENCE, EIF_POINTER, 
					EIF_POINTER, EIF_REFERENCE, EIF_REFERENCE)) a_callback;
}

/**
 * Assign `filterCollisionCallback` with the value of `a_callback`
 *
 * @param a_callback The method to assign
 */
void B2dCallbacks::setFilterCollisionCallback(EIF_POINTER a_callback){
	filterCollisionCallback = (bool (*)(EIF_REFERENCE, EIF_REFERENCE,
								EIF_REFERENCE)) a_callback;
}

/**
 * Assign `axisAlignBoundingBoxCallback` with the value of `a_callback`
 *
 * @param a_callback The method to assign
 */
void B2dCallbacks::setAxisAlignBoundingBoxCallback(EIF_POINTER a_callback){
	axisAlignBoundingBoxCallback = (bool (*)(EIF_REFERENCE,
										EIF_REFERENCE)) a_callback;
}

/**
 * Assign `rayCastCallback` with the value of `a_callback`
 *
 * @param a_callback The method to assign
 */
void B2dCallbacks::setRayCastCallback(EIF_POINTER aCallback){
	rayCastCallback = (float (*)(EIF_REFERENCE, EIF_REFERENCE, EIF_POINTER,
									EIF_POINTER, EIF_REAL_32)) aCallback;
}

/**
 * The default Box2D collision filter execution.
 *
 * @return True if the fixtures must collide. False if not.
 *
 * @param aFixtureA The first fixture to collide.
 * @param aFixtureB The second fixture to collide.
 */
bool B2dCallbacks::DefaultFilter(b2Fixture* aFixtureA, b2Fixture* aFixtureB){
	return b2DefaultFilter->ShouldCollide(aFixtureA, aFixtureB);
}



/**
 * Callback when two fixtures collide.
 *
 * Launch the Eiffel method {B2D_WORLD}.`filter_collision_callback'.
 *
 * @return True if the collision should create a contact, False if not.
 *
 * @param aFixtureA The first fixture in the collision
 * @param aFixtureB The second fixture in the collision
 */
bool B2dCallbacks::ShouldCollide (b2Fixture *aFixtureA, b2Fixture *aFixtureB){
	EIF_OBJECT lFixture1, lFixture2;
	bool lResult = true;
	if (MustShouldCollide && eiffelWorld->EiffelObject) {
		lFixture1 = (EIF_OBJECT)aFixtureA->GetUserData();
		lFixture2 = (EIF_OBJECT)aFixtureB->GetUserData();
		if (lFixture1 && lFixture2){
			lResult = filterCollisionCallback(
					eif_access(eiffelWorld->EiffelObject),
					eif_access(lFixture1), eif_access(lFixture2));
		} else {
			lResult = DefaultFilter(aFixtureA, aFixtureB);
		}
	} else {
		lResult = DefaultFilter(aFixtureA, aFixtureB);
	}
	return lResult;
}



/**
 * Callback when a contact begin.
 *
 * Launch the Eiffel method {B2D_WORLD}.`begin_contact'.
 *
 * @param aContact The contact that begin.
 */
void B2dCallbacks::BeginContact(b2Contact* aContact){
	EIF_OBJECT lFixture1, lFixture2;
	if (MustBeginContact && eiffelWorld->EiffelObject) {
		lFixture1 = (EIF_OBJECT)aContact->GetFixtureA()->GetUserData();
		lFixture2 = (EIF_OBJECT)aContact->GetFixtureB()->GetUserData();
		if (lFixture1 && lFixture2){
			beginContactCallback(eif_access(eiffelWorld->EiffelObject),aContact,
					eif_access(lFixture1), eif_access(lFixture2));
		}
	}	
}





/**
 * Callback when a contact end.
 *
 * Launch the Eiffel method {B2D_WORLD}.`end_contact'.
 *
 * @param aContact The contact that ended.
 */
void B2dCallbacks::EndContact(b2Contact* aContact){
	EIF_OBJECT lFixture1, lFixture2;
	if (MustEndContact && eiffelWorld->EiffelObject) {
		lFixture1 = (EIF_OBJECT)aContact->GetFixtureA()->GetUserData();
		lFixture2 = (EIF_OBJECT)aContact->GetFixtureB()->GetUserData();
		if (lFixture1 && lFixture2){
			endContactCallback(eif_access(eiffelWorld->EiffelObject),aContact,
					eif_access(lFixture1), eif_access(lFixture2));
		}
	}	
}

/**
 * Callback before a contact is solved.
 *
 * Launch the Eiffel method {B2D_WORLD}.`pre_solve_contact'.
 *
 * @param aContact The contact that need to be solved.
 * @param aOldManifest The manifest before the contact happened
 */
void B2dCallbacks::PreSolve(b2Contact* aContact, const b2Manifold *aOldManifold){
	EIF_OBJECT lFixture1, lFixture2;
	if (MustPreSolve && eiffelWorld->EiffelObject) {
		lFixture1 = (EIF_OBJECT)aContact->GetFixtureA()->GetUserData();
		lFixture2 = (EIF_OBJECT)aContact->GetFixtureB()->GetUserData();
		if (lFixture1 && lFixture2){
			preSolveContactCallback(
					eif_access(eiffelWorld->EiffelObject),aContact,
					(void *)aOldManifold, eif_access(lFixture1),
					eif_access(lFixture2));
		}
	}	
}

/**
 * Callback after a contact is solved.
 *
 * Launch the Eiffel method {B2D_WORLD}.`post_solve_contact'.
 *
 * @param aContact The contact that need to be solved.
 * @param aImpulse The sub-step forces of the impact
 */
void B2dCallbacks::PostSolve(b2Contact* aContact, const b2ContactImpulse *aImpulse){
	EIF_OBJECT lFixture1, lFixture2;
	if (MustPostSolve && eiffelWorld->EiffelObject) {
		lFixture1 = (EIF_OBJECT)aContact->GetFixtureA()->GetUserData();
		lFixture2 = (EIF_OBJECT)aContact->GetFixtureB()->GetUserData();
		if (lFixture1 && lFixture2){
			postSolveContactCallback(eif_access(eiffelWorld->EiffelObject),
					aContact, (void *)aImpulse, eif_access(lFixture1),
					eif_access(lFixture2));
		}
	}	
}

/**
 * Callback when an Axis Align Bounding Box (AABB) request have a result.
 *
 * Launch the Eiffel method {B2D_WORLD}.`axis_align_bounding_box_callback'.
 *
 * @return True if the callback process should stop.
 *
 * @param aFixture The fixture found at the requested location
 */
bool B2dCallbacks::ReportFixture (b2Fixture *aFixture){
	EIF_OBJECT lFixture;
	bool lResult = true;
	if (eiffelWorld->EiffelObject) {
		lFixture = (EIF_OBJECT)aFixture->GetUserData();
		if (lFixture){
			lResult = axisAlignBoundingBoxCallback(
					eif_access(eiffelWorld->EiffelObject),
					eif_access(lFixture));
		}
	}
	return lResult;
}


/**
 * Callback when a ray cast is requested and have a result.
 *
 * Launch the Eiffel method {B2D_WORLD}.`ray_cast_callback'.
 *
 * @return True if the callback process should stop.
 *
 * @param aFixture The fixture found
 * @param aPoint The point of origin of the ray
 * @param aNormal The direction of the ray
 * @param aFraction The length of the ray
 */
float B2dCallbacks::ReportFixture (b2Fixture *aFixture, const b2Vec2 &aPoint,
								const b2Vec2 &aNormal, float aFraction){
	EIF_OBJECT lFixture;
	float lResult = 1.0;
	if (eiffelWorld->EiffelObject && rayCastCallback) {
		printf("Fraction: %f\n", aFraction);
		lFixture = (EIF_OBJECT)aFixture->GetUserData();
		if (lFixture){
			lResult = rayCastCallback(eif_access(eiffelWorld->EiffelObject),
					eif_access(lFixture), (void *)&aPoint, (void *)&aNormal, aFraction);
		}
	}
	return lResult;

}


/**
 * Assing `MustShouldCollide`.
 *
 * @param aFlag The value to assign
 */
void B2dCallbacks::SetMustShouldCollide(bool aFlag){
	MustShouldCollide = aFlag;
}


/**
 * Assing `MustBeginContact`.
 *
 * @param aFlag The value to assign
 */
void B2dCallbacks::SetMustBeginContact(bool aFlag){
	MustBeginContact = aFlag;
}

/**
 * Assing `MustEndContact`.
 *
 * @param aFlag The value to assign
 */
void B2dCallbacks::SetMustEndContact(bool aFlag){
	MustEndContact = aFlag;
}

/**
 * Assing `MustPreSolve`.
 *
 * @param aFlag The value to assign
 */
void B2dCallbacks::SetMustPreSolve(bool aFlag){
	MustPreSolve = aFlag;
}

/**
 * Assing `MustPostSolve`.
 *
 * @param aFlag The value to assign
 */
void B2dCallbacks::SetMustPostSolve(bool aFlag){
	MustPostSolve = aFlag;
}


