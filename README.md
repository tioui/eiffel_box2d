Eiffel_Box2d
============

An Eiffel Wrapper for the Box2d library (https://box2d.org/).

Note
----

Please note that this library is not functionnal yet. Please stay tuned for the first release candidate version.

Installation
------------

To install box2d, copy the root directory of the project in $ISE_EIFFEL/contrib/library/box2d.

You have to compile the C part of the library by doing the following in the directory
$ISE_EIFFEL/contrib/library/box2d/Clib:

```bash
finish_freezing -library
```

On Linux, you need to install the Box2D development library. For exemple, on Debian/Ubuntu:

```bash
sudo apt install libbox2d-dev
```

License
-------

MIT License

Copyright (c) 2020 Louis Marchand and Patrick Boucher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
