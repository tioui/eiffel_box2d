note
	description: "Tests for {B2D_MANIFOLD}"
	author: "Louis Marchand"
	date: "Thu, 02 Jul 2020 16:19:12 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_MANIFOLD_TESTS

inherit
	EQA_TEST_SET
		redefine
			on_prepare
		end
	B2D_ANY
		undefine
			default_create
		end

feature {NONE} -- Events

feature {NONE} -- Events

	on_prepare
			-- <Precursor>
		local
			l_edge:B2D_SHAPE_EDGE
			l_circle:B2D_SHAPE_CIRCLE
			l_body_definition:B2D_BODY_DEFINITION
			l_body_1, l_body_2:B2D_BODY
			l_world:B2D_WORLD
		do
			create l_edge.make (-10, 1, 10, 1)
			create l_circle.make_with_coordinates (3, 11, 9.99999)
			create l_world.make (0, -10)
			create l_body_definition.make_static
			l_world.create_body (l_body_definition)
			l_body_1 := l_world.bodies.at (1)
			l_body_1.create_fixture_from_shape (l_edge, 0)
			create l_body_definition.make_dynamic
			l_world.create_body (l_body_definition)
			l_body_2 := l_world.bodies.at (2)
			l_body_2.create_fixture_from_shape (l_circle, 10)
			l_world.step
			assert("Contacts count valid", l_world.contacts.count ~ 1)
			manifold := l_world.contacts.at (1).manifold
		end

feature -- Test routines

	test_points_wrong
			-- Wrong test for {B2D_MANIFOLD}.`points'
		note
			testing:  	"covers/{B2D_MANIFOLD}.points"
		local
			l_retry:BOOLEAN
			l_result:LIST[B2D_MANIFOLD_POINT]
		do
			if not l_retry then
				manifold.put_null
				l_result := manifold.points
				assert("{B2D_MANIFOLD}.points valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_point_count_wrong
			-- Wrong test for {B2D_MANIFOLD}.`point_count'
		note
			testing:  	"covers/{B2D_MANIFOLD}.point_count"
		local
			l_retry:BOOLEAN
			l_result:INTEGER
		do
			if not l_retry then
				manifold.put_null
				l_result := manifold.point_count
				assert("{B2D_MANIFOLD}.point_count valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_local_point_wrong
			-- Wrong test for {B2D_MANIFOLD}.`local_point'
		note
			testing:  	"covers/{B2D_MANIFOLD}.local_point"
		local
			l_retry:BOOLEAN
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				manifold.put_null
				l_result := manifold.local_point
				assert("{B2D_MANIFOLD}.local_point valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_points_local_point_wrong
			-- Wrong test for {B2D_MANIFOLD_POINT}.`local_point'
		note
			testing:  	"covers/{B2D_MANIFOLD_POINT}.local_point"
		local
			l_retry:BOOLEAN
			l_point:B2D_MANIFOLD_POINT
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				l_point := manifold.points.at (1)
				l_point.put_null
				l_result := l_point.local_point
				assert("{B2D_MANIFOLD_POINT}.local_point valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_points_normal_impulse_wrong
			-- Wrong test for {B2D_MANIFOLD_POINT}.`normal_impulse'
		note
			testing:  	"covers/{B2D_MANIFOLD_POINT}.normal_impulse"
		local
			l_retry:BOOLEAN
			l_point:B2D_MANIFOLD_POINT
			l_result:REAL_32
		do
			if not l_retry then
				l_point := manifold.points.at (1)
				l_point.put_null
				l_result := l_point.normal_impulse
				assert("{B2D_MANIFOLD_POINT}.normal_impulse valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_points_tangent_impulse_wrong
			-- Wrong test for {B2D_MANIFOLD_POINT}.`tangent_impulse'
		note
			testing:  	"covers/{B2D_MANIFOLD_POINT}.tangent_impulse"
		local
			l_retry:BOOLEAN
			l_point:B2D_MANIFOLD_POINT
			l_result:REAL_32
		do
			if not l_retry then
				l_point := manifold.points.at (1)
				l_point.put_null
				l_result := l_point.tangent_impulse
				assert("{B2D_MANIFOLD_POINT}.tangent_impulse valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_points_id_wrong
			-- Wrong test for {B2D_MANIFOLD_POINT}.`id'
		note
			testing:  	"covers/{B2D_MANIFOLD_POINT}.id"
		local
			l_retry:BOOLEAN
			l_point:B2D_MANIFOLD_POINT
			l_result:NATURAL
		do
			if not l_retry then
				l_point := manifold.points.at (1)
				l_point.put_null
				l_result := l_point.id
				assert("{B2D_MANIFOLD_POINT}.id valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_points_index_1_wrong
			-- Wrong test for {B2D_MANIFOLD_POINT}.`index_1'
		note
			testing:  	"covers/{B2D_MANIFOLD_POINT}.index_1"
		local
			l_retry:BOOLEAN
			l_point:B2D_MANIFOLD_POINT
			l_result:INTEGER
		do
			if not l_retry then
				l_point := manifold.points.at (1)
				l_point.put_null
				l_result := l_point.index_1
				assert("{B2D_MANIFOLD_POINT}.index_1 valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_points_index_2_wrong
			-- Wrong test for {B2D_MANIFOLD_POINT}.`index_2'
		note
			testing:  	"covers/{B2D_MANIFOLD_POINT}.index_2"
		local
			l_retry:BOOLEAN
			l_point:B2D_MANIFOLD_POINT
			l_result:INTEGER
		do
			if not l_retry then
				l_point := manifold.points.at (1)
				l_point.put_null
				l_result := l_point.index_2
				assert("{B2D_MANIFOLD_POINT}.index_2 valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_points_is_vertex_1_wrong
			-- Wrong test for {B2D_MANIFOLD_POINT}.`is_vertex_1'
		note
			testing:  	"covers/{B2D_MANIFOLD_POINT}.is_vertex_1"
		local
			l_retry:BOOLEAN
			l_point:B2D_MANIFOLD_POINT
			l_result:BOOLEAN
		do
			if not l_retry then
				l_point := manifold.points.at (1)
				l_point.put_null
				l_result := l_point.is_intersect_vertex_1
				assert("{B2D_MANIFOLD_POINT}.is_vertex_1 valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_points_is_vertex_2_wrong
			-- Wrong test for {B2D_MANIFOLD_POINT}.`is_vertex_2'
		note
			testing:  	"covers/{B2D_MANIFOLD_POINT}.is_vertex_2"
		local
			l_retry:BOOLEAN
			l_point:B2D_MANIFOLD_POINT
			l_result:BOOLEAN
		do
			if not l_retry then
				l_point := manifold.points.at (1)
				l_point.put_null
				l_result := l_point.is_intersect_vertex_2
				assert("{B2D_MANIFOLD_POINT}.is_vertex_2 valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_points_is_face_1_wrong
			-- Wrong test for {B2D_MANIFOLD_POINT}.`is_face_1'
		note
			testing:  	"covers/{B2D_MANIFOLD_POINT}.is_face_1"
		local
			l_retry:BOOLEAN
			l_point:B2D_MANIFOLD_POINT
			l_result:BOOLEAN
		do
			if not l_retry then
				l_point := manifold.points.at (1)
				l_point.put_null
				l_result := l_point.is_intersect_face_1
				assert("{B2D_MANIFOLD_POINT}.is_face_1 valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_points_is_face_2_wrong
			-- Wrong test for {B2D_MANIFOLD_POINT}.`is_face_2'
		note
			testing:  	"covers/{B2D_MANIFOLD_POINT}.is_face_2"
		local
			l_retry:BOOLEAN
			l_point:B2D_MANIFOLD_POINT
			l_result:BOOLEAN
		do
			if not l_retry then
				l_point := manifold.points.at (1)
				l_point.put_null
				l_result := l_point.is_intersect_face_2
				assert("{B2D_MANIFOLD_POINT}.is_face_2 valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_point_states
			-- Wrong test for {B2D_MANIFOLD_POINT}.`point_states'
		note
			testing:  	"covers/{B2D_MANIFOLD_POINT}.point_states"
		do
			assert("Not implemented: need callbacks.", False)
		end

feature {NONE} -- Implementation

	manifold:B2D_MANIFOLD
			-- The {B2D_MANIFOLD} used in `Current'
end


