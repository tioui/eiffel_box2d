note
	description: "Tests for {B2D_JOINT_GEAR_DEFINITION}"
	author: "Louis Marchand"
	date: "Mon, 06 Jul 2020 20:31:57 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_JOINT_GEAR_DEFINITION_TESTS

inherit
	EQA_TEST_SET
		redefine
			on_prepare
		end
	B2D_ANY
		undefine
			default_create
		end

feature {NONE} -- Implementation

	on_prepare
			-- <Precursor>
		do
			create world.make (0, -10)
			world.create_body (create {B2D_BODY_DEFINITION}.make_dynamic)
			check attached world.last_body as la_body_1 then
				world.create_body (create {B2D_BODY_DEFINITION}.make_dynamic)
				check attached world.last_body as la_body_2 then
					world.create_body (create {B2D_BODY_DEFINITION}.make_dynamic)
					check attached world.last_body as la_body_3 then
						world.create_joint (create {B2D_JOINT_PRISMATIC_DEFINITION}.make_with_vector_and_angle (la_body_1, la_body_2, la_body_1.center_of_mass, {DOUBLE_MATH}.pi_4.truncated_to_real))
						check attached {B2D_JOINT_GEARABLE} world.last_joint as la_joint_1 then
							joint_1 := la_joint_1
							world.create_joint (create {B2D_JOINT_PRISMATIC_DEFINITION}.make_with_vector_and_angle (la_body_2, la_body_3, la_body_3.center_of_mass, {DOUBLE_MATH}.pi_2.truncated_to_real))
							check attached {B2D_JOINT_GEARABLE} world.last_joint as la_joint_2 then
								joint_2 := la_joint_2
							end
						end
					end
				end
			end
		end

feature -- Test routines

	test_make
			-- Test of {B2D_JOINT_GEAR_DEFINITION}.make
		note
			testing:
				"covers/{B2D_JOINT_GEAR_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_GEAR_DEFINITION
		do
			create l_definition.make (joint_1, joint_2)
			assert ("Joint_1 valid.", l_definition.joint_1 ~ joint_1)
			assert ("Joint_2 valid.", l_definition.joint_2 ~ joint_2)
			assert ("ratio valid.", l_definition.ratio ~ 1.0)
		end

	test_joint_1
			-- Test of {B2D_JOINT_GEAR_DEFINITION}.set_joint_1
		note
			testing:  "covers/{B2D_JOINT_GEAR_DEFINITION}.set_joint_1",
			          "covers/{B2D_JOINT_GEAR_DEFINITION}.joint_1"
		local
			l_definition:B2D_JOINT_GEAR_DEFINITION
		do
			create l_definition.make (joint_1, joint_2)
			world.create_body (create {B2D_BODY_DEFINITION}.make_dynamic)
			check attached world.last_body as la_body_4 then
				world.create_body (create {B2D_BODY_DEFINITION}.make_dynamic)
				check attached world.last_body as la_body_5 then
					world.create_joint (create {B2D_JOINT_PRISMATIC_DEFINITION}.make_with_vector_and_angle (la_body_4, la_body_5, la_body_4.center_of_mass, {DOUBLE_MATH}.pi_4.truncated_to_real))
					check attached {B2D_JOINT_PRISMATIC} world.last_joint as la_new_joint then
						l_definition.set_joint_1 (la_new_joint)
						assert ("set_joint_1 valid.", l_definition.joint_1 ~ la_new_joint)
					end
				end
			end
		end

	test_joint_2
			-- Test of {B2D_JOINT_GEAR_DEFINITION}.set_joint_2
		note
			testing:  "covers/{B2D_JOINT_GEAR_DEFINITION}.set_joint_2",
			          "covers/{B2D_JOINT_GEAR_DEFINITION}.joint_2"
		local
			l_definition:B2D_JOINT_GEAR_DEFINITION
		do
			create l_definition.make (joint_1, joint_2)
			world.create_body (create {B2D_BODY_DEFINITION}.make_dynamic)
			check attached world.last_body as la_body_4 then
				world.create_body (create {B2D_BODY_DEFINITION}.make_dynamic)
				check attached world.last_body as la_body_5 then
					world.create_joint (create {B2D_JOINT_PRISMATIC_DEFINITION}.make_with_vector_and_angle (la_body_4, la_body_5, la_body_4.center_of_mass, {DOUBLE_MATH}.pi_4.truncated_to_real))
					check attached {B2D_JOINT_PRISMATIC} world.last_joint as la_new_joint then
						l_definition.set_joint_2 (la_new_joint)
						assert ("set_joint_2 valid.", l_definition.joint_2 ~ la_new_joint)
					end
				end
			end
		end

	test_ratio
			-- Test of {B2D_JOINT_GEAR_DEFINITION}.set_ratio
		note
			testing:  "covers/{B2D_JOINT_GEAR_DEFINITION}.set_ratio",
			          "covers/{B2D_JOINT_GEAR_DEFINITION}.ratio"
		local
			l_definition:B2D_JOINT_GEAR_DEFINITION
		do
			create l_definition.make (joint_1, joint_2)
			l_definition.set_ratio (2.0)
			assert("set_ratio valid.", l_definition.ratio ~ 2.0)
			l_definition.set_ratio (0.0)
			assert("set_ratio valid.", l_definition.ratio ~ 0.0)
		end

	test_set_ratio_wrong
			-- Wrong test for {B2D_JOINT_GEAR_DEFINITION}.`set_ratio'
		note
			testing:
				"covers/{B2D_JOINT_GEAR}.set_ratio"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_GEAR_DEFINITION
		do
			if not l_retry then
				create l_definition.make (joint_1, joint_2)
				l_definition.put_null
				l_definition.set_ratio (0.0)
				assert("set_ratio valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_ratio_wrong
			-- Wrong test for {B2D_JOINT_GEAR_DEFINITION}.`ratio'
		note
			testing:
				"covers/{B2D_JOINT_GEAR}.ratio"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_GEAR_DEFINITION
			l_result:REAL_32
		do
			if not l_retry then
				create l_definition.make (joint_1, joint_2)
				l_definition.put_null
				l_result := l_definition.ratio
				assert("ratio valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_joint_1_wrong_1
			-- Wrong test 1 for {B2D_JOINT_GEAR_DEFINITION}.`set_joint_1'
		note
			testing:
				"covers/{B2D_JOINT_GEAR}.set_joint_1"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_GEAR_DEFINITION
		do
			if not l_retry then
				create l_definition.make (joint_1, joint_2)
				world.create_body (create {B2D_BODY_DEFINITION}.make_dynamic)
				check attached world.last_body as la_body_4 then
					world.create_body (create {B2D_BODY_DEFINITION}.make_dynamic)
					check attached world.last_body as la_body_5 then
						world.create_joint (create {B2D_JOINT_PRISMATIC_DEFINITION}.make_with_vector_and_angle (la_body_4, la_body_5, la_body_4.center_of_mass, {DOUBLE_MATH}.pi_4.truncated_to_real))
						check attached {B2D_JOINT_PRISMATIC} world.last_joint as la_new_joint then
							l_definition.put_null
							l_definition.set_joint_1 (la_new_joint)
							assert ("set_joint_1 valid.", False)
						end
					end
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_joint_1_wrong_2
			-- Wrong test 2 for {B2D_JOINT_GEAR_DEFINITION}.`set_joint_1'
		note
			testing:
				"covers/{B2D_JOINT_GEAR}.set_joint_1"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_GEAR_DEFINITION
		do
			if not l_retry then
				create l_definition.make (joint_1, joint_2)
				world.create_body (create {B2D_BODY_DEFINITION}.make_dynamic)
				check attached world.last_body as la_body_4 then
					world.create_body (create {B2D_BODY_DEFINITION}.make_dynamic)
					check attached world.last_body as la_body_5 then
						world.create_joint (create {B2D_JOINT_PRISMATIC_DEFINITION}.make_with_vector_and_angle (la_body_4, la_body_5, la_body_4.center_of_mass, {DOUBLE_MATH}.pi_4.truncated_to_real))
						check attached {B2D_JOINT_PRISMATIC} world.last_joint as la_new_joint then
							la_new_joint.put_null
							l_definition.set_joint_1 (la_new_joint)
							assert ("set_joint_1 valid.", False)
						end
					end
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_joint_2_wrong_1
			-- Wrong test 1 for {B2D_JOINT_GEAR_DEFINITION}.`set_joint_2'
		note
			testing:
				"covers/{B2D_JOINT_GEAR}.set_joint_2"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_GEAR_DEFINITION
		do
			if not l_retry then
				create l_definition.make (joint_1, joint_2)
				world.create_body (create {B2D_BODY_DEFINITION}.make_dynamic)
				check attached world.last_body as la_body_4 then
					world.create_body (create {B2D_BODY_DEFINITION}.make_dynamic)
					check attached world.last_body as la_body_5 then
						world.create_joint (create {B2D_JOINT_PRISMATIC_DEFINITION}.make_with_vector_and_angle (la_body_4, la_body_5, la_body_4.center_of_mass, {DOUBLE_MATH}.pi_4.truncated_to_real))
						check attached {B2D_JOINT_PRISMATIC} world.last_joint as la_new_joint then
							l_definition.put_null
							l_definition.set_joint_2 (la_new_joint)
							assert ("set_joint_2 valid.", False)
						end
					end
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_joint_2_wrong_2
			-- Wrong test 2 for {B2D_JOINT_GEAR_DEFINITION}.`set_joint_2'
		note
			testing:
				"covers/{B2D_JOINT_GEAR}.set_joint_2"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_GEAR_DEFINITION
		do
			if not l_retry then
				create l_definition.make (joint_1, joint_2)
				world.create_body (create {B2D_BODY_DEFINITION}.make_dynamic)
				check attached world.last_body as la_body_4 then
					world.create_body (create {B2D_BODY_DEFINITION}.make_dynamic)
					check attached world.last_body as la_body_5 then
						world.create_joint (create {B2D_JOINT_PRISMATIC_DEFINITION}.make_with_vector_and_angle (la_body_4, la_body_5, la_body_4.center_of_mass, {DOUBLE_MATH}.pi_4.truncated_to_real))
						check attached {B2D_JOINT_PRISMATIC} world.last_joint as la_new_joint then
							la_new_joint.put_null
							l_definition.set_joint_2 (la_new_joint)
							assert ("set_joint_2 valid.", False)
						end
					end
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

feature {NONE} -- Implementation
	world:B2D_WORLD
			-- The {B2D_WORLD} used in `Current'

	joint_1, joint_2:B2D_JOINT_GEARABLE
			-- {B2D_JOINT} used in `Current'
end


