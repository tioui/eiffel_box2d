note
	description: "Tests for {B2D_ROTATION}"
	author: "Louis Marchand"
	date: "Fri, 12 Jun 2020 22:31:53 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_TRANSFORM_TESTS

inherit
	EQA_TEST_SET
	B2D_ANY
		undefine
			default_create
		end

feature -- Test routines

	test_make_normal
			-- Normal test {B2D_TRANSFORM}.make
		note
			testing:  "covers/{B2D_TRANSFORM}.make", "covers/{B2D_TRANSFORM}.position", "covers/{B2D_TRANSFORM}.rotation"
		local
			l_transform:B2D_TRANSFORM
		do
			create l_transform.make
			assert ("make Valid: X", l_transform.position.x ~ 0.0)
			assert ("make Valid: Y", l_transform.position.y ~ 0.0)
			assert ("make Valid: Angle", l_transform.rotation.angle ~ 0.0)
		end

	test_default_create_normal
			-- Normal test {B2D_TRANSFORM}.default_create
		note
			testing:  "covers/{B2D_TRANSFORM}.default_create", "covers/{B2D_TRANSFORM}.position", "covers/{B2D_TRANSFORM}.rotation"
		local
			l_transform:B2D_TRANSFORM
		do
			create l_transform
			assert ("default_create Valid: X", l_transform.position.x ~ 0.0)
			assert ("default_create Valid: Y", l_transform.position.y ~ 0.0)
			assert ("default_create Valid: Angle", l_transform.rotation.angle ~ 0.0)
		end

	test_make_with_coordinates_and_angle_normal
			-- Normal test {B2D_TRANSFORM}.make_with_coordinates_and_angle
		note
			testing:  "covers/{B2D_TRANSFORM}.make_with_coordinates_and_angle", "covers/{B2D_TRANSFORM}.position", "covers/{B2D_TRANSFORM}.rotation"
		local
			l_transform:B2D_TRANSFORM
		do
			create l_transform.make_with_coordinates_and_angle(
						349.20358, -335.35082, 2.15)
			assert ("make_with_coordinates_and_angle Valid: X", l_transform.position.x ~ 349.20358)
			assert ("make_with_coordinates_and_angle Valid: Y", l_transform.position.y ~ -335.35082)
			assert ("make_with_coordinates_and_angle Valid: Angle", l_transform.rotation.angle ~ 2.15)
		end

	test_make_from_item_normal
			-- Normal test {B2D_TRANSFORM}.make_from_item
		note
			testing:  "covers/{B2D_TRANSFORM}.make_with_coordinates_and_angle", "covers/{B2D_TRANSFORM}.position", "covers/{B2D_TRANSFORM}.rotation"
		local
			l_transform_1, l_transform_2:B2D_TRANSFORM
		do
			create l_transform_1.make_with_coordinates_and_angle(
						349.20358, -335.35082, 2.15)
			create l_transform_2.make_from_item (l_transform_1.item)
			assert ("make_from_item Valid: item", l_transform_2.item /~ l_transform_1.item)
			assert ("make_from_item Valid: object", l_transform_2 ~ l_transform_1)
		end

	test_make_with_vector_and_rotation
			-- Test {B2D_TRANSFORM}.make_with_vector_and_rotation
		note
			testing:  "covers/{B2D_TRANSFORM}.make_with_vector_and_rotation", "covers/{B2D_TRANSFORM}.position", "covers/{B2D_TRANSFORM}.rotation"
		local
			l_transform:B2D_TRANSFORM
			l_position:B2D_VECTOR_2D
			l_rotation:B2D_ROTATION
		do
			create l_position.make_with_coordinates (349.20358, -335.35082)
			create l_rotation.make (2.15)
			create l_transform.make_with_vector_and_rotation(
						l_position, l_rotation)
			assert ("make_with_vector_and_rotation Valid: Position", l_transform.position ~ l_position)
			assert ("make_with_vector_and_rotation Valid: Rotation", l_transform.rotation ~ l_rotation)
		end

	test_set_identity
			-- Test {B2D_TRANSFORM}.set_identity
		note
			testing:  "covers/{B2D_TRANSFORM}.set_identity", "covers/{B2D_TRANSFORM}.position", "covers/{B2D_TRANSFORM}.rotation"
		local
			l_transform:B2D_TRANSFORM
		do
			create l_transform.make_with_coordinates_and_angle (10, 11, 1)
			l_transform.set_identity
			assert ("set_identity Valid: X", l_transform.position.x ~ 0.0)
			assert ("set_identity Valid: Y", l_transform.position.y ~ 0.0)
			assert ("set_identity Valid: Angle", l_transform.rotation.angle ~ 0.0)
		end

	test_set_position
			-- Test {B2D_TRANSFORM}.set_position
		note
			testing:  "covers/{B2D_TRANSFORM}.set_position", "covers/{B2D_TRANSFORM}.position", "covers/{B2D_TRANSFORM}.rotation"
		local
			l_transform:B2D_TRANSFORM
		do
			create l_transform.make_with_coordinates_and_angle(
						349.20358, -335.35082, 2.15)
			l_transform.set_position (165.71355, 38.06875)
			assert ("set_position Valid: X", l_transform.position.x ~ 165.71355)
			assert ("set_position Valid: Y", l_transform.position.y ~ 38.06875)
		end

	test_set_position_from_vector
			-- Test {B2D_TRANSFORM}.set_position_from_vector
		note
			testing:  "covers/{B2D_TRANSFORM}.set_position_from_vector", "covers/{B2D_TRANSFORM}.position", "covers/{B2D_TRANSFORM}.rotation"
		local
			l_transform:B2D_TRANSFORM
			l_position:B2D_VECTOR_2D
		do
			create l_position.make_with_coordinates (165.71355, 38.06875)
			create l_transform.make_with_coordinates_and_angle(
						349.20358, -335.35082, 2.15)
			l_transform.set_position_from_vector(l_position)
			assert ("set_position_from_vector Valid: Position", l_transform.position ~ l_position)
		end

	test_set_rotation
			-- Test {B2D_TRANSFORM}.set_rotation
		note
			testing:  "covers/{B2D_TRANSFORM}.set_rotation", "covers/{B2D_TRANSFORM}.position", "covers/{B2D_TRANSFORM}.rotation"
		local
			l_transform:B2D_TRANSFORM
		do
			create l_transform.make_with_coordinates_and_angle(
						349.20358, -335.35082, 2.15)
			l_transform.set_rotation (-1.8)
			assert ("set_rotation Valid", l_transform.rotation.angle ~ -1.8)
		end

	test_is_equal_normal_1
			-- Normal Test 1 {B2D_TRANSFORM}.is_equal
		note
			testing:  "covers/{B2D_TRANSFORM}.is_equal"
		local
			l_transform1, l_transform2:B2D_TRANSFORM
		do
			create l_transform1.make_with_coordinates_and_angle(
						349.20358, -335.35082, 2.15)
			create l_transform2.make_with_coordinates_and_angle(
						349.20358, -335.35082, 2.15)
			assert ("is_equal Valid", l_transform1 ~ l_transform2)
		end

	test_is_equal_normal_2
			-- Normal Test 2 {B2D_TRANSFORM}.is_equal
		note
			testing:  "covers/{B2D_TRANSFORM}.is_equal"
		local
			l_transform1, l_transform2:B2D_TRANSFORM
		do
			create l_transform1.make_with_coordinates_and_angle(
						349.20358, -335.35082, 2.15)
			create l_transform2.make_with_coordinates_and_angle(
						-198.2923, -335.35082, 2.15)
			assert ("is_equal Valid", l_transform1 /~ l_transform2)
		end

	test_is_equal_normal_3
			-- Normal Test 3 {B2D_TRANSFORM}.is_equal
		note
			testing:  "covers/{B2D_TRANSFORM}.is_equal"
		local
			l_transform1, l_transform2:B2D_TRANSFORM
		do
			create l_transform1.make_with_coordinates_and_angle(
						349.20358, -335.35082, 2.15)
			create l_transform2.make_with_coordinates_and_angle(
						349.20358, -198.2923, 2.15)
			assert ("is_equal Valid", l_transform1 /~ l_transform2)
		end

	test_is_equal_normal_4
			-- Normal Test 4 {B2D_TRANSFORM}.is_equal
		note
			testing:  "covers/{B2D_TRANSFORM}.is_equal"
		local
			l_transform1, l_transform2:B2D_TRANSFORM
		do
			create l_transform1.make_with_coordinates_and_angle(
						349.20358, -335.35082, 2.15)
			create l_transform2.make_with_coordinates_and_angle(
						349.20358, -335.35082, 1.89)
			assert ("is_equal Valid", l_transform1 /~ l_transform2)
		end

	test_is_equal_normal_5
			-- Normal Test 5 {B2D_TRANSFORM}.is_equal
		note
			testing:  "covers/{B2D_TRANSFORM}.is_equal"
		local
			l_transform1, l_transform2:B2D_TRANSFORM
		do
			create l_transform1.own_from_item(create {POINTER})
			create l_transform2.make_with_coordinates_and_angle(
						349.20358, -335.35082, 1.89)
			assert ("is_equal Valid", l_transform1 /~ l_transform2)
		end

	test_is_equal_normal_6
			-- Normal Test 6 {B2D_TRANSFORM}.is_equal
		note
			testing:  "covers/{B2D_TRANSFORM}.is_equal"
		local
			l_transform1, l_transform2:B2D_TRANSFORM
		do
			create l_transform1.make_with_coordinates_and_angle(
						349.20358, -335.35082, 2.15)
			create l_transform2.own_from_item(create {POINTER})
			assert ("is_equal Valid", l_transform1 /~ l_transform2)
		end

	test_is_equal_normal_7
			-- Normal Test 7 {B2D_TRANSFORM}.is_equal
		note
			testing:  "covers/{B2D_TRANSFORM}.is_equal"
		local
			l_transform1, l_transform2:B2D_TRANSFORM
		do
			create l_transform1.own_from_item(create {POINTER})
			create l_transform2.own_from_item(create {POINTER})
			assert ("is_equal Valid", l_transform1 ~ l_transform2)
		end

	test_position_wrong
			-- Wrong test for position
		note
			testing:  "covers/{B2D_TRANSFORM}.position"
		local
			l_retry:BOOLEAN
			l_rotation:B2D_TRANSFORM
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_rotation.own_from_item(create {POINTER})
				l_result := l_rotation.position
				assert("position Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_rotation_wrong
			-- Wrong test for rotation
		note
			testing:  "covers/{B2D_TRANSFORM}.rotation"
		local
			l_retry:BOOLEAN
			l_rotation:B2D_TRANSFORM
			l_result:B2D_ROTATION
		do
			if not l_retry then
				create l_rotation.own_from_item(create {POINTER})
				l_result := l_rotation.rotation
				assert("rotation Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_rotation_wrong
			-- Wrong test for set_rotation
		note
			testing:  "covers/{B2D_TRANSFORM}.set_rotation"
		local
			l_retry:BOOLEAN
			l_rotation:B2D_TRANSFORM
		do
			if not l_retry then
				create l_rotation.own_from_item(create {POINTER})
				l_rotation.set_rotation(10)
				assert("set_rotation Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_position_wrong
			-- Wrong test for set_position
		note
			testing:  "covers/{B2D_TRANSFORM}.set_position"
		local
			l_retry:BOOLEAN
			l_rotation:B2D_TRANSFORM
		do
			if not l_retry then
				create l_rotation.own_from_item(create {POINTER})
				l_rotation.set_position(10, 20)
				assert("set_position Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_position_from_vector_wrong
			-- Wrong test for set_position_from_vector
		note
			testing:  "covers/{B2D_TRANSFORM}.set_position_from_vector"
		local
			l_retry:BOOLEAN
			l_rotation:B2D_TRANSFORM
		do
			if not l_retry then
				create l_rotation.own_from_item(create {POINTER})
				l_rotation.set_position_from_vector(create {B2D_VECTOR_2D})
				assert("set_position_from_vector Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_identity_wrong
			-- Wrong test for set_identity
		note
			testing:  "covers/{B2D_TRANSFORM}.set_identity"
		local
			l_retry:BOOLEAN
			l_rotation:B2D_TRANSFORM
		do
			if not l_retry then
				create l_rotation.own_from_item(create {POINTER})
				l_rotation.set_identity
				assert("set_identity Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

end


