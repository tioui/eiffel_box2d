note
	description: "Tests for {B2D_JOINT_WELD_DEFINITION}."
	author: "Patrick Boucher"
	date: "Fri, 10 Jul 2020 20:34:44 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_JOINT_WELD_DEFINITION_TESTS

inherit
	EQA_TEST_SET
		redefine
			on_prepare
		end
	B2D_ANY
		undefine
			default_create
		end
	B2D_COMPARE_REAL
		undefine
			default_create
		end

feature {NONE} -- Events

	on_prepare
			-- <Precursor>
		local
			l_body_definition:B2D_BODY_DEFINITION
		do
			create world.make (0, -9.8)
			create l_body_definition
			world.create_body (l_body_definition)
			body_1 := world.last_body
			world.create_body (l_body_definition)
			body_2 := world.last_body
		end

	world:B2D_WORLD
			-- The world in which the tests will be performed.

	body_1:detachable B2D_BODY
			-- The first body of each {B2D_JOINT_WELD_DEFINITION} during the tests.

	body_2:detachable B2D_BODY
			-- The second body of each {B2D_JOINT_WELD_DEFINITION} during the tests.

feature -- Normal test routines

	test_make_normal
			-- Normal test for `make'.
		note
			testing:  "covers/{B2D_JOINT_WELD_DEFINITION}.make",
					  "covers/{B2D_JOINT_WELD_DEFINITION}.local_anchor_1",
					  "covers/{B2D_JOINT_WELD_DEFINITION}.local_anchor_2"
		local
			l_definition:B2D_JOINT_WELD_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				la_body_1.set_transform (-2, 2, 0)
				la_body_2.set_transform (2, -2, 0)
				create l_definition.make (la_body_1, la_body_2, 2, 2)
				assert ("body_1 is assigned", l_definition.body_1 ~ la_body_1)
				assert ("body_2 is assigned", l_definition.body_2 ~ la_body_2)
				assert ("anchor valid x", l_definition.anchor.x ~ 2)
				assert ("anchor valid y", l_definition.anchor.y ~ 2)
				assert ("local_anchor_1 is assigned x", l_definition.local_anchor_1.x ~ 4)
				assert ("local_anchor_1 is assigned y", l_definition.local_anchor_1.y ~ 0)
				assert ("local_anchor_2 is assigned x", l_definition.local_anchor_2.x ~ 0)
				assert ("local_anchor_2 is assigned y", l_definition.local_anchor_2.y ~ 4)
			end
		end

	test_make_with_vector_normal
			-- Normal test for `make_with_vector'.
		note
			testing:  "covers/{B2D_JOINT_WELD_DEFINITION}.make_with_vector",
					  "covers/{B2D_JOINT_WELD_DEFINITION}.local_anchor_1",
					  "covers/{B2D_JOINT_WELD_DEFINITION}.local_anchor_2"
		local
			l_definition:B2D_JOINT_WELD_DEFINITION
			l_anchor:B2D_VECTOR_2D
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				la_body_1.set_transform (-2, 2, 0)
				la_body_2.set_transform (2, -2, 0)
				create l_anchor.make_with_coordinates (2, 2)
				create l_definition.make_with_vector (la_body_1, la_body_2, l_anchor)
				assert ("body_1 is assigned", l_definition.body_1 ~ la_body_1)
				assert ("body_2 is assigned", l_definition.body_2 ~ la_body_2)
				assert ("anchor valid x", l_definition.anchor.x ~ 2)
				assert ("anchor valid y", l_definition.anchor.y ~ 2)
				assert ("local_anchor_1 is assigned x", l_definition.local_anchor_1.x ~ 4)
				assert ("local_anchor_1 is assigned y", l_definition.local_anchor_1.y ~ 0)
				assert ("local_anchor_2 is assigned x", l_definition.local_anchor_2.x ~ 0)
				assert ("local_anchor_2 is assigned y", l_definition.local_anchor_2.y ~ 4)
			end
		end

	test_local_anchor_1_normal
			-- Normal test for `local_anchor_1'.
		note
			testing:  "covers/{B2D_JOINT_WELD_DEFINITION}.local_anchor_1",
					  "covers/{B2D_JOINT_WELD_DEFINITION}.set_anchor_with_vector"
		local
			l_definition:B2D_JOINT_WELD_DEFINITION
			l_anchor:B2D_VECTOR_2D
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				la_body_1.set_transform (2, 2, 0)
				create l_definition.make (la_body_1, la_body_2, 0, 0)
				create l_anchor.make_with_coordinates (-1, 3)
				l_definition.set_anchor_with_vector (l_anchor)
				assert ("set_anchor_with_vector valid x", l_definition.local_anchor_1.x ~ -3)
				assert ("set_anchor_with_vector valid y", l_definition.local_anchor_1.y ~ 1)
			end
		end

	test_local_anchor_2_normal
			-- Normal test for `local_anchor_2'.
		note
			testing:  "covers/{B2D_JOINT_WELD_DEFINITION}.local_anchor_2",
					  "covers/{B2D_JOINT_WELD_DEFINITION}.set_anchor_with_vector"
		local
			l_definition:B2D_JOINT_WELD_DEFINITION
			l_anchor:B2D_VECTOR_2D
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				la_body_2.set_transform (2, 2, 0)
				create l_definition.make (la_body_1, la_body_2, 0, 0)
				create l_anchor.make_with_coordinates (-1, 3)
				l_definition.set_anchor_with_vector (l_anchor)
				assert ("set_anchor_with_vector valid x", l_definition.local_anchor_2.x ~ -3)
				assert ("set_anchor_with_vector valid y", l_definition.local_anchor_2.y ~ 1)
			end
		end

	test_reference_angle_normal
			-- Normal test for `reference_angle'.
		note
			testing:  "covers/{B2D_JOINT_WELD_DEFINITION}.reference_angle",
					  "covers/{B2D_JOINT_WELD_DEFINITION}.update_reference_angle"
		local
			l_definition:B2D_JOINT_WELD_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				la_body_1.set_transform (0, 0, {MATH_CONST}.pi_2.truncated_to_real)
				create l_definition.make (la_body_1, la_body_2, 0, 0)
				assert ("reference_angle valid", compare_real_32 (l_definition.reference_angle, -{MATH_CONST}.pi_2.truncated_to_real))
				la_body_2.set_transform (0, 0, -{MATH_CONST}.pi_2.truncated_to_real)
				l_definition.update_reference_angle
				assert ("update_reference_angle valid", compare_real_32 (l_definition.reference_angle, -{MATH_CONST}.pi.truncated_to_real))
			end
		end

	test_frequency_normal
			-- Normal test for `frequency'.
		note
			testing:  "covers/{B2D_JOINT_WELD_DEFINITION}.frequency",
					  "covers/{B2D_JOINT_WELD_DEFINITION}.set_frequency"
		local
			l_definition:B2D_JOINT_WELD_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0)
				l_definition.set_frequency (20.5)
				assert ("set_frequency valid", l_definition.frequency ~ 20.5)
			end
		end

	test_damping_ratio_normal
			-- Normal test for `damping_ratio'.
		note
			testing:  "covers/{B2D_JOINT_WELD_DEFINITION}.damping_ratio",
					  "covers/{B2D_JOINT_WELD_DEFINITION}.set_damping_ratio"
		local
			l_definition:B2D_JOINT_WELD_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0)
				l_definition.set_damping_ratio (0.236)
				assert ("set_damping_ratio valid", l_definition.damping_ratio ~ 0.236)
			end
		end

feature -- Limit test routines

	test_frequency_limit_1
			-- Limit test for `frequency'.
		note
			testing:  "covers/{B2D_JOINT_WELD_DEFINITION}.frequency",
					  "covers/{B2D_JOINT_WELD_DEFINITION}.set_frequency"
		local
			l_definition:B2D_JOINT_WELD_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0)
				l_definition.set_frequency (0)
				assert ("set_frequency valid", l_definition.frequency ~ 0)
			end
		end

	test_frequency_limit_2
			-- Limit test for `frequency'.
		note
			testing:  "covers/{B2D_JOINT_WELD_DEFINITION}.frequency",
					  "covers/{B2D_JOINT_WELD_DEFINITION}.set_frequency"
		local
			l_definition:B2D_JOINT_WELD_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0)
				l_definition.set_frequency (30)
				assert ("set_frequency valid", l_definition.frequency ~ 30)
			end
		end

	test_damping_ratio_limit
			-- Limit test for `damping_ratio'.
		note
			testing:  "covers/{B2D_JOINT_WELD_DEFINITION}.damping_ratio",
					  "covers/{B2D_JOINT_WELD_DEFINITION}.set_damping_ratio"
		local
			l_definition:B2D_JOINT_WELD_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0)
				l_definition.set_damping_ratio (0)
				assert ("set_damping_ratio valid", l_definition.damping_ratio ~ 0)
			end
		end

feature -- Wrong test routines

	test_make_wrong
			-- Test for `make' when arguments do not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.make"
		local
			l_retry:INTEGER
			l_definition:B2D_JOINT_WELD_DEFINITION
		do
			if l_retry ~ 0 then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					la_body_1.put_null
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					assert ("a_body_1 exists", False)
				end
			elseif l_retry ~ 1 then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					la_body_2.put_null
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					assert ("a_body_2 exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := l_retry + 1
				retry
			end
		end

	test_make_with_vector_wrong
			-- Test for `' when .
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.make_with_vector"
		local
			l_retry:INTEGER
			l_definition:B2D_JOINT_WELD_DEFINITION
			l_anchor:B2D_VECTOR_2D
		do
			if l_retry ~ 0 then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					la_body_1.put_null
					create l_anchor
					create l_definition.make_with_vector (la_body_1, la_body_2, l_anchor)
					assert ("a_body_1 exists", False)
				end
			elseif l_retry ~ 1 then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					la_body_2.put_null
					create l_anchor
					create l_definition.make_with_vector (la_body_1, la_body_2, l_anchor)
					assert ("a_body_2 exists", False)
				end
			elseif l_retry ~ 2 then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_anchor
					l_anchor.put_null
					create l_definition.make_with_vector (la_body_1, la_body_2, l_anchor)
					assert ("a_anchor exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := l_retry + 1
				retry
			end
		end

	test_local_anchor_1_wrong
			-- Test for `local_anchor_1' when `item' does not exists.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.local_anchor_1"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WELD_DEFINITION
			l_discard:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_discard := l_definition.local_anchor_1
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_local_anchor_2_wrong
			-- Test for `local_anchor_2' when `item' does not exists.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.local_anchor_2"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WELD_DEFINITION
			l_discard:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_discard := l_definition.local_anchor_2
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_anchor_with_vector_wrong
			-- Test for `set_anchor_with_vector' when `item' does not exists.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_anchor_with_vector"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WELD_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_definition.set_anchor_with_vector (create {B2D_VECTOR_2D})
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_reference_angle_wrong
			-- Test for `reference_angle' when `item' does not exists.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.reference_angle"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WELD_DEFINITION
			l_discard:REAL_32
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_discard := l_definition.reference_angle
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_update_reference_angle_wrong
			-- Test for `update_reference_angle' when `item' does not exists.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.update_reference_angle"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WELD_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_definition.update_reference_angle
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_frequency_wrong
			-- Test for `frequency' when `item' does not exists.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.frequency"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WELD_DEFINITION
			l_discard:REAL_32
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_discard := l_definition.frequency
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_frequency_wrong_1
			-- Test for `set_frequency' when `item' does not exists.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_frequency"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WELD_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_definition.set_frequency (12)
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_frequency_wrong_2
			-- Test for `set_frequency' when `a_value' is negative.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}."
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WELD_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.set_frequency (-0.01)
					assert ("set_frequency valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_damping_ratio_wrong
			-- Test for `damping_ratio' when `item' does not exists.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.damping_ratio"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WELD_DEFINITION
			l_discard:REAL_32
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_discard := l_definition.damping_ratio
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_damping_ratio_wrong_1
			-- Test for `set_damping_ratio' when `item' does not exists.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_damping_ratio"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WELD_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_definition.set_damping_ratio (0.5)
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_damping_ratio_wrong_2
			-- Test for `set_damping_ratio' when `a_value' is negative.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_damping_ratio"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WELD_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.set_damping_ratio (-0.01)
					assert ("set_damping_ratio valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

end


