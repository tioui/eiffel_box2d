note
	description: "Tests for {B2D_ROTATION}"
	author: "Louis Marchand"
	date: "Fri, 12 Jun 2020 20:47:45 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_ROTATION_TESTS

inherit
	EQA_TEST_SET
	B2D_COMPARE_REAL
		undefine
			default_create
		end
	B2D_ANY
		undefine
			default_create
		end

feature -- Test routines

	test_make_normal_1
			-- Normal test {B2D_ROTATION}.make
		note
			testing:  "covers/{B2D_ROTATION}.make", "covers/{B2D_ROTATION}.angle"
		local
			l_rotation:B2D_ROTATION
		do
			create l_rotation.make (3.10)
			assert ("make Valid", l_rotation.angle ~ 3.10)
		end

	test_make_from_item_normal_1
			-- Normal test {B2D_ROTATION}.make_from_item
		note
			testing:  "covers/{B2D_ROTATION}.make_from_item", "covers/{B2D_ROTATION}.angle"
		local
			l_rotation_1, l_rotation_2:B2D_ROTATION
		do
			create l_rotation_1.make (3.10)
			create l_rotation_2.make_from_item (l_rotation_1.item)
			assert ("make_from_item Valid. item", l_rotation_1.item /~ l_rotation_2.item)
			assert ("make_from_item Valid. object", l_rotation_1 ~ l_rotation_2)
		end

	test_make_normal_2
			-- Normal test {B2D_ROTATION}.make
		note
			testing:  "covers/{B2D_ROTATION}.make", "covers/{B2D_ROTATION}.angle"
		local
			l_rotation:B2D_ROTATION
		do
			create l_rotation.make (4.10)
			assert ("make Valid", compare_real_32 (-2.18319, l_rotation.angle))
		end

	test_default_create_normal_1
			-- Normal test {B2D_ROTATION}.default_create
		note
			testing:  "covers/{B2D_ROTATION}.default_create", "covers/{B2D_ROTATION}.angle"
		local
			l_rotation:B2D_ROTATION
		do
			create l_rotation
			assert ("default_create Valid", l_rotation.angle ~ 0)
		end

	test_set_angle_normal_1
			-- Normal test {B2D_ROTATION}.set_angle
		note
			testing:  "covers/{B2D_ROTATION}.set_angle", "covers/{B2D_ROTATION}.angle"
		local
			l_rotation:B2D_ROTATION
		do
			create l_rotation
			l_rotation.set_angle (3.10)
			assert ("set_angle Valid", l_rotation.angle ~ 3.10)
		end

	test_set_angle_normal_2
			-- Normal test {B2D_ROTATION}.set_angle
		note
			testing:  "covers/{B2D_ROTATION}.set_angle", "covers/{B2D_ROTATION}.angle"
		local
			l_rotation:B2D_ROTATION
		do
			create l_rotation
			l_rotation.set_angle (4.10)
			assert ("set_angle Valid", compare_real_32(l_rotation.angle, -2.18319))
		end

	test_set_identity_normal
			-- Normal test {B2D_ROTATION}.set_identity
		note
			testing:  "covers/{B2D_ROTATION}.set_identity", "covers/{B2D_ROTATION}.set_angle", "covers/{B2D_ROTATION}.angle"
		local
			l_rotation:B2D_ROTATION
		do
			create l_rotation
			l_rotation.set_angle (3.10)
			l_rotation.set_identity
			assert ("set_identity Valid", l_rotation.angle ~ 0)
		end

	test_x_axis_normal
			-- Normal test {B2D_ROTATION}.x_axis
		note
			testing:  "covers/{B2D_ROTATION}.x_axis", "covers/{B2D_ROTATION}.make", "covers/{B2D_ROTATION}.angle"
		local
			l_rotation:B2D_ROTATION
			l_axis:B2D_VECTOR_2D
		do
			create l_rotation.make ({DOUBLE_MATH}.pi_4.truncated_to_real)
			l_axis := l_rotation.x_axis
			assert ("x_axis Valid: X", compare_real_32(0.707, l_axis.x))
			assert ("x_axis Valid: Y", compare_real_32(0.707, l_axis.y))
		end

	test_y_axis_normal
			-- Normal test {B2D_ROTATION}.y_axis
		note
			testing:  "covers/{B2D_ROTATION}.y_axis", "covers/{B2D_ROTATION}.make", "covers/{B2D_ROTATION}.angle"
		local
			l_rotation:B2D_ROTATION
			l_axis:B2D_VECTOR_2D
		do
			create l_rotation.make ({DOUBLE_MATH}.pi_4.truncated_to_real)
			l_axis := l_rotation.y_axis
			assert ("x_axis Valid: X", compare_real_32(-0.707, l_axis.x))
			assert ("x_axis Valid: Y", compare_real_32(0.707, l_axis.y))
		end

	test_sine_angle_normal
			-- Normal test {B2D_ROTATION}.sine_angle
		note
			testing:  "covers/{B2D_ROTATION}.sine_angle", "covers/{B2D_ROTATION}.make"
		local
			l_rotation:B2D_ROTATION
		do
			create l_rotation.make ({DOUBLE_MATH}.pi_2.truncated_to_real)
			assert ("sine_angle Valid", compare_real_32(1, l_rotation.sine_angle))
		end

	test_cosine_angle_normal
			-- Normal test {B2D_ROTATION}.cosine_angle
		note
			testing:  "covers/{B2D_ROTATION}.cosine_angle", "covers/{B2D_ROTATION}.make"
		local
			l_rotation:B2D_ROTATION
		do
			create l_rotation.make ({DOUBLE_MATH}.pi_2.truncated_to_real)
			assert ("cosine_angle Valid", compare_real_32(0, l_rotation.cosine_angle))
		end

	test_is_equal_normal_1
			-- Normal test 1 {B2D_ROTATION}.is_equal
		note
			testing:  "covers/{B2D_ROTATION}.is_equal", "covers/{B2D_ROTATION}.make"
		local
			l_rotation1, l_rotation2:B2D_ROTATION
		do
			create l_rotation1.make (-343.10126)
			create l_rotation2.make (-343.10126)
			assert ("is_equal Valid", l_rotation1 ~ l_rotation2)
		end

	test_is_equal_normal_2
			-- Normal test 2 {B2D_ROTATION}.is_equal
		note
			testing:  "covers/{B2D_ROTATION}.is_equal", "covers/{B2D_ROTATION}.make"
		local
			l_rotation1, l_rotation2:B2D_ROTATION
		do
			create l_rotation1.make (-343.10126)
			create l_rotation2.make (-335.04707)
			assert ("is_equal Valid", l_rotation1 /~ l_rotation2)
		end

	test_is_equal_normal_3
			-- Normal test 3 {B2D_ROTATION}.is_equal
		note
			testing:  "covers/{B2D_ROTATION}.is_equal", "covers/{B2D_ROTATION}.make"
		local
			l_rotation1, l_rotation2:B2D_ROTATION
		do
			create l_rotation1.own_from_item(create {POINTER})
			create l_rotation2.make (-335.04707)
			assert ("is_equal Valid", l_rotation1 /~ l_rotation2)
		end

	test_is_equal_normal_4
			-- Normal test 4 {B2D_ROTATION}.is_equal
		note
			testing:  "covers/{B2D_ROTATION}.is_equal", "covers/{B2D_ROTATION}.make"
		local
			l_rotation1, l_rotation2:B2D_ROTATION
		do
			create l_rotation1.make (-343.10126)
			create l_rotation2.own_from_item(create {POINTER})
			assert ("is_equal Valid", l_rotation1 /~ l_rotation2)
		end

	test_is_equal_normal_5
			-- Normal test 5 {B2D_ROTATION}.is_equal
		note
			testing:  "covers/{B2D_ROTATION}.is_equal", "covers/{B2D_ROTATION}.make"
		local
			l_rotation1, l_rotation2:B2D_ROTATION
		do
			create l_rotation1.own_from_item(create {POINTER})
			create l_rotation2.own_from_item(create {POINTER})
			assert ("is_equal Valid", l_rotation1 ~ l_rotation2)
		end

	test_angle_wrong
			-- Wrong test for angle
		note
			testing:  "covers/{B2D_ROTATION}.angle"
		local
			l_retry:BOOLEAN
			l_rotation:B2D_ROTATION
			l_result:REAL_32
		do
			if not l_retry then
				create l_rotation.own_from_item(create {POINTER})
				l_result := l_rotation.angle
				assert("angle Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_angle_wrong
			-- Wrong test for set_angle
		note
			testing:  "covers/{B2D_ROTATION}.set_angle"
		local
			l_retry:BOOLEAN
			l_rotation:B2D_ROTATION
		do
			if not l_retry then
				create l_rotation.own_from_item(create {POINTER})
				l_rotation.set_angle(10)
				assert("set_angle Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_identity_wrong
			-- Wrong test for set_identity
		note
			testing:  "covers/{B2D_ROTATION}.set_identity"
		local
			l_retry:BOOLEAN
			l_rotation:B2D_ROTATION
		do
			if not l_retry then
				create l_rotation.own_from_item(create {POINTER})
				l_rotation.set_identity
				assert("set_identity Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_x_axis_wrong
			-- Wrong test for x_axis
		note
			testing:  "covers/{B2D_ROTATION}.x_axis"
		local
			l_retry:BOOLEAN
			l_rotation:B2D_ROTATION
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_rotation.own_from_item(create {POINTER})
				l_result := l_rotation.x_axis
				assert("x_axis Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_y_axis_wrong
			-- Wrong test for y_axis
		note
			testing:  "covers/{B2D_ROTATION}.y_axis"
		local
			l_retry:BOOLEAN
			l_rotation:B2D_ROTATION
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_rotation.own_from_item(create {POINTER})
				l_result := l_rotation.y_axis
				assert("y_axis Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_cosine_angle_wrong
			-- Wrong test for cosine_angle
		note
			testing:  "covers/{B2D_ROTATION}.cosine_angle"
		local
			l_retry:BOOLEAN
			l_rotation:B2D_ROTATION
			l_result:REAL_32
		do
			if not l_retry then
				create l_rotation.own_from_item(create {POINTER})
				l_result := l_rotation.cosine_angle
				assert("cosine_angle Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_sine_angle_wrong
			-- Wrong test for sine_angle
		note
			testing:  "covers/{B2D_ROTATION}.sine_angle"
		local
			l_retry:BOOLEAN
			l_rotation:B2D_ROTATION
			l_result:REAL_32
		do
			if not l_retry then
				create l_rotation.own_from_item(create {POINTER})
				l_result := l_rotation.sine_angle
				assert("sine_angle Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

end


