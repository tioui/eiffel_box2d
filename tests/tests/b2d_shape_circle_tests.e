note
	description: "Tests for {B2D_SHAPE_CIRCLE}"
	author: "Louis Marchand"
	date: "Fri, 12 Jun 2020 18:05:46 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_SHAPE_CIRCLE_TESTS

inherit
	EQA_TEST_SET
	B2D_COMPARE_REAL
		undefine
			default_create
		end
	B2D_ANY
		undefine
			default_create
		end

feature -- Test routines

	test_default_create_normal
			-- Normal test {B2D_SHAPE_CIRCLE}.default_create
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.default_create"
		local
			l_shape:B2D_SHAPE_CIRCLE
		do
			create l_shape
		end

	test_make_normal
			-- Normal test {B2D_SHAPE_CIRCLE}.make
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.make"
		local
			l_shape:B2D_SHAPE_CIRCLE
		do
			create l_shape.make
		end

	test_make_with_coordinates_normal
			-- Normal test {B2D_SHAPE_CIRCLE}.make_with_coordinates
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.make_with_coordinates", "covers/{B2D_SHAPE_CIRCLE}.position",
						"covers/{B2D_SHAPE_CIRCLE}.radius"
		local
			l_input:B2D_SHAPE_CIRCLE
		do
			create l_input.make_with_coordinates (3.36435, -93.03718, -164.57557)
			assert("make_from_coordinates Valid: position.x", l_input.position.x ~ 3.36435)
			assert("make_from_coordinates Valid: position.y", l_input.position.y ~ -93.03718)
			assert("make_with_coordinates Valid: radius", l_input.radius ~ -164.57557)
		end

	test_make_with_vector_normal
			-- Normal test {B2D_SHAPE_CIRCLE}.make_with_vector
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.make_with_vector", "covers/{B2D_SHAPE_CIRCLE}.position",
						"covers/{B2D_SHAPE_CIRCLE}.radius"
		local
			l_input:B2D_SHAPE_CIRCLE
			l_vector:B2D_VECTOR_2D
		do
			create l_vector.make_with_coordinates (3.36435, -93.03718)
			create l_input.make_with_vector (l_vector, -164.57557)
			assert("make_with_vector Valid: position.x", l_input.position.x ~ 3.36435)
			assert("make_with_vector Valid: position.y", l_input.position.y ~ -93.03718)
			assert("make_with_vector Valid: radius", l_input.radius ~ -164.57557)
		end

	test_child_count_normal
			-- Normal test {B2D_SHAPE_CIRCLE}.child_count
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.child_count", "covers/{B2D_SHAPE_CIRCLE}.default_create"
		local
			l_input:B2D_SHAPE
		do
			create {B2D_SHAPE_CIRCLE}l_input
			assert("child_count Valid", l_input.child_count ~ 1)
		end

	test_set_radius_normal
			-- Normal test {B2D_SHAPE_CIRCLE}.set_radius
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.set_radius", "covers/{B2D_SHAPE_CIRCLE}.default_create",
						"covers/{B2D_SHAPE_CIRCLE}.radius"
		local
			l_input:B2D_SHAPE_CIRCLE
		do
			create l_input
			l_input.set_radius (639.51548)
			assert("set_radius Valid", l_input.radius ~ 639.51548)
		end

	test_contains_normal_1
			-- Normal test 1 {B2D_SHAPE_CIRCLE}.contains
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.contains", "covers/{B2D_SHAPE_CIRCLE}.make_with_coordinates"
		local
			l_input:B2D_SHAPE_CIRCLE
			l_transform:B2D_TRANSFORM
			l_point:B2D_VECTOR_2D
		do
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			create l_point.make_with_coordinates (9, 9)
			create l_input.make_with_coordinates (10, 10, 5)
			assert("contains Valid", l_input.contains_with_vector (l_transform, l_point))
		end

	test_contains_normal_2
			-- Normal test 2 {B2D_SHAPE_CIRCLE}.contains
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.contains", "covers/{B2D_SHAPE_CIRCLE}.make_with_coordinates"
		local
			l_input:B2D_SHAPE_CIRCLE
			l_transform:B2D_TRANSFORM
			l_point:B2D_VECTOR_2D
		do
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			create l_point.make_with_coordinates (20, 20)
			create l_input.make_with_coordinates (10, 10, 5)
			assert("contains Valid", not l_input.contains_with_vector (l_transform, l_point))
		end

	test_contains_limit
			-- Limit test {B2D_SHAPE_CIRCLE}.contains
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.contains", "covers/{B2D_SHAPE_CIRCLE}.make_with_coordinates"
		local
			l_input:B2D_SHAPE_CIRCLE
			l_transform:B2D_TRANSFORM
			l_point:B2D_VECTOR_2D
		do
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			create l_point.make_with_coordinates (10, 15)
			create l_input.make_with_coordinates (10, 10, 5)
			assert("contains Valid", l_input.contains_with_vector (l_transform, l_point))
		end

	test_ray_cast_normal_1
			-- Normal test 1 {B2D_SHAPE_CIRCLE}.ray_cast
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.ray_cast", "covers/{B2D_SHAPE_CIRCLE}.make_with_coordinates"
		local
			l_shape:B2D_SHAPE_CIRCLE
			l_input:B2D_RAY_CAST_INPUT
			l_transform:B2D_TRANSFORM
		do
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			create l_shape.make_with_coordinates (10, 10, 5)
			create l_input.make_with_coordinates (10, 20, 10, 10, 15)
			if attached l_shape.ray_cast (l_input, l_transform) as la_output then
				assert("ray_cast Valid. Normal: X", la_output.normal.x ~ 0)
				assert("ray_cast Valid. Normal: Y", la_output.normal.y ~ 1)
				assert("ray_cast Valid. Fraction", la_output.fraction ~ 0.5)
			else
				assert("ray_cast Valid", False)
			end
		end

	test_ray_cast_normal_2
			-- Normal test 2 {B2D_SHAPE_CIRCLE}.ray_cast
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.ray_cast", "covers/{B2D_SHAPE_CIRCLE}.make_with_coordinates"
		local
			l_shape:B2D_SHAPE_CIRCLE
			l_input:B2D_RAY_CAST_INPUT
			l_transform:B2D_TRANSFORM
		do
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			create l_shape.make_with_coordinates (10, 10, 5)
			create l_input.make_with_coordinates (150, 150, 160, 160, 12)
			if attached l_shape.ray_cast (l_input, l_transform) as la_output then
				assert("ray_cast Valid", False)
			else
				assert("ray_cast Valid", True)
			end
		end

	test_ray_cast_normal_3
			-- Normal test 3 {B2D_SHAPE}.ray_cast
		note
			testing:  "covers/{B2D_SHAPE}.ray_cast", "covers/{B2D_SHAPE_CIRCLE}.make_with_coordinates"
		local
			l_shape:B2D_SHAPE
			l_input:B2D_RAY_CAST_INPUT
			l_transform:B2D_TRANSFORM
		do
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			create {B2D_SHAPE_CIRCLE}l_shape.make_with_coordinates (10, 10, 5)
			create l_input.make_with_coordinates (10, 20, 10, 10, 15)
			if attached l_shape.ray_cast (l_input, l_transform, 1) as la_output then
				assert("ray_cast Valid. Normal: X", la_output.normal.x ~ 0)
				assert("ray_cast Valid. Normal: Y", la_output.normal.y ~ 1)
				assert("ray_cast Valid. Fraction", la_output.fraction ~ 0.5)
			else
				assert("ray_cast Valid", False)
			end
		end

	test_ray_cast_normal_4
			-- Normal test 4 {B2D_SHAPE}.ray_cast
		note
			testing:  "covers/{B2D_SHAPE}.ray_cast", "covers/{B2D_SHAPE_CIRCLE}.make_with_coordinates"
		local
			l_shape:B2D_SHAPE
			l_input:B2D_RAY_CAST_INPUT
			l_transform:B2D_TRANSFORM
		do
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			create {B2D_SHAPE_CIRCLE}l_shape.make_with_coordinates (10, 10, 5)
			create l_input.make_with_coordinates (150, 150, 160, 160, 12)
			if attached l_shape.ray_cast (l_input, l_transform, 1) as la_output then
				assert("ray_cast Valid", False)
			else
				assert("ray_cast Valid", True)
			end
		end

	test_ray_cast_wrong_1
			-- Wrong test 1 {B2D_SHAPE_CIRCLE}.ray_cast
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.ray_cast", "covers/{B2D_SHAPE_CIRCLE}.make_with_coordinates"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE
			l_input:B2D_RAY_CAST_INPUT
			l_transform:B2D_TRANSFORM
			l_output: detachable B2D_RAY_CAST_OUTPUT
		do
			if not l_retry then
				create l_transform.make_with_coordinates_and_angle (0, 0, 0)
				create {B2D_SHAPE_CIRCLE}l_shape.make_with_coordinates (10, 10, 5)
				create l_input.make_with_coordinates (150, 150, 160, 160, 12)
				l_output := l_shape.ray_cast (l_input, l_transform, 2)
				assert("ray_cast Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_ray_cast_wrong_2
			-- Wrong test 2 {B2D_SHAPE_CIRCLE}.ray_cast
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.ray_cast", "covers/{B2D_SHAPE_CIRCLE}.make_with_coordinates"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE
			l_input:B2D_RAY_CAST_INPUT
			l_transform:B2D_TRANSFORM
			l_output: detachable B2D_RAY_CAST_OUTPUT
		do
			if not l_retry then
				create l_transform.make_with_coordinates_and_angle (0, 0, 0)
				create {B2D_SHAPE_CIRCLE}l_shape.make_with_coordinates (10, 10, 5)
				create l_input.make_with_coordinates (150, 150, 160, 160, 12)
				l_output := l_shape.ray_cast (l_input, l_transform, 0)
				assert("ray_cast Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_mass_normal
			-- Normal test {B2D_SHAPE_CIRCLE}.mass
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.mass", "covers/{B2D_SHAPE_CIRCLE}.make_with_coordinates"
		local
			l_shape:B2D_SHAPE_CIRCLE
			l_mass:B2D_MASS
		do
			create l_shape.make_with_coordinates (10, 10, 5)
			l_mass := l_shape.mass (10)
			assert("mass Valid: center.x", l_mass.center.x ~ 10)
			assert("mass Valid: center.y", l_mass.center.y ~ 10)
			assert("mass Valid: Mass", compare_real_32 (l_mass.mass, 785.398))
			assert("mass Valid: inertia", compare_real_32 (l_mass.inertia, 166897))
		end

	test_mass_limit
			-- Limit test {B2D_SHAPE_CIRCLE}.mass
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.mass", "covers/{B2D_SHAPE_CIRCLE}.make_with_coordinates"
		local
			l_shape:B2D_SHAPE_CIRCLE
			l_mass:B2D_MASS
		do
			create l_shape.make_with_coordinates (10, 10, 5)
			l_mass := l_shape.mass (0)
			assert("mass Valid: center.x", l_mass.center.x ~ 10)
			assert("mass Valid: center.y", l_mass.center.y ~ 10)
			assert("mass Valid: Mass", l_mass.mass ~ 0)
			assert("mass Valid: inertia", l_mass.inertia ~ 0)
		end

	test_axis_aligned_bounding_box_normal_1
			-- Normal test 1 {B2D_SHAPE_CIRCLE}.axis_aligned_bounding_box
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.axis_aligned_bounding_box", "covers/{B2D_SHAPE_CIRCLE}.make_with_coordinates"
		local
			l_shape:B2D_SHAPE_CIRCLE
			l_result:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_transform:B2D_TRANSFORM
		do
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			create l_shape.make_with_coordinates (10, 10, 5)
			l_result := l_shape.axis_aligned_bounding_box (l_transform)
			assert("axis_aligned_bounding_box Valid: lower_bound.x", l_result.lower_bound.x ~ 5)
			assert("axis_aligned_bounding_box Valid: lower_bound.y", l_result.lower_bound.y ~ 5)
			assert("axis_aligned_bounding_box Valid: upper_bound.x", l_result.upper_bound.x ~ 15)
			assert("axis_aligned_bounding_box Valid: upper_bound.y", l_result.upper_bound.y ~ 15)
		end

	test_axis_aligned_bounding_box_normal_2
			-- Normal test 2 {B2D_SHAPE}.axis_aligned_bounding_box
		note
			testing:  "covers/{B2D_SHAPE}.axis_aligned_bounding_box", "covers/{B2D_SHAPE_CIRCLE}.make_with_coordinates"
		local
			l_shape:B2D_SHAPE
			l_result:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_transform:B2D_TRANSFORM
		do
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			create {B2D_SHAPE_CIRCLE}l_shape.make_with_coordinates (10, 10, 5)
			l_result := l_shape.axis_aligned_bounding_box (l_transform, 1)
			assert("axis_aligned_bounding_box Valid: lower_bound.x", l_result.lower_bound.x ~ 5)
			assert("axis_aligned_bounding_box Valid: lower_bound.y", l_result.lower_bound.y ~ 5)
			assert("axis_aligned_bounding_box Valid: upper_bound.x", l_result.upper_bound.x ~ 15)
			assert("axis_aligned_bounding_box Valid: upper_bound.y", l_result.upper_bound.y ~ 15)
		end

	test_axis_aligned_bounding_box_wrong_1
			-- Wrong test 1 {B2D_SHAPE_CIRCLE}.axis_aligned_bounding_box
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.axis_aligned_bounding_box", "covers/{B2D_SHAPE_CIRCLE}.make_with_coordinates"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE
			l_result:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_transform:B2D_TRANSFORM
		do
			if not l_retry then
				create l_transform.make_with_coordinates_and_angle (0, 0, 0)
				create {B2D_SHAPE_CIRCLE}l_shape.make_with_coordinates (10, 10, 5)
				l_result := l_shape.axis_aligned_bounding_box (l_transform, 2)
				assert("axis_aligned_bounding_box Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_axis_aligned_bounding_box_wrong_2
			-- Wrong test 2 {B2D_SHAPE_CIRCLE}.axis_aligned_bounding_box
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.axis_aligned_bounding_box", "covers/{B2D_SHAPE_CIRCLE}.make_with_coordinates"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE
			l_result:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_transform:B2D_TRANSFORM
		do
			if not l_retry then
				create l_transform.make_with_coordinates_and_angle (0, 0, 0)
				create {B2D_SHAPE_CIRCLE}l_shape.make_with_coordinates (10, 10, 5)
				l_result := l_shape.axis_aligned_bounding_box (l_transform, 0)
				assert("axis_aligned_bounding_box Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_is_valid_normal
			-- Normal test {B2D_SHAPE_CIRCLE}.is_valid
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.is_valid"
		local
			l_shape:B2D_SHAPE_CIRCLE
		do
			create l_shape
			assert("is_valid not valid.", l_shape.is_valid)
		end

	test_position_wrong
			-- Wrong test 1 {B2D_SHAPE_CIRCLE}.position
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.position"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CIRCLE
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_shape.own_from_item(create {POINTER})
				l_result := l_shape.position
				assert("position Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_radius_wrong
			-- Wrong test 1 {B2D_SHAPE_CIRCLE}.radius
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.radius"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CIRCLE
			l_result:REAL_32
		do
			if not l_retry then
				create l_shape.own_from_item(create {POINTER})
				l_result := l_shape.radius
				assert("radius Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_radius_wrong
			-- Wrong test 1 {B2D_SHAPE_CIRCLE}.set_radius
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.set_radius"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CIRCLE
		do
			if not l_retry then
				create l_shape.own_from_item(create {POINTER})
				l_shape.set_radius(10)
				assert("set_radius Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_axis_aligned_bounding_box_wrong_3
			-- Wrong test 3 {B2D_SHAPE_CIRCLE}.axis_aligned_bounding_box
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.axis_aligned_bounding_box"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CIRCLE
			l_transform: B2D_TRANSFORM
			l_result:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			if not l_retry then
				create l_shape.own_from_item(create {POINTER})
				create l_transform
				l_result := l_shape.axis_aligned_bounding_box(l_transform)
				assert("axis_aligned_bounding_box Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_axis_aligned_bounding_box_wrong_4
			-- Wrong test 4 {B2D_SHAPE_CIRCLE}.axis_aligned_bounding_box
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.axis_aligned_bounding_box", "covers/{B2D_SHAPE_CIRCLE}.make_with_coordinates"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE
			l_result:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_transform:B2D_TRANSFORM
		do
			if not l_retry then
				create l_transform.make_with_coordinates_and_angle (0, 0, 0)
				create {B2D_SHAPE_CIRCLE}l_shape.own_from_item(create {POINTER})
				l_result := l_shape.axis_aligned_bounding_box (l_transform, 1)
				assert("axis_aligned_bounding_box Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_contains_wrong
			-- Wrong test 1 {B2D_SHAPE_CIRCLE}.contains
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.contains"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CIRCLE
			l_transform: B2D_TRANSFORM
			l_point: B2D_VECTOR_2D
			l_result:BOOLEAN
		do
			if not l_retry then
				create l_shape.own_from_item(create {POINTER})
				create l_transform
				create l_point
				l_result := l_shape.contains_with_vector(l_transform, l_point)
				assert("contains Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_is_valid_wrong
			-- Wrong test 1 {B2D_SHAPE_CIRCLE}.is_valid
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.is_valid"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CIRCLE
			l_result:BOOLEAN
		do
			if not l_retry then
				create l_shape.own_from_item(create {POINTER})
				l_result := l_shape.is_valid
				assert("is_valid Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_mass_wrong
			-- Wrong test 1 {B2D_SHAPE_CIRCLE}.mass
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.mass"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CIRCLE
			l_result:B2D_MASS
		do
			if not l_retry then
				create l_shape.own_from_item(create {POINTER})
				l_result := l_shape.mass(10)
				assert("mass Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_ray_cast_wrong_3
			-- Wrong test 3 {B2D_SHAPE_CIRCLE}.ray_cast
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.ray_cast", "covers/{B2D_SHAPE_CIRCLE}.make_with_coordinates"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CIRCLE
			l_input:B2D_RAY_CAST_INPUT
			l_transform:B2D_TRANSFORM
			l_output: detachable B2D_RAY_CAST_OUTPUT
		do
			if not l_retry then
				create l_transform.make_with_coordinates_and_angle (0, 0, 0)
				create l_shape.own_from_item(create {POINTER})
				create l_input.make_with_coordinates (150, 150, 160, 160, 12)
				l_output := l_shape.ray_cast (l_input, l_transform)
				assert("ray_cast Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_ray_cast_wrong_4
			-- Wrong test 4 {B2D_SHAPE_CIRCLE}.ray_cast
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.ray_cast", "covers/{B2D_SHAPE_CIRCLE}.make_with_coordinates"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE
			l_input:B2D_RAY_CAST_INPUT
			l_transform:B2D_TRANSFORM
			l_output: detachable B2D_RAY_CAST_OUTPUT
		do
			if not l_retry then
				create l_transform.make_with_coordinates_and_angle (0, 0, 0)
				create {B2D_SHAPE_CIRCLE}l_shape.own_from_item(create {POINTER})
				create l_input.make_with_coordinates (150, 150, 160, 160, 12)
				l_output := l_shape.ray_cast (l_input, l_transform, 1)
				assert("ray_cast Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_position_with_vector_normal
			-- Normal test for set_position_with_vector
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.set_position_with_vector",
					  "covers/{B2D_SHAPE_CIRCLE}.position",
					  "covers/{B2D_SHAPE_CIRCLE}.default_create"
		local
			l_shape:B2D_SHAPE_CIRCLE
			l_vector:B2D_VECTOR_2D
		do
			create l_shape
			create l_vector.make_with_coordinates (7.90835, -0.03097)
			l_shape.set_position_with_vector (l_vector)
			assert ("set_position_with_vector valid", l_shape.position ~ l_vector)
		end

	test_set_position_with_vector_wrong_1
			-- Wrong test 1 for set_position_with_vector
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.set_position_with_vector",
					  "covers/{B2D_SHAPE_CIRCLE}.position",
					  "covers/{B2D_SHAPE_CIRCLE}.default_create"
		local
			l_shape:B2D_SHAPE_CIRCLE
			l_vector:B2D_VECTOR_2D
			l_retry:BOOLEAN
		do
			if not l_retry then
				create l_shape
				create l_vector.make_with_coordinates (7.90835, -0.03097)
				l_shape.put_null
				l_shape.set_position_with_vector (l_vector)
				assert ("make_with_coordinates valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_position_with_vector_wrong_2
			-- Wrong test 2 for set_position_with_vector
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.set_position_with_vector",
					  "covers/{B2D_SHAPE_CIRCLE}.position",
					  "covers/{B2D_SHAPE_CIRCLE}.default_create"
		local
			l_shape:B2D_SHAPE_CIRCLE
			l_vector:B2D_VECTOR_2D
			l_retry:BOOLEAN
		do
			if not l_retry then
				create l_shape
				create l_vector.make_with_coordinates (7.90835, -0.03097)
				l_vector.put_null
				l_shape.set_position_with_vector (l_vector)
				assert ("make_with_coordinates valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_position_normal
			-- Normal test for set_position_with_vector
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.set_position",
					  "covers/{B2D_SHAPE_CIRCLE}.position",
					  "covers/{B2D_SHAPE_CIRCLE}.default_create"
		local
			l_shape:B2D_SHAPE_CIRCLE
		do
			create l_shape
			l_shape.set_position (7.90835, -0.03097)
			assert ("make_with_coordinates valid X", l_shape.position.x ~ 7.90835)
			assert ("make_with_coordinates valid Y", l_shape.position.y ~ -0.03097)
		end

	test_set_position_wrong_1
			-- Wrong test 1 for set_position
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.set_position",
					  "covers/{B2D_SHAPE_CIRCLE}.position",
					  "covers/{B2D_SHAPE_CIRCLE}.default_create"
		local
			l_shape:B2D_SHAPE_CIRCLE
			l_retry:BOOLEAN
		do
			if not l_retry then
				create l_shape
				l_shape.put_null
				l_shape.set_position (0, 0)
				assert ("make_with_coordinates valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_position_wrong_1
			-- Wrong test 1 for position
		note
			testing:  "covers/{B2D_SHAPE_CIRCLE}.position",
					  "covers/{B2D_SHAPE_CIRCLE}.default_create"
		local
			l_shape:B2D_SHAPE_CIRCLE
			l_retry:BOOLEAN
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_shape
				l_shape.put_null
				l_result := l_shape.position
				assert ("position valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

end


