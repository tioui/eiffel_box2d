note
	description: "Tests for {B2D_JOINT_WHEEL_DEFINITION}."
	author: "Parick Boucher"
	date: "Mon, 27 Jul 2020 01:15:39 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_JOINT_WHEEL_DEFINITION_TESTS

inherit
	EQA_TEST_SET
		redefine
			on_prepare
		end
	B2D_ANY
		undefine
			default_create
		end
	B2D_COMPARE_REAL
		undefine
			default_create
		end

feature {NONE} -- Events

	on_prepare
			-- <Precursor>
		local
			l_body_definition:B2D_BODY_DEFINITION
		do
			create world.make (0, -9.8)
			create l_body_definition
			l_body_definition.set_position (-10, -10)
			world.create_body (l_body_definition)
			body_1 := world.last_body
			l_body_definition.set_position (10, 10)
			world.create_body (l_body_definition)
			body_2 := world.last_body
		end

	world:B2D_WORLD
			-- The world in which the tests will be performed.

	body_1:detachable B2D_BODY
			-- The first body of each {B2D_JOINT_WHEEL_DEFINITION} during the tests.

	body_2:detachable B2D_BODY
			-- The second body of each {B2D_JOINT_WHEEL_DEFINITION} during the tests.

feature -- Normal test routines

	test_make_normal
			-- Normal test for `make'.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.make",
					  "covers/{B2D_JOINT_WHEEL_DEFINITION}.make_with_vector"
		local
			l_definition:B2D_JOINT_WHEEL_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, -10, -10, 0, 1)
				assert ("body_1 is assigned", l_definition.body_1 ~ la_body_1)
				assert ("body_2 is assigned", l_definition.body_2 ~ la_body_2)
				assert ("local_anchor_1 valid x", l_definition.local_anchor_1.x ~ 0)
				assert ("local_anchor_1 valid y", l_definition.local_anchor_1.y ~ 0)
				assert ("local_anchor_2 valid x", l_definition.local_anchor_2.x ~ -20)
				assert ("local_anchor_2 valid y", l_definition.local_anchor_2.y ~ -20)
				assert ("local_axis valid x", l_definition.local_axis.x ~ 0)
				assert ("local_axis valid y", l_definition.local_axis.y ~ 1)
			end
		end

	test_make_with_vector_normal
			-- Normal test for `make_with_vector'.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.make_with_vector"
		local
			l_definition:B2D_JOINT_WHEEL_DEFINITION
			l_anchor, l_axis:B2D_VECTOR_2D
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_anchor.make_with_coordinates (-10, -10)
				create l_axis.make_with_coordinates (0, 1)
				create l_definition.make_with_vector (la_body_1, la_body_2, l_anchor, l_axis)
				assert ("body_1 is assigned", l_definition.body_1 ~ la_body_1)
				assert ("body_2 is assigned", l_definition.body_2 ~ la_body_2)
				assert ("local_anchor_1 valid x", l_definition.local_anchor_1.x ~ 0)
				assert ("local_anchor_1 valid y", l_definition.local_anchor_1.y ~ 0)
				assert ("local_anchor_2 valid x", l_definition.local_anchor_2.x ~ -20)
				assert ("local_anchor_2 valid y", l_definition.local_anchor_2.y ~ -20)
				assert ("local_axis valid x", l_definition.local_axis.x ~ 0)
				assert ("local_axis valid y", l_definition.local_axis.y ~ 1)
			end
		end

	test_make_with_vector_and_angle_normal
			-- Normal test for `make_with_vector_and_angle'.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.make_with_vector_and_angle",
					  "covers/{B2D_JOINT_WHEEL_DEFINITION}.make_with_vector"
		local
			l_definition:B2D_JOINT_WHEEL_DEFINITION
			l_anchor:B2D_VECTOR_2D
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_anchor.make_with_coordinates (-10, -10)
				create l_definition.make_with_vector_and_angle (la_body_1, la_body_2, l_anchor, {MATH_CONST}.pi_2.truncated_to_real)
				assert ("body_1 is assigned", l_definition.body_1 ~ la_body_1)
				assert ("body_2 is assigned", l_definition.body_2 ~ la_body_2)
				assert ("local_anchor_1 valid x", l_definition.local_anchor_1.x ~ 0)
				assert ("local_anchor_1 valid y", l_definition.local_anchor_1.y ~ 0)
				assert ("local_anchor_2 valid x", l_definition.local_anchor_2.x ~ -20)
				assert ("local_anchor_2 valid y", l_definition.local_anchor_2.y ~ -20)
				assert ("local_axis valid x", compare_real_32 (l_definition.local_axis.x, 0))
				assert ("local_axis valid y", compare_real_32 (l_definition.local_axis.x, 0))
			end
		end

	test_make_with_angle_normal
			-- Normal test for `make_with_angle'.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.make_with_vector_and_angle",
					  "covers/{B2D_JOINT_WHEEL_DEFINITION}.make_with_angle"
		local
			l_definition:B2D_JOINT_WHEEL_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make_with_angle (la_body_1, la_body_2, -10, -10, {MATH_CONST}.pi_2.truncated_to_real)
				assert ("body_1 is assigned", l_definition.body_1 ~ la_body_1)
				assert ("body_2 is assigned", l_definition.body_2 ~ la_body_2)
				assert ("local_anchor_1 valid x", l_definition.local_anchor_1.x ~ 0)
				assert ("local_anchor_1 valid y", l_definition.local_anchor_1.y ~ 0)
				assert ("local_anchor_2 valid x", l_definition.local_anchor_2.x ~ -20)
				assert ("local_anchor_2 valid y", l_definition.local_anchor_2.y ~ -20)
				assert ("local_axis valid x", compare_real_32 (l_definition.local_axis.x, 0))
				assert ("local_axis valid y", compare_real_32 (l_definition.local_axis.x, 0))
			end
		end

	test_set_anchor_with_vector_normal
			-- Normal test for `set_anchor_with_vector'.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_anchor_with_vector",
					  "covers/{B2D_JOINT_WHEEL_DEFINITION}.local_anchor_1",
					  "covers/{B2D_JOINT_WHEEL_DEFINITION}.local_anchor_2",
					  "covers/{B2D_JOINT_WHEEL_DEFINITION}.anchor"
		local
			l_definition:B2D_JOINT_WHEEL_DEFINITION
			l_anchor:B2D_VECTOR_2D
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
				create l_anchor.make_with_coordinates (-5, -10)
				l_definition.set_anchor_with_vector (l_anchor)
				assert ("anchor valid x", l_definition.anchor.x ~ -5)
				assert ("anchor valid y", l_definition.anchor.y ~ -10)
				assert ("local_anchor_1 valid x", l_definition.local_anchor_1.x ~ 5)
				assert ("local_anchor_1 valid y", l_definition.local_anchor_1.y ~ 0)
				assert ("local_anchor_2 valid x", l_definition.local_anchor_2.x ~ -15)
				assert ("local_anchor_2 valid y", l_definition.local_anchor_2.y ~ -20)
			end
		end

	test_set_local_axis_with_vector_normal
			-- Normal test for `set_local_axis_with_vector'.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_local_axis_with_vector",
					  "covers/{B2D_JOINT_WHEEL_DEFINITION}.local_axis"
		local
			l_definition:B2D_JOINT_WHEEL_DEFINITION
			l_axis:B2D_VECTOR_2D
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
				create l_axis.make_with_coordinates (1, 0)
				l_definition.set_local_axis_with_vector (l_axis)
				assert ("set_local_axis_with_vector valid x", l_definition.local_axis.x ~ 1)
				assert ("set_local_axis_with_vector valid y", l_definition.local_axis.y ~ 0)
			end
		end

	test_is_motor_enabled_normal
			-- Normal test for `is_motor_enabled'.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.is_motor_enabled",
					  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_is_motor_enabled"
		local
			l_definition:B2D_JOINT_WHEEL_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
				assert ("default value", not l_definition.is_motor_enabled)
				l_definition.set_is_motor_enabled (True)
				assert ("set_is_motor_enabled valid True", l_definition.is_motor_enabled)
				l_definition.set_is_motor_enabled (False)
				assert ("set_is_motor_enabled valid False", not l_definition.is_motor_enabled)
			end
		end

	test_motor_speed_normal
			-- Normal test for `motor_speed'.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.motor_speed",
					  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_motor_speed"
		local
			l_definition:B2D_JOINT_WHEEL_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
				l_definition.set_motor_speed (25.5)
				assert ("set_motor_speed valid", l_definition.motor_speed ~ 25.5)
			end
		end

	test_max_motor_torque_normal
			-- Normal test for `maximum_motor_torque'.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.max_motor_torque",
					  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_max_motor_torque"
		local
			l_definition:B2D_JOINT_WHEEL_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
				l_definition.set_maximum_motor_torque (48.123)
				assert ("set_max_motor_torque valid", l_definition.maximum_motor_torque ~ 48.123)
			end
		end

	test_frequency_normal
			-- Normal test for `frequency'.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.frequency",
					  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_frequency"
		local
			l_definition:B2D_JOINT_WHEEL_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
				l_definition.set_frequency (25.5)
				assert ("set_frequency valid", l_definition.frequency ~ 25.5)
			end
		end

	test_damping_ratio_normal
			-- Normal test for `damping_ratio'.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.damping_ratio",
					  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_damping_ratio"
		local
			l_definition:B2D_JOINT_WHEEL_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
				l_definition.set_damping_ratio (0.5)
				assert ("set_damping_ratio valid", l_definition.damping_ratio ~ 0.5)
			end
		end

feature -- Limit test routines

	test_set_motor_speed_limit
			-- Limit test for `set_motor_speed'.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_motor_speed",
					  "covers/{B2D_JOINT_WHEEL_DEFINITION}.motor_speed"
		local
			l_definition:B2D_JOINT_WHEEL_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
				l_definition.set_motor_speed (0)
				assert ("set_motor_speed valid", l_definition.motor_speed ~ 0)
			end
		end

	test_set_max_motor_torque_limit
			-- Limit test for `set_maximum_motor_torque'.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_max_motor_torque",
					  "covers/{B2D_JOINT_WHEEL_DEFINITION}.max_motor_torque"
		local
			l_definition:B2D_JOINT_WHEEL_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
				l_definition.set_maximum_motor_torque (0)
				assert ("set_max_motor_torque valid", l_definition.maximum_motor_torque ~ 0)
			end
		end

	test_set_frequency_limit_1
			-- Limit test for `set_frequency'.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_frequency",
					  "covers/{B2D_JOINT_WHEEL_DEFINITION}.frequency"
		local
			l_definition:B2D_JOINT_WHEEL_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
				l_definition.set_frequency (0)
				assert ("set_frequency valid", l_definition.frequency ~ 0)
			end
		end

	test_set_frequency_limit_2
			-- Limit test for `set_frequency'.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_frequency",
					  "covers/{B2D_JOINT_WHEEL_DEFINITION}.frequency"
		local
			l_definition:B2D_JOINT_WHEEL_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
				l_definition.set_frequency (30)
				assert ("set_frequency valid", l_definition.frequency = 30)
			end
		end

	test_set_damping_ratio_limit
			-- Limit test for `set_damping_ratio'.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_damping_ratio",
					  "covers/{B2D_JOINT_WHEEL_DEFINITION}.damping_ratio"
		local
			l_definition:B2D_JOINT_WHEEL_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
				l_definition.set_damping_ratio (0)
				assert ("set_damping_ratio valid", l_definition.damping_ratio ~ 0)
			end
		end

feature -- Wrong test routines

	test_make_wrong_1
			-- Test for `make' when arguments do not exist.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.make"
		local
			l_retry:INTEGER_32
			l_definition:B2D_JOINT_WHEEL_DEFINITION
		do
			if l_retry ~ 0 then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					la_body_1.put_null
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
					assert ("make valid", False)
				end
			elseif l_retry ~ 1 then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					la_body_2.put_null
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
					assert ("make valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := l_retry + 1
				retry
			end
		end

	test_make_wrong_2
			-- Test for `make' when `a_axis_x' and `a_axis_y' are both 0.0.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.make"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WHEEL_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 0)
					assert ("make valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_make_with_vector_wrong_1
			-- Test for `make_with_vector' when arguments do not exist.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.make_with_vector"
		local
			l_retry:INTEGER_32
			l_definition:B2D_JOINT_WHEEL_DEFINITION
			l_anchor, l_axis:B2D_VECTOR_2D
		do
			if l_retry ~ 0 then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					la_body_1.put_null
					create l_anchor
					create l_axis.make_with_coordinates (0, 1)
					create l_definition.make_with_vector (la_body_1, la_body_2, l_anchor, l_axis)
					assert ("make_with_vector valid", False)
				end
			elseif l_retry ~ 1 then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					la_body_2.put_null
					create l_anchor
					create l_axis.make_with_coordinates (0, 1)
					create l_definition.make_with_vector (la_body_1, la_body_2, l_anchor, l_axis)
					assert ("make_with_vector valid", False)
				end
			elseif l_retry ~ 2 then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_anchor
					l_anchor.put_null
					create l_axis.make_with_coordinates (0, 1)
					create l_definition.make_with_vector (la_body_1, la_body_2, l_anchor, l_axis)
					assert ("make_with_vector valid", False)
				end
			elseif l_retry ~ 3 then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_anchor
					create l_axis.make_with_coordinates (0, 1)
					l_axis.put_null
					create l_definition.make_with_vector (la_body_1, la_body_2, l_anchor, l_axis)
					assert ("make_with_vector valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := l_retry + 1
				retry
			end
		end

	test_make_with_vector_wrong_2
			-- Test for `make_with_vector' when `a_axis' does not point in any direction.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.make_with_vector"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WHEEL_DEFINITION
			l_anchor, l_axis:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_anchor
					create l_axis.make_with_coordinates (0, 0)
					create l_definition.make_with_vector (la_body_1, la_body_2, l_anchor, l_axis)
					assert ("make_with_vector valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_make_with_vector_and_angle_wrong
			-- Test for `make_with_vector_and_angle' when arguments do not exist.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.make_with_vector_and_angle"
		local
			l_retry:INTEGER_32
			l_definition:B2D_JOINT_WHEEL_DEFINITION
			l_anchor:B2D_VECTOR_2D
		do
			if l_retry ~ 0 then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					la_body_1.put_null
					create l_anchor
					create l_definition.make_with_vector_and_angle (la_body_1, la_body_2, l_anchor, 0)
					assert ("make_with_vector_and_angle valid", False)
				end
			elseif l_retry ~ 1 then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					la_body_2.put_null
					create l_anchor
					create l_definition.make_with_vector_and_angle (la_body_1, la_body_2, l_anchor, 0)
					assert ("make_with_vector_and_angle valid", False)
				end
			elseif l_retry ~ 2 then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_anchor
					l_anchor.put_null
					create l_definition.make_with_vector_and_angle (la_body_1, la_body_2, l_anchor, 0)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := l_retry + 1
				retry
			end
		end

	test_make_with_angle_wrong
			-- Test for `make_with_angle' when arguments do not exist.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.make_with_angle"
		local
			l_retry:INTEGER_32
			l_definition:B2D_JOINT_WHEEL_DEFINITION
		do
			if l_retry ~ 0 then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					la_body_1.put_null
					create l_definition.make_with_angle (la_body_1, la_body_2, 0, 0, 0)
					assert ("make_with_angle valid", False)
				end
			elseif l_retry ~ 1 then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					la_body_2.put_null
					create l_definition.make_with_angle (la_body_1, la_body_2, 0, 0, 0)
					assert ("make_with_angle valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := l_retry + 1
				retry
			end
		end

	test_local_anchor_1_wrong
			-- Test for `local_anchor_1' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.local_anchor_1"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WHEEL_DEFINITION
			l_discard:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
					l_definition.put_null
					l_discard := l_definition.local_anchor_1
					assert ("local_anchor_1 valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_local_anchor_2_wrong
			-- Test for `local_anchor_2' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.local_anchor_2"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WHEEL_DEFINITION
			l_discard:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
					l_definition.put_null
					l_discard := l_definition.local_anchor_2
					assert ("local_anchor_2 valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_anchor_with_vector_wrong_1
			-- Test for `set_anchor_with_vector' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_anchor_with_vector"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WHEEL_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
					l_definition.put_null
					l_definition.set_anchor_with_vector (create {B2D_VECTOR_2D})
					assert ("set_anchor_with_vector valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_anchor_with_vector_wrong_2
			-- Test for `set_anchor_with_vector' when `a_world_point' does not exist.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_anchor_with_vector"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WHEEL_DEFINITION
			l_anchor:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
					create l_anchor
					l_anchor.put_null
					l_definition.set_anchor_with_vector (l_anchor)
					assert ("set_anchor_with_vector valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_local_axis_wrong
			-- Test for `local_axis' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.local_axis"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WHEEL_DEFINITION
			l_discard:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
					l_definition.put_null
					l_discard := l_definition.local_axis
					assert ("local_axis valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_local_axis_with_vector_wrong_1
			-- Test for `set_local_axis_with_vector' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_local_axis_with_vector"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WHEEL_DEFINITION
			l_axis:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
					l_definition.put_null
					create l_axis.make_with_coordinates (0, 1)
					l_definition.set_local_axis_with_vector (l_axis)
					assert ("set_local_axis_with_vector valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_local_axis_with_vector_wrong_2
			-- Test for `set_local_axis_with_vector' when `a_world_axis' does not exist.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_local_axis_with_vector"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WHEEL_DEFINITION
			l_axis:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
					create l_axis.make_with_coordinates (0, 1)
					l_axis.put_null
					l_definition.set_local_axis_with_vector (l_axis)
					assert ("set_local_axis_with_vector valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_local_axis_with_vector_wrong_3
			-- Test for `set_local_axis_with_vector' when `a_world_axis' does not point in any direction.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_local_axis_with_vector"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WHEEL_DEFINITION
			l_axis:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
					create l_axis.make_with_coordinates (0, 0)
					l_definition.set_local_axis_with_vector (l_axis)
					assert ("set_local_axis_with_vector valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_is_motor_enabled_wrong
			-- Test for `is_motor_enabled' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.is_motor_enabled"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WHEEL_DEFINITION
			l_discard:BOOLEAN
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
					l_definition.put_null
					l_discard := l_definition.is_motor_enabled
					assert ("is_motor_enabled valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_is_motor_enabled_wrong
			-- Test for `set_is_motor_enabled' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_is_motor_enabled"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WHEEL_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
					l_definition.put_null
					l_definition.set_is_motor_enabled (True)
					assert ("set_is_motor_enabled valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_motor_speed_wrong
			-- Test for `motor_speed' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.motor_speed"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WHEEL_DEFINITION
			l_discard:REAL_32
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
					l_definition.put_null
					l_discard := l_definition.motor_speed
					assert ("motor_speed valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_motor_speed_wrong_1
			-- Test for `set_motor_speed' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_motor_speed"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WHEEL_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
					l_definition.put_null
					l_definition.set_motor_speed (25.5)
					assert ("set_motor_speed valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_motor_speed_wrong_2
			-- Test for `set_motor_speed' when `a_value' is negative.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_motor_speed"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WHEEL_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
					l_definition.put_null
					l_definition.set_motor_speed (-0.01)
					assert ("set_motor_speed valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_max_motor_torque_wrong
			-- Test for `maximum_motor_torque' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}."
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WHEEL_DEFINITION
			l_discard:REAL_32
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
					l_definition.put_null
					l_discard := l_definition.maximum_motor_torque
					assert ("max_motor_torque valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_max_motor_torque_wrong_1
			-- Test for `set_maximum_motor_torque' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_max_motor_torque"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WHEEL_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
					l_definition.put_null
					l_definition.set_maximum_motor_torque (48.123)
					assert ("set_max_motor_torque valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_max_motor_torque_wrong_2
			-- Test for `set_maximum_motor_torque' when `a_value' is negative.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_max_motor_torque"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WHEEL_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
					l_definition.set_maximum_motor_torque (-0.01)
					assert ("set_max_motor_torque valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_frequency_wrong
			-- Test for `frequency' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.frequency"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WHEEL_DEFINITION
			l_discard:REAL_32
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
					l_definition.put_null
					l_discard := l_definition.frequency
					assert ("frequency valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_frequency_wrong_1
			-- Test for `set_frequency' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_frequency"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WHEEL_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
					l_definition.put_null
					l_definition.set_frequency (25.5)
					assert ("set_frequency valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_frequency_wrong_2
			-- Test for `set_frequency' when `a_value' is negative.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_frequency"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WHEEL_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
					l_definition.set_frequency (-0.001)
					assert ("set_frequency valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_frequency_wrong_3
			-- Test for `set_frequency' when `a_value' is higher than half de frame rate.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_frequency"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WHEEL_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
					l_definition.set_frequency (30.5)
					assert ("set_frequency valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_damping_ratio_wrong
			-- Test for `damping_ratio' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.damping_ratio"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WHEEL_DEFINITION
			l_discard:REAL_32
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
					l_definition.put_null
					l_discard := l_definition.damping_ratio
					assert ("damping_ratio valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_damping_ratio_wrong_1
			-- Test for `set_damping_ratio' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_damping_ratio"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WHEEL_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
					l_definition.put_null
					l_definition.set_damping_ratio (0.5)
					assert ("set_damping_ratio valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_damping_ratio_wrong_2
			-- Test for `set_damping_ratio' when `a_value' is negative.
		note
			testing:  "covers/{B2D_JOINT_WHEEL_DEFINITION}.set_damping_ratio"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_WHEEL_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 1)
					l_definition.set_damping_ratio (-0.001)
					assert ("set_damping_ratio valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

end


