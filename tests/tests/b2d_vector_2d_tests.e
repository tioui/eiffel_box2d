note
	description: "Tests for {B2D_VECTOR_2D_TESTS}"
	author: "Louis Marchand"
	date: "Tue, 09 Jun 2020 19:59:46 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_VECTOR_2D_TESTS

inherit
	EQA_TEST_SET
	B2D_ANY
		undefine
			default_create
		end
	B2D_COMPARE_REAL
		undefine
			default_create
		end

feature -- Test routines

	test_make_from_coordinates_normal
			-- Normal test for {B2D_VECTOR_2D}.make_from_coordinates
		note
			testing:  "covers/{B2D_VECTOR_2D}.make_from_coordinates", "covers/{B2D_VECTOR_2D}.x", "covers/{B2D_VECTOR_2D}.y"
		local
			l_vector:B2D_VECTOR_2D
		do
			create l_vector.make_with_coordinates (10.0, 20.0)
			assert ("X value not valid.", l_vector.x ~ 10.0)
			assert ("Y value not valid.", l_vector.y ~ 20.0)
		end

	test_make_from_item_normal
			-- Normal test for {B2D_VECTOR_2D}.make_from_item
		note
			testing:  "covers/{B2D_VECTOR_2D}.make_from_item", "covers/{B2D_VECTOR_2D}.x", "covers/{B2D_VECTOR_2D}.y"
		local
			l_vector_1, l_vector_2:B2D_VECTOR_2D
		do
			create l_vector_1.make_with_coordinates (10.0, 20.0)
			create l_vector_2.make_from_item (l_vector_1.item)
			assert ("make_from_item valid. item", l_vector_1.item /~ l_vector_2.item)
			assert ("make_from_item valid. object", l_vector_1 ~ l_vector_2)
		end

	test_default_create_normal
			-- Normal test for {B2D_VECTOR_2D}.make_from_coordinates
		note
			testing:  "covers/{B2D_VECTOR_2D}.default_create", "covers/{B2D_VECTOR_2D}.x", "covers/{B2D_VECTOR_2D}.y"
		local
			l_vector:B2D_VECTOR_2D
		do
			create l_vector
			assert ("X value not valid.", l_vector.x ~ 0.0)
			assert ("Y value not valid.", l_vector.y ~ 0.0)
		end

	test_set_x_normal
			-- Normal test for {B2D_VECTOR_2D}.set_x
		note
			testing:  "covers/{B2D_VECTOR_2D}.set_x", "covers/{B2D_VECTOR_2D}.x", "covers/{B2D_VECTOR_2D}.y"
		local
			l_vector:B2D_VECTOR_2D
		do
			create l_vector
			l_vector.set_x (30.123)
			assert ("X value not valid.", l_vector.x ~ 30.123)
			assert ("Y value not valid.", l_vector.y ~ 0.0)
		end

	test_set_y_normal
			-- Normal test for {B2D_VECTOR_2D}.set_y
		note
			testing:  "covers/{B2D_VECTOR_2D}.set_y", "covers/{B2D_VECTOR_2D}.x", "covers/{B2D_VECTOR_2D}.y"
		local
			l_vector:B2D_VECTOR_2D
		do
			create l_vector
			l_vector.set_y (-0.456)
			assert ("X value not valid.", l_vector.x ~ 0.0)
			assert ("Y value not valid.", l_vector.y ~ -0.456)
		end

	test_set_zero_normal
			-- Normal test for {B2D_VECTOR_2D}.set_zero
		note
			testing:  "covers/{B2D_VECTOR_2D}.set_zero", "covers/{B2D_VECTOR_2D}.x", "covers/{B2D_VECTOR_2D}.y"
		local
			l_vector:B2D_VECTOR_2D
		do
			create l_vector.make_with_coordinates (10.0, 20.0)
			l_vector.set_zero
			assert ("X value not valid.", l_vector.x ~ 0.0)
			assert ("Y value not valid.", l_vector.y ~ 0.0)
		end

	test_set_coordinates_normal
			-- Normal test for {B2D_VECTOR_2D}.set_coordinates
		note
			testing:  "covers/{B2D_VECTOR_2D}.set_coordinates", "covers/{B2D_VECTOR_2D}.x", "covers/{B2D_VECTOR_2D}.y"
		local
			l_vector:B2D_VECTOR_2D
		do
			create l_vector.make_with_coordinates (10.0, 20.0)
			l_vector.set_coordinates (38738.14709, -11246.87843)
			assert ("X value not valid.", l_vector.x ~ 38738.14709)
			assert ("Y value not valid.", l_vector.y ~ -11246.87843)
		end

	test_magnitude_normal
			-- Normal test for {B2D_VECTOR_2D}.magnitude
		note
			testing:  "covers/{B2D_VECTOR_2D}.magnitude"
		local
			l_vector:B2D_VECTOR_2D
		do
			create l_vector.make_with_coordinates (3, 4)
			assert ("magnitude valid", l_vector.magnitude ~ 5)
			l_vector.set_coordinates (0, -5)
			assert ("magnitude valid", l_vector.magnitude ~ 5)
			l_vector.set_coordinates (0, 0)
			assert ("magnitude valid", l_vector.magnitude ~ 0)
		end

	test_normalize_normal
			-- Normal test for {B2D_VECTOR_2D}.normalize
		note
			testing:  "covers/{B2D_VECTOR_2D}.normalize", "covers/{B2D_VECTOR_2D}.magnitude"
		local
			l_vector:B2D_VECTOR_2D
		do
			create l_vector.make_with_coordinates (-1.81394, 5.19563)
			l_vector.normalize
			assert ("normalize valid", l_vector.magnitude ~ 1.0)
			l_vector.set_coordinates (0, 0)
			assert ("normalize valid at (0,0)", l_vector.magnitude ~ 0.0)
		end

	test_normalized_normal
			-- Normal test for {B2D_VECTOR_2D}.normalized
		note
			testing:
				"covers/{B2D_VECTOR_2D}.normalized",
				"covers/{B2D_VECTOR_2D}.normalize",
				"covers/{B2D_VECTOR_2D}.magnitude"
		local
			l_vector, l_normalized:B2D_VECTOR_2D
		do
			create l_vector.make_with_coordinates (0.333, 0.45812)
			l_normalized := l_vector.normalized
			assert ("normalized valid", l_normalized.magnitude ~ 1.0)
			assert ("normalized returns copy", l_normalized /= l_vector)
			assert ("point in same direction",
				compare_real_32 (l_normalized.x * l_vector.magnitude, l_vector.x) and
				compare_real_32 (l_normalized.y * l_vector.magnitude, l_vector.y))
			l_normalized.set_coordinates (0, 0)
			assert ("normalized valid at (0,0)", l_normalized.magnitude ~ 0.0)
		end

	test_distance_normal
			-- Normal test for {B2D_VECTOR_2D}.distance
		note
			testing:
				"covers/{B2D_VECTOR_2D}.distance"
		local
			l_vector_1, l_vector_2:B2D_VECTOR_2D
			l_distance:REAL_32
		do
			create l_vector_1.make_with_coordinates (0.2, 5.3)
			create l_vector_2.make_with_coordinates (2.25, -1.03)
			l_distance := l_vector_1.distance (l_vector_2)
			assert ("distance valid", compare_real_32 (l_distance, 6.65367))
			l_vector_1.set_zero
			l_vector_2.set_zero
			l_distance := l_vector_1.distance (l_vector_2)
			assert ("distance valid", l_distance ~ 0)
		end

	test_is_equal_normal_1
			-- Normal test 1 for {B2D_VECTOR_2D}.is_equal
		note
			testing:  "covers/{B2D_VECTOR_2D}.is_equal", "covers/{B2D_VECTOR_2D}.make_with_coordinates"
		local
			l_vector1, l_vector2:B2D_VECTOR_2D
		do
			create l_vector1.make_with_coordinates (10.0, 20.0)
			create l_vector2.make_with_coordinates (10.0, 20.0)
			assert ("is_equal Valid.", l_vector1 ~ l_vector2)
		end

	test_is_equal_normal_2
			-- Normal test 2 for {B2D_VECTOR_2D}.is_equal
		note
			testing:  "covers/{B2D_VECTOR_2D}.is_equal", "covers/{B2D_VECTOR_2D}.make_with_coordinates"
		local
			l_vector1, l_vector2:B2D_VECTOR_2D
		do
			create l_vector1.make_with_coordinates (10.0, 20.0)
			create l_vector2.make_with_coordinates (11.0, 20.0)
			assert ("is_equal Valid.", l_vector1 /~ l_vector2)
		end

	test_is_equal_normal_3
			-- Normal test 3 for {B2D_VECTOR_2D}.is_equal
		note
			testing:  "covers/{B2D_VECTOR_2D}.is_equal", "covers/{B2D_VECTOR_2D}.make_with_coordinates"
		local
			l_vector1, l_vector2:B2D_VECTOR_2D
		do
			create l_vector1.make_with_coordinates (10.0, 20.0)
			create l_vector2.make_with_coordinates (10.0, 21.0)
			assert ("is_equal Valid.", l_vector1 /~ l_vector2)
		end

	test_is_equal_normal_4
			-- Normal test 4 for {B2D_VECTOR_2D}.is_equal
		note
			testing:  "covers/{B2D_VECTOR_2D}.is_equal", "covers/{B2D_VECTOR_2D}.make_with_coordinates"
		local
			l_vector1, l_vector2:B2D_VECTOR_2D
		do
			create l_vector1.own_from_item(create {POINTER})
			create l_vector2.make_with_coordinates (10.0, 21.0)
			assert ("is_equal Valid.", l_vector1 /~ l_vector2)
		end

	test_is_equal_normal_5
			-- Normal test 5 for {B2D_VECTOR_2D}.is_equal
		note
			testing:  "covers/{B2D_VECTOR_2D}.is_equal", "covers/{B2D_VECTOR_2D}.make_with_coordinates"
		local
			l_vector1, l_vector2:B2D_VECTOR_2D
		do
			create l_vector1.make_with_coordinates (10.0, 20.0)
			create l_vector2.own_from_item(create {POINTER})
			assert ("is_equal Valid.", l_vector1 /~ l_vector2)
		end

	test_is_equal_normal_6
			-- Normal test 6 for {B2D_VECTOR_2D}.is_equal
		note
			testing:  "covers/{B2D_VECTOR_2D}.is_equal", "covers/{B2D_VECTOR_2D}.make_with_coordinates"
		local
			l_vector1, l_vector2:B2D_VECTOR_2D
		do
			create l_vector1.own_from_item(create {POINTER})
			create l_vector2.own_from_item(create {POINTER})
			assert ("is_equal Valid.", l_vector1 ~ l_vector2)
		end

	test_out_normal_1
			-- Normal test 1 for {B2D_VECTOR_2D}.out
		note
			testing:  "covers/{B2D_VECTOR_2D}.out", "covers/{B2D_VECTOR_2D}.make_with_coordinates"
		local
			l_vector:B2D_VECTOR_2D
		do
			create l_vector.make_with_coordinates (6.90601, 4.04254)
			assert ("is_equal Valid.", l_vector.out ~ "[6.90601,4.04254]")
		end

	test_out_normal_2
			-- Normal test 2 for {B2D_VECTOR_2D}.out
		note
			testing:  "covers/{B2D_VECTOR_2D}.out", "covers/{B2D_VECTOR_2D}.make_with_coordinates"
		local
			l_vector:B2D_VECTOR_2D
		do
			create l_vector.own_from_item(create {POINTER})
			assert ("is_equal Valid.", l_vector.out ~ "[,]")
		end

	test_x_wrong
			-- Wrong test for x
		note
			testing:  "covers/{B2D_VECTOR_2D}.x"
		local
			l_retry:BOOLEAN
			l_rotation:B2D_VECTOR_2D
			l_result:REAL_32
		do
			if not l_retry then
				create l_rotation.own_from_item(create {POINTER})
				l_result := l_rotation.x
				assert("x Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_y_wrong
			-- Wrong test for y
		note
			testing:  "covers/{B2D_VECTOR_2D}.y"
		local
			l_retry:BOOLEAN
			l_rotation:B2D_VECTOR_2D
			l_result:REAL_32
		do
			if not l_retry then
				create l_rotation.own_from_item(create {POINTER})
				l_result := l_rotation.y
				assert("y Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_x_wrong
			-- Wrong test for set_x
		note
			testing:  "covers/{B2D_VECTOR_2D}.set_x"
		local
			l_retry:BOOLEAN
			l_rotation:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_rotation.own_from_item(create {POINTER})
				l_rotation.set_x(1)
				assert("set_x Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_y_wrong
			-- Wrong test for set_y
		note
			testing:  "covers/{B2D_VECTOR_2D}.set_y"
		local
			l_retry:BOOLEAN
			l_rotation:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_rotation.own_from_item(create {POINTER})
				l_rotation.set_y(1)
				assert("set_y Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_zero_wrong
			-- Wrong test for set_zero
		note
			testing:  "covers/{B2D_VECTOR_2D}.set_zero"
		local
			l_retry:BOOLEAN
			l_rotation:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_rotation.own_from_item(create {POINTER})
				l_rotation.set_zero
				assert("set_zero Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_coordinates_wrong
			-- Wrong test for set_coordinates
		note
			testing:  "covers/{B2D_VECTOR_2D}.set_coordinates"
		local
			l_retry:BOOLEAN
			l_rotation:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_rotation.own_from_item(create {POINTER})
				l_rotation.set_coordinates(1,2)
				assert("set_coordinates Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_magnitude_wrong
			-- Wrong test for magnitude
		note
			testing:  "covers/{B2D_VECTOR_2D}.magnitude"
		local
			l_retry:BOOLEAN
			l_vector:B2D_VECTOR_2D
			l_discard:REAL_32
		do
			if not l_retry then
				create l_vector
				l_vector.put_null
				l_discard := l_vector.magnitude
				assert ("magnitude valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_normalize_wrong
			-- Wrong test for normalize
		note
			testing:  "covers/{B2D_VECTOR_2D}.normalize"
		local
			l_retry:BOOLEAN
			l_vector:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_vector
				l_vector.put_null
				l_vector.normalize
				assert ("normalize valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_normalized_wrong
			-- Wrong test for normalized
		note
			testing:  "covers/{B2D_VECTOR_2D}.normalized"
		local
			l_retry:BOOLEAN
			l_vector:B2D_VECTOR_2D
			l_discard:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_vector
				l_vector.put_null
				l_discard := l_vector.normalized
				assert ("normalized valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_distance_wrong_1
			-- Wrong test for distance when `item' does not exist.
		note
			testing:  "covers/{B2D_VECTOR_2D}.distance"
		local
			l_retry:BOOLEAN
			l_vector:B2D_VECTOR_2D
			l_discard:REAL_32
		do
			if not l_retry then
				create l_vector
				l_vector.put_null
				l_discard := l_vector.distance (create {B2D_VECTOR_2D})
				assert ("distance valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_distance_wrong_2
			-- Wrong test for distance when `a_vector' does not exist.
		note
			testing:  "covers/{B2D_VECTOR_2D}.normalized"
		local
			l_retry:BOOLEAN
			l_vector_1, l_vector_2:B2D_VECTOR_2D
			l_discard:REAL_32
		do
			if not l_retry then
				create l_vector_1
				create l_vector_2
				l_vector_2.put_null
				l_discard := l_vector_1.distance (l_vector_2)
				assert ("distance valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

end


