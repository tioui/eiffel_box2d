note
	description: "Tests for {B2D_RAY_CAST_INPUT}"
	author: "Louis Marchand"
	date: "Fri, 12 Jun 2020 18:05:46 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_RAY_CAST_INPUT_TESTS

inherit
	EQA_TEST_SET
	B2D_ANY
		undefine
			default_create
		end

feature -- Test routines

	test_make
			-- Normal test {B2D_RAY_CAST_INPUT}.make
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.make"
		local
			l_input:B2D_RAY_CAST_INPUT
		do
			create l_input.make
		end

	test_make_with_coordinates
			-- Normal test {B2D_RAY_CAST_INPUT}.make_with_coordinates
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.make_with_coordinates", "covers/{B2D_RAY_CAST_INPUT}.point_1",
						"covers/{B2D_RAY_CAST_INPUT}.point_2", "covers/{B2D_RAY_CAST_INPUT}.maximum_fraction"
		local
			l_input:B2D_RAY_CAST_INPUT
		do
			create l_input.make_with_coordinates (3.36435, -93.03718, 889.08975, 436.70258, 662.37217)
			assert("make_from_coordinates Valid: Point_1.x", l_input.point_1.x ~ 3.36435)
			assert("make_from_coordinates Valid: Point_1.y", l_input.point_1.y ~ -93.03718)
			assert("make_from_coordinates Valid: Point_2.x", l_input.point_2.x ~ 889.08975)
			assert("make_from_coordinates Valid: Point_2.y", l_input.point_2.y ~ 436.70258)
			assert("make_from_coordinates Valid: maximum_fraction", l_input.maximum_fraction ~ 662.37217)
		end

	test_make_with_vectors
			-- Normal test {B2D_RAY_CAST_INPUT}.make_with_vectors
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.make_with_vectors", "covers/{B2D_RAY_CAST_INPUT}.point_1",
						"covers/{B2D_RAY_CAST_INPUT}.point_2", "covers/{B2D_RAY_CAST_INPUT}.maximum_fraction"
		local
			l_input:B2D_RAY_CAST_INPUT
			l_vector_1, l_vector_2:B2D_VECTOR_2D
		do
			create l_vector_1.make_with_coordinates (3.36435, -93.03718)
			create l_vector_2.make_with_coordinates (889.08975, 436.70258)
			create l_input.make_with_vectors (l_vector_1, l_vector_2, 662.37217)
			assert("make_from_vectors Valid: Point_1.x", l_input.point_1.x ~ 3.36435)
			assert("make_from_make_from_vectorscoordinates Valid: Point_1.y", l_input.point_1.y ~ -93.03718)
			assert("make_from_vectors Valid: Point_2.x", l_input.point_2.x ~ 889.08975)
			assert("make_from_vectors Valid: Point_2.y", l_input.point_2.y ~ 436.70258)
			assert("make_from_vectors Valid: maximum_fraction", l_input.maximum_fraction ~ 662.37217)
		end

	test_set_maximum_fraction_normal
			-- Normal test {B2D_RAY_CAST_INPUT}.set_maximum_fraction
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.set_maximum_fraction", "covers/{B2D_RAY_CAST_INPUT}.default_create",
						"covers/{B2D_RAY_CAST_INPUT}.maximum_fraction"
		local
			l_input:B2D_RAY_CAST_INPUT
		do
			create l_input
			l_input.set_maximum_fraction (-963.95901)
			assert("set_maximum_fraction Valid", l_input.maximum_fraction ~ -963.95901)
		end

	test_set_maximum_fraction_wrong
			-- Wrong test for set_maximum_fraction
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.set_maximum_fraction",
					  "covers/{B2D_RAY_CAST_INPUT}.default_create"
		local
			l_input:B2D_RAY_CAST_INPUT
			l_retry:BOOLEAN
		do
			if not l_retry then
				create l_input
				l_input.put_null
				l_input.set_maximum_fraction (1)
				assert ("set_maximum_fraction valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_maximum_fraction_wrong
			-- Wrong test for maximum_fraction
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.maximum_fraction",
					  "covers/{B2D_RAY_CAST_INPUT}.default_create"
		local
			l_input:B2D_RAY_CAST_INPUT
			l_retry:BOOLEAN
			l_result:REAL_32
		do
			if not l_retry then
				create l_input
				l_input.put_null
				l_result := l_input.maximum_fraction
				assert ("maximum_fraction valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_is_equal_Normal_1
			-- Normal test 1 {B2D_RAY_CAST_INPUT}.is_equal
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.is_equal", "covers/{B2D_RAY_CAST_INPUT}.point_1",
						"covers/{B2D_RAY_CAST_INPUT}.point_2", "covers/{B2D_RAY_CAST_INPUT}.maximum_fraction"
		local
			l_input1, l_input2:B2D_RAY_CAST_INPUT
		do
			create l_input1.make_with_coordinates (3.36435, -93.03718, 889.08975, 436.70258, 662.37217)
			create l_input2.make_with_coordinates (3.36435, -93.03718, 889.08975, 436.70258, 662.37217)
			assert("is_equal Valid", l_input1 ~ l_input2)
		end

	test_is_equal_Normal_2
			-- Normal test 2 {B2D_RAY_CAST_INPUT}.is_equal
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.is_equal", "covers/{B2D_RAY_CAST_INPUT}.point_1",
						"covers/{B2D_RAY_CAST_INPUT}.point_2", "covers/{B2D_RAY_CAST_INPUT}.maximum_fraction"
		local
			l_input1, l_input2:B2D_RAY_CAST_INPUT
		do
			create l_input1.make_with_coordinates (3.36435, -93.03718, 889.08975, 436.70258, 662.37217)
			create l_input2.make_with_coordinates (-110.29875, -93.03718, 889.08975, 436.70258, 662.37217)
			assert("is_equal Valid", l_input1 /~ l_input2)
		end

	test_is_equal_Normal_3
			-- Normal test 3 {B2D_RAY_CAST_INPUT}.is_equal
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.is_equal", "covers/{B2D_RAY_CAST_INPUT}.point_1",
						"covers/{B2D_RAY_CAST_INPUT}.point_2", "covers/{B2D_RAY_CAST_INPUT}.maximum_fraction"
		local
			l_input1, l_input2:B2D_RAY_CAST_INPUT
		do
			create l_input1.make_with_coordinates (3.36435, -93.03718, 889.08975, 436.70258, 662.37217)
			create l_input2.make_with_coordinates (3.36435, -884.15893, 889.08975, 436.70258, 662.37217)
			assert("is_equal Valid", l_input1 /~ l_input2)
		end

	test_is_equal_Normal_4
			-- Normal test 4 {B2D_RAY_CAST_INPUT}.is_equal
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.is_equal", "covers/{B2D_RAY_CAST_INPUT}.point_1",
						"covers/{B2D_RAY_CAST_INPUT}.point_2", "covers/{B2D_RAY_CAST_INPUT}.maximum_fraction"
		local
			l_input1, l_input2:B2D_RAY_CAST_INPUT
		do
			create l_input1.make_with_coordinates (3.36435, -93.03718, 889.08975, 436.70258, 662.37217)
			create l_input2.make_with_coordinates (3.36435, -93.03718, -407.95921, 436.70258, 662.37217)
			assert("is_equal Valid", l_input1 /~ l_input2)
		end

	test_is_equal_Normal_5
			-- Normal test 5 {B2D_RAY_CAST_INPUT}.is_equal
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.is_equal", "covers/{B2D_RAY_CAST_INPUT}.point_1",
						"covers/{B2D_RAY_CAST_INPUT}.point_2", "covers/{B2D_RAY_CAST_INPUT}.maximum_fraction"
		local
			l_input1, l_input2:B2D_RAY_CAST_INPUT
		do
			create l_input1.make_with_coordinates (3.36435, -93.03718, 889.08975, 436.70258, 662.37217)
			create l_input2.make_with_coordinates (3.36435, -93.03718, 889.08975, -592.07821, 662.37217)
			assert("is_equal Valid", l_input1 /~ l_input2)
		end

	test_is_equal_Normal_6
			-- Normal test 6 {B2D_RAY_CAST_INPUT}.is_equal
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.is_equal", "covers/{B2D_RAY_CAST_INPUT}.point_1",
						"covers/{B2D_RAY_CAST_INPUT}.point_2", "covers/{B2D_RAY_CAST_INPUT}.maximum_fraction"
		local
			l_input1, l_input2:B2D_RAY_CAST_INPUT
		do
			create l_input1.make_with_coordinates (3.36435, -93.03718, 889.08975, 436.70258, 662.37217)
			create l_input2.make_with_coordinates (3.36435, -93.03718, 889.08975, 436.70258, -911.44896)
			assert("is_equal Valid", l_input1 /~ l_input2)
		end

	test_is_equal_Normal_7
			-- Normal test 7 {B2D_RAY_CAST_INPUT}.is_equal
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.is_equal"
		local
			l_input1, l_input2:B2D_RAY_CAST_INPUT
		do
			create l_input1.make_with_coordinates (3.36435, -93.03718, 889.08975, 436.70258, 662.37217)
			create l_input2.make_with_coordinates (3.36435, -93.03718, 889.08975, 436.70258, 662.37217)
			l_input1.put_null
			assert("is_equal Valid", l_input1 /~ l_input2)
		end

	test_is_equal_Normal_8
			-- Normal test 8 {B2D_RAY_CAST_INPUT}.is_equal
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.is_equal"
		local
			l_input1, l_input2:B2D_RAY_CAST_INPUT
		do
			create l_input1.make_with_coordinates (3.36435, -93.03718, 889.08975, 436.70258, 662.37217)
			create l_input2.make_with_coordinates (3.36435, -93.03718, 889.08975, 436.70258, 662.37217)
			l_input2.put_null
			assert("is_equal Valid", l_input1 /~ l_input2)
		end

	test_is_equal_Normal_9
			-- Normal test 9 {B2D_RAY_CAST_INPUT}.is_equal
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.is_equal"
		local
			l_input1, l_input2:B2D_RAY_CAST_INPUT
		do
			create l_input1.make_with_coordinates (3.36435, -93.03718, 889.08975, 436.70258, 662.37217)
			create l_input2.make_with_coordinates (3.36435, -93.03718, 889.08975, 436.70258, -911.44896)
			l_input1.put_null
			l_input2.put_null
			assert("is_equal Valid", l_input1 ~ l_input2)
		end

	test_set_point_1_with_vector_normal
			-- Normal test for set_point_1_with_vector
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.set_point_1_with_vector",
					  "covers/{B2D_RAY_CAST_INPUT}.point_1",
					  "covers/{B2D_RAY_CAST_INPUT}.default_create"
		local
			l_input:B2D_RAY_CAST_INPUT
			l_vector:B2D_VECTOR_2D
		do
			create l_input
			create l_vector.make_with_coordinates (7.90835, -0.03097)
			l_input.set_point_1_with_vector (l_vector)
			assert ("set_point_1_with_vector valid", l_input.point_1 ~ l_vector)
		end

	test_set_point_1_with_vector_wrong_1
			-- Wrong test 1 for set_point_1_with_vector
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.set_point_1_with_vector",
					  "covers/{B2D_RAY_CAST_INPUT}.point_1",
					  "covers/{B2D_RAY_CAST_INPUT}.default_create"
		local
			l_input:B2D_RAY_CAST_INPUT
			l_vector:B2D_VECTOR_2D
			l_retry:BOOLEAN
		do
			if not l_retry then
				create l_input
				create l_vector.make_with_coordinates (7.90835, -0.03097)
				l_input.put_null
				l_input.set_point_1_with_vector (l_vector)
				assert ("make_with_coordinates valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_point_1_with_vector_wrong_2
			-- Wrong test 2 for set_point_1_with_vector
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.set_point_1_with_vector",
					  "covers/{B2D_RAY_CAST_INPUT}.point_1",
					  "covers/{B2D_RAY_CAST_INPUT}.default_create"
		local
			l_input:B2D_RAY_CAST_INPUT
			l_vector:B2D_VECTOR_2D
			l_retry:BOOLEAN
		do
			if not l_retry then
				create l_input
				create l_vector.make_with_coordinates (7.90835, -0.03097)
				l_vector.put_null
				l_input.set_point_1_with_vector (l_vector)
				assert ("make_with_coordinates valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_point_1_normal
			-- Normal test for set_point_1_with_vector
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.set_point_1",
					  "covers/{B2D_RAY_CAST_INPUT}.point_1",
					  "covers/{B2D_RAY_CAST_INPUT}.default_create"
		local
			l_input:B2D_RAY_CAST_INPUT
		do
			create l_input
			l_input.set_point_1 (7.90835, -0.03097)
			assert ("make_with_coordinates valid X", l_input.point_1.x ~ 7.90835)
			assert ("make_with_coordinates valid Y", l_input.point_1.y ~ -0.03097)
		end

	test_set_point_1_wrong_1
			-- Wrong test 1 for set_point_1
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.set_point_1",
					  "covers/{B2D_RAY_CAST_INPUT}.point_1",
					  "covers/{B2D_RAY_CAST_INPUT}.default_create"
		local
			l_input:B2D_RAY_CAST_INPUT
			l_retry:BOOLEAN
		do
			if not l_retry then
				create l_input
				l_input.put_null
				l_input.set_point_1 (0, 0)
				assert ("make_with_coordinates valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_point_1_wrong_1
			-- Wrong test 1 for point_1
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.point_1",
					  "covers/{B2D_RAY_CAST_INPUT}.default_create"
		local
			l_input:B2D_RAY_CAST_INPUT
			l_retry:BOOLEAN
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_input
				l_input.put_null
				l_result := l_input.point_1
				assert ("point_1 valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_point_2_with_vector_normal
			-- Normal test for set_point_2_with_vector
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.set_point_2_with_vector",
					  "covers/{B2D_RAY_CAST_INPUT}.point_2",
					  "covers/{B2D_RAY_CAST_INPUT}.default_create"
		local
			l_input:B2D_RAY_CAST_INPUT
			l_vector:B2D_VECTOR_2D
		do
			create l_input
			create l_vector.make_with_coordinates (7.90835, -0.03097)
			l_input.set_point_2_with_vector (l_vector)
			assert ("set_point_2_with_vector valid", l_input.point_2 ~ l_vector)
		end

	test_set_point_2_with_vector_wrong_1
			-- Wrong test 1 for set_point_2_with_vector
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.set_point_2_with_vector",
					  "covers/{B2D_RAY_CAST_INPUT}.point_2",
					  "covers/{B2D_RAY_CAST_INPUT}.default_create"
		local
			l_input:B2D_RAY_CAST_INPUT
			l_vector:B2D_VECTOR_2D
			l_retry:BOOLEAN
		do
			if not l_retry then
				create l_input
				create l_vector.make_with_coordinates (7.90835, -0.03097)
				l_input.put_null
				l_input.set_point_2_with_vector (l_vector)
				assert ("make_with_coordinates valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_point_2_with_vector_wrong_2
			-- Wrong test 2 for set_point_2_with_vector
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.set_point_2_with_vector",
					  "covers/{B2D_RAY_CAST_INPUT}.point_2",
					  "covers/{B2D_RAY_CAST_INPUT}.default_create"
		local
			l_input:B2D_RAY_CAST_INPUT
			l_vector:B2D_VECTOR_2D
			l_retry:BOOLEAN
		do
			if not l_retry then
				create l_input
				create l_vector.make_with_coordinates (7.90835, -0.03097)
				l_vector.put_null
				l_input.set_point_2_with_vector (l_vector)
				assert ("make_with_coordinates valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_point_2_normal
			-- Normal test for set_point_2_with_vector
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.set_point_2",
					  "covers/{B2D_RAY_CAST_INPUT}.point_2",
					  "covers/{B2D_RAY_CAST_INPUT}.default_create"
		local
			l_input:B2D_RAY_CAST_INPUT
		do
			create l_input
			l_input.set_point_2 (7.90835, -0.03097)
			assert ("make_with_coordinates valid X", l_input.point_2.x ~ 7.90835)
			assert ("make_with_coordinates valid Y", l_input.point_2.y ~ -0.03097)
		end

	test_set_point_2_wrong_1
			-- Wrong test 1 for set_point_2
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.set_point_2",
					  "covers/{B2D_RAY_CAST_INPUT}.point_2",
					  "covers/{B2D_RAY_CAST_INPUT}.default_create"
		local
			l_input:B2D_RAY_CAST_INPUT
			l_retry:BOOLEAN
		do
			if not l_retry then
				create l_input
				l_input.put_null
				l_input.set_point_2 (0, 0)
				assert ("make_with_coordinates valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_point_2_wrong_1
			-- Wrong test 1 for point_2
		note
			testing:  "covers/{B2D_RAY_CAST_INPUT}.point_2",
					  "covers/{B2D_RAY_CAST_INPUT}.default_create"
		local
			l_input:B2D_RAY_CAST_INPUT
			l_retry:BOOLEAN
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_input
				l_input.put_null
				l_result := l_input.point_2
				assert ("point_2 valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

end


