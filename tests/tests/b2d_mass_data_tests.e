note
	description: "Tests for {B2D_MASS_DATA}"
	author: "Louis Marchand"
	date: "Thu, 25 Jun 2020 13:45:55 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_MASS_DATA_TESTS

inherit
	EQA_TEST_SET
	B2D_ANY
		undefine
			default_create
		end

feature -- Test routines

	test_set_center_with_vector_normal
			-- Normal test for set_center_with_vector
		note
			testing:  "covers/{B2D_MASS_DATA}.set_center_with_vector",
					  "covers/{B2D_MASS_DATA}.center",
					  "covers/{B2D_MASS_DATA}.default_create"
		local
			l_mass:B2D_MASS_DATA
			l_vector:B2D_VECTOR_2D
		do
			create l_mass
			create l_vector.make_with_coordinates (7.90835, -0.03097)
			l_mass.set_center_with_vector (l_vector)
			assert ("set_center_with_vector valid", l_mass.center ~ l_vector)
		end

	test_set_center_with_vector_wrong_1
			-- Wrong test 1 for set_center_with_vector
		note
			testing:  "covers/{B2D_MASS_DATA}.set_center_with_vector",
					  "covers/{B2D_MASS_DATA}.center",
					  "covers/{B2D_MASS_DATA}.default_create"
		local
			l_mass:B2D_MASS_DATA
			l_vector:B2D_VECTOR_2D
			l_retry:BOOLEAN
		do
			if not l_retry then
				create l_mass
				create l_vector.make_with_coordinates (7.90835, -0.03097)
				l_mass.put_null
				l_mass.set_center_with_vector (l_vector)
				assert ("set_center_with_vector valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_center_with_vector_wrong_2
			-- Wrong test 2 for set_center_with_vector
		note
			testing:  "covers/{B2D_MASS_DATA}.set_center_with_vector",
					  "covers/{B2D_MASS_DATA}.center",
					  "covers/{B2D_MASS_DATA}.default_create"
		local
			l_mass:B2D_MASS_DATA
			l_vector:B2D_VECTOR_2D
			l_retry:BOOLEAN
		do
			if not l_retry then
				create l_mass
				create l_vector.make_with_coordinates (7.90835, -0.03097)
				l_vector.put_null
				l_mass.set_center_with_vector (l_vector)
				assert ("make_with_coordinates valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_center_normal
			-- Normal test for set_center
		note
			testing:  "covers/{B2D_MASS_DATA}.set_center",
					  "covers/{B2D_MASS_DATA}.center",
					  "covers/{B2D_MASS_DATA}.default_create"
		local
			l_mass:B2D_MASS_DATA
		do
			create l_mass
			l_mass.set_center (7.90835, -0.03097)
			assert ("set_center valid X", l_mass.center.x ~ 7.90835)
			assert ("set_center valid Y", l_mass.center.y ~ -0.03097)
		end

	test_set_center_wrong_1
			-- Wrong test 1 for set_center
		note
			testing:  "covers/{B2D_MASS_DATA}.set_center",
					  "covers/{B2D_MASS_DATA}.center",
					  "covers/{B2D_MASS_DATA}.default_create"
		local
			l_mass:B2D_MASS_DATA
			l_retry:BOOLEAN
		do
			if not l_retry then
				create l_mass
				l_mass.put_null
				l_mass.set_center (0, 0)
				assert ("set_center valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_center_wrong_1
			-- Wrong test 1 for center
		note
			testing:  "covers/{B2D_MASS_DATA}.center",
					  "covers/{B2D_MASS_DATA}.default_create"
		local
			l_mass:B2D_MASS_DATA
			l_retry:BOOLEAN
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_mass
				l_mass.put_null
				l_result := l_mass.center
				assert ("center valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_to_data_normal
			-- Normal test for to_data
		note
			testing:  "covers/{B2D_MASS_DATA}.to_data",
					  "covers/{B2D_MASS_DATA}.set_center",
					  "covers/{B2D_MASS_DATA}.center",
					  "covers/{B2D_MASS_DATA}.set_inertia",
					  "covers/{B2D_MASS_DATA}.inertia",
					  "covers/{B2D_MASS_DATA}.set_mass",
					  "covers/{B2D_MASS_DATA}.mass",
					  "covers/{B2D_MASS_DATA}.default_create"
		local
			l_mass_data_1, l_mass_data_2:B2D_MASS_DATA
			l_mass:B2D_MASS
		do
			create l_mass_data_1
			l_mass_data_1.set_inertia (5.08076)
			l_mass_data_1.set_mass (-8.55233)
			l_mass_data_1.set_center (7.90835, -0.03097)
			l_mass := l_mass_data_1
			l_mass_data_2 := l_mass.to_data
			assert ("to_data valid center.x", l_mass_data_2.center.x ~ 7.90835)
			assert ("to_data valid center.y", l_mass_data_2.center.y ~ -0.03097)
			assert ("to_data valid inertia", l_mass_data_2.inertia ~ 5.08076)
			assert ("to_data valid mass", l_mass_data_2.mass ~ -8.55233)
		end

	test_to_data_wrong
			-- Wrong test for to_data
		note
			testing:  "covers/{B2D_MASS_DATA}.to_data",
					  "covers/{B2D_MASS_DATA}.default_create"
		local
			l_mass_data_1, l_mass_data_2:B2D_MASS_DATA
			l_mass:B2D_MASS
			l_retry:BOOLEAN
		do
			if not l_retry then
				create l_mass_data_1
				l_mass := l_mass_data_1
				l_mass.put_null
				l_mass_data_2 := l_mass.to_data
				assert ("to_data valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_mass_normal
			-- Normal test for set_mass
		note
			testing:  "covers/{B2D_MASS_DATA}.set_mass",
					  "covers/{B2D_MASS_DATA}.mass",
					  "covers/{B2D_MASS_DATA}.default_create"
		local
			l_mass:B2D_MASS_DATA
		do
			create l_mass
			l_mass.set_mass (2.59475)
			assert ("set_mass valid X", l_mass.mass ~ 2.59475)
		end

	test_set_mass_wrong
			-- Wrong test for set_mass
		note
			testing:  "covers/{B2D_MASS_DATA}.set_mass",
					  "covers/{B2D_MASS_DATA}.default_create"
		local
			l_mass:B2D_MASS_DATA
			l_retry:BOOLEAN
		do
			if not l_retry then
				create l_mass
				l_mass.put_null
				l_mass.set_mass (2.59475)
				assert ("set_mass valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_mass_wrong
			-- Wrong test for mass
		note
			testing:  "covers/{B2D_MASS_DATA}.mass",
					  "covers/{B2D_MASS_DATA}.default_create"
		local
			l_mass:B2D_MASS_DATA
			l_retry:BOOLEAN
			l_result:REAL_32
		do
			if not l_retry then
				create l_mass
				l_mass.put_null
				l_result := l_mass.mass
				assert ("mass valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_inertia_normal
			-- Normal test for set_inertia
		note
			testing:  "covers/{B2D_MASS_DATA}.set_inertia",
					  "covers/{B2D_MASS_DATA}.inertia",
					  "covers/{B2D_MASS_DATA}.default_create"
		local
			l_mass:B2D_MASS_DATA
		do
			create l_mass
			l_mass.set_inertia (2.59475)
			assert ("set_inertia valid X", l_mass.inertia ~ 2.59475)
		end

	test_set_inertia_wrong
			-- Wrong test for set_inertia
		note
			testing:  "covers/{B2D_MASS_DATA}.set_inertia",
					  "covers/{B2D_MASS_DATA}.default_create"
		local
			l_mass:B2D_MASS_DATA
			l_retry:BOOLEAN
		do
			if not l_retry then
				create l_mass
				l_mass.put_null
				l_mass.set_inertia (2.59475)
				assert ("set_inertia valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_inertia_wrong
			-- Wrong test for inertia
		note
			testing:  "covers/{B2D_MASS_DATA}.inertia",
					  "covers/{B2D_MASS_DATA}.default_create"
		local
			l_mass:B2D_MASS_DATA
			l_retry:BOOLEAN
			l_result:REAL_32
		do
			if not l_retry then
				create l_mass
				l_mass.put_null
				l_result := l_mass.inertia
				assert ("inertia valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_is_equal_normal_1
			-- Normal test 1 for is_equal
		note
			testing:  "covers/{B2D_MASS_DATA}.is_equal",
					  "covers/{B2D_MASS_DATA}.set_inertia",
					  "covers/{B2D_MASS_DATA}.set_mass",
					  "covers/{B2D_MASS_DATA}.set_ceter",
					  "covers/{B2D_MASS_DATA}.default_create"
		local
			l_mass_1, l_mass_2:B2D_MASS_DATA
		do
			create l_mass_1
			create l_mass_2
			l_mass_1.set_center (3.48491, -9.52159)
			l_mass_1.set_mass (5.73545)
			l_mass_1.set_inertia (-4.1112)
			l_mass_2.set_center (3.48491, -9.52159)
			l_mass_2.set_mass (5.73545)
			l_mass_2.set_inertia (-4.1112)
			assert ("is_equal valid", l_mass_1 ~ l_mass_2)
		end

	test_is_equal_normal_2
			-- Normal test 2 for is_equal
		note
			testing:  "covers/{B2D_MASS_DATA}.is_equal",
					  "covers/{B2D_MASS_DATA}.set_inertia",
					  "covers/{B2D_MASS_DATA}.set_mass",
					  "covers/{B2D_MASS_DATA}.set_ceter",
					  "covers/{B2D_MASS_DATA}.default_create"
		local
			l_mass_1, l_mass_2:B2D_MASS_DATA
		do
			create l_mass_1
			create l_mass_2
			l_mass_1.set_center (3.48491, -9.52159)
			l_mass_1.set_mass (5.73545)
			l_mass_1.set_inertia (-4.1112)
			l_mass_2.set_center (-7.35124, -9.52159)
			l_mass_2.set_mass (5.73545)
			l_mass_2.set_inertia (-4.1112)
			assert ("is_equal valid", l_mass_1 /~ l_mass_2)
		end

	test_is_equal_normal_3
			-- Normal test 3 for is_equal
		note
			testing:  "covers/{B2D_MASS_DATA}.is_equal",
					  "covers/{B2D_MASS_DATA}.set_inertia",
					  "covers/{B2D_MASS_DATA}.set_mass",
					  "covers/{B2D_MASS_DATA}.set_ceter",
					  "covers/{B2D_MASS_DATA}.default_create"
		local
			l_mass_1, l_mass_2:B2D_MASS_DATA
		do
			create l_mass_1
			create l_mass_2
			l_mass_1.set_center (3.48491, -9.52159)
			l_mass_1.set_mass (5.73545)
			l_mass_1.set_inertia (-4.1112)
			l_mass_2.set_center (3.48491, -7.35124)
			l_mass_2.set_mass (5.73545)
			l_mass_2.set_inertia (-4.1112)
			assert ("is_equal valid", l_mass_1 /~ l_mass_2)
		end

	test_is_equal_normal_4
			-- Normal test 4 for is_equal
		note
			testing:  "covers/{B2D_MASS_DATA}.is_equal",
					  "covers/{B2D_MASS_DATA}.set_inertia",
					  "covers/{B2D_MASS_DATA}.set_mass",
					  "covers/{B2D_MASS_DATA}.set_ceter",
					  "covers/{B2D_MASS_DATA}.default_create"
		local
			l_mass_1, l_mass_2:B2D_MASS_DATA
		do
			create l_mass_1
			create l_mass_2
			l_mass_1.set_center (3.48491, -9.52159)
			l_mass_1.set_mass (5.73545)
			l_mass_1.set_inertia (-4.1112)
			l_mass_2.set_center (3.48491, -9.52159)
			l_mass_2.set_mass (-7.35124)
			l_mass_2.set_inertia (-4.1112)
			assert ("is_equal valid", l_mass_1 /~ l_mass_2)
		end

	test_is_equal_normal_5
			-- Normal test 5 for is_equal
		note
			testing:  "covers/{B2D_MASS_DATA}.is_equal",
					  "covers/{B2D_MASS_DATA}.set_inertia",
					  "covers/{B2D_MASS_DATA}.set_mass",
					  "covers/{B2D_MASS_DATA}.set_ceter",
					  "covers/{B2D_MASS_DATA}.default_create"
		local
			l_mass_1, l_mass_2:B2D_MASS_DATA
		do
			create l_mass_1
			create l_mass_2
			l_mass_1.set_center (3.48491, -9.52159)
			l_mass_1.set_mass (5.73545)
			l_mass_1.set_inertia (-4.1112)
			l_mass_2.set_center (3.48491, -9.52159)
			l_mass_2.set_mass (5.73545)
			l_mass_2.set_inertia (-7.35124)
			assert ("is_equal valid", l_mass_1 /~ l_mass_2)
		end

	test_is_equal_limit
			-- Limit test for is_equal
		note
			testing:  "covers/{B2D_MASS_DATA}.is_equal",
					  "covers/{B2D_MASS_DATA}.default_create"
		local
			l_mass_1, l_mass_2:B2D_MASS_DATA
		do
			create l_mass_1
			create l_mass_2
			assert ("is_equal valid", l_mass_1 ~ l_mass_2)
		end

	test_is_equal_normal_6
			-- Normal test 6 for is_equal
		note
			testing:  "covers/{B2D_MASS_DATA}.is_equal",
					  "covers/{B2D_MASS_DATA}.default_create"
		local
			l_mass_1, l_mass_2:B2D_MASS_DATA
		do
			create l_mass_1
			create l_mass_2
			l_mass_1.put_null
			assert ("is_equal valid", l_mass_1 /~ l_mass_2)
		end

	test_is_equal_normal_7
			-- Normal test 7 for is_equal
		note
			testing:  "covers/{B2D_MASS_DATA}.is_equal",
					  "covers/{B2D_MASS_DATA}.default_create"
		local
			l_mass_1, l_mass_2:B2D_MASS_DATA
		do
			create l_mass_1
			create l_mass_2
			l_mass_2.put_null
			assert ("is_equal valid", l_mass_1 /~ l_mass_2)
		end

	test_is_equal_normal_8
			-- Normal test 8 for is_equal
		note
			testing:  "covers/{B2D_MASS_DATA}.is_equal",
					  "covers/{B2D_MASS_DATA}.default_create"
		local
			l_mass_1, l_mass_2:B2D_MASS_DATA
		do
			create l_mass_1
			create l_mass_2
			l_mass_1.set_inertia (-4.1112)
			l_mass_2.set_inertia (-7.35124)
			l_mass_1.put_null
			l_mass_2.put_null
			assert ("is_equal valid", l_mass_1 ~ l_mass_2)
		end

end


