note
	description: "Tests for {B2D_BODY_DEFINITION}."
	author: "Patrick Boucher"
	date: "Sat, 20 Jun 2020 16:32:00"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_BODY_DEFINITION_TESTS

inherit
	EQA_TEST_SET

feature -- Test routines

	test_make_dynamic
			-- Test routine for {B2D_BODY_DEFINITION}.make_dynamic
		note
			testing:
				"covers/{B2D_BODY_DEFINITION}.default_create",
				"covers/{B2D_BODY_DEFINITION}.make_dynamic",
				"covers/{B2D_BODY_DEFINITION}.make_with_body_type"
		local
			l_definition: B2D_BODY_DEFINITION
		do
			create l_definition
			assert ("make_dynamic valid", l_definition.is_dynamic)
		end

	test_make_kinematic
			-- Test routine for {B2D_BODY_DEFINITION}.make_kinematic
		note
			testing:
				"covers/{B2D_BODY_DEFINITION}.make_kinematic",
				"covers/{B2D_BODY_DEFINITION}.make_with_body_type"
		local
			l_definition: B2D_BODY_DEFINITION
		do
			create l_definition.make_kinematic
			assert ("make_kinematic valid", l_definition.is_kinematic)
		end

	test_make_static
			-- Test routine for {B2D_BODY_DEFINITION}.make_static
		note
			testing:
				"covers/{B2D_BODY_DEFINITION}.make_static",
				"covers/{B2D_BODY_DEFINITION}.make_with_body_type"
		local
			l_definition: B2D_BODY_DEFINITION
		do
			create l_definition.make_static
			assert ("make_static valid", l_definition.is_static)
		end

	test_position
			-- Test routine for {B2D_BODY_DEFINITION}.set_position
		note
			testing:
				"covers/{B2D_BODY_DEFINITION}.set_position",
				"covers/{B2D_BODY_DEFINITION}.position"
		local
			l_definition: B2D_BODY_DEFINITION
			l_position: B2D_VECTOR_2D
		do
			create l_definition
			create l_position.make_with_coordinates (1.5, 5.5)
			l_definition.position := l_position
			assert ("set_position valid", l_definition.position ~ l_position)
		end

	test_angle
			-- Test routine for {B2D_BODY_DEFINITION}.set_angle
		note
			testing:
				"covers/{B2D_BODY_DEFINITION}.set_angle",
				"covers/{B2D_BODY_DEFINITION}.angle"
		local
			l_definition: B2D_BODY_DEFINITION
			l_value: REAL_32
		do
			l_value := -15.314
			create l_definition
			l_definition.set_angle (l_value)
			assert ("set_angle valid", l_definition.angle ~ l_value)
		end

	test_linear_velocity_1
			-- Test routine for {B2D_BODY_DEFINITION}.set_linear_velocity
		note
			testing:
				"covers/{B2D_BODY_DEFINITION}.set_linear_velocity",
				"covers/{B2D_BODY_DEFINITION}.linear_velocity"
		local
			l_definition: B2D_BODY_DEFINITION
			l_linear_velocity: B2D_VECTOR_2D
		do
			create l_linear_velocity.make_with_coordinates (10, -2.3)
			create l_definition
			l_definition.set_linear_velocity_with_vector (l_linear_velocity)
			assert ("set_linear_velocity valid", l_definition.linear_velocity ~ l_linear_velocity)
		end

	test_linear_velocity_2
			-- Test routine for {B2D_BODY_DEFINITION}.set_linear_velocity
		note
			testing:
				"covers/{B2D_BODY_DEFINITION}.set_linear_velocity",
				"covers/{B2D_BODY_DEFINITION}.linear_velocity"
		local
			l_definition: B2D_BODY_DEFINITION
			l_linear_velocity: B2D_VECTOR_2D
		do
			create l_linear_velocity.make_with_coordinates (10, -2.3)
			create l_definition
			l_definition.set_linear_velocity (10, -2.3)
			assert ("set_linear_velocity valid", l_definition.linear_velocity ~ l_linear_velocity)
		end

	test_angular_velocity
			-- Test routine for {B2D_BODY_DEFINITION}.set_angular_velocity
		note
			testing:
				"covers/{B2D_BODY_DEFINITION}.set_angular_velocity",
				"covers/{B2D_BODY_DEFINITION}.angular_velocity"
		local
			l_definition: B2D_BODY_DEFINITION
			l_value: REAL_32
		do
			l_value := -23.4523
			create l_definition
			l_definition.set_angular_velocity (l_value)
			assert ("set_angular_velocity valid", l_definition.angular_velocity ~ l_value)
		end

	test_linear_damping
			-- Test routine for {B2D_BODY_DEFINITION}.set_linear_damping
		note
			testing:
				"covers/{B2D_BODY_DEFINITION}.set_linear_damping",
				"covers/{B2D_BODY_DEFINITION}.linear_damping"
		local
			l_definition: B2D_BODY_DEFINITION
			l_value: REAL_32
		do
			l_value := 0.025
			create l_definition
			l_definition.set_linear_damping (l_value)
			assert ("set_linear_damping valid", l_definition.linear_damping ~ l_value)
		end

	test_angular_damping
			-- Test routine for {B2D_BODY_DEFINITION}.set_angular_damping
		note
			testing:
				"covers/{B2D_BODY_DEFINITION}.set_angular_damping",
				"covers/{B2D_BODY_DEFINITION}.angular_damping"
		local
			l_definition: B2D_BODY_DEFINITION
			l_value: REAL_32
		do
			l_value := 0.09
			create l_definition
			l_definition.set_angular_damping (l_value)
			assert ("set_angular_damping valid", l_definition.angular_damping ~ l_value)
		end

	test_allow_sleep
			-- Test routine for {B2D_BODY_DEFINITION}.set_allow_sleep
		note
			testing:
				"covers/{B2D_BODY_DEFINITION}.set_allow_sleep",
				"covers/{B2D_BODY_DEFINITION}.allow_sleep"
		local
			l_definition: B2D_BODY_DEFINITION
		do
			create l_definition
			assert ("allow_sleep True by default", l_definition.allow_sleep)
			l_definition.set_allow_sleep (False)
			assert ("set_allow_sleep valid", not l_definition.allow_sleep)
			l_definition.enable_allow_sleep
			assert ("enable_allow_sleep valid", l_definition.allow_sleep)
			l_definition.disable_allow_sleep
			assert ("disable_allow_sleep valid", not l_definition.allow_sleep)
		end

	test_is_awake
			-- Test routine for {B2D_BODY_DEFINITION}.set_is_awake
		note
			testing:
				"covers/{B2D_BODY_DEFINITION}.set_is_awake",
				"covers/{B2D_BODY_DEFINITION}.is_awake"
		local
			l_definition: B2D_BODY_DEFINITION
		do
			create l_definition
			assert ("is_awake True by default", l_definition.is_awake)
			l_definition.set_is_awake (False)
			assert ("set_is_awake valid", not l_definition.is_awake)
			l_definition.wake
			assert ("wake valid", l_definition.is_awake)
			l_definition.sleep
			assert ("sleep valid", not l_definition.is_awake)
		end

	test_fixed_rotation
			-- Test routine for {B2D_BODY_DEFINITION}.set_fixed_rotation
		note
			testing:
				"covers/{B2D_BODY_DEFINITION}.set_fixed_rotation",
				"covers/{B2D_BODY_DEFINITION}.fixed_rotation"
		local
			l_definition: B2D_BODY_DEFINITION
		do
			create l_definition
			assert ("fixed_rotation False by default", not l_definition.is_fixed_rotation)
			l_definition.set_is_fixed_rotation (True)
			assert ("set_fixed_rotation valid", l_definition.is_fixed_rotation)
			l_definition.disable_fixed_rotation
			assert ("disable_fixed_rotation valid", not l_definition.is_fixed_rotation)
			l_definition.enable_fixed_rotation
			assert ("enable_fixed_rotation valid", l_definition.is_fixed_rotation)
		end

	test_bullet
			-- Test routine for {B2D_BODY_DEFINITION}.set_is_bullet
		note
			testing:
				"covers/{B2D_BODY_DEFINITION}.set_is_bullet",
				"covers/{B2D_BODY_DEFINITION}.is_bullet"
		local
			l_definition: B2D_BODY_DEFINITION
		do
			create l_definition
			assert ("is_bullet False by default", not l_definition.is_bullet)
			l_definition.set_is_bullet (True)
			assert ("set_is_bullet valid", l_definition.is_bullet)
			l_definition.disable_bullet
			assert ("disable_bullet valid", not l_definition.is_bullet)
			l_definition.enable_bullet
			assert ("enable_bullet valid", l_definition.is_bullet)
		end

	test_active
			-- Test routine for {B2D_BODY_DEFINITION}.set_is_active
		note
			testing:
				"covers/{B2D_BODY_DEFINITION}.set_is_active",
				"covers/{B2D_BODY_DEFINITION}.is_active"
		local
			l_definition: B2D_BODY_DEFINITION
		do
			create l_definition
			assert ("is_active True by default", l_definition.is_active)
			l_definition.set_is_active (False)
			assert ("set_is_active valid", not l_definition.is_active)
			l_definition.activate
			assert ("activate valid", l_definition.is_active)
			l_definition.deactivate
			assert ("deactivate valid", not l_definition.is_active)
		end

	test_gravity_scale
			-- Test routine for {B2D_BODY_DEFINITION}.set_gravity_scale
		note
			testing:
				"covers/{B2D_BODY_DEFINITION}.set_gravity_scale",
				"covers/{B2D_BODY_DEFINITION}.gravity_scale"
		local
			l_definition: B2D_BODY_DEFINITION
			l_value: REAL_32
		do
			create l_definition
			assert ("gravity_scale default valid", l_definition.gravity_scale ~ 1.0)
			l_value := 0.33
			l_definition.set_gravity_scale (l_value)
			assert ("set_gravity_scale valid", l_definition.gravity_scale ~ l_value)
		end
end


