note
	description: "Tests for {B2D_JOINT_PULLEY_DEFINITION}"
	author: "Patrick Boucher"
	date: "Fri, 03 Jul 2020 03:54:17 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_JOINT_PULLEY_DEFINITION_TESTS

inherit
	EQA_TEST_SET
		redefine
			on_prepare
		end
	B2D_ANY
		undefine
			default_create
		end

feature {NONE} -- Implementation

	on_prepare
			-- <Precursor>
		local
			l_body_definition:B2D_BODY_DEFINITION
			l_default_ground_anchor_1:B2D_VECTOR_2D
			l_default_ground_anchor_2:B2D_VECTOR_2D
			l_default_anchor_1:B2D_VECTOR_2D
			l_default_anchor_2:B2D_VECTOR_2D
		do
			create world.make (0, -9.8)
			create l_body_definition
			world.create_body (l_body_definition)
			body_1 := world.last_body
			l_body_definition.set_position (10, 10)
			world.create_body (l_body_definition)
			body_2 := world.last_body
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_default_ground_anchor_1.make_with_coordinates (0, 15)
				create l_default_ground_anchor_2.make_with_coordinates (10, 15)
				create l_default_anchor_1.make_with_coordinates (0, 0)
				create l_default_anchor_2.make_with_coordinates (10, 10)
				create definition.make (la_body_1, la_body_2, l_default_ground_anchor_1, l_default_ground_anchor_2, l_default_anchor_1, l_default_anchor_2)
				if not attached definition then
					assert ("definition do not exist", False)
				end
			else
				assert ("bodies do not exist", False)
			end
		end

	world:B2D_WORLD
			-- The world in which the tests will be performed.

	body_1:detachable B2D_BODY
			-- The first body of each {B2D_JOINT_PULLEY_DEFINITION} during the tests.

	body_2:detachable B2D_BODY
			-- The second body of each {B2D_JOINT_PULLEY_DEFINITION} during the tests.

	definition:detachable B2D_JOINT_PULLEY_DEFINITION
			-- The definition in which the tests will be performed.

feature -- Normal test routines

	test_ground_anchor_1_normal
			-- Normal test for {B2D_JOINT_PULLEY_DEFINITION}.ground_anchor_1
		note
			testing:  "covers/{B2D_JOINT_PULLEY_DEFINITION}.ground_anchor_1",
					  "covers/{B2D_JOINT_PULLEY_DEFINITION}.set_ground_anchor_1"
		do
			if attached definition as la_definition then
				la_definition.set_ground_anchor_1 (create {B2D_VECTOR_2D}.make_with_coordinates (5.25, 6.3))
				assert ("set_ground_anchor_1 valid x", la_definition.ground_anchor_1.x ~ 5.25)
				assert ("set_ground_anchor_1 valid y", la_definition.ground_anchor_1.y ~ 6.3)
			end
		end

	test_ground_anchor_2_normal
			-- Normal test for {B2D_JOINT_PULLEY_DEFINITION}.ground_anchor_2
		note
			testing:  "covers/{B2D_JOINT_PULLEY_DEFINITION}.ground_anchor_2",
					  "covers/{B2D_JOINT_PULLEY_DEFINITION}.set_ground_anchor_2"
		do
			if attached definition as la_definition then
				la_definition.set_ground_anchor_2 (create {B2D_VECTOR_2D}.make_with_coordinates (-1.23, 0.02))
				assert ("set_ground_anchor_2 valid x", la_definition.ground_anchor_2.x ~ -1.23)
				assert ("set_ground_anchor_2 valid y", la_definition.ground_anchor_2.y ~ 0.02)
			end
		end

	test_local_anchor_1_normal
			-- Normal test for {B2D_JOINT_PULLEY_DEFINITION}.local_anchor_1
		note
			testing:  "covers/{B2D_JOINT_PULLEY_DEFINITION}.local_anchor_1",
					  "covers/{B2D_JOINT_PULLEY_DEFINITION}.set_local_anchor_1",
					  "covers/{B2D_JOINT_PULLEY_DEFINITION}.set_local_anchor_1_with_world_point"
		do
			if attached definition as la_definition then
				la_definition.set_local_anchor_1 (create {B2D_VECTOR_2D}.make_with_coordinates (1.2, -5.65))
				assert ("set_local_anchor_1 valid x", la_definition.local_anchor_1.x ~ 1.2)
				assert ("set_local_anchor_1 valid y", la_definition.local_anchor_1.y ~ -5.65)
				la_definition.set_local_anchor_1_with_world_point (create {B2D_VECTOR_2D}.make_with_coordinates (-10.456, 6.123))
				assert ("set_local_anchor_1_with_world_point valid x", la_definition.local_anchor_1.x ~ -10.456)
				assert ("set_local_anchor_1_with_world_point valid y", la_definition.local_anchor_1.y ~ 6.123)
			end
		end

	test_local_anchor_2_normal
			-- Normal test for {B2D_JOINT_PULLEY_DEFINITION}.local_anchor_2
		note
			testing:  "covers/{B2D_JOINT_PULLEY_DEFINITION}.local_anchor_2",
					  "covers/{B2D_JOINT_PULLEY_DEFINITION}.set_local_anchor_2",
					  "covers/{B2D_JOINT_PULLEY_DEFINITION}.set_local_anchor_2_with_world_point"
		do
			if attached definition as la_definition then
				la_definition.set_local_anchor_2 (create {B2D_VECTOR_2D}.make_with_coordinates (1.2, -5.65))
				assert ("set_local_anchor_2 valid x", la_definition.local_anchor_2.x ~ 1.2)
				assert ("set_local_anchor_2 valid y", la_definition.local_anchor_2.y ~ -5.65)
				la_definition.set_local_anchor_2_with_world_point (create {B2D_VECTOR_2D}.make_with_coordinates (15.5, 5.5))
				assert ("set_local_anchor_2_with_world_point valid x", la_definition.local_anchor_2.x ~ 5.5)
				assert ("set_local_anchor_2_with_world_point valid y", la_definition.local_anchor_2.y ~ -4.5)
			end
		end

	test_segment_length_1_normal
			-- Normal test for {B2D_JOINT_PULLEY_DEFINITION}.segment_length_1
		note
			testing:  "covers/{B2D_JOINT_PULLEY_DEFINITION}.segment_length_1"
		do
			if attached definition as la_definition then
				assert ("segment_length_1 valid", la_definition.segment_length_1 ~ 15)
				la_definition.set_local_anchor_1_with_world_point (create {B2D_VECTOR_2D}.make_with_coordinates (0, 2))
				la_definition.set_ground_anchor_1 (create {B2D_VECTOR_2D}.make_with_coordinates (0, -5))
				assert ("segment_length_1 valid", la_definition.segment_length_1 ~ 7)
			end
		end

	test_segment_length_2_normal
			-- Normal test for {B2D_JOINT_PULLEY_DEFINITION}.segment_length_2
		note
			testing:  "covers/{B2D_JOINT_PULLEY_DEFINITION}.segment_length_2"
		do
			if attached definition as la_definition then
				assert ("segment_length_2 valid", la_definition.segment_length_2 ~ 5)
				la_definition.set_local_anchor_2_with_world_point (create {B2D_VECTOR_2D}.make_with_coordinates (10, -6))
				la_definition.set_ground_anchor_2 (create {B2D_VECTOR_2D}.make_with_coordinates (10, -2))
				assert ("segment_length_2 valid", la_definition.segment_length_2 ~ 4)
			end
		end

	test_ratio_normal
			-- Normal test for {B2D_JOINT_PULLEY_DEFINITION}.ratio
		note
			testing:  "covers/{B2D_JOINT_PULLEY_DEFINITION}.ratio",
					  "covers/{B2D_JOINT_PULLEY_DEFINITION}.set_ratio"
		do
			if attached definition as la_definition then
				la_definition.set_ratio (0.5)
				assert ("set_ratio valid", la_definition.ratio ~ 0.5)
			end
		end

feature -- Limit test routines

	test_set_ratio_limit
			-- Limit test for {B2D_JOINT_PULLEY_DEFINITION}.set_ratio
		note
			testing:  "covers/{B2D_JOINT_PULLEY_DEFINITION}.ratio",
					  "covers/{B2D_JOINT_PULLEY_DEFINITION}.set_ratio"
		do
			if attached definition as la_definition then
				la_definition.set_ratio ({REAL_32}.epsilon)
				assert ("set_ratio valid", la_definition.ratio ~ {REAL_32}.epsilon)
			end
		end

feature -- Wrong test routines

	test_ground_anchor_1_wrong
			-- Test for {B2D_JOINT_PULLEY_DEFINITION}.ground_anchor_1 when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PULLEY_DEFINITION}.ground_anchor_1"
		local
			l_retry:BOOLEAN
			l_discard:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached definition as la_definition then
					la_definition.put_null
					l_discard := la_definition.ground_anchor_1
					assert ("ground_anchor_1 valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_ground_anchor_1_wrong
			-- Test for {B2D_JOINT_PULLEY_DEFINITION}.set_ground_anchor_1 when arguments do not exist.
		note
			testing:  "covers/{B2D_JOINT_PULLEY_DEFINITION}.set_ground_anchor_1"
		local
			l_retry:INTEGER_32
			l_point:B2D_VECTOR_2D
		do
			if attached definition as la_definition then
				if l_retry ~ 0 then
					la_definition.put_null
					la_definition.set_ground_anchor_1 (create {B2D_VECTOR_2D})
					assert ("Exists", False)
				elseif l_retry ~ 1 then
					create l_point
					l_point.put_null
					la_definition.set_ground_anchor_1 (l_point)
					assert ("Vector_Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := l_retry + 1
				retry
			end
		end

	test_ground_anchor_2_wrong
			-- Test for {B2D_JOINT_PULLEY_DEFINITION}.ground_anchor_2 when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PULLEY_DEFINITION}.ground_anchor_2"
		local
			l_retry:BOOLEAN
			l_discard:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached definition as la_definition then
					la_definition.put_null
					l_discard := la_definition.ground_anchor_2
					assert ("ground_anchor_2 valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_ground_anchor_2_wrong
			-- Test for {B2D_JOINT_PULLEY_DEFINITION}.set_ground_anchor_2 when arguments do not exist.
		note
			testing:  "covers/{B2D_JOINT_PULLEY_DEFINITION}.set_ground_anchor_1"
		local
			l_retry:INTEGER_32
			l_point:B2D_VECTOR_2D
		do
			if attached definition as la_definition then
				if l_retry ~ 0 then
					la_definition.put_null
					la_definition.set_ground_anchor_2 (create {B2D_VECTOR_2D})
					assert ("Exists", False)
				elseif l_retry ~ 1 then
					create l_point
					l_point.put_null
					la_definition.set_ground_anchor_2 (l_point)
					assert ("Vector_Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := l_retry + 1
				retry
			end
		end

	test_local_anchor_1_wrong
			-- Test for {B2D_JOINT_PULLEY_DEFINITION}.local_anchor_1 when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PULLEY_DEFINITION}.local_anchor_1"
		local
			l_retry:BOOLEAN
			l_discard:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached definition as la_definition then
					la_definition.put_null
					l_discard := la_definition.local_anchor_1
					assert ("local_anchor_1 valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_local_anchor_1_wrong
			-- Test for {B2D_JOINT_PULLEY_DEFINITION}.set_local_anchor_1 when arguments do not exist.
		note
			testing:  "covers/{B2D_JOINT_PULLEY_DEFINITION}.set_local_anchor_1"
		local
			l_retry:INTEGER_32
			l_point:B2D_VECTOR_2D
		do
			if attached definition as la_definition then
				if l_retry ~ 0 then
					la_definition.put_null
					la_definition.set_local_anchor_1 (create {B2D_VECTOR_2D})
					assert ("Exists", False)
				elseif l_retry ~ 1 then
					create l_point
					l_point.put_null
					la_definition.set_local_anchor_1 (l_point)
					assert ("Vector_Valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := l_retry + 1
				retry
			end
		end

	test_set_local_anchor_1_with_world_point_wrong
			-- Test for {B2D_JOINT_PULLEY_DEFINITION}.set_local_anchor_1_with_world_point
			-- when arguments do not exist.
		note
			testing:  "covers/{B2D_JOINT_PULLEY_DEFINITION}.set_local_anchor_1_with_world_point"
		local
			l_retry:INTEGER_32
			l_point:B2D_VECTOR_2D
		do
			if attached definition as la_definition then
				if l_retry ~ 0 then
					la_definition.put_null
					la_definition.set_local_anchor_1_with_world_point (create {B2D_VECTOR_2D})
					assert ("Exists", False)
				elseif l_retry ~ 1 then
					create l_point
					l_point.put_null
					la_definition.set_local_anchor_1_with_world_point (l_point)
					assert ("Vector_Valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := l_retry + 1
				retry
			end
		end

	test_local_anchor_2_wrong
			-- Test for {B2D_JOINT_PULLEY_DEFINITION}.local_anchor_2 when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PULLEY_DEFINITION}.local_anchor_2"
		local
			l_retry:BOOLEAN
			l_discard:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached definition as la_definition then
					la_definition.put_null
					l_discard := la_definition.local_anchor_2
					assert ("local_anchor_2 valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_local_anchor_2_wrong
			-- Test for {B2D_JOINT_PULLEY_DEFINITION}.set_local_anchor_2 when arguments do not exist.
		note
			testing:  "covers/{B2D_JOINT_PULLEY_DEFINITION}.set_local_anchor_2"
		local
			l_retry:INTEGER_32
			l_point:B2D_VECTOR_2D
		do
			if attached definition as la_definition then
				if l_retry ~ 0 then
					la_definition.put_null
					la_definition.set_local_anchor_2 (create {B2D_VECTOR_2D})
					assert ("Exists", False)
				elseif l_retry ~ 1 then
					create l_point
					l_point.put_null
					la_definition.set_local_anchor_2 (l_point)
					assert ("Vector_Valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := l_retry + 1
				retry
			end
		end

	test_set_local_anchor_2_with_world_point_wrong
			-- Test for {B2D_JOINT_PULLEY_DEFINITION}.set_local_anchor_2_with_world_point
			-- when arguments do not exist.
		note
			testing:  "covers/{B2D_JOINT_PULLEY_DEFINITION}.set_local_anchor_1_with_world_point"
		local
			l_retry:INTEGER_32
			l_point:B2D_VECTOR_2D
		do
			if attached definition as la_definition then
				if l_retry ~ 0 then
					la_definition.put_null
					la_definition.set_local_anchor_2_with_world_point (create {B2D_VECTOR_2D})
					assert ("Exists", False)
				elseif l_retry ~ 1 then
					create l_point
					l_point.put_null
					la_definition.set_local_anchor_2_with_world_point (l_point)
					assert ("Vector_Valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := l_retry + 1
				retry
			end
		end

	test_segment_length_1_wrong
			-- Test for {B2D_JOINT_PULLEY_DEFINITION}.segment_length_1 when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PULLEY_DEFINITION}.segment_length_1"
		local
			l_retry:BOOLEAN
			l_discard:REAL_32
		do
			if not l_retry then
				if attached definition as la_definition then
					la_definition.put_null
					l_discard := la_definition.segment_length_1
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_segment_length_2_wrong
			-- Test for {B2D_JOINT_PULLEY_DEFINITION}.segment_length_2 when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PULLEY_DEFINITION}.segment_length_2"
		local
			l_retry:BOOLEAN
			l_discard:REAL_32
		do
			if not l_retry then
				if attached definition as la_definition then
					la_definition.put_null
					l_discard := la_definition.segment_length_2
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_ratio_wrong
			-- Test for {B2D_JOINT_PULLEY_DEFINITION}.ratio when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PULLEY_DEFINITION}.ratio"
		local
			l_retry:BOOLEAN
			l_discard:REAL_32
		do
			if not l_retry then
				if attached definition as la_definition then
					la_definition.put_null
					l_discard := la_definition.ratio
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_ratio_wrong_1
			-- Test for {B2D_JOINT_PULLEY_DEFINITION}.set_ratio when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PULLEY_DEFINITION}.set_ratio"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				if attached definition as la_definition then
					la_definition.put_null
					la_definition.set_ratio (1)
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_ratio_wrong_2
			-- Test for {B2D_JOINT_PULLEY_DEFINITION}.set_ratio when argument is not positive.
		note
			testing:  "covers/{B2D_JOINT_PULLEY_DEFINITION}."
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				if attached definition as la_definition then
					la_definition.set_ratio (0)
					assert ("Positive", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

end


