note
	description: "Tests for {B2D_WORLD} (everything but callbacks)"
	author: "Louis Marchand"
	date: "Thu, 11 Jun 2020 20:29:27 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_WORLD_TESTS

inherit
	EQA_TEST_SET
	B2D_ANY
		undefine
			default_create
		end

feature -- Test routines

	test_make_normal
			-- Normal test for {B2D_WORLD}.make
		note
			testing:  "covers/{B2D_WORLD}.make", "covers/{B2D_WORLD}.gravity"
		local
			l_world:B2D_WORLD
		do
			create l_world.make (-1.6563, -0.44497)
			assert ("Make Valid X", l_world.gravity.x ~ -1.6563)
			assert ("Make Valid Y", l_world.gravity.y ~ -0.44497)
		end

	test_make_from_vector_normal
			-- Normal test for {B2D_WORLD}.make_from_vector
		note
			testing:  "covers/{B2D_WORLD}.make_from_vector", "covers/{B2D_WORLD}.gravity"
		local
			l_vector:B2D_VECTOR_2D
			l_world:B2D_WORLD
		do
			create l_vector.make_with_coordinates (4.23718, -8.09874)
			create l_world.make_from_vector (l_vector)
			assert ("make_from_vector Valid", l_world.gravity ~ l_vector)
		end

	test_set_gravity_normal
			-- Normal test for {B2D_WORLD}.set_gravity
		note
			testing:  "covers/{B2D_WORLD}.set_gravity", "covers/{B2D_WORLD}.gravity"
		local
			l_world:B2D_WORLD
		do
			create l_world.make (-1.6563, -0.44497)
			l_world.set_gravity (5.85327, -6.86926)
			assert ("set_gravity Valid X", l_world.gravity.x ~ 5.85327)
			assert ("set_gravity Valid Y", l_world.gravity.y ~ -6.86926)
		end

	test_set_gravity_from_vector_normal
			-- Normal test for {B2D_WORLD}.set_gravity_from_vector
		note
			testing:  "covers/{B2D_WORLD}.set_gravity_from_vector", "covers/{B2D_WORLD}.gravity"
		local
			l_vector:B2D_VECTOR_2D
			l_world:B2D_WORLD
		do
			create l_vector.make_with_coordinates (6.28434, 1.52783)
			create l_world.make (-1.6563, -0.44497)
			l_world.set_gravity_from_vector (l_vector)
			assert ("set_gravity_from_vector Valid", l_world.gravity ~ l_vector)
		end

	test_create_body_normal
			-- Normal test for {B2D_WORLD}.create_body
		note
			testing:  "covers/{B2D_WORLD}.create_body",
					  "covers/{B2D_WORLD}.bodies",
					  "covers/{B2D_WORLD}.body_count",
					  "covers/{B2D_WORLD}.last_body"
		local
			l_world:B2D_WORLD
			l_body_def:B2D_BODY_DEFINITION
		do
			create l_world.make (0, 10)
			create l_body_def.make_dynamic
			l_world.create_body (l_body_def)
			assert ("create_body valid: count + 1", l_world.body_count ~ 1)
			assert ("create_body valid: bodies.count + 1", l_world.bodies.count ~ 1)
			assert ("create_body valid: last_body", attached l_world.last_body)
			l_world.create_body (l_body_def)
			assert ("create_body valid: count + 1", l_world.body_count ~ 2)
			assert ("create_body valid: bodies.count + 1", l_world.bodies.count ~ 2)
			assert ("create_body valid: last_body", attached l_world.last_body)
		end

	test_destroy_body_normal
			-- Normal test for {B2D_WORLD}.destroy_body
		note
			testing:  "covers/{B2D_WORLD}.create_body",
					  "covers/{B2D_WORLD}.destroy_body",
					  "covers/{B2D_WORLD}.bodies",
					  "covers/{B2D_WORLD}.body_count",
					  "covers/{B2D_WORLD}.last_body"
		local
			l_world:B2D_WORLD
			l_body_def:B2D_BODY_DEFINITION
		do
			create l_world.make (0, 10)
			create l_body_def.make_dynamic
			l_world.create_body (l_body_def)
			if attached l_world.last_body as la_body_1 then
				l_world.create_body (l_body_def)
				if attached l_world.last_body as la_body_2 and then la_body_1 /= la_body_2 then
					l_world.destroy_body (la_body_1)
					assert ("destroy_body valid: count - 1", l_world.body_count ~ 1)
					assert ("destroy_body valid: bodies.count - 1", l_world.bodies.count ~ 1)
					assert ("destroy_body valid: body.exists", not la_body_1.exists)
					l_world.destroy_body (la_body_2)
					assert ("destroy_body valid: count - 1", l_world.body_count ~ 0)
					assert ("destroy_body valid: bodies.count - 1", l_world.bodies.count ~ 0)
					assert ("destroy_body valid: body.exists", not la_body_2.exists)
				else
					assert ("Cannot create body.", False)
				end
			else
				assert ("Cannot create body.", False)
			end
		end

	test_create_joint_normal
			-- Normal test for {B2D_WORLD}.create_joint
		note
			testing:  "covers/{B2D_WORLD}.create_joint", "covers/{B2D_WORLD}.joints", "covers/{B2D_WORLD}.joint_count"
		local
			l_world:B2D_WORLD
			l_joint_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_body_definition:B2D_BODY_DEFINITION
		do
			create l_world.make (0, -10)
			create l_body_definition.make_dynamic
			l_world.create_body (l_body_definition)
			l_world.create_body (l_body_definition)
			create l_joint_definition.make (l_world.bodies.at (1), l_world.bodies.at (2), 5, 10, -5, -10)
			l_world.create_joint (l_joint_definition)
			assert ("create_joint valid: count + 1", l_world.joint_count ~ 1)
			assert ("create_joint valid: bodies.count + 1", l_world.joints.count ~ 1)
			assert ("create_joint valid: last_body", attached l_world.last_joint)
			l_world.create_joint (l_joint_definition)
			assert ("create_joint valid: count + 1", l_world.joint_count ~ 2)
			assert ("create_joint valid: bodies.count + 1", l_world.joints.count ~ 2)
			assert ("create_joint valid: last_body", attached l_world.last_joint)
		end

	test_destroy_joint_normal
			-- Normal test for {B2D_WORLD}.destroy_joint
		note
			testing:  "covers/{B2D_WORLD}.destroy_joint", "covers/{B2D_WORLD}.create_joint", "covers/{B2D_WORLD}.joints", "covers/{B2D_WORLD}.joint_count"
		local
			l_world:B2D_WORLD
			l_joint_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_body_definition:B2D_BODY_DEFINITION
		do
			create l_world.make (0, -10)
			create l_body_definition.make_dynamic
			l_world.create_body (l_body_definition)
			l_world.create_body (l_body_definition)
			create l_joint_definition.make (l_world.bodies.at (1), l_world.bodies.at (2), 5, 10, -5, -10)
			l_world.create_joint (l_joint_definition)
			if attached l_world.last_joint as la_joint_1 then
				l_world.create_joint (l_joint_definition)
				if attached l_world.last_joint as la_joint_2 then
					l_world.destroy_joint (la_joint_1)
					assert ("destroy_joint valid: count + 1", l_world.joint_count ~ 1)
					assert ("destroy_joint valid: bodies.count + 1", l_world.joints.count ~ 1)
					assert ("destroy_joint valid: joint.exists", not la_joint_1.exists)
					l_world.destroy_joint (la_joint_2)
					assert ("destroy_joint valid: count + 1", l_world.joint_count ~ 0)
					assert ("destroy_joint valid: bodies.count + 1", l_world.joints.count ~ 0)
					assert ("destroy_joint valid: joint.exists", not la_joint_2.exists)
				else
					assert("Cannot create joints.", False)
				end
			else
				assert("Cannot create joints.", False)
			end
		end

	test_contacts_normal
			-- Normal test for {B2D_WORLD}.contacts
		note
			testing:  "covers/{B2D_WORLD}.contacts", "covers/{B2D_WORLD}.contacts_count"
		local
			l_edge:B2D_SHAPE_EDGE
			l_circle:B2D_SHAPE_CIRCLE
			l_world:B2D_WORLD
			l_body_definition:B2D_BODY_DEFINITION
			l_body_1, l_body_2:B2D_BODY
		do
			create l_edge.make (-10, 1, 10, 1)
			create l_circle.make_with_coordinates (3, 11, 9.99999)
			create l_world.make (0, -10)
			create l_body_definition.make_static
			l_world.create_body (l_body_definition)
			l_body_1 := l_world.bodies.at (1)
			l_body_1.create_fixture_from_shape (l_edge, 0)
			create l_body_definition.make_dynamic
			l_world.create_body (l_body_definition)
			l_body_2 := l_world.bodies.at (2)
			l_body_2.create_fixture_from_shape (l_circle, 10)
			l_world.step
			assert("contact_count valid.", l_world.contact_count ~ 1)
			assert("contacts.count valid.", l_world.contacts.count ~ 1)
		end

	test_step_time_normal
			-- Normal test for {B2D_WORLD}.step_time
		note
			testing:  "covers/{B2D_WORLD}.step_time", "covers/{B2D_WORLD}.set_step_time"
		local
			l_world:B2D_WORLD
		do
			create l_world.make (-1.6563, -0.44497)
			l_world.set_step_time (-5.3491)
			l_world.set_step_time (1.2395)
			assert ("set_step_time Valid", l_world.step_time ~ 1.2395)
		end

	test_step_position_normal
			-- Normal test for {B2D_WORLD}.step_position
		note
			testing:  "covers/{B2D_WORLD}.step_position", "covers/{B2D_WORLD}.set_step_position"
		local
			l_world:B2D_WORLD
		do
			create l_world.make (-1.6563, -0.44497)
			l_world.set_step_position (-5)
			l_world.set_step_position (4)
			assert ("set_step_position Valid", l_world.step_position ~ 4)
		end

	test_step_velocity_normal
			-- Normal test for {B2D_WORLD}.step_velocity
		note
			testing:  "covers/{B2D_WORLD}.step_velocity", "covers/{B2D_WORLD}.set_step_velocity"
		local
			l_world:B2D_WORLD
		do
			create l_world.make (-1.6563, -0.44497)
			l_world.set_step_velocity (71)
			l_world.set_step_velocity (-926)
			assert ("set_step_velocity Valid", l_world.step_velocity ~ -926)
		end

	test_step_normal
			-- Normal test for {B2D_WORLD}.step
		note
			testing:  "covers/{B2D_WORLD}.step",
					  "covers/{B2D_WORLD}.create_body",
					  "covers/{B2D_WORLD}.bodies"
		local
			l_edge:B2D_SHAPE_EDGE
			l_circle:B2D_SHAPE_CIRCLE
			l_world:B2D_WORLD
			l_body_definition:B2D_BODY_DEFINITION
			l_body_1, l_body_2:B2D_BODY
		do
			create l_edge.make (-10, 1, 10, 1)
			create l_circle.make_with_coordinates (3, 11, 9.99999)
			create l_world.make (-10, -10)
			create l_body_definition.make_static
			l_body_definition.set_position (0, 1)
			l_world.create_body (l_body_definition)
			l_body_1 := l_world.bodies.at (1)
			l_body_1.create_fixture_from_shape (l_edge, 0)
			create l_body_definition.make_dynamic
			l_body_definition.set_position (3, 11)
			l_world.create_body (l_body_definition)
			l_body_2 := l_world.bodies.at (2)
			l_body_2.create_fixture_from_shape (l_circle, 10)
			l_world.step
			assert("step valid: l_body_1.position.x", l_body_1.position.x ~ 0)
			assert("step valid: l_body_1.position.y", l_body_1.position.y ~ 1)
			assert("step valid: l_body_2.position.x", l_body_2.position.x < 3)
			assert("step valid: l_body_2.position.y", l_body_2.position.y < 11)
		end

	test_is_auto_clear_forces_normal
			-- Normal test for {B2D_WORLD}.is_auto_clear_forces
		note
			testing:  "covers/{B2D_WORLD}.is_auto_clear_forces", "covers/{B2D_WORLD}.set_is_auto_clear_forces", "covers/{B2D_WORLD}.enable_auto_clear_forces", "covers/{B2D_WORLD}.disable_auto_clear_forces"
		local
			l_world:B2D_WORLD
		do
			create l_world.make (-1.6563, -0.44497)
			l_world.set_is_auto_clear_forces(False)
			l_world.enable_auto_clear_forces
			assert ("enable_auto_clear_forces Valid", l_world.is_auto_clear_forces)
			l_world.disable_auto_clear_forces
			assert ("disable_auto_clear_forces Valid", not l_world.is_auto_clear_forces)
			l_world.set_is_auto_clear_forces(True)
			assert ("set_is_auto_clear_forces Valid", l_world.is_auto_clear_forces)
		end

	test_clear_forces_normal
			-- Normal test for {B2D_WORLD}.clear_forces
		note
			testing:  "covers/{B2D_WORLD}.clear_forces",
					  "covers/{B2D_WORLD}.create_body",
					  "covers/{B2D_WORLD}.bodies",
					  "covers/{B2D_WORLD}.step"
		local
			l_world:B2D_WORLD
			l_body_definition:B2D_BODY_DEFINITION
			l_body:B2D_BODY
		do
			create l_world.make (0, -10)
			create l_body_definition
			l_world.create_body (l_body_definition)
			l_body := l_world.bodies.at (1)
			l_body.apply_force_to_center (0, -100)
			l_world.clear_forces
			l_world.step
			assert ("clear_forces valid", l_body.position.y > -0.01)
		end

	test_allow_sleeping_normal
			-- Normal test for {B2D_WORLD}.allow_sleeping
		note
			testing:  "covers/{B2D_WORLD}.allow_sleeping", "covers/{B2D_WORLD}.set_allow_sleeping", "covers/{B2D_WORLD}.enable_sleeping", "covers/{B2D_WORLD}.disable_sleeping"
		local
			l_world:B2D_WORLD
		do
			create l_world.make (-1.6563, -0.44497)
			l_world.set_allow_sleeping(False)
			l_world.enable_sleeping
			assert ("enable_sleeping Valid", l_world.allow_sleeping)
			l_world.disable_sleeping
			assert ("disable_sleeping Valid", not l_world.allow_sleeping)
			l_world.set_allow_sleeping(True)
			assert ("set_allow_sleeping Valid", l_world.allow_sleeping)
		end

	test_warm_starting_normal
			-- Normal test for {B2D_WORLD}.warm_starting
		note
			testing:  "covers/{B2D_WORLD}.warm_starting", "covers/{B2D_WORLD}.set_warm_starting", "covers/{B2D_WORLD}.enable_warm_starting", "covers/{B2D_WORLD}.disable_warm_starting"
		local
			l_world:B2D_WORLD
		do
			create l_world.make (-1.6563, -0.44497)
			l_world.set_can_warm_starting(False)
			l_world.enable_warm_starting
			assert ("enable_warm_starting Valid", l_world.can_warm_starting)
			l_world.disable_warm_starting
			assert ("disable_warm_starting Valid", not l_world.can_warm_starting)
			l_world.set_can_warm_starting(True)
			assert ("set_can_warm_starting Valid", l_world.can_warm_starting)
		end

	test_continuous_physics_normal
			-- Normal test for {B2D_WORLD}.is_continuous_physics
		note
			testing:  "covers/{B2D_WORLD}.is_continuous_physics", "covers/{B2D_WORLD}.set_is_continuous_physics", "covers/{B2D_WORLD}.enable_continuous_physics", "covers/{B2D_WORLD}.disable_continuous_physics"
		local
			l_world:B2D_WORLD
		do
			create l_world.make (-1.6563, -0.44497)
			l_world.set_is_continuous_physics(False)
			l_world.enable_continuous_physics
			assert ("enable_continuous_physics Valid", l_world.is_continuous_physics)
			l_world.disable_continuous_physics
			assert ("disable_continuous_physics Valid", not l_world.is_continuous_physics)
			l_world.set_is_continuous_physics(True)
			assert ("set_is_continuous_physics Valid", l_world.is_continuous_physics)
		end

	test_sub_stepping_normal
			-- Normal test for {B2D_WORLD}.is_sub_stepping
		note
			testing:  "covers/{B2D_WORLD}.is_sub_stepping", "covers/{B2D_WORLD}.set_is_sub_stepping", "covers/{B2D_WORLD}.enable_sub_stepping", "covers/{B2D_WORLD}.disable_sub_stepping"
		local
			l_world:B2D_WORLD
		do
			create l_world.make (-1.6563, -0.44497)
			l_world.set_is_sub_stepping(False)
			l_world.enable_sub_stepping
			assert ("enable_sub_stepping Valid", l_world.is_sub_stepping)
			l_world.disable_sub_stepping
			assert ("disable_sub_stepping Valid", not l_world.is_sub_stepping)
			l_world.set_is_sub_stepping(True)
			assert ("set_is_sub_stepping Valid", l_world.is_sub_stepping)
		end

	test_proxy_count_normal
			-- Normal test for {B2D_WORLD}.proxy_count
		note
			testing:  "covers/{B2D_WORLD}.proxy_count",
					  "covers/{B2D_WORLD}.create_body",
					  "covers/{B2D_WORLD}.bodies"
		local
			l_edge:B2D_SHAPE_EDGE
			l_circle:B2D_SHAPE_CIRCLE
			l_world:B2D_WORLD
			l_body_definition:B2D_BODY_DEFINITION
			l_body_1, l_body_2:B2D_BODY
		do
			create l_edge.make (-10, 1, 10, 1)
			create l_circle.make_with_coordinates (3, 11, 9.99999)
			create l_world.make (0, -10)
			create l_body_definition.make_static
			l_body_definition.set_position (0, 1)
			l_world.create_body (l_body_definition)
			l_body_1 := l_world.bodies.at (1)
			l_body_1.create_fixture_from_shape (l_edge, 0)
			create l_body_definition.make_dynamic
			l_body_definition.set_position (3, 11)
			l_world.create_body (l_body_definition)
			l_body_2 := l_world.bodies.at (2)
			l_body_2.create_fixture_from_shape (l_circle, 10)
			assert("proxy_count valid.", l_world.proxy_count ~ 2)
		end

	test_tree_height_normal
			-- Normal test for {B2D_WORLD}.tree_height
		note
			testing:  "covers/{B2D_WORLD}.tree_height",
					  "covers/{B2D_WORLD}.create_body",
					  "covers/{B2D_WORLD}.bodies"
		local
			l_edge:B2D_SHAPE_EDGE
			l_circle:B2D_SHAPE_CIRCLE
			l_world:B2D_WORLD
			l_body_definition:B2D_BODY_DEFINITION
			l_body_1, l_body_2:B2D_BODY
		do
			create l_edge.make (-10, 1, 10, 1)
			create l_circle.make_with_coordinates (3, 11, 9.99999)
			create l_world.make (0, -10)
			create l_body_definition.make_static
			l_body_definition.set_position (0, 1)
			l_world.create_body (l_body_definition)
			l_body_1 := l_world.bodies.at (1)
			l_body_1.create_fixture_from_shape (l_edge, 0)
			create l_body_definition.make_dynamic
			l_body_definition.set_position (3, 11)
			l_world.create_body (l_body_definition)
			l_body_2 := l_world.bodies.at (2)
			l_body_2.create_fixture_from_shape (l_circle, 10)
			assert("tree_height valid.", l_world.tree_height ~ 1)
		end

	test_tree_balance_normal
			-- Normal test for {B2D_WORLD}.tree_balance
		note
			testing:  "covers/{B2D_WORLD}.tree_balance",
					  "covers/{B2D_WORLD}.create_body",
					  "covers/{B2D_WORLD}.bodies"
		local
			l_edge:B2D_SHAPE_EDGE
			l_circle:B2D_SHAPE_CIRCLE
			l_world:B2D_WORLD
			l_body_definition:B2D_BODY_DEFINITION
			l_body_1, l_body_2:B2D_BODY
		do
			create l_edge.make (-10, 1, 10, 1)
			create l_circle.make_with_coordinates (3, 11, 9.99999)
			create l_world.make (0, -10)
			create l_body_definition.make_static
			l_body_definition.set_position (0, 1)
			l_world.create_body (l_body_definition)
			l_body_1 := l_world.bodies.at (1)
			l_body_1.create_fixture_from_shape (l_edge, 0)
			create l_body_definition.make_dynamic
			l_body_definition.set_position (3, 11)
			l_world.create_body (l_body_definition)
			l_body_2 := l_world.bodies.at (2)
			l_body_2.create_fixture_from_shape (l_circle, 10)
			assert("tree_balance valid.", l_world.tree_balance ~ 0)
		end

	test_tree_quality_normal
			-- Normal test for {B2D_WORLD}.tree_quality
		note
			testing:  "covers/{B2D_WORLD}.tree_quality",
					  "covers/{B2D_WORLD}.create_body",
					  "covers/{B2D_WORLD}.bodies"
		local
			l_edge:B2D_SHAPE_EDGE
			l_circle:B2D_SHAPE_CIRCLE
			l_world:B2D_WORLD
			l_body_definition:B2D_BODY_DEFINITION
			l_body_1, l_body_2:B2D_BODY
		do
			create l_edge.make (-10, 1, 10, 1)
			create l_circle.make_with_coordinates (3, 11, 9.99999)
			create l_world.make (0, -10)
			create l_body_definition.make_static
			l_body_definition.set_position (0, 1)
			l_world.create_body (l_body_definition)
			l_body_1 := l_world.bodies.at (1)
			l_body_1.create_fixture_from_shape (l_edge, 0)
			create l_body_definition.make_dynamic
			l_body_definition.set_position (3, 11)
			l_world.create_body (l_body_definition)
			l_body_2 := l_world.bodies.at (2)
			l_body_2.create_fixture_from_shape (l_circle, 10)
			assert("tree_quality valid.", l_world.tree_quality > 2.07834 and l_world.tree_quality < 2.1)
		end

	test_move_origin_normal
			-- Normal test for {B2D_WORLD}.move_origin
		note
			testing:  "covers/{B2D_WORLD}.move_origin",
					  "covers/{B2D_WORLD}.create_body",
					  "covers/{B2D_WORLD}.bodies",
					  "covers/{B2D_WORLD}.step"
		local
			l_world:B2D_WORLD
			l_body_definition:B2D_BODY_DEFINITION
			l_body:B2D_BODY
		do
			create l_world.make (0, -10)
			create l_body_definition
			l_body_definition.set_position (1, 2)
			l_world.create_body (l_body_definition)
			l_body := l_world.bodies.at (1)
			l_world.move_origin (10, 10)
			assert ("move_origin valid X", l_body.position.x ~ -9)
			assert ("move_origin valid Y", l_body.position.y ~ -8)
		end

	test_move_origin_with_vector_normal
			-- Normal test for {B2D_WORLD}.move_origin_with_vector
		note
			testing:  "covers/{B2D_WORLD}.move_origin_with_vector",
					  "covers/{B2D_WORLD}.create_body",
					  "covers/{B2D_WORLD}.bodies",
					  "covers/{B2D_WORLD}.step"
		local
			l_world:B2D_WORLD
			l_body_definition:B2D_BODY_DEFINITION
			l_body:B2D_BODY
			l_vector:B2D_VECTOR_2D
		do
			create l_world.make (0, -10)
			create l_body_definition
			l_body_definition.set_position (1, 2)
			l_world.create_body (l_body_definition)
			l_body := l_world.bodies.at (1)
			create l_vector.make_with_coordinates (10, 10)
			l_world.move_origin_with_vector (l_vector)
			assert ("move_origin valid X", l_body.position.x ~ -9)
			assert ("move_origin valid Y", l_body.position.y ~ -8)
		end

	test_create_body_wrong_1
			-- Wrong test 1 for create_body
		note
			testing:  "covers/{B2D_WORLD}.create_body",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_world.create_body (create {B2D_BODY_DEFINITION})
				assert("create_body Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_create_body_wrong_2
			-- Wrong test 2 for create_body
		note
			testing:  "covers/{B2D_WORLD}.create_body",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
			l_body_definition:B2D_BODY_DEFINITION
		do
			if not l_retry then
				create l_world.make (0, 0)
				create l_body_definition
				l_body_definition.put_null
				l_world.create_body (l_body_definition)
				assert("create_body Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end
	test_destroy_body_wrong_1
			-- Wrong test 1 for destroy_body
		note
			testing:  "covers/{B2D_WORLD}.destroy_body",
					  "covers/{B2D_WORLD}.create_body",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.create_body (create {B2D_BODY_DEFINITION})
				l_world.put_null
				l_world.destroy_body (l_world.bodies.at (1))
				assert("create_body Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_destroy_body_wrong_2
			-- Wrong test 2 for destroy_body
		note
			testing:  "covers/{B2D_WORLD}.destroy_body",
					  "covers/{B2D_WORLD}.bodies",
					  "covers/{B2D_WORLD}.create_body",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.create_body (create {B2D_BODY_DEFINITION})
				l_world.bodies.at (1).put_null
				l_world.destroy_body (l_world.bodies.at (1))
				assert("create_body Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_create_joint_wrong_1
			-- Wrong test 1 for create_joint
		note
			testing:  "covers/{B2D_WORLD}.create_joint",
					  "covers/{B2D_WORLD}.create_body",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
			l_joint_definition:B2D_JOINT_DISTANCE_DEFINITION
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.create_body (create {B2D_BODY_DEFINITION})
				l_world.create_body (create {B2D_BODY_DEFINITION})
				create l_joint_definition.make (l_world.bodies.at (1), l_world.bodies.at (2), 0, 0, 0, 0)
				l_world.put_null
				l_world.create_joint (l_joint_definition)
				assert("create_joint Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_create_joint_wrong_2
			-- Wrong test 2 for create_joint
		note
			testing:  "covers/{B2D_WORLD}.create_joint",
					  "covers/{B2D_WORLD}.make",
					  "covers/{B2D_WORLD}.create_body",
					  "covers/{B2D_WORLD}.create_joint"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
			l_joint_definition:B2D_JOINT_DISTANCE_DEFINITION
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.create_body (create {B2D_BODY_DEFINITION})
				l_world.create_body (create {B2D_BODY_DEFINITION})
				create l_joint_definition.make (l_world.bodies.at (1), l_world.bodies.at (2), 0, 0, 0, 0)
				l_joint_definition.put_null
				l_world.create_joint (l_joint_definition)
				assert("create_joint Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_destroy_joint_wrong_1
			-- Wrong test 1 for destroy_joint
		note
			testing:  "covers/{B2D_WORLD}.destroy_joint",
					  "covers/{B2D_WORLD}.create_body",
					  "covers/{B2D_WORLD}.create_joint",
					  "covers/{B2D_WORLD}.joints",
					  "covers/{B2D_WORLD}.bodies",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
			l_joint_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_joint:B2D_JOINT
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.create_body (create {B2D_BODY_DEFINITION})
				l_world.create_body (create {B2D_BODY_DEFINITION})
				create l_joint_definition.make (l_world.bodies.at (1), l_world.bodies.at (2), 0, 0, 0, 0)
				l_world.create_joint (l_joint_definition)
				l_joint := l_world.joints.at (1)
				l_world.put_null
				l_world.destroy_joint (l_joint)
				assert("destroy_joint Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_destroy_joint_wrong_2
			-- Wrong test 2 for destroy_joint
		note
			testing:  "covers/{B2D_WORLD}.destroy_joint",
					  "covers/{B2D_WORLD}.create_joint",
					  "covers/{B2D_WORLD}.create_body",
					  "covers/{B2D_WORLD}.bodies",
					  "covers/{B2D_WORLD}.joints",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
			l_joint_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_joint:B2D_JOINT
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.create_body (create {B2D_BODY_DEFINITION})
				l_world.create_body (create {B2D_BODY_DEFINITION})
				create l_joint_definition.make (l_world.bodies.at (1), l_world.bodies.at (2), 0, 0, 0, 0)
				l_world.create_joint (l_joint_definition)
				l_joint := l_world.joints.at (1)
				l_joint.put_null
				l_world.destroy_joint (l_joint)
				assert("destroy_joint Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_step_wrong
			-- Wrong test for step
		note
			testing:  "covers/{B2D_WORLD}.step",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_world.step
				assert("step Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_gravity_wrong
			-- Wrong test for set_gravity
		note
			testing:  "covers/{B2D_WORLD}.set_gravity",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_world.set_gravity (0, 10)
				assert("set_gravity Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_gravity_from_vector_wrong
			-- Wrong test for set_gravity_from_vector
		note
			testing:  "covers/{B2D_WORLD}.set_gravity_from_vector",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_world.set_gravity_from_vector (create {B2D_VECTOR_2D})
				assert("set_gravity_from_vector Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_is_locked_wrong
			-- Wrong test for is_locked
		note
			testing:  "covers/{B2D_WORLD}.is_locked",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
			l_result:BOOLEAN
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_result := l_world.is_locked
				assert("is_locked Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_body_count_wrong
			-- Wrong test for body_count
		note
			testing:  "covers/{B2D_WORLD}.body_count",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
			l_result:INTEGER
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_result := l_world.body_count
				assert("body_count Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_bodies_wrong
			-- Wrong test for bodies
		note
			testing:  "covers/{B2D_WORLD}.bodies",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
			l_result:LIST[B2D_BODY]
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_result := l_world.bodies
				assert("bodies Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_joint_count_wrong
			-- Wrong test for joint_count
		note
			testing:  "covers/{B2D_WORLD}.joint_count",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
			l_result:INTEGER
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_result := l_world.joint_count
				assert("joint_count Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_joints_wrong
			-- Wrong test for joints
		note
			testing:  "covers/{B2D_WORLD}.joints",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
			l_result:LIST[B2D_JOINT]
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_result := l_world.joints
				assert("joints Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_contact_count_wrong
			-- Wrong test for contact_count
		note
			testing:  "covers/{B2D_WORLD}.contact_count",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
			l_result:INTEGER
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_result := l_world.contact_count
				assert("contact_count Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_contacts_wrong
			-- Wrong test for contacts
		note
			testing:  "covers/{B2D_WORLD}.contacts",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
			l_result:LIST[B2D_CONTACT]
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_result := l_world.contacts
				assert("contacts Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_is_auto_clear_forces_wrong
			-- Wrong test for is_auto_clear_forces
		note
			testing:  "covers/{B2D_WORLD}.is_auto_clear_forces",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
			l_result:BOOLEAN
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_result := l_world.is_auto_clear_forces
				assert("is_auto_clear_forces Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_is_auto_clear_forces_wrong
			-- Wrong test for set_is_auto_clear_forces
		note
			testing:  "covers/{B2D_WORLD}.set_is_auto_clear_forces",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_world.set_is_auto_clear_forces(True)
				assert("set_is_auto_clear_forces Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_enable_auto_clear_forces_wrong
			-- Wrong test for enable_auto_clear_forces
		note
			testing:  "covers/{B2D_WORLD}.enable_auto_clear_forces",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_world.enable_auto_clear_forces
				assert("enable_auto_clear_forces Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_disable_auto_clear_forces_wrong
			-- Wrong test for disable_auto_clear_forces
		note
			testing:  "covers/{B2D_WORLD}.disable_auto_clear_forces",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_world.disable_auto_clear_forces
				assert("disable_auto_clear_forces Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_clear_forces_wrong
			-- Wrong test for clear_forces
		note
			testing:  "covers/{B2D_WORLD}.clear_forces",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_world.clear_forces
				assert("clear_forces Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_allow_sleeping_wrong
			-- Wrong test for allow_sleeping
		note
			testing:  "covers/{B2D_WORLD}.allow_sleeping",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
			l_result:BOOLEAN
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_result := l_world.allow_sleeping
				assert("allow_sleeping Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_allow_sleeping_wrong
			-- Wrong test for set_allow_sleeping
		note
			testing:  "covers/{B2D_WORLD}.set_allow_sleeping",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_world.set_allow_sleeping(True)
				assert("set_allow_sleeping Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_enable_sleeping_wrong
			-- Wrong test for enable_sleeping
		note
			testing:  "covers/{B2D_WORLD}.enable_sleeping",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_world.enable_sleeping
				assert("enable_sleeping Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_disable_sleeping_wrong
			-- Wrong test for disable_sleeping
		note
			testing:  "covers/{B2D_WORLD}.disable_sleeping",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_world.disable_sleeping
				assert("disable_sleeping Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_can_warm_starting_wrong
			-- Wrong test for can_warm_starting
		note
			testing:  "covers/{B2D_WORLD}.can_warm_starting",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
			l_result:BOOLEAN
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_result := l_world.can_warm_starting
				assert("can_warm_starting Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_can_warm_starting_wrong
			-- Wrong test for set_can_warm_starting
		note
			testing:  "covers/{B2D_WORLD}.set_can_warm_starting",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_world.set_can_warm_starting(True)
				assert("set_can_warm_starting Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_enable_warm_starting_wrong
			-- Wrong test for enable_warm_starting
		note
			testing:  "covers/{B2D_WORLD}.enable_warm_starting",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_world.enable_warm_starting
				assert("enable_warm_starting Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_disable_warm_starting_wrong
			-- Wrong test for disable_warm_starting
		note
			testing:  "covers/{B2D_WORLD}.disable_warm_starting",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_world.disable_warm_starting
				assert("disable_warm_starting Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_is_continuous_physics_wrong
			-- Wrong test for is_continuous_physics
		note
			testing:  "covers/{B2D_WORLD}.is_continuous_physics",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
			l_result:BOOLEAN
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_result := l_world.is_continuous_physics
				assert("is_continuous_physics Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_is_continuous_physics_wrong
			-- Wrong test for set_is_continuous_physics
		note
			testing:  "covers/{B2D_WORLD}.set_is_continuous_physics",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_world.set_is_continuous_physics(True)
				assert("set_is_continuous_physics Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_enable_continuous_physics_wrong
			-- Wrong test for enable_continuous_physics
		note
			testing:  "covers/{B2D_WORLD}.enable_continuous_physics",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_world.enable_continuous_physics
				assert("enable_continuous_physics Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_disable_continuous_physics_wrong
			-- Wrong test for disable_continuous_physics
		note
			testing:  "covers/{B2D_WORLD}.disable_continuous_physics",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_world.disable_continuous_physics
				assert("disable_continuous_physics Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_is_sub_stepping_wrong
			-- Wrong test for is_sub_stepping
		note
			testing:  "covers/{B2D_WORLD}.is_sub_stepping",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
			l_result:BOOLEAN
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_result := l_world.is_sub_stepping
				assert("is_sub_stepping Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_is_sub_stepping_wrong
			-- Wrong test for set_is_sub_stepping
		note
			testing:  "covers/{B2D_WORLD}.set_is_sub_stepping",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_world.set_is_sub_stepping(True)
				assert("set_is_sub_stepping Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_enable_sub_stepping_wrong
			-- Wrong test for enable_sub_stepping
		note
			testing:  "covers/{B2D_WORLD}.enable_sub_stepping",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_world.enable_sub_stepping
				assert("enable_sub_stepping Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_disable_sub_stepping_wrong
			-- Wrong test for disable_sub_stepping
		note
			testing:  "covers/{B2D_WORLD}.disable_sub_stepping",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_world.disable_sub_stepping
				assert("disable_sub_stepping Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_proxy_count_wrong
			-- Wrong test for proxy_count
		note
			testing:  "covers/{B2D_WORLD}.proxy_count",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
			l_result:INTEGER
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_result := l_world.proxy_count
				assert("proxy_count Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_tree_height_wrong
			-- Wrong test for tree_height
		note
			testing:  "covers/{B2D_WORLD}.tree_height",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
			l_result:INTEGER
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_result := l_world.tree_height
				assert("tree_height Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_tree_balance_wrong
			-- Wrong test for tree_balance
		note
			testing:  "covers/{B2D_WORLD}.tree_balance",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
			l_result:INTEGER
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_result := l_world.tree_balance
				assert("tree_balance Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_tree_quality_wrong
			-- Wrong test for tree_quality
		note
			testing:  "covers/{B2D_WORLD}.tree_quality",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
			l_result:REAL_32
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_result := l_world.tree_quality
				assert("tree_quality Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_move_origin_wrong
			-- Wrong test for move_origin
		note
			testing:  "covers/{B2D_WORLD}.move_origin",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_world.move_origin(10,10)
				assert("move_origin Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_move_origin_with_vector_wrong
			-- Wrong test for move_origin_with_vector
		note
			testing:  "covers/{B2D_WORLD}.move_origin_with_vector",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_world.move_origin_with_vector(create {B2D_VECTOR_2D})
				assert("move_origin_with_vector Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_profile_wrong
			-- Wrong test for profile
		note
			testing:  "covers/{B2D_WORLD}.profile",
					  "covers/{B2D_WORLD}.make"
		local
			l_retry:BOOLEAN
			l_world:B2D_WORLD
			l_result:B2D_PROFILE
		do
			if not l_retry then
				create l_world.make (0, 0)
				l_world.put_null
				l_result := l_world.profile
				assert("profile Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

end


