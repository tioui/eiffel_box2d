note
	description: "Tests for {B2D_JOINT_ROPE_DEFINITION}."
	author: "Patrick Boucher"
	date: "Fri, 10 Jul 2020 05:53:26 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_JOINT_ROPE_DEFINITION_TESTS

inherit
	EQA_TEST_SET
		redefine
			on_prepare
		end
	B2D_ANY
		undefine
			default_create
		end
	B2D_COMPARE_REAL
		undefine
			default_create
		end

feature {NONE} -- Events

	on_prepare
			-- <Precursor>
		local
			l_body_definition:B2D_BODY_DEFINITION
		do
			create world.make (0, -9.8)
			create l_body_definition
			world.create_body (l_body_definition)
			body_1 := world.last_body
			world.create_body (l_body_definition)
			body_2 := world.last_body
		end

	world:B2D_WORLD
			-- The world in which the tests will be performed.

	body_1:detachable B2D_BODY
			-- The first body of each {B2D_JOINT_ROPE_DEFINITION} during the tests.

	body_2:detachable B2D_BODY
			-- The second body of each {B2D_JOINT_ROPE_DEFINITION} during the tests.

feature -- Normal test routines

	test_make_normal
			-- Normal test for `make'.
		note
			testing:  "covers/{B2D_JOINT_ROPE_DEFINITION}.make",
					  "covers/{B2D_JOINT_ROPE_DEFINITION}.local_anchor_1",
					  "covers/{B2D_JOINT_ROPE_DEFINITION}.local_anchor_2",
					  "covers/{B2D_JOINT_ROPE_DEFINITION}.max_length"
		local
			l_definition:B2D_JOINT_ROPE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				la_body_2.set_transform (30, 0, 0)
				create l_definition.make (la_body_1, la_body_2, -2.3, 2.3, 1.6, 1.45, 35.123)
				assert ("body_1 is assigned", l_definition.body_1 ~ la_body_1)
				assert ("body_2 is assigned", l_definition.body_2 ~ la_body_2)
				assert ("local_anchor_1 assigned x", l_definition.local_anchor_1.x ~ -2.3)
				assert ("local_anchor_1 assigned y", l_definition.local_anchor_1.y ~ 2.3)
				assert ("local_anchor_2 assigned x", l_definition.local_anchor_2.x ~ 1.6)
				assert ("local_anchor_2 assigned y", l_definition.local_anchor_2.y ~ 1.45)
				assert ("max_length is assigned", l_definition.max_length ~ 35.123)
			end
		end

	test_make_with_vector_normal
			-- Normal test for `make_with_vector'.
		note
			testing:  "covers/{B2D_JOINT_ROPE_DEFINITION}.make_with_vector",
					  "covers/{B2D_JOINT_ROPE_DEFINITION}.local_anchor_1",
					  "covers/{B2D_JOINT_ROPE_DEFINITION}.local_anchor_2",
					  "covers/{B2D_JOINT_ROPE_DEFINITION}.max_length"
		local
			l_definition:B2D_JOINT_ROPE_DEFINITION
			l_anchor_1, l_anchor_2:B2D_VECTOR_2D
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				la_body_2.set_transform (30, 0, 0)
				create l_anchor_1.make_with_coordinates (-2.3, 2.3)
				create l_anchor_2.make_with_coordinates (1.6, 1.45)
				create l_definition.make_with_vector (la_body_1, la_body_2, l_anchor_1, l_anchor_2, 35.123)
				assert ("body_1 is assigned", l_definition.body_1 ~ la_body_1)
				assert ("body_2 is assigned", l_definition.body_2 ~ la_body_2)
				assert ("local_anchor_1 assigned x", l_definition.local_anchor_1.x ~ -2.3)
				assert ("local_anchor_1 assigned y", l_definition.local_anchor_1.y ~ 2.3)
				assert ("local_anchor_2 assigned x", l_definition.local_anchor_2.x ~ 1.6)
				assert ("local_anchor_2 assigned y", l_definition.local_anchor_2.y ~ 1.45)
				assert ("max_length is assigned", l_definition.max_length ~ 35.123)
			end
		end

	test_local_anchor_1_normal
			-- Normal test for `local_anchor_1'.
		note
			testing:  "covers/{B2D_JOINT_ROPE_DEFINITION}.local_anchor_1",
					  "covers/{B2D_JOINT_ROPE_DEFINITION}.set_local_anchor_1"
		local
			l_definition:B2D_JOINT_ROPE_DEFINITION
			l_anchor:B2D_VECTOR_2D
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 0, 30)
				create l_anchor.make_with_coordinates (10, 10.5)
				l_definition.set_local_anchor_1 (l_anchor)
				assert ("set_local_anchor valid x", l_definition.local_anchor_1.x ~ 10)
				assert ("set_local_anchor valid y", l_definition.local_anchor_1.y ~ 10.5)
			end
		end

	test_local_anchor_2_normal
			-- Normal test for `local_anchor_2'.
		note
			testing:  "covers/{B2D_JOINT_ROPE_DEFINITION}.local_anchor_2",
					  "covers/{B2D_JOINT_ROPE_DEFINITION}.set_local_anchor_2"
		local
			l_definition:B2D_JOINT_ROPE_DEFINITION
			l_anchor:B2D_VECTOR_2D
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 0, 30)
				create l_anchor.make_with_coordinates (10, 10.5)
				l_definition.set_local_anchor_2 (l_anchor)
				assert ("set_local_anchor valid x", l_definition.local_anchor_2.x ~ 10)
				assert ("set_local_anchor valid y", l_definition.local_anchor_2.y ~ 10.5)
			end
		end

	test__normal
			-- Normal test for `'.
		note
			testing:  "covers/{B2D_JOINT_ROPE_DEFINITION}.",
					  "covers/{B2D_JOINT_ROPE_DEFINITION}."
		local
			l_definition:B2D_JOINT_ROPE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 0, 30)
			end
		end

	test_max_length_normal
			-- Normal test for `max_length'.
		note
			testing:  "covers/{B2D_JOINT_ROPE_DEFINITION}.max_length",
					  "covers/{B2D_JOINT_ROPE_DEFINITION}.set_max_length"
		local
			l_definition:B2D_JOINT_ROPE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 0, 30)
				l_definition.set_max_length (20.25)
				assert ("set_max_length valid", l_definition.max_length ~ 20.25)
			end
		end

feature -- Limit test routines

	test_make_limit_1
			-- Limit test for `make'.
		note
			testing:  "covers/{B2D_JOINT_ROPE_DEFINITION}.make",
					  "covers/{B2D_JOINT_ROPE_DEFINITION}.max_length"
		local
			l_definition:B2D_JOINT_ROPE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 0, 0.0051)
				assert ("max_length is assigned", l_definition.max_length ~ 0.0051)
			end
		end

	test_make_limit_2
			-- Limit test for `make'.
		note
			testing:  "covers/{B2D_JOINT_ROPE_DEFINITION}.make",
					  "covers/{B2D_JOINT_ROPE_DEFINITION}.max_length"
		local
			l_definition:B2D_JOINT_ROPE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				la_body_2.set_transform (10.25, 0, 0)
				create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 0, 10.25)
				assert ("max_length is assigned", l_definition.max_length ~ 10.25)
			end
		end

	test_make_with_vector_limit_1
			-- Limit test for `make_with_vector'.
		note
			testing:  "covers/{B2D_JOINT_ROPE_DEFINITION}.make_with_vector",
					  "covers/{B2D_JOINT_ROPE_DEFINITION}.max_length"
		local
			l_definition:B2D_JOINT_ROPE_DEFINITION
			l_anchor_1, l_anchor_2:B2D_VECTOR_2D
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_anchor_1
				create l_anchor_2
				create l_definition.make_with_vector (la_body_1, la_body_2, l_anchor_1, l_anchor_2, 0.0051)
				assert ("max_length is assigned", l_definition.max_length ~ 0.0051)
			end
		end

	test_make_with_vector_limit_2
			-- Limit test for `make_with_vector'.
		note
			testing:  "covers/{B2D_JOINT_ROPE_DEFINITION}.make_with_vector",
					  "covers/{B2D_JOINT_ROPE_DEFINITION}.max_length"
		local
			l_definition:B2D_JOINT_ROPE_DEFINITION
			l_anchor_1, l_anchor_2:B2D_VECTOR_2D
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				la_body_2.set_transform (10.25, 0, 0)
				create l_anchor_1
				create l_anchor_2
				create l_definition.make_with_vector (la_body_1, la_body_2, l_anchor_1, l_anchor_2, 10.25)
				assert ("max_length is assigned", l_definition.max_length ~ 10.25)
			end
		end

	test_set_max_length_limit_1
			-- Limit test for `set_max_length'.
		note
			testing:  "covers/{B2D_JOINT_ROPE_DEFINITION}.set_max_length"
		local
			l_definition:B2D_JOINT_ROPE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 0, 30)
				l_definition.set_max_length (0.0051)
				assert ("set_max_length valid", l_definition.max_length ~ 0.0051)
			end
		end

	test_set_max_length_limit_2
			-- Limit test for `'.
		note
			testing:  "covers/{B2D_JOINT_ROPE_DEFINITION}.set_max_length"
		local
			l_definition:B2D_JOINT_ROPE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				la_body_2.set_transform (10.123, 0, 0)
				create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 0, 30)
				l_definition.set_max_length (10.123)
				assert ("set_max_length valid", l_definition.max_length ~ 10.123)
			end
		end

feature -- Wrong test routines

	test_make_wrong_1
			-- Test for `make' when arguments do not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.make"
		local
			l_retry:INTEGER
			l_definition:B2D_JOINT_ROPE_DEFINITION
		do
			if l_retry ~ 0 then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					la_body_1.put_null
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 0, 30)
					assert ("a_body_1 exists", False)
				end
			elseif l_retry ~ 1 then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					la_body_2.put_null
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 0, 30)
					assert ("a_body_2 exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := l_retry + 1
				retry
			end
		end

	test_make_wrong_2
			-- Test for `make' when `max_length' is shorter than the distance between
			-- `body_1' and `body_2'.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.make"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_ROPE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					la_body_2.set_transform (12, 0, 0)
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 0, 10)
					assert ("Max_Length_Is_At_Least_The_Distance_Between_Bodies", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_make_wrong_3
			-- Test for `make' when `max_length' is shorter than the minimum length.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.make"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_ROPE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 0, 0)
					assert ("Max_Length_Minimum_Value", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_make_with_vector_wrong_1
			-- Test for `make_with_vector' when arguments do not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.make_with_vector"
		local
			l_retry:INTEGER
			l_definition:B2D_JOINT_ROPE_DEFINITION
			l_anchor_1, l_anchor_2:B2D_VECTOR_2D
		do
			if l_retry ~ 0 then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					la_body_1.put_null
					create l_anchor_1
					create l_anchor_2
					create l_definition.make_with_vector (la_body_1, la_body_2, l_anchor_1, l_anchor_2, 10)
					assert ("a_body_1 exists", False)
				end
			elseif l_retry ~ 1 then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					la_body_2.put_null
					create l_anchor_1
					create l_anchor_2
					create l_definition.make_with_vector (la_body_1, la_body_2, l_anchor_1, l_anchor_2, 10)
					assert ("a_body_2 exists", False)
				end
			elseif l_retry ~ 2 then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_anchor_1
					l_anchor_1.put_null
					create l_anchor_2
					create l_definition.make_with_vector (la_body_1, la_body_2, l_anchor_1, l_anchor_2, 10)
					assert ("a_local_anchor_1 exists", False)
				end
			elseif l_retry ~ 3 then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_anchor_1
					create l_anchor_2
					l_anchor_2.put_null
					create l_definition.make_with_vector (la_body_1, la_body_2, l_anchor_1, l_anchor_2, 10)
					assert ("a_local_anchor_2 exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := l_retry + 1
				retry
			end
		end

	test_make_with_vector_wrong_2
			-- Test for `make_with_vector' when `max_length' is shorter than the distance between
			-- `body_1' and `body_2'.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.make_with_vector"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_ROPE_DEFINITION
			l_anchor_1, l_anchor_2:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					la_body_2.set_transform (12, 0, 0)
					create l_anchor_1
					create l_anchor_2
					create l_definition.make_with_vector (la_body_1, la_body_2, l_anchor_1, l_anchor_2, 10)
					assert ("Max_Length_Is_At_Least_The_Distance_Between_Bodies", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_make_with_vector_wrong_3
			-- Test for `make_with_vector' when `max_length' is shorter than the minimum length.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.make_with_vector"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_ROPE_DEFINITION
			l_anchor_1, l_anchor_2:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_anchor_1
					create l_anchor_2
					create l_definition.make_with_vector (la_body_1, la_body_2, l_anchor_1, l_anchor_2, 0.005)
					assert ("Max_Length_Minimum_Value", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_local_anchor_1_wrong
			-- Test for `local_anchor_1' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.local_anchor_1"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_ROPE_DEFINITION
			l_discard:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 0, 30)
					l_definition.put_null
					l_discard := l_definition.local_anchor_1
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_local_anchor_1_wrong
			-- Test for `set_local_anchor_1' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_local_anchor_1"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_ROPE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 0, 30)
					l_definition.put_null
					l_definition.set_local_anchor_1 (create {B2D_VECTOR_2D})
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_local_anchor_2_wrong
			-- Test for `local_anchor_2' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.local_anchor_2"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_ROPE_DEFINITION
			l_discard:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 0, 30)
					l_definition.put_null
					l_discard := l_definition.local_anchor_2
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_local_anchor_2_wrong
			-- Test for `set_local_anchor_2' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_local_anchor_2"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_ROPE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 0, 30)
					l_definition.put_null
					l_definition.set_local_anchor_2 (create {B2D_VECTOR_2D})
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_max_length_wrong
			-- Test for `max_length' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.max_length"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_ROPE_DEFINITION
			l_discard:REAL_32
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 0, 30)
					l_definition.put_null
					l_discard := l_definition.max_length
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_max_length_wrong_1
			-- Test for `set_max_length' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_max_length"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_ROPE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 0, 30)
					l_definition.put_null
					l_definition.set_max_length (20)
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_max_length_wrong_2
			-- Test for `set_max_length' when `a_value' is shorter than minimum.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_max_length"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_ROPE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 0, 30)
					l_definition.set_max_length (0.005)
					assert ("set_max_length valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_max_length_wrong_3
			-- Test for `set_max_length' when `a_value' is shorter than the distance between
			-- the bodies.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_max_length"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_ROPE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					la_body_2.set_transform (10.25, 0, 0)
					create l_definition.make (la_body_1, la_body_2, 0, 0, 0, 0, 30)
					l_definition.set_max_length (10.2)
					assert ("set_max_length valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

end


