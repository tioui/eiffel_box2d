note
	description: "Tests for {B2D_SHAPE_FACTORY}"
	author: "Louis Marchand"
	date: "Thu, 18 Jun 2020 19:25:33 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_SHAPE_FACTORY_TESTS

inherit
	EQA_TEST_SET
	B2D_ANY
		undefine
			default_create
		end

feature -- Test routines

	test_own_shape_circle_class_normal
			-- Normal test for class call of `own_shape' using {B2D_SHAPE_CIRCLE}
		note
			testing:  "covers/{B2D_SHAPE_FACTORY}.own_shape"
		local
			l_shape_source:B2D_SHAPE
		do
			create {B2D_SHAPE_CIRCLE}l_shape_source
			if attached {B2D_SHAPE_CIRCLE} {B2D_SHAPE_FACTORY}.own_shape (l_shape_source.item) as la_shape then
				assert ("own_shape valid: Shared", not la_shape.is_shared)
			else
				assert ("own_shape valid: Type not valid", False)
			end
		end

	test_own_shape_polygon_class_normal
			-- Normal test for class call of `own_shape' using {B2D_SHAPE_POLYGON}
		note
			testing:  "covers/{B2D_SHAPE_FACTORY}.own_shape"
		local
			l_shape_source:B2D_SHAPE
		do
			create {B2D_SHAPE_POLYGON}l_shape_source.make_box (10, 10)
			if attached {B2D_SHAPE_POLYGON} {B2D_SHAPE_FACTORY}.own_shape (l_shape_source.item) as la_shape then
				assert ("own_shape valid: Shared", not la_shape.is_shared)
			else
				assert ("own_shape valid: Type not valid", False)
			end
		end

	test_own_shape_edge_class_normal
			-- Normal test for class call of `own_shape' using {B2D_SHAPE_EDGE}
		note
			testing:  "covers/{B2D_SHAPE_FACTORY}.own_shape"
		local
			l_shape_source:B2D_SHAPE
		do
			create {B2D_SHAPE_EDGE}l_shape_source.make (1, 2, 3, 4)
			if attached {B2D_SHAPE_EDGE} {B2D_SHAPE_FACTORY}.own_shape (l_shape_source.item) as la_shape then
				assert ("own_shape valid: Shared", not la_shape.is_shared)
			else
				assert ("own_shape valid: Type not valid", False)
			end
		end

	test_own_shape_chain_class_normal
			-- Normal test for class call of `own_shape' using {B2D_SHAPE_CHAIN}
		note
			testing:  "covers/{B2D_SHAPE_FACTORY}.own_shape"
		local
			l_shape_source:B2D_SHAPE
		do
			create {B2D_SHAPE_CHAIN}l_shape_source.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-0.666666666666667, {REAL_32}4.55555555555555],
									[{REAL_32}13.1111111111111, {REAL_32}2.22222222222223],
									[{REAL_32}10.0, {REAL_32}-3.0],
									[{REAL_32}1.6666666666667, {REAL_32}-0.666666666666667]
								>>)
			if attached {B2D_SHAPE_CHAIN} {B2D_SHAPE_FACTORY}.own_shape (l_shape_source.item) as la_shape then
				assert ("own_shape valid: Shared", not la_shape.is_shared)
			else
				assert ("own_shape valid: Type not valid", False)
			end
		end

	test_own_shape_class_wrong
			-- Wrong test for class call of `own_shape'
		note
			testing:  "covers/{B2D_SHAPE_FACTORY}.own_shape"
		local
			l_retry:BOOLEAN
			l_shape:detachable B2D_SHAPE
		do
			if not l_retry then
				l_shape := {B2D_SHAPE_FACTORY}.own_shape (create {POINTER})
				assert("own_shape Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_own_shape_circle_normal
			-- Normal test for `own_shape' using {B2D_SHAPE_CIRCLE}
		note
			testing:  "covers/{B2D_SHAPE_FACTORY}.own_shape"
		local
			l_factory:B2D_SHAPE_FACTORY
			l_shape_source:B2D_SHAPE
		do
			create l_factory
			create {B2D_SHAPE_CIRCLE}l_shape_source
			if attached {B2D_SHAPE_CIRCLE} l_factory.own_shape (l_shape_source.item) as la_shape then
				assert ("own_shape valid: Shared", not la_shape.is_shared)
			else
				assert ("own_shape valid: Type not valid", False)
			end
		end

	test_own_shape_polygon_normal
			-- Normal test for `own_shape' using {B2D_SHAPE_POLYGON}
		note
			testing:  "covers/{B2D_SHAPE_FACTORY}.own_shape"
		local
			l_factory:B2D_SHAPE_FACTORY
			l_shape_source:B2D_SHAPE
		do
			create l_factory
			create {B2D_SHAPE_POLYGON}l_shape_source.make_box (10, 10)
			if attached {B2D_SHAPE_POLYGON} l_factory.own_shape (l_shape_source.item) as la_shape then
				assert ("own_shape valid: Shared", not la_shape.is_shared)
			else
				assert ("own_shape valid: Type not valid", False)
			end
		end

	test_own_shape_edge_normal
			-- Normal test for `own_shape' using {B2D_SHAPE_EDGE}
		note
			testing:  "covers/{B2D_SHAPE_FACTORY}.own_shape"
		local
			l_factory:B2D_SHAPE_FACTORY
			l_shape_source:B2D_SHAPE
		do
			create l_factory
			create {B2D_SHAPE_EDGE}l_shape_source.make (1, 2, 3, 4)
			if attached {B2D_SHAPE_EDGE} l_factory.own_shape (l_shape_source.item) as la_shape then
				assert ("own_shape valid: Shared", not la_shape.is_shared)
			else
				assert ("own_shape valid: Type not valid", False)
			end
		end

	test_own_shape_chain_normal
			-- Normal test for `own_shape' using {B2D_SHAPE_CHAIN}
		note
			testing:  "covers/{B2D_SHAPE_FACTORY}.own_shape"
		local
			l_factory:B2D_SHAPE_FACTORY
			l_shape_source:B2D_SHAPE
		do
			create l_factory
			create {B2D_SHAPE_CHAIN}l_shape_source.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-0.666666666666667, {REAL_32}4.55555555555555],
									[{REAL_32}13.1111111111111, {REAL_32}2.22222222222223],
									[{REAL_32}10.0, {REAL_32}-3.0],
									[{REAL_32}1.6666666666667, {REAL_32}-0.666666666666667]
								>>)
			if attached {B2D_SHAPE_CHAIN} l_factory.own_shape (l_shape_source.item) as la_shape then
				assert ("own_shape valid: Shared", not la_shape.is_shared)
			else
				assert ("own_shape valid: Type not valid", False)
			end
		end

	test_own_shape_wrong
			-- Wrong test for `own_shape'
		note
			testing:  "covers/{B2D_SHAPE_FACTORY}.own_shape"
		local
			l_factory:B2D_SHAPE_FACTORY
			l_retry:BOOLEAN
			l_shape:detachable B2D_SHAPE
		do
			create l_factory
			if not l_retry then
				l_shape := l_factory.own_shape (create {POINTER})
				assert("own_shape Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_shared_shape_circle_class_normal
			-- Normal test for class call of `shared_shape' using {B2D_SHAPE_CIRCLE}
		note
			testing:  "covers/{B2D_SHAPE_FACTORY}.shared_shape"
		local
			l_shape_source:B2D_SHAPE
		do
			create {B2D_SHAPE_CIRCLE}l_shape_source
			if attached {B2D_SHAPE_CIRCLE} {B2D_SHAPE_FACTORY}.shared_shape (l_shape_source.item, l_shape_source) as la_shape then
				assert ("shared_shape valid: Shared", la_shape.is_shared)
			else
				assert ("shared_shape valid: Type not valid", False)
			end
		end

	test_shared_shape_polygon_class_normal
			-- Normal test for class call of `shared_shape' using {B2D_SHAPE_POLYGON}
		note
			testing:  "covers/{B2D_SHAPE_FACTORY}.shared_shape"
		local
			l_shape_source:B2D_SHAPE
		do
			create {B2D_SHAPE_POLYGON}l_shape_source.make_box (10, 10)
			if attached {B2D_SHAPE_POLYGON} {B2D_SHAPE_FACTORY}.shared_shape (l_shape_source.item, l_shape_source) as la_shape then
				assert ("shared_shape valid: Shared", la_shape.is_shared)
			else
				assert ("shared_shape valid: Type not valid", False)
			end
		end

	test_shared_shape_edge_class_normal
			-- Normal test for class call of `shared_shape' using {B2D_SHAPE_EDGE}
		note
			testing:  "covers/{B2D_SHAPE_FACTORY}.shared_shape"
		local
			l_shape_source:B2D_SHAPE
		do
			create {B2D_SHAPE_EDGE}l_shape_source.make (1, 2, 3, 4)
			if attached {B2D_SHAPE_EDGE} {B2D_SHAPE_FACTORY}.shared_shape (l_shape_source.item, l_shape_source) as la_shape then
				assert ("shared_shape valid: Shared", la_shape.is_shared)
			else
				assert ("shared_shape valid: Type not valid", False)
			end
		end

	test_shared_shape_chain_class_normal
			-- Normal test for class call of `shared_shape' using {B2D_SHAPE_CHAIN}
		note
			testing:  "covers/{B2D_SHAPE_FACTORY}.shared_shape"
		local
			l_shape_source:B2D_SHAPE
		do
			create {B2D_SHAPE_CHAIN}l_shape_source.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-0.666666666666667, {REAL_32}4.55555555555555],
									[{REAL_32}13.1111111111111, {REAL_32}2.22222222222223],
									[{REAL_32}10.0, {REAL_32}-3.0],
									[{REAL_32}1.6666666666667, {REAL_32}-0.666666666666667]
								>>)
			if attached {B2D_SHAPE_CHAIN} {B2D_SHAPE_FACTORY}.shared_shape (l_shape_source.item, l_shape_source) as la_shape then
				assert ("shared_shape valid: Shared", la_shape.is_shared)
			else
				assert ("shared_shape valid: Type not valid", False)
			end
		end

	test_shared_shape_class_wrong
			-- Wrong test for class call of `shared_shape'
		note
			testing:  "covers/{B2D_SHAPE_FACTORY}.shared_shape"
		local
			l_retry:BOOLEAN
			l_shape:detachable B2D_SHAPE
		do
			if not l_retry then
				l_shape := {B2D_SHAPE_FACTORY}.own_shape (create {POINTER})
				assert("shared_shape Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_shared_shape_circle_normal
			-- Normal test for `shared_shape' using {B2D_SHAPE_CIRCLE}
		note
			testing:  "covers/{B2D_SHAPE_FACTORY}.shared_shape"
		local
			l_factory:B2D_SHAPE_FACTORY
			l_shape_source:B2D_SHAPE
		do
			create l_factory
			create {B2D_SHAPE_CIRCLE}l_shape_source
			if attached {B2D_SHAPE_CIRCLE} l_factory.shared_shape (l_shape_source.item, l_shape_source) as la_shape then
				assert ("shared_shape valid: Shared", la_shape.is_shared)
			else
				assert ("shared_shape valid: Type not valid", False)
			end
		end

	test_shared_shape_polygon_normal
			-- Normal test for `shared_shape' using {B2D_SHAPE_POLYGON}
		note
			testing:  "covers/{B2D_SHAPE_FACTORY}.shared_shape"
		local
			l_factory:B2D_SHAPE_FACTORY
			l_shape_source:B2D_SHAPE
		do
			create l_factory
			create {B2D_SHAPE_POLYGON}l_shape_source.make_box (10, 10)
			if attached {B2D_SHAPE_POLYGON} l_factory.shared_shape (l_shape_source.item, l_shape_source) as la_shape then
				assert ("shared_shape valid: Shared", la_shape.is_shared)
			else
				assert ("shared_shape valid: Type not valid", False)
			end
		end

	test_shared_shape_edge_normal
			-- Normal test for `shared_shape' using {B2D_SHAPE_EDGE}
		note
			testing:  "covers/{B2D_SHAPE_FACTORY}.shared_shape"
		local
			l_factory:B2D_SHAPE_FACTORY
			l_shape_source:B2D_SHAPE
		do
			create l_factory
			create {B2D_SHAPE_EDGE}l_shape_source.make (1, 2, 3, 4)
			if attached {B2D_SHAPE_EDGE} l_factory.shared_shape (l_shape_source.item, l_shape_source) as la_shape then
				assert ("shared_shape valid: Shared", la_shape.is_shared)
			else
				assert ("shared_shape valid: Type not valid", False)
			end
		end

	test_shared_shape_chain_normal
			-- Normal test for `shared_shape' using {B2D_SHAPE_CHAIN}
		note
			testing:  "covers/{B2D_SHAPE_FACTORY}.shared_shape"
		local
			l_factory:B2D_SHAPE_FACTORY
			l_shape_source:B2D_SHAPE
		do
			create l_factory
			create {B2D_SHAPE_CHAIN}l_shape_source.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-0.666666666666667, {REAL_32}4.55555555555555],
									[{REAL_32}13.1111111111111, {REAL_32}2.22222222222223],
									[{REAL_32}10.0, {REAL_32}-3.0],
									[{REAL_32}1.6666666666667, {REAL_32}-0.666666666666667]
								>>)
			if attached {B2D_SHAPE_CHAIN} l_factory.shared_shape (l_shape_source.item, l_shape_source) as la_shape then
				assert ("shared_shape valid: Shared", la_shape.is_shared)
			else
				assert ("shared_shape valid: Type not valid", False)
			end
		end

	test_shared_shape_wrong
			-- Wrong test for `shared_shape'
		note
			testing:  "covers/{B2D_SHAPE_FACTORY}.shared_shape"
		local
			l_factory:B2D_SHAPE_FACTORY
			l_retry:BOOLEAN
			l_shape:detachable B2D_SHAPE
		do
			create l_factory
			if not l_retry then
				l_shape := l_factory.own_shape (create {POINTER})
				assert("shared_shape Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end


end


