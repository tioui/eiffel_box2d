note
	description: "Tests for {B2D_FIXTURE_DEFINITION}"
	author: "Louis Marchand"
	date: "Tue, 09 Jun 2020 19:59:46 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_FIXTURE_DEFINITION_TESTS

inherit
	EQA_TEST_SET
	B2D_COMPARE_REAL
		undefine
			default_create
		end

feature -- Test routines

	test_make
			-- Test routine for {B2D_FIXTURE_DEFINITION}.make
		note
			testing:  "covers/{B2D_FIXTURE_DEFINITION}.make", "covers/{B2D_FIXTURE_DEFINITION}.shape"
		local
			l_shape:B2D_SHAPE_CIRCLE
			l_definition:B2D_FIXTURE_DEFINITION
		do
			create l_shape
			create l_definition.make (l_shape)
			assert ("Make valid", l_definition.shape ~ l_shape)
		end

	test_friction
			-- Test routine for {B2D_FIXTURE_DEFINITION}.set_friction
		note
			testing:  "covers/{B2D_FIXTURE_DEFINITION}.set_friction", "covers/{B2D_FIXTURE_DEFINITION}.friction"
		local
			l_shape:B2D_SHAPE_CIRCLE
			l_definition:B2D_FIXTURE_DEFINITION
		do
			create l_shape
			create l_definition.make (l_shape)
			l_definition.set_friction (0.0)
			l_definition.set_friction (-8.5921)
			assert ("set_friction valid", compare_real_32(-8.5921, l_definition.friction))
		end

	test_density
			-- Test routine for {B2D_FIXTURE_DEFINITION}.set_density
		note
			testing:  "covers/{B2D_FIXTURE_DEFINITION}.set_density", "covers/{B2D_FIXTURE_DEFINITION}.density"
		local
			l_shape:B2D_SHAPE_CIRCLE
			l_definition:B2D_FIXTURE_DEFINITION
		do
			create l_shape
			create l_definition.make (l_shape)
			l_definition.set_density (0.0)
			l_definition.set_density (-1.6563)
			assert ("set_density valid", l_definition.density ~ -1.6563)
		end

	test_restitution
			-- Test routine for {B2D_FIXTURE_DEFINITION}.set_restitution
		note
			testing:  "covers/{B2D_FIXTURE_DEFINITION}.set_restitution", "covers/{B2D_FIXTURE_DEFINITION}.restitution"
		local
			l_shape:B2D_SHAPE_CIRCLE
			l_definition:B2D_FIXTURE_DEFINITION
		do
			create l_shape
			create l_definition.make (l_shape)
			l_definition.set_restitution (0.0)
			l_definition.set_restitution (0.56233)
			assert ("set_restitution valid", l_definition.restitution ~ 0.56233)
		end

	test_is_sensor
			-- Test routine for {B2D_FIXTURE_DEFINITION}.is_sensor
		note
			testing:  "covers/{B2D_FIXTURE_DEFINITION}.is_sensor", "covers/{B2D_FIXTURE_DEFINITION}.set_is_sensor",
						"covers/{B2D_FIXTURE_DEFINITION}.enable_sensor", "covers/{B2D_FIXTURE_DEFINITION}.disable_sensor"
		local
			l_shape:B2D_SHAPE_CIRCLE
			l_definition:B2D_FIXTURE_DEFINITION
		do
			create l_shape
			create l_definition.make (l_shape)
			l_definition.set_is_sensor (False)
			l_definition.enable_sensor
			assert ("enable_sensor valid", l_definition.is_sensor)
			l_definition.disable_sensor
			assert ("disable_sensor valid", not l_definition.is_sensor)
			l_definition.set_is_sensor (True)
			assert ("set_is_sensor valid", l_definition.is_sensor)
		end

	test_filter_set_category_bits
			-- Test routine for {B2D_FIXTURE_DEFINITION}.filter.set_category_bits
		note
			testing:  "covers/{B2D_FIXTURE_DEFINITION}.filter.set_category_bits", "covers/{B2D_FIXTURE_DEFINITION}.filter.category_bits"
		local
			l_shape:B2D_SHAPE_CIRCLE
			l_definition:B2D_FIXTURE_DEFINITION
		do
			create l_shape
			create l_definition.make (l_shape)
			l_definition.filter.set_category_bits (0x8000)
			assert ("filter.set_category_bits valid", l_definition.filter.category_bits ~ 0x8000)
			l_definition.filter.set_category_bits (0x0000)
			assert ("filter.set_category_bits valid", l_definition.filter.category_bits ~ 0x0000)
		end

	test_filter_set_mask_bits
			-- Test routine for {B2D_FIXTURE_DEFINITION}.filter.set_mask_bits
		note
			testing:  "covers/{B2D_FIXTURE_DEFINITION}.filter.set_mask_bits", "covers/{B2D_FIXTURE_DEFINITION}.filter.mask_bits"
		local
			l_shape:B2D_SHAPE_CIRCLE
			l_definition:B2D_FIXTURE_DEFINITION
		do
			create l_shape
			create l_definition.make (l_shape)
			l_definition.filter.set_mask_bits (0x0040)
			assert ("filter.set_mask_bits valid", l_definition.filter.mask_bits ~ 0x0040)
			l_definition.filter.set_mask_bits (0x0000)
			assert ("filter.set_mask_bits valid", l_definition.filter.mask_bits ~ 0x0000)
		end

	test_filter_set_group_index
			-- Test routine for {B2D_FIXTURE_DEFINITION}.filter.set_group_index
		note
			testing:  "covers/{B2D_FIXTURE_DEFINITION}.filter.set_group_index", "covers/{B2D_FIXTURE_DEFINITION}.filter.group_index"
		local
			l_shape:B2D_SHAPE_CIRCLE
			l_definition:B2D_FIXTURE_DEFINITION
		do
			create l_shape
			create l_definition.make (l_shape)
			l_definition.filter.set_group_index (833)
			assert ("filter.set_group_index valid", l_definition.filter.group_index ~ 833)
			l_definition.filter.set_group_index (-45)
			assert ("filter.set_group_index valid", l_definition.filter.group_index ~ -45)
		end

end


