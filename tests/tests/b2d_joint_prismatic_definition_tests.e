note
	description: "Tests for {B2D_JOINT_PRISMATIC_DEFINITION}"
	author: "Patrick Boucher"
	date: "Tue, 30 Jun 2020 03:05:09 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_JOINT_PRISMATIC_DEFINITION_TESTS

inherit
	EQA_TEST_SET
		redefine
			on_prepare
		end
	B2D_ANY
		undefine
			default_create
		end
	B2D_COMPARE_REAL
		undefine
			default_create
		end

feature {NONE} -- Implementation

	on_prepare
			-- <Precursor>
		local
			l_body_definition:B2D_BODY_DEFINITION
		do
			create world.make (0, -9.8)
			create l_body_definition
			l_body_definition.set_position (-10, -10)
			l_body_definition.set_angle ({DOUBLE_MATH}.pi_2.truncated_to_real)
			world.create_body (l_body_definition)
			body_1 := world.last_body
			l_body_definition.set_position (10, 10)
			l_body_definition.set_angle ({DOUBLE_MATH}.pi_4.truncated_to_real)
			world.create_body (l_body_definition)
			body_2 := world.last_body
		end

	world:B2D_WORLD
			-- The world in which the tests will be performed.

	body_1:detachable B2D_BODY
			-- The first body of each {B2D_JOINT_PRISMATIC_DEFINITION} during the tests.

	body_2:detachable B2D_BODY
			-- The second body of each {B2D_JOINT_PRISMATIC_DEFINITION} during the tests.

feature -- Normal test routines

	test_make_with_vector_normal
			-- Normal test for {B2D_JOINT_PRISMATIC_DEFINITION}.make_with_vector
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.make_with_vector",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.local_anchor_1",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.local_anchor_2",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.reference_angle"
		local
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
			l_expected_local_anchor_1, l_expected_local_anchor_2:B2D_VECTOR_2D
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
				assert ("Exists", l_definition.exists)
				assert ("body_1 assigned", l_definition.body_1 = la_body_1)
				assert ("body_2 assigned", l_definition.body_2 = la_body_2)
				create l_expected_local_anchor_1.make_with_coordinates (0, 0)
				assert ("local_anchor_1 valid", l_definition.local_anchor_1 ~ l_expected_local_anchor_1)
				l_expected_local_anchor_2 := la_body_2.relative_local_point (-20, -20)
				assert ("local_anchor_2 valid", l_definition.local_anchor_2 ~ l_expected_local_anchor_2)
				assert ("reference_angle valid", l_definition.reference_angle ~ -{DOUBLE_MATH}.pi_4.truncated_to_real)
				-- local_axis is relative to body_1 angle:
				assert ("local_axis valid", compare_real_32 (l_definition.local_axis.x, 1) and compare_real_32 (l_definition.local_axis.y, 0))
			else
				assert ("Bodies not valid", False)
			end
		end

	test_make_with_vector_and_angle_normal
			-- Normal test for {B2D_JOINT_PRISMATIC_DEFINITION}.make_with_vector_and_angle
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.make_with_vector_and_angle",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.local_anchor_1",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.local_anchor_2",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.reference_angle"
		local
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
			l_expected_local_anchor_1, l_expected_local_anchor_2:B2D_VECTOR_2D
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make_with_vector_and_angle (la_body_1, la_body_2, la_body_1.center_of_mass, {DOUBLE_MATH}.pi_4.truncated_to_real)
				assert ("Exists", l_definition.exists)
				assert ("body_1 assigned", l_definition.body_1 = la_body_1)
				assert ("body_2 assigned", l_definition.body_2 = la_body_2)
				create l_expected_local_anchor_1.make_with_coordinates (0, 0)
				assert ("local_anchor_1 valid", l_definition.local_anchor_1 ~ l_expected_local_anchor_1)
				l_expected_local_anchor_2 := la_body_2.relative_local_point (-20, -20)
				assert ("local_anchor_2 valid", l_definition.local_anchor_2 ~ l_expected_local_anchor_2)
				assert ("reference_angle valid", l_definition.reference_angle ~ -{DOUBLE_MATH}.pi_4.truncated_to_real)
				-- local_axis is relative to body_1 angle:
				assert ("local_axis valid", compare_real_32 (l_definition.local_axis.x, 0.707) and compare_real_32 (l_definition.local_axis.y, -0.707))
			else
				assert ("Bodies not valid", False)
			end
		end

	test_make_normal_2
			-- Normal test 2 for {B2D_JOINT_PRISMATIC_DEFINITION}.make
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.make",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.local_anchor_1",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.local_anchor_2",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.reference_angle"
		local
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
			l_expected_local_anchor_1, l_expected_local_anchor_2:B2D_VECTOR_2D
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, la_body_1.center_of_mass.x, la_body_1.center_of_mass.y, 0, 1)
				assert ("Exists", l_definition.exists)
				assert ("body_1 assigned", l_definition.body_1 = la_body_1)
				assert ("body_2 assigned", l_definition.body_2 = la_body_2)
				create l_expected_local_anchor_1.make_with_coordinates (0, 0)
				assert ("local_anchor_1 valid", l_definition.local_anchor_1 ~ l_expected_local_anchor_1)
				l_expected_local_anchor_2 := la_body_2.relative_local_point (-20, -20)
				assert ("local_anchor_2 valid", l_definition.local_anchor_2 ~ l_expected_local_anchor_2)
				assert ("reference_angle valid", l_definition.reference_angle ~ -{DOUBLE_MATH}.pi_4.truncated_to_real)
				-- local_axis is relative to body_1 angle:
				assert ("local_axis valid", compare_real_32 (l_definition.local_axis.x, 1) and compare_real_32 (l_definition.local_axis.y, 0))
			else
				assert ("Bodies not valid", False)
			end
		end

	test_make_with_angle_normal
			-- Normal test for {B2D_JOINT_PRISMATIC_DEFINITION}.make_with_angle
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.make_with_angle",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.local_anchor_1",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.local_anchor_2",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.reference_angle"
		local
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
			l_expected_local_anchor_1, l_expected_local_anchor_2:B2D_VECTOR_2D
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make_with_angle (la_body_1, la_body_2, la_body_1.center_of_mass.x, la_body_1.center_of_mass.y, {DOUBLE_MATH}.pi_4.truncated_to_real)
				assert ("Exists", l_definition.exists)
				assert ("body_1 assigned", l_definition.body_1 = la_body_1)
				assert ("body_2 assigned", l_definition.body_2 = la_body_2)
				create l_expected_local_anchor_1.make_with_coordinates (0, 0)
				assert ("local_anchor_1 valid", l_definition.local_anchor_1 ~ l_expected_local_anchor_1)
				l_expected_local_anchor_2 := la_body_2.relative_local_point (-20, -20)
				assert ("local_anchor_2 valid", l_definition.local_anchor_2 ~ l_expected_local_anchor_2)
				assert ("reference_angle valid", l_definition.reference_angle ~ -{DOUBLE_MATH}.pi_4.truncated_to_real)
				-- local_axis is relative to body_1 angle:
				assert ("local_axis valid", compare_real_32 (l_definition.local_axis.x, 0.707) and compare_real_32 (l_definition.local_axis.y, -0.707))
			else
				assert ("Bodies not valid", False)
			end
		end

	test_set_anchor_with_vector_normal
			-- Normal test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_anchor_with_vector
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.anchor",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_anchor_with_vector",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.local_anchor_1",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.local_anchor_2"
		local
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
			l_expected:REAL_32
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
				l_definition.set_anchor_with_vector (create {B2D_VECTOR_2D})
				assert ("anchor assigned", l_definition.anchor.x ~ 0 and l_definition.anchor.y ~ 0)
				-- `la_body_1' has 90 degres angle. The anchor should be at the bottom right.
				assert ("local_anchor_1 valid x", compare_real_32 (l_definition.local_anchor_1.x, 10))
				assert ("local_anchor_1 valid y", compare_real_32 (l_definition.local_anchor_1.y, -10))
				-- `la_body_2' has 45 degres angle. The anchor should be on the left.
				l_expected := -la_body_2.position.magnitude
				assert ("local_anchor_2 valid x", compare_real_32 (l_definition.local_anchor_2.x, l_expected))
				assert ("local_anchor_2 valid y", compare_real_32 (l_definition.local_anchor_2.y, 0))
			else
				assert ("Bodies not valid", False)
			end
		end

	test_set_anchor_normal
			-- Normal test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_anchor
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.anchor",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_anchor",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.local_anchor_1",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.local_anchor_2"
		local
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
			l_expected:REAL_32
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
				l_definition.set_anchor (0, 0)
				assert ("anchor assigned", l_definition.anchor.x ~ 0 and l_definition.anchor.y ~ 0)
				-- `la_body_1' has 90 degres angle. The anchor should be at the bottom right.
				assert ("local_anchor_1 valid x", compare_real_32 (l_definition.local_anchor_1.x, 10))
				assert ("local_anchor_1 valid y", compare_real_32 (l_definition.local_anchor_1.y, -10))
				-- `la_body_2' has 45 degres angle. The anchor should be on the left.
				l_expected := -la_body_2.position.magnitude
				assert ("local_anchor_2 valid x", compare_real_32 (l_definition.local_anchor_2.x, l_expected))
				assert ("local_anchor_2 valid y", compare_real_32 (l_definition.local_anchor_2.y, 0))
			else
				assert ("Bodies not valid", False)
			end
		end

	test_set_local_axis_with_vector_normal
			-- Normal test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_local_axis_with_vector
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.local_axis",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_local_axis_with_vector"
		local
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
				assert ("local_axis valid x", compare_real_32 (l_definition.local_axis.x, 1))
				assert ("local_axis valid y", compare_real_32 (l_definition.local_axis.y, 0))
				-- set_world_axis does same as b2PrismaticJoint.Initialize() :
				l_definition.set_local_axis_with_vector (create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
				assert ("set_local_axis valid x", compare_real_32 (l_definition.local_axis.x, 1))
				assert ("set_local_axis valid y", compare_real_32 (l_definition.local_axis.y, 0))
				l_definition.set_local_axis_with_vector (create {B2D_VECTOR_2D}.make_with_coordinates (3, 4))
				assert ("set_local_axis valid x", compare_real_32 (l_definition.local_axis.x, 4))
				assert ("set_local_axis valid y", compare_real_32 (l_definition.local_axis.y, -3))
				-- set_world_axis_with_angle:
				l_definition.set_local_axis_with_angle ({DOUBLE_MATH}.pi_2.truncated_to_real * 3)
				assert ("set_local_axis valid x", compare_real_32 (l_definition.local_axis.x, -1))
				assert ("set_local_axis valid y", compare_real_32 (l_definition.local_axis.y, 0))
			else
				assert ("Bodies not valid", False)
			end
		end

	test_set_local_axis_normal
			-- Normal test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_local_axis
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.local_axis",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_local_axis"
		local
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
				assert ("local_axis valid x", compare_real_32 (l_definition.local_axis.x, 1))
				assert ("local_axis valid y", compare_real_32 (l_definition.local_axis.y, 0))
				-- set_world_axis does same as b2PrismaticJoint.Initialize() :
				l_definition.set_local_axis (0, 1)
				assert ("set_local_axis valid x", compare_real_32 (l_definition.local_axis.x, 1))
				assert ("set_local_axis valid y", compare_real_32 (l_definition.local_axis.y, 0))
				l_definition.set_local_axis (3, 4)
				assert ("set_local_axis valid x", compare_real_32 (l_definition.local_axis.x, 4))
				assert ("set_local_axis valid y", compare_real_32 (l_definition.local_axis.y, -3))
				-- set_world_axis_with_angle:
				l_definition.set_local_axis_with_angle ({DOUBLE_MATH}.pi_2.truncated_to_real * 3)
				assert ("set_local_axis valid x", compare_real_32 (l_definition.local_axis.x, -1))
				assert ("set_local_axis valid y", compare_real_32 (l_definition.local_axis.y, 0))
			else
				assert ("Bodies not valid", False)
			end
		end

	test_reference_angle_normal
			-- Normal test for {B2D_JOINT_PRISMATIC_DEFINITION}.reference_angle
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.reference_angle",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.update_reference_angle"
		local
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
				assert ("reference_angle valid", l_definition.reference_angle ~ la_body_2.angle - la_body_1.angle)
				la_body_2.set_transform (10, 10, 0)
				l_definition.update_reference_angle
				assert ("update_reference_angle valid", l_definition.reference_angle ~ -{MATH_CONST}.pi_2.truncated_to_real)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_has_limit_normal
			-- Normal test for {B2D_JOINT_PRISMATIC_DEFINITION}.has_limit
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.has_limit",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_has_limit",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.enable_limit",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.disable_limit"
		local
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
				assert ("default not has_limit", not l_definition.is_limit_enabled)
				l_definition.enable_limit
				assert ("enable_limit and set_has_limit valid", l_definition.is_limit_enabled)
				l_definition.disable_limit
				assert ("disable_limit and set_has_limit valid", not l_definition.is_limit_enabled)
				l_definition.set_is_limit_enabled (True)
				assert ("set_has_limit valid True", l_definition.is_limit_enabled)
				l_definition.set_is_limit_enabled (False)
				assert ("set_has_limit valid False", not l_definition.is_limit_enabled)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_lower_limit_normal
			-- Normal test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_lower_limit
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.lower_limit",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_lower_limit"
		local
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
				l_definition.set_lower_limit (-2.456)
				assert ("set_lower_limit valid", l_definition.lower_limit ~ -2.456)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_upper_limit_normal
			-- Normal test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_upper_limit
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.upper_limit",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_upper_limit"
		local
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
				l_definition.set_upper_limit (2.456)
				assert ("set_upper_limit valid", l_definition.upper_limit ~ 2.456)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_set_limits_normal
			-- Normal test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_limits
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.lower_limit",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.upper_limit",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.limits",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_limits"
		local
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
				l_definition.set_limits (5.6, 6.2)
				assert ("lower_limit valid", l_definition.lower_limit ~ 5.6)
				assert ("upper_limit valid", l_definition.upper_limit ~ 6.2)
				assert ("limits valid", l_definition.limits.upper_limit ~ 6.2 and l_definition.limits.lower_limit ~ 5.6)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_is_motor_enabled_normal
			-- Normal test for {B2D_JOINT_PRISMATIC_DEFINITION}.is_motor_enabled
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.is_motor_enabled",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_is_motor_enabled",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.enable_motor",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.disable_motor"
		local
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
				assert ("default not is_motor_enabled", not l_definition.is_motor_enabled)
				l_definition.enable_motor
				assert ("enable_motor valid", l_definition.is_motor_enabled)
				l_definition.disable_motor
				assert ("disable_motor valid", not l_definition.is_motor_enabled)
				l_definition.set_is_motor_enabled (True)
				assert ("set_is_motor_enabled valid True", l_definition.is_motor_enabled)
				l_definition.set_is_motor_enabled (False)
				assert ("set_is_motor_enabled valid False", not l_definition.is_motor_enabled)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_max_motor_force_normal
			-- Normal test for {B2D_JOINT_PRISMATIC_DEFINITION}.max_motor_force
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.max_motor_force",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_max_motor_force"
		local
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
				l_definition.set_maximum_motor_force (50.456)
				assert ("set_max_motor_force valid", l_definition.maximum_motor_force ~ 50.456)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_motor_speed_normal
			-- Normal test for {B2D_JOINT_PRISMATIC_DEFINITION}.motor_speed
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.motor_speed",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_motor_speed"
		local
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
				l_definition.set_motor_speed (10.32)
				assert ("set_motor_speed valid", l_definition.motor_speed ~ 10.32)
			else
				assert ("Bodies not valid", False)
			end
		end

feature -- Limit test routines

	test_set_lower_limit_limit
			-- Limit test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_lower_limit
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.lower_limit",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_lower_limit"
		local
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
				l_definition.set_upper_limit (5)
				l_definition.set_lower_limit (5)
				assert ("set_lower_limit valid", l_definition.lower_limit ~ 5)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_upper_limit_limit
			-- Limit test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_upper_limit
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.upper_limit",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_upper_limit"
		local
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
				l_definition.set_lower_limit (-5)
				l_definition.set_upper_limit (-5)
				assert ("set_upper_limit valid", l_definition.upper_limit ~ -5)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_set_limits_limit
			-- Limit test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_limits
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.lower_limit",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.upper_limit",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_limits"
		local
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
				l_definition.set_limits (52.2, 52.2)
				assert ("lower_limit valid", l_definition.lower_limit ~ 52.2)
				assert ("upper_limit valid", l_definition.upper_limit ~ 52.2)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_max_motor_force_limit
			-- Limit test for {B2D_JOINT_PRISMATIC_DEFINITION}.max_motor_force
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.max_motor_force",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_max_motor_force"
		local
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
				l_definition.set_maximum_motor_force (5)
				l_definition.set_maximum_motor_force (0)
				assert ("set_max_motor_force valid", l_definition.maximum_motor_force ~ 0)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_motor_speed_limit
			-- Limit test for {B2D_JOINT_PRISMATIC_DEFINITION}.motor_speed
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.motor_speed",
					  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_motor_speed"
		local
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
				l_definition.set_motor_speed (5)
				l_definition.set_motor_speed (0)
				assert ("set_motor_speed valid", l_definition.motor_speed ~ 0)
			else
				assert ("Bodies not valid", False)
			end
		end

feature -- Wrong test routines

	test_make_wrong
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.make when arguments don't exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.make"
		local
			l_retry:INTEGER_32
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
			l_nonexistent:B2D_VECTOR_2D
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				if l_retry ~ 0 then
					la_body_1.put_null
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					assert ("a_body_1 not exists", False)
				elseif l_retry ~ 1 then
					la_body_2.put_null
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					assert ("a_body_2 not exists", False)
				elseif l_retry ~ 2 then
					l_nonexistent := la_body_1.center_of_mass
					l_nonexistent.put_null
					create l_definition.make_with_vector (la_body_1, la_body_2, l_nonexistent, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					assert ("a_anchor not exists", False)
				elseif l_retry ~ 3 then
					create l_nonexistent.make_with_coordinates (0, 1)
					l_nonexistent.put_null
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, l_nonexistent)
					assert ("a_axis not exists", False)
				end
			else
				assert ("Bodies not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := l_retry + 1
				retry
			end
		end

	test_anchor_wrong
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.anchor when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.anchor"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
			l_discard:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.put_null
					l_discard := l_definition.anchor
					assert ("anchor is valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_anchor_with_vector_wrong
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_anchor_with_vector when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_anchor_with_vector"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.put_null
					l_definition.set_anchor_with_vector (create {B2D_VECTOR_2D})
					assert ("set_anchor valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_anchor_wrong
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_anchor when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_anchor"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.put_null
					l_definition.set_anchor (0, 0)
					assert ("set_anchor valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_local_anchor_wrong
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.local_anchor_* when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}."
		local
			l_retry:REAL_32
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
			l_discard:B2D_VECTOR_2D
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
				l_definition.put_null
				if l_retry ~ 0 then
					l_discard := l_definition.local_anchor_1
					assert ("local_anchor_1 valid", False)
				elseif l_retry ~ 1 then
					l_discard := l_definition.local_anchor_2
					assert ("local_anchor_2 valid", False)
				end
			else
				assert ("Bodies not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := l_retry + 1
				retry
			end
		end

	test_local_axis_wrong
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.local_axis when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.local_axis"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
			l_discard:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.put_null
					l_discard := l_definition.local_axis
					assert ("local_axis valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_local_axis_with_vector_wrong_1
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_local_axis_with_vector when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_local_axis_with_vector"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.put_null
					l_definition.set_local_axis_with_vector (create {B2D_VECTOR_2D})
					assert ("set_local_axis valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_local_axis_wrong_1
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_local_axis when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_local_axis"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.put_null
					l_definition.set_local_axis (0, 0)
					assert ("set_local_axis valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_local_axis_with_vector_wrong_2
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_local_axis_with_vector when arguments do not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_local_axis_with_vector"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
			l_vector:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					create l_vector.make_with_coordinates (1, 0)
					l_vector.put_null
					l_definition.set_local_axis_with_vector (l_vector)
					assert ("set_local_axis_valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_local_axis_with_vector_wrong_3
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_local_axis_with_vector with axis of (0,0).
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_local_axis_with_vector"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.set_local_axis_with_vector (create {B2D_VECTOR_2D})
					assert ("set_local_axis_valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_reference_angle_wrong
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.reference_angle when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.reference_angle"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
			l_discard:REAL_32
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.put_null
					l_discard := l_definition.reference_angle
					assert ("reference_angle valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_update_reference_angle_wrong
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.update_reference_angle when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.update_reference_angle"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.put_null
					l_definition.update_reference_angle
					assert ("update_reference_angle valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_has_limit_wrong
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.has_limit when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.has_limit"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
			l_discard:BOOLEAN
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.put_null
					l_discard := l_definition.is_limit_enabled
					assert ("has_limit valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_has_limit_wrong
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_has_limit when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_has_limit"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.put_null
					l_definition.set_is_limit_enabled (False)
					assert ("set_has_limit valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_enable_limit_wrong
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.enable_limit when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.enable_limit"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.put_null
					l_definition.enable_limit
					assert ("enale_limit valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_disable_limit_wrong
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.disable_limit when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.disable_limit"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.put_null
					l_definition.disable_limit
					assert ("disable_limit valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_lower_limit_wrong
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.lower_limit when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.lower_limit"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
			l_discard:REAL_32
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.put_null
					l_discard := l_definition.lower_limit
					assert ("lower_limit valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_lower_limit_wrong_1
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_lower_limit when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_lower_limit"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.put_null
					l_definition.set_lower_limit (-1)
					assert ("set_lower_limit valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_lower_limit_wrong_2
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_lower_limit when `a_limit' is higher than `upper_limit'.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.lower_limit"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.set_lower_limit (5)
					assert ("set_lower_limit valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_upper_limit_wrong
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.upper_limit when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.upper_limit"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
			l_discard:REAL_32
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.put_null
					l_discard := l_definition.upper_limit
					assert ("upper_limit valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_upper_limit_wrong_1
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_upper_limit when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_upper_limit"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.put_null
					l_definition.set_upper_limit (-1)
					assert ("set_upper_limit valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_upper_limit_wrong_2
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_upper_limit when `a_limit' is less than `lower_limit'.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.upper_limit"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.set_upper_limit (-5)
					assert ("set_upper_limit valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_limits_wrong_1
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_limits when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_limits"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.put_null
					l_definition.set_limits (0, 2)
					assert ("set_limits valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_limits_wrong_2
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_limits when `a_lower' is higher than `a_upper'.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_limits"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.set_limits (2, 1)
					assert ("set_limits valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_is_motor_enabled_wrong
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.is_motor_enabled when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.is_motor_enabled"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
			l_discard:BOOLEAN
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.put_null
					l_discard := l_definition.is_motor_enabled
					assert ("is_motor_enabled valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_is_motor_enabled_wrong
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_is_motor_enabled when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_is_motor_enabled"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.put_null
					l_definition.set_is_motor_enabled (True)
					assert ("set_is_motor_enabled valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_enable_motor_wrong
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.enable_motor when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.enable_motor"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.put_null
					l_definition.enable_motor
					assert ("enable_motor valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_disable_motor_wrong
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.disable_motor when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.disable_motor"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.put_null
					l_definition.disable_motor
					assert ("disable_motor valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_max_motor_force_wrong
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.max_motor_force when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.max_motor_force"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
			l_discard:REAL_32
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.put_null
					l_discard := l_definition.maximum_motor_force
					assert ("max_motor_force valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_max_motor_force_wrong_1
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_max_motor_force when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_max_motor_force"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.put_null
					l_definition.set_maximum_motor_force (5)
					assert ("set_max_motor_force valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_motor_speed_wrong
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.motor_speed when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.motor_speed"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
			l_discard:REAL_32
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.put_null
					l_discard := l_definition.motor_speed
					assert ("motor_speed valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_motor_speed_wrong_1
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_motor_speed when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_motor_speed"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.put_null
					l_definition.set_motor_speed (5)
					assert ("set_motor_speed valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_motor_speed_wrong_2
			-- Test for {B2D_JOINT_PRISMATIC_DEFINITION}.set_motor_speed when `a_speed' is negative.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC_DEFINITION}.set_motor_speed"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass, create {B2D_VECTOR_2D}.make_with_coordinates (0, 1))
					l_definition.set_motor_speed (-0.1)
					assert ("set_motor_speed valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

end
