note
	description: "Tests for {B2D_JOINT_MOUSE_DEFINITION}"
	author: "Patrick Boucher"
	date: "Sat, 27 Jun 2020 16:55:46 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_JOINT_MOUSE_DEFINITION_TESTS

inherit
	EQA_TEST_SET
		redefine
			on_prepare
		end
	B2D_ANY
		undefine
			default_create
		end

feature {NONE} -- Implementation

	on_prepare
			-- <Precursor>
		local
			l_body_definition:B2D_BODY_DEFINITION
		do
			create world.make (0, -9.8)
			create l_body_definition
			l_body_definition.set_position (-10, -10)
			world.create_body (l_body_definition)
			body_1 := world.last_body
			l_body_definition.set_position (10, 10)
			world.create_body (l_body_definition)
			body_2 := world.last_body
		end

	world:B2D_WORLD
			-- The world in which the tests will be performed.

	body_1:detachable B2D_BODY
			-- The first body of each {B2D_JOINT_MOUSE_DEFINITION} during the tests.

	body_2:detachable B2D_BODY
			-- The second body of each {B2D_JOINT_MOUSE_DEFINITION} during the tests.

feature -- Normal test routines

	test_make_normal
			-- Normal test for {B2D_JOINT_MOTOR_DEFINITION}.make
		note
			testing:
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_MOUSE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2)
				assert ("Body 1 assigned", l_definition.body_1 ~ la_body_1)
				assert ("Body 2 assigned", l_definition.body_2 ~ la_body_2)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_target_anchor_position_normal
			-- Normal test for {B2D_JOINT_MOTOR_DEFINITION}.target_anchor_position
		note
			testing:
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.target_anchor_position",
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.set_target_anchor_position"
		local
			l_definition:B2D_JOINT_MOUSE_DEFINITION
			l_vector:B2D_VECTOR_2D
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2)
				create l_vector.make_with_coordinates (5.5, -2.25)
				l_definition.set_target_anchor_position (l_vector)
				assert ("set_target_anchor_position valid x", l_definition.target_anchor_position.x ~ 5.5)
				assert ("set_target_anchor_position valid y", l_definition.target_anchor_position.y ~ -2.25)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_max_force_normal
			-- Normal test for {B2D_JOINT_MOTOR_DEFINITION}.max_force
		note
			testing:
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.max_force",
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.set_max_force",
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.set_max_force_with_weight_multiplier"
		local
			l_definition:B2D_JOINT_MOUSE_DEFINITION
			l_weight:REAL_32
			l_multiplier:INTEGER_32
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2)
				-- set_max_force:
				l_definition.set_maximum_force (45.6235)
				assert ("set_max_force valid", l_definition.maximum_force ~ 45.6235)
				-- set_max_force_with_weight_multiplier:
				l_weight := la_body_1.mass * la_body_1.world.gravity.y.abs
				assert ("weight valid", l_weight ~ 9.8) -- 1 mass * 9.8 gravity
				l_multiplier := 2
				l_definition.set_maximum_force_with_weight_multiplier (la_body_1, l_multiplier)
				assert ("set_max_force_with_weight_multiplier valid", l_definition.maximum_force ~ l_weight * l_multiplier)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_frequency_normal
			-- Normal test for {B2D_JOINT_MOTOR_DEFINITION}.frequency
		note
			testing:
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.frequency",
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.set_frequency"
		local
			l_definition:B2D_JOINT_MOUSE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2)
				l_definition.set_frequency (30.2)
				assert ("set_frequency valid", l_definition.frequency ~ 30.2)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_damping_ratio_normal
			-- Normal test for {B2D_JOINT_MOTOR_DEFINITION}.damping_ratio
		note
			testing:
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.damping_ratio",
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.set_damping_ratio"
		local
			l_definition:B2D_JOINT_MOUSE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2)
				l_definition.set_damping_ratio (2.5)
				assert ("set_damping_ratio valid", l_definition.damping_ratio ~ 2.5)
			else
				assert ("Bodies not valid", False)
			end
		end

feature -- Limit test routines

	test_set_max_force_limit
			-- Limit test for {B2D_JOINT_MOTOR_DEFINITION}.set_max_force
		note
			testing:
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.max_force",
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.set_max_force"
		local
			l_definition:B2D_JOINT_MOUSE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2)
				l_definition.set_maximum_force (0.0)
				assert ("set_max_force valid", l_definition.maximum_force ~ 0.0)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_set_max_force_with_weight_multiplier_limit
			-- Limit test for {B2D_JOINT_MOTOR_DEFINITION}.set_max_force_with_weight_multiplier
		note
			testing:
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.max_force",
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.set_max_force_with_weight_multiplier"
		local
			l_definition:B2D_JOINT_MOUSE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2)
				l_definition.set_maximum_force_with_weight_multiplier (la_body_1, 0)
				assert ("set_max_force_with_weight_multiplier valid", l_definition.maximum_force ~ 0.0)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_set_frequency_limit
			-- Limit test for {B2D_JOINT_MOTOR_DEFINITION}.set_frequency
		note
			testing:
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.frequency",
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.set_frequency"
		local
			l_definition:B2D_JOINT_MOUSE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2)
				l_definition.set_frequency (0.0)
				assert ("set_frequency valid", l_definition.frequency ~ 0.0)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_set_damping_ratio_limit
			-- Limit test for {B2D_JOINT_MOTOR_DEFINITION}.set_damping_ratio
		note
			testing:
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.damping_ratio",
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.set_damping_ratio"
		local
			l_definition:B2D_JOINT_MOUSE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2)
				l_definition.set_damping_ratio (0.0)
				assert ("set_damping_ratio valid", l_definition.damping_ratio ~ 0.0)
			else
				assert ("Bodies not valid", False)
			end
		end

feature -- Wrong test routines

	test_make_wrong_1
			 -- Test for {B2D_JOINT_MOTOR_DEFINITION}.make when `a_body_1' does not exist.
		note
			testing:
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.make"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOUSE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					la_body_1.put_null
					create l_definition.make (la_body_1, la_body_2)
					assert ("make valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_make_wrong_2
			 -- Test for {B2D_JOINT_MOTOR_DEFINITION}.make when `a_body_2' does not exists.
		note
			testing:
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.make"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOUSE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					la_body_2.put_null
					create l_definition.make (la_body_1, la_body_2)
					assert ("make valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_target_anchor_position_wrong
			 -- Test for {B2D_JOINT_MOTOR_DEFINITION}.target_anchor_position when `item' does not exist.
		note
			testing:
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.target_anchor_position"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOUSE_DEFINITION
			l_discard:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					l_definition.put_null
					l_discard := l_definition.target_anchor_position
					assert ("target_anchor_position valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_target_anchor_position_wrong_1
			 -- Test for {B2D_JOINT_MOTOR_DEFINITION}.set_target_anchor_position when
			 -- `item' does not exist.
		note
			testing:
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.set_target_anchor_position"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOUSE_DEFINITION
			l_vector:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					l_definition.put_null
					create l_vector
					l_definition.set_target_anchor_position (l_vector)
					assert ("set_target_anchor_position valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_target_anchor_position_wrong_2
			 -- Test for {B2D_JOINT_MOTOR_DEFINITION}.set_target_anchor_position when
			 -- `a_world_position' does not exist.
		note
			testing:
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.set_target_anchor_position"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOUSE_DEFINITION
			l_vector:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					create l_vector
					l_vector.put_null
					l_definition.set_target_anchor_position (l_vector)
					assert ("set_target_anchor_position valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_max_force_wrong
			 -- Test for {B2D_JOINT_MOTOR_DEFINITION}.max_force when `item' does not exist.
		note
			testing:
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.max_force"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOUSE_DEFINITION
			l_discard:REAL_32
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					l_definition.put_null
					l_discard := l_definition.maximum_force
					assert ("max_force valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_max_force_wrong_1
			 -- Test for {B2D_JOINT_MOTOR_DEFINITION}.set_max_force when `item' does not exist.
		note
			testing:
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.set_max_force"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOUSE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					l_definition.put_null
					l_definition.set_maximum_force (45.2)
					assert ("set_max_force valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_max_force_wrong_2
			 -- Test for {B2D_JOINT_MOTOR_DEFINITION}.set_max_force with a negative `a_force'.
		note
			testing:
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.set_max_force"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOUSE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					l_definition.set_maximum_force (-0.1)
					assert ("set_max_force valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_max_force_with_weight_multiplier_wrong_1
			 -- Test for {B2D_JOINT_MOTOR_DEFINITION}.set_max_force_with_weight_multiplier
			 -- when `item' does not exist.
		note
			testing:
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.set_max_force_with_weight_multiplier"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOUSE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					l_definition.put_null
					l_definition.set_maximum_force_with_weight_multiplier (la_body_1, 1)
					assert ("set_max_force_with_weight_multiplier valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_max_force_with_weight_multiplier_wrong_2
			 -- Test for {B2D_JOINT_MOTOR_DEFINITION}.set_max_force_with_weight_multiplier
			 -- when `a_body' does not exist.
		note
			testing:
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.set_max_force_with_weight_multiplier"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOUSE_DEFINITION
			l_body_definition:B2D_BODY_DEFINITION
			l_body:B2D_BODY
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					create l_body_definition.make_dynamic
					world.create_body (l_body_definition)
					l_body := world.bodies.at (3)
					l_body.put_null
					l_definition.set_maximum_force_with_weight_multiplier (l_body, 1)
					assert ("set_max_force_with_weight_multiplier valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_max_force_with_weight_multiplier_wrong_3
			 -- Test for {B2D_JOINT_MOTOR_DEFINITION}.set_max_force_with_weight_multiplier
			 -- with a negative `a_multiplier'.
		note
			testing:
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.set_max_force_with_weight_multiplier"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOUSE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					l_definition.set_maximum_force_with_weight_multiplier (la_body_1, -1)
					assert ("set_max_force_with_weight_multiplier valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_frequency_wrong_1
			 -- Test for {B2D_JOINT_MOTOR_DEFINITION}.set_frequency when `item' does not exist.
		note
			testing:
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.set_frequency"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOUSE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					l_definition.put_null
					l_definition.set_frequency (20.65)
					assert ("set_frequency valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_frequency_wrong_2
			 -- Test for {B2D_JOINT_MOTOR_DEFINITION}.set_frequency with a negative `a_frequency'.
		note
			testing:
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.set_frequency"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOUSE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					l_definition.set_frequency (-0.1)
					assert ("set_frequency valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_damping_ratio_wrong_1
			 -- Test for {B2D_JOINT_MOTOR_DEFINITION}.set_damping_ratio when `item' does not exist.
		note
			testing:
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.set_damping_ratio"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOUSE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					l_definition.put_null
					l_definition.set_damping_ratio (1)
					assert ("set_damping ratio valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_damping_ratio_wrong_2
			 -- Test for {B2D_JOINT_MOTOR_DEFINITION}.set_damping_ratio with a negative `a_ratio'.
		note
			testing:
				"covers/{B2D_JOINT_MOUSE_DEFINITION}.set_damping_ratio"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOUSE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					l_definition.set_damping_ratio (-0.1)
					assert ("set_damping_ratio valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

end


