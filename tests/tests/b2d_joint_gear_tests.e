note
	description: "Tests for {B2D_JOINT_GEAR}"
	author: "Louis Marchand"
	date: "Thu, 02 Jul 2020 16:19:12 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_JOINT_GEAR_TESTS

inherit
	EQA_TEST_SET
		redefine
			on_prepare
		end
	B2D_ANY
		undefine
			default_create
		end

feature {NONE} -- Events

	on_prepare
			-- <Precursor>
		local
			l_definition:B2D_JOINT_GEAR_DEFINITION
			l_body_definition:B2D_BODY_DEFINITION
			l_shape:B2D_SHAPE_CIRCLE
		do
			create world.make (0, -10)
			create l_body_definition.make_dynamic
			l_body_definition.set_position (-10, 0)
			world.create_body (l_body_definition)
			check attached world.last_body as la_body_1 then
				create l_shape.make_with_coordinates (-10, 0, 2)
				la_body_1.create_fixture_from_shape (l_shape, 5)
				create l_body_definition.make_dynamic
				l_body_definition.set_position (10, 0)
				world.create_body (l_body_definition)
				check attached world.last_body as la_body_2 then
					body_1 := la_body_2
					create l_shape.make_with_coordinates (10, 0, 2)
					la_body_2.create_fixture_from_shape (l_shape, 5)
					create l_body_definition.make_dynamic
					l_body_definition.set_position (0, 10)
					world.create_body (l_body_definition)
					check attached world.last_body as la_body_3 then
						body_2 := la_body_3
						create l_shape.make_with_coordinates (0, 10, 2)
						la_body_3.create_fixture_from_shape (l_shape, 5)
						world.create_joint (create {B2D_JOINT_REVOLUTE_DEFINITION}.make_with_vector (la_body_1, la_body_2, la_body_1.center_of_mass))
						check attached {B2D_JOINT_GEARABLE} world.last_joint as la_joint_1 then
							joint_1 := la_joint_1
							world.create_joint (create {B2D_JOINT_REVOLUTE_DEFINITION}.make_with_vector (la_body_2, la_body_3, la_body_3.center_of_mass))
							check attached {B2D_JOINT_GEARABLE} world.last_joint as la_joint_2 then
								joint_2 := la_joint_2
								create l_definition.make (joint_1, joint_2)
								world.create_joint (l_definition)
								check attached {B2D_JOINT_GEAR} world.last_joint as la_gear_joint then
									joint := la_gear_joint
								end
							end
						end
					end
				end
			end
		end

feature -- Test routines

	test_anchor_normal
			-- Test routine for `anchor_*'
		note
			testing:
					"covers/{B2D_JOINT_GEAR}.anchor_1",
					"covers/{B2D_JOINT_GEAR}.anchor_2"
		do
			assert ("anchor_1 X", joint.anchor_1.x ~ -20)
			assert ("anchor_1 Y", joint.anchor_1.y ~ 0)
			assert ("anchor_2 X", joint.anchor_2.x ~ 0)
			assert ("anchor_2 Y", joint.anchor_2.y ~ 20)
		end

	test_is_active_normal
			-- Test routine for `is_active'
		note
			testing:
					"covers/{B2D_JOINT_GEAR}.is_active",
					"covers/{B2D_BODY}.activate",
					"covers/{B2D_BODY}.deactivate"
		do
			assert ("Default is_active valid", joint.is_active)
			body_1.deactivate
			assert ("Body 1 Desactivate is_active valid", not joint.is_active)
			body_1.activate
			assert ("Default is_active valid", joint.is_active)
			body_2.deactivate
			assert ("Body 1 Desactivate is_active valid", not joint.is_active)
			body_1.deactivate
			body_2.deactivate
			assert ("Body 1 Desactivate is_active valid", not joint.is_active)
		end

	test_body_normal
			-- Test routine for `body_*'
		note
			testing:
					"covers/{B2D_JOINT_GEAR}.body_1",
					"covers/{B2D_JOINT_GEAR}.body_2"
		do
			assert ("body_1 valid", joint.body_1 ~ body_1)
			assert ("body_2 valid", joint.body_2 ~ body_2)
		end

	test_reaction_force_normal
			-- Test routine for `reaction_force'
		note
			testing:
					"covers/{B2D_JOINT_GEAR}.reaction_force",
					"covers/{B2D_WORLD}.step",
					"covers/{B2D_BODY}.position"
		do
			joint.set_ratio (10)
			world.step
			assert ("reaction_force valid X", joint.reaction_force (60).x ~ 0)
			assert ("reaction_force valid Y", joint.reaction_force (60).y ~ 0)
		end

	test_reaction_torque_normal
			-- Test routine for `reaction_torque'
		note
			testing:
					"covers/{B2D_JOINT_GEAR}.reaction_torque"
		do
			joint.set_ratio (2)
			world.step
			assert ("reaction_torque valid", joint.reaction_torque (60) ~ 0)
		end

	test_is_collide_connected_normal
			-- Test routine for `is_collide_connected'
		note
			testing:
					"covers/{B2D_JOINT_GEAR}.is_collide_connected"
		do
			assert ("is_collide_connected valid", not joint.is_collide_connected)
		end

	test_joints_normal
			-- Normal test for {B2D_JOINT_GEAR}.`joint_1' and {B2D_JOINT_GEAR}.`joint_2'
		note
			testing:  	"covers/{B2D_JOINT_GEAR}.joint_1", "covers/{B2D_JOINT_GEAR}.joint_2"
		do
			assert("joint_1 valid.", joint.joint_1 ~ joint_1)
			assert("joint_2 valid.", joint.joint_2 ~ joint_2)
		end

	test_ratio_normal
			-- Normal test for {B2D_JOINT_GEAR}.`ratio'
		note
			testing:  	"covers/{B2D_JOINT_GEAR}.ratio", "covers/{B2D_JOINT_GEAR}.set_ratio"
		do
			assert("ratio default value.", joint.ratio ~ 1.0)
			joint.set_ratio (2.0)
			assert("set_ratio valid.", joint.ratio ~ 2.0)
			joint.set_ratio (0.0)
			assert("set_ratio valid.", joint.ratio ~ 0.0)
		end

feature -- Limit test case


	test_reaction_force_limit
			-- Limit test routine for `reaction_force'
		note
			testing:
					"covers/{B2D_JOINT_GEAR}.reaction_force",
					"covers/{B2D_WORLD}.step"
		do
			assert ("reaction_force default valid X", joint.reaction_force (0).x ~ 0)
			assert ("reaction_force default valid Y", joint.reaction_force (0).y ~ 0)
			world.step
			assert ("reaction_force valid X", joint.reaction_force (0).x ~ 0)
			assert ("reaction_force valid Y", joint.reaction_force (0).y ~ 0)
		end

	test_reaction_torque_limit
			-- Limit test routine for `reaction_torque'
		note
			testing:
					"covers/{B2D_JOINT_GEAR}.reaction_torque",
					"covers/{B2D_WORLD}.step"
		do
			assert ("reaction_torque valid", joint.reaction_torque (0) ~ 0)
			world.step
			assert ("reaction_torque valid", joint.reaction_torque (0) ~ 0)
		end

feature -- Wrong test case

	test_anchor_1_wrong
			-- Test for {B2D_JOINT_GEAR}.anchor_1 when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_GEAR}.anchor_1"
		local
			l_retry:BOOLEAN
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				joint.put_null
				l_result := joint.anchor_1
				assert ("joint.anchor_1 on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_anchor_2_wrong
			-- Test for {B2D_JOINT_GEAR}.anchor_2 when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_GEAR}.anchor_2"
		local
			l_retry:BOOLEAN
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				joint.put_null
				l_result := joint.anchor_2
				assert ("joint.anchor_2 on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_is_collide_connected_wrong
			-- Test for {B2D_JOINT_GEAR}.is_collide_connected when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_GEAR}.is_collide_connected"
		local
			l_retry:BOOLEAN
			l_result:BOOLEAN
		do
			if not l_retry then
				joint.put_null
				l_result := joint.is_collide_connected
				assert ("joint.is_collide_connected on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_ratio_wrong
			-- Wrong test for {B2D_JOINT_GEAR}.`ratio'
		note
			testing:
				"covers/{B2D_JOINT_GEAR}.ratio"
		local
			l_retry:BOOLEAN
			l_result:REAL_32
		do
			if not l_retry then
				joint.put_null
				l_result := joint.ratio
				assert("ratio valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_ratio_wrong
			-- Wrong test for {B2D_JOINT_GEAR}.`set_ratio'
		note
			testing:
				"covers/{B2D_JOINT_GEAR}.set_ratio"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				joint.put_null
				joint.set_ratio(0.0)
				assert("set_ratio valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_reaction_force_wrong
			-- Wrong test for `reaction_force'
		note
			testing:
				"covers/{B2D_JOINT_GEAR}.reaction_force"
		local
			l_retry:BOOLEAN
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				joint.put_null
				l_result := joint.reaction_force (60)
				assert("reaction_force valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_reaction_torque_wrong
			-- Wrong test for `reaction_torque'
		note
			testing:
				"covers/{B2D_JOINT_GEAR}.reaction_torque"
		local
			l_retry:BOOLEAN
			l_result:REAL_32
		do
			if not l_retry then
				joint.put_null
				l_result := joint.reaction_torque (60)
				assert("reaction_torque valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end


feature {NONE} -- Implementation

	world:B2D_WORLD
			-- The {B2D_WORLD} containing `joint'

	body_1, body_2: B2D_BODY
			-- The {B2D_BODY} used to create `joint'

	joint_1, joint_2:B2D_JOINT_GEARABLE
			-- {B2D_JOINT} used in `joint'

	joint:B2D_JOINT_GEAR
			-- The object to test in `Current'

end


