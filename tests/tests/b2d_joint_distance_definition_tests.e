note
	description: "Tests for {B2D_JOINT_DISTANCE_DEFINITION}"
	author: "Louis Marchand"
	date: "Wed, 24 Jun 2020 14:14:14 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_JOINT_DISTANCE_DEFINITION_TESTS

inherit
	EQA_TEST_SET
		redefine
			on_prepare
		end
	B2D_ANY
		undefine
			default_create
		end

feature {NONE} -- Initialisation

	on_prepare
			-- <Precursor>
		local
			l_body_definition:B2D_BODY_DEFINITION
		do
			create world.make (0, -10)
			create l_body_definition.make_dynamic
			l_body_definition.position.set_coordinates (10, 5)
			world.create_body (l_body_definition)
			body_1 := world.last_body
			l_body_definition.position.set_coordinates (-10, -5)
			world.create_body (l_body_definition)
			body_2 := world.last_body
		end

feature -- Test routines

	test_make_normal
			-- Normal test for make
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.body_1",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.body_2",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.local_anchor_1",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.local_anchor_2"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
				assert("make valid: body_1", l_definition.body_1 ~ la_body_1)
				assert("make valid: body_2", l_definition.body_2 ~ la_body_2)
				assert("make valid: anchor_1.x", l_definition.local_anchor_1.x ~ 0)
				assert("make valid: anchor_1.y", l_definition.local_anchor_1.y ~ 0)
				assert("make valid: anchor_2.x", l_definition.local_anchor_1.x ~ 0)
				assert("make valid: anchor_2.y", l_definition.local_anchor_1.y ~ 0)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_make_with_vector_normal
			-- Normal test for make_with_vector
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make_with_vector",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.body_1",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.body_2",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.local_anchor_1",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.local_anchor_2"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_vector_1, l_vector_2:B2D_VECTOR_2D
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_vector_1.make_with_coordinates (10, 5)
				create l_vector_2.make_with_coordinates (-10, -5)
				create l_definition.make_with_vector (la_body_1, la_body_2, l_vector_1, l_vector_2)
				assert("make valid: body_1", l_definition.body_1 ~ la_body_1)
				assert("make valid: body_2", l_definition.body_2 ~ la_body_2)
				assert("make valid: anchor_1.x", l_definition.local_anchor_1.x ~ 0)
				assert("make valid: anchor_1.y", l_definition.local_anchor_1.y ~ 0)
				assert("make valid: anchor_2.x", l_definition.local_anchor_1.x ~ 0)
				assert("make valid: anchor_2.y", l_definition.local_anchor_1.y ~ 0)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_local_anchor_1_normal
			-- Normal test for local_anchor_1
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_local_anchor_1",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_local_anchor_1_with_vector",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_world_anchor_1",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_world_anchor_1_with_vector",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.local_anchor_1",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
				assert("local_anchor_1 valid x", l_definition.local_anchor_1.x ~ 0)
				assert("local_anchor_1 valid y", l_definition.local_anchor_1.y ~ 0)
				l_definition.set_local_anchor_1 (-6.39732, 4.97371)
				assert("set_local_anchor_1 valid x", l_definition.local_anchor_1.x ~ -6.39732)
				assert("set_local_anchor_1 valid y", l_definition.local_anchor_1.y ~ 4.97371)
				l_definition.set_local_anchor_1_with_vector (create {B2D_VECTOR_2D})
				assert("set_local_anchor_1_with_vector valid x", l_definition.local_anchor_1.x ~ 0)
				assert("set_local_anchor_1_with_vector valid y", l_definition.local_anchor_1.y ~ 0)
				l_definition.set_world_anchor_1_with_vector (create {B2D_VECTOR_2D})
				assert("set_world_anchor_1_with_vector valid x", l_definition.local_anchor_1.x ~ -10)
				assert("set_world_anchor_1_with_vector valid y", l_definition.local_anchor_1.y ~ -5)
				l_definition.set_world_anchor_1 (-10, -5)
				assert("set_world_anchor_1 valid x", l_definition.local_anchor_1.x ~ -20)
				assert("set_world_anchor_1 valid y", l_definition.local_anchor_1.y ~ -10)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_local_anchor_2_normal
			-- Normal test for local_anchor_2
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_local_anchor_2",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_local_anchor_2_with_vector",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_world_anchor_2",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_world_anchor_2_with_vector",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.local_anchor_2",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
				assert("local_anchor_2 valid x", l_definition.local_anchor_2.x ~ 0)
				assert("local_anchor_2 valid y", l_definition.local_anchor_2.y ~ 0)
				l_definition.set_local_anchor_2 (-6.39732, 4.97371)
				assert("set_local_anchor_2 valid x", l_definition.local_anchor_2.x ~ -6.39732)
				assert("set_local_anchor_2 valid y", l_definition.local_anchor_2.y ~ 4.97371)
				l_definition.set_local_anchor_2_with_vector (create {B2D_VECTOR_2D})
				assert("set_local_anchor_2_with_vector valid x", l_definition.local_anchor_2.x ~ 0)
				assert("set_local_anchor_2_with_vector valid y", l_definition.local_anchor_2.y ~ 0)
				l_definition.set_world_anchor_2_with_vector (create {B2D_VECTOR_2D})
				assert("set_world_anchor_2_with_vector valid x", l_definition.local_anchor_2.x ~ 10)
				assert("set_world_anchor_2_with_vector valid y", l_definition.local_anchor_2.y ~ 5)
				l_definition.set_world_anchor_2 (10, 5)
				assert("set_world_anchor_2 valid x", l_definition.local_anchor_2.x ~ 20)
				assert("set_world_anchor_2 valid y", l_definition.local_anchor_2.y ~ 10)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_length_normal
			-- Normal test for set_length
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_length",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.length",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
				l_definition.set_length (20)
				assert("set_length valid", l_definition.length ~ 20)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_frequency_normal
			-- Normal test for set_frequency
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_frequency",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.frequency",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
				l_definition.set_frequency (7.56358)
				assert("set_frequency valid", l_definition.frequency ~ 7.56358)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_frequency_limit
			-- Limit test for set_frequency
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_frequency",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.frequency",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
				l_definition.set_frequency (0)
				assert("set_frequency valid", l_definition.frequency ~ 0)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_damping_ratio_normal
			-- Normal test for set_damping_ratio
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_damping_ratio",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.damping_ratio",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
				l_definition.set_damping_ratio (0.91845)
				assert("set_damping_ratio valid", l_definition.damping_ratio ~ 0.91845)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_damping_ratio_limit
			-- Limit test for set_damping_ratio
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_damping_ratio",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.damping_ratio",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
				l_definition.set_damping_ratio (0)
				assert("set_damping_ratio valid", l_definition.damping_ratio ~ 0)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_body_1_normal
			-- Normal test for body_1
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.body_1",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_body_1",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
				world.create_body (create {B2D_BODY_DEFINITION})
				if attached world.last_body as la_body then
					l_definition.set_body_1 (la_body)
					assert("set_body_1 valid", l_definition.body_1 ~ la_body)
				else
					assert ("Cannot create body", False)
				end
			else
				assert ("Bodies not valid", False)
			end
		end

	test_body_2_normal
			-- Normal test for body_2
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.body_2",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_body_2",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
				world.create_body (create {B2D_BODY_DEFINITION})
				if attached world.last_body as la_body then
					l_definition.set_body_2 (la_body)
					assert("set_body_2 valid", l_definition.body_2 ~ la_body)
				else
					assert ("Cannot create body", False)
				end
			else
				assert ("Bodies not valid", False)
			end
		end

	test_must_collide_connected_normal
			-- Normal test for must_collide_connected
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.must_collide_connected",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_must_collide_connected",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.enable_collide_connected",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.disable_collide_connected",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
				assert("must_collide_connected default", not l_definition.must_collide_connected)
				l_definition.enable_collide_connected
				assert("enable_collide_connected valid", l_definition.must_collide_connected)
				l_definition.disable_collide_connected
				assert("disable_collide_connected valid", not l_definition.must_collide_connected)
				l_definition.set_must_collide_connected (True)
				assert("set_must_collide_connected valid", l_definition.must_collide_connected)
				l_definition.set_must_collide_connected (False)
				assert("set_must_collide_connected valid", not l_definition.must_collide_connected)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_local_anchor_1_wrong
			-- Wrong test for local_anchor_1
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.local_anchor_1",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.put_null
					l_result := l_definition.local_anchor_1
					assert("local_anchor_1 valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_local_anchor_2_wrong
			-- Wrong test for local_anchor_2
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.local_anchor_2",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.put_null
					l_result := l_definition.local_anchor_2
					assert("local_anchor_2 valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_local_anchor_1_with_vector_wrong_1
			-- Wrong test 1 for set_local_anchor_1_with_vector
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_local_anchor_1_with_vector",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.put_null
					l_definition.set_local_anchor_1_with_vector(create {B2D_VECTOR_2D})
					assert("set_local_anchor_1_with_vector valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_local_anchor_1_with_vector_wrong_2
			-- Wrong test 2 for set_local_anchor_1_with_vector
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_local_anchor_1_with_vector",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.set_local_anchor_1_with_vector(create {B2D_VECTOR_2D}.own_from_item (create {POINTER}))
					assert("set_local_anchor_1_with_vector valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_local_anchor_2_with_vector_wrong_1
			-- Wrong test 2 for set_local_anchor_2_with_vector
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_local_anchor_2_with_vector",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.put_null
					l_definition.set_local_anchor_2_with_vector(create {B2D_VECTOR_2D})
					assert("set_local_anchor_2_with_vector valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_local_anchor_2_with_vector_wrong_2
			-- Wrong test 2 for set_local_anchor_2_with_vector
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_local_anchor_2_with_vector",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.set_local_anchor_2_with_vector(create {B2D_VECTOR_2D}.own_from_item (create {POINTER}))
					assert("set_local_anchor_2_with_vector valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_local_anchor_1_wrong
			-- Wrong test for set_local_anchor_1
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_local_anchor_1",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.put_null
					l_definition.set_local_anchor_1(0,0)
					assert("set_local_anchor_1 valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_local_anchor_2_wrong
			-- Wrong test for set_local_anchor_2
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_local_anchor_2",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.put_null
					l_definition.set_local_anchor_2(0,0)
					assert("set_local_anchor_2 valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_world_anchor_1_with_vector_wrong_1
			-- Wrong test 1 for set_world_anchor_1_with_vector
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_world_anchor_1_with_vector",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.put_null
					l_definition.set_world_anchor_1_with_vector(create {B2D_VECTOR_2D})
					assert("set_world_anchor_1_with_vector valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_world_anchor_1_with_vector_wrong_2
			-- Wrong test 2 for set_world_anchor_1_with_vector
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_world_anchor_1_with_vector",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.set_world_anchor_1_with_vector(create {B2D_VECTOR_2D}.own_from_item (create {POINTER}))
					assert("set_world_anchor_1_with_vector valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_world_anchor_2_with_vector_wrong_1
			-- Wrong test 2 for set_world_anchor_2_with_vector
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_world_anchor_2_with_vector",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.put_null
					l_definition.set_world_anchor_2_with_vector(create {B2D_VECTOR_2D})
					assert("set_world_anchor_2_with_vector valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_world_anchor_2_with_vector_wrong_2
			-- Wrong test 2 for set_world_anchor_2_with_vector
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_world_anchor_2_with_vector",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.set_world_anchor_2_with_vector(create {B2D_VECTOR_2D}.own_from_item (create {POINTER}))
					assert("set_world_anchor_2_with_vector valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_world_anchor_1_wrong
			-- Wrong test for set_world_anchor_1
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_world_anchor_1",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.put_null
					l_definition.set_world_anchor_1(0,0)
					assert("set_world_anchor_1 valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_world_anchor_2_wrong
			-- Wrong test for set_local_anchor_2
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_world_anchor_2",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.put_null
					l_definition.set_world_anchor_2(0,0)
					assert("set_world_anchor_2 valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_length_wrong
			-- Wrong test for length
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.length",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
			l_result:REAL_32
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.put_null
					l_result := l_definition.length
					assert("length valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_length_wrong
			-- Wrong test for set_length
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_length",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.put_null
					l_definition.set_length(10)
					assert("set_length valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_frequency_wrong
			-- Wrong test for frequency
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.frequency",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
			l_result:REAL_32
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.put_null
					l_result := l_definition.frequency
					assert("frequency valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_frequency_wrong_1
			-- Wrong test for set_frequency
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_frequency",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.put_null
					l_definition.set_frequency(10)
					assert("set_frequency valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_frequency_wrong_2
			-- Wrong test for set_frequency
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_frequency",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.set_frequency(-0.01)
					assert("set_frequency valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_damping_ratio_wrong
			-- Wrong test for damping_ratio
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.damping_ratio",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
			l_result:REAL_32
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.put_null
					l_result := l_definition.damping_ratio
					assert("damping_ratio valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_damping_ratio_wrong_1
			-- Wrong test for set_damping_ratio
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_damping_ratio",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.put_null
					l_definition.set_damping_ratio(10)
					assert("set_damping_ratio valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_damping_ratio_wrong_2
			-- Wrong test for set_damping_ratio
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_damping_ratio",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.set_damping_ratio(-0.01)
					assert("set_damping_ratio valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_must_collide_connected_wrong
			-- Wrong test for must_collide_connected
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.must_collide_connected",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
			l_result:BOOLEAN
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.put_null
					l_result := l_definition.must_collide_connected
					assert("must_collide_connected valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_must_collide_connected_wrong
			-- Wrong test for set_must_collide_connected
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_must_collide_connected",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.put_null
					l_definition.set_must_collide_connected(True)
					assert("set_must_collide_connected valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_enable_collide_connected_wrong
			-- Wrong test for enable_collide_connected
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.enable_collide_connected",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.put_null
					l_definition.enable_collide_connected
					assert("enable_collide_connected valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_disable_collide_connected_wrong
			-- Wrong test for disable_collide_connected
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.disable_collide_connected",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.put_null
					l_definition.disable_collide_connected
					assert("disable_collide_connected valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_body_1_wrong
			-- Wrong test for body_1
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_body_1",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
		do
			if l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.put_null
					world.create_body (create {B2D_BODY_DEFINITION})
					if attached world.last_body as la_body then
						l_definition.set_body_1 (la_body)
						assert("set_body_1 valid", False)
					else
						assert ("Cannot create body", False)
					end
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_body_2_wrong
			-- Wrong test for body_2
		note
			testing:
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.set_body_2",
				"covers/{B2D_JOINT_DISTANCE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_DISTANCE_DEFINITION
			l_retry:BOOLEAN
		do
			if l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 10, 5, -10, -5)
					l_definition.put_null
					world.create_body (create {B2D_BODY_DEFINITION})
					if attached world.last_body as la_body then
						l_definition.set_body_2 (la_body)
						assert("set_body_1 valid", False)
					else
						assert ("Cannot create body", False)
					end
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

feature -- Access

	body_1, body_2:detachable B2D_BODY
			-- {B2D_BODY} used in `Current'

	world:B2D_WORLD
			-- The {B2D_WORLD} used in `Current'

end


