note
	description: "Tests for {B2D_JOINT_MOUSE}"
	author: "Louis Marchand"
	date: "Wed, 29 Jul 2020 20:20:07 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_JOINT_MOUSE_TESTS

inherit
	EQA_TEST_SET
		redefine
			on_prepare
		end
	B2D_ANY
		undefine
			default_create
		end

feature {NONE} -- Events

	on_prepare
			-- <Precursor>
		local
			l_definition:B2D_JOINT_MOUSE_DEFINITION
		do
			create world.make (0, -10)
			world.create_body (create {B2D_BODY_DEFINITION}.make_dynamic)
			check attached world.last_body as la_body_1 then
				body_1 := la_body_1
				la_body_1.create_fixture_from_shape (create {B2D_SHAPE_POLYGON}.make_box (5, 5), 5)
				world.create_body (create {B2D_BODY_DEFINITION}.make_dynamic)
				check attached world.last_body as la_body_2 then
					body_2 := la_body_2
					la_body_2.create_fixture_from_shape (create {B2D_SHAPE_POLYGON}.make_box (5, 5), 5)
					create l_definition.make (la_body_1, la_body_2)
					world.create_joint (l_definition)
					check attached {B2D_JOINT_MOUSE} world.last_joint as la_joint then
						joint := la_joint
					end
				end
			end
		end

feature -- Normal test routines

	test_anchor_normal
			-- Test routine for `anchor_*'
		note
			testing:
					"covers/{B2D_JOINT_MOUSE}.anchor_1",
					"covers/{B2D_JOINT_MOUSE}.anchor_2"
		do
			assert ("anchor_1 X", joint.anchor_1.x ~ 0)
			assert ("anchor_1 Y", joint.anchor_1.y ~ 0)
			assert ("anchor_2 X", joint.anchor_2.x ~ 0)
			assert ("anchor_2 Y", joint.anchor_2.y ~ 0)
		end

	test_is_active_normal
			-- Test routine for `is_active'
		note
			testing:
					"covers/{B2D_JOINT_MOUSE}.is_active",
					"covers/{B2D_BODY}.activate",
					"covers/{B2D_BODY}.deactivate"
		do
			assert ("Default is_active valid", joint.is_active)
			body_1.deactivate
			assert ("Body 1 Desactivate is_active valid", not joint.is_active)
			body_1.activate
			assert ("Default is_active valid", joint.is_active)
			body_2.deactivate
			assert ("Body 1 Desactivate is_active valid", not joint.is_active)
			body_1.deactivate
			body_2.deactivate
			assert ("Body 1 Desactivate is_active valid", not joint.is_active)
		end

	test_is_collide_connected_normal
			-- Test routine for `is_collide_connected'
		note
			testing:
					"covers/{B2D_JOINT_MOUSE}.is_collide_connected"
		do
			assert ("is_collide_connected valid", not joint.is_collide_connected)
		end

	test_body_normal
			-- Test routine for `body_*'
		note
			testing:
					"covers/{B2D_JOINT_MOUSE}.body_1",
					"covers/{B2D_JOINT_MOUSE}.body_2"
		do
			assert ("body_1 valid", joint.body_1 ~ body_1)
			assert ("body_2 valid", joint.body_2 ~ body_2)
		end

	test_target_normal
			-- Test routine for `target'
		note
			testing:
					"covers/{B2D_JOINT_MOUSE}.target",
					"covers/{B2D_JOINT_MOUSE}.set_target",
					"covers/{B2D_JOINT_MOUSE}.set_target_with_vector"
		local
			l_vector:B2D_VECTOR_2D
		do
			assert ("Default target X", joint.target.x ~ 0)
			assert ("Default target Y", joint.target.y ~ 0)
			joint.set_target (10, 10)
			assert ("set_target valid: X", joint.target.x ~ 10)
			assert ("set_target valid Y", joint.target.y ~ 10)
			create l_vector.make_with_coordinates (5, 5)
			joint.set_target_with_vector (l_vector)
			assert ("set_target_with_vector valid", joint.target ~ l_vector)
		end

	test_maximum_force_normal
			-- Test routine for `maximum_force'
		note
			testing:
					"covers/{B2D_JOINT_MOUSE}.maximum_force",
					"covers/{B2D_JOINT_MOUSE}.set_maximum_force"
		do
			assert ("Default maximum_force", joint.maximum_force ~ 0)
			joint.set_maximum_force (10)
			assert ("set_maximum_force valid", joint.maximum_force ~ 10)
			joint.set_maximum_force_with_weight_multiplier (joint.body_1, 2)
			assert ("set_maximum_force_with_weight_multiplier valid", joint.maximum_force ~ 10000)
		end

	test_frequency_normal
			-- Test routine for `frequency'
		note
			testing:
					"covers/{B2D_JOINT_MOUSE}.frequency",
					"covers/{B2D_JOINT_MOUSE}.set_frequency"
		do
			assert ("Default frequency", joint.frequency ~ 5)
			joint.set_frequency (10)
			assert ("set_frequency valid", joint.frequency ~ 10)
		end

	test_damping_ratio_normal
			-- Test routine for `damping_ratio'
		note
			testing:
					"covers/{B2D_JOINT_MOUSE}.damping_ratio",
					"covers/{B2D_JOINT_MOUSE}.set_damping_ratio"
		do
			assert ("Default frequency", joint.damping_ratio ~ 0.7)
			joint.set_damping_ratio (10)
			assert ("set_frequency valid", joint.damping_ratio ~ 10)
		end

	test_reaction_force_normal
			-- Test routine for `reaction_force'
		note
			testing:
					"covers/{B2D_JOINT_MOUSE}.reaction_force",
					"covers/{B2D_WORLD}.step",
					"covers/{B2D_BODY}.position"
		do
			joint.set_maximum_force_with_weight_multiplier (joint.body_2, 1000)
			joint.set_target (100, 100)
			world.step
			assert ("step valid X", joint.body_2.position.x > 0)
			assert ("step valid Y", joint.body_2.position.y > 0)
			assert ("reaction_force valid X", joint.reaction_force (60).x > 0)
			assert ("reaction_force valid Y", joint.reaction_force (60).y > 0)
		end

	test_reaction_torque_normal
			-- Test routine for `reaction_torque'
		note
			testing:
					"covers/{B2D_JOINT_MOUSE}.reaction_torque"
		do
			joint.set_maximum_force_with_weight_multiplier (joint.body_2, 1000)
			joint.set_target (100, 100)
			world.step
			assert ("reaction_torque valid", joint.reaction_torque (60) ~ 0)
		end

	test_shift_origin_normal
			-- Test routine for `shift_origin'
		note
			testing:
					"covers/{B2D_JOINT_MOUSE}.shift_origin"
		do
			joint.set_target (100, 120)
			joint.shift_origin (10, 5)
			assert ("shift_origin valid X", joint.target.x ~ 90)
			assert ("shift_origin valid Y", joint.target.y ~ 115)
			assert ("shift_origin anchor_1 X", joint.anchor_1.x ~ 90)
			assert ("shift_origin anchor_1 Y", joint.anchor_1.y ~ 115)
		end

	test_shift_origin_with_vector_normal
			-- Test routine for `shift_origin_with_vector'
		note
			testing:
					"covers/{B2D_JOINT_MOUSE}.shift_origin_with_vector"
		local
			l_vector:B2D_VECTOR_2D
		do
			joint.set_target (100, 120)
			create l_vector.make_with_coordinates (10, 5)
			joint.shift_origin_with_vector (l_vector)
			assert ("shift_origin_with_vector valid X", joint.target.x ~ 90)
			assert ("shift_origin_with_vector valid Y", joint.target.y ~ 115)
		end

feature -- Limit test routines

	test_maximum_force_limit
			-- Limit test routine for `maximum_force'
		note
			testing:
					"covers/{B2D_JOINT_MOUSE}.maximum_force",
					"covers/{B2D_JOINT_MOUSE}.set_maximum_force"
		do
			joint.set_maximum_force (0)
			assert ("set_maximum_force valid", joint.maximum_force ~ 0)
		end

	test_frequency_limit
			-- Limit test routine for `frequency'
		note
			testing:
					"covers/{B2D_JOINT_MOUSE}.frequency",
					"covers/{B2D_JOINT_MOUSE}.set_frequency"
		do
			joint.set_frequency (0)
			assert ("set_frequency valid", joint.frequency ~ 0)
		end

	test_damping_ratio_limit
			-- Limit test routine for `damping_ratio'
		note
			testing:
					"covers/{B2D_JOINT_MOUSE}.damping_ratio",
					"covers/{B2D_JOINT_MOUSE}.set_damping_ratio"
		do
			joint.set_damping_ratio (0)
			assert ("set_frequency valid", joint.damping_ratio ~ 0)
		end

	test_shift_origin_limit
			-- Limit test routine for `shift_origin'
		note
			testing:
					"covers/{B2D_JOINT_MOUSE}.shift_origin"
		do
			joint.set_target (100, 120)
			joint.shift_origin (0, 0)
			assert ("shift_origin valid X", joint.target.x ~ 100)
			assert ("shift_origin valid Y", joint.target.y ~ 120)
		end

	test_shift_origin_with_vector_limit
			-- Limit test routine for `shift_origin_with_vector'
		note
			testing:
					"covers/{B2D_JOINT_MOUSE}.shift_origin_with_vector"
		local
			l_vector:B2D_VECTOR_2D
		do
			joint.set_target (100, 120)
			create l_vector.make_with_coordinates (0, 0)
			joint.shift_origin_with_vector (l_vector)
			assert ("shift_origin_with_vector valid X", joint.target.x ~ 100)
			assert ("shift_origin_with_vector valid Y", joint.target.y ~ 120)
		end

	test_reaction_force_limit
			-- Limit test routine for `reaction_force'
		note
			testing:
					"covers/{B2D_JOINT_MOUSE}.reaction_force",
					"covers/{B2D_WORLD}.step"
		do
			assert ("reaction_force default valid X", joint.reaction_force (0).x ~ 0)
			assert ("reaction_force default valid Y", joint.reaction_force (0).y ~ 0)
			world.step
			assert ("reaction_force valid X", joint.reaction_force (0).x ~ 0)
			assert ("reaction_force valid Y", joint.reaction_force (0).y ~ 0)
		end

	test_reaction_torque_limit
			-- Limit test routine for `reaction_torque'
		note
			testing:
					"covers/{B2D_JOINT_MOUSE}.reaction_torque",
					"covers/{B2D_WORLD}.step"
		do
			assert ("reaction_torque valid", joint.reaction_torque (0) ~ 0)
			world.step
			assert ("reaction_torque valid", joint.reaction_torque (0) ~ 0)
		end

feature -- Wrong test routines

	test_anchor_1_wrong
			-- Test for {B2D_JOINT_MOUSE}.anchor_1 when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_MOUSE}.anchor_1"
		local
			l_retry:BOOLEAN
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				joint.put_null
				l_result := joint.anchor_1
				assert ("joint.anchor_1 on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_anchor_2_wrong
			-- Test for {B2D_JOINT_MOUSE}.anchor_2 when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_MOUSE}.anchor_2"
		local
			l_retry:BOOLEAN
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				joint.put_null
				l_result := joint.anchor_2
				assert ("joint.anchor_2 on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_is_collide_connected_wrong
			-- Test for {B2D_JOINT_MOUSE}.is_collide_connected when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_MOUSE}.is_collide_connected"
		local
			l_retry:BOOLEAN
			l_result:BOOLEAN
		do
			if not l_retry then
				joint.put_null
				l_result := joint.is_collide_connected
				assert ("joint.is_collide_connected on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_target_wrong
			-- Wrong test for `target'
		note
			testing:
				"covers/{B2D_JOINT_MOUSE}.target"
		local
			l_retry:BOOLEAN
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				joint.put_null
				l_result := joint.target
				assert("target valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_target_wrong
			-- Wrong test for `set_target'
		note
			testing:
				"covers/{B2D_JOINT_MOUSE}.set_target"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				joint.put_null
				joint.set_target(10, 10)
				assert("set_target valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_target_with_vector_wrong_1
			-- Wrong test 1 for `set_target_with_vector'
		note
			testing:
				"covers/{B2D_JOINT_MOUSE}.set_target_with_vector"
		local
			l_retry:BOOLEAN
			l_vector:B2D_VECTOR_2D
		do
			if not l_retry then
				joint.put_null
				create l_vector.make_with_coordinates (10, 10)
				joint.set_target_with_vector(l_vector)
				assert("set_target_with_vector valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_target_with_vector_wrong_2
			-- Wrong test 2 for `set_target_with_vector'
		note
			testing:
				"covers/{B2D_JOINT_MOUSE}.set_target_with_vector"
		local
			l_retry:BOOLEAN
			l_vector:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_vector.make_with_coordinates (10, 10)
				l_vector.put_null
				joint.set_target_with_vector(l_vector)
				assert("set_target_with_vector valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_maximum_force_wrong
			-- Wrong test for `maximum_force'
		note
			testing:
				"covers/{B2D_JOINT_MOUSE}.maximum_force"
		local
			l_retry:BOOLEAN
			l_result:REAL_32
		do
			if not l_retry then
				joint.put_null
				l_result := joint.maximum_force
				assert("maximum_force valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_maximum_force_wrong_1
			-- Wrong test 1 for `set_maximum_force'
		note
			testing:
				"covers/{B2D_JOINT_MOUSE}.set_maximum_force"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				joint.put_null
				joint.set_maximum_force(10)
				assert("set_maximum_force valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_maximum_force_wrong_2
			-- Wrong test 2 for `set_maximum_force'
		note
			testing:
				"covers/{B2D_JOINT_MOUSE}.set_maximum_force"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				joint.set_maximum_force(-1)
				assert("set_maximum_force valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_maximum_force_with_weight_multiplier_wrong_1
			-- Wrong test 1 for `set_maximum_force_with_weight_multiplier'
		note
			testing:
				"covers/{B2D_JOINT_MOUSE}.set_maximum_force_with_weight_multiplier"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				joint.put_null
				joint.set_maximum_force_with_weight_multiplier (joint.body_1, 10)
				assert("set_maximum_force_with_weight_multiplier valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_maximum_force_with_weight_multiplier_wrong_2
			-- Wrong test 2 for `set_maximum_force_with_weight_multiplier'
		note
			testing:
				"covers/{B2D_JOINT_MOUSE}.set_maximum_force_with_weight_multiplier"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				joint.body_1.put_null
				joint.set_maximum_force_with_weight_multiplier (joint.body_1, 10)
				assert("set_maximum_force_with_weight_multiplier valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_maximum_force_with_weight_multiplier_wrong_3
			-- Wrong test 3 for `set_maximum_force_with_weight_multiplier'
		note
			testing:
				"covers/{B2D_JOINT_MOUSE}.set_maximum_force_with_weight_multiplier"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				joint.set_maximum_force_with_weight_multiplier (joint.body_1, -1)
				assert("set_maximum_force_with_weight_multiplier valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_frequency_wrong
			-- Wrong test for `frequency'
		note
			testing:
				"covers/{B2D_JOINT_MOUSE}.frequency"
		local
			l_retry:BOOLEAN
			l_result:REAL_32
		do
			if not l_retry then
				joint.put_null
				l_result := joint.frequency
				assert("frequency valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_frequency_wrong_1
			-- Wrong test 1 for `set_frequency'
		note
			testing:
				"covers/{B2D_JOINT_MOUSE}.set_frequency"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				joint.put_null
				joint.set_frequency(10)
				assert("set_frequency valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_frequency_wrong_2
			-- Wrong test 2 for `set_frequency'
		note
			testing:
				"covers/{B2D_JOINT_MOUSE}.set_frequency"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				joint.set_frequency(-1)
				assert("set_frequency valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_damping_ratio_wrong
			-- Wrong test for `damping_ratio'
		note
			testing:
				"covers/{B2D_JOINT_MOUSE}.damping_ratio"
		local
			l_retry:BOOLEAN
			l_result:REAL_32
		do
			if not l_retry then
				joint.put_null
				l_result := joint.damping_ratio
				assert("damping_ratio valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_damping_ratio_wrong_1
			-- Wrong test 1 for `set_damping_ratio'
		note
			testing:
				"covers/{B2D_JOINT_MOUSE}.set_damping_ratio"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				joint.put_null
				joint.set_damping_ratio(10)
				assert("set_damping_ratio valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_damping_ratio_wrong_2
			-- Wrong test 2 for `set_damping_ratio'
		note
			testing:
				"covers/{B2D_JOINT_MOUSE}.set_damping_ratio"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				joint.set_damping_ratio(-2)
				assert("set_damping_ratio valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_reaction_force_wrong
			-- Wrong test for `reaction_force'
		note
			testing:
				"covers/{B2D_JOINT_MOUSE}.reaction_force"
		local
			l_retry:BOOLEAN
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				joint.put_null
				l_result := joint.reaction_force (60)
				assert("reaction_force valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_reaction_torque_wrong
			-- Wrong test for `reaction_torque'
		note
			testing:
				"covers/{B2D_JOINT_MOUSE}.reaction_torque"
		local
			l_retry:BOOLEAN
			l_result:REAL_32
		do
			if not l_retry then
				joint.put_null
				l_result := joint.reaction_torque (60)
				assert("reaction_torque valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_shift_origin_wrong
			-- Wrong test for `shift_origin'
		note
			testing:
				"covers/{B2D_JOINT_MOUSE}.shift_origin"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				joint.put_null
				joint.shift_origin (10, 10)
				assert("shift_origin valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_shift_origin_with_vector_wrong_1
			-- Wrong test 1 for `shift_origin_with_vector'
		note
			testing:
				"covers/{B2D_JOINT_MOUSE}.shift_origin_with_vector"
		local
			l_retry:BOOLEAN
			l_vector:B2D_VECTOR_2D
		do
			if not l_retry then
				joint.put_null
				create l_vector.make_with_coordinates (10, 5)
				joint.shift_origin_with_vector (l_vector)
				assert("shift_origin_with_vector valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_shift_origin_with_vector_wrong_2
			-- Wrong test 2 for `shift_origin_with_vector'
		note
			testing:
				"covers/{B2D_JOINT_MOUSE}.shift_origin_with_vector"
		local
			l_retry:BOOLEAN
			l_vector:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_vector.make_with_coordinates (10, 5)
				l_vector.put_null
				joint.shift_origin_with_vector (l_vector)
				assert("shift_origin_with_vector valid.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end




feature {NONE} -- Implementation

	world:B2D_WORLD
			-- The {B2D_WORLD} containing `joint'

	body_1, body_2:B2D_BODY
			-- The {B2D_BODY} used as anchor of `joint'

	joint:B2D_JOINT_MOUSE
			-- The {B2D_JOINT} tested in `Current'


end


