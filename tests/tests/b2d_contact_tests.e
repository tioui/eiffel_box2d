note
	description: "Tests for {B2D_CONTACT}"
	author: "Louis Marchand"
	date: "Thu, 02 Jul 2020 16:19:12 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_CONTACT_TESTS

inherit
	EQA_TEST_SET
		redefine
			on_prepare
		end
	B2D_COMPARE_REAL
		undefine
			default_create
		end
	B2D_ANY
		undefine
			default_create
		end

feature {NONE} -- Events

	on_prepare
			-- <Precursor>
		local
			l_edge:B2D_SHAPE_EDGE
			l_circle:B2D_SHAPE_CIRCLE
			l_body_definition:B2D_BODY_DEFINITION
		do
			create l_edge.make (-10, 1, 10, 1)
			create l_circle.make_with_coordinates (3, 11, 9.99999)
			create world.make (0, -10)
			create l_body_definition.make_static
			world.create_body (l_body_definition)
			body_1 := world.bodies.at (1)
			body_1.create_fixture_from_shape (l_edge, 0)
			create l_body_definition.make_dynamic
			world.create_body (l_body_definition)
			body_2 := world.bodies.at (2)
			body_2.create_fixture_from_shape (l_circle, 10)
			world.step
			assert("Contacts count valid", world.contacts.count ~ 1)
			contact := world.contacts.at (1)
		end

feature -- Test routines

	test_manifold_normal
			-- Normal test for `manifold'
		note
			testing:  "covers/{B2D_CONTACT}.manifold"
		local
			l_manifold:B2D_MANIFOLD
		do
			l_manifold := contact.manifold
			assert("Manifold Valid: point_count", l_manifold.point_count ~ 1)
			assert("Manifold Valid: local_point.x", l_manifold.local_point.x ~ -10)
			assert("Manifold Valid: local_point.y", l_manifold.local_point.y ~ 1)
		end

	test_world_manifold_normal
			-- Normal test for `world_manifold'
		note
			testing:  "covers/{B2D_CONTACT}.world_manifold"
		local
			l_world_manifold:B2D_WORLD_MANIFOLD
		do
			l_world_manifold := contact.world_manifold
			assert("world_manifold Valid: point_count", l_world_manifold.point_count ~ 1)
			assert("world_manifold Valid: local_point.x", l_world_manifold.points.at (1).x ~ 3)
			assert("world_manifold Valid: local_point.y", compare_real_32 (l_world_manifold.points.at (1).y, 1.0055))
		end

	test_is_touching_normal
			-- Normal test for `is_touching'
		note
			testing:  "covers/{B2D_CONTACT}.is_touching"
		do
			assert("is_touching valid.", world.contacts.at (1).is_touching)
		end

	test_is_enabled_normal
			-- Normal test for `is_enabled'
		note
			testing:  	"covers/{B2D_CONTACT}.is_enabled",
						"covers/{B2D_CONTACT}.set_is_enabled",
						"covers/{B2D_CONTACT}.enable",
						"covers/{B2D_CONTACT}.disable"
		do
			assert("is_enabled=True by default.", contact.is_enabled)
			contact.set_is_enabled (False)
			assert("set_is_enabled(False) Valid.", not contact.is_enabled)
			contact.enable
			assert("enable Valid.", contact.is_enabled)
			contact.disable
			assert("disable Valid.", not contact.is_enabled)
			contact.set_is_enabled (True)
			assert("set_is_enabled(True) Valid.", contact.is_enabled)
		end

	test_fixture_1_normal
			-- Normal test for `fixture_1'
		note
			testing:  "covers/{B2D_CONTACT}.fixture_1"
		do
			assert("fixture_1 valid.", contact.fixture_1.body ~ body_1)
		end

	test_fixture_2_normal
			-- Normal test for `fixture_2'
		note
			testing:  "covers/{B2D_CONTACT}.fixture_2"
		do
			assert("fixture_2 valid.", contact.fixture_2.body ~ body_2)
		end

	test_fixture_1_child_index_normal
			-- Normal test for `fixture_1_child_index'
		note
			testing:  "covers/{B2D_CONTACT}.fixture_1_child_index"
		do
			assert("fixture_1_child_index valid.", contact.fixture_1_child_index ~ 1)
		end

	test_fixture_2_child_index_normal
			-- Normal test for `fixture_2_child_index'
		note
			testing:  "covers/{B2D_CONTACT}.fixture_2_child_index"
		do
			assert("fixture_2_child_index valid.", contact.fixture_2_child_index ~ 1)
		end

	test_friction_normal
			-- Normal test for `friction'
		note
			testing:  	"covers/{B2D_CONTACT}.friction",
						"covers/{B2D_CONTACT}.set_friction",
						"covers/{B2D_CONTACT}.reset_friction"
		do
			assert("friction valid.", contact.friction ~ 0.2)
			contact.set_friction (0.5)
			assert("set_friction valid.", contact.friction ~ 0.5)
			contact.reset_friction
			assert("reset_friction valid.", contact.friction ~ 0.2)
		end

	test_restitution_normal
			-- Normal test for `restitution'
		note
			testing:  	"covers/{B2D_CONTACT}.restitution",
						"covers/{B2D_CONTACT}.set_restitution",
						"covers/{B2D_CONTACT}.reset_restitution"
		do
			assert("restitution valid.", contact.restitution ~ 0)
			contact.set_restitution (0.5)
			assert("set_restitution valid.", contact.restitution ~ 0.5)
			contact.reset_restitution
			assert("reset_restitution valid.", contact.restitution ~ 0)
		end

	test_tangent_speed_normal
			-- Normal test for `tangent_speed'
		note
			testing:  	"covers/{B2D_CONTACT}.tangent_speed",
						"covers/{B2D_CONTACT}.set_tangent_speed"
		do
			assert("tangent_speed valid.", contact.tangent_speed ~ 0)
			contact.set_tangent_speed (0.5)
			assert("set_tangent_speed valid.", contact.tangent_speed ~ 0.5)
		end

	test_manifold_wrong
			-- Wrong test for `manifold'
		note
			testing:  	"covers/{B2D_CONTACT}.manifold"
		local
			l_retry:BOOLEAN
			l_result:B2D_MANIFOLD
		do
			if not l_retry then
				contact.put_null
				l_result := contact.manifold
				assert("manifold valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_world_manifold_wrong
			-- Wrong test for `world_manifold'
		note
			testing:  	"covers/{B2D_CONTACT}.world_manifold"
		local
			l_retry:BOOLEAN
			l_result:B2D_WORLD_MANIFOLD
		do
			if not l_retry then
				contact.put_null
				l_result := contact.world_manifold
				assert("world_manifold valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_is_touching_wrong
			-- Wrong test for `is_touching'
		note
			testing:  	"covers/{B2D_CONTACT}.is_touching"
		local
			l_retry:BOOLEAN
			l_result:BOOLEAN
		do
			if not l_retry then
				contact.put_null
				l_result := contact.is_touching
				assert("is_touching valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_is_enabled_wrong
			-- Wrong test for `is_enabled'
		note
			testing:  	"covers/{B2D_CONTACT}.is_enabled"
		local
			l_retry:BOOLEAN
			l_result:BOOLEAN
		do
			if not l_retry then
				contact.put_null
				l_result := contact.is_enabled
				assert("is_enabled valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_is_enabled_wrong
			-- Wrong test for `set_is_enabled'
		note
			testing:  	"covers/{B2D_CONTACT}.set_is_enabled"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				contact.put_null
				contact.set_is_enabled(True)
				assert("set_is_enabled valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_enable_wrong
			-- Wrong test for `enable'
		note
			testing:  	"covers/{B2D_CONTACT}.enable"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				contact.put_null
				contact.enable
				assert("enable valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_disable_wrong
			-- Wrong test for `disable'
		note
			testing:  	"covers/{B2D_CONTACT}.disable"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				contact.put_null
				contact.disable
				assert("disable valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_fixture_1_child_index_wrong
			-- Wrong test for `fixture_1_child_index'
		note
			testing:  	"covers/{B2D_CONTACT}.fixture_1_child_index"
		local
			l_retry:BOOLEAN
			l_result:INTEGER
		do
			if not l_retry then
				contact.put_null
				l_result := contact.fixture_1_child_index
				assert("fixture_1_child_index valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_fixture_2_child_index_wrong
			-- Wrong test for `fixture_2_child_index'
		note
			testing:  	"covers/{B2D_CONTACT}.fixture_2_child_index"
		local
			l_retry:BOOLEAN
			l_result:INTEGER
		do
			if not l_retry then
				contact.put_null
				l_result := contact.fixture_2_child_index
				assert("fixture_2_child_index valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_friction_wrong
			-- Wrong test for `friction'
		note
			testing:  	"covers/{B2D_CONTACT}.friction"
		local
			l_retry:BOOLEAN
			l_result:REAL_32
		do
			if not l_retry then
				contact.put_null
				l_result := contact.friction
				assert("friction valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_friction_wrong
			-- Wrong test for `set_friction'
		note
			testing:  	"covers/{B2D_CONTACT}.set_friction"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				contact.put_null
				contact.set_friction(0)
				assert("set_friction valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_reset_friction_wrong
			-- Wrong test for `reset_friction'
		note
			testing:  	"covers/{B2D_CONTACT}.reset_friction"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				contact.put_null
				contact.reset_friction
				assert("reset_friction valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_restitution_wrong
			-- Wrong test for `restitution'
		note
			testing:  	"covers/{B2D_CONTACT}.restitution"
		local
			l_retry:BOOLEAN
			l_result:REAL_32
		do
			if not l_retry then
				contact.put_null
				l_result := contact.restitution
				assert("restitution valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_restitution_wrong
			-- Wrong test for `set_restitution'
		note
			testing:  	"covers/{B2D_CONTACT}.set_restitution"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				contact.put_null
				contact.set_restitution(0)
				assert("set_restitution valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_reset_restitution_wrong
			-- Wrong test for `reset_restitution'
		note
			testing:  	"covers/{B2D_CONTACT}.reset_restitution"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				contact.put_null
				contact.reset_restitution
				assert("reset_restitution valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_tangent_speed_wrong
			-- Wrong test for `tangent_speed'
		note
			testing:  	"covers/{B2D_CONTACT}.tangent_speed"
		local
			l_retry:BOOLEAN
			l_result:REAL_32
		do
			if not l_retry then
				contact.put_null
				l_result := contact.tangent_speed
				assert("tangent_speed valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_tangent_speed_wrong
			-- Wrong test for `set_tangent_speed'
		note
			testing:  	"covers/{B2D_CONTACT}.set_tangent_speed"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				contact.put_null
				contact.set_tangent_speed(0)
				assert("set_tangent_speed valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_world_manifold_normal_wrong
			-- Wrong test for {B2D_WORLD_MANIFOLD}.`normal'
		note
			testing:  	"covers/{B2D_WORLD_MANIFOLD}.normal"
		local
			l_retry:BOOLEAN
			l_world_manifold:B2D_WORLD_MANIFOLD
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				l_world_manifold := contact.world_manifold
				l_world_manifold.put_null
				l_result := l_world_manifold.normal
				assert("{B2D_WORLD_MANIFOLD}.normal valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_world_manifold_points_wrong
			-- Wrong test for {B2D_WORLD_MANIFOLD}.`points'
		note
			testing:  	"covers/{B2D_WORLD_MANIFOLD}.points"
		local
			l_retry:BOOLEAN
			l_world_manifold:B2D_WORLD_MANIFOLD
			l_result:LIST[B2D_VECTOR_2D]
		do
			if not l_retry then
				l_world_manifold := contact.world_manifold
				l_world_manifold.put_null
				l_result := l_world_manifold.points
				assert("{B2D_WORLD_MANIFOLD}.points valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_world_manifold_separations_wrong
			-- Wrong test for {B2D_WORLD_MANIFOLD}.`separations'
		note
			testing:  	"covers/{B2D_WORLD_MANIFOLD}.separations"
		local
			l_retry:BOOLEAN
			l_world_manifold:B2D_WORLD_MANIFOLD
			l_result:LIST[REAL_32]
		do
			if not l_retry then
				l_world_manifold := contact.world_manifold
				l_world_manifold.put_null
				l_result := l_world_manifold.separations
				assert("{B2D_WORLD_MANIFOLD}.separations valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

feature {NONE} -- Implementation

	world:B2D_WORLD
			-- The {B2D_WORLD} used in `Current'

	contact:B2D_CONTACT
			-- The contact that is being tested in `Current'

	body_1:B2D_BODY
			-- The first {B2D_BODY} of the contact

	body_2:B2D_BODY
			-- The first {B2D_BODY} of the contact
end


