note
	description: "Tests for {B2D_AXIS_ALIGNED_BOUNDING_BOX}"
	author: "Louis Marchand"
	date: "Fri, 12 Jun 2020 13:21:36 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_AXIS_ALIGNED_BOUNDING_BOX_TESTS

inherit
	EQA_TEST_SET
	B2D_ANY
		undefine
			default_create
		end

feature -- Test routines

	test_make_with_coordinates_normal
			-- Normal test {B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.is_valid", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			create l_aabb.make_with_coordinates (2, 3, 4, 5)
			assert("make_with_coordinates valid: Lower_Bound.X", l_aabb.lower_bound.x ~ 2)
			assert("make_with_coordinates valid: Lower_Bound.Y", l_aabb.lower_bound.y ~ 3)
			assert("make_with_coordinates valid: Upper_Bound.X", l_aabb.upper_bound.x ~ 4)
			assert("make_with_coordinates valid: Upper_Bound.Y", l_aabb.upper_bound.y ~ 5)
		end

	test_make_with_vectors_normal
			-- Normal test {B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_vectors
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.is_valid", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_vectors",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			create l_aabb.make_with_vectors(create {B2D_VECTOR_2D}.make_with_coordinates (2, 3), create {B2D_VECTOR_2D}.make_with_coordinates (4, 5))
			assert("make_with_vectors valid: Lower_Bound.X", l_aabb.lower_bound.x ~ 2)
			assert("make_with_vectors valid: Lower_Bound.Y", l_aabb.lower_bound.y ~ 3)
			assert("make_with_vectors valid: Upper_Bound.X", l_aabb.upper_bound.x ~ 4)
			assert("make_with_vectors valid: Upper_Bound.Y", l_aabb.upper_bound.y ~ 5)
		end

	test_is_valid_normal
			-- Normal test {B2D_AXIS_ALIGNED_BOUNDING_BOX}.is_valid
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.is_valid", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			create l_aabb.make_with_coordinates (2, 3, 3, 4)
			assert ("Is_Valid", l_aabb.is_valid)
			l_aabb.lower_bound.set_coordinates (3, 4)
			l_aabb.upper_bound.set_coordinates (2, 3)
			assert ("Not Is_Valid", not l_aabb.is_valid)
		end

	test_lower_bound_normal
			-- Normal test {B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound
		note
			testing: "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.set_lower_bound",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.set_lower_bound_with_vector",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates"
		local
			l_aabb:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			create l_aabb.make_with_coordinates (0, 0, 0, 0)
			l_aabb.lower_bound.set_coordinates (-4.0664, -4.68944)
			assert ("set_lower_bound valid X", l_aabb.lower_bound.x ~ -4.0664)
			assert ("set_lower_bound valid Y", l_aabb.lower_bound.y ~ -4.68944)
			l_aabb.set_lower_bound (-3.60359, 5.17299)
			assert ("set_lower_bound valid X", l_aabb.lower_bound.x ~ -3.60359)
			assert ("set_lower_bound valid Y", l_aabb.lower_bound.y ~ 5.17299)
			l_aabb.set_lower_bound_with_vector (create {B2D_VECTOR_2D}.make_with_coordinates (-9.07425, -2.12063))
			assert ("set_lower_bound_with_vector valid X", l_aabb.lower_bound.x ~ -9.07425)
			assert ("set_lower_bound_with_vector valid Y", l_aabb.lower_bound.y ~ -2.12063)
		end

	test_upper_bound_normal
			-- Normal test {B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound
		note
			testing: "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.set_upper_bound",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.set_upper_bound_with_vector",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates"
		local
			l_aabb:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			create l_aabb.make_with_coordinates (0, 0, 0, 0)
			l_aabb.upper_bound.set_coordinates (-4.0664, -4.68944)
			assert ("set_upper_bound valid X", l_aabb.upper_bound.x ~ -4.0664)
			assert ("set_upper_bound valid Y", l_aabb.upper_bound.y ~ -4.68944)
			l_aabb.set_upper_bound (-3.60359, 5.17299)
			assert ("set_upper_bound valid X", l_aabb.upper_bound.x ~ -3.60359)
			assert ("set_upper_bound valid Y", l_aabb.upper_bound.y ~ 5.17299)
			l_aabb.set_upper_bound_with_vector (create {B2D_VECTOR_2D}.make_with_coordinates (-9.07425, -2.12063))
			assert ("set_upper_bound_with_vector valid X", l_aabb.upper_bound.x ~ -9.07425)
			assert ("set_upper_bound_with_vector valid Y", l_aabb.upper_bound.y ~ -2.12063)
		end

	test_is_valid_limit
			-- Limit test {B2D_AXIS_ALIGNED_BOUNDING_BOX}.is_valid
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.is_valid", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			create l_aabb.make_with_coordinates (1, 1, 1, 1)
			l_aabb.lower_bound.set_coordinates (1, 1)
			l_aabb.upper_bound.set_coordinates (1, 1)
			assert ("Not Is_Valid", l_aabb.is_valid)
		end

	test_combine_into_current_normal_1
			-- Normal test 1 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine_into_current
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine_into_current", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb1, l_aabb2:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			create l_aabb1.make_with_coordinates (1, 1, 3, 3)
			create l_aabb2.make_with_coordinates (1, 3, 1, 6)
			l_aabb1.combine_into_current (l_aabb2)
			assert ("combine_into_current Valid AABB1 Lower X", l_aabb1.lower_bound.x ~ 1)
			assert ("combine_into_current Valid AABB1 Lower Y", l_aabb1.lower_bound.y ~ 1)
			assert ("combine_into_current Valid AABB1 Upper X", l_aabb1.upper_bound.x ~ 3)
			assert ("combine_into_current Valid AABB1 Upper Y", l_aabb1.upper_bound.y ~ 6)
			assert ("combine_into_current Valid AABB2 Lower X", l_aabb2.lower_bound.x ~ 1)
			assert ("combine_into_current Valid AABB2 Lower Y", l_aabb2.lower_bound.y ~ 3)
			assert ("combine_into_current Valid AABB2 Upper X", l_aabb2.upper_bound.x ~ 1)
			assert ("combine_into_current Valid AABB2 Upper Y", l_aabb2.upper_bound.y ~ 6)
		end

	test_combine_into_current_normal_2
			-- Normal test 2 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine_into_current
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine_into_current", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb1, l_aabb2:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			create l_aabb1.make_with_coordinates (1, 1, 3, 3)
			create l_aabb2.make_with_coordinates (5, 5, 9, 9)
			l_aabb1.combine_into_current (l_aabb2)
			assert ("combine_into_current Valid AABB1 Lower X", l_aabb1.lower_bound.x ~ 1)
			assert ("combine_into_current Valid AABB1 Lower Y", l_aabb1.lower_bound.y ~ 1)
			assert ("combine_into_current Valid AABB1 Upper X", l_aabb1.upper_bound.x ~ 9)
			assert ("combine_into_current Valid AABB1 Upper Y", l_aabb1.upper_bound.y ~ 9)
			assert ("combine_into_current Valid AABB2 Lower X", l_aabb2.lower_bound.x ~ 5)
			assert ("combine_into_current Valid AABB2 Lower Y", l_aabb2.lower_bound.y ~ 5)
			assert ("combine_into_current Valid AABB2 Upper X", l_aabb2.upper_bound.x ~ 9)
			assert ("combine_into_current Valid AABB2 Upper Y", l_aabb2.upper_bound.y ~ 9)
		end

	test_combine_into_current_limit
			-- Limit test {B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine_into_current
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine_into_current", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb1, l_aabb2:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			create l_aabb1.make_with_coordinates (1, 1, 3, 3)
			create l_aabb2.make_with_coordinates (5, 5, 9, 9)
			l_aabb1.combine_into_current (l_aabb2)
			assert ("combine_into_current Valid AABB1 Lower X", l_aabb1.lower_bound.x ~ 1)
			assert ("combine_into_current Valid AABB1 Lower Y", l_aabb1.lower_bound.y ~ 1)
			assert ("combine_into_current Valid AABB1 Upper X", l_aabb1.upper_bound.x ~ 9)
			assert ("combine_into_current Valid AABB1 Upper Y", l_aabb1.upper_bound.y ~ 9)
			assert ("combine_into_current Valid AABB2 Lower X", l_aabb2.lower_bound.x ~ 5)
			assert ("Comcombine_into_currentbine_1 Valid AABB2 Lower Y", l_aabb2.lower_bound.y ~ 5)
			assert ("Comcombine_into_currentbine_1 Valid AABB2 Upper X", l_aabb2.upper_bound.x ~ 9)
			assert ("combine_into_current Valid AABB2 Upper Y", l_aabb2.upper_bound.y ~ 9)
		end

	test_combine_into_current_wrong_1
			-- Wrong test 1 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine_into_current
		note
			testing: "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine_into_current", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_retry:BOOLEAN
			l_aabb1, l_aabb2:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			if not l_retry then
				create l_aabb1.make_with_coordinates (3, 3, 1, 1)
				create l_aabb2.make_with_coordinates (5, 5, 9, 9)
				l_aabb1.combine_into_current (l_aabb2)
				assert("combine_into_current Wrong 1 passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_combine_into_current_wrong_2
			-- Wrong test 2 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine_into_current
		note
			testing: "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine_into_current", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_retry:BOOLEAN
			l_aabb1, l_aabb2:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			if not l_retry then
				create l_aabb1.make_with_coordinates (1, 1, 3, 3)
				create l_aabb2.make_with_coordinates (9, 9, 5, 5)
				l_aabb1.combine_into_current (l_aabb2)
				assert("combine_into_current Wrong 2 passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_combine_normal_1
			-- Normal test 1 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb1, l_aabb2, l_aabb3:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			create l_aabb1.make_with_coordinates (1, 3, 1, 6)
			create l_aabb2.make_with_coordinates (3, 1, 6, 1)
			l_aabb3:= l_aabb1.combine (l_aabb2)
			assert ("combine Valid AABB1 Lower X", l_aabb3.lower_bound.x ~ 1)
			assert ("combine Valid AABB1 Lower Y", l_aabb3.lower_bound.y ~ 1)
			assert ("combine Valid AABB1 Upper X", l_aabb3.upper_bound.x ~ 6)
			assert ("combine Valid AABB1 Upper Y", l_aabb3.upper_bound.y ~ 6)
			assert ("combine Valid AABB2 Lower X", l_aabb1.lower_bound.x ~ 1)
			assert ("combine Valid AABB2 Lower Y", l_aabb1.lower_bound.y ~ 3)
			assert ("combine Valid AABB2 Upper X", l_aabb1.upper_bound.x ~ 1)
			assert ("combine Valid AABB2 Upper Y", l_aabb1.upper_bound.y ~ 6)
			assert ("combine Valid AABB3 Lower X", l_aabb2.lower_bound.x ~ 3)
			assert ("combine Valid AABB3 Lower Y", l_aabb2.lower_bound.y ~ 1)
			assert ("combine Valid AABB3 Upper X", l_aabb2.upper_bound.x ~ 6)
			assert ("combine Valid AABB3 Upper Y", l_aabb2.upper_bound.y ~ 1)
		end

	test_combine_normal_2
			-- Normal test 2 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb1, l_aabb2, l_aabb3:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			create l_aabb1.make_with_coordinates (5, 5, 7, 9)
			create l_aabb2.make_with_coordinates (6, 6, 9, 7)
			l_aabb3:= l_aabb1.combine (l_aabb2)
			assert ("combine Valid AABB1 Lower X", l_aabb3.lower_bound.x ~ 5)
			assert ("combine Valid AABB1 Lower Y", l_aabb3.lower_bound.y ~ 5)
			assert ("combine Valid AABB1 Upper X", l_aabb3.upper_bound.x ~ 9)
			assert ("combine Valid AABB1 Upper Y", l_aabb3.upper_bound.y ~ 9)
			assert ("combine Valid AABB2 Lower X", l_aabb1.lower_bound.x ~ 5)
			assert ("combine Valid AABB2 Lower Y", l_aabb1.lower_bound.y ~ 5)
			assert ("combine Valid AABB2 Upper X", l_aabb1.upper_bound.x ~ 7)
			assert ("combine Valid AABB2 Upper Y", l_aabb1.upper_bound.y ~ 9)
			assert ("combine Valid AABB3 Lower X", l_aabb2.lower_bound.x ~ 6)
			assert ("combine Valid AABB3 Lower Y", l_aabb2.lower_bound.y ~ 6)
			assert ("combine Valid AABB3 Upper X", l_aabb2.upper_bound.x ~ 9)
			assert ("combine Valid AABB3 Upper Y", l_aabb2.upper_bound.y ~ 7)
		end

	test_combine_limit
			-- Limit test {B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb1, l_aabb2, l_aabb3:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			create l_aabb1.make_with_coordinates (0, 0, 0, 0)
			create l_aabb2.make_with_coordinates (0, 0, 0, 0)
			l_aabb3:= l_aabb1.combine (l_aabb2)
			assert ("combine Valid AABB1 Lower X", l_aabb3.lower_bound.x ~ 0)
			assert ("combine Valid AABB1 Lower Y", l_aabb3.lower_bound.y ~ 0)
			assert ("combine Valid AABB1 Upper X", l_aabb3.upper_bound.x ~ 0)
			assert ("combine Valid AABB1 Upper Y", l_aabb3.upper_bound.y ~ 0)
			assert ("combine Valid AABB2 Lower X", l_aabb1.lower_bound.x ~ 0)
			assert ("combine Valid AABB2 Lower Y", l_aabb1.lower_bound.y ~ 0)
			assert ("combine Valid AABB2 Upper X", l_aabb1.upper_bound.x ~ 0)
			assert ("combine Valid AABB2 Upper Y", l_aabb1.upper_bound.y ~ 0)
			assert ("combine Valid AABB3 Lower X", l_aabb2.lower_bound.x ~ 0)
			assert ("combine Valid AABB3 Lower Y", l_aabb2.lower_bound.y ~ 0)
			assert ("combine Valid AABB3 Upper X", l_aabb2.upper_bound.x ~ 0)
			assert ("combine Valid AABB3 Upper Y", l_aabb2.upper_bound.y ~ 0)
		end

	test_combine_wrong_1
			-- Wrong test 1 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_retry:BOOLEAN
			l_aabb1, l_aabb2, l_aabb3:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			if not l_retry then
				create l_aabb1.make_with_coordinates (3, 3, 1, 1)
				create l_aabb2.make_with_coordinates (5, 5, 9, 9)
				l_aabb3:= l_aabb1.combine (l_aabb2)
				assert("combine Wrong 1 passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_combine_wrong_2
			-- Wrong test 1 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_retry:BOOLEAN
			l_aabb1, l_aabb2, l_aabb3:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			if not l_retry then
				create l_aabb1.make_with_coordinates (1, 1, 3, 3)
				create l_aabb2.make_with_coordinates (9, 9, 5, 5)
				l_aabb3:= l_aabb1.combine (l_aabb2)
				assert("combine Wrong 1 passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_contains_normal_1
			-- Normal test 1 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.contains
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.contains", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb1, l_aabb2:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			create l_aabb1.make_with_coordinates (1, 1, 4, 4)
			create l_aabb2.make_with_coordinates (2, 2, 3, 3)
			assert ("contains Valid", l_aabb1.contains(l_aabb2))
		end

	test_contains_normal_2
			-- Normal test 2 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.contains
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.contains", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb1, l_aabb2:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			create l_aabb1.make_with_coordinates (1, 1, 4, 4)
			create l_aabb2.make_with_coordinates (5, 5, 6, 6)
			assert ("contains Valid", not l_aabb1.contains(l_aabb2))
		end

	test_contains_normal_3
			-- Normal test 3 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.contains
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.contains", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb1, l_aabb2:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			create l_aabb1.make_with_coordinates (1, 1, 4, 4)
			create l_aabb2.make_with_coordinates (3, 3, 6, 6)
			assert ("contains Valid", not l_aabb1.contains(l_aabb2))
		end

	test_contains_limit
			-- Limit test 3 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.contains
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.contains", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb1, l_aabb2:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			create l_aabb1.make_with_coordinates (1, 1, 4, 4)
			create l_aabb2.make_with_coordinates (1, 1, 4, 4)
			assert ("contains Valid", l_aabb1.contains(l_aabb2))
		end

	test_contains_wrong_1
			-- Wrong test 1 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.contains
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.contains", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_retry, l_result:BOOLEAN
			l_aabb1, l_aabb2:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			if not l_retry then
				create l_aabb1.make_with_coordinates (3, 3, 1, 1)
				create l_aabb2.make_with_coordinates (5, 5, 9, 9)
				l_result:= l_aabb1.contains (l_aabb2)
				assert("contains Wrong 1 passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_contains_wrong_2
			-- Wrong test 1 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.contains
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.contains", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_retry, l_result:BOOLEAN
			l_aabb1, l_aabb2:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			if not l_retry then
				create l_aabb1.make_with_coordinates (1, 1, 3, 3)
				create l_aabb2.make_with_coordinates (9, 9, 5, 5)
				l_result:= l_aabb1.contains (l_aabb2)
				assert("contains Wrong 1 passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_center_normal
			-- Normal test {B2D_AXIS_ALIGNED_BOUNDING_BOX}.center
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.center", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb1:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_center:B2D_VECTOR_2D
		do
			create l_aabb1.make_with_coordinates (1, 2, 4, 5)
			l_center := l_aabb1.center
			assert ("center Valid X", l_center.x ~ 2.5)
			assert ("center Valid Y", l_center.y ~ 3.5)
		end

	test_center_limit
			-- Limit test {B2D_AXIS_ALIGNED_BOUNDING_BOX}.center
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.center", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb1:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_center:B2D_VECTOR_2D
		do
			create l_aabb1.make_with_coordinates (1, 1, 1, 1)
			l_center := l_aabb1.center
			assert ("center Valid X", l_center.x ~ 1)
			assert ("center Valid Y", l_center.y ~ 1)
		end

	test_center_wrong_1
			-- Wrong test 1 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.center
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.center", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_retry:BOOLEAN
			l_aabb1:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_center:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_aabb1.make_with_coordinates (3, 3, 1, 1)
				l_center := l_aabb1.center
				assert("center Wrong 1 passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_extends_normal
			-- Normal test {B2D_AXIS_ALIGNED_BOUNDING_BOX}.extends
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.extends", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb1:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_extends:B2D_VECTOR_2D
		do
			create l_aabb1.make_with_coordinates (1, 2, 4, 9)
			l_extends := l_aabb1.extends
			assert ("extends Valid X", l_extends.x ~ 1.5)
			assert ("extends Valid Y", l_extends.y ~ 3.5)
		end

	test_extends_limit
			-- Limit test {B2D_AXIS_ALIGNED_BOUNDING_BOX}.extends
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.extends", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb1:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_extends:B2D_VECTOR_2D
		do
			create l_aabb1.make_with_coordinates (1, 1, 1, 1)
			l_extends := l_aabb1.extends
			assert ("extends Valid X", l_extends.x ~ 0)
			assert ("ceextendsnter Valid Y", l_extends.y ~ 0)
		end

	test_extends_wrong_1
			-- Wrong test 1 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.extends
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.extends", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_retry:BOOLEAN
			l_aabb1:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_extends:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_aabb1.make_with_coordinates (3, 3, 1, 1)
				l_extends := l_aabb1.extends
				assert("extends Wrong 1 passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_perimeter_normal
			-- Normal test {B2D_AXIS_ALIGNED_BOUNDING_BOX}.perimeter
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.perimeter", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb1:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			create l_aabb1.make_with_coordinates (1, 2, 4, 9)
			assert ("perimeter Valid", l_aabb1.perimeter ~ 20)
		end

	test_perimeter_limit
			-- Limit test {B2D_AXIS_ALIGNED_BOUNDING_BOX}.perimeter
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.perimeter", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb1:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			create l_aabb1.make_with_coordinates (1, 1, 1, 1)
			assert ("perimeter Valid", l_aabb1.perimeter ~ 0)
		end

	test_perimeter_wrong_1
			-- Wrong test 1 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.perimeter
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.perimeter", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_retry:BOOLEAN
			l_aabb1:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_result:REAL_32
		do
			if not l_retry then
				create l_aabb1.make_with_coordinates (3, 3, 1, 1)
				l_result := l_aabb1.perimeter
				assert("extends Wrong 1 passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_is_equal_normal_1
			-- Normal test 1 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.is_equal
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.is_equal", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb1, l_aabb2:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			create l_aabb1.make_with_coordinates (1, 2, 4, 9)
			create l_aabb2.make_with_coordinates (1, 2, 4, 9)
			assert ("is_equal Valid", l_aabb1 ~ l_aabb2)
		end

	test_is_equal_normal_2
			-- Normal test 2 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.is_equal
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.is_equal", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb1, l_aabb2:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			create l_aabb1.make_with_coordinates (1, 2, 4, 9)
			create l_aabb2.make_with_coordinates (2, 2, 4, 9)
			assert ("is_equal Valid", l_aabb1 /~ l_aabb2)
		end

	test_is_equal_normal_3
			-- Normal test 3 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.is_equal
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.is_equal", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb1, l_aabb2:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			create l_aabb1.make_with_coordinates (1, 2, 4, 9)
			create l_aabb2.make_with_coordinates (1, 3, 4, 9)
			assert ("is_equal Valid", l_aabb1 /~ l_aabb2)
		end

	test_is_equal_normal_4
			-- Normal test 4 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.is_equal
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.is_equal", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb1, l_aabb2:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			create l_aabb1.make_with_coordinates (1, 2, 4, 9)
			create l_aabb2.make_with_coordinates (1, 2, 5, 9)
			assert ("is_equal Valid", l_aabb1 /~ l_aabb2)
		end

	test_is_equal_normal_5
			-- Normal test 5 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.is_equal
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.is_equal", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb1, l_aabb2:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			create l_aabb1.make_with_coordinates (1, 2, 4, 9)
			create l_aabb2.make_with_coordinates (1, 2, 4, 8)
			assert ("is_equal Valid", l_aabb1 /~ l_aabb2)
		end

	test_ray_cast_normal_1
			-- Normal test 1 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.ray_cast
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.ray_cast", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb1:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_input:B2D_RAY_CAST_INPUT
		do
			create l_aabb1.make_with_coordinates (1, 2, 4, 9)
			create l_input.make_with_coordinates (15, 15, 10, 10, 12)
			if attached l_aabb1.ray_cast (l_input) as la_output then
				assert ("ray_cast Valid normal X", la_output.normal.x ~ 1)
				assert ("ray_cast Valid normal Y", la_output.normal.y ~ 0)
				assert ("ray_cast Valid normal Fraction", la_output.fraction ~ 2.2)
			else
				assert ("ray_cast Valid", False)
			end

		end

	test_ray_cast_normal_2
			-- Normal test 2 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.ray_cast
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.ray_cast", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_aabb1:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_input:B2D_RAY_CAST_INPUT
		do
			create l_aabb1.make_with_coordinates (1, 2, 4, 9)
			create l_input.make_with_coordinates (150, 150, 160, 160, 12)
			assert ("ray_cast Valid", not attached l_aabb1.ray_cast (l_input))
		end

	test_ray_cast_wrong_1
			-- Wrong test 1 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.ray_cast
		note
			testing:  "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.ray_cast", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
						"covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound", "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_retry:BOOLEAN
			l_aabb1:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_input:B2D_RAY_CAST_INPUT
			l_output: detachable B2D_RAY_CAST_OUTPUT
		do
			if not l_retry then
				create l_aabb1.make_with_coordinates (4, 9, 1, 2)
				create l_input.make_with_coordinates (150, 150, 160, 160, 12)
				l_output := l_aabb1.ray_cast (l_input)
				assert("ray_cast Wrong 1 passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_ray_cast_wrong_2
			-- Wrong test 2 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.ray_cast
		note
			testing: " covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.ray_cast",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound"
		local
			l_retry:BOOLEAN
			l_aabb1:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_input:B2D_RAY_CAST_INPUT
			l_output: detachable B2D_RAY_CAST_OUTPUT
		do
			if not l_retry then
				create l_aabb1.make_with_coordinates (1, 2, 4, 9)
				l_aabb1.put_null
				create l_input.make_with_coordinates (15, 15, 10, 10, 12)
				l_output := l_aabb1.ray_cast (l_input)
				assert("ray_cast Wrong 1 passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_is_valid_wrong
			-- Wrong test {B2D_AXIS_ALIGNED_BOUNDING_BOX}.is_valid
		note
			testing: " covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.is_valid",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make"
		local
			l_retry:BOOLEAN
			l_aabb1:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_result:BOOLEAN
		do
			if not l_retry then
				create l_aabb1.make
				l_aabb1.put_null
				l_result := l_aabb1.is_valid
				assert("is_valid Wrong passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_lower_bound_wrong
			-- Wrong test {B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound
		note
			testing: " covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.lower_bound",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make"
		local
			l_retry:BOOLEAN
			l_aabb1:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_aabb1.make
				l_aabb1.put_null
				l_result := l_aabb1.lower_bound
				assert("lower_bound Wrong passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_lower_bound_with_vector_wrong_1
			-- Wrong test 1 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.set_lower_bound_with_vector
		note
			testing: " covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.set_lower_bound_with_vector",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make"
		local
			l_retry:BOOLEAN
			l_aabb1:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_input:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_aabb1.make
				l_aabb1.put_null
				create l_input
				l_aabb1.set_lower_bound_with_vector(l_input)
				assert("set_lower_bound_with_vector Wrong 1 passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_lower_bound_with_vector_wrong_2
			-- Wrong test 2 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.set_lower_bound_with_vector
		note
			testing: " covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.set_lower_bound_with_vector",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make"
		local
			l_retry:BOOLEAN
			l_aabb1:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_input:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_aabb1.make
				create l_input.own_from_item (create {POINTER})
				l_aabb1.set_lower_bound_with_vector(l_input)
				assert("set_lower_bound_with_vector Wrong 2 passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_lower_bound_wrong
			-- Wrong test {B2D_AXIS_ALIGNED_BOUNDING_BOX}.set_lower_bound
		note
			testing: " covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.set_lower_bound",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make"
		local
			l_retry:BOOLEAN
			l_aabb1:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			if not l_retry then
				create l_aabb1.make
				l_aabb1.put_null
				l_aabb1.set_lower_bound(0, 0)
				assert("set_lower_bound Wrong passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end











	test_upper_bound_wrong
			-- Wrong test {B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound
		note
			testing: " covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.upper_bound",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make"
		local
			l_retry:BOOLEAN
			l_aabb1:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_aabb1.make
				l_aabb1.put_null
				l_result := l_aabb1.upper_bound
				assert("upper_bound Wrong passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_upper_bound_with_vector_wrong_1
			-- Wrong test 1 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.set_upper_bound_with_vector
		note
			testing: " covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.set_upper_bound_with_vector",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make"
		local
			l_retry:BOOLEAN
			l_aabb1:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_input:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_aabb1.make
				l_aabb1.put_null
				create l_input
				l_aabb1.set_upper_bound_with_vector(l_input)
				assert("set_upper_bound_with_vector Wrong 1 passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_upper_bound_with_vector_wrong_2
			-- Wrong test 2 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.set_upper_bound_with_vector
		note
			testing: " covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.set_upper_bound_with_vector",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make"
		local
			l_retry:BOOLEAN
			l_aabb1:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_input:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_aabb1.make
				create l_input.own_from_item (create {POINTER})
				l_aabb1.set_upper_bound_with_vector(l_input)
				assert("set_upper_bound_with_vector Wrong 2 passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_upper_bound_wrong
			-- Wrong test {B2D_AXIS_ALIGNED_BOUNDING_BOX}.set_upper_bound
		note
			testing: " covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.set_upper_bound",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make"
		local
			l_retry:BOOLEAN
			l_aabb1:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			if not l_retry then
				create l_aabb1.make
				l_aabb1.put_null
				l_aabb1.set_upper_bound(0, 0)
				assert("set_upper_bound Wrong passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_combine_wrong_3
			-- Wrong test 3 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine
		note
			testing: " covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates"
		local
			l_retry:BOOLEAN
			l_aabb1, l_aabb2, l_aabb3:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			if not l_retry then
				create l_aabb1.make_with_coordinates (3, 1, 6, 1)
				create l_aabb2.make
				l_aabb1.put_null
				l_aabb3:= l_aabb1.combine (l_aabb2)
				assert("combine Wrong 3 passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_combine_wrong_4
			-- Wrong test 4 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine
		note
			testing: " covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates"
		local
			l_retry:BOOLEAN
			l_aabb1, l_aabb2, l_aabb3:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			if not l_retry then
				create l_aabb1.make_with_coordinates (3, 1, 6, 1)
				create l_aabb2.make
				l_aabb2.put_null
				l_aabb3:= l_aabb1.combine (l_aabb2)
				assert("combine Wrong 4 passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_contains_wrong_3
			-- Wrong test 3 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.contains
		note
			testing: " covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.contains",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates"
		local
			l_retry:BOOLEAN
			l_aabb1, l_aabb2:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_result:BOOLEAN
		do
			if not l_retry then
				create l_aabb1.make_with_coordinates (3, 1, 6, 1)
				create l_aabb2.make
				l_aabb1.put_null
				l_result := l_aabb1.contains (l_aabb2)
				assert("contains Wrong 3 passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_contains_wrong_4
			-- Wrong test 4 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.contains
		note
			testing: " covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.contains",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates"
		local
			l_retry:BOOLEAN
			l_aabb1, l_aabb2:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_result:BOOLEAN
		do
			if not l_retry then
				create l_aabb1.make_with_coordinates (3, 1, 6, 1)
				create l_aabb2.make
				l_aabb2.put_null
				l_result := l_aabb1.contains (l_aabb2)
				assert("contains Wrong 4 passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_combine_into_current_wrong_3
			-- Wrong test 3 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine_into_current
		note
			testing: " covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine_into_current",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates"
		local
			l_retry:BOOLEAN
			l_aabb1, l_aabb2:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			if not l_retry then
				create l_aabb1.make_with_coordinates (3, 1, 6, 1)
				create l_aabb2.make
				l_aabb1.put_null
				l_aabb1.combine_into_current (l_aabb2)
				assert("combine_into_current Wrong 3 passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_combine_into_current_wrong_4
			-- Wrong test 4 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine_into_current
		note
			testing: " covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.combine_into_current",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates"
		local
			l_retry:BOOLEAN
			l_aabb1, l_aabb2:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			if not l_retry then
				create l_aabb1.make_with_coordinates (3, 1, 6, 1)
				create l_aabb2.make_with_coordinates (0, 0, 1, 1)
				l_aabb2.put_null
				l_aabb1.combine_into_current (l_aabb2)
				assert("combine_into_current Wrong 4 passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_center_wrong_2
			-- Wrong test 2 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.center
		note
			testing: " covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.center",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates"
		local
			l_retry:BOOLEAN
			l_aabb1:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_aabb1.make_with_coordinates (0, 0, 1, 1)
				l_aabb1.put_null
				l_result := l_aabb1.center
				assert("center Wrong passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_extends_wrong_2
			-- Wrong test 2 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.extends
		note
			testing: " covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.extends",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates"
		local
			l_retry:BOOLEAN
			l_aabb1:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_aabb1.make_with_coordinates (0, 0, 1, 1)
				l_aabb1.put_null
				l_result := l_aabb1.extends
				assert("extends Wrong passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_perimeter_wrong_2
			-- Wrong test 2 {B2D_AXIS_ALIGNED_BOUNDING_BOX}.perimeter
		note
			testing: " covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.perimeter",
					 "covers/{B2D_AXIS_ALIGNED_BOUNDING_BOX}.make_with_coordinates"
		local
			l_retry:BOOLEAN
			l_aabb1:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_result:REAL_32
		do
			if not l_retry then
				create l_aabb1.make_with_coordinates (0, 0, 1, 1)
				l_aabb1.put_null
				l_result := l_aabb1.perimeter
				assert("perimeter Wrong passed.", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end


end
