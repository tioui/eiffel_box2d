note
	description: "Tests for {B2D_SHAPE_EDGE}"
	author: "Louis Marchand"
	date: "Thu, 12 Jun 2020 19:25:33 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_SHAPE_EDGE_TESTS

inherit
	EQA_TEST_SET
	B2D_COMPARE_REAL
		undefine
			default_create
		end
	B2D_ANY
		undefine
			default_create
		end

feature -- Test routines

	test_make_normal
			-- Normal test for `make'
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.make", "covers/{B2D_SHAPE_EDGE}.start_vertex", "covers/{B2D_SHAPE_EDGE}.end_vertex"
		local
			l_shape:B2D_SHAPE_EDGE
		do
			create l_shape.make (-4.36659, 5.01439, -0.67469, 0.33661)
			assert ("make not valid: start_vertex.x", l_shape.start_vertex.x ~ -4.36659)
			assert ("make not valid: start_vertex.y", l_shape.start_vertex.y ~ 5.01439)
			assert ("make not valid: end_vertex.x", l_shape.end_vertex.x ~ -0.67469)
			assert ("make not valid: end_vertex.y", l_shape.end_vertex.y ~ 0.33661)
		end

	test_make_limit
			-- Limit test for `make'
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.make", "covers/{B2D_SHAPE_EDGE}.start_vertex", "covers/{B2D_SHAPE_EDGE}.end_vertex"
		local
			l_shape:B2D_SHAPE_EDGE
		do
			create l_shape.make (0, 0, 0, 0)
			assert ("make not valid: start_vertex.x", l_shape.start_vertex.x ~ 0)
			assert ("make not valid: start_vertex.y", l_shape.start_vertex.y ~ 0)
			assert ("make not valid: end_vertex.x", l_shape.end_vertex.x ~ 0)
			assert ("make not valid: end_vertex.y", l_shape.end_vertex.y ~ 0)
		end

	test_before_vertex_normal
			-- Normal test for `before_vertex'
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.make", "covers/{B2D_SHAPE_EDGE}.before_vertex", "covers/{B2D_SHAPE_EDGE}.set_before_vertex", "covers/{B2D_SHAPE_EDGE}.clear_before_vertex"
		local
			l_shape:B2D_SHAPE_EDGE
			l_vertex:B2D_VECTOR_2D
		do
			create l_shape.make (-4.36659, 5.01439, -0.67469, 0.33661)
			l_shape.clear_before_vertex
			create l_vertex.make_with_coordinates (-2.52443, -8.83484)
			l_shape.set_before_vertex_with_vector (l_vertex)
			assert ("set_before_vertex_with_vector not valid: has_before_vertex", l_shape.has_before_vertex)
			assert ("set_before_vertex_with_vector not valid: before_vertex", l_shape.before_vertex ~ l_vertex)
			l_shape.clear_before_vertex
			assert ("clear_before_vertex not valid: has_before_vertex", not l_shape.has_before_vertex)
			l_shape.set_before_vertex (-2.52443, -8.83484)
			assert ("set_before_vertex not valid: has_before_vertex", l_shape.has_before_vertex)
			assert ("set_before_vertex not valid: before_vertex.x", l_shape.before_vertex.x ~ -2.52443)
			assert ("set_before_vertex not valid: before_vertex.y", l_shape.before_vertex.y ~ -8.83484)
		end

	test_before_vertex_limit
			-- Limit test for `before_vertex'
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.make", "covers/{B2D_SHAPE_EDGE}.before_vertex", "covers/{B2D_SHAPE_EDGE}.set_before_vertex", "covers/{B2D_SHAPE_EDGE}.clear_before_vertex"
		local
			l_shape:B2D_SHAPE_EDGE
			l_vertex:B2D_VECTOR_2D
		do
			create l_shape.make (-4.36659, 5.01439, -0.67469, 0.33661)
			create l_vertex.make_with_coordinates (-4.36659, 5.01439)
			l_shape.set_before_vertex_with_vector (l_vertex)
			assert ("set_before_vertex_with_vector not valid: before_vertex", l_shape.before_vertex ~ l_vertex)
		end

	test_before_vertex_wrong
			-- Wrong test for `before_vertex'
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.make", "covers/{B2D_SHAPE_EDGE}.before_vertex", "covers/{B2D_SHAPE_EDGE}.set_before_vertex", "covers/{B2D_SHAPE_EDGE}.clear_before_vertex"
		local
			l_retry:BOOLEAN
			l_result:B2D_VECTOR_2D
			l_shape:B2D_SHAPE_EDGE
		do
			if not l_retry then
				create l_shape.make (-4.36659, 5.01439, -0.67469, 0.33661)
				l_shape.clear_before_vertex
				l_result := l_shape.before_vertex
				assert("before_vertex Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_after_vertex_normal
			-- Normal test for `after_vertex'
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.make", "covers/{B2D_SHAPE_EDGE}.after_vertex", "covers/{B2D_SHAPE_EDGE}.set_after_vertex", "covers/{B2D_SHAPE_EDGE}.clear_after_vertex"
		local
			l_shape:B2D_SHAPE_EDGE
			l_vertex:B2D_VECTOR_2D
		do
			create l_shape.make (-4.36659, 5.01439, -0.67469, 0.33661)
			l_shape.clear_after_vertex
			create l_vertex.make_with_coordinates (-2.52443, -8.83484)
			l_shape.set_after_vertex_with_vector (l_vertex)
			assert ("set_after_vertex_with_vector not valid: has_after_vertex", l_shape.has_after_vertex)
			assert ("set_after_vertex_with_vector not valid: after_vertex", l_shape.after_vertex ~ l_vertex)
			l_shape.clear_after_vertex
			assert ("clear_after_vertex not valid: has_after_vertex", not l_shape.has_after_vertex)
			l_shape.set_after_vertex (-2.52443, -8.83484)
			assert ("set_after_vertex not valid: has_after_vertex", l_shape.has_after_vertex)
			assert ("set_after_vertex not valid: after_vertex.x", l_shape.after_vertex.x ~ -2.52443)
			assert ("set_after_vertex not valid: after_vertex.y", l_shape.after_vertex.y ~ -8.83484)
		end

	test_after_vertex_limit
			-- Limit test for `after_vertex'
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.make", "covers/{B2D_SHAPE_EDGE}.after_vertex", "covers/{B2D_SHAPE_EDGE}.set_after_vertex", "covers/{B2D_SHAPE_EDGE}.clear_after_vertex"
		local
			l_shape:B2D_SHAPE_EDGE
			l_vertex:B2D_VECTOR_2D
		do
			create l_shape.make (-4.36659, 5.01439, -0.67469, 0.33661)
			create l_vertex.make_with_coordinates (-0.67469, 0.33661)
			l_shape.set_after_vertex_with_vector (l_vertex)
			assert ("set_after_vertex_with_vector not valid: after_vertex", l_shape.after_vertex ~ l_vertex)
		end

	test_after_vertex_wrong_1
			-- Wrong test 1 for `after_vertex'
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.make", "covers/{B2D_SHAPE_EDGE}.after_vertex", "covers/{B2D_SHAPE_EDGE}.set_after_vertex", "covers/{B2D_SHAPE_EDGE}.clear_after_vertex"
		local
			l_retry:BOOLEAN
			l_result:B2D_VECTOR_2D
			l_shape:B2D_SHAPE_EDGE
		do
			if not l_retry then
				create l_shape.make (-4.36659, 5.01439, -0.67469, 0.33661)
				l_shape.clear_after_vertex
				l_result := l_shape.after_vertex
				assert("after_vertex Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_contains_normal
			-- Normal test for `contains_with_vector'
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.make", "covers/{B2D_SHAPE_EDGE}.contains"
		local
			l_shape:B2D_SHAPE_EDGE
			l_transform:B2D_TRANSFORM
			l_point:B2D_VECTOR_2D
		do
			create l_point.make_with_coordinates (10.1, 5)
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			create l_shape.make (-4.36659, 5.01439, -0.67469, 0.33661)
			assert ("contains not valid", not l_shape.contains_with_vector (l_transform, l_point))
		end

	test_child_count
			-- Normal test for child_count
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.make", "covers/{B2D_SHAPE_EDGE}.child_count"
		local
			l_shape:B2D_SHAPE
		do
			create {B2D_SHAPE_EDGE}l_shape.make (-4.36659, 5.01439, -0.67469, 0.33661)
			assert("child_count Valid", l_shape.child_count ~ 1)
		end

	test_ray_cast_normal_1
			-- Normal test 1 for ray_cast
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.make", "covers/{B2D_SHAPE_EDGE}.ray_cast"
		local
			l_shape:B2D_SHAPE_EDGE
			l_transform:B2D_TRANSFORM
			l_input:B2D_RAY_CAST_INPUT
		do
			create l_shape.make (0, 10, 0, -10)
			create l_input.make_with_coordinates (10, 0, -10, 0, 1)
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			if attached l_shape.ray_cast (l_input, l_transform) as la_output then
				assert("ray_cast Valid. Normal: X", la_output.normal.x ~ 1)
				assert("ray_cast Valid. Normal: Y", la_output.normal.y ~ 0)
				assert("ray_cast Valid. Fraction", la_output.fraction ~ 0.5)
			else
				assert("ray_cast Valid", False)
			end
		end

	test_ray_cast_normal_2
			-- Normal test 2 for ray_cast
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.make", "covers/{B2D_SHAPE_EDGE}.ray_cast"
		local
			l_shape:B2D_SHAPE_EDGE
			l_transform:B2D_TRANSFORM
			l_input:B2D_RAY_CAST_INPUT
		do
			create l_shape.make (0, 10, 0, -10)
			create l_input.make_with_coordinates (100, 0, 50, 0, 1)
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			if attached l_shape.ray_cast (l_input, l_transform) as la_output then
				assert("ray_cast Valid", False)
			end
		end

	test_ray_cast_normal_3
			-- Normal test 3 for ray_cast
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.make", "covers/{B2D_SHAPE}.ray_cast"
		local
			l_shape:B2D_SHAPE
			l_transform:B2D_TRANSFORM
			l_input:B2D_RAY_CAST_INPUT
		do
			create {B2D_SHAPE_EDGE}l_shape.make (0, 10, 0, -10)
			create l_input.make_with_coordinates (10, 0, -10, 0, 1)
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			if attached l_shape.ray_cast (l_input, l_transform, 1) as la_output then
				assert("ray_cast Valid. Normal: X", la_output.normal.x ~ 1)
				assert("ray_cast Valid. Normal: Y", la_output.normal.y ~ 0)
				assert("ray_cast Valid. Fraction", la_output.fraction ~ 0.5)
			else
				assert("ray_cast Valid", False)
			end
		end

	test_ray_cast_normal_4
			-- Normal test 4 for ray_cast
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.make", "covers/{B2D_SHAPE}.ray_cast"
		local
			l_shape:B2D_SHAPE
			l_transform:B2D_TRANSFORM
			l_input:B2D_RAY_CAST_INPUT
		do
			create {B2D_SHAPE_EDGE}l_shape.make (0, 10, 0, -10)
			create l_input.make_with_coordinates (100, 0, 50, 0, 1)
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			if attached l_shape.ray_cast (l_input, l_transform, 1) as la_output then
				assert("ray_cast Valid", False)
			end
		end

	test_ray_cast_wrong_1
			-- Wrong test 1 for ray_cast
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.make", "covers/{B2D_SHAPE}.ray_cast"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE
			l_input:B2D_RAY_CAST_INPUT
			l_transform:B2D_TRANSFORM
			l_output: detachable B2D_RAY_CAST_OUTPUT
		do
			if not l_retry then
				create l_transform.make_with_coordinates_and_angle (0, 0, 0)
				create {B2D_SHAPE_EDGE}l_shape.make (0, 10, 0, -10)
				create l_input.make_with_coordinates (150, 150, 160, 160, 12)
				l_output := l_shape.ray_cast (l_input, l_transform, 2)
				assert("ray_cast Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_ray_cast_wrong_2
			-- Wrong test 2 for ray_cast
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.make", "covers/{B2D_SHAPE}.ray_cast"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE
			l_input:B2D_RAY_CAST_INPUT
			l_transform:B2D_TRANSFORM
			l_output: detachable B2D_RAY_CAST_OUTPUT
		do
			if not l_retry then
				create l_transform.make_with_coordinates_and_angle (0, 0, 0)
				create {B2D_SHAPE_EDGE}l_shape.make (0, 10, 0, -10)
				create l_input.make_with_coordinates (150, 150, 160, 160, 12)
				l_output := l_shape.ray_cast (l_input, l_transform, 0)
				assert("ray_cast Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_mass_normal
			-- Normal test for mass
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.make", "covers/{B2D_SHAPE_EDGE}.mass"
		local
			l_shape:B2D_SHAPE_EDGE
			l_mass:B2D_MASS
		do
			create l_shape.make (0, 10, 0, -10)
			l_mass := l_shape.mass (10)
			assert("mass Valid: center.x", l_mass.center.x ~ 0)
			assert("mass Valid: center.y", l_mass.center.y ~ 0)
			assert("mass Valid: Mass", compare_real_32 (l_mass.mass, 0))
			assert("mass Valid: inertia", compare_real_32 (l_mass.inertia, 0))
		end

	test_mass_limit
			-- Limit test for mass
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.make", "covers/{B2D_SHAPE_EDGE}.mass"
		local
			l_shape:B2D_SHAPE_EDGE
			l_mass:B2D_MASS
		do
			create l_shape.make (0, 10, 0, -10)
			l_mass := l_shape.mass (0)
			assert("mass Valid: center.x", l_mass.center.x ~ 0)
			assert("mass Valid: center.y", l_mass.center.y ~ 0)
			assert("mass Valid: Mass", compare_real_32 (l_mass.mass, 0))
			assert("mass Valid: inertia", compare_real_32 (l_mass.inertia, 0))
		end

	test_axis_aligned_bounding_box_normal_1
			-- Normal test 1 for axis_aligned_bounding_box
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.make_box", "covers/{B2D_SHAPE_EDGE}.axis_aligned_bounding_box"
		local
			l_shape:B2D_SHAPE_EDGE
			l_result:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_transform:B2D_TRANSFORM
		do
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			create l_shape.make (0, 10, 0, -10)
			l_result := l_shape.axis_aligned_bounding_box (l_transform)
			assert("axis_aligned_bounding_box Valid: lower_bound.x", l_result.lower_bound.x ~ 0 - l_shape.width)
			assert("axis_aligned_bounding_box Valid: lower_bound.y", l_result.lower_bound.y ~ -10 - l_shape.width)
			assert("axis_aligned_bounding_box Valid: upper_bound.x", l_result.upper_bound.x ~ 0 + l_shape.width)
			assert("axis_aligned_bounding_box Valid: upper_bound.y", l_result.upper_bound.y ~ 10 + l_shape.width)
		end

	test_axis_aligned_bounding_box_normal_2
			-- Normal test 2 for axis_aligned_bounding_box
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.make_box", "covers/{B2D_SHAPE}.axis_aligned_bounding_box"
		local
			l_shape:B2D_SHAPE
			l_edge:B2D_SHAPE_EDGE
			l_result:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_transform:B2D_TRANSFORM
		do
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			create l_edge.make (0, 10, 0, -10)
			l_shape := l_edge
			l_result := l_shape.axis_aligned_bounding_box (l_transform, 1)
			assert("axis_aligned_bounding_box Valid: lower_bound.x", l_result.lower_bound.x ~ 0 - l_edge.width)
			assert("axis_aligned_bounding_box Valid: lower_bound.y", l_result.lower_bound.y ~ -10 - l_edge.width)
			assert("axis_aligned_bounding_box Valid: upper_bound.x", l_result.upper_bound.x ~ 0 + l_edge.width)
			assert("axis_aligned_bounding_box Valid: upper_bound.y", l_result.upper_bound.y ~ 10 + l_edge.width)
		end

	test_axis_aligned_bounding_box_wrong_1
			-- Wrong test 1 for axis_aligned_bounding_box
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.make_box", "covers/{B2D_SHAPE_EDGE}.axis_aligned_bounding_box"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE
			l_result:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_transform:B2D_TRANSFORM
		do
			if not l_retry then
				create l_transform.make_with_coordinates_and_angle (0, 0, 0)
				create {B2D_SHAPE_EDGE}l_shape.make (0, 10, 0, -10)
				l_result := l_shape.axis_aligned_bounding_box (l_transform, 0)
				assert("axis_aligned_bounding_box Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_axis_aligned_bounding_box_wrong_2
			-- Wrong test 2 for axis_aligned_bounding_box
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.make_box", "covers/{B2D_SHAPE_EDGE}.axis_aligned_bounding_box"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE
			l_result:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_transform:B2D_TRANSFORM
		do
			if not l_retry then
				create l_transform.make_with_coordinates_and_angle (0, 0, 0)
				create {B2D_SHAPE_EDGE}l_shape.make (0, 10, 0, -10)
				l_result := l_shape.axis_aligned_bounding_box (l_transform, 2)
				assert("axis_aligned_bounding_box Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_clear_after_vertex_wrong
			-- Wrong test for `clear_after_vertex'
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.clear_after_vertex"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_EDGE
		do
			if not l_retry then
				create l_shape.own_from_item(create {POINTER})
				l_shape.clear_after_vertex
				assert("clear_after_vertex Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_after_vertex_wrong
			-- Wrong test for `set_after_vertex'
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.set_after_vertex"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_EDGE
		do
			if not l_retry then
				create l_shape.own_from_item(create {POINTER})
				l_shape.set_after_vertex (10, 10)
				assert("set_after_vertex Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_after_vertex_with_vector_wrong
			-- Wrong test for `set_after_vertex_with_vector'
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.set_after_vertex_with_vector"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_EDGE
		do
			if not l_retry then
				create l_shape.own_from_item(create {POINTER})
				l_shape.set_after_vertex_with_vector (create {B2D_VECTOR_2D})
				assert("set_after_vertex_with_vector Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_clear_before_vertex_wrong
			-- Wrong test for `clear_before_vertex'
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.clear_before_vertex"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_EDGE
		do
			if not l_retry then
				create l_shape.own_from_item(create {POINTER})
				l_shape.clear_before_vertex
				assert("clear_before_vertex Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_before_vertex_wrong
			-- Wrong test for `set_before_vertex'
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.set_before_vertex"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_EDGE
		do
			if not l_retry then
				create l_shape.own_from_item(create {POINTER})
				l_shape.set_before_vertex (10, 10)
				assert("set_before_vertex Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_before_vertex_with_vector_wrong
			-- Wrong test for `set_before_vertex_with_vector'
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.set_before_vertex_with_vector"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_EDGE
		do
			if not l_retry then
				create l_shape.own_from_item(create {POINTER})
				l_shape.set_before_vertex_with_vector (create {B2D_VECTOR_2D})
				assert("set_before_vertex_with_vector Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_start_vertex_with_vector_normal
			-- Normal test for set_start_vertex_with_vector
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.set_start_vertex_with_vector",
					  "covers/{B2D_SHAPE_EDGE}.start_vertex",
					  "covers/{B2D_SHAPE_EDGE}.default_create"
		local
			l_shape:B2D_SHAPE_EDGE
			l_vector:B2D_VECTOR_2D
		do
			create l_shape.make (1, 1, 10, 10)
			create l_vector.make_with_coordinates (7.90835, -0.03097)
			l_shape.set_start_vertex_with_vector (l_vector)
			assert ("set_start_vertex_with_vector valid", l_shape.start_vertex ~ l_vector)
		end

	test_set_start_vertex_with_vector_wrong_1
			-- Wrong test 1 for set_start_vertex_with_vector
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.set_start_vertex_with_vector",
					  "covers/{B2D_SHAPE_EDGE}.start_vertex",
					  "covers/{B2D_SHAPE_EDGE}.default_create"
		local
			l_shape:B2D_SHAPE_EDGE
			l_vector:B2D_VECTOR_2D
			l_retry:BOOLEAN
		do
			if not l_retry then
				create l_shape.make (1, 1, 10, 10)
				create l_vector.make_with_coordinates (7.90835, -0.03097)
				l_shape.put_null
				l_shape.set_start_vertex_with_vector (l_vector)
				assert ("make_with_coordinates valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_start_vertex_with_vector_wrong_2
			-- Wrong test 2 for set_start_vertex_with_vector
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.set_start_vertex_with_vector",
					  "covers/{B2D_SHAPE_EDGE}.start_vertex",
					  "covers/{B2D_SHAPE_EDGE}.default_create"
		local
			l_shape:B2D_SHAPE_EDGE
			l_vector:B2D_VECTOR_2D
			l_retry:BOOLEAN
		do
			if not l_retry then
				create l_shape.make (1, 1, 10, 10)
				create l_vector.make_with_coordinates (7.90835, -0.03097)
				l_vector.put_null
				l_shape.set_start_vertex_with_vector (l_vector)
				assert ("make_with_coordinates valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_start_vertex_normal
			-- Normal test for set_start_vertex_with_vector
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.set_start_vertex",
					  "covers/{B2D_SHAPE_EDGE}.start_vertex",
					  "covers/{B2D_SHAPE_EDGE}.default_create"
		local
			l_shape:B2D_SHAPE_EDGE
		do
			create l_shape.make (1, 1, 10, 10)
			l_shape.set_start_vertex (7.90835, -0.03097)
			assert ("make_with_coordinates valid X", l_shape.start_vertex.x ~ 7.90835)
			assert ("make_with_coordinates valid Y", l_shape.start_vertex.y ~ -0.03097)
		end

	test_set_start_vertex_wrong_1
			-- Wrong test 1 for set_start_vertex
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.set_start_vertex",
					  "covers/{B2D_SHAPE_EDGE}.start_vertex",
					  "covers/{B2D_SHAPE_EDGE}.default_create"
		local
			l_shape:B2D_SHAPE_EDGE
			l_retry:BOOLEAN
		do
			if not l_retry then
				create l_shape.make (1, 1, 10, 10)
				l_shape.put_null
				l_shape.set_start_vertex (0, 0)
				assert ("make_with_coordinates valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_start_vertex_wrong_1
			-- Wrong test 1 for start_vertex
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.start_vertex",
					  "covers/{B2D_SHAPE_EDGE}.default_create"
		local
			l_shape:B2D_SHAPE_EDGE
			l_retry:BOOLEAN
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_shape.make (1, 1, 10, 10)
				l_shape.put_null
				l_result := l_shape.start_vertex
				assert ("start_vertex valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end









	test_set_end_vertex_with_vector_normal
			-- Normal test for set_end_vertex_with_vector
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.set_end_vertex_with_vector",
					  "covers/{B2D_SHAPE_EDGE}.end_vertex",
					  "covers/{B2D_SHAPE_EDGE}.default_create"
		local
			l_shape:B2D_SHAPE_EDGE
			l_vector:B2D_VECTOR_2D
		do
			create l_shape.make (1, 1, 10, 10)
			create l_vector.make_with_coordinates (7.90835, -0.03097)
			l_shape.set_end_vertex_with_vector (l_vector)
			assert ("set_end_vertex_with_vector valid", l_shape.end_vertex ~ l_vector)
		end

	test_set_end_vertex_with_vector_wrong_1
			-- Wrong test 1 for set_end_vertex_with_vector
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.set_end_vertex_with_vector",
					  "covers/{B2D_SHAPE_EDGE}.end_vertex",
					  "covers/{B2D_SHAPE_EDGE}.default_create"
		local
			l_shape:B2D_SHAPE_EDGE
			l_vector:B2D_VECTOR_2D
			l_retry:BOOLEAN
		do
			if not l_retry then
				create l_shape.make (1, 1, 10, 10)
				create l_vector.make_with_coordinates (7.90835, -0.03097)
				l_shape.put_null
				l_shape.set_end_vertex_with_vector (l_vector)
				assert ("make_with_coordinates valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_end_vertex_with_vector_wrong_2
			-- Wrong test 2 for set_end_vertex_with_vector
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.set_end_vertex_with_vector",
					  "covers/{B2D_SHAPE_EDGE}.end_vertex",
					  "covers/{B2D_SHAPE_EDGE}.default_create"
		local
			l_shape:B2D_SHAPE_EDGE
			l_vector:B2D_VECTOR_2D
			l_retry:BOOLEAN
		do
			if not l_retry then
				create l_shape.make (1, 1, 10, 10)
				create l_vector.make_with_coordinates (7.90835, -0.03097)
				l_vector.put_null
				l_shape.set_end_vertex_with_vector (l_vector)
				assert ("make_with_coordinates valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_end_vertex_normal
			-- Normal test for set_end_vertex_with_vector
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.set_end_vertex",
					  "covers/{B2D_SHAPE_EDGE}.end_vertex",
					  "covers/{B2D_SHAPE_EDGE}.default_create"
		local
			l_shape:B2D_SHAPE_EDGE
		do
			create l_shape.make (1, 1, 10, 10)
			l_shape.set_end_vertex (7.90835, -0.03097)
			assert ("make_with_coordinates valid X", l_shape.end_vertex.x ~ 7.90835)
			assert ("make_with_coordinates valid Y", l_shape.end_vertex.y ~ -0.03097)
		end

	test_set_end_vertex_wrong_1
			-- Wrong test 1 for set_end_vertex
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.set_end_vertex",
					  "covers/{B2D_SHAPE_EDGE}.end_vertex",
					  "covers/{B2D_SHAPE_EDGE}.default_create"
		local
			l_shape:B2D_SHAPE_EDGE
			l_retry:BOOLEAN
		do
			if not l_retry then
				create l_shape.make (1, 1, 10, 10)
				l_shape.put_null
				l_shape.set_end_vertex (0, 0)
				assert ("make_with_coordinates valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_end_vertex_wrong_1
			-- Wrong test 1 for end_vertex
		note
			testing:  "covers/{B2D_SHAPE_EDGE}.end_vertex",
					  "covers/{B2D_SHAPE_EDGE}.default_create"
		local
			l_shape:B2D_SHAPE_EDGE
			l_retry:BOOLEAN
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_shape.make (1, 1, 10, 10)
				l_shape.put_null
				l_result := l_shape.end_vertex
				assert ("end_vertex valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

end


