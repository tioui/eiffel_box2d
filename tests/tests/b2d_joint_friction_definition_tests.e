note
	description: "Tests for {B2D_JOINT_FRICTION_DEFINITION}"
	author: "Patrick Boucher"
	date: "Wed, 24 Jun 2020 21:56:59 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_JOINT_FRICTION_DEFINITION_TESTS

inherit
	EQA_TEST_SET
		redefine
			on_prepare
		end

feature {NONE} -- Implemetation

	on_prepare
			-- <Precursor>
		do
			create world.make (0, 1)
			if world.exists and not world.is_locked then
				assert ("World creation succeed", True)
			else
				assert ("World creation succeed", False)
			end
		end


feature -- Test routines

	test_make
			-- Test routine for {B2D_FRICTION_JOINT_DEFINITION}.make
		note
			testing:  "covers/{B2D_JOINT_FRICTION_DEFINITION}.make",
			          "covers/{B2D_JOINT_FRICTION_DEFINITION}.make_with_vector"
		local
			l_joint_definition:B2D_JOINT_FRICTION_DEFINITION
			l_body_definition:B2D_BODY_DEFINITION
			l_body_1, l_body_2:B2D_BODY
		do
			create l_body_definition
			world.create_body (l_body_definition)
			world.create_body (l_body_definition)
			l_body_1 := world.bodies [1]
			l_body_2 := world.bodies [2]
			create l_joint_definition.make (l_body_1, l_body_2, 10, 10)
			assert ("make valid",
				l_joint_definition.body_1 ~ l_body_1 and l_joint_definition.body_2 ~ l_body_2)
		end

	test_local_anchor
			-- Test routine for {B2D_FRICTION_JOINT_DEFINITION}.local_anchor_body_*
		note
			testing:  "covers/{B2D_JOINT_FRICTION_DEFINITION}.local_anchor_body_1",
			          "covers/{B2D_JOINT_FRICTION_DEFINITION}.local_anchor_body_2"
		local
			l_joint_definition:B2D_JOINT_FRICTION_DEFINITION
			l_body_definition:B2D_BODY_DEFINITION
		do
			create l_body_definition
			world.create_body (l_body_definition)
			l_body_definition.set_position (10, 10)
			world.create_body (l_body_definition)
			create l_joint_definition.make (world.bodies [1], world.bodies [2], 5, 5)
			assert ("local_anchor_body_1 valid",
				l_joint_definition.local_anchor_1.x ~ 5 and l_joint_definition.local_anchor_1.y ~ 5)
			assert ("local_anchor_body_2 valid",
				l_joint_definition.local_anchor_2.x ~ -5 and l_joint_definition.local_anchor_2.y ~ -5)
		end

	test_max_force
			-- Test routine for {B2D_FRICTION_JOINT_DEFINITION}.set_max_force
		note
			testing:  "covers/{B2D_JOINT_FRICTION_DEFINITION}.set_max_force",
			          "covers/{B2D_JOINT_FRICTION_DEFINITION}.max_force"
		local
			l_joint_definition:B2D_JOINT_FRICTION_DEFINITION
			l_body_definition:B2D_BODY_DEFINITION
		do
			create l_body_definition
			world.create_body (l_body_definition)
			world.create_body (l_body_definition)
			create l_joint_definition.make (world.bodies [1], world.bodies [2], 0, 0)
			l_joint_definition.set_max_force (0.795)
			assert ("set_max_force valid", l_joint_definition.max_force ~ 0.795)
		end

	test_max_torque
			-- Test routine for {B2D_FRICTION_JOINT_DEFINITION}.set_max_torque
		note
			testing:  "covers/{B2D_JOINT_FRICTION_DEFINITION}.set_max_torque",
			          "covers/{B2D_JOINT_FRICTION_DEFINITION}.ma_torque"
		local
			l_joint_definition:B2D_JOINT_FRICTION_DEFINITION
			l_body_definition:B2D_BODY_DEFINITION
		do
			create l_body_definition
			world.create_body (l_body_definition)
			world.create_body (l_body_definition)
			create l_joint_definition.make (world.bodies [1], world.bodies [2], 0, 0)
			l_joint_definition.set_max_torque (0.375)
			assert ("set_max_torque valid", l_joint_definition.max_torque ~ 0.375)
		end

feature {NONE} -- Implementation


	world:B2D_WORLD
			-- The world that the definition will be tested in.
end


