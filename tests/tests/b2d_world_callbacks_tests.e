note
	description: "Tests for callbacks of {B2D_WORLD}"
	author: "Louis Marchand"
	date: "Thu, 11 Jun 2020 20:29:27 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_WORLD_CALLBACKS_TESTS

inherit
	EQA_TEST_SET
		redefine
			on_prepare
		end
	B2D_COMPARE_REAL
		undefine
			default_create
		end
	B2D_ANY
		undefine
			default_create
		end

feature {NONE} -- Events

	on_prepare
			-- <Precursor>
		local
			l_body_definition:B2D_BODY_DEFINITION
		do
			create world.make (0, -10)
			create l_body_definition.make_dynamic
			world.create_body (l_body_definition)
			create l_body_definition.make_dynamic
			world.create_body (l_body_definition)
			create l_body_definition.make_static
			world.create_body (l_body_definition)
			create l_body_definition.make_static
			world.create_body (l_body_definition)
			assert("Body count not valid.", world.body_count ~ 4)
			assert("Body 1 not valid.", world.bodies[1].is_dynamic)
			assert("Body 2 not valid.", world.bodies[2].is_dynamic)
			assert("Body 3 not valid.", world.bodies[3].is_static)
			assert("Body 4 not valid.", world.bodies[4].is_static)
			world.bodies[1].create_fixture_from_shape (create {B2D_SHAPE_POLYGON}.make_box (1, 1), 1.0)
			world.bodies[2].create_fixture_from_shape (create {B2D_SHAPE_POLYGON}.make_box (2, 2), 1.0)
			world.bodies[3].create_fixture_from_shape (create {B2D_SHAPE_POLYGON}.make_box (3, 3), 0.0)
			world.bodies[4].create_fixture_from_shape (create {B2D_SHAPE_POLYGON}.make_box (4, 4), 0.0)
			world.bodies[1].set_attached_data (1)
			world.bodies[2].set_attached_data (2)
			world.bodies[3].set_attached_data (3)
			world.bodies[4].set_attached_data (4)
			assert("Fixtures not valid.", across world.bodies as la_bodies all la_bodies.item.fixture_count ~ 1 end)
		end

feature -- Test routines

	test_ray_cast_query_normal_1
			-- Normal test 1 for `ray_cast_query'
		note
			testing:  "covers/{B2D_WORLD}.ray_cast_query"
		do
			world.bodies[2].set_transform (10, 10, 0)
			world.bodies[3].set_transform (20, 20, 0)
			world.bodies[4].set_transform (100, 0, 0)
			index := 0
			world.ray_cast_query (agent ray_cast_action_1, 30, 30, -30, -30)
			assert("Number of iteration valid.", index ~ 2)
		end

	test_ray_cast_query_normal_2
			-- Normal test 2 for `ray_cast_query'
		note
			testing:  "covers/{B2D_WORLD}.ray_cast_query"
		do
			world.bodies[2].set_transform (10, 10, 0)
			world.bodies[3].set_transform (20, 20, 0)
			world.bodies[4].set_transform (100, 0, 0)
			index := 0
			world.ray_cast_query (agent ray_cast_action_2, 30, 30, -30, -30)
			assert("Number of iteration valid.", index ~ 2)
		end

	test_ray_cast_query_normal_3
			-- Normal test 2 for `ray_cast_query'
		note
			testing:  "covers/{B2D_WORLD}.ray_cast_query"
		do
			world.bodies[2].set_transform (10, 10, 0)
			world.bodies[3].set_transform (20, 20, 0)
			world.bodies[4].set_transform (100, 0, 0)
			index := 0
			world.ray_cast_query (agent ray_cast_action_3, 30, 30, -30, -30)
			assert("Number of iteration valid.", index ~ 2)
		end

	test_ray_cast_query_wrong
			-- Wrong test for `ray_cast_query'.
		note
			testing:  "covers/{B2D_WORLD}.ray_cast_query"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				world.bodies[2].set_transform (10, 10, 0)
				world.bodies[3].set_transform (20, 20, 0)
				world.bodies[4].set_transform (100, 0, 0)
				world.put_null
				world.ray_cast_query (agent ray_cast_action_1, 30, 30, -30, -30)
				assert ("ray_cast_query valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_ray_cast_query_with_vectors_normal_1
			-- Normal test 1 for `ray_cast_query_with_vectors'
		note
			testing:  "covers/{B2D_WORLD}.ray_cast_query_with_vectors"
		local
			l_point_1, l_point_2:B2D_VECTOR_2D
		do
			world.bodies[2].set_transform (10, 10, 0)
			world.bodies[3].set_transform (20, 20, 0)
			world.bodies[4].set_transform (100, 0, 0)
			index := 0
			create l_point_1.make_with_coordinates (30, 30)
			create l_point_2.make_with_coordinates (-30, -30)
			world.ray_cast_query_with_vectors (agent ray_cast_action_1, l_point_1, l_point_2)
			assert("Number of iteration valid.", index ~ 2)
		end

	test_ray_cast_query_with_vectors_normal_2
			-- Normal test 2 for `ray_cast_query_with_vectors'
		note
			testing:  "covers/{B2D_WORLD}.ray_cast_query_with_vectors"
		local
			l_point_1, l_point_2:B2D_VECTOR_2D
		do
			world.bodies[2].set_transform (10, 10, 0)
			world.bodies[3].set_transform (20, 20, 0)
			world.bodies[4].set_transform (100, 0, 0)
			index := 0
			create l_point_1.make_with_coordinates (30, 30)
			create l_point_2.make_with_coordinates (-30, -30)
			world.ray_cast_query_with_vectors (agent ray_cast_action_2, l_point_1, l_point_2)
			assert("Number of iteration valid.", index ~ 2)
		end

	test_ray_cast_query_with_vectors_normal_3
			-- Normal test 2 for `ray_cast_query_with_vectors'
		note
			testing:  "covers/{B2D_WORLD}.ray_cast_query_with_vectors"
		local
			l_point_1, l_point_2:B2D_VECTOR_2D
		do
			world.bodies[2].set_transform (10, 10, 0)
			world.bodies[3].set_transform (20, 20, 0)
			world.bodies[4].set_transform (100, 0, 0)
			index := 0
			create l_point_1.make_with_coordinates (30, 30)
			create l_point_2.make_with_coordinates (-30, -30)
			world.ray_cast_query_with_vectors (agent ray_cast_action_3, l_point_1, l_point_2)
			assert("Number of iteration valid.", index ~ 2)
		end

	test_ray_cast_query_with_vectors_wrong_1
			-- Wrong test 1 for `ray_cast_query_with_vectors'.
		note
			testing:  "covers/{B2D_WORLD}.ray_cast_query_with_vectors"
		local
			l_retry:BOOLEAN
			l_point_1, l_point_2:B2D_VECTOR_2D
		do
			if not l_retry then
				world.bodies[2].set_transform (10, 10, 0)
				world.bodies[3].set_transform (20, 20, 0)
				world.bodies[4].set_transform (100, 0, 0)
				create l_point_1.make_with_coordinates (30, 30)
				create l_point_2.make_with_coordinates (-30, -30)
				world.put_null
				world.ray_cast_query_with_vectors (agent ray_cast_action_1, l_point_1, l_point_2)
				assert ("ray_cast_query_with_vectors valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_ray_cast_query_with_vectors_wrong_2
			-- Wrong test 2 for `ray_cast_query_with_vectors'.
		note
			testing:  "covers/{B2D_WORLD}.ray_cast_query_with_vectors"
		local
			l_retry:BOOLEAN
			l_point_1, l_point_2:B2D_VECTOR_2D
		do
			if not l_retry then
				world.bodies[2].set_transform (10, 10, 0)
				world.bodies[3].set_transform (20, 20, 0)
				world.bodies[4].set_transform (100, 0, 0)
				create l_point_1.make_with_coordinates (30, 30)
				create l_point_2.make_with_coordinates (-30, -30)
				l_point_1.put_null
				world.ray_cast_query_with_vectors (agent ray_cast_action_1, l_point_1, l_point_2)
				assert ("ray_cast_query_with_vectors valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_ray_cast_query_with_vectors_wrong_3
			-- Wrong test 3 for `ray_cast_query_with_vectors'.
		note
			testing:  "covers/{B2D_WORLD}.ray_cast_query_with_vectors"
		local
			l_retry:BOOLEAN
			l_point_1, l_point_2:B2D_VECTOR_2D
		do
			if not l_retry then
				world.bodies[2].set_transform (10, 10, 0)
				world.bodies[3].set_transform (20, 20, 0)
				world.bodies[4].set_transform (100, 0, 0)
				create l_point_1.make_with_coordinates (30, 30)
				create l_point_2.make_with_coordinates (-30, -30)
				l_point_2.put_null
				world.ray_cast_query_with_vectors (agent ray_cast_action_1, l_point_1, l_point_2)
				assert ("ray_cast_query_with_vectors valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_all_ray_casts_normal
			-- Normal test for `all_ray_casts'
		note
			testing:  "covers/{B2D_WORLD}.all_ray_casts"
		local
			l_result:LIST[B2D_WORLD_RAY_CAST_OUTPUT]
		do
			world.bodies[2].set_transform (10, 10, 0)
			world.bodies[3].set_transform (20, 20, 0)
			world.bodies[4].set_transform (100, 0, 0)
			l_result := world.all_ray_casts (30, 30, -30, -30)
			assert("all_ray_casts count Valid.", l_result.count ~ 3)
			assert("all_ray_casts body 1 Valid.", l_result[1].fixture.body ~ world.bodies[3])
			assert("all_ray_casts fraction 1 Valid.", compare_real_32 (l_result[1].fraction, 0.116667))
			assert("all_ray_casts normal 1 X Valid.", l_result[1].normal.x ~ 1)
			assert("all_ray_casts normal 1 Y Valid.", l_result[1].normal.y ~ 0)
			assert("all_ray_casts point 1 X Valid.", l_result[1].point.x ~ 23)
			assert("all_ray_casts point 1 Y Valid.", l_result[1].point.y ~ 23)
			assert("all_ray_casts body 2 Valid.", l_result[2].fixture.body ~ world.bodies[2])
			assert("all_ray_casts fraction 2 Valid.", compare_real_32 (l_result[2].fraction, 0.3))
			assert("all_ray_casts normal 2 X Valid.", l_result[2].normal.x ~ 1)
			assert("all_ray_casts normal 2 Y Valid.", l_result[2].normal.y ~ 0)
			assert("all_ray_casts point 2 X Valid.", l_result[2].point.x ~ 12)
			assert("all_ray_casts point 2 Y Valid.", l_result[2].point.y ~ 12)
			assert("all_ray_casts body 3 Valid.", l_result[3].fixture.body ~ world.bodies[1])
			assert("all_ray_casts fraction 3 Valid.", compare_real_32 (l_result[3].fraction, 0.483333))
			assert("all_ray_casts normal 3 X Valid.", l_result[3].normal.x ~ 1)
			assert("all_ray_casts normal 3 Y Valid.", l_result[3].normal.y ~ 0)
			assert("all_ray_casts point 3 X Valid.", l_result[3].point.x ~ 1)
			assert("all_ray_casts point 3 Y Valid.", l_result[3].point.y ~ 1)
		end

	test_all_ray_casts_wrong
			-- Wrong test for `all_ray_casts'.
		note
			testing:  "covers/{B2D_WORLD}.all_ray_casts"
		local
			l_retry:BOOLEAN
			l_result:LIST[B2D_WORLD_RAY_CAST_OUTPUT]
		do
			if not l_retry then
				world.bodies[2].set_transform (10, 10, 0)
				world.bodies[3].set_transform (20, 20, 0)
				world.bodies[4].set_transform (100, 0, 0)
				world.put_null
				l_result := world.all_ray_casts (30, 30, -30, -30)
				assert ("all_ray_casts valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_all_ray_casts_with_vectors_normal
			-- Normal test for `all_ray_casts_with_vectors'
		note
			testing:  "covers/{B2D_WORLD}.all_ray_casts_with_vectors"
		local
			l_result:LIST[B2D_WORLD_RAY_CAST_OUTPUT]
			l_point_1, l_point_2:B2D_VECTOR_2D
		do
			world.bodies[2].set_transform (10, 10, 0)
			world.bodies[3].set_transform (20, 20, 0)
			world.bodies[4].set_transform (100, 0, 0)
			create l_point_1.make_with_coordinates (30, 30)
			create l_point_2.make_with_coordinates (-30, -30)
			l_result := world.all_ray_casts_with_vectors (l_point_1, l_point_2)
			assert("all_ray_casts_with_vectors count Valid.", l_result.count ~ 3)
			assert("all_ray_casts_with_vectors body 1 Valid.", l_result[1].fixture.body ~ world.bodies[3])
			assert("all_ray_casts_with_vectors fraction 1 Valid.", compare_real_32 (l_result[1].fraction, 0.116667))
			assert("all_ray_casts_with_vectors normal 1 X Valid.", l_result[1].normal.x ~ 1)
			assert("all_ray_casts_with_vectors normal 1 Y Valid.", l_result[1].normal.y ~ 0)
			assert("all_ray_casts_with_vectors point 1 X Valid.", l_result[1].point.x ~ 23)
			assert("all_ray_casts_with_vectors point 1 Y Valid.", l_result[1].point.y ~ 23)
			assert("all_ray_casts_with_vectors body 2 Valid.", l_result[2].fixture.body ~ world.bodies[2])
			assert("all_ray_casts_with_vectors fraction 2 Valid.", compare_real_32 (l_result[2].fraction, 0.3))
			assert("all_ray_casts_with_vectors normal 2 X Valid.", l_result[2].normal.x ~ 1)
			assert("all_ray_casts_with_vectors normal 2 Y Valid.", l_result[2].normal.y ~ 0)
			assert("all_ray_casts_with_vectors point 2 X Valid.", l_result[2].point.x ~ 12)
			assert("all_ray_casts_with_vectors point 2 Y Valid.", l_result[2].point.y ~ 12)
			assert("all_ray_casts_with_vectors body 3 Valid.", l_result[3].fixture.body ~ world.bodies[1])
			assert("all_ray_casts_with_vectors fraction 3 Valid.", compare_real_32 (l_result[3].fraction, 0.483333))
			assert("all_ray_casts_with_vectors normal 3 X Valid.", l_result[3].normal.x ~ 1)
			assert("all_ray_casts_with_vectors normal 3 Y Valid.", l_result[3].normal.y ~ 0)
			assert("all_ray_casts_with_vectors point 3 X Valid.", l_result[3].point.x ~ 1)
			assert("all_ray_casts_with_vectors point 3 Y Valid.", l_result[3].point.y ~ 1)
		end

	test_all_ray_casts_with_vectors_wrong
			-- Wrong test for `all_ray_casts_with_vectors'.
		note
			testing:  "covers/{B2D_WORLD}.all_ray_casts_with_vectors"
		local
			l_retry:BOOLEAN
			l_result:LIST[B2D_WORLD_RAY_CAST_OUTPUT]
			l_point_1, l_point_2:B2D_VECTOR_2D
		do
			if not l_retry then
				world.bodies[2].set_transform (10, 10, 0)
				world.bodies[3].set_transform (20, 20, 0)
				world.bodies[4].set_transform (100, 0, 0)
				create l_point_1.make_with_coordinates (30, 30)
				create l_point_2.make_with_coordinates (-30, -30)
				world.put_null
				l_result := world.all_ray_casts_with_vectors (l_point_1, l_point_2)
				assert ("all_ray_casts valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_overlap_query_normal_1
			-- Normal test 1 for `overlap_query'
		note
			testing:  "covers/{B2D_WORLD}.overlap_query"
		do
			index := 0
			world.overlap_query (agent overlap_query_action, 1.5, 1.5, 5, 5)
			assert("Number of iteration valid.", index ~ 2)
		end

	test_overlap_query_wrong
			-- Wrong test for `overlap_query'.
		note
			testing:  "covers/{B2D_WORLD}.overlap_query"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				world.put_null
				world.overlap_query (agent overlap_query_action, 1.5, 1.5, 5, 5)
				assert ("overlap_query valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_overlap_query_with_axis_align_bounding_box_normal_1
			-- Normal test 1 for `overlap_query_with_axis_align_bounding_box'
		note
			testing:  "covers/{B2D_WORLD}.overlap_query_with_axis_align_bounding_box"
		local
			l_aabb:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			index := 0
			create l_aabb.make_with_coordinates (1.5, 1.5, 5, 5)
			world.overlap_query_with_axis_align_bounding_box (agent overlap_query_action, l_aabb)
			assert("Number of iteration valid.", index ~ 2)
		end

	test_overlap_query_with_axis_align_bounding_box_wrong_1
			-- Wrong test 1 for `overlap_query_with_axis_align_bounding_box'.
		note
			testing:  "covers/{B2D_WORLD}.overlap_query_with_axis_align_bounding_box"
		local
			l_retry:BOOLEAN
			l_aabb:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			if not l_retry then
				create l_aabb.make_with_coordinates (1.5, 1.5, 5, 5)
				world.put_null
				world.overlap_query_with_axis_align_bounding_box (agent overlap_query_action, l_aabb)
				assert ("overlap_query_with_axis_align_bounding_box valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_overlap_query_with_axis_align_bounding_box_wrong_2
			-- Wrong test 2 for `overlap_query_with_axis_align_bounding_box'.
		note
			testing:  "covers/{B2D_WORLD}.overlap_query_with_axis_align_bounding_box"
		local
			l_retry:BOOLEAN
			l_aabb:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			if not l_retry then
				create l_aabb.make_with_coordinates (1.5, 1.5, 5, 5)
				l_aabb.put_null
				world.overlap_query_with_axis_align_bounding_box (agent overlap_query_action, l_aabb)
				assert ("overlap_query_with_axis_align_bounding_box valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_all_overlaps_normal
			-- Normal test for `all_overlaps'
		note
			testing:  "covers/{B2D_WORLD}.all_overlaps"
		local
			l_result:LIST[B2D_FIXTURE]
		do
			l_result := world.all_overlaps (1.5, 1.5, 5, 5)
			assert("all_overlaps count Valid.", l_result.count ~ 3)
			assert("all_overlaps body 1 Valid.", l_result[1].body ~ world.bodies[4])
			assert("all_overlaps body 2 Valid.", l_result[2].body ~ world.bodies[3])
			assert("all_overlaps body 3 Valid.", l_result[3].body ~ world.bodies[2])
		end

	test_all_overlaps_wrong
			-- Wrong test for `all_overlaps'.
		note
			testing:  "covers/{B2D_WORLD}.all_overlaps"
		local
			l_retry:BOOLEAN
			l_result:LIST[B2D_FIXTURE]
		do
			if not l_retry then
				world.put_null
				l_result := world.all_overlaps (1.5, 1.5, 5, 5)
				assert ("all_overlaps valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_all_overlaps_with_axis_align_bounding_box_normal
			-- Normal test for `all_overlaps_with_axis_align_bounding_box'
		note
			testing:  "covers/{B2D_WORLD}.all_overlaps_with_axis_align_bounding_box"
		local
			l_result:LIST[B2D_FIXTURE]
			l_aabb:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			create l_aabb.make_with_coordinates (1.5, 1.5, 5, 5)
			l_result := world.all_overlaps_with_axis_align_bounding_box (l_aabb)
			assert("all_overlaps_with_axis_align_bounding_box count Valid.", l_result.count ~ 3)
			assert("all_overlaps_with_axis_align_bounding_box body 1 Valid.", l_result[1].body ~ world.bodies[4])
			assert("all_overlaps_with_axis_align_bounding_box body 2 Valid.", l_result[2].body ~ world.bodies[3])
			assert("all_overlaps_with_axis_align_bounding_box body 3 Valid.", l_result[3].body ~ world.bodies[2])
		end

	test_all_overlaps_with_axis_align_bounding_box_wrong_1
			-- Wrong test 1 for `all_overlaps_with_axis_align_bounding_box'.
		note
			testing:  "covers/{B2D_WORLD}.all_overlaps_with_axis_align_bounding_box"
		local
			l_retry:BOOLEAN
			l_aabb:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_result:LIST[B2D_FIXTURE]
		do
			if not l_retry then
				create l_aabb.make_with_coordinates (1.5, 1.5, 5, 5)
				world.put_null
				l_result := world.all_overlaps_with_axis_align_bounding_box (l_aabb)
				assert ("all_overlaps_with_axis_align_bounding_box valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_all_overlaps_with_axis_align_bounding_box_wrong_2
			-- Wrong test 2 for `all_overlaps_with_axis_align_bounding_box'.
		note
			testing:  "covers/{B2D_WORLD}.all_overlaps_with_axis_align_bounding_box"
		local
			l_retry:BOOLEAN
			l_aabb:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_result:LIST[B2D_FIXTURE]
		do
			if not l_retry then
				create l_aabb.make_with_coordinates (1.5, 1.5, 5, 5)
				l_aabb.put_null
				l_result := world.all_overlaps_with_axis_align_bounding_box (l_aabb)
				assert ("all_overlaps_with_axis_align_bounding_box valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_filter_collision_normal_1
			-- Normal test 1 for `filter_collision'
		note
			testing:  "covers/{B2D_WORLD}.filter_collision"
		do
			world.bodies[1].set_transform (0, 3, 0)
			world.bodies[2].set_transform (0, 100, 0)
			world.bodies[3].set_transform (100, 0, 0)
			world.set_filter_collision (agent filter_collision_1)
			index := 0
			world.step
			assert("set_filter_collision iteration valid.", index ~ 1)
			assert("set_filter_collision position valid.", world.bodies[1].position.y > 3)
		end

	test_filter_collision_normal_2
			-- Normal test 2 for `filter_collision'
		note
			testing:  "covers/{B2D_WORLD}.filter_collision"
		do
			world.bodies[1].set_transform (0, 3, 0)
			world.bodies[2].set_transform (0, 100, 0)
			world.bodies[3].set_transform (100, 0, 0)
			world.set_filter_collision (agent filter_collision_2)
			index := 0
			world.step
			assert("set_filter_collision iteration valid.", index ~ 1)
			assert("set_filter_collision position valid.", world.bodies[1].position.y < 3)
		end

	test_default_filter_collision_normal_1
			-- Normal test 1 for `default_filter_collision'
		note
			testing:  "covers/{B2D_WORLD}.default_filter_collision"
		local
			l_result:BOOLEAN
		do
			world.bodies[1].set_transform (0, 3, 0)
			world.bodies[2].set_transform (0, 100, 0)
			world.bodies[3].set_transform (100, 0, 0)
			l_result := world.default_filter_collision (world.bodies[1].fixtures[1], world.bodies[4].fixtures[1])
			assert("default_filter_collision Valid.", l_result)
		end

	test_default_filter_collision_normal_2
			-- Normal test 2 for `default_filter_collision'
		note
			testing:  "covers/{B2D_WORLD}.default_filter_collision"
		local
			l_result:BOOLEAN
			l_filter_1, l_filter_2:B2D_FILTER
		do
			world.bodies[1].set_transform (0, 3, 0)
			world.bodies[2].set_transform (0, 100, 0)
			world.bodies[3].set_transform (100, 0, 0)
			create l_filter_1
			create l_filter_2
			l_filter_1.group_index := 1
			l_filter_2.group_index := 1
			world.bodies[1].fixtures[1].filter := l_filter_1
			world.bodies[4].fixtures[1].filter := l_filter_2
			l_result := world.default_filter_collision (world.bodies[1].fixtures[1], world.bodies[4].fixtures[1])
			assert("default_filter_collision Valid.", l_result)
		end

	test_default_filter_collision_normal_3
			-- Normal test 3 for `default_filter_collision'
		note
			testing:  "covers/{B2D_WORLD}.default_filter_collision"
		local
			l_result:BOOLEAN
			l_filter_1, l_filter_2:B2D_FILTER
		do
			world.bodies[1].set_transform (0, 3, 0)
			world.bodies[2].set_transform (0, 100, 0)
			world.bodies[3].set_transform (100, 0, 0)
			create l_filter_1
			create l_filter_2
			l_filter_1.group_index := -1
			l_filter_2.group_index := -1
			world.bodies[1].fixtures[1].filter := l_filter_1
			world.bodies[4].fixtures[1].filter := l_filter_2
			l_result := world.default_filter_collision (world.bodies[1].fixtures[1], world.bodies[4].fixtures[1])
			assert("default_filter_collision Valid.", not l_result)
		end

	test_default_filter_collision_normal_4
			-- Normal test 4 for `default_filter_collision'
		note
			testing:  "covers/{B2D_WORLD}.default_filter_collision"
		local
			l_result:BOOLEAN
			l_filter_1, l_filter_2:B2D_FILTER
		do
			world.bodies[1].set_transform (0, 3, 0)
			world.bodies[2].set_transform (0, 100, 0)
			world.bodies[3].set_transform (100, 0, 0)
			create l_filter_1
			create l_filter_2
			l_filter_1.group_index := 0
			l_filter_1.category_bits := 0x0002
			l_filter_1.mask_bits := 0x0004
			l_filter_2.group_index := 0
			l_filter_2.category_bits := 0x0004
			l_filter_2.mask_bits := 0x0002
			world.bodies[1].fixtures[1].filter := l_filter_1
			world.bodies[4].fixtures[1].filter := l_filter_2
			l_result := world.default_filter_collision (world.bodies[1].fixtures[1], world.bodies[4].fixtures[1])
			assert("default_filter_collision Valid.", l_result)
		end

	test_default_filter_collision_normal_5
			-- Normal test 5 for `default_filter_collision'
		note
			testing:  "covers/{B2D_WORLD}.default_filter_collision"
		local
			l_result:BOOLEAN
			l_filter_1, l_filter_2:B2D_FILTER
		do
			world.bodies[1].set_transform (0, 3, 0)
			world.bodies[2].set_transform (0, 100, 0)
			world.bodies[3].set_transform (100, 0, 0)
			create l_filter_1
			create l_filter_2
			l_filter_1.group_index := 0
			l_filter_1.category_bits := 0x0004
			l_filter_1.mask_bits := 0x0002
			l_filter_2.group_index := 0
			l_filter_2.category_bits := 0x0002
			l_filter_2.mask_bits := 0x0002
			world.bodies[1].fixtures[1].filter := l_filter_1
			world.bodies[4].fixtures[1].filter := l_filter_2
			l_result := world.default_filter_collision (world.bodies[1].fixtures[1], world.bodies[4].fixtures[1])
			assert("default_filter_collision Valid.", not l_result)
		end

	test_default_filter_collision_normal_6
			-- Normal test 6 for `default_filter_collision'
		note
			testing:  "covers/{B2D_WORLD}.default_filter_collision"
		local
			l_result:BOOLEAN
			l_filter_1, l_filter_2:B2D_FILTER
		do
			world.bodies[1].set_transform (0, 3, 0)
			world.bodies[2].set_transform (0, 100, 0)
			world.bodies[3].set_transform (100, 0, 0)
			create l_filter_1
			create l_filter_2
			l_filter_1.group_index := 0
			l_filter_1.category_bits := 0x0002
			l_filter_1.mask_bits := 0x0004
			l_filter_2.group_index := 0
			l_filter_2.category_bits := 0x0002
			l_filter_2.mask_bits := 0x0002
			world.bodies[1].fixtures[1].filter := l_filter_1
			world.bodies[4].fixtures[1].filter := l_filter_2
			l_result := world.default_filter_collision (world.bodies[1].fixtures[1], world.bodies[4].fixtures[1])
			assert("default_filter_collision Valid.", not l_result)
		end

	test_default_filter_collision_normal_7
			-- Normal test 7 for `default_filter_collision'
		note
			testing:  "covers/{B2D_WORLD}.default_filter_collision"
		local
			l_result:BOOLEAN
			l_filter_1, l_filter_2:B2D_FILTER
		do
			world.bodies[1].set_transform (0, 3, 0)
			world.bodies[2].set_transform (0, 100, 0)
			world.bodies[3].set_transform (100, 0, 0)
			create l_filter_1
			create l_filter_2
			l_filter_1.group_index := 0
			l_filter_1.category_bits := 0x0002
			l_filter_1.mask_bits := 0x0002
			l_filter_2.group_index := 0
			l_filter_2.category_bits := 0x0004
			l_filter_2.mask_bits := 0x0002
			world.bodies[1].fixtures[1].filter := l_filter_1
			world.bodies[4].fixtures[1].filter := l_filter_2
			l_result := world.default_filter_collision (world.bodies[1].fixtures[1], world.bodies[4].fixtures[1])
			assert("default_filter_collision Valid.", not l_result)
		end

	test_default_filter_collision_normal_8
			-- Normal test 8 for `default_filter_collision'
		note
			testing:  "covers/{B2D_WORLD}.default_filter_collision"
		local
			l_result:BOOLEAN
			l_filter_1, l_filter_2:B2D_FILTER
		do
			world.bodies[1].set_transform (0, 3, 0)
			world.bodies[2].set_transform (0, 100, 0)
			world.bodies[3].set_transform (100, 0, 0)
			create l_filter_1
			create l_filter_2
			l_filter_1.group_index := 0
			l_filter_1.category_bits := 0x0002
			l_filter_1.mask_bits := 0x0002
			l_filter_2.group_index := 0
			l_filter_2.category_bits := 0x0002
			l_filter_2.mask_bits := 0x0004
			world.bodies[1].fixtures[1].filter := l_filter_1
			world.bodies[4].fixtures[1].filter := l_filter_2
			l_result := world.default_filter_collision (world.bodies[1].fixtures[1], world.bodies[4].fixtures[1])
			assert("default_filter_collision Valid.", not l_result)
		end

	test_default_filter_collision_wrong_1
			-- Wrong test 1 for `default_filter_collision'.
		note
			testing:  "covers/{B2D_WORLD}.default_filter_collision"
		local
			l_retry, l_result:BOOLEAN
		do
			if not l_retry then
				world.bodies[1].set_transform (0, 3, 0)
				world.bodies[2].set_transform (0, 100, 0)
				world.bodies[3].set_transform (100, 0, 0)
				world.bodies[1].fixtures[1].put_null
				l_result := world.default_filter_collision (world.bodies[1].fixtures[1], world.bodies[4].fixtures[1])
				assert ("default_filter_collision valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_default_filter_collision_wrong_2
			-- Wrong test 2 for `default_filter_collision'.
		note
			testing:  "covers/{B2D_WORLD}.default_filter_collision"
		local
			l_retry, l_result:BOOLEAN
		do
			if not l_retry then
				world.bodies[1].set_transform (0, 3, 0)
				world.bodies[2].set_transform (0, 100, 0)
				world.bodies[3].set_transform (100, 0, 0)
				world.bodies[4].fixtures[1].put_null
				l_result := world.default_filter_collision (world.bodies[1].fixtures[1], world.bodies[4].fixtures[1])
				assert ("default_filter_collision valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_begin_contact_actions_normal
			-- Normal test for `begin_contact_actions'
		note
			testing:  "covers/{B2D_WORLD}.begin_contact_actions"
		do
			world.bodies[1].set_transform (0, 3, 0)
			world.bodies[2].set_transform (0, 100, 0)
			world.bodies[3].set_transform (100, 0, 0)
			index := 0
			world.begin_contact_actions.extend (agent begin_contact_action_1)
			world.step
			world.step
			assert("begin_contact_actions Valid.", index ~ 1)
		end

	test_end_contact_actions_normal
			-- Normal test for `end_contact_actions'
		note
			testing:  "covers/{B2D_WORLD}.end_contact_actions"
		local
			l_index:INTEGER
		do
			world.bodies[1].set_transform (0, 5, 0)
			world.bodies[2].set_transform (0, 17, 0)
			world.bodies[3].set_transform (100, 0, 0)
			index := 0
			world.end_contact_actions.extend (agent end_contact_action)
			across 1 |..| 85 as la_index loop
				world.step
				l_index:= la_index.item
			end
			assert("end_contact_actions Valid.", index ~ 1)
		end

	test_pre_solve_contact_actions_normal_1
			-- Normal test 1 for `pre_solve_contact_actions'
		note
			testing:  "covers/{B2D_WORLD}.pre_solve_contact_actions"
		do
			world.bodies[1].set_transform (0, 4, 0)
			world.bodies[2].set_transform (0, 100, 0)
			world.bodies[3].set_transform (100, 0, 0)
			index := 0
			world.pre_solve_contact_actions.extend (agent pre_solve_contact_action_1)
			world.step
			assert("pre_solve_contact_actions body Valid.", world.bodies[1].position.y > 4)
			assert("pre_solve_contact_actions Valid.", index ~ 1)
		end

	test_pre_solve_contact_actions_normal_2
			-- Normal test 2 for `pre_solve_contact_actions'
		note
			testing:  "covers/{B2D_WORLD}.pre_solve_contact_actions"
		do
			world.bodies[1].set_transform (0, 4, 0)
			world.bodies[2].set_transform (0, 100, 0)
			world.bodies[3].set_transform (100, 0, 0)
			index := 0
			world.pre_solve_contact_actions.extend (agent pre_solve_contact_action_2)
			world.step
			assert("pre_solve_contact_actions body Valid.", world.bodies[1].position.y < 4)
			assert("pre_solve_contact_actions Valid.", index ~ 1)
		end

	test_post_solve_contact_action_normal
			-- Normal test 1 for `post_solve_contact_action'
		note
			testing:  "covers/{B2D_WORLD}.post_solve_contact_action"
		do
			world.bodies[1].set_transform (0, 4, 0)
			world.bodies[2].set_transform (0, 100, 0)
			world.bodies[3].set_transform (100, 0, 0)
			index := 0
			world.post_solve_contact_actions.extend (agent post_solve_contact_action)
			world.step
			assert("post_solve_contact_actions Valid.", index ~ 1)
		end



feature {NONE} -- Implementation

	ray_cast_action_1(a_output:B2D_WORLD_RAY_CAST_OUTPUT):REAL_32
			-- Action of `test_ray_cast_query_normal_1' and `test_ray_cast_query_with_vectors_normal_1'
		do
			index := index + 1
			if index ~ 1 then
				assert("Callback 1 body Valid.", a_output.fixture.body ~ world.bodies[3])
				assert("Callback 1 fraction Valid.", compare_real_32 (a_output.fraction, 0.116667))
				assert("Callback 1 normal X Valid.", a_output.normal.x ~ 1)
				assert("Callback 1 normal Y Valid.", a_output.normal.y ~ 0)
				assert("Callback 1 point X Valid.", a_output.point.x ~ 23)
				assert("Callback 1 point Y Valid.", a_output.point.y ~ 23)
				Result := 1.0
			elseif index ~ 2 then
				assert("Callback 2 body Valid.", a_output.fixture.body ~ world.bodies[2])
				assert("Callback 1 fraction Valid.", compare_real_32 (a_output.fraction, 0.3))
				assert("Callback 1 normal X Valid.", a_output.normal.x ~ 1)
				assert("Callback 1 normal Y Valid.", a_output.normal.y ~ 0)
				assert("Callback 1 point X Valid.", a_output.point.x ~ 12)
				assert("Callback 1 point Y Valid.", a_output.point.y ~ 12)
				Result := 0.0
			else
				assert("Callback index not Valid.", False)
			end
		end

	ray_cast_action_2(a_output:B2D_WORLD_RAY_CAST_OUTPUT):REAL_32
			-- Action of `test_ray_cast_query_normal_2' and `test_ray_cast_query_with_vectors_normal_2'
		do
			index := index + 1
			if index ~ 1 then
				assert("Callback 1 body Valid.", a_output.fixture.body ~ world.bodies[3])
				assert("Callback 1 fraction Valid.", compare_real_32 (a_output.fraction, 0.116667))
				assert("Callback 1 normal X Valid.", a_output.normal.x ~ 1)
				assert("Callback 1 normal Y Valid.", a_output.normal.y ~ 0)
				assert("Callback 1 point X Valid.", a_output.point.x ~ 23)
				assert("Callback 1 point Y Valid.", a_output.point.y ~ 23)
				Result := 0.4
			elseif index ~ 2 then
				assert("Callback 2 body Valid.", a_output.fixture.body ~ world.bodies[2])
				assert("Callback 1 fraction Valid.", compare_real_32 (a_output.fraction, 0.3))
				assert("Callback 1 normal X Valid.", a_output.normal.x ~ 1)
				assert("Callback 1 normal Y Valid.", a_output.normal.y ~ 0)
				assert("Callback 1 point X Valid.", a_output.point.x ~ 12)
				assert("Callback 1 point Y Valid.", a_output.point.y ~ 12)
				Result := 0.4
			else
				assert("Callback index not Valid.", False)
			end
		end

	ray_cast_action_3(a_output:B2D_WORLD_RAY_CAST_OUTPUT):REAL_32
			-- Action of `test_ray_cast_query_normal_3' and `test_ray_cast_query_with_vectors_normal_3'
		do
			index := index + 1
			if index ~ 1 then
				assert("Callback 1 body Valid.", a_output.fixture.body ~ world.bodies[3])
				assert("Callback 1 fraction Valid.", compare_real_32 (a_output.fraction, 0.116667))
				assert("Callback 1 normal X Valid.", a_output.normal.x ~ 1)
				assert("Callback 1 normal Y Valid.", a_output.normal.y ~ 0)
				assert("Callback 1 point X Valid.", a_output.point.x ~ 23)
				assert("Callback 1 point Y Valid.", a_output.point.y ~ 23)
				Result := -1.0
			elseif index ~ 2 then
				assert("Callback 2 body Valid.", a_output.fixture.body ~ world.bodies[2])
				assert("Callback 1 fraction Valid.", compare_real_32 (a_output.fraction, 0.3))
				assert("Callback 1 normal X Valid.", a_output.normal.x ~ 1)
				assert("Callback 1 normal Y Valid.", a_output.normal.y ~ 0)
				assert("Callback 1 point X Valid.", a_output.point.x ~ 12)
				assert("Callback 1 point Y Valid.", a_output.point.y ~ 12)
				Result := 0.0
			else
				assert("Callback index not Valid.", False)
			end
		end

	overlap_query_action(a_fixture:B2D_FIXTURE):BOOLEAN
			-- Action of `test_overlap_query_normal' and `test_overlap_query_with_axis_align_bounding_box_normal'
		do
			index := index + 1
			if index ~ 1 then
				assert("Callback 1 body Valid.", a_fixture.body ~ world.bodies[4])
				Result := True
			elseif index ~ 2 then
				assert("Callback 2 body Valid.", a_fixture.body ~ world.bodies[3])
				Result := False
			else
				assert("Callback index not Valid.", False)
			end
		end

	filter_collision_1(a_fixture_1, a_fixture_2:B2D_FIXTURE):BOOLEAN
			-- Action of `test_filter_collision_normal_1'
		do
			index := 1
			assert("Fixture 1 Valid.", a_fixture_1.body ~ world.bodies[1])
			assert("Fixture 2 Valid.", a_fixture_2.body ~ world.bodies[4])
			Result := True
		end

	filter_collision_2(a_fixture_1, a_fixture_2:B2D_FIXTURE):BOOLEAN
			-- Action of `test_filter_collision_normal_2'
		do
			index := 1
			assert("Fixture 1 Valid.", a_fixture_1.body ~ world.bodies[1])
			assert("Fixture 2 Valid.", a_fixture_2.body ~ world.bodies[4])
			Result := False
		end

	begin_contact_action_1(a_contact:B2D_CONTACT)
			-- Action of `test_begin_contact_actions_normal'
		do
			index := index + 1
			assert("Callback 1 body Valid.", a_contact.fixture_1.body ~ world.bodies[1])
			assert("Callback 2 body Valid.", a_contact.fixture_2.body ~ world.bodies[4])
			assert("Callback friction Valid.", a_contact.friction ~ 0.2)
			assert("Callback restitution Valid.", a_contact.restitution ~ 0)
			assert("Callback tangent_speed Valid.", a_contact.tangent_speed ~ 0)
			assert("Callback world_manifold.point_count Valid.", a_contact.world_manifold.point_count ~ 2)
			assert("Callback a_contact.world_manifold.points[1].x Valid.", a_contact.world_manifold.points.at (1).x ~ -1.02)
			assert("Callback a_contact.world_manifold.points[1].y Valid.", a_contact.world_manifold.points.at (1).y ~ 3)
			assert("Callback a_contact.world_manifold.points[2].x Valid.", a_contact.world_manifold.points.at (2).x ~ 1.02)
			assert("Callback a_contact.world_manifold.points[2].y Valid.", a_contact.world_manifold.points.at (2).y ~ 3)
			assert("Callback a_contact.world_manifold.normal.x Valid.", a_contact.world_manifold.normal.x ~ 0)
			assert("Callback a_contact.world_manifold.normal.y Valid.", a_contact.world_manifold.normal.y ~ -1)
			assert("Callback a_contact.world_manifold.separations[1] Valid.", compare_real_32 (a_contact.world_manifold.separations.at (1), -2.02))
			assert("Callback a_contact.world_manifold.separations[2] Valid.", compare_real_32 (a_contact.world_manifold.separations.at (2), -2.02))
		end

	end_contact_action(a_contact:B2D_CONTACT)
			-- Action of `test_end_contact_actions_normal'
		do
			index := index + 1
			assert("Callback 1 body Valid.", a_contact.fixture_1.body ~ world.bodies[1])
			assert("Callback 2 body Valid.", a_contact.fixture_2.body ~ world.bodies[2])
			assert("Callback friction Valid.", a_contact.friction ~ 0.2)
			assert("Callback restitution Valid.", a_contact.restitution ~ 0)
			assert("Callback tangent_speed Valid.", a_contact.tangent_speed ~ 0)
		end

	pre_solve_contact_action_1(a_contact:B2D_CONTACT; a_old_manifold:B2D_MANIFOLD)
			-- Action of `test_pre_solve_contact_actions_normal_1'
		local
			l_point_states: LIST [TUPLE [old_state: B2D_POINT_STATE; current_state: B2D_POINT_STATE]]
		do
			index := index + 1
			assert("Callback 1 body Valid.", a_contact.fixture_1.body ~ world.bodies[1])
			assert("Callback 2 body Valid.", a_contact.fixture_2.body ~ world.bodies[4])
			assert("Callback friction Valid.", a_contact.friction ~ 0.2)
			assert("Callback restitution Valid.", a_contact.restitution ~ 0)
			assert("Callback tangent_speed Valid.", a_contact.tangent_speed ~ 0)
			assert("Callback manifold.is_face_1 Valid.", a_contact.manifold.is_face_1)
			assert("Callback manifold.point_count Valid.", a_contact.manifold.point_count ~ 2)
			assert("Callback a_contact.manifold.local_point.x Valid.", a_contact.manifold.local_point.x ~ 0)
			assert("Callback a_contact.manifold.local_point.y Valid.", a_contact.manifold.local_point.y ~ -1)
			assert("Callback a_contact.manifold.local_normal.x Valid.", a_contact.manifold.local_normal.x ~ 0)
			assert("Callback a_contact.manifold.local_normal.y Valid.", a_contact.manifold.local_normal.y ~ -1)
			assert("Callback a_contact.manifold.points[1].is_face_1 Valid.", a_contact.manifold.points[1].is_face_1)
			assert("Callback a_contact.manifold.points[1].local_point.x Valid.", a_contact.manifold.points[1].local_point.x ~ -1.02)
			assert("Callback a_contact.manifold.points[1].local_point.y Valid.", a_contact.manifold.points[1].local_point.y ~ 4)
			assert("Callback a_contact.manifold.points[1].is_intersect_face_1 Valid.", not a_contact.manifold.points[1].is_intersect_face_1)
			assert("Callback a_contact.manifold.points[1].is_intersect_face_2 Valid.", a_contact.manifold.points[1].is_intersect_face_2)
			assert("Callback a_contact.manifold.points[1].is_intersect_vertex_1 Valid.", a_contact.manifold.points[1].is_intersect_vertex_1)
			assert("Callback a_contact.manifold.points[1].is_intersect_vertex_2 Valid.", not a_contact.manifold.points[1].is_intersect_vertex_2)
			assert("Callback a_contact.manifold.points[1].normal_impulse Valid.", a_contact.manifold.points[1].normal_impulse ~ 0)
			assert("Callback a_contact.manifold.points[1].tangent_impulse Valid.", a_contact.manifold.points[1].tangent_impulse ~ 0)
			assert("Callback a_contact.manifold.points[2].is_face_1 Valid.", a_contact.manifold.points[2].is_face_1)
			assert("Callback a_contact.manifold.points[2].local_point.x Valid.", a_contact.manifold.points[2].local_point.x ~ 1.02)
			assert("Callback a_contact.manifold.points[2].local_point.y Valid.", a_contact.manifold.points[2].local_point.y ~ 4)
			assert("Callback a_contact.manifold.points[2].is_intersect_face_1 Valid.", not a_contact.manifold.points[2].is_intersect_face_1)
			assert("Callback a_contact.manifold.points[2].is_intersect_face_2 Valid.", a_contact.manifold.points[2].is_intersect_face_2)
			assert("Callback a_contact.manifold.points[2].is_intersect_vertex_1 Valid.", a_contact.manifold.points[2].is_intersect_vertex_1)
			assert("Callback a_contact.manifold.points[2].is_intersect_vertex_2 Valid.", not a_contact.manifold.points[2].is_intersect_vertex_2)
			assert("Callback a_contact.manifold.points[2].normal_impulse Valid.", a_contact.manifold.points[2].normal_impulse ~ 0)
			assert("Callback a_old_manifold.point_count Valid.", a_old_manifold.point_count ~ 0)
			assert("Callback a_old_manifold.local_point.x Valid.", a_old_manifold.local_point.x ~ 0)
			assert("Callback a_old_manifold.local_point.y Valid.", a_old_manifold.local_point.y ~ 0)
			l_point_states := a_contact.manifold.point_states (a_old_manifold)
			assert("Callback l_point_states.count Valid.", l_point_states.count ~ 2)
			assert("Callback l_point_states[1].old_state Null Valid.", l_point_states[1].old_state.is_null)
			assert("Callback l_point_states[1].current_state Added Valid.", l_point_states[1].current_state.is_added)
			assert("Callback l_point_states[2].old_state Null Valid.", l_point_states[2].old_state.is_null)
			assert("Callback l_point_states[2].current_state Added Valid.", l_point_states[2].current_state.is_added)

		end

	pre_solve_contact_action_2(a_contact:B2D_CONTACT; a_old_manifold:B2D_MANIFOLD)
			-- Action of `test_pre_solve_contact_actions_normal_1'
		do
			index := index + 1
			assert("Callback 1 body Valid.", a_contact.fixture_1.body ~ world.bodies[1])
			assert("Callback 2 body Valid.", a_contact.fixture_2.body ~ world.bodies[4])
			a_contact.disable
		end

	post_solve_contact_action(a_contact:B2D_CONTACT; a_impulse:B2D_CONTACT_IMPULSE)
			-- Action of `test_pre_solve_contact_actions_normal_1'
		do
			index := index + 1
			assert("Callback 1 body Valid.", a_contact.fixture_1.body ~ world.bodies[1])
			assert("Callback 2 body Valid.", a_contact.fixture_2.body ~ world.bodies[4])
			assert("Callback friction Valid.", a_contact.friction ~ 0.2)
			assert("Callback restitution Valid.", a_contact.restitution ~ 0)
			assert("Callback tangent_speed Valid.", a_contact.tangent_speed ~ 0)
			assert("Callback a_impulse.count Valid.", a_impulse.count ~ 2)
			assert("Callback a_impulse.normal[1] Valid.", compare_real_32 (a_impulse.normal[1], 0.333333))
			assert("Callback a_impulse.normal[2] Valid.", a_impulse.normal[2] ~ 0)
			assert("Callback a_impulse.tangent[1] Valid.", compare_real_32 (a_impulse.tangent[1], 0))
			assert("Callback a_impulse.tangent[2] Valid.", compare_real_32 (a_impulse.tangent[2], 0))
		end


	index:INTEGER
			-- Used to follow callback order

	world:B2D_WORLD
			-- The {B2D_WORLD} used in `Current'
end


