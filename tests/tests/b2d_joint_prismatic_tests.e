note
	description: "Tests for {B2D_JOINT_PRISMATIC}"
	author: "Louis Marchand"
	date: "Thu, 17 Feb 2022 17:32:29 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_JOINT_PRISMATIC_TESTS

inherit
	EQA_TEST_SET
		redefine
			on_prepare
		end
	B2D_COMPARE_REAL
		undefine
			default_create
		end
	B2D_ANY
		undefine
			default_create
		end

feature {NONE} -- Events

	on_prepare
			-- <Precursor>
		local
			l_definition:B2D_JOINT_PRISMATIC_DEFINITION
			l_body_definition:B2D_BODY_DEFINITION
		do
			create world.make (0, -10)
			world.create_body (create {B2D_BODY_DEFINITION}.make_dynamic)
			check attached world.last_body as la_body_1 then
				body_1 := la_body_1
				la_body_1.create_fixture_from_shape (create {B2D_SHAPE_POLYGON}.make_box (5, 5), 5)
				create l_body_definition.make_static
				l_body_definition.set_position (0, 10)
				l_body_definition.set_angle ({SINGLE_MATH}.pi.truncated_to_real)
				world.create_body (l_body_definition)
				check attached world.last_body as la_body_2 then
					body_2 := la_body_2
					la_body_2.create_fixture_from_shape (create {B2D_SHAPE_POLYGON}.make_box (5, 5), 0)
					create l_definition.make (la_body_1, la_body_2, 2, 3, 4, 5)
					world.create_joint (l_definition)
					check attached {B2D_JOINT_PRISMATIC} world.last_joint as la_joint then
						joint := la_joint
					end
				end
			end
		end

feature -- Normal test routines

	test_anchor_normal
			-- Test routine for `anchor_*'
		note
			testing:
					"covers/{B2D_JOINT_PRISMATIC}.anchor_1",
					"covers/{B2D_JOINT_PRISMATIC}.anchor_2"
		do
			assert ("anchor_1 X", joint.anchor_1.x ~ 2)
			assert ("anchor_1 Y", joint.anchor_1.y ~ 3)
			assert ("anchor_2 X", joint.anchor_2.x ~ 2)
			assert ("anchor_2 Y", joint.anchor_2.y ~ 3)
		end

	test_is_active_normal
			-- Test routine for `is_active'
		note
			testing:
					"covers/{B2D_JOINT_PRISMATIC}.is_active",
					"covers/{B2D_BODY}.activate",
					"covers/{B2D_BODY}.deactivate"
		do
			assert ("Default is_active valid", joint.is_active)
			body_1.deactivate
			assert ("Body 1 Desactivate is_active valid", not joint.is_active)
			body_1.activate
			assert ("Default is_active valid", joint.is_active)
			body_2.deactivate
			assert ("Body 1 Desactivate is_active valid", not joint.is_active)
			body_1.deactivate
			body_2.deactivate
			assert ("Body 1 Desactivate is_active valid", not joint.is_active)
		end

	test_reaction_force_normal
			-- Test routine for `reaction_force'
		note
			testing:
					"covers/{B2D_JOINT_PRISMATIC}.reaction_force",
					"covers/{B2D_WORLD}.step"
		do
			assert ("reaction_force default valid X", joint.reaction_force ((1/60).truncated_to_real).x ~ 0)
			assert ("reaction_force default valid Y", joint.reaction_force ((1/60).truncated_to_real).y ~ 0)
			world.step
			assert ("reaction_force valid X", joint.reaction_force ((1/60).truncated_to_real).x > 0)
			assert ("reaction_force valid Y", joint.reaction_force ((1/60).truncated_to_real).y < 0)
		end

	test_reaction_torque_normal
			-- Test routine for `reaction_torque'
		note
			testing:
					"covers/{B2D_JOINT_PRISMATIC}.reaction_torque",
					"covers/{B2D_WORLD}.step"
		do
			assert ("reaction_torque valid", joint.reaction_torque ((1/60).truncated_to_real) ~ 0)
			world.step
			assert ("reaction_torque valid", joint.reaction_torque ((1/60).truncated_to_real) > 0)
		end

	test_is_collide_connected_normal
			-- Test routine for `is_collide_connected'
		note
			testing:
					"covers/{B2D_JOINT_PRISMATIC}.is_collide_connected"
		do
			assert ("is_collide_connected valid", not joint.is_collide_connected)
		end

	test_body_normal
			-- Test routine for `body_*'
		note
			testing:
					"covers/{B2D_JOINT_PRISMATIC}.body_1",
					"covers/{B2D_JOINT_PRISMATIC}.body_2"
		do
			assert ("body_1 valid", joint.body_1 ~ body_1)
			assert ("body_2 valid", joint.body_2 ~ body_2)
		end

	test_local_anchor_normal
			-- Test routine for `local_anchor_*'
		note
			testing:
					"covers/{B2D_JOINT_PRISMATIC}.local_anchor_1",
					"covers/{B2D_JOINT_PRISMATIC}.local_anchor_2"
		do
			assert ("local_anchor_1 X", joint.local_anchor_1.x ~ 2)
			assert ("local_anchor_1 Y", joint.local_anchor_1.y ~ 3)
			assert ("local_anchor_2 X", compare_real_32 (joint.local_anchor_2.x, -2))
			assert ("local_anchor_2 Y", compare_real_32 (joint.local_anchor_2.y, 7))
		end

	test_local_axis_normal
			-- Test routine for `local_axis_1'
		note
			testing:
					"covers/{B2D_JOINT_PRISMATIC}.local_axis_1"
		do
			assert ("local_axis_1 X", joint.local_anchor_1.x ~ 2)
			assert ("local_axis_1 Y", joint.local_anchor_1.y ~ 3)
		end

	test_reference_angle_normal
			-- Test routine for `reference_angle'
		note
			testing:
					"covers/{B2D_JOINT_PRISMATIC}.reference_angle"
		do
			assert ("reference_angle valid", compare_real_32 (joint.reference_angle, {SINGLE_MATH}.pi.truncated_to_real))
		end

	test_translation_normal
			-- Test routine for `translation'
		note
			testing:
					"covers/{B2D_JOINT_PRISMATIC}.translation"
		do
			assert ("translation valid", joint.translation ~ 0)
			world.step
			assert ("translation valid", joint.translation > 0)
		end

	test_speed_normal
			-- Test routine for `speed'
		note
			testing:
					"covers/{B2D_JOINT_PRISMATIC}.speed"
		do
			assert ("speed X", joint.speed ~ 0)
			world.step
			assert ("speed X", joint.speed > 0)
		end

	test_is_limit_enabled_normal
			-- Test routine for `is_limit_enabled'
		note
			testing:
					"covers/{B2D_JOINT_PRISMATIC}.is_limit_enabled",
					"covers/{B2D_JOINT_PRISMATIC}.set_is_limit_enabled",
					"covers/{B2D_JOINT_PRISMATIC}.enable_limit",
					"covers/{B2D_JOINT_PRISMATIC}.disable_limit"
		do
			assert ("Default is_limit_enabled", not joint.is_limit_enabled)
			joint.set_is_limit_enabled (True)
			assert ("set_is_limit_enabled True", joint.is_limit_enabled)
			joint.set_is_limit_enabled (False)
			assert ("set_is_limit_enabled False", not joint.is_limit_enabled)
			joint.enable_limit
			assert ("enable_limit valid", joint.is_limit_enabled)
			joint.disable_limit
			assert ("disable_limit valid", not joint.is_limit_enabled)
		end

	test_limits_normal
			-- Test routine for `limits'
		note
			testing:
					"covers/{B2D_JOINT_PRISMATIC}.limits",
					"covers/{B2D_JOINT_PRISMATIC}.set_limits",
					"covers/{B2D_JOINT_PRISMATIC}.lower_limit",
					"covers/{B2D_JOINT_PRISMATIC}.set_lower_limit",
					"covers/{B2D_JOINT_PRISMATIC}.upper_limit",
					"covers/{B2D_JOINT_PRISMATIC}.setupper_limit"
		do
			assert ("Default limits", joint.limits.lower_limit ~ 0 and joint.limits.upper_limit ~ 0)
			assert ("Default lower_limit", joint.lower_limit ~ 0)
			assert ("Default upper_limit", joint.upper_limit ~ 0)
			joint.set_limits (-10, 10)
			assert ("set_limits valid", joint.limits.lower_limit ~ -10 and joint.limits.upper_limit ~ 10)
			assert ("set_limits lower_limit valid", joint.lower_limit ~ -10)
			assert ("set_limits upper_limit valid", joint.upper_limit ~ 10)
			joint.set_upper_limit (20)
			assert ("set_limits valid", joint.limits.lower_limit ~ -10 and joint.limits.upper_limit ~ 20)
			assert ("set_limits upper_limit valid", joint.upper_limit ~ 20)
			joint.set_lower_limit (-20)
			assert ("set_limits valid", joint.limits.lower_limit ~ -20 and joint.limits.upper_limit ~ 20)
			assert ("set_limits upper_limit valid", joint.lower_limit ~ -20)
		end

	test_is_motor_enabled_normal
			-- Test routine for `is_motor_enabled'
		note
			testing:
					"covers/{B2D_JOINT_PRISMATIC}.is_motor_enabled",
					"covers/{B2D_JOINT_PRISMATIC}.set_is_motor_enabled",
					"covers/{B2D_JOINT_PRISMATIC}.enable_motor",
					"covers/{B2D_JOINT_PRISMATIC}.disable_motor"
		do
			assert ("Default is_motor_enabled", not joint.is_motor_enabled)
			joint.set_is_motor_enabled (True)
			assert ("set_is_motor_enabled True", joint.is_motor_enabled)
			joint.set_is_motor_enabled (False)
			assert ("set_is_motor_enabled False", not joint.is_motor_enabled)
			joint.enable_motor
			assert ("enable_motor valid", joint.is_motor_enabled)
			joint.disable_motor
			assert ("disable_motor valid", not joint.is_motor_enabled)
		end

	test_motor_speed_normal
			-- Test routine for `motor_speed'
		note
			testing:
					"covers/{B2D_JOINT_PRISMATIC}.motor_speed",
					"covers/{B2D_JOINT_PRISMATIC}.set_motor_speed"
		do
			assert ("Default motor_speed", joint.motor_speed ~ 0)
			joint.set_motor_speed (2)
			assert ("set_motor_speed valid", joint.motor_speed ~ 2)
		end

	test_maximum_motor_force_normal
			-- Test routine for `maximum_motor_force'
		note
			testing:
					"covers/{B2D_JOINT_PRISMATIC}.maximum_motor_force",
					"covers/{B2D_JOINT_PRISMATIC}.set_maximum_motor_force"
		do
			assert ("Default maximum_motor_force", joint.maximum_motor_force ~ 0)
			joint.set_maximum_motor_force (2)
			assert ("set_maximum_motor_force valid", joint.maximum_motor_force ~ 2)
		end

	test_motor_force_normal
			-- Test routine for `motor_force'
		note
			testing:
					"covers/{B2D_JOINT_PRISMATIC}.motor_force",
					"covers/{B2D_JOINT_PRISMATIC}.set_maximum_motor_force",
					"covers/{B2D_JOINT_PRISMATIC}.enable_motor"
		do
			joint.enable_motor
			joint.set_maximum_motor_force (10)
			world.step
			assert ("motor_force valid", joint.motor_force((1/60).truncated_to_real) < 0)
			joint.set_maximum_motor_force (-10)
			world.step
			assert ("motor_force valid", joint.motor_force((1/60).truncated_to_real) > 0)
		end


feature -- Limit test routines

	test_limits_limit
			-- Limit test routine for `limits'
		note
			testing:
					"covers/{B2D_JOINT_PRISMATIC}.limits",
					"covers/{B2D_JOINT_PRISMATIC}.set_limits",
					"covers/{B2D_JOINT_PRISMATIC}.lower_limit",
					"covers/{B2D_JOINT_PRISMATIC}.set_lower_limit",
					"covers/{B2D_JOINT_PRISMATIC}.upper_limit",
					"covers/{B2D_JOINT_PRISMATIC}.set_upper_limit"
		do
			joint.set_limits (0, 0)
			assert ("set_limits limit valid", joint.limits.lower_limit ~ 0 and joint.limits.upper_limit ~ 0)
			joint.set_lower_limit (0)
			assert ("set_lower_limit limit valid", joint.limits.lower_limit ~ 0 and joint.limits.upper_limit ~ 0)
			joint.set_upper_limit (0)
			assert ("set_upper_limit limit valid", joint.limits.lower_limit ~ 0 and joint.limits.upper_limit ~ 0)
		end

	test_reaction_force_limit
			-- Limit test routine for `reaction_force'
		note
			testing:
					"covers/{B2D_JOINT_PRISMATIC}.reaction_force",
					"covers/{B2D_WORLD}.step"
		do
			assert ("reaction_force default valid X", joint.reaction_force (0).x ~ 0)
			assert ("reaction_force default valid Y", joint.reaction_force (0).y ~ 0)
			world.step
			assert ("reaction_force valid X", joint.reaction_force (0).x ~ 0)
			assert ("reaction_force valid Y", joint.reaction_force (0).y ~ 0)
		end

	test_reaction_torque_limit
			-- Limit test routine for `reaction_torque'
		note
			testing:
					"covers/{B2D_JOINT_PRISMATIC}.reaction_torque",
					"covers/{B2D_WORLD}.step"
		do
			assert ("reaction_torque valid", joint.reaction_torque (0) ~ 0)
			world.step
			assert ("reaction_torque valid", joint.reaction_torque (0) ~ 0)
		end

	test_motor_force_limit
			-- Limit test routine for `motor_force'
		note
			testing:
					"covers/{B2D_JOINT_PRISMATIC}.motor_force",
					"covers/{B2D_JOINT_PRISMATIC}.set_maximum_motor_force",
					"covers/{B2D_JOINT_PRISMATIC}.enable_motor"
		do
			joint.enable_motor
			assert ("motor_force valid", joint.motor_force(0) ~ 0)
			joint.set_maximum_motor_force (10)
			world.step
			assert ("motor_force valid", joint.motor_force(0) ~ 0)
			joint.set_maximum_motor_force (-10)
			world.step
			assert ("motor_force valid", joint.motor_force(0) ~ 0)
		end

feature -- Wrong test routines

	test_anchor_1_wrong
			-- Test for {B2D_JOINT_PRISMATIC}.anchor_1 when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC}.anchor_1"
		local
			l_retry:BOOLEAN
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				joint.put_null
				l_result := joint.anchor_1
				assert ("joint.anchor_1 on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_anchor_2_wrong
			-- Test for {B2D_JOINT_PRISMATIC}.anchor_2 when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC}.anchor_2"
		local
			l_retry:BOOLEAN
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				joint.put_null
				l_result := joint.anchor_2
				assert ("joint.anchor_2 on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_reaction_force_wrong
			-- Test for {B2D_JOINT_PRISMATIC}.reaction_force when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC}.reaction_force"
		local
			l_retry:BOOLEAN
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				joint.put_null
				l_result := joint.reaction_force(1)
				assert ("joint.reaction_force on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_reaction_torque_wrong
			-- Test for {B2D_JOINT_PRISMATIC}.reaction_torque when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC}.reaction_torque"
		local
			l_retry:BOOLEAN
			l_result:REAL_32
		do
			if not l_retry then
				joint.put_null
				l_result := joint.reaction_torque(1)
				assert ("joint.reaction_torque on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_is_collide_connected_wrong
			-- Test for {B2D_JOINT_PRISMATIC}.is_collide_connected when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC}.is_collide_connected"
		local
			l_retry:BOOLEAN
			l_result:BOOLEAN
		do
			if not l_retry then
				joint.put_null
				l_result := joint.is_collide_connected
				assert ("joint.is_collide_connected on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_local_anchor_1_wrong
			-- Test for {B2D_JOINT_PRISMATIC}.local_anchor_1 when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC}.local_anchor_1"
		local
			l_retry:BOOLEAN
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				joint.put_null
				l_result := joint.local_anchor_1
				assert ("joint.local_anchor_1 on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_local_anchor_2_wrong
			-- Test for {B2D_JOINT_PRISMATIC}.local_anchor_2 when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC}.local_anchor_2"
		local
			l_retry:BOOLEAN
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				joint.put_null
				l_result := joint.local_anchor_2
				assert ("joint.local_anchor_2 on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_local_axis_1_wrong
			-- Test for {B2D_JOINT_PRISMATIC}.local_axis_1 when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC}.local_axis_1"
		local
			l_retry:BOOLEAN
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				joint.put_null
				l_result := joint.local_axis_1
				assert ("joint.local_axis_1 on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_reference_angle_wrong
			-- Test for {B2D_JOINT_PRISMATIC}.reference_angle when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC}.reference_angle"
		local
			l_retry:BOOLEAN
			l_result:REAL_32
		do
			if not l_retry then
				joint.put_null
				l_result := joint.reference_angle
				assert ("joint.reference_angle on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_translation_wrong
			-- Test for {B2D_JOINT_PRISMATIC}.translation when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC}.translation"
		local
			l_retry:BOOLEAN
			l_result:REAL_32
		do
			if not l_retry then
				joint.put_null
				l_result := joint.translation
				assert ("joint.translation on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_speed_wrong
			-- Test for {B2D_JOINT_PRISMATIC}.speed when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC}.speed"
		local
			l_retry:BOOLEAN
			l_result:REAL_32
		do
			if not l_retry then
				joint.put_null
				l_result := joint.speed
				assert ("joint.speed on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_is_limit_enabled_wrong
			-- Test for {B2D_JOINT_PRISMATIC}.is_limit_enabled when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC}.is_limit_enabled"
		local
			l_retry:BOOLEAN
			l_result:BOOLEAN
		do
			if not l_retry then
				joint.put_null
				l_result := joint.is_limit_enabled
				assert ("joint.is_limit_enabled on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_is_limit_enabled_wrong
			-- Test for {B2D_JOINT_PRISMATIC}.set_is_limit_enabled when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC}.set_is_limit_enabled"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				joint.put_null
				joint.set_is_limit_enabled(False)
				assert ("joint.set_is_limit_enabled on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_enable_limit_wrong
			-- Test for {B2D_JOINT_PRISMATIC}.enable_limit when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC}.enable_limit"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				joint.put_null
				joint.enable_limit
				assert ("joint.enable_limit on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_disable_limit_wrong
			-- Test for {B2D_JOINT_PRISMATIC}.disable_limit when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC}.disable_limit"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				joint.put_null
				joint.disable_limit
				assert ("joint.disable_limit on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_lower_limit_wrong
			-- Test for {B2D_JOINT_PRISMATIC}.lower_limit when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC}.lower_limit"
		local
			l_retry:BOOLEAN
			l_result:REAL_32
		do
			if not l_retry then
				joint.put_null
				l_result := joint.lower_limit
				assert ("joint.lower_limit on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_upper_limit_wrong
			-- Test for {B2D_JOINT_PRISMATIC}.upper_limit when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC}.upper_limit"
		local
			l_retry:BOOLEAN
			l_result:REAL_32
		do
			if not l_retry then
				joint.put_null
				l_result := joint.upper_limit
				assert ("joint.upper_limit on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_limits_wrong
			-- Test for {B2D_JOINT_PRISMATIC}.set_limits when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC}.set_limits"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				joint.put_null
				joint.set_limits(0,1)
				assert ("joint.set_limits on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_limits_wrong
			-- Test for {B2D_JOINT_PRISMATIC}.limits when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC}.limits"
		local
			l_retry:BOOLEAN
			l_result:TUPLE[lower_limit, upper_limit:REAL_32]
		do
			if not l_retry then
				joint.put_null
				l_result := joint.limits
				assert ("joint.limits on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_is_motor_enabled_wrong
			-- Test for {B2D_JOINT_PRISMATIC}.is_motor_enabled when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC}.is_motor_enabled"
		local
			l_retry:BOOLEAN
			l_result:BOOLEAN
		do
			if not l_retry then
				joint.put_null
				l_result := joint.is_motor_enabled
				assert ("joint.is_motor_enabled on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_is_motor_enabled_wrong
			-- Test for {B2D_JOINT_PRISMATIC}.set_is_motor_enabled when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC}.set_is_motor_enabled"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				joint.put_null
				joint.set_is_motor_enabled(False)
				assert ("joint.set_is_motor_enabled on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_enable_motor_wrong
			-- Test for {B2D_JOINT_PRISMATIC}.enable_motor when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC}.enable_motor"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				joint.put_null
				joint.enable_motor
				assert ("joint.enable_motor on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_disable_motor_wrong
			-- Test for {B2D_JOINT_PRISMATIC}.disable_motor when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC}.disable_motor"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				joint.put_null
				joint.disable_motor
				assert ("joint.disable_motor on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end


	test_maximum_motor_force_wrong
			-- Test for {B2D_JOINT_PRISMATIC}.maximum_motor_force when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC}.maximum_motor_force"
		local
			l_retry:BOOLEAN
			l_result:REAL_32
		do
			if not l_retry then
				joint.put_null
				l_result := joint.maximum_motor_force
				assert ("joint.maximum_motor_force on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_maximum_motor_force_wrong
			-- Test for {B2D_JOINT_PRISMATIC}.set_maximum_motor_force when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC}.set_maximum_motor_force"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				joint.put_null
				joint.set_maximum_motor_force(10)
				assert ("joint.set_maximum_motor_force on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_motor_force_wrong
			-- Test for {B2D_JOINT_PRISMATIC}.motor_force when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_PRISMATIC}.motor_force"
		local
			l_retry:BOOLEAN
			l_result:REAL_32
		do
			if not l_retry then
				joint.put_null
				l_result := joint.motor_force(0.1)
				assert ("joint.motor_force on Void not valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end
feature {NONE} -- Implementation

	world:B2D_WORLD
			-- The {B2D_WORLD} containing `joint'

	body_1, body_2:B2D_BODY
			-- The {B2D_BODY} used as anchor of `joint'

	joint:B2D_JOINT_PRISMATIC
			-- The {B2D_JOINT} tested in `Current'

end


