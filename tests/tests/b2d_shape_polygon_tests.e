note
	description: "Tests for {B2D_SHAPE_POLYGON}"
	author: "Louis Marchand"
	date: "Thu, 11 Jun 2020 20:29:27 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_SHAPE_POLYGON_TESTS

inherit
	EQA_TEST_SET
	B2D_COMPARE_REAL
		undefine
			default_create
		end

feature -- Test routines

	test_make_box_normal
			-- Normal test for `make_box'
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_box", "covers/{B2D_SHAPE_POLYGON}.vertices", "covers/{B2D_SHAPE_POLYGON}.count"
		local
			l_shape:B2D_SHAPE_POLYGON
			l_vertices:LIST[B2D_VECTOR_2D]
		do
			create l_shape.make_box (4.35192, 4.58191)
			assert ("make_box not valid: count", l_shape.count ~ 4)
			l_vertices := l_shape.vertices
			assert ("make_box not valid: vertices[1].x", l_vertices[1].x ~ -4.35192)
			assert ("make_box not valid: vertices[1].y", l_vertices[1].y ~ -4.58191)
			assert ("make_box not valid: vertices[2].x", l_vertices[2].x ~ 4.35192)
			assert ("make_box not valid: vertices[2].y", l_vertices[2].y ~ -4.58191)
			assert ("make_box not valid: vertices[3].x", l_vertices[3].x ~ 4.35192)
			assert ("make_box not valid: vertices[3].y", l_vertices[3].y ~ 4.58191)
			assert ("make_box not valid: vertices[4].x", l_vertices[4].x ~ -4.35192)
			assert ("make_box not valid: vertices[4].y", l_vertices[4].y ~ 4.58191)
		end

	test_make_box_limit
			-- Limit test for `make_box'
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_box", "covers/{B2D_SHAPE_POLYGON}.vertices", "covers/{B2D_SHAPE_POLYGON}.count"
		local
			l_shape:B2D_SHAPE_POLYGON
			l_vertices:LIST[B2D_VECTOR_2D]
		do
			create l_shape.make_box (0, 0)
			assert ("make_box not valid: count", l_shape.count ~ 4)
			l_vertices := l_shape.vertices
			assert ("make_box not valid: vertices[1].x", l_vertices[1].x ~ 0)
			assert ("make_box not valid: vertices[1].y", l_vertices[1].y ~ 0)
			assert ("make_box not valid: vertices[2].x", l_vertices[2].x ~ 0)
			assert ("make_box not valid: vertices[2].y", l_vertices[2].y ~ 0)
			assert ("make_box not valid: vertices[3].x", l_vertices[3].x ~ 0)
			assert ("make_box not valid: vertices[3].y", l_vertices[3].y ~ 0)
			assert ("make_box not valid: vertices[4].x", l_vertices[4].x ~ 0)
			assert ("make_box not valid: vertices[4].y", l_vertices[4].y ~ 0)
		end

	test_make_oriented_box_normal
			-- Normal test for `make_oriented_box'
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_oriented_box", "covers/{B2D_SHAPE_POLYGON}.vertices", "covers/{B2D_SHAPE_POLYGON}.count"
		local
			l_shape:B2D_SHAPE_POLYGON
			l_vertices:LIST[B2D_VECTOR_2D]
		do
			create l_shape.make_oriented_box (4.35192, 4.58191, 0.57418, -5.65106, -2.43975)
			assert ("make_oriented_box not valid: count", l_shape.count ~ 4)
			l_vertices := l_shape.vertices
			assert ("make_oriented_box not valid: vertices[1].x", compare_real_32 (l_vertices[1].x, -6.81645))
			assert ("make_oriented_box not valid: vertices[1].y", compare_real_32 (l_vertices[1].y, -8.65063))
			assert ("make_oriented_box not valid: vertices[2].x", compare_real_32 (l_vertices[2].x, 0.491626))
			assert ("make_oriented_box not valid: vertices[2].y", compare_real_32 (l_vertices[2].y, -3.92317))
			assert ("make_oriented_box not valid: vertices[3].x", compare_real_32 (l_vertices[3].x, -4.48567))
			assert ("make_oriented_box not valid: vertices[3].y", compare_real_32 (l_vertices[3].y, 3.77113))
			assert ("make_oriented_box not valid: vertices[4].x", compare_real_32 (l_vertices[4].x, -11.7937))
			assert ("make_oriented_box not valid: vertices[4].y", compare_real_32 (l_vertices[4].y, -0.956335))
		end

	test_make_oriented_box_limit_1
			-- Limite test 1 for `make_oriented_box'
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_oriented_box", "covers/{B2D_SHAPE_POLYGON}.vertices", "covers/{B2D_SHAPE_POLYGON}.count"
		local
			l_shape:B2D_SHAPE_POLYGON
			l_vertices:LIST[B2D_VECTOR_2D]
		do
			create l_shape.make_oriented_box (0, 0, 0.57418, -5.65106, -2.43975)
			assert ("make_oriented_box not valid: count", l_shape.count ~ 4)
			l_vertices := l_shape.vertices
			assert ("make_oriented_box not valid: vertices[1].x", compare_real_32 (l_vertices[1].x, -5.65106))
			assert ("make_oriented_box not valid: vertices[1].y", compare_real_32 (l_vertices[1].y, -2.43975))
			assert ("make_oriented_box not valid: vertices[2].x", compare_real_32 (l_vertices[2].x, -5.65106))
			assert ("make_oriented_box not valid: vertices[2].y", compare_real_32 (l_vertices[2].y, -2.43975))
			assert ("make_oriented_box not valid: vertices[3].x", compare_real_32 (l_vertices[3].x, -5.65106))
			assert ("make_oriented_box not valid: vertices[3].y", compare_real_32 (l_vertices[3].y, -2.43975))
			assert ("make_oriented_box not valid: vertices[4].x", compare_real_32 (l_vertices[4].x, -5.65106))
			assert ("make_oriented_box not valid: vertices[4].y", compare_real_32 (l_vertices[4].y, -2.43975))
		end

	test_make_oriented_box_limit_2
			-- Limit test 2 for `make_oriented_box'
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_oriented_box", "covers/{B2D_SHAPE_POLYGON}.vertices", "covers/{B2D_SHAPE_POLYGON}.count"
		local
			l_shape:B2D_SHAPE_POLYGON
			l_vertices:LIST[B2D_VECTOR_2D]
		do
			create l_shape.make_oriented_box (4.35192, 4.58191, 0, -5.65106, -2.43975)
			assert ("make_oriented_box not valid: count", l_shape.count ~ 4)
			l_vertices := l_shape.vertices
			assert ("make_oriented_box not valid: vertices[1].x", compare_real_32 (l_vertices[1].x, -10.00298))
			assert ("make_oriented_box not valid: vertices[1].y", compare_real_32 (l_vertices[1].y, -7.02166))
			assert ("make_oriented_box not valid: vertices[2].x", compare_real_32 (l_vertices[2].x, -1.29914))
			assert ("make_oriented_box not valid: vertices[2].y", compare_real_32 (l_vertices[2].y, -7.02166))
			assert ("make_oriented_box not valid: vertices[3].x", compare_real_32 (l_vertices[3].x, -1.29914))
			assert ("make_oriented_box not valid: vertices[3].y", compare_real_32 (l_vertices[3].y, 2.14216))
			assert ("make_oriented_box not valid: vertices[4].x", compare_real_32 (l_vertices[4].x, -10.00298))
			assert ("make_oriented_box not valid: vertices[4].y", compare_real_32 (l_vertices[4].y, 2.14216))
		end

	test_make_oriented_box_limit_3
			-- Limit test 3 for `make_oriented_box'
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_oriented_box", "covers/{B2D_SHAPE_POLYGON}.vertices", "covers/{B2D_SHAPE_POLYGON}.count"
		local
			l_shape:B2D_SHAPE_POLYGON
			l_vertices:LIST[B2D_VECTOR_2D]
		do
			create l_shape.make_oriented_box (4.35192, 4.58191, 0.57418, 0, 0)
			assert ("make_oriented_box not valid: count", l_shape.count ~ 4)
			l_vertices := l_shape.vertices
			assert ("make_oriented_box not valid: vertices[1].x", compare_real_32 (l_vertices[1].x, -1.16539))
			assert ("make_oriented_box not valid: vertices[1].y", compare_real_32 (l_vertices[1].y, -6.21088))
			assert ("make_oriented_box not valid: vertices[2].x", compare_real_32 (l_vertices[2].x, 6.14269))
			assert ("make_oriented_box not valid: vertices[2].y", compare_real_32 (l_vertices[2].y, -1.48342))
			assert ("make_oriented_box not valid: vertices[3].x", compare_real_32 (l_vertices[3].x, 1.16539))
			assert ("make_oriented_box not valid: vertices[3].y", compare_real_32 (l_vertices[3].y, 6.21088))
			assert ("make_oriented_box not valid: vertices[4].x", compare_real_32 (l_vertices[4].x, -6.14269))
			assert ("make_oriented_box not valid: vertices[4].y", compare_real_32 (l_vertices[4].y, 1.48342))
		end

	test_make_oriented_box_with_vector_normal
			-- Normal test for `make_oriented_box_with_vector'
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_oriented_box_with_vector", "covers/{B2D_SHAPE_POLYGON}.vertices", "covers/{B2D_SHAPE_POLYGON}.count"
		local
			l_shape:B2D_SHAPE_POLYGON
			l_vertices:LIST[B2D_VECTOR_2D]
			l_vector:B2D_VECTOR_2D
		do
			create l_vector.make_with_coordinates (-5.65106, -2.43975)
			create l_shape.make_oriented_box_with_vector (4.35192, 4.58191, 0.57418, l_vector)
			assert ("make_oriented_box_with_vector not valid: count", l_shape.count ~ 4)
			l_vertices := l_shape.vertices
			assert ("make_oriented_box_with_vector not valid: vertices[1].x", compare_real_32 (l_vertices[1].x, -6.81645))
			assert ("make_oriented_box_with_vector not valid: vertices[1].y", compare_real_32 (l_vertices[1].y, -8.65063))
			assert ("make_oriented_box_with_vector not valid: vertices[2].x", compare_real_32 (l_vertices[2].x, 0.491626))
			assert ("make_oriented_box_with_vector not valid: vertices[2].y", compare_real_32 (l_vertices[2].y, -3.92317))
			assert ("make_oriented_box_with_vector not valid: vertices[3].x", compare_real_32 (l_vertices[3].x, -4.48567))
			assert ("make_oriented_box_with_vector not valid: vertices[3].y", compare_real_32 (l_vertices[3].y, 3.77113))
			assert ("make_oriented_box_with_vector not valid: vertices[4].x", compare_real_32 (l_vertices[4].x, -11.7937))
			assert ("make_oriented_box_with_vector not valid: vertices[4].y", compare_real_32 (l_vertices[4].y, -0.956335))
		end

	test_make_oriented_box_with_vector_limit_1
			-- Limite test 1 for `make_oriented_box_with_vector'
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_oriented_box_with_vector", "covers/{B2D_SHAPE_POLYGON}.vertices", "covers/{B2D_SHAPE_POLYGON}.count"
		local
			l_shape:B2D_SHAPE_POLYGON
			l_vertices:LIST[B2D_VECTOR_2D]
			l_vector:B2D_VECTOR_2D
		do
			create l_vector.make_with_coordinates (-5.65106, -2.43975)
			create l_shape.make_oriented_box_with_vector (0, 0, 0.57418, l_vector)
			assert ("make_oriented_box_with_vector not valid: count", l_shape.count ~ 4)
			l_vertices := l_shape.vertices
			assert ("make_oriented_box_with_vector not valid: vertices[1].x", compare_real_32 (l_vertices[1].x, -5.65106))
			assert ("make_oriented_box_with_vector not valid: vertices[1].y", compare_real_32 (l_vertices[1].y, -2.43975))
			assert ("make_oriented_box_with_vector not valid: vertices[2].x", compare_real_32 (l_vertices[2].x, -5.65106))
			assert ("make_oriented_box_with_vector not valid: vertices[2].y", compare_real_32 (l_vertices[2].y, -2.43975))
			assert ("make_oriented_box_with_vector not valid: vertices[3].x", compare_real_32 (l_vertices[3].x, -5.65106))
			assert ("make_oriented_box_with_vector not valid: vertices[3].y", compare_real_32 (l_vertices[3].y, -2.43975))
			assert ("make_oriented_box_with_vector not valid: vertices[4].x", compare_real_32 (l_vertices[4].x, -5.65106))
			assert ("make_oriented_box_with_vector not valid: vertices[4].y", compare_real_32 (l_vertices[4].y, -2.43975))
		end

	test_make_oriented_box_with_vector_limit_2
			-- Limit test 2 for `make_oriented_box_with_vector'
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_oriented_box_with_vector", "covers/{B2D_SHAPE_POLYGON}.vertices", "covers/{B2D_SHAPE_POLYGON}.count"
		local
			l_shape:B2D_SHAPE_POLYGON
			l_vertices:LIST[B2D_VECTOR_2D]
			l_vector:B2D_VECTOR_2D
		do
			create l_vector.make_with_coordinates (-5.65106, -2.43975)
			create l_shape.make_oriented_box_with_vector (4.35192, 4.58191, 0, l_vector)
			assert ("make_oriented_box_with_vector not valid: count", l_shape.count ~ 4)
			l_vertices := l_shape.vertices
			assert ("make_oriented_box_with_vector not valid: vertices[1].x", compare_real_32 (l_vertices[1].x, -10.00298))
			assert ("make_oriented_box_with_vector not valid: vertices[1].y", compare_real_32 (l_vertices[1].y, -7.02166))
			assert ("make_oriented_box_with_vector not valid: vertices[2].x", compare_real_32 (l_vertices[2].x, -1.29914))
			assert ("make_oriented_box_with_vector not valid: vertices[2].y", compare_real_32 (l_vertices[2].y, -7.02166))
			assert ("make_oriented_box_with_vector not valid: vertices[3].x", compare_real_32 (l_vertices[3].x, -1.29914))
			assert ("make_oriented_box_with_vector not valid: vertices[3].y", compare_real_32 (l_vertices[3].y, 2.14216))
			assert ("make_oriented_box_with_vector not valid: vertices[4].x", compare_real_32 (l_vertices[4].x, -10.00298))
			assert ("make_oriented_box_with_vector not valid: vertices[4].y", compare_real_32 (l_vertices[4].y, 2.14216))
		end

	test_make_oriented_box_with_vector_limit_3
			-- Limit test 3 for `make_oriented_box_with_vector'
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_oriented_box_with_vector", "covers/{B2D_SHAPE_POLYGON}.vertices", "covers/{B2D_SHAPE_POLYGON}.count"
		local
			l_shape:B2D_SHAPE_POLYGON
			l_vertices:LIST[B2D_VECTOR_2D]
			l_vector:B2D_VECTOR_2D
		do
			create l_vector.make_with_coordinates (0, 0)
			create l_shape.make_oriented_box_with_vector (4.35192, 4.58191, 0.57418, l_vector)
			assert ("make_oriented_box_with_vector not valid: count", l_shape.count ~ 4)
			l_vertices := l_shape.vertices
			assert ("make_oriented_box_with_vector not valid: vertices[1].x", compare_real_32 (l_vertices[1].x, -1.16539))
			assert ("make_oriented_box_with_vector not valid: vertices[1].y", compare_real_32 (l_vertices[1].y, -6.21088))
			assert ("make_oriented_box_with_vector not valid: vertices[2].x", compare_real_32 (l_vertices[2].x, 6.14269))
			assert ("make_oriented_box_with_vector not valid: vertices[2].y", compare_real_32 (l_vertices[2].y, -1.48342))
			assert ("make_oriented_box_with_vector not valid: vertices[3].x", compare_real_32 (l_vertices[3].x, 1.16539))
			assert ("make_oriented_box_with_vector not valid: vertices[3].y", compare_real_32 (l_vertices[3].y, 6.21088))
			assert ("make_oriented_box_with_vector not valid: vertices[4].x", compare_real_32 (l_vertices[4].x, -6.14269))
			assert ("make_oriented_box_with_vector not valid: vertices[4].y", compare_real_32 (l_vertices[4].y, 1.48342))
		end

	test_make_with_vertices_normal
			-- Normal test for `make_with_vertices'
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_with_vertices", "covers/{B2D_SHAPE_POLYGON}.vertices", "covers/{B2D_SHAPE_POLYGON}.count"
		local
			l_shape:B2D_SHAPE_POLYGON
			l_vertices:LIST[B2D_VECTOR_2D]
		do
			create l_shape.make_with_vertices (<<
											[{REAL_32}72.285714286, {REAL_32}5.0],
											[{REAL_32}70.428571429, {REAL_32}18.57142857],
											[{REAL_32}15.428571429, {REAL_32}44.285714286],
											[{REAL_32}17.571428571, {REAL_32}24.0],
											[{REAL_32}41.0, {REAL_32}9.285714286]
											>>)
			assert ("make_with_vertices not valid: count", l_shape.count ~ 5)
			l_vertices := l_shape.vertices
			assert ("make_with_vertices not valid: vertices[1].x", compare_real_32 (l_vertices[1].x, 72.285714286))
			assert ("make_with_vertices not valid: vertices[1].y", compare_real_32 (l_vertices[1].y, 5))
			assert ("make_with_vertices not valid: vertices[2].x", compare_real_32 (l_vertices[2].x, 70.428571429))
			assert ("make_with_vertices not valid: vertices[2].y", compare_real_32 (l_vertices[2].y, 18.57142857))
			assert ("make_with_vertices not valid: vertices[3].x", compare_real_32 (l_vertices[3].x, 15.428571429))
			assert ("make_with_vertices not valid: vertices[3].y", compare_real_32 (l_vertices[3].y, 44.285714286))
			assert ("make_with_vertices not valid: vertices[4].x", compare_real_32 (l_vertices[4].x, 17.571428571))
			assert ("make_with_vertices not valid: vertices[4].y", compare_real_32 (l_vertices[4].y, 24))
			assert ("make_with_vertices not valid: vertices[5].x", compare_real_32 (l_vertices[5].x, 41.0))
			assert ("make_with_vertices not valid: vertices[5].y", compare_real_32 (l_vertices[5].y, 9.285714286))
		end

	test_make_with_vertices_limit_1
			-- Limit test 1 for `make_with_vertices'
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_with_vertices", "covers/{B2D_SHAPE_POLYGON}.exists"
		local
			l_shape:B2D_SHAPE_POLYGON
		do
			create l_shape.make_with_vertices (<<
											[{REAL_32}1.0, {REAL_32}1.0],
											[{REAL_32}2.0, {REAL_32}2.0],
											[{REAL_32}3.0, {REAL_32}3.0]
											>>)

			assert ("make_with_vertices not valid: exists", not l_shape.exists)
		end

	test_make_with_vertices_limit_2
			-- Limit test 2 for `make_with_vertices'
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_with_vertices", "covers/{B2D_SHAPE_POLYGON}.vertices", "covers/{B2D_SHAPE_POLYGON}.count"
		local
			l_shape:B2D_SHAPE_POLYGON
			l_vertices:LIST[B2D_VECTOR_2D]
		do
			create l_shape.make_with_vertices (<<
											[{REAL_32}72.285714286, {REAL_32}5.0],
											[{REAL_32}70.428571429, {REAL_32}18.57142857],
											[{REAL_32}15.428571429, {REAL_32}44.285714286]
											>>)
			assert ("make_with_vertices not valid: count", l_shape.count ~ 3)
			l_vertices := l_shape.vertices
			assert ("make_with_vertices not valid: vertices[1].x", compare_real_32 (l_vertices[1].x, 72.285714286))
			assert ("make_with_vertices not valid: vertices[1].y", compare_real_32 (l_vertices[1].y, 5))
			assert ("make_with_vertices not valid: vertices[2].x", compare_real_32 (l_vertices[2].x, 70.428571429))
			assert ("make_with_vertices not valid: vertices[2].y", compare_real_32 (l_vertices[2].y, 18.57142857))
			assert ("make_with_vertices not valid: vertices[3].x", compare_real_32 (l_vertices[3].x, 15.428571429))
			assert ("make_with_vertices not valid: vertices[3].y", compare_real_32 (l_vertices[3].y, 44.285714286))
		end

	test_make_with_vertices_limit_3
			-- Limit test 3 for `make_with_vertices'
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_with_vertices", "covers/{B2D_SHAPE_POLYGON}.vertices", "covers/{B2D_SHAPE_POLYGON}.count"
		local
			l_shape:B2D_SHAPE_POLYGON
			l_vertices:LIST[B2D_VECTOR_2D]
		do
			create l_shape.make_with_vertices (<<
											[{REAL_32}15.0, {REAL_32}-2.555555556],
											[{REAL_32}14.666666667, {REAL_32}2.666666667],
											[{REAL_32}8.555555556, {REAL_32}7.444444444],
											[{REAL_32}-2.111111111, {REAL_32}7.555555556],
											[{REAL_32}-9.666666667, {REAL_32}2.888888889],
											[{REAL_32}-8.888888889, {REAL_32}-5.555555556],
											[{REAL_32}0.333333333, {REAL_32}-7.0],
											[{REAL_32}8.888888889, {REAL_32}-4.444444444]
											>>)
			assert ("make_with_vertices not valid: count", l_shape.count ~ 8)
			l_vertices := l_shape.vertices
			assert ("make_with_vertices not valid: vertices[1].x", compare_real_32 (l_vertices[1].x, 15.0))
			assert ("make_with_vertices not valid: vertices[1].y", compare_real_32 (l_vertices[1].y, -2.555555556))
			assert ("make_with_vertices not valid: vertices[2].x", compare_real_32 (l_vertices[2].x, 14.666666667))
			assert ("make_with_vertices not valid: vertices[2].y", compare_real_32 (l_vertices[2].y, 2.666666667))
			assert ("make_with_vertices not valid: vertices[3].x", compare_real_32 (l_vertices[3].x, 8.555555556))
			assert ("make_with_vertices not valid: vertices[3].y", compare_real_32 (l_vertices[3].y, 7.444444444))
			assert ("make_with_vertices not valid: vertices[4].x", compare_real_32 (l_vertices[4].x, -2.111111111))
			assert ("make_with_vertices not valid: vertices[4].y", compare_real_32 (l_vertices[4].y, 7.555555556))
			assert ("make_with_vertices not valid: vertices[5].x", compare_real_32 (l_vertices[5].x, -9.666666667))
			assert ("make_with_vertices not valid: vertices[5].y", compare_real_32 (l_vertices[5].y, 2.888888889))
			assert ("make_with_vertices not valid: vertices[6].x", compare_real_32 (l_vertices[6].x, -8.888888889))
			assert ("make_with_vertices not valid: vertices[6].y", compare_real_32 (l_vertices[6].y, -5.555555556))
			assert ("make_with_vertices not valid: vertices[7].x", compare_real_32 (l_vertices[7].x, 0.333333333))
			assert ("make_with_vertices not valid: vertices[7].y", compare_real_32 (l_vertices[7].y, -7.0))
			assert ("make_with_vertices not valid: vertices[8].x", compare_real_32 (l_vertices[8].x, 8.888888889))
			assert ("make_with_vertices not valid: vertices[8].y", compare_real_32 (l_vertices[8].y, -4.444444444))
		end

	test_make_with_vertices_wrong_1
			-- Wront test 1 for `make_with_vertices'
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_with_vertices", "covers/{B2D_SHAPE_POLYGON}.vertices", "covers/{B2D_SHAPE_POLYGON}.count"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_POLYGON
		do
			if not l_retry then
				create l_shape.make_with_vertices (<<
											[{REAL_32}72.285714286, {REAL_32}5.0],
											[{REAL_32}70.428571429, {REAL_32}18.57142857]
											>>)
				assert("make_with_vertices Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_make_with_vertices_wrong_2
			-- Wrong test 2 for `make_with_vertices'
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_with_vertices", "covers/{B2D_SHAPE_POLYGON}.vertices", "covers/{B2D_SHAPE_POLYGON}.count"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_POLYGON
		do
			if not l_retry then
				create l_shape.make_with_vertices (<<
											[{REAL_32}15.0, {REAL_32}-2.555555556],
											[{REAL_32}14.666666667, {REAL_32}2.666666667],
											[{REAL_32}8.555555556, {REAL_32}7.444444444],
											[{REAL_32}-2.111111111, {REAL_32}7.555555556],
											[{REAL_32}-9.666666667, {REAL_32}2.888888889],
											[{REAL_32}-8.888888889, {REAL_32}-5.555555556],
											[{REAL_32}0.333333333, {REAL_32}-7.0],
											[{REAL_32}8.888888889, {REAL_32}-4.444444444],
											[{REAL_32}9.0, {REAL_32}-4.444444444]
											>>)
				assert("make_with_vertices Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_make_with_vertices_vector_normal
			-- Normal test for `make_with_vertices_vector'
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_with_vertices_vector", "covers/{B2D_SHAPE_POLYGON}.vertices", "covers/{B2D_SHAPE_POLYGON}.count"
		local
			l_shape:B2D_SHAPE_POLYGON
			l_vertices:LIST[B2D_VECTOR_2D]
		do
			create l_shape.make_with_vertices_vector (<<
											create {B2D_VECTOR_2D}.make_with_coordinates (72.285714286, 5.0),
											create {B2D_VECTOR_2D}.make_with_coordinates (70.428571429, 18.57142857),
											create {B2D_VECTOR_2D}.make_with_coordinates (15.428571429, 44.285714286),
											create {B2D_VECTOR_2D}.make_with_coordinates (17.571428571, 24.0),
											create {B2D_VECTOR_2D}.make_with_coordinates (41.0, 9.285714286)
											>>)
			assert ("make_with_vertices_vector not valid: count", l_shape.count ~ 5)
			l_vertices := l_shape.vertices
			assert ("make_with_vertices_vector not valid: vertices[1].x", compare_real_32 (l_vertices[1].x, 72.285714286))
			assert ("make_with_vertices_vector not valid: vertices[1].y", compare_real_32 (l_vertices[1].y, 5))
			assert ("make_with_vertices_vector not valid: vertices[2].x", compare_real_32 (l_vertices[2].x, 70.428571429))
			assert ("make_with_vertices_vector not valid: vertices[2].y", compare_real_32 (l_vertices[2].y, 18.57142857))
			assert ("make_with_vertices_vector not valid: vertices[3].x", compare_real_32 (l_vertices[3].x, 15.428571429))
			assert ("make_with_vertices_vector not valid: vertices[3].y", compare_real_32 (l_vertices[3].y, 44.285714286))
			assert ("make_with_vertices_vector not valid: vertices[4].x", compare_real_32 (l_vertices[4].x, 17.571428571))
			assert ("make_with_vertices_vector not valid: vertices[4].y", compare_real_32 (l_vertices[4].y, 24))
			assert ("make_with_vertices_vector not valid: vertices[5].x", compare_real_32 (l_vertices[5].x, 41.0))
			assert ("make_with_vertices_vector not valid: vertices[5].y", compare_real_32 (l_vertices[5].y, 9.285714286))
		end

	test_make_with_vertices_vector_limit_1
			-- Limit test 1 for `make_with_vertices_vector'
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_with_vertices_vector", "covers/{B2D_SHAPE_POLYGON}.exists"
		local
			l_shape:B2D_SHAPE_POLYGON
		do
			create l_shape.make_with_vertices_vector (<<
											create {B2D_VECTOR_2D}.make_with_coordinates (1.0, 1.0),
											create {B2D_VECTOR_2D}.make_with_coordinates (2.0, 2.0),
											create {B2D_VECTOR_2D}.make_with_coordinates (3.0, 3.0)
											>>)

			assert ("make_with_vertices_vector not valid: exists", not l_shape.exists)
		end

	test_make_with_vertices_vector_limit_2
			-- Limit test 2 for `make_with_vertices_vector'
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_with_vertices_vector", "covers/{B2D_SHAPE_POLYGON}.vertices", "covers/{B2D_SHAPE_POLYGON}.count"
		local
			l_shape:B2D_SHAPE_POLYGON
			l_vertices:LIST[B2D_VECTOR_2D]
		do
			create l_shape.make_with_vertices_vector (<<
											create {B2D_VECTOR_2D}.make_with_coordinates (72.285714286, 5.0),
											create {B2D_VECTOR_2D}.make_with_coordinates (70.428571429, 18.57142857),
											create {B2D_VECTOR_2D}.make_with_coordinates (15.428571429, 44.285714286)
											>>)
			assert ("make_with_vertices_vector not valid: count", l_shape.count ~ 3)
			l_vertices := l_shape.vertices
			assert ("make_with_vertices_vector not valid: vertices[1].x", compare_real_32 (l_vertices[1].x, 72.285714286))
			assert ("make_with_vertices_vector not valid: vertices[1].y", compare_real_32 (l_vertices[1].y, 5))
			assert ("make_with_vertices_vector not valid: vertices[2].x", compare_real_32 (l_vertices[2].x, 70.428571429))
			assert ("make_with_vertices_vector not valid: vertices[2].y", compare_real_32 (l_vertices[2].y, 18.57142857))
			assert ("make_with_vertices_vector not valid: vertices[3].x", compare_real_32 (l_vertices[3].x, 15.428571429))
			assert ("make_with_vertices_vector not valid: vertices[3].y", compare_real_32 (l_vertices[3].y, 44.285714286))
		end

	test_make_with_vertices_vector_limit_3
			-- Limit test 3 for `make_with_vertices_vector'
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_with_vertices_vector", "covers/{B2D_SHAPE_POLYGON}.vertices", "covers/{B2D_SHAPE_POLYGON}.count"
		local
			l_shape:B2D_SHAPE_POLYGON
			l_vertices:LIST[B2D_VECTOR_2D]
		do
			create l_shape.make_with_vertices_vector (<<
											create {B2D_VECTOR_2D}.make_with_coordinates (15.0, -2.555555556),
											create {B2D_VECTOR_2D}.make_with_coordinates (14.666666667, 2.666666667),
											create {B2D_VECTOR_2D}.make_with_coordinates (8.555555556, 7.444444444),
											create {B2D_VECTOR_2D}.make_with_coordinates (-2.111111111, 7.555555556),
											create {B2D_VECTOR_2D}.make_with_coordinates (-9.666666667, 2.888888889),
											create {B2D_VECTOR_2D}.make_with_coordinates (-8.888888889, -5.555555556),
											create {B2D_VECTOR_2D}.make_with_coordinates (0.333333333, -7.0),
											create {B2D_VECTOR_2D}.make_with_coordinates (8.888888889, -4.444444444)
											>>)
			assert ("make_with_vertices_vector not valid: count", l_shape.count ~ 8)
			l_vertices := l_shape.vertices
			assert ("make_with_vertices_vector not valid: vertices[1].x", compare_real_32 (l_vertices[1].x, 15.0))
			assert ("make_with_vertices_vector not valid: vertices[1].y", compare_real_32 (l_vertices[1].y, -2.555555556))
			assert ("make_with_vertices_vector not valid: vertices[2].x", compare_real_32 (l_vertices[2].x, 14.666666667))
			assert ("make_with_vertices_vector not valid: vertices[2].y", compare_real_32 (l_vertices[2].y, 2.666666667))
			assert ("make_with_vertices_vector not valid: vertices[3].x", compare_real_32 (l_vertices[3].x, 8.555555556))
			assert ("make_with_vertices_vector not valid: vertices[3].y", compare_real_32 (l_vertices[3].y, 7.444444444))
			assert ("make_with_vertices_vector not valid: vertices[4].x", compare_real_32 (l_vertices[4].x, -2.111111111))
			assert ("make_with_vertices_vector not valid: vertices[4].y", compare_real_32 (l_vertices[4].y, 7.555555556))
			assert ("make_with_vertices_vector not valid: vertices[5].x", compare_real_32 (l_vertices[5].x, -9.666666667))
			assert ("make_with_vertices_vector not valid: vertices[5].y", compare_real_32 (l_vertices[5].y, 2.888888889))
			assert ("make_with_vertices_vector not valid: vertices[6].x", compare_real_32 (l_vertices[6].x, -8.888888889))
			assert ("make_with_vertices_vector not valid: vertices[6].y", compare_real_32 (l_vertices[6].y, -5.555555556))
			assert ("make_with_vertices_vector not valid: vertices[7].x", compare_real_32 (l_vertices[7].x, 0.333333333))
			assert ("make_with_vertices_vector not valid: vertices[7].y", compare_real_32 (l_vertices[7].y, -7.0))
			assert ("make_with_vertices_vector not valid: vertices[8].x", compare_real_32 (l_vertices[8].x, 8.888888889))
			assert ("make_with_vertices_vector not valid: vertices[8].y", compare_real_32 (l_vertices[8].y, -4.444444444))
		end

	test_make_with_vertices_vector_wrong_1
			-- Wrong test 1 for `make_with_vertices_vector'
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_with_vertices_vector", "covers/{B2D_SHAPE_POLYGON}.vertices", "covers/{B2D_SHAPE_POLYGON}.count"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_POLYGON
		do
			if not l_retry then
				create l_shape.make_with_vertices_vector (<<
											create {B2D_VECTOR_2D}.make_with_coordinates (72.285714286, 5.0),
											create {B2D_VECTOR_2D}.make_with_coordinates (70.428571429, 18.57142857)
											>>)
				assert("make_with_vertices_vector Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_make_with_vertices_vector_wrong_2
			-- Wrong test 2 for `make_with_vertices_vector'
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_with_vertices_vector", "covers/{B2D_SHAPE_POLYGON}.vertices", "covers/{B2D_SHAPE_POLYGON}.count"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_POLYGON
		do
			if not l_retry then
				create l_shape.make_with_vertices_vector (<<
											create {B2D_VECTOR_2D}.make_with_coordinates (15.0, -2.555555556),
											create {B2D_VECTOR_2D}.make_with_coordinates (14.666666667, 2.666666667),
											create {B2D_VECTOR_2D}.make_with_coordinates (8.555555556, 7.444444444),
											create {B2D_VECTOR_2D}.make_with_coordinates (-2.111111111, 7.555555556),
											create {B2D_VECTOR_2D}.make_with_coordinates (-9.666666667, 2.888888889),
											create {B2D_VECTOR_2D}.make_with_coordinates (-8.888888889, -5.555555556),
											create {B2D_VECTOR_2D}.make_with_coordinates (0.333333333, -7.0),
											create {B2D_VECTOR_2D}.make_with_coordinates (8.888888889, -4.444444444),
											create {B2D_VECTOR_2D}.make_with_coordinates (9.0, -4.444444444)
											>>)
				assert("make_with_vertices_vector Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_normal_vertices_normal
			-- Normal test for `normal_vertices'
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_with_vertices", "covers/{B2D_SHAPE_POLYGON}.normal_vertices"
		local
			l_shape:B2D_SHAPE_POLYGON
			l_vertices:LIST[B2D_VECTOR_2D]
		do
			create l_shape.make_with_vertices (<<
											[{REAL_32}72.285714286, {REAL_32}5.0],
											[{REAL_32}70.428571429, {REAL_32}18.57142857],
											[{REAL_32}15.428571429, {REAL_32}44.285714286],
											[{REAL_32}17.571428571, {REAL_32}24.0],
											[{REAL_32}41.0, {REAL_32}9.285714286]
											>>)
			l_vertices := l_shape.normal_vertices
			assert ("normal_vertices not valid: vertices[1].x", compare_real_32 (l_vertices[1].x, 0.990767))
			assert ("normal_vertices not valid: vertices[1].y", compare_real_32 (l_vertices[1].y, 0.135578))
			assert ("normal_vertices not valid: vertices[2].x", compare_real_32 (l_vertices[2].x, 0.423529))
			assert ("normal_vertices not valid: vertices[2].y", compare_real_32 (l_vertices[2].y, 0.905882))
			assert ("normal_vertices not valid: vertices[3].x", compare_real_32 (l_vertices[3].x, -0.994467))
			assert ("normal_vertices not valid: vertices[3].y", compare_real_32 (l_vertices[3].y, -0.105049))
			assert ("normal_vertices not valid: vertices[4].x", compare_real_32 (l_vertices[4].x, -0.531854))
			assert ("normal_vertices not valid: vertices[4].y", compare_real_32 (l_vertices[4].y, -0.846836))
			assert ("normal_vertices not valid: vertices[5].x", compare_real_32 (l_vertices[5].x, -0.135719))
			assert ("normal_vertices not valid: vertices[5].y", compare_real_32 (l_vertices[5].y, -0.990747))
		end

	test_is_valid_normal_1
			-- Normal test 1 for `is_valid'
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_with_vertices", "covers/{B2D_SHAPE_POLYGON}.is_valid"
		local
			l_shape:B2D_SHAPE_POLYGON
		do
			create l_shape.make_with_vertices (<<
											[{REAL_32}72.285714286, {REAL_32}5.0],
											[{REAL_32}70.428571429, {REAL_32}18.57142857],
											[{REAL_32}15.428571429, {REAL_32}44.285714286],
											[{REAL_32}17.571428571, {REAL_32}24.0],
											[{REAL_32}41.0, {REAL_32}9.285714286]
											>>)
			assert ("is_valid not valid", l_shape.is_valid)
		end

	test_is_valid_normal_2
			-- Normal test 2 for `is_valid'
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_with_vertices", "covers/{B2D_SHAPE_POLYGON}.is_valid"
		local
			l_shape:B2D_SHAPE_POLYGON
		do
			create l_shape.make_with_vertices (<<
											[{REAL_32}1.0, {REAL_32}1.0],
											[{REAL_32}3.0, {REAL_32}1.0],
											[{REAL_32}0.0, {REAL_32}2.0],
											[{REAL_32}1.0, {REAL_32}3.0]
											>>)
			assert ("Implementation problem: Always return True", not l_shape.is_valid)
		end

	test_centroid_normal
			-- Normal test for centroid
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_box", "covers/{B2D_SHAPE_POLYGON}.centroid"
		local
			l_input:B2D_SHAPE_POLYGON
		do
			create l_input.make_oriented_box (5, 5, 0, 10, 10)
			assert("centroid Valid: X", l_input.centroid.x ~ 10)
			assert("centroid Valid: Y", l_input.centroid.y ~ 10)
		end

	test_child_count
			-- Normal test for child_count
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_box", "covers/{B2D_SHAPE_POLYGON}.child_count"
		local
			l_input:B2D_SHAPE
		do
			create {B2D_SHAPE_POLYGON}l_input.make_box (10, 10)
			assert("child_count Valid", l_input.child_count ~ 1)
		end

	test_contains_normal_1
			-- Normal test 1 for contains
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_box", "covers/{B2D_SHAPE_POLYGON}.contains"
		local
			l_shape:B2D_SHAPE_POLYGON
			l_transform:B2D_TRANSFORM
			l_point:B2D_VECTOR_2D
		do
			create l_point.make_with_coordinates (5, 5)
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			create l_shape.make_box (10, 10)
			assert("contains Valid", l_shape.contains_with_vector (l_transform, l_point))
		end

	test_contains_normal_2
			-- Normal test 2 for contains
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_box", "covers/{B2D_SHAPE_POLYGON}.contains"
		local
			l_shape:B2D_SHAPE_POLYGON
			l_transform:B2D_TRANSFORM
			l_point:B2D_VECTOR_2D
		do
			create l_point.make_with_coordinates (10.1, 5)
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			create l_shape.make_box (10, 10)
			assert("contains Valid", not l_shape.contains_with_vector (l_transform, l_point))
		end

	test_contains_limit
			-- Limit test for contains
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_box", "covers/{B2D_SHAPE_POLYGON}.contains"
		local
			l_shape:B2D_SHAPE_POLYGON
			l_transform:B2D_TRANSFORM
			l_point:B2D_VECTOR_2D
		do
			create l_point.make_with_coordinates (10, 10)
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			create l_shape.make_box (10, 10)
			assert("contains Valid", l_shape.contains_with_vector (l_transform, l_point))
		end

	test_ray_cast_normal_1
			-- Limit test 1 for ray_cast
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_box", "covers/{B2D_SHAPE_POLYGON}.ray_cast"
		local
			l_shape:B2D_SHAPE_POLYGON
			l_transform:B2D_TRANSFORM
			l_input:B2D_RAY_CAST_INPUT
		do
			create l_input.make_with_coordinates (10, 20, 10, 10, 15)
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			create l_shape.make_box (10, 10)
			if attached l_shape.ray_cast (l_input, l_transform) as la_output then
				assert("ray_cast Valid. Normal: X", la_output.normal.x ~ 0)
				assert("ray_cast Valid. Normal: Y", la_output.normal.y ~ 1)
				assert("ray_cast Valid. Fraction", la_output.fraction ~ 1)
			else
				assert("ray_cast Valid", False)
			end
		end

	test_ray_cast_normal_2
			-- Limit test 2 for ray_cast
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_box", "covers/{B2D_SHAPE_POLYGON}.ray_cast"
		local
			l_shape:B2D_SHAPE_POLYGON
			l_transform:B2D_TRANSFORM
			l_input:B2D_RAY_CAST_INPUT
		do
			create l_input.make_with_coordinates (150, 150, 160, 160, 12)
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			create l_shape.make_box (10, 10)
			assert("ray_cast Valid", not attached l_shape.ray_cast (l_input, l_transform))
		end

	test_ray_cast_normal_3
			-- Limit test 3 for ray_cast
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_box", "covers/{B2D_SHAPE}.ray_cast"
		local
			l_shape:B2D_SHAPE
			l_transform:B2D_TRANSFORM
			l_input:B2D_RAY_CAST_INPUT
		do
			create l_input.make_with_coordinates (10, 20, 10, 10, 15)
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			create {B2D_SHAPE_POLYGON}l_shape.make_box (10, 10)
			if attached l_shape.ray_cast (l_input, l_transform, 1) as la_output then
				assert("ray_cast Valid. Normal: X", la_output.normal.x ~ 0)
				assert("ray_cast Valid. Normal: Y", la_output.normal.y ~ 1)
				assert("ray_cast Valid. Fraction", la_output.fraction ~ 1)
			else
				assert("ray_cast Valid", False)
			end
		end

	test_ray_cast_normal_4
			-- Limit test 4 for ray_cast
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_box", "covers/{B2D_SHAPE}.ray_cast"
		local
			l_shape:B2D_SHAPE
			l_transform:B2D_TRANSFORM
			l_input:B2D_RAY_CAST_INPUT
		do
			create l_input.make_with_coordinates (150, 150, 160, 160, 12)
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			create {B2D_SHAPE_POLYGON}l_shape.make_box (10, 10)
			assert("ray_cast Valid", not attached l_shape.ray_cast (l_input, l_transform, 1))
		end

	test_ray_cast_wrong_1
			-- Wrong test 1 for ray_cast
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_box", "covers/{B2D_SHAPE_POLYGON}.ray_cast"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE
			l_input:B2D_RAY_CAST_INPUT
			l_transform:B2D_TRANSFORM
			l_output: detachable B2D_RAY_CAST_OUTPUT
		do
			if not l_retry then
				create l_transform.make_with_coordinates_and_angle (0, 0, 0)
				create {B2D_SHAPE_POLYGON}l_shape.make_box (10, 10)
				create l_input.make_with_coordinates (150, 150, 160, 160, 12)
				l_output := l_shape.ray_cast (l_input, l_transform, 2)
				assert("ray_cast Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_ray_cast_wrong_2
			-- Wrong test 2 for ray_cast
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_box", "covers/{B2D_SHAPE_POLYGON}.ray_cast"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE
			l_input:B2D_RAY_CAST_INPUT
			l_transform:B2D_TRANSFORM
			l_output: detachable B2D_RAY_CAST_OUTPUT
		do
			if not l_retry then
				create l_transform.make_with_coordinates_and_angle (0, 0, 0)
				create {B2D_SHAPE_POLYGON}l_shape.make_box (10, 10)
				create l_input.make_with_coordinates (150, 150, 160, 160, 12)
				l_output := l_shape.ray_cast (l_input, l_transform, 0)
				assert("ray_cast Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_ray_cast_wrong_3
			-- Wrong test 3 for mass
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_box", "covers/{B2D_SHAPE_POLYGON}.ray_cast"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_POLYGON
			l_input:B2D_RAY_CAST_INPUT
			l_transform:B2D_TRANSFORM
			l_output: detachable B2D_RAY_CAST_OUTPUT
		do
			if not l_retry then
				create l_transform.make_with_coordinates_and_angle (0, 0, 0)
				create l_input.make_with_coordinates (150, 150, 160, 160, 12)
				create l_shape.make_with_vertices_vector (<<
											create {B2D_VECTOR_2D}.make_with_coordinates (1.0, 1.0),
											create {B2D_VECTOR_2D}.make_with_coordinates (2.0, 2.0),
											create {B2D_VECTOR_2D}.make_with_coordinates (3.0, 3.0)
											>>)
				l_output := l_shape.ray_cast (l_input, l_transform)
				assert("ray_cast Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_mass_normal
			-- Normal test for mass
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_box", "covers/{B2D_SHAPE_POLYGON}.mass"
		local
			l_shape:B2D_SHAPE_POLYGON
			l_mass:B2D_MASS
		do
			create l_shape.make_box (10, 10)
			l_mass := l_shape.mass (10)
			assert("mass Valid: center.x", l_mass.center.x ~ 0)
			assert("mass Valid: center.y", l_mass.center.y ~ 0)
			assert("mass Valid: Mass", compare_real_32 (l_mass.mass, 4000))
			assert("mass Valid: inertia", compare_real_32 (l_mass.inertia, 266667))
		end

	test_mass_limit
			-- Limit test for mass
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_box", "covers/{B2D_SHAPE_POLYGON}.mass"
		local
			l_shape:B2D_SHAPE_POLYGON
			l_mass:B2D_MASS
		do
			create l_shape.make_box (10, 10)
			l_mass := l_shape.mass (0)
			assert("mass Valid: center.x", l_mass.center.x ~ 0)
			assert("mass Valid: center.y", l_mass.center.y ~ 0)
			assert("mass Valid: Mass", compare_real_32 (l_mass.mass, 0))
			assert("mass Valid: inertia", compare_real_32 (l_mass.inertia, 0))
		end

	test_mass_wrong
			-- Wrong test 3 for mass
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_box", "covers/{B2D_SHAPE_POLYGON}.mass"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_POLYGON
			l_result:B2D_MASS
		do
			if not l_retry then
				create l_shape.make_with_vertices_vector (<<
											create {B2D_VECTOR_2D}.make_with_coordinates (1.0, 1.0),
											create {B2D_VECTOR_2D}.make_with_coordinates (2.0, 2.0),
											create {B2D_VECTOR_2D}.make_with_coordinates (3.0, 3.0)
											>>)
				l_result := l_shape.mass(0)
				assert("mass Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_axis_aligned_bounding_box_normal_1
			-- Normal test 1 for axis_aligned_bounding_box
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_box", "covers/{B2D_SHAPE_POLYGON}.axis_aligned_bounding_box", "covers/{B2D_SHAPE_POLYGON}.edge_width"
		local
			l_shape:B2D_SHAPE_POLYGON
			l_result:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_transform:B2D_TRANSFORM
		do
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			create l_shape.make_box (10, 10)
			l_result := l_shape.axis_aligned_bounding_box (l_transform)
			assert("axis_aligned_bounding_box Valid: lower_bound.x", l_result.lower_bound.x ~ -10 - l_shape.edge_width)
			assert("axis_aligned_bounding_box Valid: lower_bound.y", l_result.lower_bound.y ~ -10 - l_shape.edge_width)
			assert("axis_aligned_bounding_box Valid: upper_bound.x", l_result.upper_bound.x ~ 10 + l_shape.edge_width)
			assert("axis_aligned_bounding_box Valid: upper_bound.y", l_result.upper_bound.y ~ 10 + l_shape.edge_width)
		end

	test_axis_aligned_bounding_box_normal_2
			-- Normal test 2 for axis_aligned_bounding_box
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_box", "covers/{B2D_SHAPE}.axis_aligned_bounding_box", "covers/{B2D_SHAPE_POLYGON}.edge_width"
		local
			l_shape:B2D_SHAPE
			l_polygon:B2D_SHAPE_POLYGON
			l_result:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_transform:B2D_TRANSFORM
		do
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			create l_polygon.make_box (10, 10)
			l_shape := l_polygon
			l_result := l_shape.axis_aligned_bounding_box (l_transform, 1)
			assert("axis_aligned_bounding_box Valid: lower_bound.x", l_result.lower_bound.x ~ -10 - l_polygon.edge_width)
			assert("axis_aligned_bounding_box Valid: lower_bound.y", l_result.lower_bound.y ~ -10 - l_polygon.edge_width)
			assert("axis_aligned_bounding_box Valid: upper_bound.x", l_result.upper_bound.x ~ 10 + l_polygon.edge_width)
			assert("axis_aligned_bounding_box Valid: upper_bound.y", l_result.upper_bound.y ~ 10 + l_polygon.edge_width)
		end

	test_axis_aligned_bounding_box_wrong_1
			-- Wrong test 1 for axis_aligned_bounding_box
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_box", "covers/{B2D_SHAPE_POLYGON}.axis_aligned_bounding_box", "covers/{B2D_SHAPE_POLYGON}.edge_width"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE
			l_result:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_transform:B2D_TRANSFORM
		do
			if not l_retry then
				create l_transform.make_with_coordinates_and_angle (0, 0, 0)
				create {B2D_SHAPE_POLYGON}l_shape.make_box (10, 10)
				l_result := l_shape.axis_aligned_bounding_box (l_transform, 0)
				assert("axis_aligned_bounding_box Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_axis_aligned_bounding_box_wrong_2
			-- Wrong test 2 for axis_aligned_bounding_box
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_box", "covers/{B2D_SHAPE_POLYGON}.axis_aligned_bounding_box", "covers/{B2D_SHAPE_POLYGON}.edge_width"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE
			l_result:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_transform:B2D_TRANSFORM
		do
			if not l_retry then
				create l_transform.make_with_coordinates_and_angle (0, 0, 0)
				create {B2D_SHAPE_POLYGON}l_shape.make_box (10, 10)
				l_result := l_shape.axis_aligned_bounding_box (l_transform, 2)
				assert("axis_aligned_bounding_box Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_axis_aligned_bounding_box_wrong_3
			-- Wrong test 3 for axis_aligned_bounding_box
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_box", "covers/{B2D_SHAPE_POLYGON}.axis_aligned_bounding_box"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_POLYGON
			l_result:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_transform:B2D_TRANSFORM
		do
			if not l_retry then
				create l_transform.make_with_coordinates_and_angle (0, 0, 0)
				create l_shape.make_with_vertices_vector (<<
											create {B2D_VECTOR_2D}.make_with_coordinates (1.0, 1.0),
											create {B2D_VECTOR_2D}.make_with_coordinates (2.0, 2.0),
											create {B2D_VECTOR_2D}.make_with_coordinates (3.0, 3.0)
											>>)
				l_result := l_shape.axis_aligned_bounding_box (l_transform)
				assert("axis_aligned_bounding_box Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_is_valid_wrong
			-- Wrong test for is_valid
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_with_vertices_vector", "covers/{B2D_SHAPE_POLYGON}.is_valid"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_POLYGON
			l_result:BOOLEAN
		do
			if not l_retry then
				create l_shape.make_with_vertices_vector (<<
											create {B2D_VECTOR_2D}.make_with_coordinates (1.0, 1.0),
											create {B2D_VECTOR_2D}.make_with_coordinates (2.0, 2.0),
											create {B2D_VECTOR_2D}.make_with_coordinates (3.0, 3.0)
											>>)
				l_result := l_shape.is_valid
				assert("is_valid Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_centroid_wrong
			-- Wrong test for centroid
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_with_vertices_vector", "covers/{B2D_SHAPE_POLYGON}.centroid"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_POLYGON
			l_result:B2D_VECTOR_2D
		do
			if not l_retry then
				create l_shape.make_with_vertices_vector (<<
											create {B2D_VECTOR_2D}.make_with_coordinates (1.0, 1.0),
											create {B2D_VECTOR_2D}.make_with_coordinates (2.0, 2.0),
											create {B2D_VECTOR_2D}.make_with_coordinates (3.0, 3.0)
											>>)
				l_result := l_shape.centroid
				assert("centroid Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_count_wrong
			-- Wrong test for count
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_with_vertices_vector", "covers/{B2D_SHAPE_POLYGON}.count"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_POLYGON
			l_result:INTEGER
		do
			if not l_retry then
				create l_shape.make_with_vertices_vector (<<
											create {B2D_VECTOR_2D}.make_with_coordinates (1.0, 1.0),
											create {B2D_VECTOR_2D}.make_with_coordinates (2.0, 2.0),
											create {B2D_VECTOR_2D}.make_with_coordinates (3.0, 3.0)
											>>)
				l_result := l_shape.count
				assert("count Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_vertices_wrong
			-- Wrong test for vertices
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_with_vertices_vector", "covers/{B2D_SHAPE_POLYGON}.vertices"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_POLYGON
			l_result:LIST[B2D_VECTOR_2D]
		do
			if not l_retry then
				create l_shape.make_with_vertices_vector (<<
											create {B2D_VECTOR_2D}.make_with_coordinates (1.0, 1.0),
											create {B2D_VECTOR_2D}.make_with_coordinates (2.0, 2.0),
											create {B2D_VECTOR_2D}.make_with_coordinates (3.0, 3.0)
											>>)
				l_result := l_shape.vertices
				assert("vertices Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_normal_vertices_wrong
			-- Wrong test for vertices
		note
			testing:  "covers/{B2D_SHAPE_POLYGON}.make_with_vertices_vector", "covers/{B2D_SHAPE_POLYGON}.normal_vertices"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_POLYGON
			l_result:LIST[B2D_VECTOR_2D]
		do
			if not l_retry then
				create l_shape.make_with_vertices_vector (<<
											create {B2D_VECTOR_2D}.make_with_coordinates (1.0, 1.0),
											create {B2D_VECTOR_2D}.make_with_coordinates (2.0, 2.0),
											create {B2D_VECTOR_2D}.make_with_coordinates (3.0, 3.0)
											>>)
				l_result := l_shape.normal_vertices
				assert("normal_vertices Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end


end


