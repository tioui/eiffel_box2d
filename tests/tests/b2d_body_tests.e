note
	description: "Tests for {B2D_BODY}."
	author: "Patrick Boucher"
	date: "Tue, 22 jun 2020 01:00:00"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_BODY_TESTS

inherit
	EQA_TEST_SET
		redefine
			on_prepare
		end
	B2D_COMPARE_REAL
		undefine
			default_create
		end

feature {NONE} -- Events

	on_prepare
			-- Called before execution of each test.
			-- If {B2D_WORLD}.last_body is not define, the preparation test will fail
			-- and the execution test won't be executed.
		local
			l_world_creation_succeed: BOOLEAN
		do
			create world.make (0, 1)
			if world.exists and not world.is_locked then
				world.step_time := 1
				world.create_body (create {B2D_BODY_DEFINITION})
				if attached world.last_body as la_last_body then
					if la_last_body.exists then
						la_last_body.gravity_scale := 0
						l_world_creation_succeed := True
					end
				end
			end
			assert ("World creation succeed", l_world_creation_succeed)
		end

feature -- Test routines

	test_make
			-- Test routine for {B2D_BODY}.make
		note
			testing:  "covers/{B2D_BODY}.make", "covers/{B2D_BODY}.world"
		do
			if attached world.last_body as la_body then
				assert ("world valid", la_body.world = world)
			end
		end

	test_create_fixture
			-- Test routine for {B2D_BODY}.create_fixture*
		note
			testing:
				"covers/{B2D_BODY}.create_fixture",
				"covers/{B2D_BODY}.create_fixture_from_definition",
				"covers/{B2D_BODY}.create_fixture_from_shape",
				"covers/{B2D_BODY}.fixtures",
				"covers/{B2D_BODY}.last_fixture"
		local
			l_shape: B2D_SHAPE_POLYGON
			l_fixture_definition: B2D_FIXTURE_DEFINITION
		do
			if attached world.last_body as la_body then
				assert ("Zero fixture", la_body.fixtures.count ~ 0)
				create l_shape.make_box (1, 1)
				la_body.create_fixture_from_shape (l_shape, 1)
				assert ("last_fixture valid", la_body.last_fixture = la_body.fixtures [1])
				create l_fixture_definition.make (l_shape)
				la_body.create_fixture_from_definition (l_fixture_definition)
				assert ("last_fixture valid", la_body.last_fixture = la_body.fixtures [2])
			end
		end

	test_fixture_count
			-- Test routine for {B2D_BODY}.fixture_count*
		note
			testing:
				"covers/{B2D_BODY}.create_fixture",
				"covers/{B2D_BODY}.create_fixture_from_definition",
				"covers/{B2D_BODY}.create_fixture_from_shape",
				"covers/{B2D_BODY}.fixture_count"
		local
			l_shape: B2D_SHAPE_POLYGON
			l_fixture_definition: B2D_FIXTURE_DEFINITION
		do
			if attached world.last_body as la_body then
				assert ("Zero fixture", la_body.fixture_count ~ 0)
				create l_shape.make_box (1, 1)
				la_body.create_fixture_from_shape (l_shape, 1)
				create l_fixture_definition.make (l_shape)
				la_body.create_fixture_from_definition (l_fixture_definition)
				assert ("fixture_count valid", la_body.fixture_count = 2)
			end
		end

	test_destroy_fixture
			-- Test routine for {B2D_BODY}.destroy_fixture
		note
			testing:
				"covers/{B2D_BODY}.destroy_fixture",
				"covers/{B2D_BODY}.last_fixture",
				"covers/{B2D_BODY}.fixtures"
		local
			l_shape: B2D_SHAPE_POLYGON
			l_reference_fixture: B2D_FIXTURE
		do
			if attached world.last_body as la_body then
				create l_shape.make_box (1, 1)
				from
				until
					la_body.fixtures.count = 3
				loop
					la_body.create_fixture_from_shape (l_shape, 1)
				end
				l_reference_fixture := la_body.fixtures.last
				la_body.destroy_fixture (la_body.fixtures [2])
				assert ("fixtures decreased", la_body.fixtures.count = 2)
				assert ("Deleting middle fixture don't change last_fixture", la_body.last_fixture = l_reference_fixture)
				l_reference_fixture := la_body.fixtures.first
				la_body.destroy_fixture (la_body.fixtures.last)
				assert ("Last fixture changed", la_body.last_fixture = l_reference_fixture)
				la_body.destroy_fixture (la_body.fixtures.first)
				assert ("last_fixture Void", not attached la_body.last_fixture)
			end
		end

	test_fixtures
			-- Test routine for {B2D_BODY}.fixtures
		note
			testing:  "covers/{B2D_BODY}.fixtures"
		local
			l_shape: B2D_SHAPE_POLYGON
			l_list_1, l_list_2: LIST[B2D_FIXTURE]
		do
			if attached world.last_body as la_body then
				create l_shape.make_box (1, 1)
				la_body.create_fixture_from_shape (l_shape, 1)
				l_list_1 := la_body.fixtures
				l_list_2 := la_body.fixtures
				assert ("fixtures is copy of internal_fixtures", l_list_1 /= l_list_2)
			end
		end

	test_has_fixture
			-- Test routine for {B2D_BODY}.has_fixture
		note
			testing:  "covers/{B2D_BODY}.has_fixture"
		local
			l_shape: B2D_SHAPE_POLYGON
		do
			if attached world.last_body as la_body then
				create l_shape.make_box (1, 1)
				la_body.create_fixture_from_shape (l_shape, 1)
				assert ("has_fixture valid", la_body.has_fixture (la_body.fixtures.last))
				-- TODO: Test with a stranger fixture
				assert ("Not implemented: Needs {B2D_FIXTURE}", False)
			end
		end

	test_transform_1
			-- Test routine for {B2D_BODY}.set_tansform
		note
			testing:
				"covers/{B2D_BODY}.set_transform_with_vector",
				"covers/{B2D_BODY}.set_transform_with_transform",
				"covers/{B2D_BODY}.transform"
		local
			l_position: B2D_VECTOR_2D
			l_rotation: B2D_ROTATION
			l_transform: B2D_TRANSFORM
		do
			if attached world.last_body as la_body then
				create l_position.make_with_coordinates (-2.356, 1.025)
				create l_rotation.make (1.23)
				la_body.set_transform_with_vector (l_position, l_rotation.angle)
				assert ("transform valid", la_body.transform.position ~ l_position and la_body.transform.rotation ~ l_rotation)
				la_body.set_transform_with_vector (create {B2D_VECTOR_2D}, 0)
				create l_transform.make_with_vector_and_rotation (l_position, l_rotation)
				la_body.set_transform_with_transform (l_transform)
				assert ("transform valid", la_body.transform.position ~ l_position and la_body.transform.rotation ~ l_rotation)
			end
		end

	test_transform_2
			-- Test routine for {B2D_BODY}.set_tansform
		note
			testing:
				"covers/{B2D_BODY}.set_transform",
				"covers/{B2D_BODY}.set_transform_with_transform",
				"covers/{B2D_BODY}.transform"
		local
			l_position: B2D_VECTOR_2D
		do
			if attached world.last_body as la_body then
				create l_position.make_with_coordinates (-2.356, 1.025)
				la_body.set_transform (-2.356, 1.025, 1.23)
				assert ("transform valid", la_body.transform.position ~ l_position and compare_real_32 (la_body.transform.rotation.angle, 1.23))
			end
		end

	test_position_and_angle
			-- Test routine for {B2D_BODY}.position and {B2D_BODY}.angle
		note
			testing:
				"covers/{B2D_BODY}.position",
				"covers/{B2D_BODY}.angle",
				"covers/{B2D_BODY}.set_transform"
		local
			l_position: B2D_VECTOR_2D
			l_angle: REAL_32
		do
			if attached world.last_body as la_body then
				create l_position.make_with_coordinates (-1.02, -3.256)
				l_angle := -0.125
				la_body.set_transform_with_vector (l_position, l_angle)
				assert ("position valid", la_body.position ~ l_position)
				assert ("angle valid", la_body.angle ~ l_angle)
			end
		end

	test_center_of_mass
			-- Test routine for {B2D_BODY}.center_of_mass
		note
			testing:  "covers/{B2D_BODY}.center_of_mass"
		local
			l_origin: B2D_VECTOR_2D
			l_center: B2D_VECTOR_2D
			l_expected: B2D_VECTOR_2D
			l_shape: B2D_SHAPE_POLYGON
		do
			if attached world.last_body as la_body then
				create l_origin.make_with_coordinates (8, 8)
				la_body.set_transform_with_vector (l_origin, 0)
				create l_center.make_with_coordinates (12, 12)
				create l_shape.make_oriented_box_with_vector (l_center.x, l_center.y, 0, l_center)
				la_body.create_fixture_from_shape (l_shape, 1)
				create l_expected.make_with_coordinates (l_origin.x + l_center.x, l_origin.y + l_center.y)
				assert ("center_of_mass valid", la_body.center_of_mass ~ l_expected and la_body.center_of_mass /~ la_body.position)
			end
		end

		test_center_of_mass_local
			-- Test routine for {B2D_BODY}.center_of_mass_local
		note
			testing:  "covers/{B2D_BODY}.center_of_mass_local"
		local
			l_origin: B2D_VECTOR_2D
			l_center: B2D_VECTOR_2D
			l_shape: B2D_SHAPE_POLYGON
		do
			if attached world.last_body as la_body then
				create l_origin.make_with_coordinates (8, 8)
				la_body.set_transform_with_vector (l_origin, 0)
				create l_center.make_with_coordinates (12, 12)
				create l_shape.make_oriented_box_with_vector (l_center.x, l_center.y, 0, l_center)
				la_body.create_fixture_from_shape (l_shape, 1)
				assert ("center_of_mass_local valid", la_body.center_of_mass_local ~ l_center)
			end
		end

	test_linear_velocity_1
			-- Test routine for {B2D_BODY}.set_linear_velocity
		note
			testing:
				"covers/{B2D_BODY}.set_linear_velocity",
				"covers/{B2D_BODY}.linear_velocity"
		local
			l_linear_velocity: B2D_VECTOR_2D
		do
			if attached world.last_body as la_body then
				create l_linear_velocity.make_with_coordinates (-5.23, 1.56)
				la_body.set_linear_velocity_with_vector (l_linear_velocity)
				assert ("set_linear_velocity valid", la_body.linear_velocity ~�l_linear_velocity)
			end
		end

	test_linear_velocity_2
			-- Test routine for {B2D_BODY}.set_linear_velocity
		note
			testing:
				"covers/{B2D_BODY}.set_linear_velocity",
				"covers/{B2D_BODY}.linear_velocity"
		local
			l_linear_velocity: B2D_VECTOR_2D
		do
			if attached world.last_body as la_body then
				create l_linear_velocity.make_with_coordinates (-5.23, 1.56)
				la_body.set_linear_velocity (-5.23, 1.56)
				assert ("set_linear_velocity valid", la_body.linear_velocity ~�l_linear_velocity)
			end
		end

	test_angular_velocity
			-- Test routine for {B2D_BODY}.set_angular_velocity
		note
			testing:
				"covers/{B2D_BODY}.set_angular_velocity",
				"covers/{B2D_BODY}.angular_velocity"
		local
			l_angular_velocity: REAL_32
		do
			if attached world.last_body as la_body then
				l_angular_velocity := -0.125
				la_body.set_angular_velocity (l_angular_velocity)
				assert ("set_angular_velocity valid", la_body.angular_velocity ~ l_angular_velocity)
			end
		end

	test_apply_force_with_vectors
			-- Test routine for {B2D_BODY}.apply_force_with_vectors
		note
			testing:  "covers/{B2D_BODY}.apply_force_with_vectors"
		local
			l_shape: B2D_SHAPE_POLYGON
			l_point: B2D_VECTOR_2D
			l_force: B2D_VECTOR_2D
		do
			if attached world.last_body as la_body then
				create l_shape.make_box (1, 1)
				la_body.create_fixture_from_shape (l_shape, 1)
				-- Applying a force equals to the mass at the center of mass
				-- should result to push the body at exactly 1 m/s
				create l_force.make_with_coordinates (la_body.mass, 0)
				create l_point
				la_body.apply_force_with_vectors (l_force, l_point)
				world.step
				assert ("Apply force at center valid X", la_body.linear_velocity.x ~ 1)
				assert ("Apply force at center valid Y", la_body.linear_velocity.y ~ 0)
				assert ("Apply force at center won't rotate", la_body.angular_velocity ~ 0)
				-- Force on an off-center point should create a rotation.
				l_point.set_coordinates (0, -2)
				la_body.apply_force_with_vectors (l_force, l_point)
				world.step
				assert ("body rotating", la_body.angular_velocity /~ 0)
			end
		end

	test_apply_force
			-- Test routine for {B2D_BODY}.apply_force
		note
			testing:  "covers/{B2D_BODY}.apply_force"
		local
			l_shape: B2D_SHAPE_POLYGON
			l_point: B2D_VECTOR_2D
			l_force: B2D_VECTOR_2D
		do
			if attached world.last_body as la_body then
				create l_shape.make_box (1, 1)
				la_body.create_fixture_from_shape (l_shape, 1)
				-- Applying a force equals to the mass at the center of mass
				-- should result to push the body at exactly 1 m/s
				create l_force.make_with_coordinates (la_body.mass, 0)
				create l_point
				la_body.apply_force (la_body.mass, 0, 0, 0)
				world.step
				assert ("Apply force at center valid X", la_body.linear_velocity.x ~ 1)
				assert ("Apply force at center valid Y", la_body.linear_velocity.y ~ 0)
				assert ("Apply force at center won't rotate", la_body.angular_velocity ~ 0)
				-- Force on an off-center point should create a rotation.
				l_point.set_coordinates (0, -2)
				la_body.apply_force (la_body.mass, 0, 0, -2)
				world.step
				assert ("body rotating", la_body.angular_velocity /~ 0)
			end
		end

		test_apply_force_to_center_with_vector
			-- Test routine for {B2D_BODY}.apply_force_to_center_with_vector
		note
			testing:  "covers/{B2D_BODY}.apply_force_to_center_with_vector"
		local
			l_shape: B2D_SHAPE_POLYGON
			l_force: B2D_VECTOR_2D
		do
			if attached world.last_body as la_body then
				create l_shape.make_box (1, 1)
				la_body.create_fixture_from_shape (l_shape, 1)
				-- Applying a force equals to the mass at the center of mass
				-- should result to push the body at exactly 1 m/s
				create l_force.make_with_coordinates (la_body.mass, 0)
				la_body.apply_force_to_center_with_vector (l_force)
				world.step
				assert ("Apply force at center valid X", la_body.linear_velocity.x ~ 1)
				assert ("Apply force at center valid Y", la_body.linear_velocity.y ~ 0)
				assert ("Apply force at center won't rotate", la_body.angular_velocity ~ 0)
			end
		end

		test_apply_force_to_center
			-- Test routine for {B2D_BODY}.apply_force_to_center
		note
			testing:  "covers/{B2D_BODY}.apply_force_to_center"
		local
			l_shape: B2D_SHAPE_POLYGON
			l_force: B2D_VECTOR_2D
		do
			if attached world.last_body as la_body then
				create l_shape.make_box (1, 1)
				la_body.create_fixture_from_shape (l_shape, 1)
				-- Applying a force equals to the mass at the center of mass
				-- should result to push the body at exactly 1 m/s
				create l_force.make_with_coordinates (la_body.mass, 0)
				la_body.apply_force_to_center (la_body.mass, 0)
				world.step
				assert ("Apply force at center valid X", la_body.linear_velocity.x ~ 1)
				assert ("Apply force at center valid Y", la_body.linear_velocity.y ~ 0)
				assert ("Apply force at center won't rotate", la_body.angular_velocity ~ 0)
			end
		end

	test_apply_torque
			-- Test routine for {B2D_BODY}.apply_torque
		note
			testing:  "covers/{B2D_BODY}.apply_torque"
		do
			if attached world.last_body as la_body then
				assert ("To be implemented: Does not seem to work", False)
			end
		end

	test_apply_linear_impulse
			-- Test routine for {B2D_BODY}.apply_linear_impulse
		note
			testing:  "covers/{B2D_BODY}.apply_linear_impulse"
		local
			l_shape: B2D_SHAPE_POLYGON
			l_force: B2D_VECTOR_2D
			l_point: B2D_VECTOR_2D
		do
			if attached world.last_body as la_body then
				create l_shape.make_box (12, 12)
				la_body.create_fixture_from_shape (l_shape, 1)
				create l_force.make_with_coordinates (2.45, -6.125)
				create l_point.make_with_coordinates (-2, -4)
				la_body.apply_linear_impulse_with_vectors (l_force, l_point)
				assert ("linear_velocity changed",
					la_body.linear_velocity.x /~ 0 and la_body.linear_velocity.y /~ 0)
				assert ("angular_velocity changed", la_body.angular_velocity /~ 0)
			end
		end

	test_apply_linear_impulse_to_center
			-- Test routine for {B2D_BODY}.apply_linear_impulse_to_center
		note
			testing:
				"covers/{B2D_BODY}.apply_linear_impulse_to_center",
				"covers/{B2D_BODY}.apply_linear_impulse"
		local
			l_origin: B2D_VECTOR_2D
			l_shape: B2D_SHAPE_POLYGON
			l_force: B2D_VECTOR_2D
		do
			if attached world.last_body as la_body then
				create l_origin.make_with_coordinates (8, 8)
				la_body.set_transform_with_vector (l_origin, 0)
				create l_shape.make_box (12, 12)
				la_body.create_fixture_from_shape (l_shape, 1)
				create l_force.make_with_coordinates (2.45, -6.125)
				la_body.apply_linear_impulse_to_center_with_vector (l_force)
				assert ("linear_velocity changed",
					la_body.linear_velocity.x /~ 0 and la_body.linear_velocity.y /~ 0)
				assert ("angular_velocity not affected", la_body.angular_velocity ~ 0)
			end
		end

	test_apply_angular_impulse
			-- Test routine for {B2D_BODY}.apply_angular_impulse
		note
			testing:  "covers/{B2D_BODY}.apply_angular_impulse"
		local
			l_shape: B2D_SHAPE_POLYGON
		do
			if attached world.last_body as la_body then
				create l_shape.make_box (12, 12)
				la_body.create_fixture_from_shape (l_shape, 1)
				la_body.apply_angular_impulse (12.25)
				assert ("angular_velocity changed", la_body.angular_velocity /~ 0)
			end
		end

	test_mass
			-- Test routine for {B2D_BODY}.mass
		note
			testing:  "covers/{B2D_BODY}.mass"
		do
			if attached world.last_body as la_body then
				assert ("To be implemented", False)
			end
		end

	test_inertia
			-- Test routine for {B2D_BODY}.inertia
		note
			testing:  "covers/{B2D_BODY}.inertia"
		do
			if attached world.last_body as la_body then
				assert ("To be implemented", False)
			end
		end

	test_local_point_to_world_coordinates
			-- Test routine for {B2D_BODY}.local_point_to_world_coordinates
		note
			testing:  "covers/{B2D_BODY}.local_point_to_world_coordinates"
		local
			l_origin: B2D_VECTOR_2D
			l_local_point: B2D_VECTOR_2D
			l_expeted: B2D_VECTOR_2D
		do
			if attached world.last_body as la_body then
				create l_origin.make_with_coordinates (10, 10)
				la_body.set_transform_with_vector (l_origin, 0)
				create l_local_point.make_with_coordinates (-2, -5)
				create l_expeted.make_with_coordinates (8, 5)
				assert ("local_point_to_world_coordinates valid",
					la_body.local_point_to_world_coordinates_with_vector (l_local_point) ~ l_expeted)
			end
		end

	test_rotated_local_point
			-- Test routine for {B2D_BODY}.rotated_local_point
		note
			testing:  "covers/{B2D_BODY}.rotated_local_point"
		local
			l_angle: REAL_64
			l_point: B2D_VECTOR_2D
			l_rotated_point: B2D_VECTOR_2D
		do
			if attached world.last_body as la_body then
				create l_point.make_with_coordinates (2, 5)
				l_angle := -{MATH_CONST}.pi / 2 -- 90� clockwise
				la_body.set_transform_with_vector (create {B2D_VECTOR_2D}, l_angle.truncated_to_real)
				l_rotated_point := la_body.rotated_local_point_with_vector (l_point)
				assert ("rotated_local_point valid",
					compare_real_32 (l_rotated_point.x, 5) and compare_real_32 (l_rotated_point.y, -2))
			end
		end

	test_world_point_to_local_coordinates
			-- Test routine for {B2D_BODY}.world_point_to_local_coordinates
		note
			testing:  "covers/{B2D_BODY}.world_point_to_local_coordinates"
		local
			l_origin: B2D_VECTOR_2D
			l_world_point: B2D_VECTOR_2D
			l_expected: B2D_VECTOR_2D
		do
			if attached world.last_body as la_body then
				create l_origin.make_with_coordinates (10, 10)
				la_body.set_transform_with_vector (l_origin, 0)
				create l_world_point.make_with_coordinates (5, 15)
				create l_expected.make_with_coordinates (-5, 5)
				assert ("world_point_to_local_coordinates valid",
					la_body.world_point_to_local_coordinates_with_vector (l_world_point) ~�l_expected)
			end
		end

	test_linear_velocity_from_world_point
			-- Test routine for {B2D_BODY}.linear_velocity_from_world_point
		note
			testing:  "covers/{B2D_BODY}.linear_velocity_from_world_point"
		do
			if attached world.last_body as la_body then
				assert ("To be implemented", False)
			end
		end

	test_linear_velocity_from_local_point
			-- Test routine for {B2D_BODY}.linear_velocity_from_local_point
		note
			testing:  "covers/{B2D_BODY}.linear_velocity_from_local_point"
		do
			if attached world.last_body as la_body then
				assert ("To be implemented", False)
			end
		end

	test_linear_damping
			-- Test routine for {B2D_BODY}.set_linear_damping
		note
			testing:
				"covers/{B2D_BODY}.set_linear_damping",
				"covers/{B2D_BODY}.linear_damping"
		do
			if attached world.last_body as la_body then
				la_body.set_linear_damping (0.125)
				assert ("set_linear_damping valid", la_body.linear_damping ~ 0.125)
			end
		end

	test_angular_damping
			-- Test routine for {B2D_BODY}..set_angular_damping
		note
			testing:
				"covers/{B2D_BODY}.set_angular_damping",
				"covers/{B2D_BODY}.angular_damping"
		do
			if attached world.last_body as la_body then
				la_body.set_angular_damping (0.125)
				assert ("set_angular_damping valid", la_body.angular_damping ~�0.125)
			end
		end

	test_gravity_scale
			-- Test routine for {B2D_BODY}.set_gravity_scale
		note
			testing:
				"covers/{B2D_BODY}.set_gravity_scale",
				"covers/{B2D_BODY}.gravity_scale"
		do
			if attached world.last_body as la_body then
				la_body.set_gravity_scale (1.25)
				assert ("set_gravity_scale valid", la_body.gravity_scale ~ 1.25)
			end
		end

	test_is_bullet
			-- Test routine for {B2D_BODY}.set_is_bullet
		note
			testing:
				"covers/{B2D_BODY}.set_is_bullet",
				"covers/{B2D_BODY}.is_bullet"
		do
			if attached world.last_body as la_body then
				la_body.set_is_bullet (True)
				assert ("set_is_bullet valid", la_body.is_bullet)
				la_body.set_is_bullet (False)
				assert ("set_is_bullet valid", not la_body.is_bullet)
				la_body.enable_bullet
				assert ("enable_bullet valid", la_body.is_bullet)
				la_body.disable_bullet
				assert ("disable_bullet valid", not la_body.is_bullet)
			end
		end

	test_allow_sleep_1
			-- Test routine for {B2D_BODY}.allow_sleep
		note
			testing:
				"covers/{B2D_BODY}.set_allow_sleep",
				"covers/{B2D_BODY}.allow_sleep",
				"covers/{B2D_BODY}.is_awake"
		do
			if attached world.last_body as la_body then
				assert ("Awake at creation", la_body.is_awake)
				world.step
				assert ("Asleep after a step", not la_body.is_awake)
				assert ("is_sleeping_allowed True", la_body.allow_sleep)
				la_body.set_allow_sleep (False)
				assert ("is_sleeping_allowed False", not la_body.allow_sleep)
				assert ("Awake before second step", la_body.is_awake)
				world.step
				assert ("Still awake after second step", la_body.is_awake)
			end
		end

	test_allow_sleep_2
			-- Test routine for {B2D_BODY}.allow_sleep
		note
			testing:
				"covers/{B2D_BODY}.set_allow_sleep",
				"covers/{B2D_BODY}.allow_sleep",
				"covers/{B2D_BODY}.is_awake"
		do
			if attached world.last_body as la_body then
				la_body.set_allow_sleep (True)
				assert ("set_allow_sleep Valid", la_body.allow_sleep)
				la_body.disable_allow_sleep
				assert ("disable_allow_sleep Valid", not la_body.allow_sleep)
				la_body.enable_allow_sleep
				assert ("enable_allow_sleep Valid", la_body.allow_sleep)
			end
		end

	test_is_active
			-- Test routine for {B2D_BODY}.set_is_active
		note
			testing:
				"covers/{B2D_BODY}.set_is_active",
				"covers/{B2D_BODY}.is_active"
		do
			if attached world.last_body as la_body then
				assert ("Is active default", la_body.is_active)
				la_body.set_is_active (False)
				assert ("set_is_active valid", not la_body.is_active)
				la_body.set_is_active (True)
				assert ("set_is_active valid", la_body.is_active)
				la_body.deactivate
				assert ("set_is_active valid", not la_body.is_active)
				la_body.activate
				assert ("set_is_active valid", la_body.is_active)
			end
		end

	test_is_fixed_rotation
			-- Test routine for {B2D_BODY}.set_is_fixed_rotation
		note
			testing:
				"covers/{B2D_BODY}.set_is_fixed_rotation",
				"covers/{B2D_BODY}.is_fixed_rotation"
		local
			l_shape: B2D_SHAPE_POLYGON
		do
			if attached world.last_body as la_body then
				la_body.set_is_fixed_rotation (True)
				assert ("set_is_fixed_rotation valid", la_body.is_fixed_rotation)
				create l_shape.make_box (5, 5)
				la_body.create_fixture_from_shape (l_shape, 1)
				la_body.apply_angular_impulse (52)
				assert ("Won't rotate", la_body.angular_velocity ~ 0)
				la_body.set_is_fixed_rotation (False)
				assert ("set_is_fixed_rotation valid", not la_body.is_fixed_rotation)
				la_body.enable_fixed_rotation
				assert ("enable_fixed_rotation valid", la_body.is_fixed_rotation)
				la_body.disable_fixed_rotation
				assert ("disable_fixed_rotation valid", not la_body.is_fixed_rotation)
			end
		end

	test_joints
			-- Test routine for {B2D_BODY}.joints
		note
			testing:  "covers/{B2D_BODY}.joints"
		do
			if attached world.last_body as la_body then
				assert ("To be implemented", False)
			end
		end

	test_contacts
			-- Test routine for {B2D_BODY}.contacts
		note
			testing:  "covers/{B2D_BODY}.contacts"
		do
			if attached world.last_body as la_body then
				assert ("To be implemented", False)
			end
		end

	test_is_static
			-- Test routine for {B2D_BODY}.is_static
		note
			testing:  "covers/{B2D_BODY}.is_static"
		local
			l_definition: B2D_BODY_DEFINITION
		do
			create l_definition.make_static
			world.create_body (l_definition)
			if attached world.last_body as la_body then
				assert ("is_static valid", la_body.is_static)
			else
				assert ("Not tested", False)
			end
		end

	test_is_kinematic
			-- Test routine for {B2D_BODY}.is_kinematic
		note
			testing:  "covers/{B2D_BODY}.is_kinematic"
		local
			l_definition: B2D_BODY_DEFINITION
		do
			create l_definition.make_kinematic
			world.create_body (l_definition)
			if attached world.last_body as la_body then
				assert ("is_kinematic valid", la_body.is_kinematic)
			else
				assert ("Not tested", False)
			end
		end

	test_is_dynamic
			-- Test routine for {B2D_BODY}.is_dynamic
		note
			testing:  "covers/{B2D_BODY}.is_dynamic"
		do
			if attached world.last_body as la_body then
				assert ("is_dynamic valid", la_body.is_dynamic)
			end
		end

feature {NONE} -- Implementation

	world: B2D_WORLD
			-- The world that the {B2D_BODY}s will be tested in.

end


