note
	description: "Tests for {B2D_SHAPE_CHAIN}"
	author: "Louis Marchand"
	date: "Thu, 18 Jun 2020 19:25:33 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_SHAPE_CHAIN_TESTS

inherit
	EQA_TEST_SET
	B2D_COMPARE_REAL
		undefine
			default_create
		end
	B2D_ANY
		undefine
			default_create
		end

feature -- Test routines

	test_make_normal_1
			-- Normal test 1 for `make'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make", "covers/{B2D_SHAPE_CHAIN}.count", "covers/{B2D_SHAPE_CHAIN}.vertices"
		local
			l_shape:B2D_SHAPE_CHAIN
			l_vertices:LIST[B2D_VECTOR_2D]
		do
			create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-0.666666666666667, {REAL_32}4.55555555555555],
									[{REAL_32}13.1111111111111, {REAL_32}2.22222222222223],
									[{REAL_32}10.0, {REAL_32}-3.0],
									[{REAL_32}1.6666666666667, {REAL_32}-0.666666666666667]
								>>)
			assert ("make Valid: count", l_shape.count ~ 5)
			l_vertices := l_shape.vertices
			assert ("make Valid: vertices[1].x", l_vertices[1].x ~ -5.333333333333333)
			assert ("make Valid: vertices[1].y", l_vertices[1].y ~ -5.0)
			assert ("make Valid: vertices[2].x", l_vertices[2].x ~ -0.666666666666667)
			assert ("make Valid: vertices[2].y", l_vertices[2].y ~ 4.55555555555555)
			assert ("make Valid: vertices[3].x", l_vertices[3].x ~ 13.1111111111111)
			assert ("make Valid: vertices[3].y", l_vertices[3].y ~ 2.22222222222223)
			assert ("make Valid: vertices[4].x", l_vertices[4].x ~ 10.0)
			assert ("make Valid: vertices[4].y", l_vertices[4].y ~ -3.0)
			assert ("make Valid: vertices[5].x", l_vertices[5].x ~ 1.6666666666667)
			assert ("make Valid: vertices[5].y", l_vertices[5].y ~ -0.666666666666667)
		end

	test_make_normal_2
			-- Normal test 2 for `make'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make", "covers/{B2D_SHAPE_CHAIN}.exists"
		local
			l_shape:B2D_SHAPE_CHAIN
		do
			create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0]
								>>)
			assert ("make Valid: Exists", not l_shape.exists)
		end

	test_make_wrong_1
			-- Wrong test 1 for `make'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
		do
			if not l_retry then
				create l_shape.make (create {ARRAY[TUPLE[x, y:REAL_32]]}.make_empty)
				assert ("make Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_make_wrong_2
			-- Wrong test 2 for `make'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
		do
			if not l_retry then
				create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0]
								>>)
				assert ("make Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_make_loop_normal_1
			-- Normal test 1 for `make_loop'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_loop", "covers/{B2D_SHAPE_CHAIN}.count", "covers/{B2D_SHAPE_CHAIN}.vertices"
		local
			l_shape:B2D_SHAPE_CHAIN
			l_vertices:LIST[B2D_VECTOR_2D]
		do
			create l_shape.make_loop (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-0.666666666666667, {REAL_32}4.55555555555555],
									[{REAL_32}13.1111111111111, {REAL_32}2.22222222222223],
									[{REAL_32}10.0, {REAL_32}-3.0],
									[{REAL_32}1.6666666666667, {REAL_32}-0.666666666666667]
								>>)
			assert ("make_loop Valid: count", l_shape.count ~ 6)
			l_vertices := l_shape.vertices
			assert ("make_loop Valid: vertices[1].x", l_vertices[1].x ~ -5.333333333333333)
			assert ("make_loop Valid: vertices[1].y", l_vertices[1].y ~ -5.0)
			assert ("make_loop Valid: vertices[2].x", l_vertices[2].x ~ -0.666666666666667)
			assert ("make_loop Valid: vertices[2].y", l_vertices[2].y ~ 4.55555555555555)
			assert ("make_loop Valid: vertices[3].x", l_vertices[3].x ~ 13.1111111111111)
			assert ("make_loop Valid: vertices[3].y", l_vertices[3].y ~ 2.22222222222223)
			assert ("make_loop Valid: vertices[4].x", l_vertices[4].x ~ 10.0)
			assert ("make_loop Valid: vertices[4].y", l_vertices[4].y ~ -3.0)
			assert ("make_loop Valid: vertices[5].x", l_vertices[5].x ~ 1.6666666666667)
			assert ("make_loop Valid: vertices[5].y", l_vertices[5].y ~ -0.666666666666667)
			assert ("make_loop Valid: vertices[6].x", l_vertices[6].x ~ -5.333333333333333)
			assert ("make_loop Valid: vertices[6].y", l_vertices[6].y ~ -5.0)
		end

	test_make_loop_normal_2
			-- Normal test 2 for `make_loop'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_loop", "covers/{B2D_SHAPE_CHAIN}.exists"
		local
			l_shape:B2D_SHAPE_CHAIN
		do
			create l_shape.make_loop (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0]
								>>)
			assert ("make_loop Valid: Exists", not l_shape.exists)
		end

	test_make_loop_wrong_1
			-- Wrong test 1 for `make_loop'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_loop"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
		do
			if not l_retry then
				create l_shape.make_loop (create {ARRAY[TUPLE[x, y:REAL_32]]}.make_empty)
				assert ("make_loop Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_make_loop_wrong_2
			-- Wrong test 2 for `make_loop'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_loop"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
		do
			if not l_retry then
				create l_shape.make_loop (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0]
								>>)
				assert ("make_loop Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_make_with_vectors_normal_1
			-- Normal test 1 for `make_with_vectors'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_with_vectors", "covers/{B2D_SHAPE_CHAIN}.count", "covers/{B2D_SHAPE_CHAIN}.vertices"
		local
			l_shape:B2D_SHAPE_CHAIN
			l_vertices:LIST[B2D_VECTOR_2D]
		do
			create l_shape.make_with_vectors (<<
									create {B2D_VECTOR_2D}.make_with_coordinates (-5.333333333333333, -5.0),
									create {B2D_VECTOR_2D}.make_with_coordinates (-0.666666666666667, 4.55555555555555),
									create {B2D_VECTOR_2D}.make_with_coordinates (13.1111111111111, 2.22222222222223),
									create {B2D_VECTOR_2D}.make_with_coordinates (10.0, -3.0),
									create {B2D_VECTOR_2D}.make_with_coordinates (1.6666666666667, -0.666666666666667)
								>>)
			assert ("make Valid: count", l_shape.count ~ 5)
			l_vertices := l_shape.vertices
			assert ("make Valid: vertices[1].x", l_vertices[1].x ~ -5.333333333333333)
			assert ("make Valid: vertices[1].y", l_vertices[1].y ~ -5.0)
			assert ("make Valid: vertices[2].x", l_vertices[2].x ~ -0.666666666666667)
			assert ("make Valid: vertices[2].y", l_vertices[2].y ~ 4.55555555555555)
			assert ("make Valid: vertices[3].x", l_vertices[3].x ~ 13.1111111111111)
			assert ("make Valid: vertices[3].y", l_vertices[3].y ~ 2.22222222222223)
			assert ("make Valid: vertices[4].x", l_vertices[4].x ~ 10.0)
			assert ("make Valid: vertices[4].y", l_vertices[4].y ~ -3.0)
			assert ("make Valid: vertices[5].x", l_vertices[5].x ~ 1.6666666666667)
			assert ("make Valid: vertices[5].y", l_vertices[5].y ~ -0.666666666666667)
		end

	test_make_with_vectors_normal_2
			-- Normal test 2 for `make_with_vectors'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_with_vectors", "covers/{B2D_SHAPE_CHAIN}.exists"
		local
			l_shape:B2D_SHAPE_CHAIN
		do
			create l_shape.make_with_vectors (<<
									create {B2D_VECTOR_2D}.make_with_coordinates (-5.333333333333333, -5.0),
									create {B2D_VECTOR_2D}.make_with_coordinates (-5.333333333333333, -5.0)
								>>)
			assert ("make Valid: Exists", not l_shape.exists)
		end

	test_make_with_vectors_wrong_1
			-- Wrong test 1 for `make_with_vectors'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_with_vectors"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
		do
			if not l_retry then
				create l_shape.make_with_vectors (create {ARRAY[B2D_VECTOR_2D]}.make_empty)
				assert ("make_with_vectors Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_make_with_vectors_wrong_2
			-- Wrong test 2 for `make'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_with_vectors"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
		do
			if not l_retry then
				create l_shape.make_with_vectors (<<
									create {B2D_VECTOR_2D}.make_with_coordinates (-5.333333333333333, -5.0)
								>>)
				assert ("make_with_vectors Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_make_loop_with_vectors_normal_1
			-- Normal test 1 for `make_loop_with_vectors'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_loop_with_vectors", "covers/{B2D_SHAPE_CHAIN}.count", "covers/{B2D_SHAPE_CHAIN}.vertices"
		local
			l_shape:B2D_SHAPE_CHAIN
			l_vertices:LIST[B2D_VECTOR_2D]
		do
			create l_shape.make_loop_with_vectors (<<
									create {B2D_VECTOR_2D}.make_with_coordinates (-5.333333333333333, -5.0),
									create {B2D_VECTOR_2D}.make_with_coordinates (-0.666666666666667, 4.55555555555555),
									create {B2D_VECTOR_2D}.make_with_coordinates (13.1111111111111, 2.22222222222223),
									create {B2D_VECTOR_2D}.make_with_coordinates (10.0, -3.0),
									create {B2D_VECTOR_2D}.make_with_coordinates (1.6666666666667, -0.666666666666667)
								>>)
			assert ("make_loop_with_vectors Valid: count", l_shape.count ~ 6)
			l_vertices := l_shape.vertices
			assert ("make_loop_with_vectors Valid: vertices[1].x", l_vertices[1].x ~ -5.333333333333333)
			assert ("make_loop_with_vectors Valid: vertices[1].y", l_vertices[1].y ~ -5.0)
			assert ("make_loop_with_vectors Valid: vertices[2].x", l_vertices[2].x ~ -0.666666666666667)
			assert ("make_loop_with_vectors Valid: vertices[2].y", l_vertices[2].y ~ 4.55555555555555)
			assert ("make_loop_with_vectors Valid: vertices[3].x", l_vertices[3].x ~ 13.1111111111111)
			assert ("make_loop_with_vectors Valid: vertices[3].y", l_vertices[3].y ~ 2.22222222222223)
			assert ("make_loop_with_vectors Valid: vertices[4].x", l_vertices[4].x ~ 10.0)
			assert ("make_loop_with_vectors Valid: vertices[4].y", l_vertices[4].y ~ -3.0)
			assert ("make_loop_with_vectors Valid: vertices[5].x", l_vertices[5].x ~ 1.6666666666667)
			assert ("make_loop_with_vectors Valid: vertices[5].y", l_vertices[5].y ~ -0.666666666666667)
			assert ("make_loop_with_vectors Valid: vertices[6].x", l_vertices[6].x ~ -5.333333333333333)
			assert ("make_loop_with_vectors Valid: vertices[6].y", l_vertices[6].y ~ -5.0)
		end

	test_make_loop_with_vectors_normal_2
			-- Normal test 2 for `make_loop_with_vectors'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_loop_with_vectors", "covers/{B2D_SHAPE_CHAIN}.exists"
		local
			l_shape:B2D_SHAPE_CHAIN
		do
			create l_shape.make_loop_with_vectors (<<
									create {B2D_VECTOR_2D}.make_with_coordinates (-5.333333333333333, -5.0),
									create {B2D_VECTOR_2D}.make_with_coordinates (-5.333333333333333, -5.0)
								>>)
			assert ("make_loop_with_vectors Valid: Exists", not l_shape.exists)
		end

	test_make_loop_with_vectors_wrong_1
			-- Normal test 1 for `make_loop_with_vectors'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_loop_with_vectors"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
		do
			if not l_retry then
				create l_shape.make_loop_with_vectors (<<create {B2D_VECTOR_2D}.make_with_coordinates (-5.333333333333333, -5.0)>>)
				assert ("make_loop_with_vectors Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_make_loop_with_vectors_wrong_2
			-- Wrong test 2 for `make_loop_with_vectors'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_loop_with_vectors"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
		do
			if not l_retry then
				create l_shape.make_loop_with_vectors (create {ARRAY[B2D_VECTOR_2D]}.make_empty)
				assert ("make_loop_with_vectors Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_edge_normal_1
			-- Normal test 1 for `edge'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_with_vectors", "covers/{B2D_SHAPE_CHAIN}.edge_count", "covers/{B2D_SHAPE_CHAIN}.edge"
		local
			l_shape:B2D_SHAPE_CHAIN
		do
			create l_shape.make_with_vectors (<<
									create {B2D_VECTOR_2D}.make_with_coordinates (-5.333333333333333, -5.0),
									create {B2D_VECTOR_2D}.make_with_coordinates (-0.666666666666667, 4.55555555555555),
									create {B2D_VECTOR_2D}.make_with_coordinates (13.1111111111111, 2.22222222222223),
									create {B2D_VECTOR_2D}.make_with_coordinates (10.0, -3.0),
									create {B2D_VECTOR_2D}.make_with_coordinates (1.6666666666667, -0.666666666666667)
								>>)
			assert ("edge_count Valid", l_shape.edge_count ~ 4)
			assert ("edge Valid: l_shape.edge (1).start_vertex.x", l_shape.edge (1).start_vertex.x ~ -5.333333333333333)
			assert ("edge Valid: l_shape.edge (1).start_vertex.y", l_shape.edge (1).start_vertex.y ~ -5.0)
			assert ("edge Valid: l_shape.edge (1).end_vertex.x", l_shape.edge (1).end_vertex.x ~ -0.666666666666667)
			assert ("edge Valid: l_shape.edge (1).end_vertex.y", l_shape.edge (1).end_vertex.y ~ 4.55555555555555)
			assert ("edge Valid: l_shape.edge (1).has_before_vertex", not l_shape.edge (1).has_before_vertex)
			assert ("edge Valid: l_shape.edge (1).has_after_vertex", l_shape.edge (1).has_after_vertex)
			assert ("edge Valid: l_shape.edge (1).after_vertex.x", l_shape.edge (1).after_vertex.x ~ 13.1111111111111)
			assert ("edge Valid: l_shape.edge (1).after_vertex.y", l_shape.edge (1).after_vertex.y ~ 2.22222222222223)
			assert ("edge Valid: l_shape.edge (2).start_vertex.x", l_shape.edge (2).start_vertex.x ~ -0.666666666666667)
			assert ("edge Valid: l_shape.edge (2).start_vertex.y", l_shape.edge (2).start_vertex.y ~ 4.55555555555555)
			assert ("edge Valid: l_shape.edge (2).end_vertex.x", l_shape.edge (2).end_vertex.x ~ 13.1111111111111)
			assert ("edge Valid: l_shape.edge (2).end_vertex.y", l_shape.edge (2).end_vertex.y ~ 2.22222222222223)
			assert ("edge Valid: l_shape.edge (2).has_before_vertex", l_shape.edge (2).has_before_vertex)
			assert ("edge Valid: l_shape.edge (2).before_vertex.x", l_shape.edge (2).before_vertex.x ~ -5.333333333333333)
			assert ("edge Valid: l_shape.edge (2).before_vertex.y", l_shape.edge (2).before_vertex.y ~ -5.0)
			assert ("edge Valid: l_shape.edge (2).has_after_vertex", l_shape.edge (2).has_after_vertex)
			assert ("edge Valid: l_shape.edge (2).after_vertex.x", l_shape.edge (2).after_vertex.x ~ 10.0)
			assert ("edge Valid: l_shape.edge (2).after_vertex.y", l_shape.edge (2).after_vertex.y ~ -3.0)
			assert ("edge Valid: l_shape.edge (3).start_vertex.x", l_shape.edge (3).start_vertex.x ~ 13.1111111111111)
			assert ("edge Valid: l_shape.edge (3).start_vertex.y", l_shape.edge (3).start_vertex.y ~ 2.22222222222223)
			assert ("edge Valid: l_shape.edge (3).start_vertex.x", l_shape.edge (3).end_vertex.x ~ 10.0)
			assert ("edge Valid: l_shape.edge (3).start_vertex.y", l_shape.edge (3).end_vertex.y ~ -3.0)
			assert ("edge Valid: l_shape.edge (3).has_before_vertex", l_shape.edge (3).has_before_vertex)
			assert ("edge Valid: l_shape.edge (3).before_vertex.x", l_shape.edge (3).before_vertex.x ~ -0.666666666666667)
			assert ("edge Valid: l_shape.edge (3).before_vertex.y", l_shape.edge (3).before_vertex.y ~ 4.55555555555555)
			assert ("edge Valid: l_shape.edge (3).has_after_vertex", l_shape.edge (3).has_after_vertex)
			assert ("edge Valid: l_shape.edge (3).after_vertex.x", l_shape.edge (3).after_vertex.x ~ 1.6666666666667)
			assert ("edge Valid: l_shape.edge (3).after_vertex.y", l_shape.edge (3).after_vertex.y ~ -0.666666666666667)
			assert ("edge Valid: l_shape.edge (4).start_vertex.x", l_shape.edge (4).start_vertex.x ~ 10.0)
			assert ("edge Valid: l_shape.edge (4).start_vertex.y", l_shape.edge (4).start_vertex.y ~ -3.0)
			assert ("edge Valid: l_shape.edge (4).end_vertex.x", l_shape.edge (4).end_vertex.x ~ 1.6666666666667)
			assert ("edge Valid: l_shape.edge (4).end_vertex.y", l_shape.edge (4).end_vertex.y ~ -0.666666666666667)
			assert ("edge Valid: l_shape.edge (4).has_before_vertex", l_shape.edge (4).has_before_vertex)
			assert ("edge Valid: l_shape.edge (4).before_vertex.x", l_shape.edge (4).before_vertex.x ~ 13.1111111111111)
			assert ("edge Valid: l_shape.edge (4).before_vertex.y", l_shape.edge (4).before_vertex.y ~ 2.22222222222223)
			assert ("edge Valid: l_shape.edge (4).has_after_vertex", not l_shape.edge (4).has_after_vertex)
		end

	test_edge_limit
			-- Limit test for `edge'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_with_vectors", "covers/{B2D_SHAPE_CHAIN}.edge_count", "covers/{B2D_SHAPE_CHAIN}.edge"
		local
			l_shape:B2D_SHAPE_CHAIN
		do
			create l_shape.make_with_vectors (<<
									create {B2D_VECTOR_2D}.make_with_coordinates (-5.333333333333333, -5.0),
									create {B2D_VECTOR_2D}.make_with_coordinates (-0.666666666666667, 4.55555555555555)
								>>)
			assert ("edge_count Valid", l_shape.edge_count ~ 1)
			assert ("edge Valid: l_shape.edge (1).start_vertex.x", l_shape.edge (1).start_vertex.x ~ -5.333333333333333)
			assert ("edge Valid: l_shape.edge (1).start_vertex.y", l_shape.edge (1).start_vertex.y ~ -5.0)
			assert ("edge Valid: l_shape.edge (1).end_vertex.x", l_shape.edge (1).end_vertex.x ~ -0.666666666666667)
			assert ("edge Valid: l_shape.edge (1).end_vertex.y", l_shape.edge (1).end_vertex.y ~ 4.55555555555555)
			assert ("edge Valid: l_shape.edge (1).has_before_vertex", not l_shape.edge (1).has_before_vertex)
			assert ("edge Valid: l_shape.edge (1).has_after_vertex", not l_shape.edge (1).has_after_vertex)
		end

	test_edge_count_wrong
			-- Wrong test for `edge_count'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_loop_with_vectors", "covers/{B2D_SHAPE_CHAIN}.edge_count"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
			l_result : INTEGER
		do
			if not l_retry then
				create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0]
								>>)
				l_result := l_shape.edge_count
				assert ("edge_count Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_count_wrong
			-- Wrong test for `count'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_loop_with_vectors", "covers/{B2D_SHAPE_CHAIN}.count"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
			l_result : INTEGER
		do
			if not l_retry then
				create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0]
								>>)
				l_result := l_shape.count
				assert ("count Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_vertices_wrong
			-- Wrong test for `vertices'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_loop_with_vectors", "covers/{B2D_SHAPE_CHAIN}.vertices"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
			l_result : LIST[B2D_VECTOR_2D]
		do
			if not l_retry then
				create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0]
								>>)
				l_result := l_shape.vertices
				assert ("vertices Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_edges_wrong
			-- Wrong test for `edges'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_loop_with_vectors", "covers/{B2D_SHAPE_CHAIN}.edges"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
			l_result : LIST[B2D_SHAPE_EDGE_READABLE]
		do
			if not l_retry then
				create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0]
								>>)
				l_result := l_shape.edges
				assert ("edges Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_edge_wrong
			-- Wrong test for `edge'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_loop_with_vectors", "covers/{B2D_SHAPE_CHAIN}.edge"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
			l_result : B2D_SHAPE_EDGE_READABLE
		do
			if not l_retry then
				create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0]
								>>)
				l_result := l_shape.edge(1)
				assert ("edge Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_has_before_vertex_wrong
			-- Wrong test for `has_before_vertex'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_loop_with_vectors", "covers/{B2D_SHAPE_CHAIN}.has_before_vertex"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
			l_result : BOOLEAN
		do
			if not l_retry then
				create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0]
								>>)
				l_result := l_shape.has_before_vertex
				assert ("has_before_vertex Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_has_after_vertex_wrong
			-- Wrong test for `has_after_vertex'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_loop_with_vectors", "covers/{B2D_SHAPE_CHAIN}.has_after_vertex"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
			l_result : BOOLEAN
		do
			if not l_retry then
				create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0]
								>>)
				l_result := l_shape.has_after_vertex
				assert ("has_after_vertex Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_before_vertex_wrong_1
			-- Wrong test 1 for `before_vertex'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_loop_with_vectors", "covers/{B2D_SHAPE_CHAIN}.before_vertex"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
			l_result : B2D_VECTOR_2D
		do
			if not l_retry then
				create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0]
								>>)
				l_result := l_shape.before_vertex
				assert ("before_vertex Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_after_vertex_wrong_1
			-- Wrong test 2 for `after_vertex'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_loop_with_vectors", "covers/{B2D_SHAPE_CHAIN}.after_vertex"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
			l_result : B2D_VECTOR_2D
		do
			if not l_retry then
				create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0]
								>>)
				l_result := l_shape.after_vertex
				assert ("after_vertex Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_before_vertex_wrong
			-- Wrong test for `set_before_vertex'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_loop_with_vectors", "covers/{B2D_SHAPE_CHAIN}.set_before_vertex"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
		do
			if not l_retry then
				create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0]
								>>)
				l_shape.set_before_vertex(10, 12)
				assert ("set_before_vertex Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_after_vertex_wrong
			-- Wrong test for `set_after_vertex'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_loop_with_vectors", "covers/{B2D_SHAPE_CHAIN}.set_after_vertex"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
		do
			if not l_retry then
				create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0]
								>>)
				l_shape.set_after_vertex(10, 12)
				assert ("set_after_vertex Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_before_vertex_with_vector_wrong
			-- Wrong test for `set_before_vertex_with_vector'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_loop_with_vectors", "covers/{B2D_SHAPE_CHAIN}.set_before_vertex_with_vector"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
		do
			if not l_retry then
				create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0]
								>>)
				l_shape.set_before_vertex_with_vector(create {B2D_VECTOR_2D}.make_with_coordinates (10, 20))
				assert ("set_before_vertex_with_vector Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_after_vertex_with_vector_wrong_1
			-- Wrong test 1 for `set_before_vertex_with_vector'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_loop_with_vectors", "covers/{B2D_SHAPE_CHAIN}.set_after_vertex_with_vector"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
		do
			if not l_retry then
				create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0]
								>>)
				l_shape.set_after_vertex_with_vector(create {B2D_VECTOR_2D}.make_with_coordinates (10, 20))
				assert ("set_after_vertex_with_vector Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_after_vertex_with_vector_wrong_2
			-- Wrong 2 test for `set_before_vertex_with_vector'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make", "covers/{B2D_SHAPE_CHAIN}.set_after_vertex_with_vector"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
		do
			if not l_retry then
				create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-0.666666666666667, {REAL_32}4.55555555555555],
									[{REAL_32}13.1111111111111, {REAL_32}2.22222222222223],
									[{REAL_32}10.0, {REAL_32}-3.0],
									[{REAL_32}1.6666666666667, {REAL_32}-0.666666666666667]
								>>)
				l_shape.set_after_vertex_with_vector(create {B2D_VECTOR_2D}.own_from_item (create {POINTER}))
				assert ("set_after_vertex_with_vector Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_before_vertex_normal
			-- Normal test for `before_vertex'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make", "covers/{B2D_SHAPE_CHAIN}.before_vertex", "covers/{B2D_SHAPE_CHAIN}.set_before_vertex", "covers/{B2D_SHAPE_CHAIN}.clear_before_vertex"
		local
			l_shape:B2D_SHAPE_CHAIN
			l_vertex:B2D_VECTOR_2D
		do
			create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-0.666666666666667, {REAL_32}4.55555555555555],
									[{REAL_32}13.1111111111111, {REAL_32}2.22222222222223],
									[{REAL_32}10.0, {REAL_32}-3.0],
									[{REAL_32}1.6666666666667, {REAL_32}-0.666666666666667]
								>>)
			l_shape.clear_before_vertex
			create l_vertex.make_with_coordinates (-2.52443, -8.83484)
			l_shape.set_before_vertex_with_vector (l_vertex)
			assert ("set_before_vertex_with_vector not valid: has_before_vertex", l_shape.has_before_vertex)
			assert ("set_before_vertex_with_vector not valid: before_vertex", l_shape.before_vertex ~ l_vertex)
			l_shape.clear_before_vertex
			assert ("clear_before_vertex not valid: has_before_vertex", not l_shape.has_before_vertex)
			l_shape.set_before_vertex (-2.52443, -8.83484)
			assert ("set_before_vertex not valid: has_before_vertex", l_shape.has_before_vertex)
			assert ("set_before_vertex not valid: before_vertex.x", l_shape.before_vertex.x ~ -2.52443)
			assert ("set_before_vertex not valid: before_vertex.y", l_shape.before_vertex.y ~ -8.83484)
		end

	test_before_vertex_limit
			-- Limit test for `before_vertex'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make", "covers/{B2D_SHAPE_CHAIN}.before_vertex", "covers/{B2D_SHAPE_CHAIN}.set_before_vertex", "covers/{B2D_SHAPE_CHAIN}.clear_before_vertex"
		local
			l_shape:B2D_SHAPE_CHAIN
			l_vertex:B2D_VECTOR_2D
		do
			create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-0.666666666666667, {REAL_32}4.55555555555555],
									[{REAL_32}13.1111111111111, {REAL_32}2.22222222222223],
									[{REAL_32}10.0, {REAL_32}-3.0],
									[{REAL_32}1.6666666666667, {REAL_32}-0.666666666666667]
								>>)
			create l_vertex.make_with_coordinates (-4.36659, 5.01439)
			l_shape.set_before_vertex_with_vector (l_vertex)
			assert ("set_before_vertex_with_vector not valid: before_vertex", l_shape.before_vertex ~ l_vertex)
		end

	test_before_vertex_wrong_2
			-- Wrong test 2 for `before_vertex'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make", "covers/{B2D_SHAPE_CHAIN}.before_vertex", "covers/{B2D_SHAPE_CHAIN}.set_before_vertex", "covers/{B2D_SHAPE_CHAIN}.clear_before_vertex"
		local
			l_retry:BOOLEAN
			l_result:B2D_VECTOR_2D
			l_shape:B2D_SHAPE_CHAIN
		do
			if not l_retry then
				create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-0.666666666666667, {REAL_32}4.55555555555555],
									[{REAL_32}13.1111111111111, {REAL_32}2.22222222222223],
									[{REAL_32}10.0, {REAL_32}-3.0],
									[{REAL_32}1.6666666666667, {REAL_32}-0.666666666666667]
								>>)
				l_shape.clear_before_vertex
				l_result := l_shape.before_vertex
				assert("before_vertex Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_before_vertex_with_vector_wrong_2
			-- Wrong 2 test for `set_before_vertex_with_vector'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make", "covers/{B2D_SHAPE_CHAIN}.set_before_vertex_with_vector"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
		do
			if not l_retry then
				create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-0.666666666666667, {REAL_32}4.55555555555555],
									[{REAL_32}13.1111111111111, {REAL_32}2.22222222222223],
									[{REAL_32}10.0, {REAL_32}-3.0],
									[{REAL_32}1.6666666666667, {REAL_32}-0.666666666666667]
								>>)
				l_shape.set_before_vertex_with_vector(create {B2D_VECTOR_2D}.own_from_item (create {POINTER}))
				assert ("set_before_vertex_with_vector Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_after_vertex_normal
			-- Normal test for `after_vertex'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make", "covers/{B2D_SHAPE_CHAIN}.after_vertex", "covers/{B2D_SHAPE_CHAIN}.set_after_vertex", "covers/{B2D_SHAPE_CHAIN}.clear_after_vertex"
		local
			l_shape:B2D_SHAPE_CHAIN
			l_vertex:B2D_VECTOR_2D
		do
			create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-0.666666666666667, {REAL_32}4.55555555555555],
									[{REAL_32}13.1111111111111, {REAL_32}2.22222222222223],
									[{REAL_32}10.0, {REAL_32}-3.0],
									[{REAL_32}1.6666666666667, {REAL_32}-0.666666666666667]
								>>)
			l_shape.clear_after_vertex
			create l_vertex.make_with_coordinates (-2.52443, -8.83484)
			l_shape.set_after_vertex_with_vector (l_vertex)
			assert ("set_after_vertex_with_vector not valid: has_after_vertex", l_shape.has_after_vertex)
			assert ("set_after_vertex_with_vector not valid: after_vertex", l_shape.after_vertex ~ l_vertex)
			l_shape.clear_after_vertex
			assert ("clear_after_vertex not valid: has_after_vertex", not l_shape.has_after_vertex)
			l_shape.set_after_vertex (-2.52443, -8.83484)
			assert ("set_after_vertex not valid: has_after_vertex", l_shape.has_after_vertex)
			assert ("set_after_vertex not valid: after_vertex.x", l_shape.after_vertex.x ~ -2.52443)
			assert ("set_after_vertex not valid: after_vertex.y", l_shape.after_vertex.y ~ -8.83484)
		end

	test_after_vertex_limit
			-- Limit test for `after_vertex'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make", "covers/{B2D_SHAPE_CHAIN}.after_vertex", "covers/{B2D_SHAPE_CHAIN}.set_after_vertex", "covers/{B2D_SHAPE_CHAIN}.clear_after_vertex"
		local
			l_shape:B2D_SHAPE_CHAIN
			l_vertex:B2D_VECTOR_2D
		do
			create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-0.666666666666667, {REAL_32}4.55555555555555],
									[{REAL_32}13.1111111111111, {REAL_32}2.22222222222223],
									[{REAL_32}10.0, {REAL_32}-3.0],
									[{REAL_32}1.6666666666667, {REAL_32}-0.666666666666667]
								>>)
			create l_vertex.make_with_coordinates (-0.67469, 0.33661)
			l_shape.set_after_vertex_with_vector (l_vertex)
			assert ("set_after_vertex_with_vector not valid: after_vertex", l_shape.after_vertex ~ l_vertex)
		end

	test_after_vertex_wrong_2
			-- Wrong test 2 for `after_vertex'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make", "covers/{B2D_SHAPE_CHAIN}.after_vertex", "covers/{B2D_SHAPE_CHAIN}.set_after_vertex", "covers/{B2D_SHAPE_CHAIN}.clear_after_vertex"
		local
			l_retry:BOOLEAN
			l_result:B2D_VECTOR_2D
			l_shape:B2D_SHAPE_CHAIN
		do
			if not l_retry then
				create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-0.666666666666667, {REAL_32}4.55555555555555],
									[{REAL_32}13.1111111111111, {REAL_32}2.22222222222223],
									[{REAL_32}10.0, {REAL_32}-3.0],
									[{REAL_32}1.6666666666667, {REAL_32}-0.666666666666667]
								>>)
				l_shape.clear_after_vertex
				l_result := l_shape.after_vertex
				assert("after_vertex Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_contains_normal
			-- Normal test for `contains_with_vector'
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make", "covers/{B2D_SHAPE_CHAIN}.contains"
		local
			l_shape:B2D_SHAPE_CHAIN
			l_transform:B2D_TRANSFORM
			l_point:B2D_VECTOR_2D
		do
			create l_point.make_with_coordinates (10.1, 5)
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-0.666666666666667, {REAL_32}4.55555555555555],
									[{REAL_32}13.1111111111111, {REAL_32}2.22222222222223],
									[{REAL_32}10.0, {REAL_32}-3.0],
									[{REAL_32}1.6666666666667, {REAL_32}-0.666666666666667]
								>>)
			assert ("contains not valid", not l_shape.contains_with_vector (l_transform, l_point))
		end

	test_ray_cast_normal_1
			-- Limit test 1 for ray_cast
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make", "covers/{B2D_SHAPE_CHAIN}.ray_cast"
		local
			l_shape:B2D_SHAPE_CHAIN
			l_transform:B2D_TRANSFORM
			l_input:B2D_RAY_CAST_INPUT
		do
			create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-0.666666666666667, {REAL_32}4.55555555555555],
									[{REAL_32}13.1111111111111, {REAL_32}2.22222222222223],
									[{REAL_32}10.0, {REAL_32}-3.0],
									[{REAL_32}1.6666666666667, {REAL_32}-0.666666666666667]
								>>)
			create l_input.make_with_coordinates (20, 0, -10, 0, 1)
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			if attached l_shape.ray_cast (l_input, l_transform, 1) as la_output then
				assert("ray_cast Valid. Normal: X", compare_real_32 (la_output.normal.x, 0.898568))
				assert("ray_cast Valid. Normal: Y", compare_real_32 (la_output.normal.y, -0.438835))
				assert("ray_cast Valid. Fraction", compare_real_32 (la_output.fraction, 0.763049))
			else
				assert("ray_cast Valid", False)
			end
		end

	test_ray_cast_normal_2
			-- Limit test 2 for ray_cast
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make", "covers/{B2D_SHAPE_CHAIN}.ray_cast"
		local
			l_shape:B2D_SHAPE_CHAIN
			l_transform:B2D_TRANSFORM
			l_input:B2D_RAY_CAST_INPUT
		do
			create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-0.666666666666667, {REAL_32}4.55555555555555],
									[{REAL_32}13.1111111111111, {REAL_32}2.22222222222223],
									[{REAL_32}10.0, {REAL_32}-3.0],
									[{REAL_32}1.6666666666667, {REAL_32}-0.666666666666667]
								>>)
			create l_input.make_with_coordinates (20, 0, -10, 0, 1)
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			if attached l_shape.ray_cast (l_input, l_transform, 2) as la_output then
				assert("ray_cast Valid", False)
			end
		end

	test_ray_cast_wrong_1
			-- Wrong test 1 for ray_cast
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make", "covers/{B2D_SHAPE_CHAIN}.ray_cast"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
			l_transform:B2D_TRANSFORM
			l_input:B2D_RAY_CAST_INPUT
			l_output: detachable B2D_RAY_CAST_OUTPUT
		do
			if not l_retry then
				create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-0.666666666666667, {REAL_32}4.55555555555555],
									[{REAL_32}13.1111111111111, {REAL_32}2.22222222222223],
									[{REAL_32}10.0, {REAL_32}-3.0],
									[{REAL_32}1.6666666666667, {REAL_32}-0.666666666666667]
								>>)
				create l_input.make_with_coordinates (20, 0, -10, 0, 1)
				create l_transform.make_with_coordinates_and_angle (0, 0, 0)
				l_output := l_shape.ray_cast (l_input, l_transform, 0)
				assert("ray_cast Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_ray_cast_wrong_2
			-- Wrong test 2 for ray_cast
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make", "covers/{B2D_SHAPE_CHAIN}.ray_cast"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
			l_transform:B2D_TRANSFORM
			l_input:B2D_RAY_CAST_INPUT
			l_output: detachable B2D_RAY_CAST_OUTPUT
		do
			if not l_retry then
				create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-0.666666666666667, {REAL_32}4.55555555555555],
									[{REAL_32}13.1111111111111, {REAL_32}2.22222222222223],
									[{REAL_32}10.0, {REAL_32}-3.0],
									[{REAL_32}1.6666666666667, {REAL_32}-0.666666666666667]
								>>)
				create l_input.make_with_coordinates (20, 0, -10, 0, 1)
				create l_transform.make_with_coordinates_and_angle (0, 0, 0)
				l_output := l_shape.ray_cast (l_input, l_transform, 5)
				assert("ray_cast Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_ray_cast_wrong_3
			-- Wrong test 3 for ray_cast
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make", "covers/{B2D_SHAPE_CHAIN}.ray_cast"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
			l_transform:B2D_TRANSFORM
			l_input:B2D_RAY_CAST_INPUT
			l_output: detachable B2D_RAY_CAST_OUTPUT
		do
			if not l_retry then
				create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0]
								>>)
				create l_input.make_with_coordinates (20, 0, -10, 0, 1)
				create l_transform.make_with_coordinates_and_angle (0, 0, 0)
				l_output := l_shape.ray_cast (l_input, l_transform, 1)
				assert("ray_cast Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_mass_normal
			-- Normal test for mass
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make", "covers/{B2D_SHAPE_CHAIN}.mass"
		local
			l_shape:B2D_SHAPE_CHAIN
			l_mass:B2D_MASS
		do
			create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-0.666666666666667, {REAL_32}4.55555555555555],
									[{REAL_32}13.1111111111111, {REAL_32}2.22222222222223],
									[{REAL_32}10.0, {REAL_32}-3.0],
									[{REAL_32}1.6666666666667, {REAL_32}-0.666666666666667]
								>>)
			l_mass := l_shape.mass (10)
			assert("mass Valid: center.x", l_mass.center.x ~ 0)
			assert("mass Valid: center.y", l_mass.center.y ~ 0)
			assert("mass Valid: Mass", compare_real_32 (l_mass.mass, 0))
			assert("mass Valid: inertia", compare_real_32 (l_mass.inertia, 0))
		end

	test_mass_limit
			-- Limit test for mass
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make", "covers/{B2D_SHAPE_CHAIN}.mass"
		local
			l_shape:B2D_SHAPE_CHAIN
			l_mass:B2D_MASS
		do
			create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-0.666666666666667, {REAL_32}4.55555555555555],
									[{REAL_32}13.1111111111111, {REAL_32}2.22222222222223],
									[{REAL_32}10.0, {REAL_32}-3.0],
									[{REAL_32}1.6666666666667, {REAL_32}-0.666666666666667]
								>>)
			l_mass := l_shape.mass (0)
			assert("mass Valid: center.x", l_mass.center.x ~ 0)
			assert("mass Valid: center.y", l_mass.center.y ~ 0)
			assert("mass Valid: Mass", compare_real_32 (l_mass.mass, 0))
			assert("mass Valid: inertia", compare_real_32 (l_mass.inertia, 0))
		end

	test_axis_aligned_bounding_box_normal_1
			-- Normal test 1 for axis_aligned_bounding_box
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_box", "covers/{B2D_SHAPE_CHAIN}.axis_aligned_bounding_box"
		local
			l_shape:B2D_SHAPE_CHAIN
			l_result:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_transform:B2D_TRANSFORM
		do
			create l_transform.make_with_coordinates_and_angle (0, 0, 0)
			create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-0.666666666666667, {REAL_32}4.55555555555555],
									[{REAL_32}13.1111111111111, {REAL_32}2.22222222222223],
									[{REAL_32}10.0, {REAL_32}-3.0],
									[{REAL_32}1.6666666666667, {REAL_32}-0.666666666666667]
								>>)
			l_result := l_shape.axis_aligned_bounding_box (l_transform, 3)
			assert("axis_aligned_bounding_box Valid: lower_bound.x", l_result.lower_bound.x ~ 10)
			assert("axis_aligned_bounding_box Valid: lower_bound.y", l_result.lower_bound.y ~ -3)
			assert("axis_aligned_bounding_box Valid: upper_bound.x", l_result.upper_bound.x ~ 13.1111111111111)
			assert("axis_aligned_bounding_box Valid: upper_bound.y", l_result.upper_bound.y ~ 2.22222222222223)
		end

	test_axis_aligned_bounding_box_wrong_1
			-- Wrong test 1 for axis_aligned_bounding_box
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_box", "covers/{B2D_SHAPE_CHAIN}.axis_aligned_bounding_box"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
			l_result:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_transform:B2D_TRANSFORM
		do
			if not l_retry then
				create l_transform.make_with_coordinates_and_angle (0, 0, 0)
				create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-0.666666666666667, {REAL_32}4.55555555555555],
									[{REAL_32}13.1111111111111, {REAL_32}2.22222222222223],
									[{REAL_32}10.0, {REAL_32}-3.0],
									[{REAL_32}1.6666666666667, {REAL_32}-0.666666666666667]
								>>)
				l_result := l_shape.axis_aligned_bounding_box (l_transform, 0)
				assert("axis_aligned_bounding_box Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_axis_aligned_bounding_box_wrong_2
			-- Wrong test 2 for axis_aligned_bounding_box
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_box", "covers/{B2D_SHAPE_CHAIN}.axis_aligned_bounding_box"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
			l_result:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_transform:B2D_TRANSFORM
		do
			if not l_retry then
				create l_transform.make_with_coordinates_and_angle (0, 0, 0)
				create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-0.666666666666667, {REAL_32}4.55555555555555],
									[{REAL_32}13.1111111111111, {REAL_32}2.22222222222223],
									[{REAL_32}10.0, {REAL_32}-3.0],
									[{REAL_32}1.6666666666667, {REAL_32}-0.666666666666667]
								>>)
				l_result := l_shape.axis_aligned_bounding_box (l_transform, 5)
				assert("axis_aligned_bounding_box Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_axis_aligned_bounding_box_wrong_3
			-- Wrong test 3 for axis_aligned_bounding_box
		note
			testing:  "covers/{B2D_SHAPE_CHAIN}.make_box", "covers/{B2D_SHAPE_CHAIN}.axis_aligned_bounding_box"
		local
			l_retry:BOOLEAN
			l_shape:B2D_SHAPE_CHAIN
			l_result:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_transform:B2D_TRANSFORM
		do
			if not l_retry then
				create l_transform.make_with_coordinates_and_angle (0, 0, 0)
				create l_shape.make (<<
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0],
									[{REAL_32}-5.333333333333333, {REAL_32}-5.0]
								>>)
				l_result := l_shape.axis_aligned_bounding_box (l_transform, 1)
				assert("axis_aligned_bounding_box Valid", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end


end


