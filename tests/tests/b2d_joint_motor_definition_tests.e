note
	description: "Tests for {B2D_JOINT_MOTOR_DEFINITION}"
	author: "Patrick Boucher"
	date: "Sat, 27 Jun 2020 04:24:21 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_JOINT_MOTOR_DEFINITION_TESTS

inherit
	EQA_TEST_SET
		redefine
			on_prepare
		end
	B2D_ANY
		undefine
			default_create
		end

feature {NONE} -- Implementation

	on_prepare
			-- <Precursor>
		local
			l_body_definition:B2D_BODY_DEFINITION
		do
			create world.make (0, -9.8)
			create l_body_definition
			l_body_definition.set_position (-10, -10)
			world.create_body (l_body_definition)
			body_1 := world.last_body
			l_body_definition.set_position (10, 10)
			world.create_body (l_body_definition)
			body_2 := world.last_body
		end

	world:B2D_WORLD
			-- The world in which the tests will be performed.

	body_1:detachable B2D_BODY
			-- The first body of each {B2D_JOINT_MOTOR_DEFINITION} during the tests.

	body_2:detachable B2D_BODY
			-- The second body of each {B2D_JOINT_MOTOR_DEFINITION} during the tests.

feature -- Normal test routines

	test_make_normal
			-- Normal test for {B2D_JOINT_MOTOR_DEFINITION}.make
		note
			testing:
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.make",
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.body_1",
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.body_2"
		local
			l_definition:B2D_JOINT_MOTOR_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2)
				assert ("body_1 assigned", l_definition.body_1 ~ la_body_1)
				assert ("body_2 assigned", l_definition.body_2 ~ la_body_2)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_linear_offset_normal
			-- Normal test for {B2D_JOINT_MOTOR_DEFINITION}.linear_offset
		note
			testing:
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.set_linear_offset",
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.set_linear_offset_with_coordinates",
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.linear_offset"
		local
			l_definition:B2D_JOINT_MOTOR_DEFINITION
			l_vector:B2D_VECTOR_2D
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2)
				create l_vector.make_with_coordinates (-5.5, -4.3)
				l_definition.set_linear_offset (l_vector)
				assert ("set_linear_offset valid", l_definition.linear_offset ~ l_vector)
				l_definition.set_linear_offset_with_coordinates (1.25, 6.625)
				assert ("set_linear_offset_with_coordinates valid x", l_definition.linear_offset.x ~ 1.25)
				assert ("set_linear_offset_with_coordinates valid y", l_definition.linear_offset.y ~ 6.625)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_angular_offset_normal
			-- Normal test for {B2D_JOINT_MOTOR_DEFINITION}.angular_offset
		note
			testing:
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.angular_offset",
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.set_angular_offset"
		local
			l_definition:B2D_JOINT_MOTOR_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2)
				l_definition.set_angular_offset (-0.11256)
				assert ("set_angular_offset valid", l_definition.angular_offset ~ -0.11256)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_max_force_normal
			-- Normal test for {B2D_JOINT_MOTOR_DEFINITION}.max_force
		note
			testing:
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.max_force",
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.set_max_force"
		local
			l_definition:B2D_JOINT_MOTOR_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2)
				l_definition.set_max_force (52.126)
				assert ("set_max_force valid", l_definition.max_force ~ 52.126)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_max_torque_normal
			-- Normal test for {B2D_JOINT_MOTOR_DEFINITION}.max_torque
		note
			testing:
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.max_torque",
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.set_max_torque"
		local
			l_definition:B2D_JOINT_MOTOR_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2)
				l_definition.set_max_torque (42.652)
				assert ("set_max_torque valid", l_definition.max_torque ~ 42.652)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_position_correction_factor_normal
			-- Normal test for {B2D_JOINT_MOTOR_DEFINITION}.position_correction_factor
		note
			testing:
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.position_correction_factor",
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.set_position_correction_factor"
		local
			l_definition:B2D_JOINT_MOTOR_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2)
				l_definition.set_position_correction_factor (0.4235)
				assert ("set_position_correction_factor valid", l_definition.position_correction_factor ~ 0.4235)
			else
				assert ("Bodies not valid", False)
			end
		end

feature -- Limits test routines

	test_set_max_force_limit
			-- Limit test for {B2D_JOINT_MOTOR_DEFINITION}.set_max_force
		note
			testing:
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.max_force",
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.set_max_force"
		local
			l_definition:B2D_JOINT_MOTOR_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2)
				l_definition.set_max_force (0.0)
				assert ("set_max_force valid", l_definition.max_force ~ 0.0)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_set_max_torque_limit
			-- Limit test for {B2D_JOINT_MOTOR_DEFINITION}.set_max_torque
		note
			testing:
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.max_torque",
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.set_max_torque"
		local
			l_definition:B2D_JOINT_MOTOR_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2)
				l_definition.set_max_torque (0.0)
				assert ("set_max_torque valid", l_definition.max_torque ~ 0.0)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_set_position_correction_factor_limit_1
			-- Limit test for {B2D_JOINT_MOTOR_DEFINITION}.set_position_correction_factor (lower limit).
		note
			testing:
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.position_correction_factor",
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.set_position_correction_factor"
		local
			l_definition:B2D_JOINT_MOTOR_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2)
				l_definition.set_position_correction_factor (0.0)
				assert ("set_position_correction_limit valid", l_definition.position_correction_factor ~ 0.0)
			else
				assert ("Bodies not valid", False)
			end
		end

	test_set_position_correction_factor_limit_2
			-- Limit test for {B2D_JOINT_MOTOR_DEFINITION}.set_position_correction_factor (upper limit).
		note
			testing:
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.position_correction_factor",
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.set_position_correction_factor"
		local
			l_definition:B2D_JOINT_MOTOR_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2)
				l_definition.set_position_correction_factor (1.0)
				assert ("set_position_correction_limit valid", l_definition.position_correction_factor ~ 1.0)
			else
				assert ("Bodies not valid", False)
			end
		end

feature -- Wrong test routines

	test_linear_offset_wrong
			-- Test for {B2D_JOINT_MOTOR_DEFINITION}.linear_offset when `item' does not exist.
		note
			testing:
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.set_linear_offset"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOTOR_DEFINITION
			l_discard:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					l_definition.put_null
					l_discard := l_definition.linear_offset
					assert ("linear_offset valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_linear_offset_wrong_1
			-- Test for {B2D_JOINT_MOTOR_DEFINITION}.set_linear_offset when `item' does not exist.
		note
			testing:
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.set_linear_offset"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOTOR_DEFINITION
			l_vector:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					l_definition.put_null
					create l_vector
					l_definition.set_linear_offset (l_vector)
					assert ("set_linear_offset valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

		test_set_linear_offset_wrong_2
			-- Test for {B2D_JOINT_MOTOR_DEFINITION}.set_linear_offset when `a_offset' does not exist.
		note
			testing:
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.set_linear_offset"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOTOR_DEFINITION
			l_vector:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					create l_vector
					l_vector.put_null
					l_definition.set_linear_offset (l_vector)
					assert ("set_linear_offset valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_linear_offset_with_coordinates_wrong
			-- Test for {B2D_JOINT_MOTOR_DEFINITION}.set_linear_offset_with_coordinates
			-- when `item' does not exist.
		note
			testing:
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.set_linear_offset_with_coordinates"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOTOR_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					l_definition.put_null
					l_definition.set_linear_offset_with_coordinates (0, 0)
					assert ("set_linear_offset_with_coordinates valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_angular_offset_wrong
			-- Test for {B2D_JOINT_MOTOR_DEFINITION}.angular_offset when `item' does not exist.
		note
			testing:
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.angular_offset"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOTOR_DEFINITION
			l_discard:REAL_32
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					l_definition.put_null
					l_discard := l_definition.angular_offset
					assert ("angular_offset valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_angular_offset_wrong
			-- Test for {B2D_JOINT_MOTOR_DEFINITION}.set_angular_offset when `item' does not exist.
		note
			testing:
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.set_angular_offset"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOTOR_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					l_definition.put_null
					l_definition.set_angular_offset (0)
					assert ("set_angular_offset valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_max_force_wrong
			-- Test for {B2D_JOINT_MOTOR_DEFINITION}.max_force when `item' does not exist.
		note
			testing:
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.max_force"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOTOR_DEFINITION
			l_discard:REAL_32
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					l_definition.put_null
					l_discard := l_definition.max_force
					assert ("max_force valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_max_force_wrong_1
			-- Test for {B2D_JOINT_MOTOR_DEFINITION}.set_max_force when `item' does not exist.
		note
			testing:
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.set_max_force"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOTOR_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					l_definition.put_null
					l_definition.set_max_force (0)
					assert ("set_max_force valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_max_force_wrong_2
			-- Test for {B2D_JOINT_MOTOR_DEFINITION}.set_max_force when `a_force' is negative.
		note
			testing:
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.set_max_force"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOTOR_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					l_definition.set_max_force (-0.1)
					assert ("set_max_force valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_max_torque_wrong_1
			-- Test for {B2D_JOINT_MOTOR_DEFINITION}.set_max_torque when `item' does not exist.
		note
			testing:
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.set_max_torque"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOTOR_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					l_definition.put_null
					l_definition.set_max_torque (0)
					assert ("set_max_torque valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_max_torque_wrong_2
			-- Test for {B2D_JOINT_MOTOR_DEFINITION}.set_max_torque when `a_torque' is negative.
		note
			testing:
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.set_max_torque"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOTOR_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					l_definition.set_max_torque (-0.1)
					assert ("set_max_torque valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_position_correction_factor_wrong
			-- Test for {B2D_JOINT_MOTOR_DEFINITION}.position_correction_factor when `item' does not exist.
		note
			testing:
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.position_correction_factor"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOTOR_DEFINITION
			l_discard:REAL_32
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					l_definition.put_null
					l_discard := l_definition.position_correction_factor
					assert ("position_correction_factor valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_position_correction_factor_wrong_1
			-- Test for {B2D_JOINT_MOTOR_DEFINITION}.set_position_correction_factor when `item' does not exist.
		note
			testing:
				"covers/{B2D_JOINT_MOTOR_DEFINITION}."
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOTOR_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					l_definition.put_null
					l_definition.set_position_correction_factor (0.5)
					assert ("set_position_correction_factor valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_position_correction_factor_wrong_2
			-- Test for {B2D_JOINT_MOTOR_DEFINITION}.set_position_correction_factor when `a_correction_factor' is
			-- smaller than 0.
		note
			testing:
				"covers/{B2D_JOINT_MOTOR_DEFINITION}.set_position_correction_factor"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOTOR_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					l_definition.set_position_correction_factor (-0.1)
					assert ("set_position_correction_factor valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_position_correction_factor_wrong_3
			-- Test for {B2D_JOINT_MOTOR_DEFINITION}.set_position_correction factor when `a_correction_factor' is
			-- higher than 1.
		note
			testing:
				"covers/{B2D_JOINT_MOTOR_DEFINITION}."
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_MOTOR_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2)
					l_definition.set_position_correction_factor (1.1)
					assert ("set_position_correction_factor valid", False)
				else
					assert ("Bodies not valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

end


