note
	description: "Tests for {B2D_FIXTURE}"
	author: "Louis Marchand"
	date: "Mon, 22 Jun 2020 22:06:44 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_FIXTURE_TESTS

inherit
	EQA_TEST_SET
		redefine
			on_prepare
		end
	B2D_COMPARE_REAL
		undefine
			default_create
		end

feature {NONE} -- Initialisation

	on_prepare
			-- <Precursor>
		local
			l_shape:B2D_SHAPE_POLYGON
			l_world:B2D_WORLD
			l_body_definition:B2D_BODY_DEFINITION
		do
			create l_shape.make_box (10, 10)
			create l_world.make (0, -10)
			create l_body_definition.make_dynamic
			l_world.create_body (l_body_definition)
			if attached l_world.last_body as la_body then
				la_body.create_fixture_from_shape (l_shape, 10)
				if attached la_body.last_fixture as la_fixture then
					fixture := la_fixture
				end
			end
		end

feature -- Test routines

	test_is_sensor_normal
			-- Normal test for is_sensor
		note
			testing:  "covers/{B2D_FIXTURE}.is_sensor", "covers/{B2D_FIXTURE}.set_is_sensor", "covers/{B2D_FIXTURE}.enable_sensor", "covers/{B2D_FIXTURE}.disable_sensor"
		do
			if attached fixture as la_fixture then
				la_fixture.set_is_sensor (False)
				assert("set_is_sensor Valid", not la_fixture.is_sensor)
				la_fixture.enable_sensor
				assert("set_is_sensor Valid", la_fixture.is_sensor)
				la_fixture.disable_sensor
				assert("set_is_sensor Valid", not la_fixture.is_sensor)
				la_fixture.set_is_sensor (True)
				assert("set_is_sensor Valid", la_fixture.is_sensor)
			else
				assert("Cannot create {B2D_FIXTURE}.", False)
			end
		end

	test_filter_normal
			-- Normal test filter
		note
			testing:  "covers/{B2D_FIXTURE}.filter", "covers/{B2D_FIXTURE}.set_filter"
		local
			l_filter:B2D_FILTER
		do
			if attached fixture as la_fixture then
				create l_filter
				l_filter.set_category_bits (0x0F)
				l_filter.set_mask_bits (0x01)
				l_filter.set_group_index (2)
				la_fixture.set_filter (l_filter)
				assert("set_filter not valid", la_fixture.filter.category_bits = 0x0F)
				assert("set_filter not valid", la_fixture.filter.mask_bits = 0x01)
				assert("set_filter not valid", la_fixture.filter.group_index = 2)
			else
				assert("Cannot create {B2D_FIXTURE}.", False)
			end
		end

	test_contains_normal_1
			-- Normal test 1 contains
		note
			testing:  "covers/{B2D_FIXTURE}.contains"
		do
			if attached fixture as la_fixture then
				assert("Contains not valid", la_fixture.contains (5, 5))
			else
				assert("Cannot create {B2D_FIXTURE}.", False)
			end
		end

	test_contains_normal_2
			-- Normal test 2 contains
		note
			testing:  "covers/{B2D_FIXTURE}.contains"
		do
			if attached fixture as la_fixture then
				assert("Contains not valid", not la_fixture.contains (15, 15))
			else
				assert("Cannot create {B2D_FIXTURE}.", False)
			end
		end

	test_contains_limit_1
			-- Limit test 1 contains
		note
			testing:  "covers/{B2D_FIXTURE}.contains"
		do
			if attached fixture as la_fixture then
				assert("Contains not valid", la_fixture.contains (10, 10))
			else
				assert("Cannot create {B2D_FIXTURE}.", False)
			end
		end

	test_contains_limit_2
			-- Limit test 2 contains
		note
			testing:  "covers/{B2D_FIXTURE}.contains"
		do
			if attached fixture as la_fixture then
				assert("Contains not valid", not la_fixture.contains (10.1, 10.1))
			else
				assert("Cannot create {B2D_FIXTURE}.", False)
			end
		end

	test_contains_with_vector_normal_1
			-- Normal test 1 contains_with_vector
		note
			testing:  "covers/{B2D_FIXTURE}.contains_with_vector"
		local
			l_vector:B2D_VECTOR_2D
		do
			if attached fixture as la_fixture then
				create l_vector.make_with_coordinates (-5, -5)
				assert("contains_with_vector not valid", la_fixture.contains_with_vector (l_vector))
			else
				assert("Cannot create {B2D_FIXTURE}.", False)
			end
		end

	test_contains_with_vector_normal_2
			-- Normal test 2 contains_with_vector
		note
			testing:  "covers/{B2D_FIXTURE}.contains_with_vector"
		local
			l_vector:B2D_VECTOR_2D
		do
			if attached fixture as la_fixture then
				create l_vector.make_with_coordinates (-15, -15)
				assert("contains_with_vector not valid", not la_fixture.contains_with_vector (l_vector))
			else
				assert("Cannot create {B2D_FIXTURE}.", False)
			end
		end

	test_contains_with_vector_limit_1
			-- Limit test 1 contains_with_vector
		note
			testing:  "covers/{B2D_FIXTURE}.contains_with_vector"
		local
			l_vector:B2D_VECTOR_2D
		do
			if attached fixture as la_fixture then
				create l_vector.make_with_coordinates (-10, -10)
				assert("contains_with_vector not valid", la_fixture.contains_with_vector (l_vector))
			else
				assert("Cannot create {B2D_FIXTURE}.", False)
			end
		end

	test_contains_with_vector_limit_2
			-- Limit test 2 contains_with_vector
		note
			testing:  "covers/{B2D_FIXTURE}.contains_with_vector"
		local
			l_vector:B2D_VECTOR_2D
		do
			if attached fixture as la_fixture then
				create l_vector.make_with_coordinates (-10.1, -10.1)
				assert("contains_with_vector not valid", not la_fixture.contains_with_vector (l_vector))
			else
				assert("Cannot create {B2D_FIXTURE}.", False)
			end
		end

	test_density_normal
			-- Normal test density
		note
			testing:  "covers/{B2D_FIXTURE}.density", "covers/{B2D_FIXTURE}.set_density"
		do
			if attached fixture as la_fixture then
				la_fixture.set_density (0.0)
				la_fixture.set_density (-9.19865)
				assert("set_density not valid.", la_fixture.density ~ -9.19865)
			else
				assert("Cannot create {B2D_FIXTURE}.", False)
			end
		end

	test_friction_normal
			-- Normal test friction
		note
			testing:  "covers/{B2D_FIXTURE}.friction", "covers/{B2D_FIXTURE}.set_friction"
		do
			if attached fixture as la_fixture then
				la_fixture.set_friction (0.0)
				la_fixture.set_friction (4.8327)
				assert("set_friction not valid.", la_fixture.friction ~ 4.8327)
			else
				assert("Cannot create {B2D_FIXTURE}.", False)
			end
		end

	test_restitution_normal
			-- Normal test restitution
		note
			testing:  "covers/{B2D_FIXTURE}.restitution", "covers/{B2D_FIXTURE}.set_restitution"
		do
			if attached fixture as la_fixture then
				la_fixture.set_restitution (0.0)
				la_fixture.set_restitution (1.42015)
				assert("set_restitution not valid.", la_fixture.restitution ~ 1.42015)
			else
				assert("Cannot create {B2D_FIXTURE}.", False)
			end
		end

	test_ray_cast_normal_1
			-- Normal test 1 ray_cast
		note
			testing:  "covers/{B2D_FIXTURE}.ray_cast"
		local
			l_input:B2D_RAY_CAST_INPUT
		do
			if attached fixture as la_fixture then
				create l_input.make_with_coordinates (10, 20, 10, 10, 1)
				if attached la_fixture.ray_cast (l_input, 1) as la_output then
					assert("ray_cast Valid. Normal: X", la_output.normal.x ~ 0)
					assert("ray_cast Valid. Normal: Y", la_output.normal.y ~ 1)
					assert("ray_cast Valid. Fraction", la_output.fraction ~ 1)
				else
					assert("ray_cast Valid", False)
				end
			else
				assert("Cannot create {B2D_FIXTURE}.", False)
			end
		end

	test_ray_cast_normal_2
			-- Normal test 2 ray_cast
		note
			testing:  "covers/{B2D_FIXTURE}.ray_cast"
		local
			l_input:B2D_RAY_CAST_INPUT
		do
			if attached fixture as la_fixture then
				create l_input.make_with_coordinates (150, 150, 160, 160, 12)
				assert("ray_cast Valid", not attached la_fixture.ray_cast (l_input, 1))
			else
				assert("Cannot create {B2D_FIXTURE}.", False)
			end
		end

	test_ray_cast_wrong_1
			-- Wrong test 1 ray_cast
		note
			testing:  "covers/{B2D_FIXTURE}.ray_cast"
		local
			l_input:B2D_RAY_CAST_INPUT
			l_retry:BOOLEAN
			l_output:detachable B2D_RAY_CAST_OUTPUT
		do
			if not l_retry then
				if attached fixture as la_fixture then
					create l_input.make_with_coordinates (150, 150, 160, 160, 12)
					l_output := la_fixture.ray_cast (l_input, 2)
					assert("ray_cast not valid.", False)
				else
					assert("Cannot create {B2D_FIXTURE}.", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_ray_cast_wrong_2
			-- Wrong test 2 ray_cast
		note
			testing:  "covers/{B2D_FIXTURE}.ray_cast"
		local
			l_input:B2D_RAY_CAST_INPUT
			l_retry:BOOLEAN
			l_output:detachable B2D_RAY_CAST_OUTPUT
		do
			if not l_retry then
				if attached fixture as la_fixture then
					create l_input.make_with_coordinates (150, 150, 160, 160, 12)
					l_output := la_fixture.ray_cast (l_input, 0)
					assert("ray_cast not valid.", False)
				else
					assert("Cannot create {B2D_FIXTURE}.", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_mass_normal
			-- Normal test mass
		note
			testing:  "covers/{B2D_FIXTURE}.mass"
		local
			l_mass:B2D_MASS
		do
			if attached fixture as la_fixture then
				l_mass := la_fixture.mass (5)
				assert("mass Valid: center.x", l_mass.center.x ~ 0)
				assert("mass Valid: center.y", l_mass.center.y ~ 0)
				assert("mass Valid: Mass", compare_real_32 (l_mass.mass, 2000))
				assert("mass Valid: inertia", compare_real_32 (l_mass.inertia, 133333))
			else
				assert("Cannot create {B2D_FIXTURE}.", False)
			end
		end

	test_mass_limit
			-- Limit test mass
		note
			testing:  "covers/{B2D_FIXTURE}.mass"
		local
			l_mass:B2D_MASS
		do
			if attached fixture as la_fixture then
				l_mass := la_fixture.mass (0)
				assert("mass Valid: center.x", l_mass.center.x ~ 0)
				assert("mass Valid: center.y", l_mass.center.y ~ 0)
				assert("mass Valid: Mass", compare_real_32 (l_mass.mass, 0))
				assert("mass Valid: inertia", compare_real_32 (l_mass.inertia, 0))
			else
				assert("Cannot create {B2D_FIXTURE}.", False)
			end
		end

	test_axis_aligned_bounding_box_normal
			-- Normal test axis_aligned_bounding_box
		note
			testing:  "covers/{B2D_FIXTURE}.axis_aligned_bounding_box"
		local
			l_result:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			if attached fixture as la_fixture then
				l_result := la_fixture.axis_aligned_bounding_box (1)
				assert("axis_aligned_bounding_box Valid: lower_bound.x", l_result.lower_bound.x ~ -10.01)
				assert("axis_aligned_bounding_box Valid: lower_bound.y", l_result.lower_bound.y ~ -10.01)
				assert("axis_aligned_bounding_box Valid: upper_bound.x", l_result.upper_bound.x ~ 10.01)
				assert("axis_aligned_bounding_box Valid: upper_bound.y", l_result.upper_bound.y ~ 10.01)
			else
				assert("Cannot create {B2D_FIXTURE}.", False)
			end
		end

	test_axis_aligned_bounding_box_wrong_1
			-- Wrong test 1 axis_aligned_bounding_box
		note
			testing:  "covers/{B2D_FIXTURE}.axis_aligned_bounding_box"
		local
			l_result:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_retry:BOOLEAN
		do
			if not l_retry then
				if attached fixture as la_fixture then
					l_result := la_fixture.axis_aligned_bounding_box (2)
					assert("axis_aligned_bounding_box not Valid", False)
				else
					assert("Cannot create {B2D_FIXTURE}.", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_axis_aligned_bounding_box_wrong_2
			-- Wrong test 2 axis_aligned_bounding_box
		note
			testing:  "covers/{B2D_FIXTURE}.axis_aligned_bounding_box"
		local
			l_result:B2D_AXIS_ALIGNED_BOUNDING_BOX
			l_retry:BOOLEAN
		do
			if not l_retry then
				if attached fixture as la_fixture then
					l_result := la_fixture.axis_aligned_bounding_box (0)
					assert("axis_aligned_bounding_box not Valid", False)
				else
					assert("Cannot create {B2D_FIXTURE}.", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

feature {NONE} -- Implementation

	fixture:detachable B2D_FIXTURE
			-- The {B2D_FIXTURE} used in `Current'
end


