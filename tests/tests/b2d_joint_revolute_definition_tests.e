note
	description: "Tests for {B2D_JOINT_REVOLUTE_DEFINITION}."
	author: "Patrick Boucher"
	date: "Wed, 08 Jul 2020 05:14:39 +0000"
	revision: "0.1"
	testing: "type/manual"

class
	B2D_JOINT_REVOLUTE_DEFINITION_TESTS

inherit
	EQA_TEST_SET
		redefine
			on_prepare
		end
	B2D_ANY
		undefine
			default_create
		end
	B2D_COMPARE_REAL
		undefine
			default_create
		end

feature {NONE} -- Implementation

	on_prepare
			-- <Precursor>
		local
			l_body_definition:B2D_BODY_DEFINITION
		do
			create world.make (0, -9.8)
			create l_body_definition
			l_body_definition.set_position (-10, -10)
			l_body_definition.set_angle ({DOUBLE_MATH}.pi_2.truncated_to_real)
			world.create_body (l_body_definition)
			body_1 := world.last_body
			l_body_definition.set_position (10, 10)
			l_body_definition.set_angle (-{DOUBLE_MATH}.pi_2.truncated_to_real)
			world.create_body (l_body_definition)
			body_2 := world.last_body
		end

	world:B2D_WORLD
			-- The world in which the tests will be performed.

	body_1:detachable B2D_BODY
			-- The first body of each {B2D_JOINT_REVOLUTE_DEFINITION} during the tests.

	body_2:detachable B2D_BODY
			-- The second body of each {B2D_JOINT_REVOLUTE_DEFINITION} during the tests.

feature -- Normal test routines

	test_make_normal
			-- Normal test for `make'.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, -2, -2)
				assert ("body_1 assigned", l_definition.body_1 ~ la_body_1)
				assert ("body_2 assigned", l_definition.body_2 ~ la_body_2)
				assert ("anchor assigned x", compare_real_32 (l_definition.anchor.x, -2))
				assert ("anchor assigned y", compare_real_32 (l_definition.anchor.y, -2))
				assert ("local_anchor_1 computed x", compare_real_32 (l_definition.local_anchor_1.x, 8))
				assert ("local_anchor_1 computed y", compare_real_32 (l_definition.local_anchor_1.y, -8))
				assert ("local_anchor_2 computed x", compare_real_32 (l_definition.local_anchor_2.x, 12))
				assert ("local_anchor_2 computed y", compare_real_32 (l_definition.local_anchor_2.y, -12))
				assert ("reference_angle computed",
					compare_real_32 (l_definition.reference_angle, la_body_2.angle - la_body_1.angle))
			end
		end

	test_make_with_vector_normal
			-- Normal test for `make_with_vector'.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.make_with_vector",
					  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.make"
		local
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
			l_anchor:B2D_VECTOR_2D
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_anchor.make_with_coordinates (-2, -2)
				create l_definition.make_with_vector (la_body_1, la_body_2, l_anchor)
				assert ("body_1 assigned", l_definition.body_1 ~ la_body_1)
				assert ("body_2 assigned", l_definition.body_2 ~ la_body_2)
				assert ("anchor assigned", l_definition.anchor ~ l_anchor)
				assert ("local_anchor_1 computed x", compare_real_32 (l_definition.local_anchor_1.x, 8))
				assert ("local_anchor_1 computed y", compare_real_32 (l_definition.local_anchor_1.y, -8))
				assert ("local_anchor_2 computed x", compare_real_32 (l_definition.local_anchor_2.x, 12))
				assert ("local_anchor_2 computed y", compare_real_32 (l_definition.local_anchor_2.y, -12))
				assert ("reference_angle computed",
					compare_real_32 (l_definition.reference_angle, la_body_2.angle - la_body_1.angle))
			end
		end

	test_anchor_normal
			-- Normal test for `anchor'.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.anchor",
					  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_anchor",
					  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_anchor_with_vector",
					  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.local_anchor_1",
					  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.local_anchor_2"
		local
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
			l_anchor:B2D_VECTOR_2D
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0)
				-- set_anchor:
				l_definition.set_anchor (3, 3)
				assert ("set_anchor valid x", compare_real_32 (l_definition.anchor.x, 3))
				assert ("set_anchor valid y", compare_real_32 (l_definition.anchor.y, 3))
				assert ("local_anchor_1 valid x", compare_real_32 (l_definition.local_anchor_1.x, 13))
				assert ("local_anchor_1 valid y", compare_real_32 (l_definition.local_anchor_1.y, -13))
				assert ("local_anchor_2 valid x", compare_real_32 (l_definition.local_anchor_2.x, 7))
				assert ("local_anchor_2 valid y", compare_real_32 (l_definition.local_anchor_2.y, -7))
				-- set_anchor_with_vector:
				create l_anchor.make_with_coordinates (-2, -2)
				l_definition.set_anchor_with_vector (l_anchor)
				assert ("set_anchor_with_vector valid", l_definition.anchor ~ l_anchor)
				assert ("local_anchor_1 valid x", compare_real_32 (l_definition.local_anchor_1.x, 8))
				assert ("local_anchor_1 valid y", compare_real_32 (l_definition.local_anchor_1.y, -8))
				assert ("local_anchor_2 valid x", compare_real_32 (l_definition.local_anchor_2.x, 12))
				assert ("local_anchor_2 valid y", compare_real_32 (l_definition.local_anchor_2.y, -12))
			end
		end

	test_reference_angle_normal
			-- Normal test for `reference_angle'.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.reference_angle",
					  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.update_reference_angle"
		local
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0)
				la_body_2.set_transform (10, 10, 0)
				l_definition.update_reference_angle
				assert ("update_reference_angle valid", compare_real_32 (l_definition.reference_angle, -{MATH_CONST}.pi_2.truncated_to_real))
			end
		end

	test_has_limit_normal
			-- Normal test for `has_limit'.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.has_limit",
					  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_has_limit",
					  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.enable_limit",
					  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.disable_limit"
		local
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0)
				assert ("default no limits", not l_definition.is_limit_enabled)
				l_definition.set_is_limit_enabled (True)
				assert ("set to True valid", l_definition.is_limit_enabled)
				l_definition.set_is_limit_enabled (False)
				assert ("set to False valid", not l_definition.is_limit_enabled)
				l_definition.enable_limit
				assert ("enable_limit valid", l_definition.is_limit_enabled)
				l_definition.disable_limit
				assert ("disable_limit valid", not l_definition.is_limit_enabled)
			end
		end

	test_lower_limit_normal
			-- Normal test for `lower_limit'.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.lower_limit",
					  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_lower_limit"
		local
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0)
				l_definition.set_lower_limit (-2.3)
				assert ("set_lower_limit valid", l_definition.lower_limit ~ -2.3)
			end
		end

	test_upper_limit_normal
			-- Normal test for `upper_limit'.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.upper_limit",
					  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_upper_limit"
		local
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0)
				l_definition.set_upper_limit (2.3)
				assert ("set_upper_limit valid", l_definition.upper_limit ~ 2.3)
			end
		end

	test_set_limits_normal
			-- Normal test for `set_limits'.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_limits",
					  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.lower_limit",
					  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.upper_limit",
					  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.limits"
		local
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0)
				l_definition.set_limits (5.5, 6.6)
				assert ("set_limits valid", l_definition.lower_limit ~ 5.5 and l_definition.upper_limit ~ 6.6)
				assert ("limits valid", l_definition.limits.lower_limit ~ 5.5 and l_definition.limits.upper_limit ~ 6.6)
			end
		end

	test_is_motor_enabled_normal
			-- Normal test for `is_motor_enabled'.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.is_motor_enabled",
					  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_is_motor_enabled",
					  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.enable_motor",
					  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.disable_motor"
		local
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0)
				assert ("default motor disabled", not l_definition.is_motor_enabled)
				l_definition.set_is_motor_enabled (True)
				assert ("set True valid", l_definition.is_motor_enabled)
				l_definition.set_is_motor_enabled (False)
				assert ("set False valid", not l_definition.is_motor_enabled)
				l_definition.enable_motor
				assert ("enable_motor valid", l_definition.is_motor_enabled)
				l_definition.disable_motor
				assert ("disable_motor valid", not l_definition.is_motor_enabled)
			end
		end

	test_motor_speed_normal
			-- Normal test for `motor_speed'.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.motor_speed",
					  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_motor_speed"
		local
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0)
				l_definition.set_motor_speed (0.3)
				assert ("set_motor_speed valid", l_definition.motor_speed ~ 0.3)
			end
		end

	test_max_motor_torque_normal
			-- Normal test for `'.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.max_motor_torque",
					  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_max_motor_torque"
		local
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0)
				l_definition.set_maximum_motor_torque (5.3)
				assert ("set_max_motor_torque valid", l_definition.maximum_motor_torque ~ 5.3)
			end
		end

feature -- Limit test routines

	test_set_lower_limit_limit
			-- Limit test for `set_lower_limit'.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.lower_limit",
					  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_lower_limit"
		local
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0)
				l_definition.set_upper_limit (2.36)
				l_definition.set_lower_limit (2.36)
				assert ("set_lower_limit valid", l_definition.lower_limit ~ 2.36)
			end
		end

	test_set_upper_limit_limit
			-- Limit test for `set_upper_limit'.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.upper_limit",
					  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_upper_limit"
		local
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0)
				l_definition.set_lower_limit (-2.36)
				l_definition.set_upper_limit (-2.36)
				assert ("set_upper_limit valid", l_definition.upper_limit ~ -2.36)
			end
		end

	test_set_limits_limit
			-- Limit test for `set_limits'.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_limits",
					  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.lower_limit",
					  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.upper_limit"
		local
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0)
				l_definition.set_limits (2.36, 2.36)
				assert ("set_limits valid", l_definition.lower_limit ~ 2.36	and l_definition.upper_limit ~ 2.36)
			end
		end

	test_set_motor_speed_limit
			-- Limit test for `set_motor_speed'.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.motor_speed",
					  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_motor_speed"
		local
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0)
				l_definition.set_motor_speed (0.0)
				assert ("set_motor_speed valid", l_definition.motor_speed ~ 0.0)
			end
		end

	test_set_max_motor_torque_limit
			-- Limit test for `set_maximum_motor_torque'.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_max_motor_torque",
					  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.max_motor_torque"
		local
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				create l_definition.make (la_body_1, la_body_2, 0, 0)
				l_definition.set_maximum_motor_torque (0.0)
				assert ("set_max_motor_torque valid", l_definition.maximum_motor_torque ~ 0.0)
			end
		end

feature -- Wrong test routines

	test_make_wrong
			-- Test for `make' when arguments do not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.make_with_vector"
		local
			l_retry:INTEGER
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				if l_retry ~ 0 then
					la_body_1.put_null
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					assert ("Body_1_Exists", False)
				elseif l_retry ~ 1 then
					la_body_2.put_null
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					assert ("Body_2_Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := l_retry + 1
				retry
			end
		end

	test_make_with_vector_wrong
			-- Test for `make_with_vector' when arguments do not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.make_with_vector"
		local
			l_retry:INTEGER
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
			l_anchor:B2D_VECTOR_2D
		do
			if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
				if l_retry ~ 0 then
					la_body_1.put_null
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					assert ("Body_1_Exists", False)
				elseif l_retry ~ 1 then
					la_body_2.put_null
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					assert ("Body_2_Exists", False)
				elseif l_retry ~ 2 then
					create l_anchor
					l_anchor.put_null
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					assert ("Anchor_Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := l_retry + 1
				retry
			end
		end

	test_anchor_wrong
			-- Test for `anchor' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.anchor"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
			l_discard:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_discard := l_definition.anchor
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_anchor_wrong
			-- Test for `set_anchor' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_anchor"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_definition.set_anchor (0, 0)
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_anchor_with_vector_wrong_1
			-- Test for `set_anchor_with_vector' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_anchor_with_vector"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_definition.set_anchor_with_vector (create {B2D_VECTOR_2D})
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_anchor_with_vector_wrong_2
			-- Test for `set_anchor_with_vector' when `a_world_point' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_anchor_with_vector"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
			l_anchor:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					create l_anchor
					l_anchor.put_null
					l_definition.set_anchor_with_vector (l_anchor)
					assert ("Vector_Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_local_anchor_1_wrong
			-- Test for `local_anchor_1' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.local_anchor_1"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
			l_discard:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_discard := l_definition.local_anchor_1
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_local_anchor_2_wrong
			-- Test for `local_anchor_2' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.local_anchor_2"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
			l_discard:B2D_VECTOR_2D
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_discard := l_definition.local_anchor_2
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_reference_angle_wrong
			-- Test for `reference_angle' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.reference_angle"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
			l_discard:REAL_32
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_discard := l_definition.reference_angle
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_update_reference_angle_wrong
			-- Test for `update_reference_angle' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.update_reference_angle"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_definition.update_reference_angle
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_has_limit_wrong
			-- Test for `has_limit' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.has_limit"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
			l_discard:BOOLEAN
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_discard := l_definition.is_limit_enabled
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_has_limit_wrong
			-- Test for `set_has_limit' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_has_limit"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_definition.set_is_limit_enabled (True)
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_enable_limit_wrong
			-- Test for `enable_limit' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.enable_limit"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_definition.enable_limit
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_disable_limit_wrong
			-- Test for `disable_limit' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.disable_limit"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_definition.disable_limit
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_lower_limit_wrong
			-- Test for `lower_limit' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.lower_limit"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
			l_discard:REAL_32
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_discard := l_definition.lower_limit
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_lower_limit_wrong_1
			-- Test for `set_lower_limit' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_lower_limit"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_definition.set_lower_limit (-2)
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_lower_limit_wrong_2
			-- Test for `set_lower_limit' when `a_value' is higher than `upper_limit'.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_lower_limit"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.set_lower_limit (l_definition.upper_limit + 0.1)
					assert ("set_lower_limit valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_upper_limit_wrong
			-- Test for `upper_limit' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.upper_limit"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
			l_discard:REAL_32
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_discard := l_definition.upper_limit
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_upper_limit_wrong_1
			-- Test for `set_upper_limit' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_upper_limit"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_definition.set_upper_limit (2)
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_upper_limit_wrong_2
			-- Test for `set_upper_limit' when `a_value' is smaller than `lower_limit'.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_upper_limit"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.set_upper_limit (l_definition.lower_limit - 0.1)
					assert ("set_upper_limit valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_is_motor_enabled_wrong
			-- Test for `is_motor_enabled' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.is_motor_enabled"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
			l_discard:BOOLEAN
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_discard := l_definition.is_motor_enabled
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_is_motor_enabled_wrong
			-- Test for `set_is_motor_enabled' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_is_motor_enabled"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_definition.set_is_motor_enabled (True)
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_enable_motor_wrong
			-- Test for `enable_motor' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.enable_motor"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_definition.enable_motor
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_disable_motor_wrong
			-- Test for `disable_motor' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.disable_motor"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_definition.disable_motor
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_motor_speed_wrong
			-- Test for `motor_speed' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.motor_speed"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
			l_discard:REAL_32
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_discard := l_definition.motor_speed
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_motor_speed_wrong_1
			-- Test for `set_motor_speed' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_motor_speed"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_definition.set_motor_speed (0.2)
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_motor_speed_wrong_2
			-- Test for `set_motor_speed' when `a_value' is negative.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_motor_speed"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.set_motor_speed (-0.1)
					assert ("set_motor_speed valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_max_motor_torque_wrong
			-- Test for `maximum_motor_torque' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.max_motor_torque"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
			l_discard:REAL_32
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_discard := l_definition.maximum_motor_torque
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_max_motor_torque_wrong_1
			-- Test for `set_maximum_motor_torque' when `item' does not exist.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_max_motor_torque"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.put_null
					l_definition.set_maximum_motor_torque (0.2)
					assert ("Exists", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

	test_set_max_motor_torque_wrong
			-- Test for `set_maximum_motor_torque' when `a_value' is negative.
		note
			testing:  "covers/{B2D_JOINT_REVOLUTE_DEFINITION}.set_max_motor_torque"
		local
			l_retry:BOOLEAN
			l_definition:B2D_JOINT_REVOLUTE_DEFINITION
		do
			if not l_retry then
				if attached body_1 as la_body_1 and attached body_2 as la_body_2 then
					create l_definition.make (la_body_1, la_body_2, 0, 0)
					l_definition.set_maximum_motor_torque (-0.1)
					assert ("set_max_motor_torque valid", False)
				end
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry := True
				retry
			end
		end

end


