﻿note
	description: "A 2D column vector."
	author: "Louis Marchand"
	date: "Tue, 09 Jun 2020 13:27:08 +0000"
	revision: "0.1"

class
	B2D_ROTATION

inherit
	B2D_MEMORY_OBJECT
		redefine
			is_equal, default_create
		end

create
	make,
	default_create

create {B2D_ANY}
	own_from_item,
	shared_from_item,
	make_from_item

feature {NONE} -- Initialisation

	default_create
			-- Initialisation of `Current' as an identity angle.
		do
			make(0.0)
		end

	make(a_angle:REAL_32)
			-- Initialisation of `Current' using `a_angle'
			-- as `angle'. `angle' will be normalized (between -π and π).
		do
			item := {B2D_EXTERNAL}.b2Rot_new(a_angle)
		end

	make_from_item(a_item:POINTER)
			-- Create a copy of the rotation pointed by `a_item' into `Current'.
			-- `a_item' can be NULL
		local
			l_rotation:B2D_ROTATION
		do
			if a_item.is_default_pointer then
				own_from_item (a_item)
			else
				create l_rotation.shared_from_item (a_item, Void)
				make (l_rotation.angle)
			end
		end

feature -- Access

	angle:REAL_32 assign set_angle
			-- The angle value of `Current'
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2Rot_GetAngle(item)
		end

	set_angle(a_angle:REAL_32)
			-- Assign `angle' with the value of `a_angle'.
			-- `angle' will be normalized (between -π and π).
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2Rot_Set(item, a_angle)
		end

	set_identity
			-- Assign `angle' with the identity value
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2Rot_SetIdentity(item)
		ensure
			Is_Identity: angle ~ 0.0
		end

	x_axis:B2D_VECTOR_2D
			-- The vertical axis
		require
			Exists: exists
		do
			create Result
			if Result.exists then
				{B2D_EXTERNAL}.b2Rot_GetXAxis(item, Result.item)
			end
		end

	y_axis:B2D_VECTOR_2D
			-- The horizontal axis
		require
			Exists: exists
		do
			create Result
			if Result.exists then
				{B2D_EXTERNAL}.b2Rot_GetYAxis(item, Result.item)
			end
		end

	cosine_angle:REAL_32
			-- The cosine of `angle'
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2Rot_c_get(item)
		end

	sine_angle:REAL_32
			-- The sine of `angle'
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2Rot_s_get(item)
		end

	is_equal(a_other:like Current):BOOLEAN
			-- Is `a_other` attached to an object considered
			-- equal to current object?
		do
			if exists and a_other.exists then
				Result := angle ~ a_other.angle
			else
				Result := exists ~ a_other.exists
			end
		end

feature {NONE} -- Implementation

	delete
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2Rot_delete(item)
		end

end
