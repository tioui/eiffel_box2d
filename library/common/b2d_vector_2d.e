note
	description: "A 2D column vector."
	author: "Louis Marchand"
	date: "Fri, 05 Jun 2020 16:59:55 +0000"
	revision: "0.1"

class
	B2D_VECTOR_2D

inherit
	B2D_MEMORY_OBJECT
		redefine
			default_create, is_equal, out
		end

create
	default_create,
	make,
	make_with_coordinates,
	make_from_other

create {B2D_ANY}
	own_from_item,
	shared_from_item,
	make_from_item

feature {NONE} -- Initialisation

	default_create
			-- <Precursor>
		do
			make
		end

	make
			-- Initialisation of `Current'
		do
			make_with_coordinates(0,0)
		ensure
			X_Valid: exists implies x ~ 0.0
			Y_Valid: exists implies y ~ 0.0
		end

	make_with_coordinates(a_x, a_y:REAL_32)
			-- Initialisation of `Current' using `a_x' as `x' and `a_y' as `y'
		do
			own_from_item({B2D_EXTERNAL}.b2vec2_new_coordinates (a_x, a_y))
		ensure
			X_Valid: exists implies x ~ a_x
			Y_Valid: exists implies y ~ a_y
		end

	make_from_other(a_other:like Current)
			-- Initialisation of `Current' using the coordinates of `a_other'
		require
			Other_Exist: a_other.exists
		do
			make_with_coordinates (a_other.x, a_other.y)
		end

	make_from_item(a_item:POINTER)
			-- Create a copy of the vector pointed by `a_item' into `Current'.
			-- `a_item' can be NULL
		local
			l_vector:B2D_VECTOR_2D
		do
			if a_item.is_default_pointer then
				own_from_item (a_item)
			else
				create l_vector.shared_from_item (a_item, Void)
				make_from_other (l_vector)
			end
		end

feature -- Access

	x:REAL_32 assign set_x
			-- Horizontal coordinate of `Current'
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2vec2_x_get (item)
		end

	y:REAL_32 assign set_y
			-- Vertical coordinate of `Current'
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2vec2_y_get (item)
		end

	set_x(a_x:REAL_32)
			-- Assign `x' with the value of `a_x'
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2vec2_x_set (item, a_x)
		ensure
			Is_Assing: x = a_x
		end

	set_y(a_y:REAL_32)
			-- Assign `y' with the value of `a_y'
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2vec2_y_set (item, a_y)
		ensure
			Is_Assing: y = a_y
		end

	set_zero
			-- Assign 0 to both `x' and `y'
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2Vec2_set_zero(item)
		ensure
			X_Valid: x ~ 0.0
			Y_Valid: y ~ 0.0
		end

	set_coordinates(a_x, a_y:REAL_32)
			-- Assing `a_x' to`x' and `a_y' to `y'
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2Vec2_set(item, a_x, a_y)
		ensure
			X_Valid: x ~ a_x
			Y_Valid: y ~ a_y
		end

	magnitude:REAL_32
			-- Length of `Current'.
		require
			Exists: exists
		do
			Result := {SINGLE_MATH}.sqrt (x * x + y * y)
		end

	normalize
			-- Make `Current' have a `magnitude' of 1.
			-- The direction is kept.
		require
			Exists: exists
		local
			l_magnitude:REAL_32
		do
			l_magnitude := magnitude
			if l_magnitude > 0.0 then
				set_coordinates (x / l_magnitude, y / l_magnitude)
			end
		ensure
			Magnitude_Valid: magnitude ~ 0.0 or magnitude ~ 1.0
		end

	normalized:B2D_VECTOR_2D
			-- Make a new normalized {B2D_VECTOR_2D} of `Current'.
		require
			Exists: exists
		do
			create Result.make_with_coordinates (x, y)
			Result.normalize
		ensure
			Magnitude_Valid: Result.magnitude ~ 0.0 or Result.magnitude ~ 1.0
			Kept_Direction: {B2D_COMPARE_REAL}.compare_real_32 (Result.x * magnitude, x) and {B2D_COMPARE_REAL}.compare_real_32 (Result.y * magnitude, y)
		end

	distance (a_vector:B2D_VECTOR_2D):REAL_32
			-- The distance between `Current' and `a_vector'.
		require
			Exists: exists
			Vector_Exists: a_vector.exists
		local
			l_difference:B2D_VECTOR_2D
		do
			create l_difference
			l_difference.set_x (a_vector.x - x)
			l_difference.set_y (a_vector.y - y)
			Result := l_difference.magnitude
		end

	is_equal(a_other:like Current):BOOLEAN
			-- Is `a_other` attached to an object considered
			-- equal to current object?
		do
			if exists and a_other.exists then
				Result := {B2D_COMPARE_REAL}.compare_real_32 (x, a_other.x) and {B2D_COMPARE_REAL}.compare_real_32 (y, a_other.y)
			else
				Result := exists ~ a_other.exists
			end
		end

	out:STRING
			-- <Precursor>
		do
			if exists then
				Result := "[" + x.out + "," + y.out + "]"
			else
				Result := "[,]"
			end

		end

feature {NONE} -- Implementation

	delete
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2vec2_delete(item)
		end
end
