note
	description: "Main class of the Box2D engine."
	author: "Louis Marchand"
	date: "Mon, 15 Jun 2020 14:43:33 +0000"
	revision: "0.1"

deferred class
	B2D_LIST_HELPER

inherit
	B2D_MEMORY_OBJECT

feature {NONE} -- Implementation

	has_same_item(
					a_list:LIST[B2D_MEMORY_OBJECT];
					a_get_list, a_get_next:FUNCTION[POINTER, POINTER]
				):BOOLEAN
			-- `True' if `a_list' and the internal list retreived by `a_get_list'
			-- and `a_get_next' have the same `item'
		local
			l_c_list:POINTER
			l_count:INTEGER
		do
			Result := True
			from
				l_c_list := a_get_list(item)
				l_count := 0
			until
				l_c_list.is_default_pointer or not Result
			loop
				l_count := l_count + 1
				Result := across a_list as la_list some la_list.item.item ~ l_c_list end
				l_c_list := a_get_next(l_c_list)
			end
			Result := Result or l_count ~ a_list.count
		end

	remove_from_internal_list(a_list:LIST[B2D_MEMORY_OBJECT]; a_object:B2D_MEMORY_OBJECT)
			-- Remove every element of `a_list' that have the same `item' as `a_object'
		do
			from
				a_list.start
			until
				a_list.exhausted
			loop
				if a_list.item.item ~ a_object.item then
					a_list.remove
				else
					a_list.forth
				end
			end
		end

	c_array_to_vector_list(a_pointer:POINTER; a_count:INTEGER):LIST[B2D_VECTOR_2D]
			-- Transfert internal Array of b2Vec2 into Eiffel {LIST} of {B2D_VECTOR_2D}
		do
			create {ARRAYED_LIST[B2D_VECTOR_2D]}Result.make (a_count)
			across 0 |..| (a_count - 1) as la_index loop
				Result.extend (create {B2D_VECTOR_2D}.make_from_item (
									a_pointer+(la_index.item * {MANAGED_POINTER}.pointer_bytes)
								))
			end
		ensure
			class
		end

end
