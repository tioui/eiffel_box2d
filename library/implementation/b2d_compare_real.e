note
	description: "Used to compare {REAL_32} values"
	author: "Louis Marchand"
	date: "Mon, 15 Jun 2020 23:49:13 +0000"
	revision: "0.1"

class
	B2D_COMPARE_REAL

feature -- Access

	compare_real_32(a_number_1, a_number_2:REAL_32):BOOLEAN
			-- Compare `a_number_1' and `a_number_2' to see if they are close enough
		do
			Result := (a_number_1 - a_number_2).abs < (a_number_1.abs.max (a_number_2.abs) * sqrt_epsilon).max({REAL_32}.machine_epsilon)
		ensure
			class
		end

	sqrt_epsilon:REAL_64
			-- Equal sqrt(epsilon)
		once
			Result := {DOUBLE_MATH}.sqrt({REAL_32}.machine_epsilon)
		ensure
			class
		end

end
