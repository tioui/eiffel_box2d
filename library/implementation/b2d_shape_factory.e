note
	description: "Used to create {B2D_SHAPE} from a C pointer."
	author: "Louis Marchand"
	date: "Tue, 09 Jun 2020 13:27:08 +0000"
	revision: "0.1"

class
	B2D_SHAPE_FACTORY

inherit
	B2D_ANY

create {B2D_ANY}
	default_create

feature {B2D_ANY} -- Access

	own_shape(a_item:POINTER):detachable B2D_SHAPE
			-- Get a {B2D_SHAPE} from pointer `a_item'. `a_item' will be freed.
		require
			Not_Null: not a_item.is_default_pointer
		do
			if {B2D_EXTERNAL}.b2Shape_GetType(a_item) ~ {B2D_EXTERNAL}.b2Shape_type_e_circle then
				create {B2D_SHAPE_CIRCLE}Result.own_from_item(a_item)
			elseif {B2D_EXTERNAL}.b2Shape_GetType(a_item) ~ {B2D_EXTERNAL}.b2shape_type_e_polygon then
				create {B2D_SHAPE_POLYGON}Result.own_from_item(a_item)
			elseif {B2D_EXTERNAL}.b2Shape_GetType(a_item) ~ {B2D_EXTERNAL}.b2shape_type_e_edge then
				create {B2D_SHAPE_EDGE}Result.own_from_item(a_item)
			elseif {B2D_EXTERNAL}.b2Shape_GetType(a_item) ~ {B2D_EXTERNAL}.b2shape_type_e_chain then
				create {B2D_SHAPE_CHAIN}Result.own_from_item(a_item)
			end
		ensure
			class
		end

	shared_shape(a_item:POINTER; a_with: detachable B2D_ANY):detachable B2D_SHAPE
			-- Get a {B2D_SHAPE} from pointer `a_item'. `a_item' will not be freed.
		require
			Not_Null: not a_item.is_default_pointer
		do
			if {B2D_EXTERNAL}.b2Shape_GetType(a_item) ~ {B2D_EXTERNAL}.b2Shape_type_e_circle then
				create {B2D_SHAPE_CIRCLE}Result.shared_from_item(a_item, a_with)
			elseif {B2D_EXTERNAL}.b2Shape_GetType(a_item) ~ {B2D_EXTERNAL}.b2shape_type_e_polygon then
				create {B2D_SHAPE_POLYGON}Result.shared_from_item(a_item, a_with)
			elseif {B2D_EXTERNAL}.b2Shape_GetType(a_item) ~ {B2D_EXTERNAL}.b2shape_type_e_edge then
				create {B2D_SHAPE_EDGE}Result.shared_from_item(a_item, a_with)
			elseif {B2D_EXTERNAL}.b2Shape_GetType(a_item) ~ {B2D_EXTERNAL}.b2shape_type_e_chain then
				create {B2D_SHAPE_CHAIN}Result.shared_from_item(a_item, a_with)
			end
		ensure
			class
		end

end
