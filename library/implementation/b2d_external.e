note
	description: "External routine of the Box2D wrapper."
	author: "Louis Marchand and Patrick Boucher"
	date: "Fri, 05 Jun 2020 16:59:55 +0000"
	revision: "0.1"

class
	B2D_EXTERNAL

feature -- Additions

	frozen B2dCallbacks_new:POINTER
			-- C initialisation of a B2dCallbacks
		external
			"C++ inline use <box2d_additions.h>"
		alias
			"new B2dCallbacks()"
		end

	frozen B2dCallbacks_delete(a_object:POINTER)
			-- C destruction of a B2dCallbacks
		external
			"C++ inline use <box2d_additions.h>"
		alias
			"delete ((B2dCallbacks *)$a_object)"
		end

	frozen B2dCallbacks_setRayCastCallback(a_object, a_callback:POINTER)
			-- C function B2dCallbacks.setRayCastCallback()
		external
			"C++ inline use <box2d_additions.h>"
		alias
			"((B2dCallbacks *)$a_object)->setRayCastCallback((EIF_OBJECT)$a_callback)"
		end

	frozen B2dCallbacks_setBeginContactCallback(a_object, a_callback:POINTER)
			-- C function B2dCallbacks.setBeginContactCallback()
		external
			"C++ inline use <box2d_additions.h>"
		alias
			"((B2dCallbacks *)$a_object)->setBeginContactCallback((EIF_OBJECT)$a_callback)"
		end

	frozen B2dCallbacks_setEndContactCallback(a_object, a_callback:POINTER)
			-- C function B2dCallbacks.setEndContactCallback()
		external
			"C++ inline use <box2d_additions.h>"
		alias
			"((B2dCallbacks *)$a_object)->setEndContactCallback((EIF_OBJECT)$a_callback)"
		end

	frozen B2dCallbacks_setPreSolveContactCallback(a_object, a_callback:POINTER)
			-- C function B2dCallbacks.setPreSolveContactCallback()
		external
			"C++ inline use <box2d_additions.h>"
		alias
			"((B2dCallbacks *)$a_object)->setPreSolveContactCallback((EIF_OBJECT)$a_callback)"
		end

	frozen B2dCallbacks_setPostSolveContactCallback(a_object, a_callback:POINTER)
			-- C function B2dCallbacks.setPostSolveContactCallback()
		external
			"C++ inline use <box2d_additions.h>"
		alias
			"((B2dCallbacks *)$a_object)->setPostSolveContactCallback((EIF_OBJECT)$a_callback)"
		end

	frozen B2dCallbacks_setFilterCollisionCallback(a_object, a_callback:POINTER)
			-- C function B2dCallbacks.setFilterCollisionCallback()
		external
			"C++ inline use <box2d_additions.h>"
		alias
			"((B2dCallbacks *)$a_object)->setFilterCollisionCallback((EIF_OBJECT)$a_callback)"
		end

	frozen B2dCallbacks_setAxisAlignBoundingBoxCallback(a_object, a_callback:POINTER)
			-- C function B2dCallbacks.setAxisAlignBoundingBoxCallback()
		external
			"C++ inline use <box2d_additions.h>"
		alias
			"((B2dCallbacks *)$a_object)->setAxisAlignBoundingBoxCallback((EIF_OBJECT)$a_callback) "
		end

	frozen B2dCallbacks_SetEiffelWorld(a_object, a_eiffel_object:POINTER)
			-- C function B2dCallbacks.SetEiffelWorld()
		external
			"C++ inline use <box2d_additions.h>"
		alias
			"((B2dCallbacks *)$a_object)->SetEiffelWorld((EIF_OBJECT)$a_eiffel_object)"
		end

	frozen B2dCallbacks_ClearEiffelWorld(a_object:POINTER)
			-- C function B2dCallbacks.ClearEiffelWorld()
		external
			"C++ inline use <box2d_additions.h>"
		alias
			"((B2dCallbacks *)$a_object)->ClearEiffelWorld() "
		end

	frozen B2dCallbacks_SetMustShouldCollide(a_object:POINTER; a_eiffel_object:BOOLEAN)
			-- C function B2dCallbacks.SetMustShouldCollide()
		external
			"C++ inline use <box2d_additions.h>"
		alias
			"((B2dCallbacks *)$a_object)->SetMustShouldCollide($a_eiffel_object)"
		end

	frozen B2dCallbacks_SetMustBeginContact(a_object:POINTER; a_eiffel_object:BOOLEAN)
			-- C function B2dCallbacks.SetMustBeginContact()
		external
			"C++ inline use <box2d_additions.h>"
		alias
			"((B2dCallbacks *)$a_object)->SetMustBeginContact($a_eiffel_object)"
		end

	frozen B2dCallbacks_SetMustEndContact(a_object:POINTER; a_eiffel_object:BOOLEAN)
			-- C function B2dCallbacks.SetMustEndContact()
		external
			"C++ inline use <box2d_additions.h>"
		alias
			"((B2dCallbacks *)$a_object)->SetMustEndContact($a_eiffel_object)"
		end

	frozen B2dCallbacks_SetMustPreSolve(a_object:POINTER; a_eiffel_object:BOOLEAN)
			-- C function B2dCallbacks.SetMustPreSolve()
		external
			"C++ inline use <box2d_additions.h>"
		alias
			"((B2dCallbacks *)$a_object)->SetMustPreSolve($a_eiffel_object)"
		end

	frozen B2dCallbacks_SetMustPostSolve(a_object:POINTER; a_eiffel_object:BOOLEAN)
			-- C function B2dCallbacks.SetMustPostSolve()
		external
			"C++ inline use <box2d_additions.h>"
		alias
			"((B2dCallbacks *)$a_object)->SetMustPostSolve($a_eiffel_object)"
		end

	frozen B2dCallbacks_DefaultFilter(a_object:POINTER; a_fixture_1, a_fixture_2:POINTER):BOOLEAN
			-- C function b2ContactFilter.DefaultFilter()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((B2dCallbacks *)$a_object)->DefaultFilter((b2Fixture *)$a_fixture_1, (b2Fixture *)$a_fixture_2)"
		end

	frozen adopt_eiffel_object(a_object:B2D_ANY):POINTER
			-- C function to adopt `a_object'
		external
			"C++ inline"
		alias
			"eif_adopt($a_object)"
		end

	frozen wean_eiffel_object(a_object:POINTER)
			-- C function to wean `a_object'
		external
			"C++ inline"
		alias
			"eif_wean((EIF_OBJECT)$a_object)"
		end

	frozen EiffelObjectChain_new:POINTER
			-- C initialisation of a B2dCallbacks
		external
			"C++ inline use <box2d_additions.h>"
		alias
			"new EiffelObjectChain ()"
		end

	frozen EiffelObjectChain_delete(a_object:POINTER)
			-- C destruction of a EiffelObjectChain
		external
			"C++ inline use <box2d_additions.h>"
		alias
			"delete ((EiffelObjectChain *)$a_object)"
		end

	frozen AddToEiffelObjectChain(a_chain, a_object:POINTER)
			-- C function to AddToEiffelObjectChain()
		external
			"C++ inline use <box2d_additions.h>"
		alias
			"AddToEiffelObjectChain((EiffelObjectChain *)$a_chain, (EIF_OBJECT)$a_object)"
		end

	frozen RemoveFromEiffelObjectChain(a_chain, a_object:POINTER)
			-- C function to RemoveFromEiffelObjectChain()
		external
			"C++ inline use <box2d_additions.h>"
		alias
			"RemoveFromEiffelObjectChain((EiffelObjectChain *)$a_chain, (EIF_OBJECT)$a_object)"
		end

	frozen WeanAllEiffelObjectChain(a_chain:POINTER)
			-- C function to WeanAllEiffelObjectChain()
		external
			"C++ inline use <box2d_additions.h>"
		alias
			"WeanAllEiffelObjectChain((EiffelObjectChain *)$a_chain)"
		end

	frozen CountEiffelObjectChain(a_chain:POINTER):INTEGER
			-- C function to CountEiffelObjectChain()
		external
			"C++ inline use <box2d_additions.h>"
		alias
			"CountEiffelObjectChain((EiffelObjectChain *)$a_chain)"
		end

feature -- b2AABB

	frozen b2AABB_new:POINTER
			-- C initialisation of a b2AABB
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2AABB ()"
		end

	frozen b2AABB_delete(a_object:POINTER)
			-- C destruction of a b2AABB
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2AABB *)$a_object)"
		end

	frozen b2AABB_IsValid(a_object:POINTER):BOOLEAN
			-- C function b2AABB.IsValid()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2AABB *)$a_object)->IsValid()"
		end

	frozen b2AABB_GetCenter(a_object, a_value:POINTER)
			-- C function b2AABB.GetCenter()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2AABB *)$a_object)->GetCenter();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2AABB_GetExtents(a_object, a_value:POINTER)
			-- C function b2AABB.GetExtents()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2AABB *)$a_object)->GetExtents();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2AABB_GetPerimeter(a_object:POINTER):REAL_32
			-- C function b2AABB.GetPerimeter()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2AABB *)$a_object)->GetPerimeter()"
		end

	frozen b2AABB_lowerBound_get(a_object:POINTER):POINTER
			-- C getter of b2AABB.lowerBound
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2AABB *)$a_object)->lowerBound)"
		end

	frozen b2AABB_upperBound_get(a_object:POINTER):POINTER
			-- C getter of b2AABB.upperBound
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2AABB *)$a_object)->upperBound)"
		end

	frozen b2AABB_Combine(a_object, a_other:POINTER)
			-- C function b2AABB.Combine()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2AABB *)$a_object)->Combine(*((b2AABB *)$a_other))"
		end

	frozen b2AABB_Combine2(a_object, a_other1, a_other2:POINTER)
			-- C function b2AABB.Combine()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2AABB *)$a_object)->Combine(*((b2AABB *)$a_other1),*((b2AABB *)$a_other2))"
		end

	frozen b2AABB_Contains(a_object, a_other:POINTER):BOOLEAN
			-- C function b2AABB.Contains()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2AABB *)$a_object)->Contains(*((b2AABB *)$a_other))"
		end

	frozen b2AABB_RayCast(a_object, a_output, a_input:POINTER):BOOLEAN
			-- C function b2AABB.RayCast()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2AABB *)$a_object)->RayCast((b2RayCastOutput *)$a_output, *((b2RayCastInput *)$a_input))"
		end


feature -- b2Body

	frozen b2Body_createFixture_with_definition (a_object: POINTER; a_fixture_definition: POINTER): POINTER
			-- C function b2Body.CreateFixture(const b2FixtureDef *def)
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->CreateFixture((b2FixtureDef *)$a_fixture_definition)"
		end

	frozen b2Body_createFixture_with_shape (a_object: POINTER; a_shape: POINTER; a_density: REAL_32): POINTER
			-- C function b2Body.CreateFixture(const b2Shape *shape, float density)
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->CreateFixture((b2Shape *)$a_shape, $a_density)"
		end

	frozen b2Body_destroyFixture (a_object: POINTER; a_fixture: POINTER)
			-- C function b2Body.DestroyFixture()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->DestroyFixture((b2Fixture *)$a_fixture)"
		end

	frozen b2Body_setTransform (a_object: POINTER; a_position: POINTER; a_angle: REAL_32)
			-- C function b2Body.SetTransform()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->SetTransform(*((b2Vec2 *)$a_position), $a_angle)"
		end

	frozen b2Body_SetUserData (a_object: POINTER; a_data:ANY)
			-- C function b2Body.SetUserData()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->SetUserData($a_data) "
		end

	frozen b2Body_getTransform (a_object: POINTER): POINTER
			-- C function b2Body.GetTransform()
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2Body *)$a_object)->GetTransform())"
		end

	frozen b2Body_getPosition (a_object: POINTER): POINTER
			-- C function b2Body.GetPosition()
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2Body *)$a_object)->GetPosition())"
		end

	frozen b2Body_getAngle (a_object: POINTER): REAL_32
			-- C function b2Body.GetAngle()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->GetAngle()"
		end

	frozen b2Body_getWorldCenter (a_object: POINTER): POINTER
			-- C function b2Body.GetWorldCenter()
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2Body *)$a_object)->GetWorldCenter())"
		end

	frozen b2Body_getLocalCenter (a_object: POINTER): POINTER
			-- C function b2Body.GetLocalCenter()
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2Body *)$a_object)->GetLocalCenter())"
		end

	frozen b2Body_setLinearVelocity (a_object: POINTER; a_velocity: POINTER)
			-- C function b2Body.SetLinearVelocity()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->SetLinearVelocity(*((b2Vec2 *)$a_velocity))"
		end

	frozen b2Body_getLinearVelocity (a_object: POINTER): POINTER
			-- C function b2Body.GetLinearVelocity()
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2Body *)$a_object)->GetLinearVelocity())"
		end

	frozen b2Body_setAngularVelocity (a_object: POINTER; a_radian_per_second: REAL_32)
			-- C function b2Body.SetAngularVelocity()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->SetAngularVelocity($a_radian_per_second)"
		end

	frozen b2Body_getAngularVelocity (a_object: POINTER): REAL_32
			-- C function b2Body.GetAngularVelocity()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->GetAngularVelocity()"
		end

	frozen b2Body_applyForce (a_object:POINTER; a_force:POINTER; a_world_position:POINTER; a_wake_up:BOOLEAN)
			-- C function b2Body.ApplyForce()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->ApplyForce(*((b2Vec2 *)$a_force), *((b2Vec2 *)$a_world_position), $a_wake_up)"
		end

	frozen b2Body_applyForceToCenter (a_object:POINTER; a_force:POINTER; a_wake_up:BOOLEAN)
			-- C function b2Body.ApplyForceToCenter()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->ApplyForceToCenter(*((b2Vec2 *)$a_force), $a_wake_up)"
		end

	frozen b2Body_applyTorque (a_object:POINTER; a_torque:REAL_32; a_wake_up:BOOLEAN)
			-- C function b2Body.ApplyTorque()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->ApplyTorque($a_torque, $a_wake_up)"
		end

	frozen b2Body_applyLinearImpulse (a_object:POINTER; a_impulse:POINTER; a_world_point:POINTER; a_wake_up:BOOLEAN)
			-- C function b2Body.ApplyLinearImpulse()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->ApplyLinearImpulse(*((b2Vec2 *)$a_impulse), *((b2Vec2 *)$a_world_point), $a_wake_up)"
		end

-- Could be done with Box2D 2.4.0
--
--	frozen b2Body_applyLinearImpulseToCenter (a_object:POINTER; a_impulse:POINTER; a_wake_up:BOOLEAN)
--			-- C function b2Body.ApplyLinearImpulseToCenter()
--		external
--			"C++ inline use <Box2D.h>"
--		alias
--			"((b2Body *)$a_object)->ApplyLinearImpulseToCenter(*((b2Vec2 *)$a_impulse), $a_wake_up)"
--		end

	frozen b2Body_applyAngularImpulse (a_object:POINTER; a_impulse:REAL_32; a_wake_up:BOOLEAN)
			-- C function b2Body.ApplyAngularImpulse()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->ApplyAngularImpulse($a_impulse, $a_wake_up)"
		end

	frozen b2Body_getMass (a_object: POINTER): REAL_32
			-- C function b2Body.GetMass()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->GetMass()"
		end

	frozen b2Body_getInertia (a_object:POINTER): REAL_32
			-- C function b2Body.GetInertia()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->GetInertia()"
		end

	frozen b2Body_getMassData (a_object:POINTER; a_data:POINTER)
			-- C function b2Body.GetMassData()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->GetMassData((b2MassData *)$a_data)"
		end

	frozen b2Body_setMassData (a_object:POINTER; a_data:POINTER)
			-- C function b2Body.SetMassData()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->SetMassData((b2MassData *)$a_data)"
		end

	frozen b2Body_resetMassData (a_object:POINTER)
			-- C function b2Body.ResetMassData()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->ResetMassData()"
		end

	frozen b2Body_getWorldPoint (a_object:POINTER; a_local_point:POINTER; a_world_point:POINTER)
			-- C function b2Body.GetWorldPoint()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_world_point = ((b2Body *)$a_object)->GetWorldPoint(*((b2Vec2 *)$a_local_point));
				((b2Vec2 *)$a_world_point)->Set(l_world_point.x, l_world_point.y);
					]"
		end

	frozen b2Body_getWorldVector (a_object:POINTER; a_local_vector:POINTER; a_world_vector:POINTER)
			-- C function b2Body.GetWorldVector()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_world_vector = ((b2Body *)$a_object)->GetWorldVector(*((b2Vec2 *)$a_local_vector));
				((b2Vec2 *)$a_world_vector)->Set(l_world_vector.x, l_world_vector.y);
					]"
		end

	frozen b2Body_getLocalPoint (a_object:POINTER; a_world_point:POINTER; a_local_point:POINTER)
			-- C function b2Body.GetLocalPoint()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_local_point = ((b2Body *)$a_object)->GetLocalPoint(*((b2Vec2 *)$a_world_point));
				((b2Vec2 *)$a_local_point)->Set(l_local_point.x, l_local_point.y);
					]"
		end

	frozen b2Body_getLocalVector (a_object:POINTER; a_world_vector:POINTER; a_local_vector:POINTER)
			-- C function b2Body.GetLocalVector()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_local_vector = ((b2Body *)$a_object)->GetLocalVector(*((b2Vec2 *)$a_world_vector));
				((b2Vec2 *)$a_local_vector)->Set(l_local_vector.x, l_local_vector.y);
					]"
		end

	frozen b2Body_getLinearVelocityFromWorldPoint (a_object:POINTER; a_world_point:POINTER; a_linear_velocity:POINTER)
			-- C function b2Body.GetLinearVelocityFromWorldPoint()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_linear_velocity = ((b2Body *)$a_object)->GetLinearVelocityFromWorldPoint(*((b2Vec2 *)$a_world_point));
				((b2Vec2 *)$a_linear_velocity)->Set(l_linear_velocity.x, l_linear_velocity.y);
					]"
		end

	frozen b2Body_getLinearVelocityFromLocalPoint (a_object:POINTER; a_local_point:POINTER; a_linear_velocity:POINTER)
			-- C function b2Body.GetLinearVelocityFromLocalPoint()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_linear_velocity = ((b2Body *)$a_object)->GetLinearVelocityFromLocalPoint(*((b2Vec2 *)$a_local_point));
				((b2Vec2 *)$a_linear_velocity)->Set(l_linear_velocity.x, l_linear_velocity.y);
					]"
		end

	frozen b2Body_getLinearDamping (a_object:POINTER): REAL_32
			-- C function b2Body.GetLinearDamping()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->GetLinearDamping()"
		end

	frozen b2Body_setLinearDamping (a_object:POINTER; a_value:REAL_32)
			-- C function b2Body.SetLinearDamping()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->SetLinearDamping($a_value)"
		end

	frozen b2Body_getAngularDamping (a_object:POINTER): REAL_32
			-- C function b2Body.GetAngularDamping()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->GetAngularDamping()"
		end

	frozen b2Body_setAngularDamping (a_object:POINTER; a_value:REAL_32)
			-- C function b2Body.SetAngularDamping()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->SetAngularDamping($a_value)"
		end

	frozen b2Body_getGravityScale (a_object:POINTER): REAL_32
			-- C function b2Body.GetGravityScale()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->GetGravityScale()"
		end

	frozen b2Body_setGravityScale (a_object:POINTER; a_value:REAL_32)
			-- C function b2Body.SetGravityScale()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->SetGravityScale($a_value)"
		end

	frozen b2Body_getType (a_object:POINTER): NATURAL
			-- C function b2Body.GetType()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->GetType()"
		end

	frozen b2Body_setType (a_object:POINTER; a_value:NATURAL)
			-- C function b2Body.SetType()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->SetType((b2BodyType)$a_value)"
		end

	frozen b2Body_setBullet (a_object:POINTER; a_value:BOOLEAN)
			-- C function b2Body.SetBullet()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->SetBullet($a_value)"
		end

	frozen b2Body_isBullet (a_object:POINTER): BOOLEAN
			-- C function b2Body.IsBullet()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->IsBullet()"
		end

	frozen b2Body_setSleepingAllowed (a_object:POINTER; a_value:BOOLEAN)
			-- C function b2Body.SetSleepingAllowed()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->SetSleepingAllowed($a_value)"
		end

	frozen b2Body_isSleepingAllowed (a_object:POINTER): BOOLEAN
			-- C function b2Body.IsSleepingAllowed()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->IsSleepingAllowed()"
		end

	frozen b2Body_setAwake (a_object:POINTER; a_value:BOOLEAN)
			-- C function b2Body.SetAwake()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->SetAwake($a_value)"
		end

	frozen b2Body_isAwake (a_object:POINTER): BOOLEAN
			-- C function b2Body.IsAwake()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->IsAwake()"
		end

	frozen b2Body_setActive (a_object:POINTER; a_value:BOOLEAN)
			-- C function b2Body.SetActive()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->SetActive($a_value)"
		end

	frozen b2Body_isActive (a_object:POINTER): BOOLEAN
			-- C function b2Body.IsActive()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->IsActive()"
		end

	frozen b2Body_setFixedRotation (a_object:POINTER; a_value:BOOLEAN)
			-- C function b2Body.SetFixedRotation()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->SetFixedRotation($a_value)"
		end

	frozen b2Body_isFixedRotation (a_object:POINTER): BOOLEAN
			-- C function b2Body.IsFixedRotation()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->IsFixedRotation()"
		end

	frozen b2Body_getFixtureList (a_object:POINTER): POINTER
			-- C function b2Body.GetFixtureList()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->GetFixtureList()"
		end

	frozen b2Body_getJointList (a_object:POINTER): POINTER
			-- C function b2Body.GetJointList()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->GetJointList()"
		end

	frozen b2Body_getContactList (a_object:POINTER): POINTER
		-- C function b2Body.GetContactList()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->GetContactList()"
		end

	frozen b2Body_GetNext(a_object:POINTER):POINTER
			-- C function b2Body.GetNext()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->GetNext()"
		end

	frozen b2Body_getWorld (a_object:POINTER): POINTER
			-- C function b2Body.GetWorld()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->GetWorld()"
		end

	frozen b2Body_dump (a_object:POINTER)
			-- C function b2Body.Dump()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Body *)$a_object)->Dump()"
		end

	frozen b2Body_type_staticBody: NATURAL
			-- C value of constant b2_staticBody
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2BodyType::b2_staticBody"
		end

	frozen b2Body_type_kinematicBody: NATURAL
			-- C value of constant b2_kinematicBody
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2BodyType::b2_kinematicBody"
		end

	frozen b2Body_type_dynamicBody: NATURAL
			-- C value of constant b2_dynamicBody
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2BodyType::b2_dynamicBody"
		end

feature -- b2BodyDef

	frozen b2BodyDef_new: POINTER
			-- C initialization of a b2BodyDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2BodyDef ()"
		end

	frozen b2BodyDef_delete (a_object: POINTER)
			-- C destruction of a b2BodyDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2BodyDef *)$a_object)"
		end

	frozen b2BodyDef_type_get (a_object: POINTER): NATURAL
			-- C getter of b2BodyDef.type
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2BodyDef *)$a_object)->type"
		end

	frozen b2BodyDef_type_set (a_object: POINTER; a_value: NATURAL)
			-- C setter of b2BodyDef.type
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2BodyDef *)$a_object)->type = (b2BodyType)$a_value"
		end

	frozen b2BodyDef_position_get (a_object: POINTER): POINTER
			-- C getter of b2BodyDef.position
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2BodyDef *)$a_object)->position)"
		end

	frozen b2BodyDef_angle_get (a_object: POINTER): REAL_32
			-- C getter of b2BodyDef.angle
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2BodyDef *)$a_object)->angle"
		end

	frozen b2BodyDef_angle_set (a_object: POINTER; a_value: REAL_32)
			-- C setter of b2BodyDef.angle
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2BodyDef *)$a_object)->angle = $a_value"
		end

	frozen b2BodyDef_linearVelocity_get (a_object: POINTER): POINTER
			-- C getter of b2BodyDef.linearVelocity
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2BodyDef *)$a_object)->linearVelocity)"
		end

	frozen b2BodyDef_angularVelocity_get (a_object: POINTER): REAL_32
			-- C getter of b2BodyDef.angularVelocity
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2BodyDef *)$a_object)->angularVelocity"
		end

	frozen b2BodyDef_angularVelocity_set (a_object: POINTER; a_value: REAL_32)
			-- C setter of b2BodyDef.angularVelocity
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2BodyDef *)$a_object)->angularVelocity = $a_value"
		end

	frozen b2BodyDef_linearDamping_get (a_object: POINTER): REAL_32
			-- C getter of b2BoyDef.linearDamping
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2BodyDef *)$a_object)->linearDamping"
		end

	frozen b2BodyDef_linearDamping_set (a_object: POINTER; a_value: REAL_32)
			-- C setter of b2BodyDef.linearDamping
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2BodyDef *)$a_object)->linearDamping = $a_value"
		end

	frozen b2BodyDef_angularDamping_get (a_object: POINTER): REAL_32
			-- C getter of b2BodyDef.angularDamping
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2BodyDef *)$a_object)->angularDamping"
		end

	frozen b2BodyDef_angularDamping_set (a_object: POINTER; a_value: REAL_32)
			-- C setter of b2BodyDef.angularDamping
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2BodyDef *)$a_object)->angularDamping = $a_value"
		end

	frozen b2BodyDef_allowSleep_get (a_object: POINTER): BOOLEAN
			-- C getter of b2BodyDef.allowSleep
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2BodyDef *)$a_object)->allowSleep"
		end

	frozen b2BodyDef_allowSleep_set (a_object: POINTER; a_value: BOOLEAN)
			-- C setter of b2BodyDef.allowSleep
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2BodyDef *)$a_object)->allowSleep = $a_value"
		end

	frozen b2BodyDef_awake_get (a_object: POINTER): BOOLEAN
			-- C getter of b2BodyDef.awake
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2BodyDef *)$a_object)->awake"
		end

	frozen b2BodyDef_awake_set (a_object: POINTER; a_value: BOOLEAN)
			-- C setter of b2BodyDef.awake
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2BodyDef *)$a_object)->awake = $a_value"
		end

	frozen b2BodyDef_fixedRotation_get (a_object: POINTER): BOOLEAN
			-- C getter of b2BodyDef.fixedRotation
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2BodyDef *)$a_object)->fixedRotation"
		end

	frozen b2BodyDef_fixedRotation_set (a_object: POINTER; a_value: BOOLEAN)
			-- C setter of b2BodyDef.fixedRotation
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2BodyDef *)$a_object)->fixedRotation = $a_value"
		end

	frozen b2BodyDef_bullet_get (a_object: POINTER): BOOLEAN
			-- C getter of b2BodyDef.bullet
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2BodyDef *)$a_object)->bullet"
		end

	frozen b2BodyDef_bullet_set (a_object: POINTER; a_value: BOOLEAN)
			-- C setter of b2BodyDef.bullet
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2BodyDef *)$a_object)->bullet = $a_value"
		end

	frozen b2BodyDef_active_get (a_object: POINTER): BOOLEAN
			-- C getter of b2BodyDef.active
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2BodyDef *)$a_object)->active"
		end

	frozen b2BodyDef_active_set (a_object: POINTER; a_value: BOOLEAN)
			-- C setter of b2BodyDef.active
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2BodyDef *)$a_object)->active = $a_value"
		end

	frozen b2BodyDef_gravityScale_get (a_object: POINTER): REAL_32
			-- C getter of b2BodyDef.gravityScale
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2BodyDef *)$a_object)->gravityScale"
		end

	frozen b2BodyDef_gravityScale_set (a_object: POINTER; a_value: REAL_32)
			-- C setter of b2BodyDef.gravityScale
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2BodyDef *)$a_object)->gravityScale = $a_value"
		end

feature -- b2ChainShape

	frozen b2ChainShape_new:POINTER
			-- C initialisation of a b2ChainShape
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2ChainShape ()"
		end

	frozen b2ChainShape_delete(a_object:POINTER)
			-- C destruction of a b2ChainShape
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2ChainShape *)$a_object)"
		end

	frozen b2ChainShape_CreateLoop(a_object, a_vertices:POINTER; a_count:INTEGER)
			-- C function b2ChainShape.CreateLoop()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2ChainShape *)$a_object)->CreateLoop((b2Vec2 *)$a_vertices, $a_count)"
		end

	frozen b2ChainShape_CreateChain(a_object, a_vertices:POINTER; a_count:INTEGER)
			-- C function b2ChainShape.CreateChain()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2ChainShape *)$a_object)->CreateChain((b2Vec2 *)$a_vertices, $a_count)"
		end

	frozen b2ChainShape_GetChildCount(a_object:POINTER):INTEGER_32
			-- C function b2ChainShape.GetChildCount()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2ChainShape *)$a_object)->GetChildCount()"
		end

	frozen b2ChainShape_SetPrevVertex(a_object, a_vertex:POINTER)
			-- C function b2ChainShape.SetPrevVertex()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2ChainShape *)$a_object)->SetPrevVertex(*((b2Vec2 *) $a_vertex))"
		end

	frozen b2ChainShape_SetNextVertex(a_object, a_vertex:POINTER)
			-- C function b2ChainShape.SetNextVertex()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2ChainShape *)$a_object)->SetNextVertex(*((b2Vec2 *) $a_vertex))"
		end

	frozen b2ChainShape_GetChildEdge(a_object, a_edge:POINTER; a_index:INTEGER_32)
			-- C function b2ChainShape.GetChildEdge()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2ChainShape *)$a_object)->GetChildEdge((b2EdgeShape *) $a_edge, $a_index)"
		end


	frozen b2ChainShape_m_vertices_get(a_object:POINTER):POINTER
			-- C getter of b2ChainShape.m_vertices
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2ChainShape *)$a_object)->m_vertices"
		end

	frozen b2ChainShape_m_count_get(a_object:POINTER):INTEGER_32
			-- C getter of b2ChainShape.m_count
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2ChainShape *)$a_object)->m_count"
		end

	frozen b2ChainShape_m_prevVertex_get(a_object:POINTER):POINTER
			-- C getter of b2ChainShape.m_prevVertex
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2ChainShape *)$a_object)->m_prevVertex)"
		end

	frozen b2ChainShape_m_nextVertex_get(a_object:POINTER):POINTER
			-- C getter of b2ChainShape.m_nextVertex
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2ChainShape *)$a_object)->m_nextVertex)"
		end

	frozen b2ChainShape_m_hasPrevVertex_get(a_object:POINTER):BOOLEAN
			-- C getter of b2ChainShape.m_hasPrevVertex
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2ChainShape *)$a_object)->m_hasPrevVertex"
		end

	frozen b2ChainShape_m_hasPrevVertex_set(a_object:POINTER; a_value:BOOLEAN)
			-- C setter of b2ChainShape.m_hasPrevVertex
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2ChainShape *)$a_object)->m_hasPrevVertex = $a_value"
		end

	frozen b2ChainShape_m_hasNextVertex_get(a_object:POINTER):BOOLEAN
			-- C getter of b2ChainShape.m_hasNextVertex
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2ChainShape *)$a_object)->m_hasNextVertex"
		end

	frozen b2ChainShape_m_hasNextVertex_set(a_object:POINTER; a_value:BOOLEAN)
			-- C setter of b2ChainShape.m_hasNextVertex
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2ChainShape *)$a_object)->m_hasNextVertex = $a_value"
		end

	frozen b2ChainShape_TestPoint(a_object, a_transform, a_point:POINTER):BOOLEAN
			-- C function b2ChainShape.TestPoint()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2ChainShape *)$a_object)->TestPoint(*((b2Transform *)$a_transform), *((b2Vec2 *)$a_point))"
		end

	frozen b2ChainShape_RayCast(a_object, a_output, a_input, a_transform:POINTER; a_child_index:INTEGER_32):BOOLEAN
			-- C function b2ChainShape.RayCast()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2ChainShape *)$a_object)->RayCast((b2RayCastOutput *)$a_output, *((b2RayCastInput *)$a_input), *((b2Transform *)$a_transform), $a_child_index)"
		end

	frozen b2ChainShape_ComputeMass(a_object, a_mass_data:POINTER; a_density:REAL_32)
			-- C function b2ChainShape.ComputeMass()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2ChainShape *)$a_object)->ComputeMass((b2MassData *)$a_mass_data, $a_density)"
		end

	frozen b2ChainShape_ComputeAABB(a_object, a_output, a_transform:POINTER; a_child_index:INTEGER_32)
			-- C function b2ChainShape.ComputeAABB()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2ChainShape *)$a_object)->ComputeAABB((b2AABB *)$a_output, *((b2Transform *)$a_transform), $a_child_index)"
		end


feature -- b2CircleShape

	frozen b2CircleShape_new:POINTER
			-- C initialisation of a b2CircleShape
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2CircleShape ()"
		end

	frozen b2CircleShape_delete(a_object:POINTER)
			-- C destruction of a b2CircleShape
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2CircleShape *)$a_object)"
		end

	frozen b2CircleShape_m_p_get(a_object:POINTER):POINTER
			-- C getter of b2CircleShape.m_p
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2CircleShape *)$a_object)->m_p)"
		end


	frozen b2CircleShape_m_radius_get(a_object:POINTER):REAL_32
			-- C getter of b2CircleShape.m_radius
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2CircleShape *)$a_object)->m_radius"
		end

	frozen b2CircleShape_m_radius_set(a_object:POINTER; a_value:REAL_32)
			-- C setter of b2CircleShape.m_radius
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2CircleShape *)$a_object)->m_radius = $a_value"
		end

	frozen b2CircleShape_RayCast(a_object, a_output, a_input, a_transform:POINTER; a_child_index:INTEGER_32):BOOLEAN
			-- C function b2CircleShape.RayCast()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2CircleShape *)$a_object)->RayCast((b2RayCastOutput *)$a_output, *((b2RayCastInput *)$a_input), *((b2Transform *)$a_transform), $a_child_index)"
		end

	frozen b2CircleShape_ComputeMass(a_object, a_mass_data:POINTER; a_density:REAL_32)
			-- C function b2CircleShape.ComputeMass()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2CircleShape *)$a_object)->ComputeMass((b2MassData *)$a_mass_data, $a_density)"
		end

	frozen b2CircleShape_ComputeAABB(a_object, a_output, a_transform:POINTER; a_child_index:INTEGER_32)
			-- C function b2CircleShape.ComputeAABB()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2CircleShape *)$a_object)->ComputeAABB((b2AABB *)$a_output, *((b2Transform *)$a_transform), $a_child_index)"
		end

	frozen b2CircleShape_GetChildCount(a_object:POINTER):INTEGER_32
			-- C function b2CircleShape.GetChildCount()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2CircleShape *)$a_object)->GetChildCount()"
		end

	frozen b2CircleShape_TestPoint(a_object, a_transform, a_point:POINTER):BOOLEAN
			-- C function b2CircleShape.TestPoint()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2CircleShape *)$a_object)->TestPoint(*((b2Transform *)$a_transform), *((b2Vec2 *)$a_point))"
		end

feature -- b2Contact

	frozen b2Contact_GetNext(a_object:POINTER):POINTER
			-- C function b2Contact.GetNext()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Contact *)$a_object)->GetNext()"
		end

	frozen b2Contact_GetManifold(a_object:POINTER):POINTER
			-- C function b2Contact.GetManifold()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Contact *)$a_object)->GetManifold()"
		end

	frozen b2Contact_GetWorldManifold(a_object, a_value:POINTER)
			-- C function b2Contact.GetWorldManifold()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Contact *)$a_object)->GetWorldManifold((b2WorldManifold *)$a_value)"
		end

	frozen b2Contact_IsTouching(a_object:POINTER):BOOLEAN
			-- C function b2Contact.IsTouching()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Contact *)$a_object)->IsTouching()"
		end

	frozen b2Contact_IsEnabled(a_object:POINTER):BOOLEAN
			-- C function b2Contact.IsEnabled()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Contact *)$a_object)->IsEnabled()"
		end

	frozen b2Contact_SetEnabled(a_object:POINTER; a_value:BOOLEAN)
			-- C function b2Contact.SetEnabled()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Contact *)$a_object)->SetEnabled($a_value)"
		end

	frozen b2Contact_GetFixtureA(a_object:POINTER):POINTER
			-- C function b2Contact.GetFixtureA()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Contact *)$a_object)->GetFixtureA()"
		end

	frozen b2Contact_GetFixtureB(a_object:POINTER):POINTER
			-- C function b2Contact.GetFixtureB()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Contact *)$a_object)->GetFixtureB()"
		end

	frozen b2Contact_GetChildIndexA(a_object:POINTER):INTEGER_32
			-- C function b2Contact.GetChildIndexA()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Contact *)$a_object)->GetChildIndexA()"
		end

	frozen b2Contact_GetChildIndexB(a_object:POINTER):INTEGER_32
			-- C function b2Contact.GetChildIndexB()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Contact *)$a_object)->GetChildIndexB()"
		end

	frozen b2Contact_SetFriction(a_object:POINTER; a_value:REAL_32)
			-- C function b2Contact.SetFriction()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Contact *)$a_object)->SetFriction($a_value)"
		end

	frozen b2Contact_GetFriction(a_object:POINTER):REAL_32
			-- C function b2Contact.GetFriction()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Contact *)$a_object)->GetFriction()"
		end

	frozen b2Contact_ResetFriction(a_object:POINTER)
			-- C function b2Contact.ResetFriction()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Contact *)$a_object)->ResetFriction()"
		end

	frozen b2Contact_SetRestitution(a_object:POINTER; a_value:REAL_32)
			-- C function b2Contact.SetRestitution()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Contact *)$a_object)->SetRestitution($a_value)"
		end

	frozen b2Contact_GetRestitution(a_object:POINTER):REAL_32
			-- C function b2Contact.GetRestitution()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Contact *)$a_object)->GetRestitution()"
		end

	frozen b2Contact_ResetRestitution(a_object:POINTER)
			-- C function b2Contact.ResetRestitution()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Contact *)$a_object)->ResetRestitution()"
		end

	frozen b2Contact_SetTangentSpeed(a_object:POINTER; a_value:REAL_32)
			-- C function b2Contact.SetTangentSpeed()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Contact *)$a_object)->SetTangentSpeed($a_value)"
		end

	frozen b2Contact_GetTangentSpeed(a_object:POINTER):REAL_32
			-- C function b2Contact.GetTangentSpeed()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Contact *)$a_object)->GetTangentSpeed()"
		end

	frozen b2Contact_Evaluate(a_object, a_manifold, a_transform_1, a_transform_2:POINTER)
			-- C function b2Contact.Evaluate()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Contact *)$a_object)->Evaluate((b2Manifold *)$a_manifold, *((b2Transform *)$a_transform_1), *((b2Transform *)$a_transform_2))"
		end


feature -- b2ContactImpulse

	frozen b2ContactImpulse_count (a_object:POINTER):INTEGER_32
			-- C getter of b2ContactImpulse.count
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2ContactImpulse *)$a_object)->count"
		end

	frozen b2ContactImpulse_tangentImpulses_get_at (a_object:POINTER; a_index:INTEGER):REAL_32
			-- C getter of b2ContactImpulse.tangentImpulses[a_index]
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2ContactImpulse *)$a_object)->tangentImpulses[$a_index]"
		end

	frozen b2ContactImpulse_normalImpulses_get_at (a_object:POINTER; a_index:INTEGER):REAL_32
			-- C getter of b2ContactImpulse.normalImpulses[a_index]
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2ContactImpulse *)$a_object)->normalImpulses[$a_index]"
		end

feature -- b2DistanceJoint

	frozen b2DistanceJoint_GetAnchorA(a_object, a_value:POINTER)
			-- C function b2DistanceJoint.GetAnchorA()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2DistanceJoint *)$a_object)->GetAnchorA();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2DistanceJoint_GetAnchorB(a_object, a_value:POINTER)
			-- C function b2DistanceJoint.GetAnchorB()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2DistanceJoint *)$a_object)->GetAnchorB();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2DistanceJoint_GetReactionForce(a_object, a_value:POINTER; a_inv_dt:REAL_32)
			-- C function b2DistanceJoint.GetReactionForce()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2DistanceJoint *)$a_object)->GetReactionForce($a_inv_dt);
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2DistanceJoint_GetReactionTorque(a_object:POINTER; a_inv_dt:REAL_32):REAL_32
			-- C function b2DistanceJoint.GetReactionTorque()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2DistanceJoint *)$a_object)->GetReactionTorque($a_inv_dt)"
		end

	frozen b2DistanceJoint_ShiftOrigin(a_object, a_value:POINTER)
			-- C function b2DistanceJoint.ShiftOrigin()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2DistanceJoint *)$a_object)->ShiftOrigin(*((b2Vec2 *)$a_value))"
		end

feature -- b2DistanceJointDef

	frozen b2DistanceJointDef_new:POINTER
			-- C initialisation of a b2DistanceJointDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2DistanceJointDef ()"
		end

	frozen b2DistanceJointDef_delete(a_object:POINTER)
			-- C destruction of a b2DistanceJointDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2DistanceJointDef *)$a_object)"
		end

	frozen b2DistanceJointDef_Initialize(a_object, a_body_a, a_body_b, a_anchor_a, a_anchor_b:POINTER)
			-- C function b2DistanceJointDef.Set()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2DistanceJointDef *)$a_object)->Initialize((b2Body *) $a_body_a, (b2Body *) $a_body_b, *((b2Vec2 *) $a_anchor_a), *((b2Vec2 *) $a_anchor_b))"
		end

	frozen b2DistanceJointDef_localAnchorA_get(a_object:POINTER):POINTER
			-- C getter of b2DistanceJointDef.localAnchorA
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2DistanceJointDef *)$a_object)->localAnchorA)"
		end

	frozen b2DistanceJointDef_localAnchorB_get(a_object:POINTER):POINTER
			-- C getter of b2DistanceJointDef.localAnchorB
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2DistanceJointDef *)$a_object)->localAnchorB)"
		end

	frozen b2DistanceJointDef_length_get(a_object:POINTER):REAL_32
			-- C getter of b2DistanceJointDef.length
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2DistanceJointDef *)$a_object)->length"
		end

	frozen b2DistanceJointDef_length_set(a_object:POINTER; a_value:REAL_32)
			-- C setter of b2DistanceJointDef.length
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2DistanceJointDef *)$a_object)->length = $a_value"
		end

	frozen b2DistanceJointDef_frequencyHz_get(a_object:POINTER):REAL_32
			-- C getter of b2DistanceJointDef.frequencyHz
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2DistanceJointDef *)$a_object)->frequencyHz"
		end

	frozen b2DistanceJointDef_frequencyHz_set(a_object:POINTER; a_value:REAL_32)
			-- C setter of b2DistanceJointDef.frequencyHz
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2DistanceJointDef *)$a_object)->frequencyHz = $a_value"
		end

	frozen b2DistanceJointDef_dampingRatio_get(a_object:POINTER):REAL_32
			-- C getter of b2DistanceJointDef.dampingRatio
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2DistanceJointDef *)$a_object)->dampingRatio"
		end

	frozen b2DistanceJointDef_dampingRatio_set(a_object:POINTER; a_value:REAL_32)
			-- C setter of b2DistanceJointDef.dampingRatio
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2DistanceJointDef *)$a_object)->dampingRatio = $a_value"
		end

feature -- b2EdgeShape

	frozen b2EdgeShape_new:POINTER
			-- C initialisation of a b2EdgeShape
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2EdgeShape ()"
		end

	frozen b2EdgeShape_delete(a_object:POINTER)
			-- C destruction of a b2EdgeShape
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2EdgeShape *)$a_object)"
		end

	frozen b2EdgeShape_Set(a_object, a_v1, a_v2:POINTER)
			-- C function b2EdgeShape.Set()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2EdgeShape *)$a_object)->Set(*((b2Vec2 *) $a_v1), *((b2Vec2 *) $a_v2))"
		end

	frozen b2EdgeShape_m_vertex1_get(a_object:POINTER):POINTER
			-- C getter of b2EdgeShape.m_vertex1
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2EdgeShape *)$a_object)->m_vertex1)"
		end

	frozen b2EdgeShape_m_vertex2_get(a_object:POINTER):POINTER
			-- C getter of b2EdgeShape.m_vertex2
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2EdgeShape *)$a_object)->m_vertex2)"
		end

	frozen b2EdgeShape_m_vertex0_get(a_object:POINTER):POINTER
			-- C getter of b2EdgeShape.m_vertex0
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2EdgeShape *)$a_object)->m_vertex0)"
		end

	frozen b2EdgeShape_m_vertex3_get(a_object:POINTER):POINTER
			-- C getter of b2EdgeShape.m_vertex3
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2EdgeShape *)$a_object)->m_vertex3)"
		end

	frozen b2EdgeShape_m_hasVertex0_get(a_object:POINTER):BOOLEAN
			-- C getter of b2EdgeShape.m_hasVertex0
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2EdgeShape *)$a_object)->m_hasVertex0"
		end

	frozen b2EdgeShape_m_hasVertex0_set(a_object:POINTER; a_value:BOOLEAN)
			-- C setter of b2EdgeShape.m_hasVertex0
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2EdgeShape *)$a_object)->m_hasVertex0 = $a_value"
		end

	frozen b2EdgeShape_m_hasVertex3_get(a_object:POINTER):BOOLEAN
			-- C getter of b2EdgeShape.m_hasVertex3
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2EdgeShape *)$a_object)->m_hasVertex3"
		end

	frozen b2EdgeShape_m_hasVertex3_set(a_object:POINTER; a_value:BOOLEAN)
			-- C setter of b2EdgeShape.m_hasVertex3
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2EdgeShape *)$a_object)->m_hasVertex3 = $a_value"
		end

	frozen b2EdgeShape_RayCast(a_object, a_output, a_input, a_transform:POINTER; a_child_index:INTEGER_32):BOOLEAN
			-- C function b2EdgeShape.RayCast()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2EdgeShape *)$a_object)->RayCast((b2RayCastOutput *)$a_output, *((b2RayCastInput *)$a_input), *((b2Transform *)$a_transform), $a_child_index)"
		end

	frozen b2EdgeShape_ComputeMass(a_object, a_mass_data:POINTER; a_density:REAL_32)
			-- C function b2EdgeShape.ComputeMass()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2EdgeShape *)$a_object)->ComputeMass((b2MassData *)$a_mass_data, $a_density)"
		end

	frozen b2EdgeShape_ComputeAABB(a_object, a_output, a_transform:POINTER; a_child_index:INTEGER_32)
			-- C function b2EdgeShape.ComputeAABB()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2EdgeShape *)$a_object)->ComputeAABB((b2AABB *)$a_output, *((b2Transform *)$a_transform), $a_child_index)"
		end

	frozen b2EdgeShape_GetChildCount(a_object:POINTER):INTEGER_32
			-- C function b2EdgeShape.GetChildCount()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2EdgeShape *)$a_object)->GetChildCount()"
		end

	frozen b2EdgeShape_TestPoint(a_object, a_transform, a_point:POINTER):BOOLEAN
			-- C function b2EdgeShape.TestPoint()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2EdgeShape *)$a_object)->TestPoint(*((b2Transform *)$a_transform), *((b2Vec2 *)$a_point))"
		end

feature -- b2Filter

	frozen b2Filter_new:POINTER
			-- C initialisation of a b2Filter
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2Filter ()"
		end

	frozen b2Filter_delete(a_object:POINTER)
			-- C destruction of a b2Filter
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2Filter *)$a_object)"
		end

	frozen b2Filter_categoryBits_get(a_object:POINTER):NATURAL_16
			-- C getter of b2Filter.categoryBits
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Filter *)$a_object)->categoryBits"
		end

	frozen b2Filter_categoryBits_set(a_object:POINTER; a_value:NATURAL_16)
			-- C setter of b2Filter.categoryBits
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Filter *)$a_object)->categoryBits = $a_value"
		end

	frozen b2Filter_maskBits_get(a_object:POINTER):NATURAL_16
			-- C getter of b2Filter.maskBits
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Filter *)$a_object)->maskBits"
		end

	frozen b2Filter_maskBits_set(a_object:POINTER; a_value:NATURAL_16)
			-- C setter of b2Filter.maskBits
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Filter *)$a_object)->maskBits = $a_value"
		end

	frozen b2Filter_groupIndex_get(a_object:POINTER):INTEGER_16
			-- C getter of b2Filter.groupIndex
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Filter *)$a_object)->groupIndex"
		end

	frozen b2Filter_groupIndex_set(a_object:POINTER; a_value:INTEGER_16)
			-- C setter of b2Filter.groupIndex
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Filter *)$a_object)->groupIndex = $a_value"
		end

feature -- b2Fixture

	frozen b2Fixture_GetShape(a_object:POINTER):POINTER
			-- C function b2Fixture.GetShape()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Fixture *)$a_object)->GetShape()"
		end

	frozen b2Fixture_GetBody(a_object:POINTER):POINTER
			-- C function b2Fixture.GetBody()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Fixture *)$a_object)->GetBody()"
		end

	frozen b2Fixture_GetUserData(a_object:POINTER):POINTER
			-- C function b2Fixture.GetUserData()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Fixture *)$a_object)->GetUserData()"
		end

	frozen b2Fixture_SetUserData(a_object, a_data:POINTER)
			-- C function b2Fixture.SetUserData()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Fixture *)$a_object)->SetUserData($a_data)"
		end

	frozen b2Fixture_IsSensor(a_object:POINTER):BOOLEAN
			-- C function b2Fixture.IsSensor()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Fixture *)$a_object)->IsSensor()"
		end

	frozen b2Fixture_SetSensor(a_object:POINTER; a_value:BOOLEAN)
			-- C function b2Fixture.SetSensor()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Fixture *)$a_object)->SetSensor($a_value)"
		end

	frozen b2Fixture_GetFilterData(a_object:POINTER):POINTER
			-- C function b2Fixture.GetFilterData()
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2Fixture *)$a_object)->GetFilterData())"
		end

	frozen b2Fixture_SetFilterData(a_object, a_value:POINTER)
			-- C function b2Fixture.SetFilterData()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Fixture *)$a_object)->SetFilterData(*((b2Filter *)$a_value))"
		end

	frozen b2Fixture_Refilter(a_object:POINTER)
			-- C function b2Fixture.Refilter()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Fixture *)$a_object)->Refilter()"
		end

	frozen b2Fixture_GetDensity(a_object:POINTER):REAL_32
			-- C function b2Fixture.GetDensity()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Fixture *)$a_object)->GetDensity()"
		end

	frozen b2Fixture_SetDensity(a_object:POINTER; a_value:REAL_32)
			-- C function b2Fixture.SetDensity()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Fixture *)$a_object)->SetDensity($a_value)"
		end

	frozen b2Fixture_GetFriction(a_object:POINTER):REAL_32
			-- C function b2Fixture.GetFriction()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Fixture *)$a_object)->GetFriction()"
		end

	frozen b2Fixture_SetFriction(a_object:POINTER; a_value:REAL_32)
			-- C function b2Fixture.SetFriction()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Fixture *)$a_object)->SetFriction($a_value)"
		end

	frozen b2Fixture_GetRestitution(a_object:POINTER):REAL_32
			-- C function b2Fixture.GetRestitution()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Fixture *)$a_object)->GetRestitution()"
		end

	frozen b2Fixture_SetRestitution(a_object:POINTER; a_value:REAL_32)
			-- C function b2Fixture.SetRestitution()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Fixture *)$a_object)->SetRestitution($a_value)"
		end

feature -- b2FixtureDef

	frozen b2FixtureDef_new:POINTER
			-- C initialisation of a b2FixtureDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2FixtureDef ()"
		end

	frozen b2FixtureDef_delete(a_object:POINTER)
			-- C destruction of a b2FixtureDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2FixtureDef *)$a_object)"
		end

	frozen b2FixtureDef_shape_set(a_object:POINTER; a_value:POINTER)
			-- C setter of b2FixtureDef.shape
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2FixtureDef *)$a_object)->shape = (b2Shape *)$a_value"
		end

	frozen b2FixtureDef_shape_get(a_object:POINTER):POINTER
			-- C getter of b2FixtureDef.shape
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2FixtureDef *)$a_object)->shape"
		end

	frozen b2FixtureDef_friction_get(a_object:POINTER):REAL_32
			-- C getter of b2FixtureDef.friction
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2FixtureDef *)$a_object)->friction"
		end

	frozen b2FixtureDef_friction_set(a_object:POINTER; a_value:REAL_32)
			-- C setter of b2FixtureDef.friction
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2FixtureDef *)$a_object)->friction = $a_value"
		end

	frozen b2FixtureDef_restitution_get(a_object:POINTER):REAL_32
			-- C getter of b2FixtureDef.restitution
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2FixtureDef *)$a_object)->restitution"
		end

	frozen b2FixtureDef_restitution_set(a_object:POINTER; a_value:REAL_32)
			-- C setter of b2FixtureDef.restitution
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2FixtureDef *)$a_object)->restitution = $a_value"
		end

	frozen b2FixtureDef_density_get(a_object:POINTER):REAL_32
			-- C getter of b2FixtureDef.density
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2FixtureDef *)$a_object)->density"
		end

	frozen b2FixtureDef_density_set(a_object:POINTER; a_value:REAL_32)
			-- C setter of b2FixtureDef.density
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2FixtureDef *)$a_object)->density = $a_value"
		end

	frozen b2FixtureDef_isSensor_get(a_object:POINTER):BOOLEAN
			-- C getter of b2FixtureDef.isSensor
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2FixtureDef *)$a_object)->isSensor"
		end

	frozen b2FixtureDef_isSensor_set(a_object:POINTER; a_value:BOOLEAN)
			-- C setter of b2FixtureDef.isSensor
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2FixtureDef *)$a_object)->isSensor = $a_value"
		end

	frozen b2FixtureDef_filter_get(a_object:POINTER):POINTER
			-- C getter of b2FixtureDef.filter
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2FixtureDef *)$a_object)->filter)"
		end

feature -- b2FrictionJoint

	frozen b2FrictionJoint_GetAnchorA(a_object, a_value:POINTER)
			-- C function b2FrictionJoint.GetAnchorA()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2FrictionJoint *)$a_object)->GetAnchorA();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2FrictionJoint_GetAnchorB(a_object, a_value:POINTER)
			-- C function b2FrictionJoint.GetAnchorB()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2FrictionJoint *)$a_object)->GetAnchorB();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2FrictionJoint_GetReactionForce(a_object, a_value:POINTER; a_inv_dt:REAL_32)
			-- C function b2FrictionJoint.GetReactionForce()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2FrictionJoint *)$a_object)->GetReactionForce($a_inv_dt);
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2FrictionJoint_GetReactionTorque(a_object:POINTER; a_inv_dt:REAL_32):REAL_32
			-- C function b2FrictionJoint.GetReactionTorque()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2FrictionJoint *)$a_object)->GetReactionTorque($a_inv_dt)"
		end

	frozen b2FrictionJoint_ShiftOrigin(a_object, a_value:POINTER)
			-- C function b2FrictionJoint.ShiftOrigin()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2FrictionJoint *)$a_object)->ShiftOrigin(*((b2Vec2 *)$a_value))"
		end

feature -- b2FrictionJointDef

	frozen b2FrictionJointDef_new:POINTER
			-- C initialization of a b2FrictionJointDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2FrictionJointDef ()"
		end

	frozen b2FrictionJointDef_delete (a_object:POINTER)
			-- C destruction of a b2FrictionJointDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2FrictionJointDef *)$a_object)"
		end

	frozen b2FrictionJointDef_initialize (a_object, a_body_a, a_body_b, a_anchor:POINTER)
			-- C function b2FrictionJointDef.initialize()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2FrictionJointDef *)$a_object)->Initialize ((b2Body *)$a_body_a, (b2Body *)$a_body_b, *((b2Vec2 *)$a_anchor))"
		end

	frozen b2FrictionJointDef_localAnchorA_get (a_object:POINTER):POINTER
			-- C getter of b2FrictionJointDef.localAnchorA
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2FrictionJointDef *)$a_object)->localAnchorA)"
		end

	frozen b2FrictionJointDef_localAnchorA_set (a_object, a_value:POINTER)
			-- C setter of b2FrictionJointDef.localAnchorA
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2FrictionJointDef *)$a_object)->localAnchorA = *((b2Vec2 *)$a_value)"
		end

	frozen b2FrictionJointDef_localAnchorB_get (a_object:POINTER):POINTER
			-- C getter of b2FrictionJointDef.localAnchorB
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2FrictionJointDef *)$a_object)->localAnchorB)"
		end

	frozen b2FrictionJointDef_localAnchorB_set (a_object, a_value:POINTER)
			-- C setter of b2FrictionJointDef.localAnchorB
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2FrictionJointDef *)$a_object)->localAnchorB = *((b2Vec2 *)$a_value)"
		end

	frozen b2FrictionJointDef_maxForce_get (a_object:POINTER):REAL_32
			-- C getter of b2FrictionJointDef.maxForce
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2FrictionJointDef *)$a_object)->maxForce"
		end

	frozen b2FrictionJointDef_maxForce_set (a_object:POINTER; a_value:REAL_32)
			-- C setter of b2FrictionJointDef.maxForce
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2FrictionJointDef *)$a_object)->maxForce = $a_value"
		end

	frozen b2FrictionJointDef_maxTorque_get (a_object:POINTER):REAL_32
			-- C getter of b2FrictionJointDef.maxTorque
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2FrictionJointDef *)$a_object)->maxTorque"
		end

	frozen b2FrictionJointDef_maxTorque_set (a_object:POINTER; a_value:REAL_32)
			-- C setter of b2FrictionJointDef.maxTorque
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2FrictionJointDef *)$a_object)->maxTorque = $a_value"
		end

feature -- b2GearJoint

	frozen b2GearJoint_GetAnchorA(a_object, a_value:POINTER)
			-- C function b2GearJoint.GetAnchorA()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2GearJoint *)$a_object)->GetAnchorA();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2GearJoint_GetAnchorB(a_object, a_value:POINTER)
			-- C function b2GearJoint.GetAnchorB()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2GearJoint *)$a_object)->GetAnchorB();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2GearJoint_GetReactionForce(a_object, a_value:POINTER; a_inv_dt:REAL_32)
			-- C function b2GearJoint.GetReactionForce()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2GearJoint *)$a_object)->GetReactionForce($a_inv_dt);
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2GearJoint_GetReactionTorque(a_object:POINTER; a_inv_dt:REAL_32):REAL_32
			-- C function b2GearJoint.GetReactionTorque()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2GearJoint *)$a_object)->GetReactionTorque($a_inv_dt)"
		end

	frozen b2GearJoint_GetJoint1(a_object:POINTER):POINTER
			-- C function b2GearJoint.GetJoint1()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2GearJoint *)$a_object)->GetJoint1()"
		end

	frozen b2GearJoint_GetJoint2(a_object:POINTER):POINTER
			-- C function b2GearJoint.GetJoint2()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2GearJoint *)$a_object)->GetJoint2()"
		end

	frozen b2GearJoint_GetRatio(a_object:POINTER):REAL_32
			-- C function b2GearJoint.GetRatio()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2GearJoint *)$a_object)->GetRatio()"
		end

	frozen b2GearJoint_SetRatio(a_object:POINTER; a_value:REAL_32)
			-- C function b2GearJoint.SetRatio()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2GearJoint *)$a_object)->SetRatio($a_value)"
		end

	frozen b2GearJoint_ShiftOrigin(a_object, a_value:POINTER)
			-- C function b2GearJoint.ShiftOrigin()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2GearJoint *)$a_object)->ShiftOrigin(*((b2Vec2 *)$a_value))"
		end

feature -- b2GearJointDef

	frozen b2GearJointDef_new:POINTER
			-- C initialization of a b2GearJointDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2GearJointDef ()"
		end

	frozen b2GearJointDef_delete (a_object:POINTER)
			-- C destruction of a b2GearJointDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2GearJointDef *)$a_object)"
		end

	frozen b2GearJointDef_joint1_get (a_object:POINTER):POINTER
			-- C getter of b2GearJointDef.joint1
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2GearJointDef *)$a_object)->joint1"
		end

	frozen b2GearJointDef_joint1_set (a_object, a_value:POINTER)
			-- C setter of b2GearJointDef.joint1
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2GearJointDef *)$a_object)->joint1 = ((b2Joint *)$a_value)"
		end

	frozen b2GearJointDef_joint2_get (a_object:POINTER):POINTER
			-- C getter of b2GearJointDef.joint2
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2GearJointDef *)$a_object)->joint2"
		end

	frozen b2GearJointDef_joint2_set (a_object, a_value:POINTER)
			-- C setter of b2GearJointDef.joint2
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2GearJointDef *)$a_object)->joint2 = ((b2Joint *)$a_value)"
		end

	frozen b2GearJointDef_ratio_get (a_object:POINTER):REAL_32
			-- C getter of b2GearJointDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2GearJointDef *)$a_object)->ratio"
		end

	frozen b2GearJointDef_ratio_set (a_object:POINTER; a_value:REAL_32)
			-- C setter of b2GearJointDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2GearJointDef *)$a_object)->ratio = $a_value"
		end

feature -- b2Joint

	frozen b2Joint_GetBodyA(a_object:POINTER):POINTER
			-- C function b2Joint.GetBodyA()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Joint *)$a_object)->GetBodyA()"
		end

	frozen b2Joint_GetBodyB(a_object:POINTER):POINTER
			-- C function b2Joint.GetBodyB()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Joint *)$a_object)->GetBodyB()"
		end

	frozen b2Joint_GetNext(a_object:POINTER):POINTER
			-- C function b2Joint.GetNext()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Joint *)$a_object)->GetNext()"
		end

	frozen b2Joint_getType (a_object:POINTER):NATURAL
			-- C function b2Joint.GetType()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Joint *)$a_object)->GetType ()"
		end

	frozen b2Joint_type_unknownJoint:NATURAL
			-- C value of constant b2JointType::e_unknownJoint
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2JointType::e_unknownJoint"
		end

	frozen b2Joint_type_revoluteJoint:NATURAL
			-- C value of constant b2JointType::e_revoluteJoint
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2JointType::e_revoluteJoint"
		end

	frozen b2Joint_type_prismaticJoint:NATURAL
			-- C value of constant b2JointType::e_prismaticJoint
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2JointType::e_prismaticJoint"
		end

	frozen b2Joint_type_distanceJoint:NATURAL
			-- C value of constant b2JointType::e_distanceJoint
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2JointType::e_distanceJoint"
		end

	frozen b2Joint_type_pulleyJoint:NATURAL
			-- C value of constant b2JointType::e_pulleyJoint
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2JointType::e_pulleyJoint"
		end

	frozen b2Joint_type_mouseJoint:NATURAL
			-- C value of constant b2JointType::e_mouseJoint
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2JointType::e_mouseJoint"
		end

	frozen b2Joint_type_gearJoint:NATURAL
			-- C value of constant b2JointType::e_gearJoint
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2JointType::e_gearJoint"
		end

	frozen b2Joint_type_wheelJoint:NATURAL
			-- C value of constant b2JointType::e_wheelJoint
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2JointType::e_wheelJoint"
		end

	frozen b2Joint_type_weldJoint:NATURAL
			-- C value of constant b2JointType::e_weldJoint
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2JointType::e_weldJoint"
		end

	frozen b2Joint_type_frictionJoint:NATURAL
			-- C value of constant b2JointType::e_frictionJoint
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2JointType::e_frictionJoint"
		end

	frozen b2Joint_type_ropeJoint:NATURAL
			-- C value of constant b2JointType::e_ropeJoint
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2JointType::e_ropeJoint"
		end

	frozen b2Joint_type_motorJoint:NATURAL
			-- C value of constant b2JointType::e_motorJoint
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2JointType::e_motorJoint"
		end

	frozen b2Joint_GetCollideConnected(a_object:POINTER):BOOLEAN
			-- C function b2Joint.GetCollideConnected()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Joint *)$a_object)->GetCollideConnected()"
		end

feature -- b2JointDef

	frozen b2JointDef_new:POINTER
			-- C initialisation of a b2JointDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2JointDef ()"
		end

	frozen b2JointDef_delete(a_object:POINTER)
			-- C destruction of a b2JointDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2JointDef *)$a_object)"
		end

	frozen b2JointDef_bodyA_get(a_object:POINTER):POINTER
			-- C getter of b2JointDef.bodyA
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2JointDef *)$a_object)->bodyA"
		end

	frozen b2JointDef_bodyA_set(a_object:POINTER; a_value:POINTER)
			-- C setter of b2JointDef.bodyA
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2JointDef *)$a_object)->bodyA = (b2Body *)$a_value"
		end

	frozen b2JointDef_bodyB_get(a_object:POINTER):POINTER
			-- C getter of b2JointDef.bodyA
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2JointDef *)$a_object)->bodyB"
		end

	frozen b2JointDef_bodyB_set(a_object:POINTER; a_value:POINTER)
			-- C setter of b2JointDef.bodyB
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2JointDef *)$a_object)->bodyB = (b2Body *)$a_value"
		end

	frozen b2JointDef_collideConnected_get(a_object:POINTER):BOOLEAN
			-- C getter of b2JointDef.collideConnected
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2JointDef *)$a_object)->collideConnected"
		end

	frozen b2JointDef_collideConnected_set(a_object:POINTER; a_value:BOOLEAN)
			-- C setter of b2JointDef.collideConnected
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2JointDef *)$a_object)->collideConnected = $a_value"
		end

	frozen b2JointDef_type_get (a_object:POINTER):NATURAL
			-- C getter of b2JointDef.type
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2JointDef *)$a_object)->type"
		end

feature -- b2Manifold

	frozen b2Manifold_b2_maxManifoldPoints:INTEGER
			-- C constant b2_maxManifoldPoints
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2_maxManifoldPoints"
		end

	frozen b2Manifold_type_get (a_object:POINTER):NATURAL
			-- C getter of b2Manifold.type
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Manifold *)$a_object)->type"
		end

	frozen b2Manifold_type_e_faceA:NATURAL
			-- C constant e_faceA
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2Manifold::e_faceA"
		end

	frozen b2Manifold_type_e_faceB:NATURAL
			-- C constant e_faceB
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2Manifold::e_faceB"
		end

	frozen b2Manifold_type_e_circles:NATURAL
			-- C constant e_circles
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2Manifold::e_circles"
		end

	frozen b2Manifold_localNormal_get (a_object:POINTER):POINTER
			-- C getter of b2Manifold.localNormal
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2Manifold *)$a_object)->localNormal)"
		end

	frozen b2Manifold_localPoint_get (a_object:POINTER):POINTER
			-- C getter of b2Manifold.localPoint
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2Manifold *)$a_object)->localPoint)"
		end

	frozen b2Manifold_pointCount_get (a_object:POINTER):INTEGER
			-- C getter of b2Manifold.pointCount
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Manifold *)$a_object)->pointCount"
		end

	frozen b2Manifold_points_get_at (a_object:POINTER; a_index:INTEGER):POINTER
			-- C getter of b2Manifold.points[a_index]
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2Manifold *)$a_object)->points[$a_index])"
		end

feature -- b2ManifoldPoint

	frozen b2ManifoldPoint_normalImpulse_get (a_object:POINTER):REAL_32
			-- C getter of b2ManifoldPoint.normalImpulse
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2ManifoldPoint *)$a_object)->normalImpulse"
		end

	frozen b2ManifoldPoint_tangentImpulse_get (a_object:POINTER):REAL_32
			-- C getter of b2ManifoldPoint.tangentImpulse
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2ManifoldPoint *)$a_object)->tangentImpulse"
		end

	frozen b2ManifoldPoint_id_key_get (a_object:POINTER):NATURAL
			-- C getter of b2ManifoldPoint.id.key
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2ManifoldPoint *)$a_object)->id.key"
		end

	frozen b2ManifoldPoint_id_cf_indexA_get (a_object:POINTER):NATURAL_8
			-- C getter of b2ManifoldPoint.id.cf.indexA
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2ManifoldPoint *)$a_object)->id.cf.indexA"
		end

	frozen b2ManifoldPoint_id_cf_indexB_get (a_object:POINTER):NATURAL_8
			-- C getter of b2ManifoldPoint.id.cf.indexB
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2ManifoldPoint *)$a_object)->id.cf.indexB"
		end

	frozen b2ManifoldPoint_id_cf_typeA_get (a_object:POINTER):NATURAL_8
			-- C getter of b2ManifoldPoint.id.cf.typeA
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2ManifoldPoint *)$a_object)->id.cf.typeA"
		end

	frozen b2ManifoldPoint_id_cf_typeB_get (a_object:POINTER):NATURAL_8
			-- C getter of b2ManifoldPoint.id.cf.typeB
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2ManifoldPoint *)$a_object)->id.cf.typeB"
		end

	frozen b2ManifoldPoint_localPoint_get (a_object:POINTER):POINTER
			-- C getter of b2ManifoldPoint.localPoint
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2ManifoldPoint *)$a_object)->localPoint)"
		end

	frozen b2ContactFeature_type_e_vertex:NATURAL
			-- C constant e_vertex
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2ContactFeature::e_vertex"
		end

	frozen b2ContactFeature_type_e_face:NATURAL
			-- C constant e_face
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2ContactFeature::e_face"
		end

feature -- b2MassData

	frozen b2MassData_new:POINTER
			-- C initialisation of a b2MassData
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2MassData ()"
		end

	frozen b2MassData_delete(a_object:POINTER)
			-- C destruction of a b2MassData
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2MassData *)$a_object)"
		end

	frozen b2MassData_center_get(a_object:POINTER):POINTER
			-- C getter of b2MassData.center
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2MassData *)$a_object)->center)"
		end

	frozen b2MassData_mass_get(a_object:POINTER):REAL_32
			-- C getter of b2MassData.mass
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MassData *)$a_object)->mass"
		end

	frozen b2MassData_mass_set(a_object:POINTER; a_value:REAL_32)
			-- C setter of b2MassData.mass
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MassData *)$a_object)->mass = $a_value"
		end

	frozen b2MassData_I_get(a_object:POINTER):REAL_32
			-- C getter of b2MassData.I
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MassData *)$a_object)->I"
		end

	frozen b2MassData_I_set(a_object:POINTER; a_value:REAL_32)
			-- C setter of b2MassData.I
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MassData *)$a_object)->I = $a_value"
		end

feature -- b2MotorJoint

	frozen b2MotorJoint_GetAnchorA(a_object, a_value:POINTER)
			-- C function b2MotorJoint.GetAnchorA()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2MotorJoint *)$a_object)->GetAnchorA();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2MotorJoint_GetAnchorB(a_object, a_value:POINTER)
			-- C function b2MotorJoint.GetAnchorB()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2MotorJoint *)$a_object)->GetAnchorB();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2MotorJoint_GetReactionForce(a_object, a_value:POINTER; a_inv_dt:REAL_32)
			-- C function b2MotorJoint.GetReactionForce()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2MotorJoint *)$a_object)->GetReactionForce($a_inv_dt);
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2MotorJoint_GetReactionTorque(a_object:POINTER; a_inv_dt:REAL_32):REAL_32
			-- C function b2MotorJoint.GetReactionTorque()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MotorJoint *)$a_object)->GetReactionTorque($a_inv_dt)"
		end

	frozen b2MotorJoint_ShiftOrigin(a_object, a_value:POINTER)
			-- C function b2MotorJoint.ShiftOrigin()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MotorJoint *)$a_object)->ShiftOrigin(*((b2Vec2 *)$a_value))"
		end

feature -- b2MotorJointDef

	frozen b2MotorJointDef_new:POINTER
			-- C initialization of a b2MotorJointDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2MotorJointDef ()"
		end

	frozen b2MotorJointDef_delete (a_object:POINTER)
			-- C destruction of a b2MotorJointDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2MotorJointDef *)$a_object)"
		end

	frozen b2MotorJointDef_Initialize (a_object, a_body_a, a_body_b:POINTER)
			-- C function b2MotorJointDef.Initialize()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MotorJointDef *)$a_object)->Initialize ((b2Body *)$a_body_a, (b2Body *)$a_body_b)"
		end

	frozen b2MotorJointDef_linearOffset_get (a_object:POINTER):POINTER
			-- C getter of b2MotorJointDef.linearOffset
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2MotorJointDef *)$a_object)->linearOffset)"
		end

	frozen b2MotorJointDef_linearOffset_set (a_object, a_linear_offset:POINTER)
			-- C setter of b2MotorJointDef.linearOffset
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MotorJointDef *)$a_object)->linearOffset = *((b2Vec2 *)$a_linear_offset)"
		end

	frozen b2MotorJointDef_angularOffset_get (a_object:POINTER):REAL_32
			-- C getter of b2MotorJointDef.angularOffset
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MotorJointDef *)$a_object)->angularOffset"
		end

	frozen b2MotorJointDef_angularOffset_set (a_object:POINTER; a_value:REAL_32)
			-- C setter of b2MotorJointDef.angularOffset
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MotorJointDef *)$a_object)->angularOffset = $a_value"
		end

	frozen b2MotorJointDef_maxForce_get (a_object:POINTER):REAL_32
			-- C getter of b2MotorJointDef.maxForce
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MotorJointDef *)$a_object)->maxForce"
		end

	frozen b2MotorJointDef_maxForce_set (a_object:POINTER; a_value:REAL_32)
			-- C setter of b2MotorJointDef.maxForce
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MotorJointDef *)$a_object)->maxForce = $a_value"
		end

	frozen b2MotorJointDef_maxTorque_get (a_object:POINTER):REAL_32
			-- C getter of b2MotorJointDef.maxTorque
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MotorJointDef *)$a_object)->maxTorque"
		end

	frozen b2MotorJointDef_maxTorque_set (a_object:POINTER; a_value:REAL_32)
			-- C setter of b2MotorJointDef.maxTorque
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MotorJointDef *)$a_object)->maxTorque = $a_value"
		end

	frozen b2MotorJointDef_correctionFactor_get (a_object:POINTER):REAL_32
			-- C getter of b2MotorJointDef.correctionFactor
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MotorJointDef *)$a_object)->correctionFactor"
		end

	frozen b2MotorJointDef_correctionFactor_set (a_object:POINTER; a_value:REAL_32)
			-- C setter of b2MotorJointDef.correctionFactor
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MotorJointDef *)$a_object)->correctionFactor = $a_value"
		end

feature -- b2MouseJoint

	frozen b2MouseJoint_GetAnchorA(a_object, a_value:POINTER)
			-- C function b2MouseJoint.GetAnchorA()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2MouseJoint *)$a_object)->GetAnchorA();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2MouseJoint_GetAnchorB(a_object, a_value:POINTER)
			-- C function b2MouseJoint.GetAnchorB()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2MouseJoint *)$a_object)->GetAnchorB();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2MouseJoint_GetReactionForce(a_object, a_value:POINTER; a_inv_dt:REAL_32)
			-- C function b2MouseJoint.GetReactionForce()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2MouseJoint *)$a_object)->GetReactionForce($a_inv_dt);
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2MouseJoint_GetReactionTorque(a_object:POINTER; a_inv_dt:REAL_32):REAL_32
			-- C function b2MouseJoint.GetReactionTorque()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MouseJoint *)$a_object)->GetReactionTorque($a_inv_dt)"
		end

	frozen b2MouseJoint_ShiftOrigin(a_object, a_value:POINTER)
			-- C function b2MouseJoint.ShiftOrigin()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MouseJoint *)$a_object)->ShiftOrigin(*((b2Vec2 *)$a_value))"
		end

	frozen b2MouseJoint_GetTarget(a_object:POINTER):POINTER
			-- C function b2MouseJoint.GetTarget()
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2MouseJoint *)$a_object)->GetTarget())"
		end

	frozen b2MouseJoint_SetTarget(a_object, a_target:POINTER)
			-- C function b2MouseJoint.SetTarget()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MouseJoint *)$a_object)->SetTarget(*((b2Vec2 *)$a_target))"
		end

	frozen b2MouseJoint_GetMaxForce(a_object:POINTER):REAL_32
			-- C function b2MouseJoint.GetMaxForce()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MouseJoint *)$a_object)->GetMaxForce()"
		end

	frozen b2MouseJoint_SetMaxForce(a_object:POINTER; a_force:REAL_32)
			-- C function b2MouseJoint.SetMaxForce()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MouseJoint *)$a_object)->SetMaxForce($a_force)"
		end

	frozen b2MouseJoint_GetFrequency(a_object:POINTER):REAL_32
			-- C function b2MouseJoint.GetFrequency()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MouseJoint *)$a_object)->GetFrequency()"
		end

	frozen b2MouseJoint_SetFrequency(a_object:POINTER; a_frequency:REAL_32)
			-- C function b2MouseJoint.SetFrequency()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MouseJoint *)$a_object)->SetFrequency($a_frequency)"
		end

	frozen b2MouseJoint_GetDampingRatio(a_object:POINTER):REAL_32
			-- C function b2MouseJoint.GetDampingRatio()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MouseJoint *)$a_object)->GetDampingRatio()"
		end

	frozen b2MouseJoint_SetDampingRatio(a_object:POINTER; a_ratio:REAL_32)
			-- C function b2MouseJoint.SetDampingRatio()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MouseJoint *)$a_object)->SetDampingRatio($a_ratio)"
		end

feature -- b2MouseJointDef

	frozen b2MouseJointDef_new:POINTER
			-- C initialization of a b2MouseJointDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2MouseJointDef ()"
		end

	frozen b2MouseJointDef_delete (a_object:POINTER)
			-- C destruction of a b2MouseJointDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2MouseJointDef *)$a_object)"
		end

	frozen b2MouseJointDef_target_get (a_object:POINTER):POINTER
			-- C getter of b2MouseJointDef.target
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2MouseJointDef *)$a_object)->target)"
		end

	frozen b2MouseJointDef_target_set (a_object, a_target:POINTER)
			-- C setter of b2MouseJointDef.target
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MouseJointDef *)$a_object)->target = *((b2Vec2 *)$a_target)"
		end

	frozen b2MouseJointDef_maxForce_get (a_object:POINTER):REAL_32
			-- C getter of b2MouseJointDef.maxForce
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MouseJointDef *)$a_object)->maxForce"
		end

	frozen b2MouseJointDef_maxForce_set (a_object:POINTER; a_force:REAL_32)
			-- C setter of b2MouseJointDef.maxForce
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MouseJointDef *)$a_object)->maxForce = $a_force"
		end

	frozen b2MouseJointDef_frequencyHz_get (a_object:POINTER):REAL_32
			-- C getter of b2MouseJointDef.
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MouseJointDef *)$a_object)->frequencyHz"
		end

	frozen b2MouseJointDef_frequencyHz_set (a_object:POINTER; a_frequency:REAL_32)
			-- C setter of b2MouseJointDef.
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MouseJointDef *)$a_object)->frequencyHz = $a_frequency"
		end

	frozen b2MouseJointDef_dampingRatio_get (a_object:POINTER):REAL_32
			-- C getter of b2MouseJointDef.dampingRatio
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MouseJointDef *)$a_object)->dampingRatio"
		end

	frozen b2MouseJointDef_dampingRatio_set (a_object:POINTER; a_ratio:REAL_32)
			-- C setter of b2MouseJointDef.dampingRatio
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2MouseJointDef *)$a_object)->dampingRatio = $a_ratio"
		end

feature -- b2PointState

	frozen b2PointState_b2_nullState:NATURAL
			-- C constant b2_nullState
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2_nullState"
		end

	frozen b2PointState_b2_addState:NATURAL
			-- C constant b2_addState
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2_addState"
		end

	frozen b2PointState_b2_removeState:NATURAL
			-- C constant b2_removeState
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2_removeState"
		end

	frozen b2PointState_b2_persistState:NATURAL
			-- C constant b2_persistState
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2_persistState"
		end

	frozen b2PointState_b2GetPointStates(a_state_1, a_state_2, a_manifold_1, a_manifold_2:POINTER)
			-- C function b2GetPointStates
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2GetPointStates((b2PointState *)$a_state_1, (b2PointState *)$a_state_2, (b2Manifold *)$a_manifold_1, (b2Manifold *)$a_manifold_2)"
		end


feature -- b2PolygonShape

	frozen b2PolygonShape_new:POINTER
			-- C initialisation of a b2PolygonShape
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2PolygonShape ()"
		end

	frozen b2PolygonShape_delete(a_object:POINTER)
			-- C destruction of a b2PolygonShape
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2PolygonShape *)$a_object)"
		end

	frozen b2PolygonShape_SetAsBox(a_object:POINTER; a_x, a_y:REAL_32)
			-- C function b2PolygonShape.SetAsBox()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PolygonShape *)$a_object)->SetAsBox($a_x, $a_y)"
		end

	frozen b2PolygonShape_SetAsBox_oriented(a_object:POINTER; a_x, a_y:REAL_32; a_center:POINTER; a_angle:REAL_32)
			-- C function b2PolygonShape.SetAsBox()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PolygonShape *)$a_object)->SetAsBox($a_x, $a_y, *((b2Vec2 *) $a_center), $a_angle)"
		end

	frozen b2PolygonShape_Set(a_object:POINTER; a_points:POINTER; a_count:INTEGER_32)
			-- C function b2PolygonShape.Set()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PolygonShape *)$a_object)->Set((b2Vec2 *)$a_points, $a_count)"
		end

	frozen b2PolygonShape_Validate(a_object:POINTER):BOOLEAN
			-- C function b2PolygonShape.Validate()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PolygonShape *)$a_object)->Validate()"
		end

	frozen b2_maxPolygonVertices:INTEGER_32
			-- C constant b2_maxPolygonVertices
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2_maxPolygonVertices"
		end

	frozen b2PolygonShape_m_centroid_get(a_object:POINTER):POINTER
			-- C getter of b2Profile.m_centroid
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2PolygonShape *)$a_object)->m_centroid)"
		end

	frozen b2PolygonShape_m_vertices_get(a_object:POINTER):POINTER
			-- C getter of b2Profile.m_vertices
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PolygonShape *)$a_object)->m_vertices"
		end

	frozen b2PolygonShape_m_normals_get(a_object:POINTER):POINTER
			-- C getter of b2Profile.m_normals
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PolygonShape *)$a_object)->m_normals"
		end

	frozen b2PolygonShape_m_count_get(a_object:POINTER):INTEGER_32
			-- C getter of b2Profile.m_count
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PolygonShape *)$a_object)->m_count"
		end

	frozen b2PolygonShape_RayCast(a_object, a_output, a_input, a_transform:POINTER; a_child_index:INTEGER_32):BOOLEAN
			-- C function b2PolygonShape.RayCast()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PolygonShape *)$a_object)->RayCast((b2RayCastOutput *)$a_output, *((b2RayCastInput *)$a_input), *((b2Transform *)$a_transform), $a_child_index)"
		end

	frozen b2PolygonShape_ComputeMass(a_object, a_mass_data:POINTER; a_density:REAL_32)
			-- C function b2PolygonShape.ComputeMass()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PolygonShape *)$a_object)->ComputeMass((b2MassData *)$a_mass_data, $a_density)"
		end

	frozen b2PolygonShape_ComputeAABB(a_object, a_output, a_transform:POINTER; a_child_index:INTEGER_32)
			-- C function b2PolygonShape.ComputeAABB()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PolygonShape *)$a_object)->ComputeAABB((b2AABB *)$a_output, *((b2Transform *)$a_transform), $a_child_index)"
		end

	frozen b2PolygonShape_GetChildCount(a_object:POINTER):INTEGER_32
			-- C function b2PolygonShape.GetChildCount()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PolygonShape *)$a_object)->GetChildCount()"
		end

	frozen b2PolygonShape_TestPoint(a_object, a_transform, a_point:POINTER):BOOLEAN
			-- C function b2PolygonShape.TestPoint()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PolygonShape *)$a_object)->TestPoint(*((b2Transform *)$a_transform), *((b2Vec2 *)$a_point))"
		end

feature -- b2PrismaticJoint

	frozen b2PrismaticJoint_GetLocalAnchorA(a_object, a_value:POINTER)
			-- C function b2PrismaticJoint.GetLocalAnchorA()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2PrismaticJoint *)$a_object)->GetLocalAnchorA();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2PrismaticJoint_GetLocalAnchorB(a_object, a_value:POINTER)
			-- C function b2PrismaticJoint.GetLocalAnchorB()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2PrismaticJoint *)$a_object)->GetLocalAnchorB();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2PrismaticJoint_GetLocalAxisA(a_object, a_value:POINTER)
			-- C function b2PrismaticJoint.GetLocalAxisA()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2PrismaticJoint *)$a_object)->GetLocalAxisA();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2PrismaticJoint_GetReferenceAngle(a_object:POINTER):REAL_32
			-- C function b2PrismaticJoint.GetReferenceAngle()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJoint *)$a_object)->GetReferenceAngle()"
		end

	frozen b2PrismaticJoint_GetJointTranslation(a_object:POINTER):REAL_32
			-- C function b2PrismaticJoint.GetJointTranslation()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJoint *)$a_object)->GetJointTranslation()"
		end

	frozen b2PrismaticJoint_GetJointSpeed(a_object:POINTER):REAL_32
			-- C function b2PrismaticJoint.GetJointSpeed()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJoint *)$a_object)->GetJointSpeed()"
		end

	frozen b2PrismaticJoint_IsLimitEnabled(a_object:POINTER):BOOLEAN
			-- C function b2PrismaticJoint.IsLimitEnabled()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJoint *)$a_object)->IsLimitEnabled()"
		end

	frozen b2PrismaticJoint_EnableLimit(a_object:POINTER; a_value:BOOLEAN)
			-- C function b2PrismaticJoint.EnableLimit()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJoint *)$a_object)->EnableLimit($a_value)"
		end

	frozen b2PrismaticJoint_GetLowerLimit(a_object:POINTER):REAL_32
			-- C function b2PrismaticJoint.GetLowerLimit()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJoint *)$a_object)->GetLowerLimit()"
		end

	frozen b2PrismaticJoint_GetUpperLimit(a_object:POINTER):REAL_32
			-- C function b2PrismaticJoint.GetUpperLimit()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJoint *)$a_object)->GetUpperLimit()"
		end

	frozen b2PrismaticJoint_SetLimits(a_object:POINTER; a_lower, a_upper:REAL_32)
			-- C function b2PrismaticJoint.SetLimits()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJoint *)$a_object)->SetLimits($a_lower, $a_upper)"
		end

	frozen b2PrismaticJoint_IsMotorEnabled(a_object:POINTER):BOOLEAN
			-- C function b2PrismaticJoint.IsMotorEnabled()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJoint *)$a_object)->IsMotorEnabled()"
		end

	frozen b2PrismaticJoint_EnableMotor(a_object:POINTER; a_value:BOOLEAN)
			-- C function b2PrismaticJoint.EnableMotor()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJoint *)$a_object)->EnableMotor($a_value)"
		end

	frozen b2PrismaticJoint_GetMotorSpeed(a_object:POINTER):REAL_32
			-- C function b2PrismaticJoint.GetMotorSpeed()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJoint *)$a_object)->GetMotorSpeed()"
		end

	frozen b2PrismaticJoint_SetMotorSpeed(a_object:POINTER; a_value:REAL_32)
			-- C function b2PrismaticJoint.SetMotorSpeed()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJoint *)$a_object)->SetMotorSpeed($a_value)"
		end

	frozen b2PrismaticJoint_GetMaxMotorForce(a_object:POINTER):REAL_32
			-- C function b2PrismaticJoint.GetMaxMotorForce()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJoint *)$a_object)->GetMaxMotorForce()"
		end

	frozen b2PrismaticJoint_SetMaxMotorForce(a_object:POINTER; a_value:REAL_32)
			-- C function b2PrismaticJoint.SetMaxMotorForce()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJoint *)$a_object)->SetMaxMotorForce($a_value)"
		end

	frozen b2PrismaticJoint_GetMotorForce(a_object:POINTER; a_value:REAL_32):REAL_32
			-- C function b2PrismaticJoint.GetMotorForce()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJoint *)$a_object)->GetMotorForce($a_value)"
		end

	frozen b2PrismaticJoint_GetAnchorA(a_object, a_value:POINTER)
			-- C function b2PrismaticJoint.GetAnchorA()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2PrismaticJoint *)$a_object)->GetAnchorA();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2PrismaticJoint_GetAnchorB(a_object, a_value:POINTER)
			-- C function b2PrismaticJoint.GetAnchorB()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2PrismaticJoint *)$a_object)->GetAnchorB();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2PrismaticJoint_GetReactionForce(a_object, a_value:POINTER; a_inv_dt:REAL_32)
			-- C function b2PrismaticJoint.GetReactionForce()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2PrismaticJoint *)$a_object)->GetReactionForce($a_inv_dt);
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2PrismaticJoint_GetReactionTorque(a_object:POINTER; a_inv_dt:REAL_32):REAL_32
			-- C function b2PrismaticJoint.GetReactionTorque()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJoint *)$a_object)->GetReactionTorque($a_inv_dt)"
		end

	frozen b2PrismaticJoint_ShiftOrigin(a_object, a_value:POINTER)
			-- C function b2PrismaticJoint.ShiftOrigin()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJoint *)$a_object)->ShiftOrigin(*((b2Vec2 *)$a_value))"
		end

feature -- b2PrismaticJointDef

	frozen b2PrismaticJointDef_new:POINTER
			-- C initialization of a b2PrismaticJointDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2PrismaticJointDef ()"
		end

	frozen b2PrismaticJointDef_delete (a_object:POINTER)
			-- C destruction of a b2PrismaticJointDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2PrismaticJointDef *)$a_object)"
		end

	frozen b2PrismaticJointDef_Initialize (a_object, a_body_1, a_body_2, a_anchor, a_axis:POINTER)
			-- C function b2PrismaticJointDef.Initialize()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJointDef *)$a_object)->Initialize((b2Body *)$a_body_1, (b2Body *)$a_body_2, *((b2Vec2 *)$a_anchor), *((b2Vec2 *)$a_axis))"
		end

	frozen b2PrismaticJointDef_localAnchorA_get (a_object:POINTER):POINTER
			-- C getter of b2PrismaticJointDef.localAnchorA
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2PrismaticJointDef *)$a_object)->localAnchorA)"
		end

	frozen b2PrismaticJointDef_localAnchorA_set (a_object, a_vector:POINTER)
			-- C setter of b2PrismaticJointDef.localAnchorA
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJointDef *)$a_object)->localAnchorA = *((b2Vec2 *)$a_vector)"
		end

	frozen b2PrismaticJointDef_localAnchorB_get (a_object:POINTER):POINTER
			-- C getter of b2PrismaticJointDef.localAnchorB
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2PrismaticJointDef *)$a_object)->localAnchorB)"
		end

	frozen b2PrismaticJointDef_localAnchorB_set (a_object, a_vector:POINTER)
			-- C setter of b2PrismaticJointDef.localAnchorB
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJointDef *)$a_object)->localAnchorB = *((b2Vec2 *)$a_vector)"
		end

	frozen b2PrismaticJointDef_localAxisA_get (a_object:POINTER):POINTER
			-- C getter of b2PrismaticJointDef.localAxisA
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2PrismaticJointDef *)$a_object)->localAxisA)"
		end

	frozen b2PrismaticJointDef_localAxisA_set (a_object, a_vector:POINTER)
			-- C setter of b2PrismaticJointDef.localAxisA
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJointDef *)$a_object)->localAxisA = *((b2Vec2 *)$a_vector)"
		end

	frozen b2PrismaticJointDef_referenceAngle_get (a_object:POINTER):REAL_32
			-- C getter of b2PrismaticJointDef.referenceAngle
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJointDef *)$a_object)->referenceAngle"
		end

	frozen b2PrismaticJointDef_referenceAngle_set (a_object:POINTER; a_angle:REAL_32)
			-- C setter of b2PrismaticJointDef.referenceAngle
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJointDef *)$a_object)->referenceAngle = $a_angle"
		end

	frozen b2PrismaticJointDef_enableLimit_get (a_object:POINTER):BOOLEAN
			-- C getter of b2PrismaticJointDef.enableLimit
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJointDef *)$a_object)->enableLimit"
		end

	frozen b2PrismaticJointDef_enableLimit_set (a_object:POINTER; a_value:BOOLEAN)
			-- C setter of b2PrismaticJointDef.enableLimit
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJointDef *)$a_object)->enableLimit = $a_value"
		end

	frozen b2PrismaticJointDef_lowerTranslation_get (a_object:POINTER):REAL_32
			-- C getter of b2PrismaticJointDef.lowerTranslation
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJointDef *)$a_object)->lowerTranslation"
		end

	frozen b2PrismaticJointDef_lowerTranslation_set (a_object:POINTER; a_lower_translation:REAL_32)
			-- C setter of b2PrismaticJointDef.lowerTranslation
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJointDef *)$a_object)->lowerTranslation = $a_lower_translation"
		end

	frozen b2PrismaticJointDef_upperTranslation_get (a_object:POINTER):REAL_32
			-- C getter of b2PrismaticJointDef.upperTranslation
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJointDef *)$a_object)->upperTranslation"
		end

	frozen b2PrismaticJointDef_upperTranslation_set (a_object:POINTER; a_upper_translation:REAL_32)
			-- C setter of b2PrismaticJointDef.upperTranslation
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJointDef *)$a_object)->upperTranslation = $a_upper_translation"
		end

	frozen b2PrismaticJointDef_enableMotor_get (a_object:POINTER):BOOLEAN
			-- C getter of b2PrismaticJointDef.enableMotor
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJointDef *)$a_object)->enableMotor"
		end

	frozen b2PrismaticJointDef_enableMotor_set (a_object:POINTER; a_value:BOOLEAN)
			-- C setter of b2PrismaticJointDef.enableMotor
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJointDef *)$a_object)->enableMotor = $a_value"
		end

	frozen b2PrismaticJointDef_maxMotorForce_get (a_object:POINTER):REAL_32
			-- C getter of b2PrismaticJointDef.maxMotorForce
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJointDef *)$a_object)->maxMotorForce"
		end

	frozen b2PrismaticJointDef_maxMotorForce_set (a_object:POINTER; a_force:REAL_32)
			-- C setter of b2PrismaticJointDef.maxMotorForce
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJointDef *)$a_object)->maxMotorForce = $a_force"
		end

	frozen b2PrismaticJointDef_motorSpeed_get (a_object:POINTER):REAL_32
			-- C getter of b2PrismaticJointDef.motorSpeed
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJointDef *)$a_object)->motorSpeed"
		end

	frozen b2PrismaticJointDef_motorSpeed_set (a_object:POINTER; a_speed:REAL_32)
			-- C setter of b2PrismaticJointDef.motorSpeed
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PrismaticJointDef *)$a_object)->motorSpeed = $a_speed"
		end

feature -- b2Profile

	frozen b2Profile_step_get(a_object:POINTER):REAL_32
			-- C getter of b2Profile.step
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Profile *)$a_object)->step"
		end

	frozen b2Profile_collide_get(a_object:POINTER):REAL_32
			-- C getter of b2Profile.collide
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Profile *)$a_object)->collide"
		end

	frozen b2Profile_solveInit_get(a_object:POINTER):REAL_32
			-- C getter of b2Profile.solveInit
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Profile *)$a_object)->solveInit"
		end

	frozen b2Profile_solve_get(a_object:POINTER):REAL_32
			-- C getter of b2Profile.solve
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Profile *)$a_object)->solve"
		end

	frozen b2Profile_solveVelocity_get(a_object:POINTER):REAL_32
			-- C getter of b2Profile.solveVelocity
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Profile *)$a_object)->solveVelocity"
		end

	frozen b2Profile_solvePosition_get(a_object:POINTER):REAL_32
			-- C getter of b2Profile.solvePosition
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Profile *)$a_object)->solvePosition"
		end

	frozen b2Profile_broadphase_get(a_object:POINTER):REAL_32
			-- C getter of b2Profile.broadphase
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Profile *)$a_object)->broadphase"
		end

	frozen b2Profile_solveTOI_get(a_object:POINTER):REAL_32
			-- C getter of b2Profile.solveTOI
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Profile *)$a_object)->solveTOI"
		end

feature -- b2PulleyJoint

	frozen b2PulleyJoint_GetAnchorA(a_object, a_value:POINTER)
			-- C function b2PulleyJoint.GetAnchorA()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2PulleyJoint *)$a_object)->GetAnchorA();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2PulleyJoint_GetAnchorB(a_object, a_value:POINTER)
			-- C function b2PulleyJoint.GetAnchorB()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2PulleyJoint *)$a_object)->GetAnchorB();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2PulleyJoint_GetReactionForce(a_object, a_value:POINTER; a_inv_dt:REAL_32)
			-- C function b2PulleyJoint.GetReactionForce()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2PulleyJoint *)$a_object)->GetReactionForce($a_inv_dt);
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2PulleyJoint_GetReactionTorque(a_object:POINTER; a_inv_dt:REAL_32):REAL_32
			-- C function b2PulleyJoint.GetReactionTorque()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PulleyJoint *)$a_object)->GetReactionTorque($a_inv_dt)"
		end

	frozen b2PulleyJoint_ShiftOrigin(a_object, a_value:POINTER)
			-- C function b2PulleyJoint.ShiftOrigin()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PulleyJoint *)$a_object)->ShiftOrigin(*((b2Vec2 *)$a_value))"
		end

feature -- b2PulleyJointDef

	frozen b2PulleyJointDef_new:POINTER
			-- C initialisation of a b2PulleyJointDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2PulleyJointDef ()"
		end

	frozen b2PulleyJointDef_delete(a_object:POINTER)
			-- C destruction of a b2PulleyJointDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2PulleyJointDef *)$a_object)"
		end

	frozen b2PulleyJointDef_initialize (a_object, a_body_1, a_body_2, a_ground_anchor_1, a_ground_anchor_2, a_anchor_1, a_anchor_2:POINTER; a_ratio:REAL_32)
			-- C function b2PulleyJointDef.Initialize()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				((b2PulleyJointDef *)$a_object)->Initialize ((b2Body *)$a_body_1, (b2Body *)$a_body_2,
															*((b2Vec2 *)$a_ground_anchor_1), *((b2Vec2 *)$a_ground_anchor_2),
															*((b2Vec2 *)$a_anchor_1), *((b2Vec2 *)$a_anchor_2),
															$a_ratio)
			]"
		end

	frozen b2PulleyJointDef_groundAnchorA_get (a_object:POINTER):POINTER
			-- C getter of b2PulleyJointDef.groundAnchorA
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2PulleyJointDef *)$a_object)->groundAnchorA)"
		end

	frozen b2PulleyJointDef_groundAnchorA_set (a_object, a_world_coordinates:POINTER)
			-- C setter of b2PulleyJointDef.groundAnchorA
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PulleyJointDef *)$a_object)->groundAnchorA = *((b2Vec2 *)$a_world_coordinates)"
		end

	frozen b2PulleyJointDef_groundAnchorB_get (a_object:POINTER):POINTER
			-- C getter of b2PulleyJointDef.groundAnchorB
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2PulleyJointDef *)$a_object)->groundAnchorB)"
		end

	frozen b2PulleyJointDef_groundAnchorB_set (a_object, a_world_coordinates:POINTER)
			-- C setter of b2PulleyJointDef.groundAnchorB
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PulleyJointDef *)$a_object)->groundAnchorB = *((b2Vec2 *)$a_world_coordinates)"
		end

	frozen b2PulleyJointDef_localAnchorA_get (a_object:POINTER):POINTER
			-- C getter of b2PulleyJointDef.localAnchorA
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2PulleyJointDef *)$a_object)->localAnchorA)"
		end

	frozen b2PulleyJointDef_localAnchorA_set (a_object, a_local_coordinates:POINTER)
			-- C setter of b2PulleyJointDef.localAnchorA
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PulleyJointDef *)$a_object)->localAnchorA = *((b2Vec2 *)$a_local_coordinates)"
		end

	frozen b2PulleyJointDef_localAnchorB_get (a_object:POINTER):POINTER
			-- C getter of b2PulleyJointDef.localAnchorB
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2PulleyJointDef *)$a_object)->localAnchorB)"
		end

	frozen b2PulleyJointDef_localAnchorB_set (a_object, a_local_coordinates:POINTER)
			-- C setter of b2PulleyJointDef.localAnchorB
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PulleyJointDef *)$a_object)->localAnchorB = *((b2Vec2 *)$a_local_coordinates)"
		end

	frozen b2PulleyJointDef_lengthA_get (a_object:POINTER):REAL_32
			-- C getter of b2PulleyJointDef.lengthA
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PulleyJointDef *)$a_object)->lengthA"
		end

	frozen b2PulleyJointDef_lengthA_set (a_object:POINTER; a_value:REAL_32)
			-- C setter of b2PulleyJointDef.lengthA
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PulleyJointDef *)$a_object)->lengthA = $a_value"
		end

	frozen b2PulleyJointDef_lengthB_get (a_object:POINTER):REAL_32
			-- C getter of b2PulleyJointDef.lengthB
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PulleyJointDef *)$a_object)->lengthB"
		end

	frozen b2PulleyJointDef_lengthB_set (a_object:POINTER; a_value:REAL_32)
			-- C setter of b2PulleyJointDef.lengthB
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PulleyJointDef *)$a_object)->lengthB = $a_value"
		end

	frozen b2PulleyJointDef_ratio_get (a_object:POINTER):REAL_32
			-- C getter of b2PulleyJointDef.ratio
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PulleyJointDef *)$a_object)->ratio"
		end

	frozen b2PulleyJointDef_ratio_set (a_object:POINTER a_ratio:REAL_32)
			-- C setter of b2PulleyJointDef.ratio
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2PulleyJointDef *)$a_object)->ratio = $a_ratio"
		end

feature -- b2RayCastInput

	frozen b2RayCastInput_new:POINTER
			-- C initialisation of a b2RayCastInput
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2RayCastInput ()"
		end

	frozen b2RayCastInput_delete(a_object:POINTER)
			-- C destruction of a b2RayCastInput
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2RayCastInput *)$a_object)"
		end

	frozen b2RayCastInput_p1_get(a_object:POINTER):POINTER
			-- C getter of b2RayCastInput.p1
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2RayCastInput *)$a_object)->p1)"
		end

	frozen b2RayCastInput_p2_get(a_object:POINTER):POINTER
			-- C getter of b2RayCastInput.p2
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2RayCastInput *)$a_object)->p2)"
		end

	frozen b2RayCastInput_maxFraction_get(a_object:POINTER):REAL_32
			-- C getter of b2RayCastInput.maxFraction
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RayCastInput *)$a_object)->maxFraction"
		end

	frozen b2RayCastInput_maxFraction_set(a_object:POINTER; a_value:REAL_32)
			-- C setter of b2RayCastInput.maxFraction
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RayCastInput *)$a_object)->maxFraction =$a_value"
		end

feature -- b2RayCastOutput

	frozen b2RayCastOutput_new:POINTER
			-- C initialisation of a b2RayCastOutput
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2RayCastOutput ()"
		end

	frozen b2RayCastOutput_delete(a_object:POINTER)
			-- C destruction of a b2RayCastOutput
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2RayCastOutput *)$a_object)"
		end

	frozen b2RayCastOutput_normal_get(a_object:POINTER):POINTER
			-- C getter of b2RayCastInput.normal
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2RayCastOutput *)$a_object)->normal)"
		end

	frozen b2RayCastOutput_fraction_get(a_object:POINTER):REAL_32
			-- C getter of b2RayCastOutput.fraction
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RayCastOutput *)$a_object)->fraction"
		end

feature -- b2RevoluteJoint

	frozen b2RevoluteJoint_GetAnchorA(a_object, a_value:POINTER)
			-- C function b2RevoluteJoint.GetAnchorA()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2RevoluteJoint *)$a_object)->GetAnchorA();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2RevoluteJoint_GetAnchorB(a_object, a_value:POINTER)
			-- C function b2RevoluteJoint.GetAnchorB()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2RevoluteJoint *)$a_object)->GetAnchorB();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2RevoluteJoint_GetLocalAnchorA(a_object, a_value:POINTER)
			-- C function b2RevoluteJoint.GetLocalAnchorA()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2RevoluteJoint *)$a_object)->GetLocalAnchorA();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2RevoluteJoint_GetLocalAnchorB(a_object, a_value:POINTER)
			-- C function b2RevoluteJoint.GetLocalAnchorB()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2RevoluteJoint *)$a_object)->GetLocalAnchorB();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2RevoluteJoint_GetReferenceAngle(a_object:POINTER):REAL_32
			-- C function b2RevoluteJoint.GetReferenceAngle()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJoint *)$a_object)->GetReferenceAngle()"
		end

	frozen b2RevoluteJoint_GetJointAngle(a_object:POINTER):REAL_32
			-- C function b2RevoluteJoint.GetJointAngle()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJoint *)$a_object)->GetJointAngle()"
		end

	frozen b2RevoluteJoint_GetJointSpeed(a_object:POINTER):REAL_32
			-- C function b2RevoluteJoint.GetJointSpeed()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJoint *)$a_object)->GetJointSpeed()"
		end

	frozen b2RevoluteJoint_IsLimitEnabled(a_object:POINTER):BOOLEAN
			-- C function b2RevoluteJoint.IsLimitEnabled()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJoint *)$a_object)->IsLimitEnabled()"
		end

	frozen b2RevoluteJoint_EnableLimit(a_object:POINTER; a_value:BOOLEAN)
			-- C function b2RevoluteJoint.EnableLimit()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJoint *)$a_object)->EnableLimit($a_value)"
		end

	frozen b2RevoluteJoint_GetLowerLimit(a_object:POINTER):REAL_32
			-- C function b2RevoluteJoint.GetLowerLimit()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJoint *)$a_object)->GetLowerLimit()"
		end

	frozen b2RevoluteJoint_GetUpperLimit(a_object:POINTER):REAL_32
			-- C function b2RevoluteJoint.GetUpperLimit()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJoint *)$a_object)->GetUpperLimit()"
		end

	frozen b2RevoluteJoint_SetLimits(a_object:POINTER; a_lower, a_upper:REAL_32)
			-- C function b2RevoluteJoint.SetLimits()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJoint *)$a_object)->SetLimits($a_lower, $a_upper)"
		end

	frozen b2RevoluteJoint_IsMotorEnabled(a_object:POINTER):BOOLEAN
			-- C function b2RevoluteJoint.IsMotorEnabled()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJoint *)$a_object)->IsMotorEnabled()"
		end

	frozen b2RevoluteJoint_EnableMotor(a_object:POINTER; a_value:BOOLEAN)
			-- C function b2RevoluteJoint.EnableMotor()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJoint *)$a_object)->EnableMotor($a_value)"
		end

	frozen b2RevoluteJoint_GetMotorSpeed(a_object:POINTER):REAL_32
			-- C function b2RevoluteJoint.GetMotorSpeed()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJoint *)$a_object)->GetMotorSpeed()"
		end

	frozen b2RevoluteJoint_SetMotorSpeed(a_object:POINTER; a_value:REAL_32)
			-- C function b2RevoluteJoint.SetMotorSpeed()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJoint *)$a_object)->SetMotorSpeed($a_value)"
		end

	frozen b2RevoluteJoint_GetMaxMotorTorque(a_object:POINTER):REAL_32
			-- C function b2RevoluteJoint.GetMaxMotorTorque()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJoint *)$a_object)->GetMaxMotorTorque()"
		end

	frozen b2RevoluteJoint_SetMaxMotorTorque(a_object:POINTER; a_value:REAL_32)
			-- C function b2RevoluteJoint.SetMaxMotorTorque()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJoint *)$a_object)->SetMaxMotorTorque($a_value)"
		end

	frozen b2RevoluteJoint_GetMotorTorque(a_object:POINTER; a_inv_dt:REAL_32):REAL_32
			-- C function b2RevoluteJoint.GetMotorTorque()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJoint *)$a_object)->GetMotorTorque($a_inv_dt)"
		end


	frozen b2RevoluteJoint_GetReactionForce(a_object, a_value:POINTER; a_inv_dt:REAL_32)
			-- C function b2RevoluteJoint.GetReactionForce()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2RevoluteJoint *)$a_object)->GetReactionForce($a_inv_dt);
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2RevoluteJoint_GetReactionTorque(a_object:POINTER; a_inv_dt:REAL_32):REAL_32
			-- C function b2RevoluteJoint.GetReactionTorque()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJoint *)$a_object)->GetReactionTorque($a_inv_dt)"
		end

	frozen b2RevoluteJoint_ShiftOrigin(a_object, a_value:POINTER)
			-- C function b2RevoluteJoint.ShiftOrigin()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJoint *)$a_object)->ShiftOrigin(*((b2Vec2 *)$a_value))"
		end

feature -- b2RevoluteJointDef

	frozen b2RevoluteJointDef_new:POINTER
			-- C initialisation of a b2RevoluteJointDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2RevoluteJointDef ()"
		end

	frozen b2RevoluteJointDef_delete (a_object:POINTER)
			-- C destruction of a b2RevoluteJointDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2RevoluteJointDef *)$a_object)"
		end

	frozen b2RevoluteJointDef_initialize (a_object, a_body_1, a_body_2, a_anchor:POINTER)
			-- C function b2RevoluteJointDef.Initialize()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJointDef *)$a_object)->Initialize ((b2Body *)$a_body_1, (b2Body *)$a_body_2, *((b2Vec2 *)$a_anchor))"
		end

	frozen b2RevoluteJointDef_localAnchorA_get (a_object:POINTER):POINTER
			-- C getter of b2RevoluteJointDef.localAnchorA
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2RevoluteJointDef *)$a_object)->localAnchorA)"
		end

	frozen b2RevoluteJointDef_localAnchorA_set (a_object, a_value:POINTER)
			-- C setter of b2RevoluteJointDef.localAnchorA
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJointDef *)$a_object)->localAnchorA = *((b2Vec2 *)$a_value)"
		end

	frozen b2RevoluteJointDef_localAnchorB_get (a_object:POINTER):POINTER
			-- C getter of b2RevoluteJointDef.localAnchorB
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2RevoluteJointDef *)$a_object)->localAnchorB)"
		end

	frozen b2RevoluteJointDef_localAnchorB_set (a_object, a_value:POINTER)
			-- C setter of b2RevoluteJointDef.localAnchorB
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJointDef *)$a_object)->localAnchorB = *((b2Vec2 *)$a_value)"
		end

	frozen b2RevoluteJointDef_referenceAngle_get (a_object:POINTER):REAL_32
			-- C getter of b2RevoluteJointDef.referenceAngle
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJointDef *)$a_object)->referenceAngle"
		end

	frozen b2RevoluteJointDef_referenceAngle_set (a_object:POINTER; a_value:REAL_32)
			-- C setter of b2RevoluteJointDef.referenceAngle
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJointDef *)$a_object)->referenceAngle = $a_value"
		end

	frozen b2RevoluteJointDef_enableLimit_get (a_object:POINTER):BOOLEAN
			-- C getter of b2RevoluteJointDef.enableLimit
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJointDef *)$a_object)->enableLimit"
		end

	frozen b2RevoluteJointDef_enableLimit_set (a_object:POINTER; a_value:BOOLEAN)
			-- C setter of b2RevoluteJointDef.enableLimit
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJointDef *)$a_object)->enableLimit = $a_value"
		end

	frozen b2RevoluteJointDef_lowerAngle_get (a_object:POINTER):REAL_32
			-- C getter of b2RevoluteJointDef.lowerAngle
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJointDef *)$a_object)->lowerAngle"
		end

	frozen b2RevoluteJointDef_lowerAngle_set (a_object:POINTER; a_value:REAL_32)
			-- C setter of b2RevoluteJointDef.lowerAngle
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJointDef *)$a_object)->lowerAngle = $a_value"
		end

	frozen b2RevoluteJointDef_upperAngle_get (a_object:POINTER):REAL_32
			-- C getter of b2RevoluteJointDef.upperAngle
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJointDef *)$a_object)->upperAngle"
		end

	frozen b2RevoluteJointDef_upperAngle_set (a_object:POINTER; a_value:REAL_32)
			-- C setter of b2RevoluteJointDef.upperAngle
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJointDef *)$a_object)->upperAngle = $a_value"
		end

	frozen b2RevoluteJointDef_enableMotor_get (a_object:POINTER):BOOLEAN
			-- C getter of b2RevoluteJointDef.enableMotor
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJointDef *)$a_object)->enableMotor"
		end

	frozen b2RevoluteJointDef_enableMotor_set (a_object:POINTER; a_value:BOOLEAN)
			-- C setter of b2RevoluteJointDef.enableMotor
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJointDef *)$a_object)->enableMotor = $a_value"
		end

	frozen b2RevoluteJointDef_motorSpeed_get (a_object:POINTER):REAL_32
			-- C getter of b2RevoluteJointDef.motorSpeed
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJointDef *)$a_object)->motorSpeed"
		end

	frozen b2RevoluteJointDef_motorSpeed_set (a_object:POINTER; a_value:REAL_32)
			-- C setter of b2RevoluteJointDef.motorSpeed
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJointDef *)$a_object)->motorSpeed = $a_value"
		end

	frozen b2RevoluteJointDef_maxMotorTorque_get (a_object:POINTER):REAL_32
			-- C getter of b2RevoluteJointDef.maxMotorTorque
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJointDef *)$a_object)->maxMotorTorque"
		end

	frozen b2RevoluteJointDef_maxMotorTorque_set (a_object:POINTER; a_value:REAL_32)
			-- C setter of b2RevoluteJointDef.maxMotorTorque
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RevoluteJointDef *)$a_object)->maxMotorTorque = $a_value"
		end

feature -- b2RopeJoint

	frozen b2RopeJoint_GetAnchorA(a_object, a_value:POINTER)
			-- C function b2RopeJoint.GetAnchorA()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2RopeJoint *)$a_object)->GetAnchorA();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2RopeJoint_GetAnchorB(a_object, a_value:POINTER)
			-- C function b2RopeJoint.GetAnchorB()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2RopeJoint *)$a_object)->GetAnchorB();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2RopeJoint_GetReactionForce(a_object, a_value:POINTER; a_inv_dt:REAL_32)
			-- C function b2RopeJoint.GetReactionForce()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2RopeJoint *)$a_object)->GetReactionForce($a_inv_dt);
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2RopeJoint_GetReactionTorque(a_object:POINTER; a_inv_dt:REAL_32):REAL_32
			-- C function b2RopeJoint.GetReactionTorque()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RopeJoint *)$a_object)->GetReactionTorque($a_inv_dt)"
		end

	frozen b2RopeJoint_ShiftOrigin(a_object, a_value:POINTER)
			-- C function b2RopeJoint.ShiftOrigin()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RopeJoint *)$a_object)->ShiftOrigin(*((b2Vec2 *)$a_value))"
		end

feature -- b2RopeJointDef

	frozen b2RopeJointDef_new:POINTER
			-- C initialisation of a b2RopeJointDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2RopeJointDef ()"
		end

	frozen b2RopeJointDef_delete(a_object:POINTER)
			-- C destruction of a b2RopeJointDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2RopeJointDef *)$a_object)"
		end

	frozen b2RopeJointDef_localAnchorA_get (a_object:POINTER):POINTER
			-- C getter of b2RopeJointDef.localAnchorA
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2RopeJointDef *)$a_object)->localAnchorA)"
		end

	frozen b2RopeJointDef_localAnchorA_set (a_object, a_value:POINTER)
			-- C setter of b2RopeJointDef.localAnchorA
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RopeJointDef *)$a_object)->localAnchorA = *((b2Vec2 *)$a_value)"
		end

	frozen b2RopeJointDef_localAnchorB_get (a_object:POINTER):POINTER
			-- C getter of b2RopeJointDef.localAnchorB
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2RopeJointDef *)$a_object)->localAnchorB)"
		end

	frozen b2RopeJointDef_localAnchorB_set (a_object, a_value:POINTER)
			-- C setter of b2RopeJointDef.localAnchorB
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RopeJointDef *)$a_object)->localAnchorB = *((b2Vec2 *)$a_value)"
		end

	frozen b2RopeJointDef_maxLength_get (a_object:POINTER):REAL_32
			-- C getter of b2RopeJointDef.maxLength
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RopeJointDef *)$a_object)->maxLength"
		end

	frozen b2RopeJointDef_maxLength_set (a_object:POINTER; a_value:REAL_32)
			-- C setter of b2RopeJointDef.maxLength
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2RopeJointDef *)$a_object)->maxLength = $a_value"
		end

feature -- b2Rot

	frozen b2Rot_new(a_value:REAL_32):POINTER
			-- C initialisation of a b2Rot
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2Rot ($a_value)"
		end

	frozen b2Rot_delete(a_object:POINTER)
			-- C destruction of a b2Rot
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2Rot *)$a_object)"
		end

	frozen b2Rot_GetAngle(a_object:POINTER):REAL_32
			-- C function b2Rot.GetAngle()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Rot *)$a_object)->GetAngle()"
		end

	frozen b2Rot_Set(a_object:POINTER; a_value:REAL_32)
			-- C function b2Rot.Set()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Rot *)$a_object)->Set($a_value)"
		end

	frozen b2Rot_SetIdentity(a_object:POINTER)
			-- C function b2Rot.SetIdentity()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Rot *)$a_object)->SetIdentity()"
		end

	frozen b2Rot_GetXAxis(a_object:POINTER; a_value:POINTER)
			-- C function b2Rot.GetXAxis()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2Rot *)$a_object)->GetXAxis();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2Rot_GetYAxis(a_object:POINTER; a_value:POINTER)
			-- C function b2Rot.GetYAxis()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2Rot *)$a_object)->GetYAxis();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2Rot_s_get(a_object:POINTER):REAL_32
			-- C getter of b2Rot.s
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Rot *)$a_object)->s"
		end

	frozen b2Rot_c_get(a_object:POINTER):REAL_32
			-- C getter of b2Rot.c
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Rot *)$a_object)->c"
		end

feature -- b2Shape

	frozen b2Shape_GetChildCount(a_object:POINTER):INTEGER_32
			-- C function b2Shape.GetChildCount()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Shape *)$a_object)->GetChildCount()"
		end

	frozen b2Shape_GetType(a_object:POINTER):NATURAL
			-- C function b2Shape.GetType()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Shape *)$a_object)->GetType()"
		end

	frozen b2Shape_type_e_circle:NATURAL
			-- C constant e_circle
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2Shape::e_circle"
		end

	frozen b2Shape_type_e_edge:NATURAL
			-- C constant e_edge
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2Shape::e_edge"
		end
	frozen b2Shape_type_e_polygon:NATURAL
			-- C constant e_polygon
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2Shape::e_polygon"
		end
	frozen b2Shape_type_e_chain:NATURAL
			-- C constant e_ce_chainircle
		external
			"C++ inline use <Box2D.h>"
		alias
			"b2Shape::e_chain"
		end

	frozen b2Shape_TestPoint(a_object, a_transform, a_point:POINTER):BOOLEAN
			-- C function b2Shape.TestPoint()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Shape *)$a_object)->TestPoint(*((b2Transform *)$a_transform), *((b2Vec2 *)$a_point))"
		end

	frozen b2Shape_RayCast(a_object, a_output, a_input, a_transform:POINTER; a_child_index:INTEGER_32):BOOLEAN
			-- C function b2Shape.RayCast()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Shape *)$a_object)->RayCast((b2RayCastOutput *)$a_output, *((b2RayCastInput *)$a_input), *((b2Transform *)$a_transform), $a_child_index)"
		end

	frozen b2Shape_ComputeMass(a_object, a_mass_data:POINTER; a_density:REAL_32)
			-- C function b2Shape.ComputeMass()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Shape *)$a_object)->ComputeMass((b2MassData *)$a_mass_data, $a_density)"
		end

	frozen b2Shape_ComputeAABB(a_object, a_output, a_transform:POINTER; a_child_index:INTEGER_32)
			-- C function b2Shape.ComputeAABB()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Shape *)$a_object)->ComputeAABB((b2AABB *)$a_output, *((b2Transform *)$a_transform), $a_child_index)"
		end

feature -- b2Transform

	frozen b2Transform_new:POINTER
			-- C initialisation of a b2Transform
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2Transform ()"
		end

	frozen b2Transform_delete(a_object:POINTER)
			-- C destruction of a b2Transform
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2Transform *)$a_object)"
		end

	frozen b2Transform_p_get(a_object:POINTER):POINTER
			-- C getter of b2Transform.p
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2Transform *)$a_object)->p)"
		end

	frozen b2Transform_q_get(a_object:POINTER):POINTER
			-- C getter of b2Transform.q
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2Transform *)$a_object)->q)"
		end

	frozen b2Transform_SetIdentity(a_object:POINTER)
			-- C getter of b2Transform.SetIdentity
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Transform *)$a_object)->SetIdentity()"
		end

feature -- b2Vec2

	frozen b2Vec2_new:POINTER
			-- C initialisation of a b2Vec2
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2Vec2 ()"
		end

	frozen b2Vec2_sizeof:INTEGER
			-- C size of a b2Vec2 in byte
		external
			"C++ inline use <Box2D.h>"
		alias
			"sizeof(b2Vec2)"
		end

	frozen b2Vec2_delete(a_object:POINTER)
			-- C destruction of a b2Vec2
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2Vec2 *)$a_object)"
		end

	frozen b2Vec2_new_coordinates(a_x_in, a_y_in:REAL_32):POINTER
			-- C initialisation of a b2Vec2 from coordinates
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2Vec2 ($a_x_in, $a_y_in)"
		end

	frozen b2Vec2_x_get(a_object:POINTER):REAL_32
			-- C getter of b2Vec2.x
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Vec2 *)$a_object)->x"
		end

	frozen b2Vec2_x_set(a_object:POINTER; a_x:REAL_32)
			-- C setter of b2Vec2.x
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Vec2 *)$a_object)->x = $a_x"
		end

	frozen b2Vec2_y_get(a_object:POINTER):REAL_32
			-- C getter of b2Vec2.y
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Vec2 *)$a_object)->y"
		end

	frozen b2Vec2_y_set(a_object:POINTER; a_y:REAL_32)
			-- C setter of b2Vec2.y
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Vec2 *)$a_object)->y = $a_y"
		end

	frozen b2Vec2_set_zero(a_object:POINTER)
			-- C function b2Vec2.SetZero()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Vec2 *)$a_object)->SetZero()"
		end

	frozen b2Vec2_set(a_object:POINTER; a_x, a_y:REAL_32)
			-- C function b2Vec2.Set()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2Vec2 *)$a_object)->Set($a_x, $a_y)"
		end

feature -- b2WeldJoint

	frozen b2WeldJoint_GetAnchorA(a_object, a_value:POINTER)
			-- C function b2WeldJoint.GetAnchorA()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2WeldJoint *)$a_object)->GetAnchorA();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2WeldJoint_GetAnchorB(a_object, a_value:POINTER)
			-- C function b2WeldJoint.GetAnchorB()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2WeldJoint *)$a_object)->GetAnchorB();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2WeldJoint_GetReactionForce(a_object, a_value:POINTER; a_inv_dt:REAL_32)
			-- C function b2WeldJoint.GetReactionForce()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2WeldJoint *)$a_object)->GetReactionForce($a_inv_dt);
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2WeldJoint_GetReactionTorque(a_object:POINTER; a_inv_dt:REAL_32):REAL_32
			-- C function b2WeldJoint.GetReactionTorque()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WeldJoint *)$a_object)->GetReactionTorque($a_inv_dt)"
		end

	frozen b2WeldJoint_ShiftOrigin(a_object, a_value:POINTER)
			-- C function b2WeldJoint.ShiftOrigin()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WeldJoint *)$a_object)->ShiftOrigin(*((b2Vec2 *)$a_value))"
		end

feature -- b2WeldJointDef

	frozen b2WeldJointDef_new:POINTER
			-- C initialisation of a b2WeldJointDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2WeldJointDef ()"
		end

	frozen b2WeldJointDef_delete (a_object:POINTER)
			-- C destruction of a b2WeldJointDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2WeldJointDef *)$a_object)"
		end

	frozen b2WeldJointDef_Initialize (a_object, a_body_1, a_body_2, a_anchor:POINTER)
			-- C function b2WeldJointDef.Initialize()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WeldJointDef *)$a_object)->Initialize ((b2Body *)$a_body_1, (b2Body *)$a_body_2, *((b2Vec2 *)$a_anchor))"
		end

	frozen b2WeldJointDef_localAnchorA_get (a_object:POINTER):POINTER
			-- C getter of b2WeldJointDef.localAnchorA
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2WeldJointDef *)$a_object)->localAnchorA)"
		end

	frozen b2WeldJointDef_localAnchorA_set (a_object, a_value:POINTER)
			-- C setter of b2WeldJointDef.localAnchorA
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WeldJointDef *)$a_object)->localAnchorA = *((b2Vec2 *)$a_value)"
		end

	frozen b2WeldJointDef_localAnchorB_get (a_object:POINTER):POINTER
			-- C getter of b2WeldJointDef.localAnchorB
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2WeldJointDef *)$a_object)->localAnchorB)"
		end

	frozen b2WeldJointDef_localAnchorB_set (a_object, a_value:POINTER)
			-- C setter of b2WeldJointDef.localAnchorB
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WeldJointDef *)$a_object)->localAnchorB = *((b2Vec2 *)$a_value)"
		end

	frozen b2WeldJointDef_referenceAngle_get (a_object:POINTER):REAL_32
			-- C getter of b2WeldJointDef.referenceAngle
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WeldJointDef *)$a_object)->referenceAngle"
		end

	frozen b2WeldJointDef_referenceAngle_set (a_object:POINTER; a_value:REAL_32)
			-- C setter of b2WeldJointDef.referenceAngle
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WeldJointDef *)$a_object)->referenceAngle = $a_value"
		end

	frozen b2WeldJointDef_frequencyHz_get (a_object:POINTER):REAL_32
			-- C getter of b2WeldJointDef.frequencyHz
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WeldJointDef *)$a_object)->frequencyHz"
		end

	frozen b2WeldJointDef_frequencyHz_set (a_object:POINTER; a_value:REAL_32)
			-- C setter of b2WeldJointDef.frequencyHz
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WeldJointDef *)$a_object)->frequencyHz = $a_value"
		end

	frozen b2WeldJointDef_dampingRatio_get (a_object:POINTER):REAL_32
			-- C getter of b2WeldJointDef.dampingRatio
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WeldJointDef *)$a_object)->dampingRatio"
		end

	frozen b2WeldJointDef_dampingRatio_set (a_object:POINTER; a_value:REAL_32)
			-- C setter of b2WeldJointDef.dampingRatio
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WeldJointDef *)$a_object)->dampingRatio = $a_value"
		end

feature -- b2WheelJoint

	frozen b2WheelJoint_GetAnchorA(a_object, a_value:POINTER)
			-- C function b2WheelJoint.GetAnchorA()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2WheelJoint *)$a_object)->GetAnchorA();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2WheelJoint_GetAnchorB(a_object, a_value:POINTER)
			-- C function b2WheelJoint.GetAnchorB()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2WheelJoint *)$a_object)->GetAnchorB();
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2WheelJoint_GetReactionForce(a_object, a_value:POINTER; a_inv_dt:REAL_32)
			-- C function b2WheelJoint.GetReactionForce()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2WheelJoint *)$a_object)->GetReactionForce($a_inv_dt);
				((b2Vec2 *)$a_value)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2WheelJoint_GetReactionTorque(a_object:POINTER; a_inv_dt:REAL_32):REAL_32
			-- C function b2WheelJoint.GetReactionTorque()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WheelJoint *)$a_object)->GetReactionTorque($a_inv_dt)"
		end

	frozen b2WheelJoint_ShiftOrigin(a_object, a_value:POINTER)
			-- C function b2WheelJoint.ShiftOrigin()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WheelJoint *)$a_object)->ShiftOrigin(*((b2Vec2 *)$a_value))"
		end

feature -- b2WheelJointDef

	frozen b2WheelJointDef_new:POINTER
			-- C initialisation of a b2WheelJointDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2WheelJointDef ()"
		end

	frozen b2WheelJointDef_delete (a_object:POINTER)
			-- C destruction of a b2WheelJointDef
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2WheelJointDef *)$a_object)"
		end

	frozen b2WheelJointDef_Initialize (a_object, a_body_1, a_body_2, a_anchor, a_axis:POINTER)
			-- C function b2WheelJointDef.Initilize()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WheelJointDef *)$a_object)->Initialize ((b2Body *)$a_body_1, (b2Body *)$a_body_2, *((b2Vec2 *)$a_anchor), *((b2Vec2 *)$a_axis))"
		end

	frozen b2WheelJointDef_localAnchorA_get (a_object:POINTER):POINTER
			-- C getter of b2WheelJointDef.localAnchorA
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2WheelJointDef *)$a_object)->localAnchorA)"
		end

	frozen b2WheelJointDef_localAnchorA_set (a_object, a_anchor:POINTER)
			-- C setter of b2WheelJointDef.localAnchorA
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WheelJointDef *)$a_object)->localAnchorA = *((b2Vec2 *)$a_anchor)"
		end

	frozen b2WheelJointDef_localAnchorB_get (a_object:POINTER):POINTER
			-- C getter of b2WheelJointDef.localAnchorB
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2WheelJointDef *)$a_object)->localAnchorB)"
		end

	frozen b2WheelJointDef_localAnchorB_set (a_object, a_anchor:POINTER)
			-- C setter of b2WheelJointDef.localAnchorB
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WheelJointDef *)$a_object)->localAnchorB = *((b2Vec2 *)$a_anchor)"
		end

	frozen b2WheelJointDef_localAxisA_get (a_object:POINTER):POINTER
			-- C getter of b2WheelJointDef.localAxisA
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2WheelJointDef *)$a_object)->localAxisA)"
		end

	frozen b2WheelJointDef_localAxisA_set (a_object, a_anchor:POINTER)
			-- C setter of b2WheelJointDef.localAxisA
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WheelJointDef *)$a_object)->localAxisA = *((b2Vec2 *)$a_anchor)"
		end

	frozen b2WheelJointDef_enableMotor_get (a_object:POINTER):BOOLEAN
			-- C getter of b2WheelJointDef.enableMotor
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WheelJointDef *)$a_object)->enableMotor"
		end

	frozen b2WheelJointDef_enableMotor_set (a_object:POINTER; a_value:BOOLEAN)
			-- C setter of b2WheelJointDef.enableMotor
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WheelJointDef *)$a_object)->enableMotor = $a_value"
		end

	frozen b2WheelJointDef_maxMotorTorque_get (a_object:POINTER):REAL_32
			-- C getter of b2WheelJointDef.maxMotorTorque
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WheelJointDef *)$a_object)->maxMotorTorque"
		end

	frozen b2WheelJointDef_maxMotorTorque_set (a_object:POINTER; a_value:REAL_32)
			-- C setter of b2WheelJointDef.maxMotorTorque
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WheelJointDef *)$a_object)->maxMotorTorque = $a_value"
		end

	frozen b2WheelJointDef_motorSpeed_get (a_object:POINTER):REAL_32
			-- C getter of b2WheelJointDef.motorSpeed
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WheelJointDef *)$a_object)->motorSpeed"
		end

	frozen b2WheelJointDef_motorSpeed_set (a_object:POINTER; a_value:REAL_32)
			-- C setter of b2WheelJointDef.motorSpeed
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WheelJointDef *)$a_object)->motorSpeed = $a_value"
		end

	frozen b2WheelJointDef_frequencyHz_get (a_object:POINTER):REAL_32
			-- C getter of b2WheelJointDef.frequencyHz
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WheelJointDef *)$a_object)->frequencyHz"
		end

	frozen b2WheelJointDef_frequencyHz_set (a_object:POINTER; a_value:REAL_32)
			-- C setter of b2WheelJointDef.frequencyHz
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WheelJointDef *)$a_object)->frequencyHz = $a_value"
		end

	frozen b2WheelJointDef_dampingRatio_get (a_object:POINTER):REAL_32
			-- C getter of b2WheelJointDef.dampingRatio
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WheelJointDef *)$a_object)->dampingRatio"
		end

	frozen b2WheelJointDef_dampingRatio_set (a_object:POINTER; a_value:REAL_32)
			-- C setter of b2WheelJointDef.dampingRatio
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WheelJointDef *)$a_object)->dampingRatio = $a_value"
		end

feature -- b2World

	frozen b2World_new(a_value:POINTER):POINTER
			-- C initialisation of a b2World
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2World (*((b2Vec2 *)$a_value))"
		end

	frozen b2World_delete(a_object:POINTER)
			-- C destruction of a b2World
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2World *)$a_object)"
		end

	frozen b2World_SetContactListener(a_object, a_filter:POINTER)
			-- C function b2World.SetContactListener()
		external
			"C++ inline use <Box2D.h>"
		alias
--			"((b2WB2dCallbacks2dContactCallbacks *)$a_filter)"
			"((b2World *)$a_object)->SetContactListener((b2ContactListener *)$a_filter)"
		end

	frozen b2World_SetContactFilter(a_object, a_filter:POINTER)
			-- C function b2World.SetContactFilter()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->SetContactFilter((B2dCallbacks *)$a_filter)"
		end

	frozen b2World_CreateBody(a_object, a_value:POINTER):POINTER
			-- C function b2World.CreateBody()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->CreateBody((b2BodyDef *)$a_value)"
		end

	frozen b2World_DestroyBody(a_object, a_value:POINTER)
			-- C function b2World.DestroyBody()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->DestroyBody((b2Body *)$a_value)"
		end

	frozen b2World_CreateJoint(a_object, a_value:POINTER):POINTER
			-- C function b2World.CreateJoint()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->CreateJoint((b2JointDef *)$a_value)"
		end

	frozen b2World_DestroyJoint(a_object, a_value:POINTER)
			-- C function b2World.DestroyJoint()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->DestroyJoint((b2Joint *)$a_value)"
		end

	frozen b2World_Step(a_object:POINTER; a_time_step:REAL_32; a_velocity_iterations, a_position_iterations:INTEGER_32)
			-- C function b2World.Step()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->Step($a_time_step, $a_velocity_iterations, $a_position_iterations)"
		end

	frozen b2World_IsLocked(a_object:POINTER):BOOLEAN
			-- C function b2World.IsLocked()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->IsLocked()"
		end

	frozen b2World_GetBodyList(a_object:POINTER):POINTER
			-- C function b2World.GetBodyList()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->GetBodyList()"
		end

	frozen b2World_GetBodyCount(a_object:POINTER):INTEGER_32
			-- C function b2World.GetBodyCount()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->GetBodyCount()"
		end

	frozen b2World_GetJointList(a_object:POINTER):POINTER
			-- C function b2World.GetJointList()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->GetJointList()"
		end

	frozen b2World_GetJointCount(a_object:POINTER):INTEGER_32
			-- C function b2World.GetJointCount()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->GetJointCount()"
		end

	frozen b2World_GetContactList(a_object:POINTER):POINTER
			-- C function b2World.GetContactList()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->GetContactList()"
		end

	frozen b2World_GetContactCount(a_object:POINTER):INTEGER_32
			-- C function b2World.GetContactCount()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->GetContactCount()"
		end

	frozen b2World_ClearForces(a_object:POINTER)
			-- C function b2World.ClearForces()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->ClearForces()"
		end

	frozen b2World_SetAutoClearForces(a_object:POINTER; a_flag:BOOLEAN)
			-- C function b2World.SetAutoClearForces()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->SetAutoClearForces($a_flag)"
		end

	frozen b2World_GetAutoClearForces(a_object:POINTER):BOOLEAN
			-- C function b2World.GetAutoClearForces()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->GetAutoClearForces()"
		end

	frozen b2World_SetAllowSleeping(a_object:POINTER; a_flag:BOOLEAN)
			-- C function b2World.SetAllowSleeping()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->SetAllowSleeping($a_flag)"
		end

	frozen b2World_GetAllowSleeping(a_object:POINTER):BOOLEAN
			-- C function b2World.GetAllowSleeping()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->GetAllowSleeping()"
		end

	frozen b2World_SetWarmStarting(a_object:POINTER; a_flag:BOOLEAN)
			-- C function b2World.SetWarmStarting()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->SetWarmStarting($a_flag)"
		end

	frozen b2World_GetWarmStarting(a_object:POINTER):BOOLEAN
			-- C function b2World.GetWarmStarting()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->GetWarmStarting()"
		end

	frozen b2World_SetContinuousPhysics(a_object:POINTER; a_flag:BOOLEAN)
			-- C function b2World.SetContinuousPhysics()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->SetContinuousPhysics($a_flag)"
		end

	frozen b2World_GetContinuousPhysics(a_object:POINTER):BOOLEAN
			-- C function b2World.GetContinuousPhysics()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->GetContinuousPhysics()"
		end

	frozen b2World_SetSubStepping(a_object:POINTER; a_flag:BOOLEAN)
			-- C function b2World.SetSubStepping()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->SetSubStepping($a_flag)"
		end

	frozen b2World_GetSubStepping(a_object:POINTER):BOOLEAN
			-- C function b2World.GetSubStepping()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->GetSubStepping()"
		end

	frozen b2World_GetProxyCount(a_object:POINTER):INTEGER_32
			-- C function b2World.GetProxyCount()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->GetProxyCount()"
		end

	frozen b2World_GetTreeHeight(a_object:POINTER):INTEGER_32
			-- C function b2World.GetTreeHeight()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->GetTreeHeight()"
		end

	frozen b2World_GetTreeBalance(a_object:POINTER):INTEGER_32
			-- C function b2World.GetTreeBalance()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->GetTreeBalance()"
		end

	frozen b2World_GetTreeQuality(a_object:POINTER):REAL_32
			-- C function b2World.GetTreeQuality()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->GetTreeQuality()"
		end

	frozen b2World_SetGravity(a_object, a_gravity:POINTER)
			-- C function b2World.SetGravity()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->SetGravity(*((b2Vec2 *)$a_gravity))"
		end

	frozen b2World_GetGravity(a_object, a_gravity:POINTER)
			-- C function b2World.GetGravity()
		external
			"C++ inline use <Box2D.h>"
		alias
			"[
				b2Vec2 l_b2vec2 = ((b2World *)$a_object)->GetGravity();
				((b2Vec2 *)$a_gravity)->Set(l_b2vec2.x,l_b2vec2.y);
			]"
		end

	frozen b2World_ShiftOrigin(a_object, a_origin:POINTER)
			-- C function b2World.ShiftOrigin()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->ShiftOrigin(*((b2Vec2 *)$a_origin))"
		end

	frozen b2World_GetProfile(a_object:POINTER):POINTER
			-- C function b2World.GetProfile()
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2World *)$a_object)->GetProfile())"
		end

	frozen b2World_QueryAABB(a_object, a_callback, a_aabb:POINTER)
			-- C function b2World.QueryAABB()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->QueryAABB((B2dCallbacks *)$a_callback, *((b2AABB *)$a_aabb))"
		end

	frozen b2World_RayCast(a_object, a_callback, a_point1, a_point2:POINTER)
			-- C function b2World.RayCast()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2World *)$a_object)->RayCast((B2dCallbacks *)$a_callback, *((b2Vec2 *)$a_point1), *((b2Vec2 *)$a_point2))"
		end

feature -- b2WorldManifold

	frozen b2WorldManifold_normal(a_object:POINTER):POINTER
			-- C getter for b2WorldManifold.normal()
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2WorldManifold *)$a_object)->normal)"
		end

	frozen b2WorldManifold_new:POINTER
			-- C initialization of a b2WorldManifold
		external
			"C++ inline use <Box2D.h>"
		alias
			"new b2WorldManifold ()"
		end

	frozen b2WorldManifold_delete(a_object:POINTER)
			-- C destruction of a b2WorldManifold
		external
			"C++ inline use <Box2D.h>"
		alias
			"delete ((b2WorldManifold *)$a_object)"
		end

	frozen b2WorldManifold_Initialize(a_object, a_manifold, a_transform_1:POINTER; a_radius_1:REAL_32; a_transform_2:POINTER; a_radius_2:REAL_32)
			-- C function b2WorldManifold.Initialize()
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WorldManifold *)$a_object)->Initialize((b2Manifold *)$a_manifold,*((b2Transform *) $a_transform_1), $a_radius_1, *((b2Transform *) $a_transform_2), $a_radius_2)"
		end

	frozen b2WorldManifold_normal_get (a_object:POINTER):POINTER
			-- C getter of b2WorldManifold.normal
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2WorldManifold *)$a_object)->normal)"
		end

	frozen b2WorldManifold_points_get_at (a_object:POINTER; a_index:INTEGER):POINTER
			-- C getter of b2WorldManifold.points[a_index]
		external
			"C++ inline use <Box2D.h>"
		alias
			"&(((b2WorldManifold *)$a_object)->points[$a_index])"
		end

	frozen b2WorldManifold_separations_get_at (a_object:POINTER; a_index:INTEGER):REAL_32
			-- C getter of b2WorldManifold.separations[a_index]
		external
			"C++ inline use <Box2D.h>"
		alias
			"((b2WorldManifold *)$a_object)->separations[$a_index]"
		end

end
