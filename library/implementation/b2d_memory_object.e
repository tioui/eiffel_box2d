note
	description: "A B2D_* class that need an internal item {POINTER}."
	author: "Louis Marchand"
	date: "Fri, 05 Jun 2020 16:59:55 +0000"
	revision: "0.1"

deferred class
	B2D_MEMORY_OBJECT

inherit
	B2D_ANY
	DISPOSABLE
	HASHABLE


feature {NONE} -- Initialisation

	own_from_item(a_item:POINTER)
			-- Initialisation of `Current' using `a_item' as `item'.
			-- `Current' will delete `item'
		do
			item := a_item
			is_shared := False
		end

	shared_from_item(a_item:POINTER; a_with:detachable B2D_ANY)
			-- Initialisation of `Current' using `a_item' as `item'.
			-- `Current' will not delete `item'.
			-- `a_with' is the object that `a_item' is shared with.
		do
			item := a_item
			is_shared := True
			with := a_with
		end

feature -- Access

	exists:BOOLEAN
			-- `Current' is correctly initialized
		do
			Result := not item.is_default_pointer
		end

	hash_code: INTEGER
			-- <Precursor>
		do
			Result := item.hash_code
		end


feature {B2D_ANY} -- Implementation

	put_null
			-- Put `item' to NULL
		do
			create item
		end

	is_shared:BOOLEAN
			-- `Current' don't have to delete `item'

	item:POINTER
			-- Internal representation of `Current'

feature {NONE} -- Implementation

	with:detachable B2D_ANY
			-- The object that `item' is shared with.

	dispose
			-- <Precursor>
		do
			if not is_shared and not item.is_default_pointer then
				delete
			end
		end

	delete
			-- The internal C function that delete `item'
		require
			Can_Delete: not is_shared and not item.is_default_pointer
		deferred
		end

end
