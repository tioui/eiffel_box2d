note
	description: "The changing state of a contact point"
	author: "Louis Marchand"
	date: "Thu, 02 Jul 2020 16:19:12 +0000"
	revision: "0.1"

class
	B2D_POINT_STATE

inherit
	B2D_ANY

create {B2D_ANY}
	make

feature {NONE} -- Initialization

	make(a_item:NATURAL)
			-- Initialization of `Current' using `a_item' as `item'
		do
			item := a_item
		end

feature -- Access

	is_valid:BOOLEAN
			-- `Current' has a valid state.
		do
			Result := is_null or is_added or is_persisted or is_removed
		end

	is_null:BOOLEAN
			-- The point does not exists.
			-- Generally, the manifolds used does not interract in a point
		do
			Result := item ~ {B2D_EXTERNAL}.b2PointState_b2_nullState
		end

	is_added:BOOLEAN
			-- The point was added at update
		do
			Result := item ~ {B2D_EXTERNAL}.b2PointState_b2_addState
		end

	is_persisted:BOOLEAN
			-- The point persisted across update
		do
			Result := item ~ {B2D_EXTERNAL}.b2PointState_b2_persistState
		end

	is_removed:BOOLEAN
			-- The point was removed at update
		do
			Result := item ~ {B2D_EXTERNAL}.b2PointState_b2_addState
		end

feature {NONE} -- Implementation

	item:NATURAL
			-- Internal value of `Current'

end
