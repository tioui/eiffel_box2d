note
	description: "A polygone {2D_SHAPE}."
	author: "Louis Marchand"
	date: "Tue, 09 Jun 2020 13:27:08 +0000"
	revision: "0.1"

class
	B2D_SHAPE_POLYGON

inherit
	B2D_LIST_HELPER
	B2D_SHAPE_NO_CHILD
		rename
			radius as edge_width
		redefine
			is_valid, edge_width
		end

create
	make_box,
	make_oriented_box,
	make_oriented_box_with_vector,
	make_with_vertices,
	make_with_vertices_vector

create {B2D_ANY}
	own_from_item,
	shared_from_item

feature {NONE} -- Initialisation

	make_box(a_half_width, a_half_height:REAL_32)
			-- Initialisation of `Current' as an axis-aligned box centered
			-- on the local origin with `a_half_width' and `a_half_height'
			-- as the the half dimension of `Current'.
		do
			own_from_item ({B2D_EXTERNAL}.b2PolygonShape_new)
			if exists then
				{B2D_EXTERNAL}.b2PolygonShape_SetAsBox(item, a_half_width, a_half_height)
			end
		end

	make_oriented_box(a_half_width, a_half_height, a_angle, a_center_x, a_center_y:REAL_32)
			-- Initialisation of `Current' as a box centered at `a_center_x', `a_center_y',
			-- with `a_half_width' and `a_half_height' as the the half dimension of `Current'
			-- and with a rotation of `a_angle'.
		local
			l_vector:B2D_VECTOR_2D
		do
			create l_vector.make_with_coordinates (a_center_x, a_center_y)
			if l_vector.exists then
				make_oriented_box_with_vector(a_half_width, a_half_height, a_angle, l_vector)
			end
		end

	make_oriented_box_with_vector(a_half_width, a_half_height, a_angle:REAL_32; a_center:B2D_VECTOR_2D)
			-- Initialisation of `Current' as a box centered at `a_center',
			-- with `a_half_width' and `a_half_height' as the the half dimension of `Current'
			-- and with a rotation of `a_angle'.
		require
			Center_Exists: a_center.exists
		do
			own_from_item ({B2D_EXTERNAL}.b2PolygonShape_new)
			if exists then
				{B2D_EXTERNAL}.b2PolygonShape_SetAsBox_oriented(item, a_half_width, a_half_height, a_center.item, a_angle)
			end
		end

	make_with_vertices(a_vertices:FINITE[TUPLE[x, y:REAL_32]])
			-- Initialisaion of `Current' using `a_vertices' as vertex
			-- coordinates. Must create a convexe polygon.
		require
			Vertice_Count_Valid: a_vertices.count >= 3 and a_vertices.count <= Maximum_vertices
		local
			l_pointer:MANAGED_POINTER
			l_vector:B2D_VECTOR_2D
			l_count, l_sizeof_b2Vec2:INTEGER
		do
			own_from_item ({B2D_EXTERNAL}.b2PolygonShape_new)
			if exists then
				l_sizeof_b2Vec2 := {B2D_EXTERNAL}.b2Vec2_sizeof
				create l_pointer.make (a_vertices.count * l_sizeof_b2Vec2)
				l_count := 0
				across a_vertices as la_vertices loop
					create l_vector.shared_from_item (l_pointer.item + (l_count * l_sizeof_b2Vec2), Void)
					l_vector.set_coordinates (la_vertices.item.x, la_vertices.item.y)
					l_count := l_count + 1
				end
				set_vertices_from_pointer_and_count(l_pointer.item, a_vertices.count)
			end
		end

	make_with_vertices_vector(a_vertices:FINITE[B2D_VECTOR_2D])
			-- Initialisaion of `Current' using `a_vertices' as vertor
			-- coordinates. Must create a convexe polygon.
		require
			Vertices_Exists: across a_vertices as la_list all la_list.item.exists end
			Vertice_Count_Valid: a_vertices.count >= 3 and a_vertices.count <= Maximum_vertices
		local
			l_pointer:MANAGED_POINTER
			l_vector:B2D_VECTOR_2D
			l_count, l_sizeof_b2Vec2:INTEGER
		do
			own_from_item ({B2D_EXTERNAL}.b2PolygonShape_new)
			if exists then
				l_sizeof_b2Vec2 := {B2D_EXTERNAL}.b2Vec2_sizeof
				create l_pointer.make (a_vertices.count * l_sizeof_b2Vec2)
				l_count := 0
				across a_vertices as la_vertices loop
					create l_vector.shared_from_item (l_pointer.item + (l_count * l_sizeof_b2Vec2), Void)
					l_vector.set_coordinates (la_vertices.item.x, la_vertices.item.y)
					l_count := l_count + 1
				end
				set_vertices_from_pointer_and_count(l_pointer.item, a_vertices.count)
			end
		end

	set_vertices_from_pointer_and_count(a_pointer:POINTER; a_count:INTEGER)
			-- Initialisation of the `vertices' of `Current' using an internal C array
			-- of b2Vec2 pointed by `a_pointer' of size `a_count' (internal use only).
		require
			Exists: exists
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				{B2D_EXTERNAL}.b2PolygonShape_Set(item, a_pointer, a_count)
			else
				dispose
				put_null
			end
		rescue
			l_retry := True
			retry
		end


feature -- Access

	is_valid:BOOLEAN
			-- `Current' has a valid state.
		do
			Result := {B2D_EXTERNAL}.b2PolygonShape_Validate(item)
		end

	Maximum_vertices:INTEGER
			-- The maximum number of vertices in any {B2D_SHAPE_POLYGON}
		once
			Result := {B2D_EXTERNAL}.b2_maxPolygonVertices
		ensure
			class
		end

	centroid:B2D_VECTOR_2D
			-- Arithmetic mean position of all the points in `Current'
		require
			Exists: exists
		local
			l_vector:B2D_VECTOR_2D
		do
			create l_vector.shared_from_item ({B2D_EXTERNAL}.b2PolygonShape_m_centroid_get(item), Void)
			if l_vector.exists then
				create Result.make_with_coordinates (l_vector.x, l_vector.y)
			else
				create Result.own_from_item (l_vector.item)
			end
		end

	count:INTEGER
			-- The number of `vertices' in `Current'
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2PolygonShape_m_count_get(item)
		end

	vertices:LIST[B2D_VECTOR_2D]
			-- The point coodinates or every vertices on `Current'
		require
			Exists: exists
		do
			Result := c_array_to_vector_list({B2D_EXTERNAL}.b2PolygonShape_m_vertices_get(item), count)
		end

	normal_vertices:LIST[B2D_VECTOR_2D]
			-- The Normal representation of `vertices'
		require
			Exists: exists
		do
			Result := c_array_to_vector_list({B2D_EXTERNAL}.b2PolygonShape_m_normals_get(item), count)
		end

	edge_width:REAL_32
			-- The Used for buffer around the edge of `Current'
		do
			Result := Precursor
		end

feature {NONE} -- Implementation

	child_count:INTEGER_32
			-- The number of sub element of `Current'
		do
			Result := {B2D_EXTERNAL}.b2PolygonShape_GetChildCount(item)
		end

	internal_contains(a_transform_item, a_point_item:POINTER):BOOLEAN
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2PolygonShape_TestPoint(item, a_transform_item, a_point_item)
		end

	internal_ray_cast(a_result_item, a_input_item, a_transform_item:POINTER; a_child_index:INTEGER):BOOLEAN
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2PolygonShape_RayCast(item, a_result_item, a_input_item, a_transform_item, a_child_index)
		end

	internal_mass(a_mass_item:POINTER; a_density:REAL_32)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2PolygonShape_ComputeMass(item, a_mass_item, a_density)
		end

	internal_axis_aligned_bounding_box(a_mass_item, a_transform_item:POINTER; a_child_index:INTEGER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2PolygonShape_ComputeAABB(item, a_mass_item, a_transform_item, a_child_index)
		end

	delete
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2PolygonShape_delete(item)
		end

end
