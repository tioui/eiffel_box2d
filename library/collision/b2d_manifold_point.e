note
	description: "A contact point belonging to a {B2D_MANIFOLD}."
	author: "Louis Marchand"
	date: "Mon, 29 Jun 2020 20:38:40 +0000"
	revision: "0.1"

class
	B2D_MANIFOLD_POINT

inherit B2D_MEMORY_OBJECT

create {B2D_MANIFOLD}
	make

feature  {NONE} -- Initialization

	make(a_item:POINTER; a_manifold:B2D_MANIFOLD)
			-- Intialization of `Current' using `a_item' as `item' and
			-- shared with `a_manifold'
		do
			manifold_type := a_manifold.internal_type
			shared_from_item (a_item, a_manifold)
		end

feature -- Access

	local_point:B2D_VECTOR_2D
			-- The center or clip point of the {B2D_SHAPE}.
			-- if `is_crcle': the local center of second {B2D_SHAPE_CIRCLE};
			-- if `is_crcle': the local center of second {B2D_SHAPE_CIRCLE} or the clip point of the second {B2D_SHAPE_POLYGON};
			-- if `is_crcle': the clip point of the first {B2D_SHAPE_POLYGON}.			
		require
			Exists:exists
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2ManifoldPoint_localPoint_get(item))
		end

	normal_impulse:REAL_32
			-- The non-penetration impulse of `Current'.
			-- Note: used for internal caching; may not provide
			-- reliable contact forces, especially for high speed collisions.
		require
			Exists:exists
		do
			Result := {B2D_EXTERNAL}.b2ManifoldPoint_normalImpulse_get(item)
		end

	tangent_impulse:REAL_32
			-- The friction impulse of `Current'.
			-- Note: used for internal caching; may not provide
			-- reliable contact forces, especially for high speed collisions.
		require
			Exists:exists
		do
			Result := {B2D_EXTERNAL}.b2ManifoldPoint_tangentImpulse_get(item)
		end

	id:NATURAL
			-- The internal identifier of `Current'
		require
			Exists:exists
		do
			Result := {B2D_EXTERNAL}.b2ManifoldPoint_id_key_get(item)
		end

	index_1:INTEGER
			-- The child index of the first {B2D_SHAPE}.
			-- Usefull only if it is a {B2D_SHAPE_POLYGON}
		require
			Exists:exists
		do
			Result := {B2D_EXTERNAL}.b2ManifoldPoint_id_cf_indexA_get(item).to_integer_32
		end

	index_2:INTEGER
			-- The child index of the second {B2D_SHAPE}.
			-- Usefull only if it is a {B2D_SHAPE_POLYGON}
		require
			Exists:exists
		do
			Result := {B2D_EXTERNAL}.b2ManifoldPoint_id_cf_indexB_get(item).to_integer_32
		end

	is_intersect_vertex_1:BOOLEAN
			-- The first {B2D_SHAPE} intersect at a vertex point
		require
			Exists:exists
		do
			Result := {B2D_EXTERNAL}.b2ManifoldPoint_id_cf_typeA_get(item) ~ {B2D_EXTERNAL}.b2ContactFeature_type_e_vertex
		end

	is_intersect_vertex_2:BOOLEAN
			-- The second {B2D_SHAPE} intersect at a vertex point
		require
			Exists:exists
		do
			Result := {B2D_EXTERNAL}.b2ManifoldPoint_id_cf_typeB_get(item) ~ {B2D_EXTERNAL}.b2ContactFeature_type_e_vertex
		end

	is_intersect_face_1:BOOLEAN
			-- The first {B2D_SHAPE} intersect at a face
		require
			Exists:exists
		do
			Result := {B2D_EXTERNAL}.b2ManifoldPoint_id_cf_typeA_get(item) ~ {B2D_EXTERNAL}.b2ContactFeature_type_e_face
		end

	is_intersect_face_2:BOOLEAN
			-- The first {B2D_SHAPE} intersect at a face
		require
			Exists:exists
		do
			Result := {B2D_EXTERNAL}.b2ManifoldPoint_id_cf_typeB_get(item) ~ {B2D_EXTERNAL}.b2ContactFeature_type_e_face
		end

	is_circle:BOOLEAN
			-- The {B2D_MANIFOLD} represent a point versus point with radius (circle) contact
		do
			Result := manifold_type ~ {B2D_EXTERNAL}.b2Manifold_type_e_circles
		end

	is_face_1:BOOLEAN
			-- The {B2D_MANIFOLD} represent a clip point versus plane with radius (face) contact
		do
			Result := manifold_type ~ {B2D_EXTERNAL}.b2Manifold_type_e_faceA
		end

	is_face_2:BOOLEAN
			-- The {B2D_MANIFOLD} represent a clip point versus plane with radius (face) contact
		do
			Result := manifold_type ~ {B2D_EXTERNAL}.b2Manifold_type_e_faceB
		end


feature {NONE} -- Implementation

	manifold_type:NATURAL
			-- The internal type of the {B2D_MANIFOLD} that create `Current'

	delete
			-- <Precursor>
		do
			check Should_Not_be_used:False end
		end


end
