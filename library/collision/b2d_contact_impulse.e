note
	description: "[
					Contact impulses for reporting.
					Impulses are used instead of forces because sub-stepforces may approach infinity for
					rigid body collisions.
					These match up one-to-one with the contact points in {B2D_MANIFOLD}.
				]"
	author: "Louis Marchand"
	date: "Mon, 13 Jul 2020 20:30:32 +0000"
	revision: "0.1"

class
	B2D_CONTACT_IMPULSE

inherit
	B2D_MEMORY_OBJECT

create {B2D_ANY}
	shared_from_item

feature -- Access

	count:INTEGER
			-- The number of element in `Current'
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2ContactImpulse_count(item)
		end

	normal:LIST[REAL_32]
			-- Every normal impulse of `Current'
		require
			Exists: exists
		local
			l_count:INTEGER
		do
			l_count := count
			create {ARRAYED_LIST[REAL_32]}Result.make (l_count)
			across 1 |..| l_count as la_index loop
				Result.extend ({B2D_EXTERNAL}.b2ContactImpulse_normalImpulses_get_at(item, la_index.item))
			end
		end

	tangent:LIST[REAL_32]
			-- Every tengent impulse of `Current'
		require
			Exists: exists
		local
			l_count:INTEGER
		do
			l_count := count
			create {ARRAYED_LIST[REAL_32]}Result.make (l_count)
			across 1 |..| l_count as la_index loop
				Result.extend ({B2D_EXTERNAL}.b2ContactImpulse_tangentImpulses_get_at(item, la_index.item))
			end
		end


feature {NONE} -- Implementation


	delete
			-- <Precursor>
		do
			check Should_Not_be_used:False end
		end
end
