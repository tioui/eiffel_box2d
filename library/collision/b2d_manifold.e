note
	description: "[
					Information on contact for two touching convex shapes. Box2D supports multiple types of contact:
					- clip point versus plane with radius
					- point versus point with radius (circles)
				]"
	author: "Louis Marchand"
	date: "Mon, 29 Jun 2020 20:38:40 +0000"
	revision: "0.1"

class
	B2D_MANIFOLD

inherit
	B2D_MEMORY_OBJECT

create {B2D_ANY}
	shared_from_item

feature -- Access

	points:LIST[B2D_MANIFOLD_POINT]
			-- <Precursor>
		require
			Exists: exists
		local
			l_count:INTEGER
		do
			l_count :=point_count
			create {ARRAYED_LIST[B2D_MANIFOLD_POINT]}Result.make(l_count)
			across 0 |..| (l_count - 1) as la_index loop
				Result.extend (create {B2D_MANIFOLD_POINT}.make (
									{B2D_EXTERNAL}.b2Manifold_points_get_at(item, la_index.item), Current)
								)
			end
		end

	point_count:INTEGER
			-- The count of `points'
		require
			Exists:exists
		do
			Result := {B2D_EXTERNAL}.b2Manifold_pointCount_get(item)
		end

	local_point:B2D_VECTOR_2D
			-- The center of the {B2D_SHAPE}
			-- If `is_circle': the center of the first {B2D_SHAPE_CIRCLE}.
			-- If `is_face_1': the center of the first {B2D_SHAPE_POLYGON}.
			-- If `is_face_2': the center of the second {B2D_SHAPE_POLYGON}.
		require
			Exists:exists
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2Manifold_localPoint_get(item))
		end

	local_normal:B2D_VECTOR_2D
			-- The normal vector on the {B2D_SHAPE_POLYGON}
			-- (first if `is_face_1' and second if `is_face_2')
		require
			Exists:exists
			Not_Circle: not is_circle
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2Manifold_localNormal_get(item))
		end

	point_states(a_old_manifold:B2D_MANIFOLD):LIST[TUPLE[old_state, current_state:B2D_POINT_STATE]]
			-- The states pertain to the transition from `a_old_manifold'
			-- to `Current'. `old_state' is either persist or remove and
			-- `current_state' is either add or persist.
		require
			Exists:exists
			Old_Manifold_Exists: a_old_manifold.exists
		local
			l_array_point_1, l_array_point_2:ARRAY[NATURAL]
			l_to_c_point_1, l_to_c_point_2:ANY
			l_count:INTEGER
			l_state_1, l_state_2:B2D_POINT_STATE
		do
			create l_array_point_1.make_filled (0, 1, {B2D_EXTERNAL}.b2Manifold_b2_maxManifoldPoints)
			create l_array_point_2.make_filled (0, 1, {B2D_EXTERNAL}.b2Manifold_b2_maxManifoldPoints)
			l_to_c_point_1 := l_array_point_1.to_c
			l_to_c_point_2 := l_array_point_2.to_c
			{B2D_EXTERNAL}.b2PointState_b2GetPointStates($l_to_c_point_1, $l_to_c_point_2, a_old_manifold.item, item)
			create {ARRAYED_LIST[TUPLE[state_old, state_new:B2D_POINT_STATE]]}Result.make ({B2D_EXTERNAL}.b2Manifold_b2_maxManifoldPoints)
			from l_count := 1
			until l_count > l_array_point_1.count or l_count > l_array_point_2.count
			loop
				create l_state_1.make (l_array_point_1.at (l_count))
				create l_state_2.make (l_array_point_2.at (l_count))
				if
					l_state_1.is_valid and l_state_2.is_valid
				then
					Result.extend([l_state_1, l_state_2])
				end
				l_count := l_count + 1
			end
		ensure
			Value_Valid: across Result as la_result all
												(la_result.item.old_state.is_null or
												la_result.item.old_state.is_removed or
												la_result.item.old_state.is_persisted) and
												(la_result.item.old_state.is_null or
												la_result.item.current_state.is_added or
												la_result.item.current_state.is_persisted)
											end
		end

	is_circle:BOOLEAN
			-- `Current' represent a point versus point with radius (circle) contact
		require
			Exists:exists
		do
			Result := internal_type ~ {B2D_EXTERNAL}.b2Manifold_type_e_circles
		end

	is_face_1:BOOLEAN
			-- `Current' represent a clip point versus plane with radius (face) contact
		require
			Exists:exists
		do
			Result := internal_type ~ {B2D_EXTERNAL}.b2Manifold_type_e_faceA
		end

	is_face_2:BOOLEAN
			-- `Current' represent a clip point versus plane with radius (face) contact
		require
			Exists:exists
		do
			Result := internal_type ~ {B2D_EXTERNAL}.b2Manifold_type_e_faceB
		end

feature {B2D_MANIFOLD_POINT} -- Implementation

	internal_type:NATURAL
			-- The internal value of `is_circle', `is_face_1' and `is_face_2'
		require
			Exists:exists
		do
			Result := {B2D_EXTERNAL}.b2Manifold_type_get(item)
		end

feature {NONE} -- Implementation


	delete
			-- <Precursor>
		do
			check Should_Not_be_used:False end
		end

end
