note
	description: "A circle {B2D_SHAPE}."
	author: "Louis Marchand"
	date: "Tue, 09 Jun 2020 13:27:08 +0000"
	revision: "0.1"

class
	B2D_SHAPE_CIRCLE

inherit
	B2D_SHAPE_NO_CHILD
		redefine
			default_create, radius
		end

create
	default_create,
	make,
	make_with_coordinates,
	make_with_vector

create {B2D_ANY}
	own_from_item,
	shared_from_item

feature {NONE} -- Initialisation

	default_create
			-- Initialisation of `Current'
		do
			make
		end

	make_with_coordinates(a_position_x, a_position_y, a_radius:REAL_32)
			-- Initialisation of `Current' using `a_radius' as `radius' and
			-- `a_position_x' and `a_position_y' as `position'.
		do
			make
			if exists and position.exists then
				position.set_coordinates (a_position_x, a_position_y)
				set_radius (a_radius)
			end
		ensure
			Is_Assign_X: exists implies position.x ~ a_position_x
			Is_Assign_Y: exists implies position.y ~ a_position_y
			Is_Assign_Radius: exists implies radius ~ a_radius
		end

	make_with_vector(a_position:B2D_VECTOR_2D; a_radius:REAL_32)
			-- Initialisation of `Current' using `a_position' as `position' and
			-- `a_radius' as `radius'.
		require
			Position_Exists: a_position.exists
		do
			make_with_coordinates(a_position.x, a_position.y, a_radius)
		ensure
			Is_Assign: exists implies position~ a_position
			Is_Assign_Radius: exists implies radius ~ a_radius
		end

	make
			-- Initialisation of `Current' using `a_position_x' and `a_position_y'
			-- as `position'.
		do
			own_from_item ({B2D_EXTERNAL}.b2CircleShape_new)
		end

feature -- Access

	position:B2D_VECTOR_2D
			-- The world point of the center of `Current'
		require
			Exists: exists
		do
			create Result.shared_from_item ({B2D_EXTERNAL}.b2CircleShape_m_p_get(item), Current)
		end

	set_position_with_vector(a_position:B2D_VECTOR_2D)
			-- Assign `position' with the value of `a_position'
		require
			Exists: exists
			Positon_Exists: position.exists
			APosition_Exists: a_position.exists
		do
			position.set_coordinates (a_position.x, a_position.y)
		ensure
			Is_Assign: position ~ a_position
		end

	set_position(a_position_x, a_position_y:REAL_32)
			-- Assign `position' with the value of `a_position_x' and `a_position_y'.
		require
			Exists: exists
			Positon_Exists: position.exists
		do
			position.set_coordinates (a_position_x, a_position_y)
		ensure
			Is_Assign_X: position.x ~ a_position_x
			Is_Assign_Y: position.y ~ a_position_y
		end


	set_radius(a_radius:REAL_32)
			-- Assign `radius' with the value of `a_radius'
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2CircleShape_m_radius_set(item, a_radius)
		end

	radius:REAL_32 assign set_radius
			-- <Precursor>
		do
			Result := Precursor
		end


feature {NONE} -- Implementation

	child_count:INTEGER_32
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2CircleShape_GetChildCount(item)
		end

	internal_contains(a_transform_item, a_point_item:POINTER):BOOLEAN
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2CircleShape_TestPoint(item, a_transform_item, a_point_item)
		end

	internal_ray_cast(a_result_item, a_input_item, a_transform_item:POINTER; a_child_index:INTEGER):BOOLEAN
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2CircleShape_RayCast(item, a_result_item, a_input_item, a_transform_item, a_child_index)
		end

	internal_mass(a_mass_item:POINTER; a_density:REAL_32)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2CircleShape_ComputeMass(item, a_mass_item, a_density)
		end

	internal_axis_aligned_bounding_box(a_mass_item, a_transform_item:POINTER; a_child_index:INTEGER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2CircleShape_ComputeAABB(item, a_mass_item, a_transform_item, a_child_index)
		end

	delete
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2CircleShape_delete(item)
		end
end
