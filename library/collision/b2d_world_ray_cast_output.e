note
	description: "Result of {B2D_WORLD}.`ray_cast_query'"
	author: "Louis Marchand"
	date: "Mon, 13 Jul 2020 20:13:39 +0000"
	revision: "0.1"

class
	B2D_WORLD_RAY_CAST_OUTPUT

create
	make

feature -- Initialisation

	make(a_fixture:B2D_FIXTURE; a_point, a_normal:B2D_VECTOR_2D; a_fraction:REAL_32)
			-- Initialisation of `Current' using `a_fixture' as `fixture', `a_point' as
			-- `point', `a_normal' as `normal' and `a_fraction' as `fraction'
		do
			fixture := a_fixture
			point := a_point
			normal := a_normal
			fraction := a_fraction
		ensure
			Is_Assign_Fixture: fixture ~ a_fixture
			Is_Assign_Point: point ~ a_point
			Is_Assign_Normal: normal ~ a_normal
			Is_Assign_Fraction: fraction ~ a_fraction
		end

feature -- Access

	fixture:B2D_FIXTURE
			-- The fixture that intersect the ray.

	point:B2D_VECTOR_2D
			-- The normal vector of `Current'

	normal:B2D_VECTOR_2D
			-- The normal vector of `Current'

	fraction:REAL_32
			-- The fraction of `Current'

end
