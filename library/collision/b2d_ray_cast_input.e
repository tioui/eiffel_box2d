note
	description: "[
					Input used for Ray-Casting.
					The ray extends from point_1 to 
					point_1 + maximum_fraction * (point_2 - point_1)
				]"
	author: "Louis Marchand"
	date: "Tue, 09 Jun 2020 13:27:08 +0000"
	revision: "0.1"

class
	B2D_RAY_CAST_INPUT

inherit
	B2D_MEMORY_OBJECT
		redefine
			is_equal, default_create
		end
create
	default_create,
	make,
	make_with_vectors,
	make_with_coordinates

feature {NONE} -- Initialsation

	default_create
			-- Initialisation of `Current'
		do
			make
		end

	make
			-- Initialisation of `Current'
		do
			item := {B2D_EXTERNAL}.b2RayCastInput_new
		end

	make_with_vectors(a_vector_1, a_vector_2:B2D_VECTOR_2D; a_maximum_fraction:REAL_32)
			-- Initialisation of `Current' using `a_vector_1' as `point_1',
			-- `a_vector_2' as `point_2' and `a_maximum_fraction' as `maximum_fraction'
		require
			Vector_1_Exists: a_vector_1.exists
			Vector_2_Exists: a_vector_2.exists
		do
			make_with_coordinates(a_vector_1.x, a_vector_1.y, a_vector_2.x, a_vector_2.y, a_maximum_fraction)
		ensure
			Is_Assign_Vector_1: exists implies point_1 ~ a_vector_1
			Is_Assign_Vector_2: exists implies point_2 ~ a_vector_2
			Is_Assign_Maximum_Fraction: exists implies maximum_fraction ~ a_maximum_fraction
		end

	make_with_coordinates(a_point_1_x, a_point_1_y, a_point_2_x, a_point_2_y, a_maximum_fraction:REAL_32)
			-- Initialisation of `Current' using `a_vector_1' as `point_1',
			-- `a_vector_2' as `point_2' and `a_maximum_fraction' as `maximum_fraction'
		do
			make
			if exists then
				point_1.set_coordinates(a_point_1_x, a_point_1_y)
				point_2.set_coordinates(a_point_2_x, a_point_2_y)
				set_maximum_fraction(a_maximum_fraction)
			end
		ensure
			Is_Assign_Point_1_x: exists implies point_1.x ~ a_point_1_x
			Is_Assign_Point_1_x: exists implies point_1.y ~ a_point_1_y
			Is_Assign_Point_1_x: exists implies point_2.x ~ a_point_2_x
			Is_Assign_Point_1_x: exists implies point_2.y ~ a_point_2_y
			Is_Assign_Maximum_Fraction: exists implies maximum_fraction ~ a_maximum_fraction
		end

feature -- Access

	point_1:B2D_VECTOR_2D assign set_point_1_with_vector
			-- The first point of the Ray-Cast (see class documentation)
		require
			Exists: exists
		do
			create Result.shared_from_item ({B2D_EXTERNAL}.b2RayCastInput_p1_get(item), Current)
		end

	set_point_1_with_vector(a_point_1:B2D_VECTOR_2D)
			-- Assign `point_1' with the value of `a_point_1'
		require
			Exists: exists
			Point_1_Exists: point_1.exists
			APoint_1_Exists: a_point_1.exists
		do
			point_1.set_coordinates (a_point_1.x, a_point_1.y)
		ensure
			Is_Assign: point_1 ~ a_point_1
		end

	set_point_1(a_point_1_x, a_point_1_y:REAL_32)
			-- Assign `point_1' with the value of `a_point_1_x' and `a_point_1_y'
		require
			Exists: exists
			Point_1_Exists: point_1.exists
		do
			point_1.set_coordinates (a_point_1_x, a_point_1_y)
		ensure
			Is_Assign_X: point_1.x ~ a_point_1_x
			Is_Assign_Y: point_1.y ~ a_point_1_y
		end

	point_2:B2D_VECTOR_2D
			-- The second point of the Ray-Cast (see class documentation)
		require
			Exists: exists
		do
			create Result.shared_from_item ({B2D_EXTERNAL}.b2RayCastInput_p2_get(item), Current)
		end

	set_point_2_with_vector(a_point_2:B2D_VECTOR_2D)
			-- Assign `point_2' with the value of `a_point_2'
		require
			Exists: exists
			Point_2_Exists: point_2.exists
			APoint_2_Exists: a_point_2.exists
		do
			point_2.set_coordinates (a_point_2.x, a_point_2.y)
		ensure
			Is_Assign: point_2 ~ a_point_2
		end

	set_point_2(a_point_2_x, a_point_2_y:REAL_32)
			-- Assign `point_2' with the value of `a_point_2_x' and `a_point_2_y'
		require
			Exists: exists
			Point_2_Exists: point_2.exists
		do
			point_2.set_coordinates (a_point_2_x, a_point_2_y)
		ensure
			Is_Assign_X: point_2.x ~ a_point_2_x
			Is_Assign_Y: point_2.y ~ a_point_2_y
		end

	maximum_fraction:REAL_32 assign set_maximum_fraction
			-- The maximum fracion of the Ray-Cast (see class documentation)
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2RayCastInput_maxFraction_get(item)
		end

	set_maximum_fraction(a_maximum_fraction:REAL_32)
			-- Assign `maximum_fraction' with the value of `a_maximum_fraction'
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2RayCastInput_maxFraction_set(item, a_maximum_fraction)
		ensure
			Is_Assign: maximum_fraction ~ a_maximum_fraction
		end

	is_equal(a_other:like Current):BOOLEAN
			-- Is `a_other` attached to an object considered
			-- equal to current object?
		do
			if exists and a_other.exists then
				Result := point_1 ~ a_other.point_1 and
							point_2 ~ a_other.point_2 and
							maximum_fraction ~ a_other.maximum_fraction
			else
				Result := exists ~ a_other.exists
			end
		end


feature {NONE} -- Implementation

	delete
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2RayCastInput_delete(item)
		end


end
