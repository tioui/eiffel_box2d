note
	description: "Box used to know the bounding of a {B2D_SHAPE}."
	author: "Louis Marchand"
	date: "Tue, 09 Jun 2020 13:27:08 +0000"
	revision: "0.1"

class
	B2D_AXIS_ALIGNED_BOUNDING_BOX

inherit
	B2D_MEMORY_OBJECT
		redefine
			is_equal
		end

create
	make_with_coordinates,
	make_with_vectors

create {B2D_ANY}
	make

feature {NONE} -- Initialisation

	make
			-- Initialisation of `Current'
		do
			item := {B2D_EXTERNAL}.b2AABB_new
		end

	make_with_coordinates(a_lower_x, a_lower_y, a_upper_x, a_upper_y:REAL_32)
			-- Initialisation of `Current' using `a_lower_x' and `a_lower_y' as
			-- `lower_bound' and `a_upper_x' and `a_upper_y' as `upper_bound'
		do
			make
			if exists then
				set_lower_bound(a_lower_x, a_lower_y)
				set_upper_bound(a_upper_x, a_upper_y)
			end
		ensure
			Is_Lower_Assign_X: a_lower_x ~ lower_bound.x
			Is_Lower_Assign_Y: a_lower_y ~ lower_bound.y
			Is_Upper_Assign_X: a_upper_x ~ upper_bound.x
			Is_Upper_Assign_Y: a_upper_y ~ upper_bound.y
		end

	make_with_vectors(a_lower, a_upper:B2D_VECTOR_2D)
			-- Initialisation of `Current' using `a_lower' as
			-- `lower_bound' and `a_upper' as `upper_bound'
		require
			Lower_Exists: a_lower.exists
			Upper_Exists: a_upper.exists
		do
			make
			if exists then
				set_lower_bound_with_vector(a_lower)
				set_upper_bound_with_vector(a_upper)
			end
		ensure
			Is_Lower_Assign: a_lower ~ lower_bound
			Is_Upper_Assign: a_upper ~ upper_bound
		end

feature -- Access

	is_valid:BOOLEAN
			-- `True' if the bounds are sorted.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2AABB_IsValid(item)
		end

	lower_bound:B2D_VECTOR_2D assign set_lower_bound_with_vector
			-- The lower vertex
		require
			Exists: exists
		do
			create Result.shared_from_item ({B2D_EXTERNAL}.b2AABB_lowerBound_get(item), Current)
		end

	set_lower_bound_with_vector(a_lower_bound:B2D_VECTOR_2D)
			-- Assign `lower_bound' with the value of `a_lower_bound'
		require
			Exists: exists
			ALower_Bound_Exists: a_lower_bound.exists
			Lower_Bound_Exists: lower_bound.exists
		do
			lower_bound.set_coordinates (a_lower_bound.x, a_lower_bound.y)
		ensure
			Is_Assign: lower_bound ~ a_lower_bound
		end

	set_lower_bound(a_lower_bound_x, a_lower_bound_y:REAL_32)
			-- Assign `lower_bound' with the values `a_lower_bound_x' and `a_lower_bound_y'
		require
			Exists: exists
			Lower_Bound_Exists: lower_bound.exists
		do
			lower_bound.set_coordinates (a_lower_bound_x, a_lower_bound_y)
		ensure
			Is_Assign_X: lower_bound.x ~ a_lower_bound_x
			Is_Assign_Y: lower_bound.y ~ a_lower_bound_y
		end

	upper_bound:B2D_VECTOR_2D
			-- The upper vertex
		require
			Exists: exists
		do
			create Result.shared_from_item ({B2D_EXTERNAL}.b2AABB_upperBound_get(item), Current)
		end

	set_upper_bound_with_vector(a_upper_bound:B2D_VECTOR_2D)
			-- Assign `upper_bound' with the value of `a_upper_bound'
		require
			Exists: exists
			AUpper_Bound_Exists: a_upper_bound.exists
			Upper_Bound_Exists: upper_bound.exists
		do
			upper_bound.set_coordinates (a_upper_bound.x, a_upper_bound.y)
		ensure
			Is_Assign: upper_bound ~ a_upper_bound
		end

	set_upper_bound(a_upper_bound_x, a_upper_bound_y:REAL_32)
			-- Assign `upper_bound' with the values `a_upper_bound_x' and `a_upper_bound_y'
		require
			Exists: exists
			Upper_Bound_Exists: upper_bound.exists
		do
			upper_bound.set_coordinates (a_upper_bound_x, a_upper_bound_y)
		ensure
			Is_Assign_X: upper_bound.x ~ a_upper_bound_x
			Is_Assign_Y: upper_bound.y ~ a_upper_bound_y
		end

	combine_into_current(a_box:B2D_AXIS_ALIGNED_BOUNDING_BOX)
			-- Combine `a_box' with `Current'
		require
			Exists: exists
			Box_Exists: a_box.exists
			Is_Valid: is_valid
			Box_Is_Valid: a_box.is_valid
		do
			{B2D_EXTERNAL}.b2AABB_Combine(item, a_box.item)
		end

	combine(a_box:B2D_AXIS_ALIGNED_BOUNDING_BOX):B2D_AXIS_ALIGNED_BOUNDING_BOX
			-- Combine `Current' with `a_box'
		require
			Exists: exists
			Box_Exists: a_box.exists
			Is_Valid: is_valid
			Box_Is_Valid: a_box.is_valid
		do
			Create Result.make
			{B2D_EXTERNAL}.b2AABB_Combine2(Result.item, item, a_box.item)
		end

	contains(a_box:B2D_AXIS_ALIGNED_BOUNDING_BOX):BOOLEAN
			-- `True' if `Current' constains `a_box'
		require
			Exists: exists
			Box_Exists: a_box.exists
			Is_Valid: is_valid
			Box_Is_Valid: a_box.is_valid
		do
			Result := {B2D_EXTERNAL}.b2AABB_Contains(item, a_box.item)
		end

	ray_cast(a_input:B2D_RAY_CAST_INPUT):detachable B2D_RAY_CAST_OUTPUT
			-- Get the normal vector and the fraction for a ray-cast of `a_imput'.
		require
			Exists: exists
			Input_Exists: a_input.exists
			Is_Valid: is_valid
		local
			l_result:B2D_RAY_CAST_OUTPUT
		do
			create l_result
			if l_result.exists then
				if {B2D_EXTERNAL}.b2AABB_RayCast(item, l_result.item, a_input.item) then
					Result := l_result
				end
			end

		end

	center:B2D_VECTOR_2D
			-- The center of `Current'
		require
			Exists: exists
			Is_Valid: is_valid
		do
			create Result
			if Result.exists then
				{B2D_EXTERNAL}.b2AABB_GetCenter(item, Result.item)
			end
		end

	extends:B2D_VECTOR_2D
			-- The extents of `Current' (half-widths).
		require
			Exists: exists
			Is_Valid: is_valid
		do
			create Result
			if Result.exists then
				{B2D_EXTERNAL}.b2AABB_GetExtents(item, Result.item)
			end
		end

	perimeter:REAL_32
			-- The extents of `Current' (half-widths).
		require
			Exists: exists
			Is_Valid: is_valid
		do
			Result := {B2D_EXTERNAL}.b2AABB_GetPerimeter(item)
		end

	is_equal(a_other:like Current):BOOLEAN
			-- Is `a_other` attached to an object considered
			-- equal to current object?
		do
			if exists and a_other.exists then
				Result := lower_bound ~ a_other.lower_bound and upper_bound ~ a_other.upper_bound
			else
				Result := exists ~ a_other.exists
			end
		end


feature {NONE} -- Implementation

	delete
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2AABB_delete(item)
		end


end
