note
	description: "[
					Information on contact for a clip point versus plane (Face 2) with radius
					touching convex shapes.
				]"
	author: "Louis Marchand"
	date: "Mon, 29 Jun 2020 20:38:40 +0000"
	revision: "0.1"

class
	B2D_WORLD_MANIFOLD

inherit
	B2D_MEMORY_OBJECT

create
	make,
	make_with_radius

create {B2D_ANY}
	make_empty

feature {NONE} -- Initialisation

	make_empty(a_point_count:INTEGER)
			-- Initialisation of `Current' with `a_point_count' as `point_count'
		do
			own_from_item({B2D_EXTERNAL}.b2WorldManifold_new)
			point_count := a_point_count
		end

	make(a_manifold:B2D_MANIFOLD;
				a_shape_1:B2D_SHAPE; a_transform_1:B2D_TRANSFORM;
				a_shape_2:B2D_SHAPE; a_transform_2:B2D_TRANSFORM
			)
			-- Initialisation of `current' using `a_manifold' between `a_shape_1' with `a_transform_1'
			-- and `a_shape_2' with `a_transform_2'
		require
			Manifold_Exists: a_manifold.exists
			Shape_1_Exists: a_shape_1.exists
			Transform_1_Exists: a_transform_1.exists
			Shape_2_Exists: a_shape_2.exists
			Transform_2_Exists: a_transform_2.exists
		do
			make_with_radius(a_manifold, a_shape_1.radius, a_transform_1, a_shape_2.radius, a_transform_2)
		end

	make_with_radius(a_manifold:B2D_MANIFOLD;
				a_radius_1:REAL_32; a_transform_1:B2D_TRANSFORM;
				a_radius_2:REAL_32; a_transform_2:B2D_TRANSFORM
			)
			-- Initialisation of `current' using `a_manifold' between a shape with radius `a_shape_1'
			-- with `a_transform_1' and  shape with radius `a_radius_2' with `a_transform_2'
		require
			Manifold_Exists: a_manifold.exists
			Transform_1_Exists: a_transform_1.exists
			Transform_2_Exists: a_transform_2.exists
		do
			make_empty(a_manifold.point_count)
			if exists then
				{B2D_EXTERNAL}.b2WorldManifold_Initialize(item, a_manifold.item, a_transform_1.item, a_radius_1, a_transform_2.item, a_radius_2)
			end
		end

feature -- Access

	normal:B2D_VECTOR_2D
			-- World vector pointing from first to the second object
		require
			Exists:exists
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2WorldManifold_normal_get(item))
		end

	point_count:INTEGER
			-- The count of `points'

	points:LIST[B2D_VECTOR_2D]
			-- World contact point (point of intersection)
		require
			Exists:exists
		local
			l_count:INTEGER
		do
			l_count := point_count
			create {ARRAYED_LIST[B2D_VECTOR_2D]}Result.make (l_count)
			across 0 |..| (l_count - 1) as la_index loop
				Result.extend (create {B2D_VECTOR_2D}.make_from_item (
										{B2D_EXTERNAL}.b2WorldManifold_points_get_at(item, la_index.item))
							)
			end
		end

	separations:LIST[REAL_32]
			-- A negative value indicates overlap, in meters
		require
			Exists:exists
		local
			l_count:INTEGER
		do
			l_count := point_count
			create {ARRAYED_LIST[REAL_32]}Result.make (l_count)
			across 0 |..| (l_count - 1) as la_index loop
				Result.extend ({B2D_EXTERNAL}.b2WorldManifold_separations_get_at(item, la_index.item))
			end
		end

feature {NONE} -- Implementation

	delete
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2WorldManifold_delete(item)
		end

end
