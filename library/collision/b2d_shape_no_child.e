note
	description: "Common ancestor for {B2D_SHAPE} that always have `child_count' = 1."
	author: "Louis Marchand"
	date: "Thu, 18 Jun 2020 16:57:12 +0000"
	revision: "0.1"

deferred class
	B2D_SHAPE_NO_CHILD

inherit
	B2D_SHAPE
		rename
			ray_cast as shape_ray_cast,
			axis_aligned_bounding_box as shape_axis_aligned_bounding_box
		export
			{NONE} shape_ray_cast, shape_axis_aligned_bounding_box
		end

feature -- Access


	ray_cast(
				a_input:B2D_RAY_CAST_INPUT;
				a_transform:B2D_TRANSFORM
					):detachable B2D_RAY_CAST_OUTPUT
			-- Get the normal vector and the fraction for a ray-cast of
			-- `a_imput' using `a_transform'.
			-- Void if no contact is detected.
		require
			Exists: exists
			Input_Exists: a_input.exists
			Transform_Exists: a_transform.exists
		do
			Result := shape_ray_cast(a_input, a_transform, 1)
		end

	axis_aligned_bounding_box(a_transform:B2D_TRANSFORM): B2D_AXIS_ALIGNED_BOUNDING_BOX
			-- The box containing `Current' using `a_transform'
		require
			Exists: exists
		do
			Result := shape_axis_aligned_bounding_box(a_transform, 1)
		end

invariant
	No_Child: exists implies child_count ~ 1
end
