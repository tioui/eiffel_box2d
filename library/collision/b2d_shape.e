note
	description: "The shape of a {B2D_BODY}."
	author: "Louis Marchand"
	date: "Tue, 09 Jun 2020 13:27:08 +0000"
	revision: "0.1"

deferred class
	B2D_SHAPE

inherit
	B2D_MEMORY_OBJECT

feature -- Access

	is_valid:BOOLEAN
			-- `Current' represent a valid shape
		require
			exists
		do
			Result := True
		end

	child_count:INTEGER_32
			-- The number of sub element of `Current'
		require
			Exists: exists
		deferred
		end

	contains(a_transform:B2D_TRANSFORM; a_x, a_y:REAL_32):BOOLEAN
			-- Is the point (`a_x', `a_y') with the transformation `a_transform' is
			-- inside `Current'
		require
			Exists: exists
			Transform_Exists: a_transform.exists
		local
			l_point:B2D_VECTOR_2D
		do
			create l_point.make_with_coordinates (a_x, a_y)
			check l_point.exists then
				Result := contains_with_vector(a_transform, l_point)
			end
		end

	contains_with_vector(a_transform:B2D_TRANSFORM; a_point:B2D_VECTOR_2D):BOOLEAN
			-- Is `a_point' with the transformation `a_transform' is
			-- inside `Current'
		require
			Exists: exists
			Transform_Exists: a_transform.exists
			Point_Exists: a_point.exists
		do
			Result := internal_contains(a_transform.item, a_point.item)
		end

	radius:REAL_32
			-- The demi-dimension of `Current'
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2CircleShape_m_radius_get(item)
		end

	ray_cast(
				a_input:B2D_RAY_CAST_INPUT;
				a_transform:B2D_TRANSFORM;
				a_child_index:INTEGER_32
					):detachable B2D_RAY_CAST_OUTPUT
			-- Get the normal vector and the fraction for a ray-cast of
			-- `a_imput' using `a_transform' for the child `a_child_index'
			-- of `Current'. `a_child_index' start at 1.
			-- Void if no contact is detected.
		require
			Exists: exists
			Input_Exists: a_input.exists
			Transform_Exists: a_transform.exists
			Child_Index_Valid: a_child_index > 0 and a_child_index <= child_count
		local
			l_result:B2D_RAY_CAST_OUTPUT
		do
			create l_result
			if l_result.exists then
				if internal_ray_cast(l_result.item, a_input.item, a_transform.item, a_child_index - 1) then
					Result := l_result
				end
			end
		end

	axis_aligned_bounding_box(a_transform:B2D_TRANSFORM; a_child_index:INTEGER_32): B2D_AXIS_ALIGNED_BOUNDING_BOX
			-- The box containing `Current' using `a_transform'. `a_child_index' start at 1.
		require
			Exists: exists
			Child_Index_Valid: a_child_index > 0 and a_child_index <= child_count
		do
			create Result.make
			if Result.exists then
				internal_axis_aligned_bounding_box(Result.item, a_transform.item, a_child_index - 1)
			end
		end

	mass(a_density:REAL_32):B2D_MASS
			-- The mass of `Current' with a density of `a_density'
		require
			Exists: exists
		do
			create Result
			if Result.exists then
				internal_mass(Result.item, a_density)
			end
		end

feature {NONE} -- implementation

	internal_contains(a_transform_item, a_point_item:POINTER):BOOLEAN
			-- Internal C++ function for `contains_with_vector'
		require
			exists
		deferred
		end

	internal_ray_cast(a_result_item, a_input_item, a_transform_item:POINTER; a_child_index:INTEGER):BOOLEAN
			-- Internal C++ function for `ray_cast'
		require
			exists
		deferred
		end

	internal_mass(a_mass_item:POINTER; a_density:REAL_32)
			-- Internal C++ function for `mass'
		require
			exists
		deferred
		end

	internal_axis_aligned_bounding_box(a_mass_item, a_transform_item:POINTER; a_child_index:INTEGER)
			-- Internal C++ function for `axis_aligned_bounding_box'
		require
			exists
		deferred
		end

end
