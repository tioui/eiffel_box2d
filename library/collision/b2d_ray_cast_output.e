note
	description: "Output value of a Ray-Cast."
	author: "Louis Marchand"
	date: "Tue, 09 Jun 2020 13:27:08 +0000"
	revision: "0.1"

class
	B2D_RAY_CAST_OUTPUT

inherit
	B2D_MEMORY_OBJECT
		redefine
			default_create, is_equal
		end

create {B2D_ANY}
	default_create

feature {NONE} -- Initialisation

	default_create
			-- Initialisation of `Current'
		do
			item := {B2D_EXTERNAL}.b2RayCastOutput_new
		end

feature -- Access

	normal:B2D_VECTOR_2D
			-- The normal vector of `Current'
		require
			Exists: exists
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2RayCastOutput_normal_get(item))
		end

	fraction:REAL_32
			-- The fraction of `Current'
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2RayCastOutput_fraction_get(item)
		end

	is_equal(a_other:like Current):BOOLEAN
			-- Is `a_other` attached to an object considered
			-- equal to current object?
		do
			if exists and a_other.exists then
				Result := normal ~ a_other.normal and fraction ~ a_other.fraction
			else
				Result := exists ~ a_other.exists
			end
		end


feature {NONE} -- Implementation

	delete
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2RayCastOutput_delete(item)
		end

end
