note
	description: "An edge {B2D_SHAPE}. Does not have an area."
	author: "Louis Marchand"
	date: "Tue, 09 Jun 2020 13:27:08 +0000"
	revision: "0.1"

class
	B2D_SHAPE_EDGE

inherit
	B2D_SHAPE_EDGE_READABLE
		redefine
			start_vertex, end_vertex
		end

create
	make,
	make_with_vectors

create {B2D_ANY}
	own_from_item,
	shared_from_item

feature {NONE} -- Initialisation

	make(a_x1, a_y1, a_x2, a_y2:REAL_32)
			-- Initialisation of `Current' from the `start_vertex'
			-- (`a_x1',`a_y1') to the `end_vertex' (`a_x2',`a_y2')
		local
			l_vector1, l_vector2:B2D_VECTOR_2D
		do
			create l_vector1.make_with_coordinates (a_x1, a_y1)
			create l_vector2.make_with_coordinates (a_x2, a_y2)
			if l_vector1.exists and l_vector2.exists then
				make_with_vectors(l_vector1, l_vector2)
			end
		end

	make_with_vectors(a_start_vertex, a_end_vertex:B2D_VECTOR_2D)
			-- Initialisation of `Current' with `a_start_vertex'
			-- as `start_vertex' and `a_end_vertex' as `end_vertex'
		require
			Start_Vertex_Exists: a_start_vertex.exists
			End_Vertex_Exists: a_end_vertex.exists
		do
			default_create
			if exists then
				{B2D_EXTERNAL}.b2EdgeShape_set(item, a_start_vertex.item, a_end_vertex.item)
			end
		end

feature -- Access

	start_vertex:B2D_VECTOR_2D
			-- <Precursor>
		do
			create Result.shared_from_item ({B2D_EXTERNAL}.b2edgeshape_m_vertex1_get (item), Current)
		end

	set_start_vertex_with_vector(a_start_vertex:B2D_VECTOR_2D)
			-- Assign `start_vertex' with the value of `a_start_vertex'
		require
			Exists: exists
			Start_Vertex_Exists: start_vertex.exists
			AStart_Vertex_Exists: a_start_vertex.exists
		do
			start_vertex.set_coordinates (a_start_vertex.x, a_start_vertex.y)
		ensure
			Is_Assign: start_vertex ~ a_start_vertex
		end

	set_start_vertex(a_start_vertex_x, a_start_vertex_y:REAL_32)
			-- Assign `start_vertex' with the values `a_start_vertex_x' and `a_start_vertex_y'.
		require
			Exists: exists
			Start_Vertex_Exists: start_vertex.exists
		do
			start_vertex.set_coordinates (a_start_vertex_x, a_start_vertex_y)
		ensure
			Is_Assign_X: start_vertex.x ~ a_start_vertex_x
			Is_Assign_Y: start_vertex.y ~ a_start_vertex_y
		end

	end_vertex:B2D_VECTOR_2D
			-- <Precursor>
		do
			create Result.shared_from_item ({B2D_EXTERNAL}.b2edgeshape_m_vertex2_get (item), Current)
		end

	set_end_vertex_with_vector(a_end_vertex:B2D_VECTOR_2D)
			-- Assign `end_vertex' with the value of `a_end_vertex'
		require
			Exists: exists
			Start_Vertex_Exists: end_vertex.exists
			AStart_Vertex_Exists: a_end_vertex.exists
		do
			end_vertex.set_coordinates (a_end_vertex.x, a_end_vertex.y)
		ensure
			Is_Assign: end_vertex ~ a_end_vertex
		end

	set_end_vertex(a_end_vertex_x, a_end_vertex_y:REAL_32)
			-- Assign `end_vertex' with the values `a_end_vertex_x' and `a_end_vertex_y'.
		require
			Exists: exists
			Start_Vertex_Exists: end_vertex.exists
		do
			end_vertex.set_coordinates (a_end_vertex_x, a_end_vertex_y)
		ensure
			Is_Assign_X: end_vertex.x ~ a_end_vertex_x
			Is_Assign_Y: end_vertex.y ~ a_end_vertex_y
		end

	set_before_vertex_with_vector(a_vertex:B2D_VECTOR_2D)
			-- Assign `before_vertex' with the value of `a_vertex'
		require
			Exists: exists
		do
			set_before_vertex(a_vertex.x, a_vertex.y)
		ensure
			Has_Before_Vertex: has_before_vertex
			Is_Assign: a_vertex ~ before_vertex
		end

	set_before_vertex(a_x, a_y:REAL_32)
			-- Assign `before_vertex' with the value of `a_x' and `a_y'
		require
			Exists: exists
		local
			l_internal_vertex:B2D_VECTOR_2D
		do
			create l_internal_vertex.shared_from_item ({B2D_EXTERNAL}.b2EdgeShape_m_vertex0_get(item), Void)
			check l_internal_vertex.exists end
			l_internal_vertex.set_coordinates (a_x, a_y)
			{B2D_EXTERNAL}.b2EdgeShape_m_hasVertex0_set(item, True)
		ensure
			Has_Before_Vertex: has_before_vertex
			Is_Assign_X: a_x ~ before_vertex.x
			Is_Assign_Y: a_y ~ before_vertex.y
		end

	clear_before_vertex
			-- Put `has_before_vertex' to `False'
		require
			Exists: exists
		local
			l_internal_vertex:B2D_VECTOR_2D
		do
			create l_internal_vertex.shared_from_item ({B2D_EXTERNAL}.b2EdgeShape_m_vertex0_get(item), Void)
			check l_internal_vertex.exists end
			l_internal_vertex.set_zero
			{B2D_EXTERNAL}.b2EdgeShape_m_hasVertex0_set(item, False)
		ensure
			Is_Cleared: not has_before_vertex
		end

	set_after_vertex_with_vector(a_vertex:B2D_VECTOR_2D)
			-- Assign `after_vertex' with the value of `a_vertex'
		require
			Exists: exists
		do
			set_after_vertex(a_vertex.x, a_vertex.y)
		ensure
			Has_Before_Vertex: has_after_vertex
			Is_Assign: a_vertex ~ after_vertex
		end

	set_after_vertex(a_x, a_y:REAL_32)
			-- Assign `after_vertex' with the value of `a_x' and `a_y'
		require
			Exists: exists
		local
			l_internal_vertex:B2D_VECTOR_2D
		do
			create l_internal_vertex.shared_from_item ({B2D_EXTERNAL}.b2EdgeShape_m_Vertex3_get(item), Void)
			check l_internal_vertex.exists end
			l_internal_vertex.set_coordinates (a_x, a_y)
			{B2D_EXTERNAL}.b2EdgeShape_m_hasVertex3_set(item, True)
		ensure
			Has_Before_Vertex: has_after_vertex
			Is_Assign_X: a_x ~ after_vertex.x
			Is_Assign_Y: a_y ~ after_vertex.y
		end

	clear_after_vertex
			-- Put `has_after_vertex' to `False'
		require
			Exists: exists
		local
			l_internal_vertex:B2D_VECTOR_2D
		do
			create l_internal_vertex.shared_from_item ({B2D_EXTERNAL}.b2EdgeShape_m_Vertex3_get(item), Void)
			check l_internal_vertex.exists end
			l_internal_vertex.set_zero
			{B2D_EXTERNAL}.b2EdgeShape_m_hasVertex3_set(item, False)
		ensure
			Is_Cleared: not has_after_vertex
		end

end
