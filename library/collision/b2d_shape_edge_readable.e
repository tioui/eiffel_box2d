note
	description: "A chain {B2D_SHAPE}. Does not have an area."
	author: "Louis Marchand"
	date: "Thu, 18 Jun 2020 19:25:33 +0000"
	revision: "0.1"

class
	B2D_SHAPE_EDGE_READABLE

inherit
	B2D_SHAPE_NO_CHILD
		rename
			radius as width
		redefine
			width, default_create
		end

create {B2D_ANY}
	default_create

feature {NONE} -- Initialisation

	default_create
			-- Initialisation of `Current'
		do
			own_from_item ({B2D_EXTERNAL}.b2EdgeShape_new)
		end


feature -- Access

	start_vertex:B2D_VECTOR_2D
			-- The starting vertex of `Current'
		require
			Exists: exists
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2edgeshape_m_vertex1_get (item))
		end

	end_vertex:B2D_VECTOR_2D
			-- The ending vertex of `Current'
		require
			Exists: exists
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2edgeshape_m_vertex2_get (item))
		end

	has_before_vertex:BOOLEAN
			-- `Current' has a `before_vertex'
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2EdgeShape_m_hasVertex0_get(item)
		end

	before_vertex:B2D_VECTOR_2D
			-- If there is a vertex before `start_vertex'.
			-- Permit smooth transition
		require
			Exists: exists
			Has_Before_Vertex: has_before_vertex
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2EdgeShape_m_vertex0_get(item))
		end

	has_after_vertex:BOOLEAN
			-- `Current' has a `after_vertex'
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2EdgeShape_m_hasVertex3_get(item)
		end

	after_vertex:B2D_VECTOR_2D
			-- If there is a vertex after `end_vertex'.
			-- Permit smooth transition
		require
			Exists: exists
			Has_After_Vertex: has_after_vertex
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2EdgeShape_m_Vertex3_get(item))
		end

	width:REAL_32
			-- The Used for buffer around `Current'
		do
			Result := Precursor
		end


feature {NONE} -- Implementation

	child_count:INTEGER_32
			-- The number of sub element of `Current'
		do
			Result := {B2D_EXTERNAL}.b2EdgeShape_GetChildCount(item)
		end

	internal_contains(a_transform_item, a_point_item:POINTER):BOOLEAN
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2EdgeShape_TestPoint(item, a_transform_item, a_point_item)
		end

	internal_ray_cast(a_result_item, a_input_item, a_transform_item:POINTER; a_child_index:INTEGER):BOOLEAN
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2EdgeShape_RayCast(item, a_result_item, a_input_item, a_transform_item, a_child_index)
		end

	internal_mass(a_mass_item:POINTER; a_density:REAL_32)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2EdgeShape_ComputeMass(item, a_mass_item, a_density)
		end

	internal_axis_aligned_bounding_box(a_mass_item, a_transform_item:POINTER; a_child_index:INTEGER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2EdgeShape_ComputeAABB(item, a_mass_item, a_transform_item, a_child_index)
		end

	delete
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2EdgeShape_delete(item)
		end

end
