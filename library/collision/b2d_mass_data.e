note
	description: "Information about masses."
	author: "Louis Marchand"
	date: "Wed, 24 Jun 2020 19:48:58 +0000"
	revision: "0.1"

class
	B2D_MASS_DATA

inherit
	B2D_MASS
		export
			{NONE} to_data
		redefine
			mass, inertia, center
		end

create
	default_create

feature -- Access

	center:B2D_VECTOR_2D assign set_center_with_vector
			-- <Precursor>
		do
			create Result.shared_from_item ({B2D_EXTERNAL}.b2MassData_center_get(item), Current)
		end

	set_center_with_vector(a_center:B2D_VECTOR_2D)
			-- Assign `center' with the value of `a_center'
		require
			Exists: exists
			Center_Exists: center.exists
			ACenter_Exists: a_center.exists
		do
			center.set_coordinates (a_center.x, a_center.y)
		ensure
			Is_Assign: center ~ a_center
		end

	set_center(a_center_x, a_center_y:REAL_32)
			-- Assign `center' with the values `a_center_x' and `a_center_y'
		require
			Exists: exists
			Center_Exists: center.exists
		do
			center.set_coordinates (a_center_x, a_center_y)
		ensure
			Is_Assign_X: center.x ~ a_center_x
			Is_Assign_Y: center.y ~ a_center_y
		end

	mass:REAL_32 assign set_mass
			-- <Precursor>
		do
			Result := Precursor
		end

	set_mass(a_mass:REAL_32)
			-- Assign `mass' with the value of `a_mass'
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2MassData_mass_set(item, a_mass)
		ensure
			Is_Assign: mass ~ a_mass
		end

	inertia:REAL_32 assign set_inertia
			-- <Precursor>
		do
			Result := Precursor
		end

	set_inertia(a_inertia:REAL_32)
			-- Assign `inertia' with the value of `a_inertia'
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2MassData_I_set(item, a_inertia)
		ensure
			Is_Assign: inertia ~ a_inertia
		end
end
