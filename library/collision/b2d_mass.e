note
	description: "Read only informations about masses."
	author: "Louis Marchand"
	date: "Tue, 09 Jun 2020 13:27:08 +0000"
	revision: "0.1"

class
	B2D_MASS

inherit
	B2D_MEMORY_OBJECT
		redefine
			default_create, is_equal
		end

create {B2D_ANY}
	default_create

feature {NONE} -- Initialisation

	default_create
			-- Initialisation of `Current'
		do
			own_from_item ({B2D_EXTERNAL}.b2MassData_new)
		end

feature -- Access

	to_data:B2D_MASS_DATA
			-- Create a new {B2D_MASS_DATA} from `Current'
		require
			Exists: exists
		do
			create Result
			Result.set_center_with_vector (center)
			Result.set_inertia (inertia)
			Result.set_mass (mass)
		end

	center:B2D_VECTOR_2D
			-- The position of the shape's centroid relative to the origin of the {B2D_SHAPE}.
		require
			Exists: exists
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2MassData_center_get(item))
		end

	mass:REAL_32
			-- The mass of the {B2D_SHAPE}. Usually in kilograms.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2MassData_mass_get(item)
		end

	inertia:REAL_32
			-- The rotational inertia of the {B2D_SHAPE} about the local origin.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2MassData_I_get(item)
		end

	is_equal(a_other:like Current):BOOLEAN
			-- Is `a_other` attached to an object considered
			-- equal to current object?
		do
			if exists and a_other.exists then
				Result := center ~ a_other.center and inertia ~ a_other.inertia and mass ~ a_other.mass
			else
				Result := exists ~ a_other.exists
			end
		end


feature {NONE} -- Implementation

	delete
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2MassData_delete(item)
		end

end
