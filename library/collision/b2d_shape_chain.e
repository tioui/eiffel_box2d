note
	description: "A chain of {B2D_SHAPE_EDGE}. Does not have an area."
	author: "Louis Marchand"
	date: "Thu, 18 Jun 2020 19:25:33 +0000"
	revision: "0.1"

class
	B2D_SHAPE_CHAIN

inherit
	B2D_SHAPE
		rename
			radius as width
		redefine
			width, ray_cast, axis_aligned_bounding_box
		end
	B2D_LIST_HELPER

create
	make,
	make_loop,
	make_with_vectors,
	make_loop_with_vectors

create {B2D_ANY}
	own_from_item,
	shared_from_item

feature {NONE} -- Initialisation

	make(a_vertices:FINITE[TUPLE[x, y:REAL_32]])
			-- Initialisation of `Current' using `a_vertices'
			-- as `vertices'.
		require
			Vertices_Enough_Count: a_vertices.count >= 2
		local
			l_pointer:MANAGED_POINTER
			l_vector:B2D_VECTOR_2D
			l_count, l_sizeof_b2Vec2:INTEGER
		do
			own_from_item ({B2D_EXTERNAL}.b2ChainShape_new)
			if exists then
				l_sizeof_b2Vec2 := {B2D_EXTERNAL}.b2Vec2_sizeof
				create l_pointer.make (a_vertices.count * l_sizeof_b2Vec2)
				l_count := 0
				across a_vertices as la_vertices loop
					create l_vector.shared_from_item (l_pointer.item + (l_count * l_sizeof_b2Vec2), Void)
					l_vector.set_coordinates (la_vertices.item.x, la_vertices.item.y)
					l_count := l_count + 1
				end
				set_vertices_from_pointer_and_count(
					agent (a_item, a_pointer:POINTER; a_count:INTEGER) do {B2D_EXTERNAL}.b2ChainShape_CreateChain(a_item, a_pointer, a_count) end,
					l_pointer.item, a_vertices.count)
			end
		end

	make_loop(a_vertices:FINITE[TUPLE[x, y:REAL_32]])
			-- Initialisation of `Current' using `a_vertices'
			-- as `vertices' as a loop (`a_vertices'.`last' join
			-- with `a_vertices'.`first').
		require
			Vertices_Enough_Count: a_vertices.count >= 2
		local
			l_pointer:MANAGED_POINTER
			l_vector:B2D_VECTOR_2D
			l_count, l_sizeof_b2Vec2:INTEGER
		do
			own_from_item ({B2D_EXTERNAL}.b2ChainShape_new)
			if exists then
				l_sizeof_b2Vec2 := {B2D_EXTERNAL}.b2Vec2_sizeof
				create l_pointer.make (a_vertices.count * l_sizeof_b2Vec2)
				l_count := 0
				across a_vertices as la_vertices loop
					create l_vector.shared_from_item (l_pointer.item + (l_count * l_sizeof_b2Vec2), Void)
					l_vector.set_coordinates (la_vertices.item.x, la_vertices.item.y)
					l_count := l_count + 1
				end
				set_vertices_from_pointer_and_count(
					agent (a_item, a_pointer:POINTER; a_count:INTEGER) do {B2D_EXTERNAL}.b2ChainShape_CreateLoop(a_item, a_pointer, a_count) end,
					l_pointer.item, a_vertices.count)
			end
		end

	make_with_vectors(a_vertices:FINITE[B2D_VECTOR_2D])
			-- Initialisation of `Current' using `a_vertices'
			-- as `vertices'.
		require
			Vertices_Enough_Count: a_vertices.count >= 2
			Vertices_Exists: across a_vertices as la_list all la_list.item.exists end
		local
			l_pointer:MANAGED_POINTER
			l_vector:B2D_VECTOR_2D
			l_count, l_sizeof_b2Vec2:INTEGER
		do
			own_from_item ({B2D_EXTERNAL}.b2ChainShape_new)
			if exists then
				l_sizeof_b2Vec2 := {B2D_EXTERNAL}.b2Vec2_sizeof
				create l_pointer.make (a_vertices.count * l_sizeof_b2Vec2)
				l_count := 0
				across a_vertices as la_vertices loop
					create l_vector.shared_from_item (l_pointer.item + (l_count * l_sizeof_b2Vec2), Void)
					l_vector.set_coordinates (la_vertices.item.x, la_vertices.item.y)
					l_count := l_count + 1
				end
				set_vertices_from_pointer_and_count(
					agent (a_item, a_pointer:POINTER; a_count:INTEGER) do {B2D_EXTERNAL}.b2ChainShape_CreateChain(a_item, a_pointer, a_count) end,
					l_pointer.item, a_vertices.count)
			end
		end

	make_loop_with_vectors(a_vertices:FINITE[B2D_VECTOR_2D])
			-- Initialisation of `Current' using `a_vertices'
			-- as `vertices' as a loop (`a_vertices'.`last' join
			-- with `a_vertices'.`first').
		require
			Vertices_Enough_Count: a_vertices.count >= 2
			Vertices_Exists: across a_vertices as la_list all la_list.item.exists end
		local
			l_pointer:MANAGED_POINTER
			l_vector:B2D_VECTOR_2D
			l_count, l_sizeof_b2Vec2:INTEGER
		do
			own_from_item ({B2D_EXTERNAL}.b2ChainShape_new)
			if exists then
				l_sizeof_b2Vec2 := {B2D_EXTERNAL}.b2Vec2_sizeof
				create l_pointer.make (a_vertices.count * l_sizeof_b2Vec2)
				l_count := 0
				across a_vertices as la_vertices loop
					create l_vector.shared_from_item (l_pointer.item + (l_count * l_sizeof_b2Vec2), Void)
					l_vector.set_coordinates (la_vertices.item.x, la_vertices.item.y)
					l_count := l_count + 1
				end
				set_vertices_from_pointer_and_count(
					agent (a_item, a_pointer:POINTER; a_count:INTEGER) do {B2D_EXTERNAL}.b2ChainShape_CreateLoop(a_item, a_pointer, a_count) end,
					l_pointer.item, a_vertices.count)
			end
		end



	set_vertices_from_pointer_and_count(
							a_method:PROCEDURE[TUPLE[POINTER,POINTER,INTEGER]];
							a_pointer:POINTER; a_count:INTEGER)
			-- Initialisation of the `vertices' of `Current' using an internal C array
			-- of b2Vec2 pointed by `a_pointer' of size `a_count' (internal use only).
		require
			Exists: exists
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				a_method(item, a_pointer, a_count)
			else
				dispose
				put_null
			end
		rescue
			l_retry := True
			retry
		end

feature -- Access

	edge_count:INTEGER
			-- The number of `edge' in `Current'
		require
			Exists: exists
		do
			Result := child_count
		end

	child_count:INTEGER_32
			-- The number of sub element of `Current'
		do
			Result := {B2D_EXTERNAL}.b2ChainShape_GetChildCount(item)
		end

	edge(a_index:INTEGER):B2D_SHAPE_EDGE_READABLE
			-- The sub-{B2D_SHAPE_EDGE} of `Current'. `a_index' start at 1.
		require
			Exists: exists
			Index_Valid: a_index >= 1 and a_index <= edge_count
		do
			create Result.default_create
			if Result.exists then
				{B2D_EXTERNAL}.b2ChainShape_GetChildEdge(item, Result.item, a_index - 1)
			end
		end

	edges:LIST[B2D_SHAPE_EDGE_READABLE]
			-- Every `edge' of `Current'
		require
			Exists: exists
		local
			l_count:INTEGER
		do
			l_count := edge_count
			create {ARRAYED_LIST[B2D_SHAPE_EDGE_READABLE]}Result.make (l_count)
			across 1 |..| l_count as la_index loop
				Result.extend (edge(la_index.item))
			end
		end

	count:INTEGER
			-- The number of `vertices' in `Current'
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2ChainShape_m_count_get(item)
		end

	vertices:LIST[B2D_VECTOR_2D]
			-- The point coodinates or every vertices on `Current'
		require
			Exists: exists
		do
			Result := c_array_to_vector_list({B2D_EXTERNAL}.b2ChainShape_m_vertices_get(item), count)
		end

	has_before_vertex:BOOLEAN
			-- `Current' has a `before_vertex'
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2ChainShape_m_hasPrevVertex_get(item)
		end

	before_vertex:B2D_VECTOR_2D
			-- If there is a vertex before `start_vertex'.
			-- Permit smooth transition
		require
			Exists: exists
			Has_Before_Vertex: has_before_vertex
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2ChainShape_m_prevVertex_get(item))
		end

	has_after_vertex:BOOLEAN
			-- `Current' has a `after_vertex'
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2ChainShape_m_hasNextVertex_get(item)
		end

	after_vertex:B2D_VECTOR_2D
			-- If there is a vertex after `end_vertex'.
			-- Permit smooth transition
		require
			Exists: exists
			Has_After_Vertex: has_after_vertex
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2ChainShape_m_nextVertex_get(item))
		end

	set_before_vertex_with_vector(a_vertex:B2D_VECTOR_2D)
			-- Assign `before_vertex' with the value of `a_vertex'
		require
			Exists: exists
			Vertex_Exists: a_vertex.exists
		do
			set_before_vertex(a_vertex.x, a_vertex.y)
		ensure
			Has_Before_Vertex: has_before_vertex
			Is_Assign: a_vertex ~ before_vertex
		end

	set_before_vertex(a_x, a_y:REAL_32)
			-- Assign `before_vertex' with the value of `a_x' and `a_y'
		require
			Exists: exists
		local
			l_internal_vertex:B2D_VECTOR_2D
		do
			create l_internal_vertex.shared_from_item ({B2D_EXTERNAL}.b2ChainShape_m_prevVertex_get(item), Void)
			check l_internal_vertex.exists end
			l_internal_vertex.set_coordinates (a_x, a_y)
			{B2D_EXTERNAL}.b2ChainShape_m_hasPrevVertex_set(item, True)
		ensure
			Has_Before_Vertex: has_before_vertex
			Is_Assign_X: a_x ~ before_vertex.x
			Is_Assign_Y: a_y ~ before_vertex.y
		end

	clear_before_vertex
			-- Put `has_before_vertex' to `False'
		require
			Exists: exists
		local
			l_internal_vertex:B2D_VECTOR_2D
		do
			create l_internal_vertex.shared_from_item ({B2D_EXTERNAL}.b2ChainShape_m_prevVertex_get(item), Void)
			check l_internal_vertex.exists end
			l_internal_vertex.set_zero
			{B2D_EXTERNAL}.b2ChainShape_m_hasPrevVertex_set(item, False)
		ensure
			Is_Cleared: not has_before_vertex
		end

	set_after_vertex_with_vector(a_vertex:B2D_VECTOR_2D)
			-- Assign `after_vertex' with the value of `a_vertex'
		require
			Exists: exists
			Vertex_Exists: a_vertex.exists
		do
			set_after_vertex(a_vertex.x, a_vertex.y)
		ensure
			Has_Before_Vertex: has_after_vertex
			Is_Assign: a_vertex ~ after_vertex
		end

	set_after_vertex(a_x, a_y:REAL_32)
			-- Assign `after_vertex' with the value of `a_x' and `a_y'
		require
			Exists: exists
		local
			l_internal_vertex:B2D_VECTOR_2D
		do
			create l_internal_vertex.shared_from_item ({B2D_EXTERNAL}.b2ChainShape_m_nextVertex_get(item), Void)
			check l_internal_vertex.exists end
			l_internal_vertex.set_coordinates (a_x, a_y)
			{B2D_EXTERNAL}.b2ChainShape_m_hasNextVertex_set(item, True)
		ensure
			Has_Before_Vertex: has_after_vertex
			Is_Assign_X: a_x ~ after_vertex.x
			Is_Assign_Y: a_y ~ after_vertex.y
		end

	clear_after_vertex
			-- Put `has_after_vertex' to `False'
		require
			Exists: exists
		local
			l_internal_vertex:B2D_VECTOR_2D
		do
			create l_internal_vertex.shared_from_item ({B2D_EXTERNAL}.b2ChainShape_m_nextVertex_get(item), Void)
			check l_internal_vertex.exists end
			l_internal_vertex.set_zero
			{B2D_EXTERNAL}.b2ChainShape_m_hasNextVertex_set(item, False)
		ensure
			Is_Cleared: not has_after_vertex
		end

	ray_cast(
				a_input:B2D_RAY_CAST_INPUT;
				a_transform:B2D_TRANSFORM;
				a_edge_index:INTEGER_32
					):detachable B2D_RAY_CAST_OUTPUT
			-- Get the normal vector and the fraction for a ray-cast of
			-- `a_imput' using `a_transform' for the `edge' with index
			-- `a_edge_index' of `Current'. `a_edge_index' start at 1.
			-- Void if no contact is detected.
		do
			Result := Precursor(a_input, a_transform, a_edge_index)
		end

	axis_aligned_bounding_box(a_transform:B2D_TRANSFORM; a_edge_index:INTEGER_32): B2D_AXIS_ALIGNED_BOUNDING_BOX
			-- The box containing `Current' using `a_transform' and the `edge' with index `a_edge_index'.
			-- `a_edge_index' start at 1.
		do
			Result := Precursor(a_transform, a_edge_index)
		end

	width:REAL_32
			-- The Used for buffer around `edges'
		do
			Result := Precursor
		end

feature {NONE} -- Implementation

	internal_contains(a_transform_item, a_point_item:POINTER):BOOLEAN
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2ChainShape_TestPoint(item, a_transform_item, a_point_item)
		end

	internal_ray_cast(a_result_item, a_input_item, a_transform_item:POINTER; a_edge_index:INTEGER):BOOLEAN
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2ChainShape_RayCast(item, a_result_item, a_input_item, a_transform_item, a_edge_index)
		end

	internal_mass(a_mass_item:POINTER; a_density:REAL_32)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2ChainShape_ComputeMass(item, a_mass_item, a_density)
		end

	internal_axis_aligned_bounding_box(a_mass_item, a_transform_item:POINTER; a_edge_index:INTEGER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2ChainShape_ComputeAABB(item, a_mass_item, a_transform_item, a_edge_index)
		end

	delete
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2ChainShape_delete(item)
		end

end
