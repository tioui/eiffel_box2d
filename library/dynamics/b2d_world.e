note
	description: "Main class of the Box2D engine."
	author: "Louis Marchand"
	date: "Fri, 05 Jun 2020 16:59:55 +0000"
	revision: "0.1"

class
	B2D_WORLD


inherit
	B2D_LIST_HELPER
		redefine
			dispose
		end


create
	make,
	make_from_vector

feature {NONE} -- Initialisation

	make(a_gravity_x, a_gravity_y:REAL_32)
			-- Initialisation of `Current' using `a_gravity_x'
			-- and `a_gravity_y' to generate `gravity'
		do
			has_error := False
			create gravity.make_with_coordinates(a_gravity_x, a_gravity_y)
			if gravity.exists then
				item := {B2D_EXTERNAL}.b2World_new(gravity.item)
			else
				has_error := True
			end
			set_step_time({REAL_32}1.0 / {REAL_32}60.0)
			set_step_velocity(8)
			set_step_position(3)
			create {LINKED_LIST[B2D_BODY]}internal_bodies.make
			create {LINKED_LIST[B2D_JOINT]}internal_joints.make
			if exists then
				inititaize_callbacks
			end
			create begin_contact_actions
			create end_contact_actions
			create pre_solve_contact_actions
			create post_solve_contact_actions
		ensure
			Is_Assign_x: (not has_error and exists) implies gravity.x ~ a_gravity_x
			Is_Assign_y: (not has_error and exists) implies gravity.y ~ a_gravity_y
		end

	make_from_vector(a_gravity:B2D_VECTOR_2D)
			-- Initialisation of `Current' using `a_gravity'
			-- as`gravity'
		require
			Gravity_Exists: a_gravity.exists
		do
			make(a_gravity.x, a_gravity.y)
		ensure
			Is_Assign: gravity ~ a_gravity
		end

	inititaize_callbacks
			-- Initialization of `c_callbacks'
		require
			Exists: exists
		do
			c_callbacks := {B2D_EXTERNAL}.B2dCallbacks_new
			if not c_callbacks.is_default_pointer then
				{B2D_EXTERNAL}.b2World_SetContactListener(item, c_callbacks)
				{B2D_EXTERNAL}.b2World_SetContactFilter(item, c_callbacks)
				{B2D_EXTERNAL}.B2dCallbacks_setRayCastCallback(c_callbacks,$ray_cast_callback)
				{B2D_EXTERNAL}.B2dCallbacks_setBeginContactCallback(c_callbacks,$begin_contact_callback)
				{B2D_EXTERNAL}.B2dCallbacks_setEndContactCallback(c_callbacks,$end_contact_callback)
				{B2D_EXTERNAL}.B2dCallbacks_setPreSolveContactCallback(c_callbacks,$pre_solve_contact_callback)
				{B2D_EXTERNAL}.B2dCallbacks_setPostSolveContactCallback(c_callbacks,$post_solve_contact_callback)
				{B2D_EXTERNAL}.B2dCallbacks_setFilterCollisionCallback(c_callbacks,$filter_collision_callback)
				{B2D_EXTERNAL}.B2dCallbacks_setAxisAlignBoundingBoxCallback(c_callbacks,$axis_align_bounding_box_callback)
			end
			c_wean_chain := {B2D_EXTERNAL}.EiffelObjectChain_new
		end


feature -- Access

	create_body(a_body_definition:B2D_BODY_DEFINITION)
			-- Assign to `last_body' a new {B2D_BODY} defined by `a_body_definition',
			-- if any
		require
			Exists: exists
			Body_Definition_Exists: a_body_definition.exists
			Not_Is_Locked: not is_locked
		local
			l_body: B2D_BODY
		do
			has_error := False
			last_body := Void
			create l_body.make ({B2D_EXTERNAL}.b2World_CreateBody(item, a_body_definition.item), Current)
			if l_body.exists then
				last_body := l_body
				internal_bodies.extend (l_body)
			else
				has_error := True
			end
		ensure
			N_Error_Not_Void: not has_error implies attached last_body
		end

	last_body:detachable B2D_BODY
			-- The last {B2D_BODY} created by `create_body'

	is_body_used(a_body:B2D_BODY):BOOLEAN
			-- `a_body' is used
		do
			Result := across internal_joints as la_joints some
							la_joints.item.body_1 ~ a_body or
							la_joints.item.body_2 ~ a_body
						end
		end

	destroy_body(a_body:B2D_BODY)
			-- Remove `a_body' from `Current'
		require
			Exists: exists
			Body_Exists: a_body.exists
			Not_Is_Locked: not is_locked
			Not_Used: not is_body_used(a_body)
		local
			l_body_item:POINTER
		do
			has_error := False
			if internal_bodies.has (a_body) then
				remove_from_internal_list(internal_bodies, a_body)
				if last_body = a_body then
					if not internal_bodies.is_empty then
						last_body := internal_bodies.last
					else
						last_body := Void
					end
				end
				l_body_item := a_body.item
				a_body.put_null
				{B2D_EXTERNAL}.b2World_DestroyBody(item, l_body_item)
			else
				has_error := True
			end
		ensure
			Bodies_Decreased: not has_error implies internal_bodies.count < old internal_bodies.count
			Last_Body_Valid: not has_error implies last_body /= old a_body
		end

	create_joint(a_joint_definition:B2D_JOINT_DEFINITION)
			-- Assign to `last_joint' a new {B2D_JOINT} defined by `a_joint_definition',
			-- if any
		require
			Exists: exists
			Joint_Definition_Exists: a_joint_definition.exists
			Not_Is_Locked: not is_locked
		do
			has_error := False
			last_joint := Void
			if attached make_joint(a_joint_definition) as la_joint and then la_joint.exists then
				last_joint := la_joint
				internal_joints.extend (la_joint)
			else
				has_error := True
			end
		ensure
			N_Error_Not_Void: not has_error implies attached last_joint
		end

	last_joint:detachable B2D_JOINT
			-- The last {B2D_JOINT} created by `create_joint'

	is_joint_used(a_joint:B2D_JOINT):BOOLEAN
			-- `a_joint' is used
		do
			Result := across internal_joints as la_joints some
							attached {B2D_JOINT_GEAR} la_joints.item as la_joint and then (
								la_joint.joint_1 ~ a_joint or
								la_joint.joint_2 ~ a_joint
							)
						end
		end

	destroy_joint(a_joint:B2D_JOINT)
			-- Remove `a_joint' from `Current'
		require
			Exists: exists
			Joint_Exists: a_joint.exists
			Not_Is_Locked: not is_locked
			Not_Used: not is_joint_used(a_joint)
		local
			l_joint_item:POINTER
		do
			has_error := False
			if internal_joints.has (a_joint) then
				remove_from_internal_list(internal_joints, a_joint)
				if last_joint = a_joint then
					if not internal_joints.is_empty then
						last_joint := internal_joints.last
					else
						last_joint := Void
					end
				end
				l_joint_item := a_joint.item
				a_joint.put_null
				{B2D_EXTERNAL}.b2World_Destroyjoint(item, l_joint_item)
			else
				has_error := True
			end
		ensure
			Joints_Decreased: not has_error implies internal_joints.count < old internal_joints.count
			Last_Joint_Valid: not has_error implies last_joint /= old a_joint
		end

	step_time:REAL_32 assign set_step_time
			-- The amount of time to simulate at each `step' (should not vary
			-- Default: 1/60

	set_step_time(a_step_time:REAL_32)
			-- Assign `step_time' with the value of `a_step_time'
		do
			has_error := False
			step_time := a_step_time
		ensure
			Is_Assign: step_time ~ a_step_time
		end

	step_position:INTEGER_32 assign set_step_position
			-- The amount of position to simulate at each `step'
			-- Default: 3

	set_step_position(a_step_position:INTEGER_32)
			-- Assign `step_position' with the value of `a_step_position'
		do
			has_error := False
			step_position := a_step_position
		ensure
			Is_Assign: step_position ~ a_step_position
		end

	step_velocity:INTEGER_32 assign set_step_velocity
			-- The amount of velocity to simulate at each `step'
			-- Default: 8

	set_step_velocity(a_step_velocity:INTEGER_32)
			-- Assign `step_velocity' with the value of `a_step_velocity'
		do
			has_error := False
			step_velocity := a_step_velocity
		ensure
			Is_Assign: step_velocity ~ a_step_velocity
		end

	step
			-- Take a time step. This performs collision detection, integration, and constraint solution.
			-- Use `step_time', `step_velocity' and `step_position'
		require
			Exists: exists
		local
			l_protected_current:POINTER
		do
			has_error := False
			l_protected_current := {B2D_EXTERNAL}.adopt_eiffel_object(Current)
			{B2D_EXTERNAL}.B2dCallbacks_SetEiffelWorld(c_callbacks, l_protected_current)
			{B2D_EXTERNAL}.B2dCallbacks_SetMustBeginContact(c_callbacks, not begin_contact_actions.is_empty)
			{B2D_EXTERNAL}.B2dCallbacks_SetMustEndContact(c_callbacks, not end_contact_actions.is_empty)
			{B2D_EXTERNAL}.B2dCallbacks_SetMustPreSolve(c_callbacks, not pre_solve_contact_actions.is_empty)
			{B2D_EXTERNAL}.B2dCallbacks_SetMustPostSolve(c_callbacks, not post_solve_contact_actions.is_empty)
			{B2D_EXTERNAL}.B2dCallbacks_SetMustShouldCollide(c_callbacks, attached filter_collision)
			{B2D_EXTERNAL}.b2World_Step(item, step_time, step_velocity, step_position)
			{B2D_EXTERNAL}.B2dCallbacks_ClearEiffelWorld(c_callbacks)
			{B2D_EXTERNAL}.wean_eiffel_object(l_protected_current)
		end

	gravity:B2D_VECTOR_2D
			-- The direction of the gravity in `Current'

	set_gravity(a_x, a_y:REAL_32)
			-- Assign `gravity' with the value of `a_x' and `a_y'
		require
			Exists: exists
		local
			l_gravity:B2D_VECTOR_2D
		do
			has_error := False
			create l_gravity.make_with_coordinates (a_x, a_y)
			if l_gravity.exists then
				{B2D_EXTERNAL}.b2World_SetGravity(item, l_gravity.item)
				gravity := l_gravity
			else
				has_error := True
			end
		ensure
			Is_Assing_X: not has_error implies gravity.x ~ a_x
			Is_Assing_Y: not has_error implies gravity.y ~ a_y
		end

	set_gravity_from_vector(a_vector:B2D_VECTOR_2D)
			-- Assign `gravity' with the value of `a_vector'
		require
			Exists: exists
			Vector_Exists: a_vector.exists
		do
			set_gravity(a_vector.x, a_vector.y)
		ensure
			Is_Assing: not has_error implies gravity ~ a_vector
		end

	is_locked:BOOLEAN
			-- `Current' is in the middle of a `step'
			-- May have side effect on `has_error'.
		require
			Exists: exists
		do
			has_error := False
			Result := {B2D_EXTERNAL}.b2World_IsLocked(item)
		end

	body_count:INTEGER
			-- The number of {B2D_BODY} in `Current'
			-- May have side effect on `has_error'.
		require
			Exists: exists
		do
			has_error := False
			Result := internal_bodies.count
		end

	bodies:LIST[B2D_BODY]
			-- Every {B2D_BODY} in `Current'
			-- May have side effect on `has_error'.
		require
			Exists: exists
		do
			has_error := False
			create {ARRAYED_LIST[B2D_BODY]}Result.make_from_iterable (internal_bodies)
		end

	joint_count:INTEGER
			-- The number of {B2D_JOINT} in `Current'
			-- May have side effect on `has_error'.
		require
			Exists: exists
		do
			has_error := False
			Result := internal_joints.count
		end

	joints:LIST[B2D_JOINT]
			-- Every {B2D_JOINT} in `Current'
			-- May have side effect on `has_error'.
		require
			Exists: exists
		do
			has_error := False
			create {ARRAYED_LIST[B2D_JOINT]}Result.make_from_iterable (internal_joints)
		end

	contact_count:INTEGER
			-- The number of {B2D_CONTACT} in `Current'
			-- May have side effect on `has_error'.
		require
			Exists: exists
		do
			has_error := False
			Result := {B2D_EXTERNAL}.b2World_GetContactCount(item)
		end

	contacts:LIST[B2D_CONTACT]
			-- Every {B2D_CONTACT} in `Current'
			-- Contacts are created and destroyed in the middle
			-- of a time step. Use b2ContactListener to avoid
			-- missing contacts (not yet supported).
			-- May have side effect on `has_error'.
		require
			Exists: exists
		local
			l_c_list:POINTER
			l_fixture_1, l_fixture_2:B2D_FIXTURE
		do
			has_error := False
			create {ARRAYED_LIST[B2D_CONTACT]}Result.make (joint_count)
			from
				l_c_list := {B2D_EXTERNAL}.b2World_GetContactList(item)
			until
				l_c_list.is_default_pointer
			loop
				if attached fixture_with_pointer ({B2D_EXTERNAL}.b2Contact_GetFixtureA(l_c_list)) as la_fixture_1 then
					l_fixture_1 := la_fixture_1
				else
					create l_fixture_1.make (create {B2D_BODY}.make (create {POINTER}, Current), create {POINTER})
				end
				if attached fixture_with_pointer ({B2D_EXTERNAL}.b2Contact_GetFixtureB(l_c_list)) as la_fixture_2 then
					l_fixture_2 := la_fixture_2
				else
					create l_fixture_2.make (create {B2D_BODY}.make (create {POINTER}, Current), create {POINTER})
				end
				Result.extend(create {B2D_CONTACT}.make (l_c_list, l_fixture_1, l_fixture_2, Current))
				l_c_list := {B2D_EXTERNAL}.b2Contact_GetNext(l_c_list)
			end
		end

	is_auto_clear_forces:BOOLEAN assign set_is_auto_clear_forces
			-- `True' means that `Current' will clear all `bodies' forces
			-- at eache call of `step'. `False' means that you have to do
			-- it with manually with `clear_forces'.
			-- May have side effect on `has_error'.
		require
			Exists: exists
		do
			has_error := False
			Result := {B2D_EXTERNAL}.b2World_GetAutoClearForces(item)
		end

	set_is_auto_clear_forces(a_is_auto_clear_forces:BOOLEAN)
			-- Assign `is_auto_clear_forces' with the value of `a_is_auto_clear_forces'
		require
			Exists: exists
		do
			has_error := False
			{B2D_EXTERNAL}.b2World_SetAutoClearForces(item, a_is_auto_clear_forces)
		ensure
			Is_Assign: is_auto_clear_forces ~ a_is_auto_clear_forces
		end

	enable_auto_clear_forces
			-- Assign `is_auto_clear_forces' to `True'
		require
			Exists: exists
		do
			set_is_auto_clear_forces(True)
		ensure
			Is_Enable: is_auto_clear_forces
		end

	disable_auto_clear_forces
			-- Assign `is_auto_clear_forces' to `False'
		require
			Exists: exists
		do
			set_is_auto_clear_forces(False)
		ensure
			Is_Enable: not is_auto_clear_forces
		end


	clear_forces
			-- Manually clear the force buffer on all bodies.
			-- By default, forces are cleared automatically after each call to `step`.
			-- The default behavior is modified by calling `enable_auto_clear_forces' and
			-- `disable_auto_clear_forces'. The purpose of this function is to support sub-stepping.
			-- Sub-stepping is often used to maintain a fixed sized time step under a variable frame-rate.
			-- When you perform sub-stepping you will disable auto clearing of forces and instead call
			-- `clear_forces' after all sub-steps are complete in one pass of your game loop.
		require
			Exists: exists
		do
			has_error := False
			{B2D_EXTERNAL}.b2World_ClearForces(item)
		end

	allow_sleeping:BOOLEAN assign set_allow_sleeping
			-- `bodies' can be used to sleep.
			-- Sleep is used to stop simulating a {B2D_BODY}
			-- when it come at a rest.
			-- May have side effect on `has_error'
		require
			Exists: exists
		do
			has_error := False
			Result := {B2D_EXTERNAL}.b2World_GetAllowSleeping(item)
		end

	set_allow_sleeping(a_allow_sleeping:BOOLEAN)
			-- Assign `allow_sleeping' with the value of `a_allow_sleeping'
		require
			Exists: exists
		do
			has_error := False
			{B2D_EXTERNAL}.b2World_SetAllowSleeping(item, a_allow_sleeping)
		ensure
			Is_Assign: allow_sleeping ~ a_allow_sleeping
		end

	enable_sleeping
			-- Assign `allow_sleeping' to `True'
		require
			Exists: exists
		do
			set_allow_sleeping(True)
		ensure
			Is_Enable: allow_sleeping
		end

	disable_sleeping
			-- Assign `allow_sleeping' to `False'
		require
			Exists: exists
		do
			set_allow_sleeping(False)
		ensure
			Is_Enable: not allow_sleeping
		end

	can_warm_starting:BOOLEAN assign set_can_warm_starting
			-- Warm starting is enable (for testing purpose only)
			-- May have side effect on `has_error'
		require
			Exists: exists
		do
			has_error := False
			Result := {B2D_EXTERNAL}.b2World_GetWarmStarting(item)
		end

	set_can_warm_starting(a_can_warm_starting:BOOLEAN)
			-- Assign `can_warm_starting' with the value of `a_can_warm_starting'
		require
			Exists: exists
		do
			has_error := False
			{B2D_EXTERNAL}.b2World_SetWarmStarting(item, a_can_warm_starting)
		ensure
			Is_Assign: can_warm_starting ~ a_can_warm_starting
		end

	enable_warm_starting
			-- Assign `can_warm_starting' to `True'
		require
			Exists: exists
		do
			set_can_warm_starting(True)
		ensure
			Is_Enable: can_warm_starting
		end

	disable_warm_starting
			-- Assign `can_warm_starting' to `False'
		require
			Exists: exists
		do
			set_can_warm_starting(False)
		ensure
			Is_Enable: not can_warm_starting
		end

	is_continuous_physics:BOOLEAN assign set_is_continuous_physics
			-- Continuous physics is enable (for testing purpose only)
			-- May have side effect on `has_error'
		require
			Exists: exists
		do
			has_error := False
			Result := {B2D_EXTERNAL}.b2World_GetContinuousPhysics(item)
		end

	set_is_continuous_physics(a_is_continuous_physics:BOOLEAN)
			-- Assign `is_continuous_physics' with the value of `a_is_continuous_physics'
		require
			Exists: exists
		do
			has_error := False
			{B2D_EXTERNAL}.b2World_SetContinuousPhysics(item, a_is_continuous_physics)
		ensure
			Is_Assign: is_continuous_physics ~ a_is_continuous_physics
		end

	enable_continuous_physics
			-- Assign `is_continuous_physics' to `True'
		require
			Exists: exists
		do
			set_is_continuous_physics(True)
		ensure
			Is_Enable: is_continuous_physics
		end

	disable_continuous_physics
			-- Assign `is_continuous_physics' to `False'
		require
			Exists: exists
		do
			set_is_continuous_physics(False)
		ensure
			Is_Enable: not is_continuous_physics
		end

	is_sub_stepping:BOOLEAN assign set_is_sub_stepping
			-- Wrm starting is enable (for testing purpose only)
			-- May have side effect on `has_error'
		require
			Exists: exists
		do
			has_error := False
			Result := {B2D_EXTERNAL}.b2World_GetSubStepping(item)
		end

	set_is_sub_stepping(a_is_sub_stepping:BOOLEAN)
			-- Assign `is_sub_stepping' with the value of `a_is_sub_stepping'
		require
			Exists: exists
		do
			has_error := False
			{B2D_EXTERNAL}.b2World_SetSubStepping(item, a_is_sub_stepping)
		ensure
			Is_Assign: is_sub_stepping ~ a_is_sub_stepping
		end

	enable_sub_stepping
			-- Assign `is_sub_stepping' to `True'
		require
			Exists: exists
		do
			set_is_sub_stepping(True)
		ensure
			Is_Enable: is_sub_stepping
		end

	disable_sub_stepping
			-- Assign `is_sub_stepping' to `False'
		require
			Exists: exists
		do
			set_is_sub_stepping(False)
		ensure
			Is_Enable: not is_sub_stepping
		end

	proxy_count:INTEGER_32
			-- Number of broad-phase proxies.
			-- May have side effect on `has_error'
		require
			Exists: exists
		do
			has_error := False
			Result := {B2D_EXTERNAL}.b2World_GetProxyCount(item)
		end

	tree_height:INTEGER_32
			-- The height of the dynamic tree
			-- May have side effect on `has_error'
		require
			Exists: exists
		do
			has_error := False
			Result := {B2D_EXTERNAL}.b2World_GetTreeHeight(item)
		end

	tree_balance:INTEGER_32
			-- The balance of the dynamic tree
			-- May have side effect on `has_error'
		require
			Exists: exists
		do
			has_error := False
			Result := {B2D_EXTERNAL}.b2World_GetTreeBalance(item)
		end

	tree_quality:REAL_32
			-- Quality metric of the dynamic tree. The smaller the better. The minimum is 1.
			-- May have side effect on `has_error'
		require
			Exists: exists
		do
			has_error := False
			Result := {B2D_EXTERNAL}.b2World_GetTreeQuality(item)
		end

	move_origin(a_x, a_y:REAL_32)
			-- Move the origin by `a_x' and `a_y'
			-- The moving formula is new_origin := old_origin - position.
		require
			Exists: exists
		local
			l_vector:B2D_VECTOR_2D
		do
			has_error := False
			create l_vector.make_with_coordinates (a_x, a_y)
			if l_vector.exists then
				move_origin_with_vector(l_vector)
			else
				has_error := True
			end
		end

	move_origin_with_vector(a_move:B2D_VECTOR_2D)
			-- Move the origin by `a_move'
			-- The moving formula is new_origin := old_origin - a_move.
		require
			Exists: exists
			Move_Exists: a_move.exists
		do
			has_error := False
			{B2D_EXTERNAL}.b2World_ShiftOrigin(item, a_move.item)
		end

	profile:B2D_PROFILE
			-- The Time profiler for `step'
			-- May have side effect on `has_error'
		require
			Exists: exists
		do
			create Result.shared_from_item ({B2D_EXTERNAL}.b2World_GetProfile(item), Current)
		end

	begin_contact_actions:ACTION_SEQUENCE[TUPLE[contact:B2D_CONTACT]]
			-- Called when two {B2D_FIXTURE} begin to touch (during `step').
			-- Note that the {B2D_CONTACT} will stop existing after the launch.

	end_contact_actions:ACTION_SEQUENCE[TUPLE[contact:B2D_CONTACT]]
			-- Called when two {B2D_FIXTURE} cease to touch (during `step').
			-- Note that the {B2D_CONTACT} will stop existing after the launch.

	pre_solve_contact_actions:ACTION_SEQUENCE[TUPLE[contact:B2D_CONTACT; old_manifold:B2D_MANIFOLD]]
			-- This is called after a {B2D_CONTACT} is updated. This allows you to inspect a
			-- {B2D_CONTACT} before it goes to the solver. If you are careful, you can modify the
			-- {B2D_MANIFOLD} of the {B2D_CONTACT} (e.g. disable contact). A copy of the old
			-- {B2D_MANIFOLD} is provided so that you can detect changes.
			-- Note: this is called only for awake bodies.
			-- Note: this is called even when the number of contact points is zero.
			-- Note: this is not called for sensors.
			-- Note: if you set the number of contact points to zero, you will not get an
			-- `end_contact_actions' callback. However, you may get a `begin_contact_actions' callback
			-- the next `step'.
			-- Note that the {B2D_CONTACT} and {B2D_MANIFOLD} will stop existing after the launch.

	post_solve_contact_actions:ACTION_SEQUENCE[TUPLE[contact:B2D_CONTACT; impulse:B2D_CONTACT_IMPULSE]]
			-- This lets you inspect a contact after the solver is finished.
			-- This is useful for inspecting impulses.
			-- Note: the contact manifold does not include time of impact impulses,
			-- which can be arbitrarily large if the sub-step is small.
			-- Hence the impulse is provided explicitly in a separate data structure.
			-- Note: this is only called for contacts that are touching, solid, and awake.
			-- Note that the {B2D_CONTACT} and {B2D_CONTACT_IMPULSE} will stop existing after the launch.

	filter_collision:detachable FUNCTION[TUPLE[fixture_1, fixture_2:B2D_FIXTURE], BOOLEAN] assign set_filter_collision
			-- The method that filter if a collision between `fixture_1' and `fixture_2' should collide and
			-- make a {B2D_CONTCT}. It the {FUNCTION} return `True', the collision will happen; `False' if the
			-- should not happen.

	set_filter_collision(a_filter_collision:detachable FUNCTION[TUPLE[fixture_1, fixture_2:B2D_FIXTURE], BOOLEAN])
			-- Assign `filter_collision' with the value of `a_filter_collision'.
		do
			has_error := False
			filter_collision := a_filter_collision
		ensure
			Is_Assign: filter_collision ~ a_filter_collision
		end

	default_filter_collision(a_fixture_1, a_fixture_2:B2D_FIXTURE):BOOLEAN
			-- Applied the default library filtering to `a_fixture_1' and `a_fixture_2'.
			-- Can be usefull if you overwrite the default collision filter with `set_filter_collision'
			-- but want to know if the collision would have happen in the first place.
			-- Note: Does not detect if an actual collision occured but only if `a_fixture_1' and `a_fixture_2'
			-- should collide when one occured.
		require
			Fixture_1_Exists: a_fixture_1.exists
			Fixture_2_Exists: a_fixture_2.exists
		local
			l_filter_1, l_filter_2:B2D_FILTER
		do
			if c_callbacks.is_default_pointer then
				l_filter_1 := a_fixture_1.filter
				l_filter_2 := a_fixture_2.filter
				if l_filter_1.group_index ~ l_filter_2.group_index and l_filter_1.group_index /= 0 then
					Result := l_filter_1.group_index > 0
				else
					Result := (l_filter_1.mask_bits.bit_and (l_filter_2.category_bits)) /= 0 and
								(l_filter_1.category_bits.bit_and (l_filter_2.mask_bits)) /= 0;
				end
			else
				Result := {B2D_EXTERNAL}.B2dCallbacks_DefaultFilter(c_callbacks, a_fixture_1.item, a_fixture_2.item)
			end
		end

	all_overlaps(a_lower_x, a_lower_y, a_upper_x, a_upper_y:REAL_32):LIST[B2D_FIXTURE]
			-- Query every {B2D_FIXTURE} that overlap with `a_lower_x', `a_lower_y', `a_upper_x' and `a_upper_y'.
			-- For performance issue, you can use `overlap_query' to stop the process as soon as you find what you are
			-- looking for.
			-- May have side effect on `has_error'
		require
			Exists: exists
		do
			create {LINKED_LIST[B2D_FIXTURE]}Result.make
			overlap_query(agent get_overlap(Result, ?),a_lower_x, a_lower_y, a_upper_x, a_upper_y)
		end

	all_overlaps_with_axis_align_bounding_box(a_aabb:B2D_AXIS_ALIGNED_BOUNDING_BOX):LIST[B2D_FIXTURE]
			-- Query every {B2D_FIXTURE} that overlap with `a_aabb'.
			-- For performance issue, you can use `overlap_query' to stop the process as soon as you find what you are
			-- looking for.
			-- May have side effect on `has_error'
		require
			Exists: exists
			Aabb_Exists: a_aabb.exists
			Aabb_Valid: a_aabb.is_valid
		do
			create {LINKED_LIST[B2D_FIXTURE]}Result.make
			overlap_query_with_axis_align_bounding_box(agent get_overlap(Result, ?),a_aabb)
		end

	overlap_query(a_action:FUNCTION[TUPLE[fixture:B2D_FIXTURE], BOOLEAN]; a_lower_x, a_lower_y, a_upper_x, a_upper_y:REAL_32)
			-- Query `Current' for {B2D_FIXTURE} that overlap with `a_lower_x', `a_lower_y', `a_upper_x' and `a_upper_y'.
			-- Launch `a_action' for each {B2D_FIXTURE} found. If `a_action' return `False' the process stop immediately.
		require
			Exists: exists
		local
			l_aabb:B2D_AXIS_ALIGNED_BOUNDING_BOX
		do
			create l_aabb.make_with_coordinates (a_lower_x, a_lower_y, a_upper_x, a_upper_y)
			if l_aabb.exists and then l_aabb.is_valid then
				overlap_query_with_axis_align_bounding_box(a_action, l_aabb)
			else
				has_error := True
			end
		end

	overlap_query_with_axis_align_bounding_box(a_action:FUNCTION[TUPLE[fixture:B2D_FIXTURE], BOOLEAN]; a_aabb:B2D_AXIS_ALIGNED_BOUNDING_BOX)
			-- Query `Current' for {B2D_FIXTURE} that overlap with `a_aabb'.
			-- Launch `a_action' for each {B2D_FIXTURE} found. If `a_action' return `False' the process stop immediately.
		require
			Exists: exists
			AABB_Exists: a_aabb.exists
			AABB_Valid: a_aabb.is_valid
		local
			l_protected_current:POINTER
		do
			has_error := False
			l_protected_current := {B2D_EXTERNAL}.adopt_eiffel_object(Current)
			{B2D_EXTERNAL}.B2dCallbacks_SetEiffelWorld(c_callbacks, l_protected_current)
			axis_align_bounding_box_callback_action := a_action
			{B2D_EXTERNAL}.b2World_QueryAABB(item, c_callbacks, a_aabb.item)
			axis_align_bounding_box_callback_action := Void
			{B2D_EXTERNAL}.wean_eiffel_object(l_protected_current)
		end

	all_ray_casts(a_point_1_x, a_point_1_y, a_point_2_x, a_point_2_y:REAL_32):LIST[B2D_WORLD_RAY_CAST_OUTPUT]
			-- Query every {B2D_FIXTURE} in `Current' that intersect with a ray from `a_point_1_x', `a_point_1_y' to
			-- `a_point_2_x', `a_point_2_y'.
			-- May have side effect on `has_error'
		require
			Exists: exists
		do
			create {LINKED_LIST[B2D_WORLD_RAY_CAST_OUTPUT]}Result.make
			ray_cast_query(agent get_ray_casts(Result, ?),a_point_1_x, a_point_1_y, a_point_2_x, a_point_2_y)
		end

	all_ray_casts_with_vectors(a_point_1, a_point_2:B2D_VECTOR_2D):LIST[B2D_WORLD_RAY_CAST_OUTPUT]
			-- Query every {B2D_FIXTURE} in `Current' that intersect with a ray from `a_point_1' to
			-- `a_point_2'.
			-- May have side effect on `has_error'
		require
			Exists: exists
			Point_1_Exists: a_point_1.exists
			Point_2_Exists: a_point_2.exists
		do
			create {LINKED_LIST[B2D_WORLD_RAY_CAST_OUTPUT]}Result.make
			ray_cast_query_with_vectors(agent get_ray_casts(Result, ?),a_point_1, a_point_2)
		end

	ray_cast_query(a_action:FUNCTION[B2D_WORLD_RAY_CAST_OUTPUT, REAL_32];
					a_point_1_x, a_point_1_y, a_point_2_x, a_point_2_y:REAL_32)
			-- Query `Current' for {B2D_FIXTURE} that intersect with a ray from `a_point_1_x', `a_point_1_y' to
			-- `a_point_2_x', `a_point_2_y'.
			-- Launch `a_action' for each {B2D_FIXTURE} found.
			-- If `a_action' return `-1' the current {B2D_FIXTURE} is ignored; if `a_ation' return 0, stop the
			-- following search; if `a_action' return 1, continue the search; if `a_action' return any other
			-- fractions, limit the fraction of the following search.
		require
			Exists: exists
		local
			l_point1, l_point2:B2D_VECTOR_2D
		do
			create l_point1.make_with_coordinates (a_point_1_x, a_point_1_y)
			create l_point2.make_with_coordinates (a_point_2_x, a_point_2_y)
			if l_point1.exists and l_point2.exists then
				ray_cast_query_with_vectors(a_action, l_point1, l_point2)
			else
				has_error := True
			end
		end

	ray_cast_query_with_vectors(a_action:FUNCTION[B2D_WORLD_RAY_CAST_OUTPUT, REAL_32];
					a_point_1, a_point_2:B2D_VECTOR_2D)
			-- Query `Current' for {B2D_FIXTURE} that intersect with a ray from `a_point_1' to `a_point_2'.
			-- Launch `a_action' for each {B2D_FIXTURE} found.
			-- If `a_action' return `-1' the current {B2D_FIXTURE} is ignored; if `a_ation' return 0, stop the
			-- following search; if `a_action' return 1, continue the search; if `a_action' return any other
			-- fractions, limit the fraction of the following search.
		require
			Exists: exists
			Point_1_Exists: a_point_1.exists
			Point_2_Exists: a_point_2.exists
		local
			l_protected_current:POINTER
		do
			has_error := False
			l_protected_current := {B2D_EXTERNAL}.adopt_eiffel_object(Current)
			{B2D_EXTERNAL}.B2dCallbacks_SetEiffelWorld(c_callbacks, l_protected_current)
			ray_cast_callback_actions := a_action
			{B2D_EXTERNAL}.b2World_RayCast(item, c_callbacks, a_point_1.item, a_point_2.item)
			ray_cast_callback_actions := Void
			{B2D_EXTERNAL}.wean_eiffel_object(l_protected_current)
		end

	has_error:BOOLEAN
			-- An error occured on the last feature call

feature {B2D_ANY} -- Implementation

	add_to_wean_chain(a_pointer:POINTER)
			-- Add `a_pointer' to `c_wean_chain'
		do
			if not c_wean_chain.is_default_pointer and not a_pointer.is_default_pointer then
				{B2D_EXTERNAL}.AddToEiffelObjectChain(c_wean_chain, a_pointer)
			end
		end

	remove_from_wean_chain(a_pointer:POINTER)
			-- Remove `a_pointer' from `c_wean_chain'
		do
			if not c_wean_chain.is_default_pointer and not a_pointer.is_default_pointer then
				{B2D_EXTERNAL}.RemoveFromEiffelObjectChain(c_wean_chain, a_pointer)
			end
		end

feature {NONE} -- Implementation

	c_wean_chain:POINTER
			-- Protected Eiffel Object that must be wean when `Current' is collected

	c_callbacks:POINTER
			-- Internal C++ object callback for `begin_contact', `end_contact',
			-- `pre_solve_contact', `post_solve_contact' and `filter_collision_callback'

	begin_contact_callback(a_contact_item:POINTER; a_fixture_1, a_fixture_2:ANY)
			-- Internal callbacks when a {B2D_CONTACT}, generated from
			-- `a_contact_item', is happening. Only launched if `begin_contact_actions'
			-- in not empty.
		local
			l_contact:B2D_CONTACT
		do
			if
				attached {B2D_FIXTURE} a_fixture_1 as la_fixture_1 and
				attached {B2D_FIXTURE} a_fixture_2 as la_fixture_2
			then
				create l_contact.make (a_contact_item, la_fixture_1, la_fixture_2, Current)
				begin_contact_actions.call (l_contact)
				l_contact.put_null
			end
		end

	end_contact_callback(a_contact_item:POINTER; a_fixture_1, a_fixture_2:B2D_FIXTURE)
			-- Internal callbacks when a {B2D_CONTACT}, generated from
			-- `a_contact_item', is ending
		local
			l_contact:B2D_CONTACT
		do
			if
				attached {B2D_FIXTURE} a_fixture_1 as la_fixture_1 and
				attached {B2D_FIXTURE} a_fixture_2 as la_fixture_2
			then
				create l_contact.make (a_contact_item, la_fixture_1, la_fixture_2, Current)
				end_contact_actions.call (l_contact)
				l_contact.put_null
			end
		end

	pre_solve_contact_callback(a_contact_item, a_old_manifold_item:POINTER; a_fixture_1, a_fixture_2:B2D_FIXTURE)
			-- Internal callbacks before a {B2D_CONTACT}, generated from
			-- `a_contact_item', is solved. The old {B2D_MANIFOLD} is
			-- created from `a_old_manifold_item'.
		local
			l_contact:B2D_CONTACT
			l_manifold:B2D_MANIFOLD
		do
			if
				attached {B2D_FIXTURE} a_fixture_1 as la_fixture_1 and
				attached {B2D_FIXTURE} a_fixture_2 as la_fixture_2
			then
				create l_contact.make (a_contact_item, la_fixture_1, la_fixture_2, Current)
				create l_manifold.shared_from_item (a_old_manifold_item, l_contact)
				pre_solve_contact_actions.call (l_contact, l_manifold)
				l_contact.put_null
				l_manifold.put_null
			end
		end

	post_solve_contact_callback(a_contact_item, a_impulse_item:POINTER; a_fixture_1, a_fixture_2:B2D_FIXTURE)
			-- Internal callbacks after a {B2D_CONTACT}, generated from
			-- `a_contact_item', is solved. The impulse is
			-- created from `a_old_manifold_item'.
		local
			l_contact:B2D_CONTACT
			l_impulse:B2D_CONTACT_IMPULSE
		do
			if
				attached {B2D_FIXTURE} a_fixture_1 as la_fixture_1 and
				attached {B2D_FIXTURE} a_fixture_2 as la_fixture_2
			then
				create l_contact.make (a_contact_item, la_fixture_1, la_fixture_2, Current)
				create l_impulse.shared_from_item (a_impulse_item, Void)
				post_solve_contact_actions.call (l_contact, l_impulse)
				l_contact.put_null
				l_impulse.put_null
			end
		end

	filter_collision_callback(a_fixture_1, a_fixture_2:ANY):BOOLEAN
			-- Internal callbacks when `a_fixture_1' and `a_fixture_2' collide.
			-- Return `True' if the Collision must generate a {B2D_CONTACT} and `False' if not.
		do
			Result := True
			if
				attached {B2D_FIXTURE} a_fixture_1 as la_fixture_1 and
				attached {B2D_FIXTURE} a_fixture_2 as la_fixture_2 and
				attached filter_collision as la_filter_collision
			then
				Result := la_filter_collision(la_fixture_1, la_fixture_2)
			end
		end

	axis_align_bounding_box_callback(a_fixture:ANY):BOOLEAN
			-- Internal callback for `axis_align_bounding_box_query` when `a_fixture'
			-- is found
		do
			Result := True
			if
				attached {B2D_FIXTURE} a_fixture as la_fixture and
				attached axis_align_bounding_box_callback_action as la_action
			then
				Result := la_action(la_fixture)
			end
		end

	axis_align_bounding_box_callback_action:detachable FUNCTION[TUPLE[fixture:B2D_FIXTURE], BOOLEAN]
			-- The agent of `axis_align_bounding_box_query`

	get_overlap(a_list:LIST[B2D_FIXTURE]; a_fixture:B2D_FIXTURE):BOOLEAN
			-- Used to get the result of `all_overlaps'
		do
			a_list.extend (a_fixture)
			Result := True
		end

	ray_cast_callback(a_fixture:ANY; a_point, a_normal:POINTER; a_fraction:REAL_32):REAL_32
			-- Internal callback for `ray_cast_query` when `a_fixture'
			-- is found with at point `a_point' a ray angle `a_normal' and a distance `a_fraction'
		local
			l_point, l_normal:B2D_VECTOR_2D
		do
			print("Fraction(L): " + a_fraction.out + "%N")
			Result := 1.0
			if
				attached {B2D_FIXTURE} a_fixture as la_fixture and
				attached ray_cast_callback_actions as la_action
			then
				if a_point.is_default_pointer then
					create l_point.own_from_item (a_point)
				else
					create l_point.make_from_item (a_point)
				end
				if a_normal.is_default_pointer then
					create l_normal.own_from_item (a_normal)
				else
					create l_normal.make_from_item (a_normal)
				end
				Result := la_action(create {B2D_WORLD_RAY_CAST_OUTPUT}.make (la_fixture, l_point, l_normal, a_fraction))
			end
		end

	ray_cast_callback_actions:detachable FUNCTION[B2D_WORLD_RAY_CAST_OUTPUT, REAL_32]
			-- The agent of `ray_cast_query`

	get_ray_casts(a_list:LIST[B2D_WORLD_RAY_CAST_OUTPUT]; a_output:B2D_WORLD_RAY_CAST_OUTPUT):REAL_32
			-- Used to get the result of `all_ray_casts'
		do
			a_list.extend (a_output)
			Result := 1.0
		end

	fixture_with_pointer(a_item:POINTER):detachable B2D_FIXTURE
			-- Retreive the {B2D_FIXTURE} of every `body' with
			-- it's `item' that is `a_item'
		local
			l_cursor:INDEXABLE_ITERATION_CURSOR[B2D_BODY]
		do
			from
				l_cursor := bodies.new_cursor
			until
				l_cursor.after or attached Result
			loop
				Result := l_cursor.item.fixture_with_pointer (a_item)
				l_cursor.forth
			end
		end

	make_joint(a_definition:B2D_JOINT_DEFINITION):detachable B2D_JOINT
			-- Get a {B2D_JOINT} using `a_definition' as definition
		require
			Exists: exists
			Definition_Exists: a_definition.exists
		local
			l_item:POINTER
		do
			l_item := {B2D_EXTERNAL}.b2World_Createjoint(item, a_definition.item)
			if not  l_item.is_default_pointer then
				if attached {B2D_JOINT_DISTANCE_DEFINITION} a_definition then
					create {B2D_JOINT_DISTANCE}Result.make_from_item(l_item, a_definition.body_1, a_definition.body_2)
				elseif attached {B2D_JOINT_FRICTION_DEFINITION} a_definition then
					create {B2D_JOINT_FRICTION}Result.make_from_item(l_item, a_definition.body_1, a_definition.body_2)
				elseif attached {B2D_JOINT_GEAR_DEFINITION} a_definition as la_definition then
					create {B2D_JOINT_GEAR}Result.make(l_item, la_definition.joint_1, la_definition.joint_2)
				elseif attached {B2D_JOINT_MOTOR_DEFINITION} a_definition then
					create {B2D_JOINT_MOTOR}Result.make_from_item(l_item, a_definition.body_1, a_definition.body_2)
				elseif attached {B2D_JOINT_MOUSE_DEFINITION} a_definition then
					create {B2D_JOINT_MOUSE}Result.make_from_item(l_item, a_definition.body_1, a_definition.body_2)
				elseif attached {B2D_JOINT_PRISMATIC_DEFINITION} a_definition then
					create {B2D_JOINT_PRISMATIC}Result.make_from_item(l_item, a_definition.body_1, a_definition.body_2)
				elseif attached {B2D_JOINT_PULLEY_DEFINITION} a_definition then
					create {B2D_JOINT_PULLEY}Result.make_from_item(l_item, a_definition.body_1, a_definition.body_2)
				elseif attached {B2D_JOINT_REVOLUTE_DEFINITION} a_definition then
					create {B2D_JOINT_REVOLUTE}Result.make_from_item(l_item, a_definition.body_1, a_definition.body_2)
				elseif attached {B2D_JOINT_ROPE_DEFINITION} a_definition then
					create {B2D_JOINT_ROPE}Result.make_from_item(l_item, a_definition.body_1, a_definition.body_2)
				elseif attached {B2D_JOINT_WELD_DEFINITION} a_definition then
					create {B2D_JOINT_WELD}Result.make_from_item(l_item, a_definition.body_1, a_definition.body_2)
				elseif attached {B2D_JOINT_WHEEL_DEFINITION} a_definition then
					create {B2D_JOINT_WHEEL}Result.make_from_item(l_item, a_definition.body_1, a_definition.body_2)
				end
			end
		end


	internal_bodies:LIST[B2D_BODY]
			-- Internal value of `bodies'

	internal_joints:LIST[B2D_JOINT]
			-- Internal value of `joints'

	gravity_valid:BOOLEAN
			-- `gravity' has the same values as the internal
			-- gravity of `item'.
		require
			Exists: exists
		local
			l_values:B2D_VECTOR_2D
		do
			Result := False
			create l_values
			if l_values.exists then
				{B2D_EXTERNAL}.b2World_GetGravity(item, l_values.item)
				Result := l_values ~ gravity
			end
		end

	delete
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2World_delete(item)
		end

	dispose
			-- <Precursor>
		do
			if not c_wean_chain.is_default_pointer then
				{B2D_EXTERNAL}.WeanAllEiffelObjectChain(c_wean_chain)
				{B2D_EXTERNAL}.EiffelObjectChain_delete(c_wean_chain)
			end
			if not c_callbacks.is_default_pointer then
				{B2D_EXTERNAL}.B2dCallbacks_delete(c_callbacks)
			end
			Precursor
		end

invariant
	Bodies_Count_Valid: exists implies internal_bodies.count ~ {B2D_EXTERNAL}.b2World_GetBodyCount(item)
	Joints_Count_Valid: exists implies internal_joints.count ~ {B2D_EXTERNAL}.b2World_GetJointCount(item)
	Bodiess_Valid: exists implies has_same_item(
									internal_bodies,
									agent (a_item:POINTER):POINTER do Result := {B2D_EXTERNAL}.b2World_GetBodyList(a_item) end,
									agent (a_item:POINTER):POINTER do Result := {B2D_EXTERNAL}.b2Body_GetNext(a_item) end
								)
	Joints_Valid: exists implies has_same_item(
									internal_joints,
									agent (a_item:POINTER):POINTER do Result := {B2D_EXTERNAL}.b2World_GetJointList(a_item) end,
									agent (a_item:POINTER):POINTER do Result := {B2D_EXTERNAL}.b2Joint_GetNext(a_item) end
								)

	Gravity_Valid: exists implies gravity_valid
end
