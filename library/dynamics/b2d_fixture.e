note
	description: "Used to attach a {B2D_SHAPE} to a {B2D_BODY} for collision detection."
	author: "Louis Marchand"
	date: "Mon, 22 Jun 2020 22:06:44 +0000"
	revision: "0.1"

class
	B2D_FIXTURE

inherit
	B2D_MEMORY_OBJECT

create {B2D_BODY, B2D_WORLD}
	make

feature {NONE} -- Initialisation

	make(a_body:B2D_BODY; a_item:POINTER)
			-- Initialisation of `Current' using `a_body' as `body',
			-- `a_shape' as `shape' and `a_item' as `item'
		require
			Body_Exists: a_body.exists
		do
			shared_from_item(a_item, a_body)
			body := a_body
			if attached {B2D_SHAPE_FACTORY}.shared_shape ({B2D_EXTERNAL}.b2Fixture_GetShape(item), a_body) as la_shape then
				shape := la_shape
			else
				create {B2D_SHAPE_CIRCLE}shape.own_from_item(create {POINTER})
				put_null
			end
		ensure
			Body_Assign: body ~ a_body
		end

feature -- Access

	body:B2D_BODY
			-- The {B2D_BODY} that `Current' is assign to.

	shape:B2D_SHAPE
			-- The {B2D_SHAPE} that `Current' is assign to.

	is_sensor:BOOLEAN assign set_is_sensor
			-- `Current' has a sensor
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2Fixture_IsSensor(item)
		end

	set_is_sensor(a_is_sensor:BOOLEAN)
			-- Assign `is_sensor' with the value of `a_is_sensor'
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2Fixture_SetSensor(item, a_is_sensor)
		ensure
			Is_Assign: is_sensor ~ a_is_sensor
		end

	enable_sensor
			-- Assign `is_sensor' to `True'
		require
			Exists: exists
		do
			set_is_sensor(True)
		ensure
			Is_Set: is_sensor
		end

	disable_sensor
			-- Assign `is_sensor' to `False'
		require
			Exists: exists
		do
			set_is_sensor(False)
		ensure
			Is_Set: not is_sensor
		end

	filter:B2D_FILTER assign set_filter
			-- Copy of the informations about the fitering in `Current'
		require
			Exists: exists
		local
			l_filter:B2D_FILTER
		do
			create l_filter.shared_from_item ({B2D_EXTERNAL}.b2Fixture_GetFilterData(item), Current)
			if l_filter.exists then
				create Result
				Result.set_category_bits (l_filter.category_bits)
				Result.set_group_index (l_filter.group_index)
				Result.set_mask_bits (l_filter.mask_bits)
			else
				create Result.own_from_item (create {POINTER})
			end
		end

	set_filter(a_filter:B2D_FILTER)
			-- Assign `filter' with the value of `a_fiter'.
			-- Note that it autamatically launch `refilter'.
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2Fixture_SetFilterData(item, a_filter.item)
		ensure
			Is_Assign: filter ~ a_filter
		end

	refilter
			-- Calculate collision from scratch.
			-- Usefull when  you want to establish collision that was previously disabled.
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2Fixture_Refilter(item)
		end

	contains(a_x, a_y:REAL_32):BOOLEAN
			-- The point at (`a_x', `a_y') is in `Current'
		require
			Exists: exists
		local
			l_vector:B2D_VECTOR_2D
		do
			create l_vector.make_with_coordinates (a_x, a_y)
			check l_vector.exists then
				Result := contains_with_vector(l_vector)
			end
		end

	contains_with_vector(a_point:B2D_VECTOR_2D):BOOLEAN
			-- The point `a_point' is in `Current'
		require
			Exists: exists
			Point_Exists: a_point.exists
		do
			Result := shape.contains_with_vector (body.transform, a_point)
		end

	ray_cast(a_input:B2D_RAY_CAST_INPUT; a_child_index:INTEGER):detachable B2D_RAY_CAST_OUTPUT
			-- Get the normal vector and the fraction for a ray-cast of
			-- `a_imput' for the child `a_child_index'
			-- of `Current'. `a_child_index' start at 1.
			-- Void if no contact is detected.
		require
			Exists: exists
			Input_Exists: a_input.exists
			Child_Index_Valid: a_child_index >= 1 and a_child_index <= shape.child_count
		do
			Result := shape.ray_cast (a_input, body.transform, a_child_index)
		end

	axis_aligned_bounding_box(a_child_index:INTEGER_32): B2D_AXIS_ALIGNED_BOUNDING_BOX
			-- The box containing `Current' using `a_transform'. `a_child_index' start at 1.
		require
			Exists: exists
			Child_Index_Valid: a_child_index >= 1 and a_child_index <= shape.child_count
		do
			Result := shape.axis_aligned_bounding_box (body.transform, a_child_index)
		end

	mass(a_density:REAL_32):B2D_MASS
			-- The mass of `Current' with a density of `a_density'
		require
			Exists: exists
		do
			Result := shape.mass(a_density)
		end

	density:REAL_32 assign set_density
			-- The density of `Current'
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2Fixture_GetDensity(item)
		end

	set_density(a_density:REAL_32)
			-- Assign `density' with the value of `a_density'.
			-- `mass' of the `body' does not automatically recalculate.
			-- You must manually use `body'.`reset_mass_data'
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2Fixture_SetDensity(item, a_density)
		ensure
			Is_Assign: density ~ a_density
		end

	friction:REAL_32 assign set_friction
			-- The coefficient of friction of `Current'
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2Fixture_GetFriction(item)
		end

	set_friction(a_friction:REAL_32)
			-- Assign `friction' with the value of `a_friction'.
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2Fixture_SetFriction(item, a_friction)
		ensure
			Is_Assign: friction ~ a_friction
		end

	restitution:REAL_32 assign set_restitution
			-- The coefficient of restitution of `Current'.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2Fixture_GetRestitution(item)
		end

	set_restitution(a_restitution:REAL_32)
			-- Assign `restitution' with the value of `a_restitution'.
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2Fixture_SetRestitution(item, a_restitution)
		ensure
			Is_Assign: restitution ~ a_restitution
		end

	attached_data:detachable ANY assign set_attached_data
			-- Usefull to stock anything that the client may want to attach to `Current'

	set_attached_data(a_data:detachable ANY)
			-- Assign `attached_data' with the value of `a_data'
		do
			attached_data := a_data
		ensure
			Is_Assign: attached_data ~ a_data
		end

feature {NONE} -- Implementation

	delete
			-- <Precursor>
		do
			check Should_Not_Be_Deleted: False end
		end

invariant
	Is_Body_Valid: exists implies {B2D_EXTERNAL}.b2Fixture_GetBody(item) ~ body.item
	Is_Shape_Valid: exists implies {B2D_EXTERNAL}.b2Fixture_GetShape(item) ~ shape.item
end
