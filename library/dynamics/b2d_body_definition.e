note
	description: "Definition used to create {B2D_BODY}."
	author: "Patrick Boucher"
	date: "Sun, 07 Jun 2020 20:39:00 +0000"
	revision: "0.1"

class
	B2D_BODY_DEFINITION

inherit
	B2D_MEMORY_OBJECT
		redefine
			default_create
		end

create
	default_create,
	make_static,
	make_kinematic,
	make_dynamic

feature {NONE} -- Initialization

	default_create
			-- Initialization of `Current'.
		do
			make_dynamic
		end

	make_static
			-- Initalization of `Current' with a static body type.
		local
			l_body_type: NATURAL
		do
			l_body_type := {B2D_EXTERNAL}.b2body_type_staticbody
			make_with_body_type (l_body_type)
		end

	make_kinematic
			-- Initalization of `Current' with a kinematic body type.
		local
			l_body_type: NATURAL
		do
			l_body_type := {B2D_EXTERNAL}.b2body_type_kinematicbody
			make_with_body_type (l_body_type)
		end

	make_dynamic
			-- Initalization of `Current' with a dynamic body type.
		local
			l_body_type: NATURAL
		do
			l_body_type := {B2D_EXTERNAL}.b2body_type_dynamicbody
			make_with_body_type (l_body_type)
		end

	make_with_body_type (a_body_type: NATURAL)
			-- Initalization of `Current'.
			--
			-- `a_body_type`: The body type of `Current' obtained with
			-- {B2D_EXTERNAL}.b2body_type_*
		do
			own_from_item ({B2D_EXTERNAL}.b2BodyDef_new)
			if exists then
				{B2D_EXTERNAL}.b2bodydef_type_set (item, a_body_type)
			end
		end

feature -- Access

	is_static: BOOLEAN
			-- The body is static.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2bodydef_type_get (item) ~ {B2D_EXTERNAL}.b2body_type_staticbody
		end

	is_kinematic: BOOLEAN
			-- The body is kinematic.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2bodydef_type_get (item) ~ {B2D_EXTERNAL}.b2body_type_kinematicbody
		end

	is_dynamic: BOOLEAN
			-- The body is dynamic.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2bodydef_type_get (item) ~ {B2D_EXTERNAL}.b2body_type_dynamicbody
		end

	position: B2D_VECTOR_2D assign set_position_with_vector
			-- The world position of the body.
		require
			Exists: exists
		do
			create Result.shared_from_item ({B2D_EXTERNAL}.b2bodydef_position_get (item), Current)
		end

	set_position_with_vector (a_position: B2D_VECTOR_2D)
			-- Assign `position' with the value `a_position'.
		require
			Exists: exists
			Position_Exists: a_position.exists
			Position_Exists: position.exists
		do
			position.set_coordinates (a_position.x, a_position.y)
		ensure
			Is_Assign: position ~ a_position
		end

	set_position(a_position_x, a_position_y: REAL_32)
			-- Assign `position' with the value `a_position_x' and `a_position_y'.
		require
			Exists: exists
			Position_Exists: position.exists
		do
			position.set_coordinates (a_position_x, a_position_y)
		ensure
			Is_Assign_X: position.x ~ a_position_x
			Is_Assign_Y: position.y ~ a_position_y
		end

	angle: REAL_32 assign set_angle
			-- The world angle of the body in radians.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2bodydef_angle_get (item)
		end

	set_angle (a_angle: REAL_32)
			-- Assign `angle' with the value `a_angle'.
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2bodydef_angle_set (item, a_angle)
		ensure
			Is_Assign: angle = a_angle
		end

	linear_velocity: B2D_VECTOR_2D assign set_linear_velocity_with_vector
			-- The linear velocity of the body's origin in world co-ordinates.
		require
			Exists: exists
		do
			create Result.shared_from_item ({B2D_EXTERNAL}.b2bodydef_linearvelocity_get (item), Current)
		end

	set_linear_velocity_with_vector (a_velocity: B2D_VECTOR_2D)
			-- Assign `linear_velocity' with the value `a_velocity'.
		require
			Exists: exists
			Velocity_Exists: a_velocity.exists
			Linear_Velocity_Exists: linear_velocity.exists
			Not_Static: not is_static
		do
			linear_velocity.set_coordinates (a_velocity.x, a_velocity.y)
		ensure
			Is_Assign: linear_velocity ~ a_velocity
		end

	set_linear_velocity (a_velocity_x, a_velocity_y: REAL_32)
			-- Assign `linear_velocity' with the values `a_velocity_x' and `a_velocity_y'.
		require
			Exists: exists
			Not_Static: not is_static
		do
			linear_velocity.set_coordinates (a_velocity_x, a_velocity_y)
		ensure
			Is_Assign_X: linear_velocity.x ~ a_velocity_x
			Is_Assign_Y: linear_velocity.y ~ a_velocity_y
		end

	angular_velocity: REAL_32 assign set_angular_velocity
			-- The angular velocity of the body.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2bodydef_angularvelocity_get (item)
		end

	set_angular_velocity (a_angular_velociy: REAL_32)
			-- Assign `angular_velocity' with the value `a_angular_velocity'.
		require
			Exists: exists
			Not_Static: not is_static
		do
			{B2D_EXTERNAL}.b2bodydef_angularvelocity_set (item, a_angular_velociy)
		ensure
			Is_Assign: angular_velocity = a_angular_velociy
		end

	linear_damping: REAL_32 assign set_linear_damping
			-- The linear damping of the body.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2bodydef_lineardamping_get (item)
		end

	set_linear_damping (a_linear_damping: REAL_32)
			-- Assign `linear_damping' with the value `a_linear_damping'.
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2bodydef_lineardamping_set (item, a_linear_damping)
		ensure
			Is_Assign: linear_damping = a_linear_damping
		end

	angular_damping: REAL_32 assign set_angular_damping
			-- The angular damping of the body.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2bodydef_angulardamping_get (item)
		end

	set_angular_damping (a_angular_damping: REAL_32)
			-- Assign `angular_damping' with the value `a_angular_damping'.
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2bodydef_angulardamping_set (item, a_angular_damping)
		ensure
			Is_Assign: angular_damping = a_angular_damping
		end

	allow_sleep: BOOLEAN assign set_allow_sleep
			-- The body can fall asleep. It reduce CPU usage.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2bodydef_allowsleep_get (item)
		end

	set_allow_sleep (a_allow_sleep: BOOLEAN)
			-- Assign `allow_sleep' with the value `a_allow_sleep'.
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2bodydef_allowsleep_set (item, a_allow_sleep)
		ensure
			Is_Assign: allow_sleep ~ a_allow_sleep
		end

	enable_allow_sleep
			-- Set `allow_sleep' to `True'
		require
			Exists: exists
		do
			set_allow_sleep(True)
		ensure
			Is_Set: allow_sleep
		end

	disable_allow_sleep
			-- Set `allow_sleep' to `False'
		require
			Exists: exists
		do
			set_allow_sleep(False)
		ensure
			Is_Set: not allow_sleep
		end

	is_awake: BOOLEAN assign set_is_awake
			-- Is this body initially awake or sleeping?
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2bodydef_awake_get (item)
		end

	set_is_awake (a_awake: BOOLEAN)
			-- Assign `awake' with the value `a_awake'.
			-- A sleeping body has very low CPU cost.
		require
			Exists: exists
			Sleeping_Allowed: not a_awake implies allow_sleep
		do
			{B2D_EXTERNAL}.b2bodydef_awake_set (item, a_awake)
		ensure
			Is_Assign: is_awake = a_awake
		end

	wake
			-- Set `is_awake' to `True'
		require
			Exists: exists
		do
			set_is_awake(True)
		ensure
			Is_Set: is_awake
		end

	sleep
			-- Set `is_awake' to `False'
		require
			Exists: exists
			Sleeping_Allowed: allow_sleep
		do
			set_is_awake(False)
		ensure
			Is_Set: not is_awake
		end

	is_fixed_rotation: BOOLEAN assign set_is_fixed_rotation
			-- Should this body be prevented from rotating?
			-- Useful for characters.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2bodydef_fixedrotation_get (item)
		end

	set_is_fixed_rotation (a_is_fixed_rotation: BOOLEAN)
			-- Assign `fixed_rotation' with the value `a_fixed_rotation'.
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2bodydef_fixedrotation_set (item, a_is_fixed_rotation)
		ensure
			Is_Assign: is_fixed_rotation = a_is_fixed_rotation
		end

	enable_fixed_rotation
			-- Set `is_fixed_rotation' to `True'
		require
			Exists: exists
		do
			set_is_fixed_rotation(True)
		ensure
			Is_Set: is_fixed_rotation
		end

	disable_fixed_rotation
			-- Set `is_fixed_rotation' to `False'
		require
			Exists: exists
		do
			set_is_fixed_rotation(False)
		ensure
			Is_Set: not is_fixed_rotation
		end

	is_bullet: BOOLEAN assign set_is_bullet
			-- Is this a fast moving body that should be prevented from tunneling
			-- through other moving bodies?
			-- This setting is only considered on dynamic bodies.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2bodydef_bullet_get (item)
		end

	set_is_bullet (a_bullet: BOOLEAN)
			-- Assign `bullet' with the value `a_bullet'.
		require
			Exists: exists
			Dynamic_only: is_dynamic
		do
			{B2D_EXTERNAL}.b2bodydef_bullet_set (item, a_bullet)
		ensure
			Is_Assign: is_bullet = a_bullet
		end

	enable_bullet
			-- Set `is_bullet' to `True'
		require
			Exists: exists
		do
			set_is_bullet(True)
		ensure
			Is_Set: is_bullet
		end

	disable_bullet
			-- Set `is_bullet' to `False'
		require
			Exists: exists
		do
			set_is_bullet(False)
		ensure
			Is_Set: not is_bullet
		end

	is_active: BOOLEAN assign set_is_active
			-- Does this body start out active?
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2bodydef_active_get (item)
		end

	set_is_active (a_active: BOOLEAN)
			-- Assign `active' with the value `a_active'.
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2bodydef_active_set (item, a_active)
		ensure
			Is_Assign: is_active = a_active
		end

	activate
			-- Set `is_active' to `True'
		require
			Exists: exists
		do
			set_is_active(True)
		ensure
			Is_Set: is_active
		end

	deactivate
			-- Set `is_active' to `False'
		require
			Exists: exists
		do
			set_is_active(False)
		ensure
			Is_Set: not is_active
		end

	gravity_scale: REAL_32 assign set_gravity_scale
			-- Scale the gravity applied to this body.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2bodydef_gravityscale_get (item)
		end

	set_gravity_scale (a_gravity_scale: REAL_32)
			-- Assign `gravity_scale' with the value `a_gravity_scale'.
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2bodydef_gravityscale_set (item, a_gravity_scale)
		ensure
			Is_Assign: gravity_scale = a_gravity_scale
		end

feature {NONE} -- Implementation

	delete
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2BodyDef_delete (item)
		end

invariant
	Bullet_Valid: exists and is_bullet implies is_dynamic

end
