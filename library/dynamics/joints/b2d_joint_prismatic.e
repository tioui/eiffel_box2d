note
	description: "[
					Provides one degree of freedom: translation along an axis fixed in `body_1'. 
					Relative rotation is prevented. You can use a joint limit to restrict the
					range of motion and a {B2_JOINT_MOTOR} to drive the motion or to model 
					{B2D_JOINT_FRICTION}.
				]"
	author: "Louis Marchand"
	date: "Mon, 21 Feb 2022 15:17:30 +0000"
	revision: "0.2"

class
	B2D_JOINT_PRISMATIC

inherit
	B2D_JOINT_GEARABLE

create {B2D_ANY}
	make_from_item

feature -- Access

	local_anchor_1:B2D_VECTOR_2D
			-- The local anchor point relative to the origin of `body_1'
		do
			create Result
			if Result.exists then
				{B2D_EXTERNAL}.b2PrismaticJoint_GetLocalAnchorA(item, Result.item)
			end
		end

	local_anchor_2:B2D_VECTOR_2D
			-- The local anchor point relative to the origin of `body_2'
		do
			create Result
			if Result.exists then
				{B2D_EXTERNAL}.b2PrismaticJoint_GetLocalAnchorB(item, Result.item)
			end
		end

	local_axis_1:B2D_VECTOR_2D
			-- The local axis relative to `body_1'
		require
			Exists:exists
		do
			create Result
			if Result.exists then
				{B2D_EXTERNAL}.b2PrismaticJoint_GetLocalAxisA(item, Result.item)
			end
		end

	reference_angle:REAL_32
			-- The angle of the reference of `Current'
		do
			Result := {B2D_EXTERNAL}.b2PrismaticJoint_GetReferenceAngle(item)
		end

	translation:REAL_32
			-- The translation of `Current' (usually in meter)
		require
			Exists:exists
		do
			Result := {B2D_EXTERNAL}.b2PrismaticJoint_GetJointTranslation(item)
		end

	speed:REAL_32
			-- The speed of `Current'
		do
			Result := {B2D_EXTERNAL}.b2PrismaticJoint_GetJointSpeed(item)
		end

	is_limit_enabled: BOOLEAN assign set_is_limit_enabled
			-- `True' if `Current' must apply angle limits
		do
			Result := {B2D_EXTERNAL}.b2PrismaticJoint_IsLimitEnabled (item)
		end

	set_is_limit_enabled (a_value:BOOLEAN)
			-- Assign `is_limit_enabled' with the value `a_value'.
		do
			{B2D_EXTERNAL}.b2PrismaticJoint_EnableLimit (item, a_value)
		end

	lower_limit:REAL_32 assign set_lower_limit
			-- The lower angle limit in radian allowed in `Current'
		do
			Result := {B2D_EXTERNAL}.b2PrismaticJoint_GetLowerLimit(item)
		end

	upper_limit:REAL_32 assign set_upper_limit
			-- The upper angle limit in radian allowed in `Current'
		do
			Result := {B2D_EXTERNAL}.b2PrismaticJoint_GetUpperLimit(item)
		end

	set_limits(a_lower, a_upper:REAL_32)
			-- Assign `limits' with the values of `a_lower' and `a_upper' (in radian)
			-- Note that those limit are only apply when `is_limit_enabled' is `True'
		do
			{B2D_EXTERNAL}.b2PrismaticJoint_SetLimits(item, a_lower, a_upper)
		end

	is_motor_enabled: BOOLEAN assign set_is_motor_enabled
			-- `True' if `Current' must apply motor mecanics
		do
			Result := {B2D_EXTERNAL}.b2PrismaticJoint_IsMotorEnabled (item)
		end

	set_is_motor_enabled (a_value:BOOLEAN)
			-- Assign `is_motor_enabled' with the value `a_value'.
		do
			{B2D_EXTERNAL}.b2PrismaticJoint_EnableMotor (item, a_value)
		end

	motor_speed:REAL_32 assign set_motor_speed
			-- The speed of the motor of `Current' (in radian per second).
			-- `is_motor_enabled' must be `True' to be applied.
		do
			Result := {B2D_EXTERNAL}.b2PrismaticJoint_GetMotorSpeed(item)
		end

	set_motor_speed(a_speed:REAL_32)
			-- Assign `motor_speed' with the value of `Current'
		do
			{B2D_EXTERNAL}.b2PrismaticJoint_SetMotorSpeed(item, a_speed)
		end

	maximum_motor_force:REAL_32 assign set_maximum_motor_force
			-- The maximum level of force allow in the motor of `Current' (usually in N).
			-- `is_motor_enabled' must be `True' to be applied in `Current'.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2PrismaticJoint_GetMaxMotorForce(item)
		end

	set_maximum_motor_force(a_force:REAL_32)
			-- Assign `maximum_motor_force' with the value of `a_force' (usually in N)
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2PrismaticJoint_SetMaxMotorForce(item, a_force)
		ensure
			Is_Set: maximum_motor_force ~ a_force
		end

	motor_force(a_inverse_time_step:REAL_32):REAL_32
			-- The motor force given `a_inverse_time_step' in N
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2PrismaticJoint_GetMotorForce(item, a_inverse_time_step)
		end

feature {NONE} -- Implementation

	anchor_1_internal(a_vector:POINTER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2PrismaticJoint_GetAnchorA(item, a_vector)
		end

	anchor_2_internal(a_vector:POINTER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2PrismaticJoint_GetAnchorB(item, a_vector)
		end

	reaction_force_internal(a_inverse_time_step:REAL_32; a_vector:POINTER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2PrismaticJoint_GetReactionForce(item, a_vector, a_inverse_time_step)
		end

	reaction_torque_internal(a_inverse_time_step:REAL_32):REAL_32
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2PrismaticJoint_GetReactionTorque(item, a_inverse_time_step)
		end

	type_internal:NATURAL
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2Joint_type_prismaticJoint
		end

end
