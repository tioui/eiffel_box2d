note
	description: "[
			Common ancestor for {B2D_JOINT_DEFINITION} that can be used to create
			a {B2D_JOINT_GEARABLE}.
		]"
	author: "Patrick Boucher and Louis Marchand"
	date: "Fri, 18 Feb 2022 16:21:15 +0000"
	revision: "0.2"

deferred class
	B2D_JOINT_GEARABLE_DEFINITION

inherit
	B2D_JOINT_LIMITED
	B2D_JOINT_DEFINITION_UNIQUE_ANCHOR
	B2D_JOINT_MOTORED
	B2D_JOINT_DEFINITION

feature -- Access

	set_limits (a_lower, a_upper:REAL_32)
			-- Assign `lower_limit' and `upper_limit' with the values `a_lower' and `a_upper'.
		do
			set_upper_limit ({REAL_32}.max_value)
			set_lower_limit (a_lower)
			set_upper_limit (a_upper)
		end
end
