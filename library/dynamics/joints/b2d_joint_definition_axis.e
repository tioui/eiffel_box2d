note
	description: "Common ancestor for {B2D_JOINT_DEFINITION} that possess an axis."
	author: "Mon, 27 Jul 2020 01:15:39 +0000"
	date: "Patrick Boucher"
	revision: "0.1"

deferred class
	B2D_JOINT_DEFINITION_AXIS

inherit
	B2D_JOINT_DEFINITION

feature -- Access

	local_axis:B2D_VECTOR_2D
			-- The local translation unit axis in `body_1'.
			-- This axis is relative to angle of `body_1'.
		require
			Exists: exists
		deferred
		end

	set_local_axis_with_vector (a_world_axis:B2D_VECTOR_2D)
			-- Change the value of `local_axis' with the world axis values `a_world_axis'.
		require
			Exists: exists
			Vector_Exists: a_world_axis.exists
			Axis_Directional: not (a_world_axis.x ~ 0.0 and a_world_axis.y ~ 0.0)
		deferred
		ensure
			Is_Assigned: local_axis ~ body_1.relative_local_point_with_vector (a_world_axis)
		end

	set_local_axis (a_world_axis_x, a_world_axis_y:REAL_32)
			-- Change the value of `local_axis' with the world axis
			-- values of `a_world_axis_x' and `a_world_axis_y'.
		require
			Exists: exists
			Axis_Directional: not (a_world_axis_x ~ 0.0 and a_world_axis_y ~ 0.0)
		local
			l_axis:B2D_VECTOR_2D
		do
			create l_axis.make_with_coordinates (a_world_axis_x, a_world_axis_y)
			check l_axis.exists then
				set_local_axis_with_vector(l_axis)
			end
		ensure
			Is_Assigned: local_axis ~ body_1.relative_local_point (a_world_axis_x, a_world_axis_y)
		end

	set_local_axis_with_angle (a_angle:REAL_32)
			-- Change `local_axis' with a converted value from the `a_angle'
		require
			Exists: exists
		do
			set_local_axis_with_vector (convert_angle_to_vector (a_angle))
		end

feature {NONE} -- Implementation

	convert_angle_to_vector (a_angle:REAL_32):B2D_VECTOR_2D
			-- Convert `a_angle' into {B2D_VECTOR_2D}.
		local
			l_rotation:B2D_ROTATION
		do
			create l_rotation.make (a_angle)
			Result := l_rotation.x_axis
		end

end
