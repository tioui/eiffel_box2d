note
	description: "[
					Connected to two {B2D_BODY} and two fixed ground points.
					`Current' supports a ratio such that: length1 + ratio * length2 <= constant .
					Yes, the force transmitted is scaled by the ratio.
					Warning: `Current' can get a bit squirrelly by itself.
					They often work better when combined with {B2D_JOINT_PRISMATIC}.
					You should also cover the the anchor points with static shapes to prevent one side
					from going to zero length.
				]"
	author: "Louis Marchand"
	date: "Fri, 26 Jun 2020 00:02:22 +0000"
	revision: "0.1"

class
	B2D_JOINT_PULLEY

inherit
	B2D_JOINT_SHIFTABLE

create {B2D_ANY}
	make_from_item

feature {NONE} -- Implementation

	anchor_1_internal(a_vector:POINTER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2PulleyJoint_GetAnchorA(item, a_vector)
		end

	anchor_2_internal(a_vector:POINTER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2PulleyJoint_GetAnchorB(item, a_vector)
		end

	reaction_force_internal(a_inverse_time_step:REAL_32; a_vector:POINTER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2PulleyJoint_GetReactionForce(item, a_vector, a_inverse_time_step)
		end

	reaction_torque_internal(a_inverse_time_step:REAL_32):REAL_32
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2PulleyJoint_GetReactionTorque(item, a_inverse_time_step)
		end

	shift_origin_internal(a_vector:POINTER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2PulleyJoint_ShiftOrigin(item, a_vector)
		end

	type_internal:NATURAL
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2Joint_type_pulleyJoint
		end

end
