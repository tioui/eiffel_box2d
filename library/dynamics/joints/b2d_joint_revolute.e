note
	description: "[
					Constrains two {B2D_BODY} to share a common point while they are free to rotate
					about the point. The relative rotation about the shared point is the joint angle.
					You can limit the relative rotation with a joint limit that specifies a lower and
					upper angle. You can use a motor to drive the relative rotation about the shared
					point. A maximum motor torque is provided so that infinite forces are not generated.
				]"
	author: "Louis Marchand"
	date: "Mon, 21 Feb 2022 15:17:30 +0000"
	revision: "0.2"

class
	B2D_JOINT_REVOLUTE

inherit
	B2D_JOINT_GEARABLE

create {B2D_ANY}
	make_from_item

feature -- Access

	local_anchor_1:B2D_VECTOR_2D
			-- The local anchor point relative to the origin of `body_1'
		do
			create Result
			if Result.exists then
				{B2D_EXTERNAL}.b2RevoluteJoint_GetLocalAnchorA(item, Result.item)
			end
		end

	local_anchor_2:B2D_VECTOR_2D
			-- The local anchor point relative to the origin of `body_2'
		do
			create Result
			if Result.exists then
				{B2D_EXTERNAL}.b2RevoluteJoint_GetLocalAnchorB(item, Result.item)
			end
		end

	reference_angle:REAL_32
			-- The angle of the reference of `Current'
		do
			Result := {B2D_EXTERNAL}.b2RevoluteJoint_GetReferenceAngle(item)
		end

	angle:REAL_32
			-- The angle of `Current'
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2RevoluteJoint_GetJointAngle(item)
		end

	speed:REAL_32
			-- The speed of `Current'
		do
			Result := {B2D_EXTERNAL}.b2RevoluteJoint_GetJointSpeed(item)
		end

	is_limit_enabled: BOOLEAN assign set_is_limit_enabled
			-- `True' if `Current' must apply angle limits
		do
			Result := {B2D_EXTERNAL}.b2RevoluteJoint_IsLimitEnabled (item)
		end

	set_is_limit_enabled (a_value:BOOLEAN)
			-- Assign `is_limit_enabled' with the value `a_value'.
		do
			{B2D_EXTERNAL}.b2RevoluteJoint_EnableLimit (item, a_value)
		end

	lower_limit:REAL_32 assign set_lower_limit
			-- The lower angle limit in radian allowed in `Current'
		do
			Result := {B2D_EXTERNAL}.b2RevoluteJoint_GetLowerLimit(item)
		end

	upper_limit:REAL_32 assign set_upper_limit
			-- The upper angle limit in radian allowed in `Current'
		do
			Result := {B2D_EXTERNAL}.b2RevoluteJoint_GetUpperLimit(item)
		end

	set_limits(a_lower, a_upper:REAL_32)
			-- Assign `limits' with the values of `a_lower' and `a_upper' (in radian)
			-- Note that those limit are only apply when `is_limit_enabled' is `True'
		do
			{B2D_EXTERNAL}.b2RevoluteJoint_SetLimits(item, a_lower, a_upper)
		end

	is_motor_enabled: BOOLEAN assign set_is_motor_enabled
			-- `True' if `Current' must apply motor mecanics
		do
			Result := {B2D_EXTERNAL}.b2RevoluteJoint_IsMotorEnabled (item)
		end

	set_is_motor_enabled (a_value:BOOLEAN)
			-- Assign `is_motor_enabled' with the value `a_value'.
		do
			{B2D_EXTERNAL}.b2RevoluteJoint_EnableMotor (item, a_value)
		end

	motor_speed:REAL_32 assign set_motor_speed
			-- The speed of the motor of `Current' (in radian per second).
			-- `is_motor_enabled' must be `True' to be applied.
		do
			Result := {B2D_EXTERNAL}.b2RevoluteJoint_GetMotorSpeed(item)
		end

	set_motor_speed(a_speed:REAL_32)
			-- Assign `motor_speed' with the value of `Current'
		do
			{B2D_EXTERNAL}.b2RevoluteJoint_SetMotorSpeed(item, a_speed)
		end

	maximum_motor_torque:REAL_32 assign set_maximum_motor_torque
			-- The maximum level of torque allow in the motor of `Current' (usually in N*m).
			-- `is_motor_enabled' must be `True' to be applied in `Current'.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2RevoluteJoint_GetMaxMotorTorque(item)
		end

	set_maximum_motor_torque(a_torque:REAL_32)
			-- Assign `maximum_motor_torque' with the value of `a_torque' (usually in N*m)
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2RevoluteJoint_SetMaxMotorTorque(item, a_torque)
		ensure
			Is_Set: maximum_motor_torque ~ a_torque
		end

	motor_torque(a_inverse_time_step:REAL_32):REAL_32
			-- The motor torque given `a_inverse_time_step' in N*m
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2RevoluteJoint_GetMotorTorque(item, a_inverse_time_step)
		end

feature {NONE} -- Implementation

	anchor_1_internal(a_vector:POINTER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2RevoluteJoint_GetAnchorA(item, a_vector)
		end

	anchor_2_internal(a_vector:POINTER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2RevoluteJoint_GetAnchorB(item, a_vector)
		end

	reaction_force_internal(a_inverse_time_step:REAL_32; a_vector:POINTER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2RevoluteJoint_GetReactionForce(item, a_vector, a_inverse_time_step)
		end

	reaction_torque_internal(a_inverse_time_step:REAL_32):REAL_32
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2RevoluteJoint_GetReactionTorque(item, a_inverse_time_step)
		end

	type_internal:NATURAL
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2Joint_type_revoluteJoint
		end

end
