note
	description: "Structure that contains information to create a {B2D_JOINT_MOTOR}."
	author: "Patrick Boucher"
	date: "Sat, 27 Jun 2020 04:24:21 +0000"
	revision: "0.1"

class
	B2D_JOINT_MOTOR_DEFINITION

inherit
	B2D_JOINT_DEFINITION

create
	make

feature {NONE} -- Initialization

	make (a_body_1, a_body_2:B2D_BODY)
			-- Initialization of `Current'.
		require
			Body_1_Exists: a_body_1.exists
			Body_2_Exists: a_body_2.exists
			Bodies_Not_Same: a_body_1 /= a_body_2
		do
			own_from_item ({B2D_EXTERNAL}.b2motorjointdef_new)
			if exists then
				{B2D_EXTERNAL}.b2motorjointdef_initialize (item, a_body_1.item, a_body_2.item)
			end
			body_1 := a_body_1
			body_2 := a_body_2
		ensure
			Body_1_Assigned: body_1 ~ a_body_1
			Body_2_Assigned: body_2 ~ a_body_2
		end

feature -- access

	linear_offset:B2D_VECTOR_2D assign set_linear_offset
			-- Position of `body_1' minus the position of `body_2', in `body_1''s frame, in meters.
		require
			Exists: exists
		do
			create Result.shared_from_item ({B2D_EXTERNAL}.b2motorjointdef_linearoffset_get (item), Current)
		end

	set_linear_offset (a_offset:B2D_VECTOR_2D)
			-- Assign `linear_offset' with the value `a_offset'.
		require
			Exists: exists
			Vector_Exists: a_offset.exists
		do
			{B2D_EXTERNAL}.b2motorjointdef_linearoffset_set (item, a_offset.item)
		ensure
			Is_Assigned: linear_offset ~ a_offset
		end

	set_linear_offset_with_coordinates (a_x, a_y:REAL_32)
			-- Assign `linear_offset' with the values `a_x' and `a_y'.
		require
			Exists: exists
		local
			l_vector:B2D_VECTOR_2D
		do
			create l_vector.make_with_coordinates (a_x, a_y)
			if l_vector.exists then
				set_linear_offset (l_vector)
			end
		ensure
			Is_Assigned_X: linear_offset.x ~ a_x
			Is_Assigned_Y: linear_offset.y ~ a_y
		end

	angular_offset:REAL_32 assign set_angular_offset
			-- The `body_2' angle minus `body_1' angle in radians.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2motorjointdef_angularoffset_get (item)
		end

	set_angular_offset (a_offset:REAL_32)
			-- Assign `angular_offset' with the value `a_offset'.
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2motorjointdef_angularoffset_set (item, a_offset)
		ensure
			Is_Assigned: angular_offset ~ a_offset
		end

	max_force:REAL_32 assign set_max_force
			-- The maximum motor force in N.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2motorjointdef_maxforce_get (item)
		end

	set_max_force (a_force:REAL_32)
			-- Assign `max_force' with the value `a_force'.
		require
			Exists: exists
			Max_Force_Valid: a_force >= 0.0
		do
			{B2D_EXTERNAL}.b2motorjointdef_maxforce_set (item, a_force)
		ensure
			Is_Assigned: max_force ~ a_force
		end

	max_torque:REAL_32 assign set_max_torque
			-- The maximum motor torque in N-m.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2motorjointdef_maxtorque_get (item)
		end

	set_max_torque (a_torque:REAL_32)
			-- Assign `max_torque' with the value `a_torque'.
		require
			Exists: exists
			Max_Torque_Valid: a_torque >= 0.0
		do
			{B2D_EXTERNAL}.b2motorjointdef_maxtorque_set (item, a_torque)
		ensure
			Is_Assigned: max_torque ~ a_torque
		end

	position_correction_factor:REAL_32 assign set_position_correction_factor
			-- The position correction factor in the range [0,1].
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2motorjointdef_correctionfactor_get (item)
		end

	set_position_correction_factor (a_correction_factor:REAL_32)
			-- Assign `position_correction_factor' with the value `a_correction_factor'.
		require
			Exists: exists
			Correction_Factor_Valid: a_correction_factor >= 0.0 and a_correction_factor <= 1.0
		do
			{B2D_EXTERNAL}.b2motorjointdef_correctionfactor_set (item, a_correction_factor)
		ensure
			Is_Assigned: position_correction_factor ~ a_correction_factor
		end

feature {NONE} -- Implementation

	type_internal:NATURAL
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2joint_type_motorjoint
		end

	delete
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2motorjointdef_delete (item)
		end

invariant
	Max_Force_Valid: exists implies max_force >= 0.0
	Max_Torque_Valid: exists implies max_torque >= 0.0
	Position_Correction_Factor_Valid: exists implies position_correction_factor >= 0.0 and position_correction_factor <= 1.0

end
