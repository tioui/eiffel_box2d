note
	description: "Structure that countains information to create a {B2D_JOINT_FRICTION}."
	author: "Patrick Boucher"
	date: "Wed, 24 Jun 2020 22:01:33 +0000"
	revision: "0.1"

class
	B2D_JOINT_FRICTION_DEFINITION

inherit
	B2D_JOINT_DEFINITION_UNIQUE_ANCHOR

create
	make,
	make_with_vector

feature {NONE} -- Initialization

	make (a_body_1, a_body_2:B2D_BODY; a_anchor_x, a_anchor_y:REAL_32)
			-- Initialization of `Current'.
		require
			Body_1_Exists: a_body_1.exists
			Body_2_Exists: a_body_2.exists
			Bodies_Not_Same: a_body_1 /= a_body_2
		local
			l_anchor:B2D_VECTOR_2D
		do
			create l_anchor.make_with_coordinates (a_anchor_x, a_anchor_y)
			make_with_vector (a_body_1, a_body_2, l_anchor)
		end

	make_with_vector (a_body_1, a_body_2:B2D_BODY; a_anchor:B2D_VECTOR_2D)
			-- Initialization of `Current'.
		require
			Body_1_Exists: a_body_1.exists
			Body_2_Exists: a_body_2.exists
			Anchor_Exists: a_anchor.exists
		do
			own_from_item ({B2D_EXTERNAL}.b2frictionjointdef_new)
			if exists then
				{B2D_EXTERNAL}.b2frictionjointdef_initialize (item, a_body_1.item, a_body_2.item, a_anchor.item)
			end
			body_1 := a_body_1
			body_2 := a_body_2
		end

feature -- Access

	set_anchor_with_vector (a_world_point:B2D_VECTOR_2D)
			-- <Precursor>
		local
			l_local_anchor_1, l_local_anchor_2:B2D_VECTOR_2D
		do
			l_local_anchor_1 := body_1.world_point_to_local_coordinates_with_vector (a_world_point)
			l_local_anchor_2 := body_2.world_point_to_local_coordinates_with_vector (a_world_point)
			{B2D_EXTERNAL}.b2frictionjointdef_localanchora_set (item, l_local_anchor_1.item)
			{B2D_EXTERNAL}.b2frictionjointdef_localanchorb_set (item, l_local_anchor_2.item)
		end

	local_anchor_1:B2D_VECTOR_2D
			-- <Precursor>
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2frictionjointdef_localanchora_get (item))
		end

	local_anchor_2:B2D_VECTOR_2D
			-- <Precursor>
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2frictionjointdef_localanchorb_get (item))
		end

	max_force:REAL_32 assign set_max_force
			-- The maximum friction force in N.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2frictionjointdef_maxforce_get (item)
		end

	set_max_force (a_force:REAL_32)
			-- Assign `max_force' with the non-negative value `a_force'.
		require
			Exists: exists
			Non_Negative: a_force >= 0
		do
			{B2D_EXTERNAL}.b2frictionjointdef_maxforce_set (item, a_force)
		ensure
			Is_Assign: max_force ~ a_force
		end

	max_torque:REAL_32 assign set_max_torque
			-- The maximum friction torque in N-m.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2frictionjointdef_maxtorque_get (item)
		end

	set_max_torque (a_torque:REAL_32)
			-- Assign `max_torque' with the non-negative value `a_torque'.
		require
			Exists: exists
			Non_Negative: a_torque >= 0
		do
			{B2D_EXTERNAL}.b2frictionjointdef_maxtorque_set (item, a_torque)
		ensure
			Is_Set: max_torque ~ a_torque
		end

feature {NONE} -- Implementation

	type_internal:NATURAL
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2joint_type_frictionjoint
		end

	delete
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2FrictionJointDef_delete(item)
		end


end
