note
	description: "Used for top-down friction. It provides 2D translational friction and angular friction."
	author: "Louis Marchand"
	date: "Mon, 21 Feb 2022 15:17:30 +0000"
	revision: "0.2"

class
	B2D_JOINT_FRICTION

inherit
	B2D_JOINT

create {B2D_ANY}
	make_from_item

feature {NONE} -- Implementation

	anchor_1_internal(a_vector:POINTER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2FrictionJoint_GetAnchorA(item, a_vector)
		end

	anchor_2_internal(a_vector:POINTER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2FrictionJoint_GetAnchorB(item, a_vector)
		end

	reaction_force_internal(a_inverse_time_step:REAL_32; a_vector:POINTER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2FrictionJoint_GetReactionForce(item, a_vector, a_inverse_time_step)
		end

	type_internal:NATURAL
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2Joint_type_frictionJoint
		end

	reaction_torque_internal(a_inverse_time_step:REAL_32):REAL_32
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2FrictionJoint_GetReactionTorque(item, a_inverse_time_step)
		end

end
