note
	description: "[
					Used to make a point on a {B2D_BODY} track a specified world point.
					This a soft constraint with a maximum force. This allows the constraint
					to stretch and without applying huge forces.
				]"
	author: "Louis Marchand"
	date: "Fri, 26 Jun 2020 00:02:22 +0000"
	revision: "0.1"

class
	B2D_JOINT_MOUSE

inherit
	B2D_JOINT_SHIFTABLE

create {B2D_ANY}
	make_from_item

feature -- Access

	target:B2D_VECTOR_2D assign set_target_with_vector
			-- The position that attract the bodies.
			-- It is the 'mouse' position in world coordinates.
		require
			Exists: exists
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2MouseJoint_GetTarget(item))
		end

	set_target_with_vector(a_target:B2D_VECTOR_2D)
			-- Assign `target' with the value of `a_target'
		require
			Exists: exists
			Target_Exists: a_target.exists
		do
			{B2D_EXTERNAL}.b2MouseJoint_SetTarget(item, a_target.item)
		ensure
			Is_Assign: target ~ a_target
		end

	set_target(a_x, a_y:REAL_32)
			-- Assign `targer' with the coordinates `a_x', `a_y'
		require
			Exists: exists
		local
			l_target:B2D_VECTOR_2D
		do
			create l_target.make_with_coordinates (a_x, a_y)
			check l_target.exists then
				{B2D_EXTERNAL}.b2MouseJoint_SetTarget(item, l_target.item)
			end
		ensure
			Is_Assign_X: target.x ~ a_x
			Is_Assign_Y: target.y ~ a_y
		end

	maximum_force:REAL_32 assign set_maximum_force
			-- The force in Newton that `Current' can substain.
		require
			Exists:exists
		do
			Result := {B2D_EXTERNAL}.b2MouseJoint_GetMaxForce(item)
		end

	set_maximum_force(a_force:REAL_32)
			-- Assign `maximum_force' with the value of `a_force'
		require
			Exists: exists
			Force_Valid: a_force >= 0
		do
			{B2D_EXTERNAL}.b2MouseJoint_SetMaxForce(item, a_force)
		ensure
			Is_Assign: maximum_force ~ a_force
		end

	set_maximum_force_with_weight_multiplier (a_body:B2D_BODY; a_multiplier:INTEGER_32)
				-- Assign `maximum_force' with the `a_body''s weight multiplied by `a_multiplier'.
		require
			Exists: exists
			Body_Exists: a_body.exists
			Non_Negative_Multiplier: a_multiplier >= 0
		local
			l_force:REAL_32
		do
			if a_multiplier > 0 then
				l_force := a_multiplier * a_body.mass * a_body.world.gravity.y.abs
				if l_force < 0 then
					l_force := 0
				end
			end
			set_maximum_force (l_force)
		end

	frequency:REAL_32 assign set_frequency
			-- The frequency in Hertz.
		require
			Exists:exists
		do
			Result := {B2D_EXTERNAL}.b2MouseJoint_GetFrequency(item)
		end

	set_frequency(a_frequency:REAL_32)
			-- Assign `frequency' with the value of `a_frequency'
		require
			Exists: exists
			Frequency_Valid: a_frequency >= 0
		do
			{B2D_EXTERNAL}.b2MouseJoint_SetFrequency(item, a_frequency)
		ensure
			Is_Assign: frequency ~ a_frequency
		end

	damping_ratio:REAL_32 assign set_damping_ratio
			-- The dimensionless ratio of the dampling.
		require
			Exists:exists
		do
			Result := {B2D_EXTERNAL}.b2MouseJoint_GetDampingRatio(item)
		end

	set_damping_ratio(a_damping_ratio:REAL_32)
			-- Assign `damping_ratio' with the value of `a_damping_ratio'
		require
			Exists: exists
			Dampling_Ratio_Valid: a_damping_ratio >= 0
		do
			{B2D_EXTERNAL}.b2MouseJoint_SetDampingRatio(item, a_damping_ratio)
		ensure
			Is_Assign: damping_ratio ~ a_damping_ratio
		end

feature {NONE} -- Implementation

	anchor_1_internal(a_vector:POINTER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2MouseJoint_GetAnchorA(item, a_vector)
		end

	anchor_2_internal(a_vector:POINTER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2MouseJoint_GetAnchorB(item, a_vector)
		end

	reaction_force_internal(a_inverse_time_step:REAL_32; a_vector:POINTER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2MouseJoint_GetReactionForce(item, a_vector, a_inverse_time_step)
		end

	shift_origin_internal(a_vector:POINTER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2MouseJoint_ShiftOrigin(item, a_vector)
		end

	type_internal:NATURAL
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2Joint_type_mouseJoint
		end

	reaction_torque_internal(a_inverse_time_step:REAL_32):REAL_32
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2MouseJoint_GetReactionTorque(item, a_inverse_time_step)
		end


invariant
	Non_Negative_Max_Force: exists implies maximum_force >= 0
	Non_Negative_Frequency: exists implies frequency >= 0
	Non_Negative_Damping_Ratio: exists implies damping_ratio >= 0
end
