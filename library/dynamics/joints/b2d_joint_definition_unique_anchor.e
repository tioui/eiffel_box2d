note
	description: "[
		Common ancestor for {B2D_JOINT_DEFINITION} that possess a unique anchor that
		connects 2 {B2D_BODY} together.
			]"
	author: "Patrick Boucher"
	date: "Thu, 09 Jul 2020 04:10:58 +0000"
	revision: "0.1"

deferred class
	B2D_JOINT_DEFINITION_UNIQUE_ANCHOR

inherit
	B2D_JOINT_DEFINITION

feature -- Access

	anchor:B2D_VECTOR_2D
			-- The world point where `body_1' and `body_2' are connected.
		require
			Exists: exists
		do
			Result := body_1.local_point_to_world_coordinates_with_vector (local_anchor_1)
		end

	set_anchor (a_world_point_x, a_world_point_y:REAL_32)
			-- Modify `local_anchor_*' so that `anchor' is assign with the value of `a_world_point_x' and `a_world_point_y'.
		require
			Exists: exists
		local
			l_anchor:B2D_VECTOR_2D
		do
			create l_anchor.make_with_coordinates (a_world_point_x, a_world_point_y)
			check l_anchor.exists then
				set_anchor_with_vector(l_anchor)
			end
		ensure
			Is_Assign_X: anchor.x ~ a_world_point_x
			Is_Assign_Y: anchor.y ~ a_world_point_y
			Local_Anchor_1_Assigned: local_anchor_1 ~ body_1.world_point_to_local_coordinates (a_world_point_x, a_world_point_y)
			Local_Anchor_2_Assigned: local_anchor_2 ~ body_2.world_point_to_local_coordinates (a_world_point_x, a_world_point_y)
		end

	set_anchor_with_vector (a_world_point:B2D_VECTOR_2D)
			-- Modify `local_anchor_*' so that `anchor' is assign with the value `a_world_point'.
		require
			Exists: exists
			Vector_Exists: a_world_point.exists
		deferred
		ensure
			Anchor_Assigned: anchor ~ a_world_point
			Local_Anchor_1_Assigned: local_anchor_1 ~ body_1.world_point_to_local_coordinates_with_vector (a_world_point)
			Local_Anchor_2_Assigned: local_anchor_2 ~ body_2.world_point_to_local_coordinates_with_vector (a_world_point)
		end

	local_anchor_1:B2D_VECTOR_2D
			-- The local anchor point relative to the origin of `body_1'.
		require
			Exists: exists
		deferred
		end

	local_anchor_2:B2D_VECTOR_2D
			-- The local anchor point relative to the origin of `body_2'.
		require
			Exists: exists
		deferred
		end

end
