note
	description: "[
					Enforces a maximum distance between two points on two {B2D_BODY}.
					It has no other effect. 
					Warning: if you attempt to change the maximum length during the simulation
					you will get some non-physical behavior. A model that would allow you to
					dynamically modify the length would have some sponginess, so I chose not to
					implement it that way.
					See {B2D_JOINT_DISTANCE} if you want to dynamically control length.
				]"
	author: "Louis Marchand"
	date: "Mon, 21 Feb 2022 15:17:30 +0000"
	revision: "0.2"

class
	B2D_JOINT_ROPE

inherit
	B2D_JOINT

create {B2D_ANY}
	make_from_item

feature {NONE} -- Implementation

	anchor_1_internal(a_vector:POINTER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2RopeJoint_GetAnchorA(item, a_vector)
		end

	anchor_2_internal(a_vector:POINTER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2RopeJoint_GetAnchorB(item, a_vector)
		end

	reaction_force_internal(a_inverse_time_step:REAL_32; a_vector:POINTER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2RopeJoint_GetReactionForce(item, a_vector, a_inverse_time_step)
		end

	reaction_torque_internal(a_inverse_time_step:REAL_32):REAL_32
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2RopeJoint_GetReactionTorque(item, a_inverse_time_step)
		end

	type_internal:NATURAL
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2Joint_type_ropeJoint
		end

end
