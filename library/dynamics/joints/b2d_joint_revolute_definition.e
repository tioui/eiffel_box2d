note
	description: "Structure used to create a {B2D_JOINT_REVOLUTE}."
	author: "Patrick Boucher"
	date: "Sun, 05 Jul 2020 20:59:40 +0000"
	revision: "0.1"

class
	B2D_JOINT_REVOLUTE_DEFINITION

inherit
	B2D_JOINT_GEARABLE_DEFINITION

create
	make,
	make_with_vector

feature {NONE} -- Initialization

	make (a_body_1, a_body_2:B2D_BODY; a_anchor_x, a_anchor_y:REAL_32)
			-- Initialization of `Current' using `a_body_1' as`body_1' and `a_body_2' as `body_2'.
			-- Computing `local_anchor_1', `local_anchor_2' from the world point `a_anchor.x', `a_anchor_y'
			-- and computing `reference_angle' from the rotation of `body_1' and `body_2'.
		require
			Body_1_Exists: a_body_1.exists
			Body_2_Exists: a_body_2.exists
			Bodies_Not_Same: a_body_1 /= a_body_2
		local
			l_anchor_vector:B2D_VECTOR_2D
		do
			create l_anchor_vector.make_with_coordinates (a_anchor_x, a_anchor_y)
			check l_anchor_vector.exists then
				make_with_vector (a_body_1, a_body_2, l_anchor_vector)
			end
		ensure
			Body_1_Assigned: body_1 ~ a_body_1
			Body_2_Assigned: body_2 ~ a_body_2
		end

	make_with_vector (a_body_1, a_body_2:B2D_BODY; a_anchor:B2D_VECTOR_2D)
			-- Initialization of `Current' using `a_body_1' as`body_1' and `a_body_2' as `body_2'.
			-- Computing `local_anchor_1', `local_anchor_2' from the world point `a_anchor' and
			-- computing `reference_angle' from the rotation of `body_1' and `body_2'.
		require
			Body_1_Exists: a_body_1.exists
			Body_2_Exists: a_body_2.exists
			Anchor_Exists: a_anchor.exists
		do
			own_from_item ({B2D_EXTERNAL}.b2revolutejointdef_new)
			if exists then
				{B2D_EXTERNAL}.b2revolutejointdef_initialize (item, a_body_1.item, a_body_2.item, a_anchor.item)
			end
			body_1 := a_body_1
			body_2 := a_body_2
		ensure
			Body_1_Assigned: body_1 ~ a_body_1
			Body_2_Assigned: body_2 ~ a_body_2
		end

feature -- Access

	set_anchor_with_vector (a_world_point:B2D_VECTOR_2D)
			-- <Precursor>
		local
			l_local_anchor_1, l_local_anchor_2:B2D_VECTOR_2D
		do
			l_local_anchor_1 := body_1.world_point_to_local_coordinates_with_vector (a_world_point)
			l_local_anchor_2 := body_2.world_point_to_local_coordinates_with_vector (a_world_point)
			{B2D_EXTERNAL}.b2revolutejointdef_localanchora_set (item, l_local_anchor_1.item)
			{B2D_EXTERNAL}.b2revolutejointdef_localanchorb_set (item, l_local_anchor_2.item)
		end

	local_anchor_1:B2D_VECTOR_2D
			-- <Precursor>
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2revolutejointdef_localanchora_get (item))
		end

	local_anchor_2:B2D_VECTOR_2D
			-- <Precursor>
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2revolutejointdef_localanchorb_get (item))
		end

	reference_angle:REAL_32
			-- <Precursor>
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2revolutejointdef_referenceangle_get (item)
		end

	update_reference_angle
			-- <Precursor>
		require
			Exists: exists
		local
			l_angle:REAL_32
		do
			l_angle := body_2.angle - body_1.angle
			{B2D_EXTERNAL}.b2revolutejointdef_referenceangle_set (item, l_angle)
		end

	is_limit_enabled:BOOLEAN assign set_is_limit_enabled
			-- Is rotation of `Current' is limited?
		do
			Result := {B2D_EXTERNAL}.b2revolutejointdef_enablelimit_get (item)
		end

	set_is_limit_enabled (a_value:BOOLEAN)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2revolutejointdef_enablelimit_set (item, a_value)
		end

	lower_limit:REAL_32 assign set_lower_limit
			-- The lower rotation limit in radians.
		do
			Result := {B2D_EXTERNAL}.b2revolutejointdef_lowerangle_get (item)
		end

	set_lower_limit (a_value:REAL_32)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2revolutejointdef_lowerangle_set (item, a_value)
		end

	upper_limit:REAL_32
			-- The upper rotation limit in radians.
		do
			Result := {B2D_EXTERNAL}.b2revolutejointdef_upperangle_get (item)
		end

	set_upper_limit (a_value:REAL_32)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2revolutejointdef_upperangle_set (item, a_value)
		end

	is_motor_enabled:BOOLEAN assign set_is_motor_enabled
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2revolutejointdef_enablemotor_get (item)
		end

	set_is_motor_enabled (a_value:BOOLEAN)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2revolutejointdef_enablemotor_set (item, a_value)
		end

	motor_speed:REAL_32 assign set_motor_speed
			-- Rotation speed when powered, in radians per seconds.
		do
			Result := {B2D_EXTERNAL}.b2revolutejointdef_motorspeed_get (item)
		end

	set_motor_speed (a_value:REAL_32)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2revolutejointdef_motorspeed_set (item, a_value)
		end


	maximum_motor_torque:REAL_32
			-- The maximum motor torque in N-m.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2revolutejointdef_maxmotortorque_get (item)
		end

	set_maximum_motor_torque (a_value:REAL_32)
			-- Assign `maximum_motor_torque' with the value `a_value'.
		require
			Exists: exists
			Non_Negative_Torque: a_value >= 0.0
		do
			{B2D_EXTERNAL}.b2revolutejointdef_maxmotortorque_set (item, a_value)
		ensure
			Is_Assign: maximum_motor_torque ~ a_value
		end

feature {NONE} -- Implementation

	type_internal:NATURAL
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2joint_type_revolutejoint
		end

	delete
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2revolutejointdef_delete (item)
		end

end
