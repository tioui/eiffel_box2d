note
	description: "Common ancestor of every {B2D_JOINT_*_EFINITION}."
	author: "Louis Marchand"
	date: "Mon, 22 Jun 2020 20:01:15 +0000"
	revision: "0.1"

deferred class
	B2D_JOINT_DEFINITION

inherit
	B2D_MEMORY_OBJECT

feature -- Access

	body_1:B2D_BODY
			-- The first {B2D_BODY} that is connected to `Current'

	set_body_1(a_body_1:B2D_BODY)
			-- Assign `body_1' with the value of `a_body_1'
		require
			Exists: exists
			Body_Exists: a_body_1.exists
		do
			{B2D_EXTERNAL}.b2JointDef_bodyA_set(item, a_body_1.item)
			body_1 := a_body_1
		ensure
			Is_Assign: body_1 ~ a_body_1
		end

	body_2:B2D_BODY
			-- The second {B2D_BODY} that is connected to `Current'

	set_body_2(a_body_2:B2D_BODY)
			-- Assign `body_2' with the value of `a_body_2'
		require
			Exists: exists
			Body_Exists: a_body_2.exists
		do
			{B2D_EXTERNAL}.b2JointDef_bodyB_set(item, a_body_2.item)
			body_2 := a_body_2
		ensure
			Is_Assign: body_2 ~ a_body_2
		end

	must_collide_connected:BOOLEAN assign set_must_collide_connected
			-- `True' if `body_1' and `body_2' can collide.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2JointDef_collideConnected_get(item)
		end

	set_must_collide_connected(a_must_collide_connected:BOOLEAN)
			-- Assign `must_collide_connected' with the value of `a_must_collide_connected'
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2JointDef_collideConnected_set(item, a_must_collide_connected)
		ensure
			Is_Assign: must_collide_connected ~ a_must_collide_connected
		end

	enable_collide_connected
			-- Put `must_collide_connected' to `True'
		require
			Exists: exists
		do
			set_must_collide_connected(True)
		ensure
			Is_Set: must_collide_connected
		end

	disable_collide_connected
			-- Put `must_collide_connected' to `True'
		require
			Exists: exists
		do
			set_must_collide_connected(False)
		ensure
			Is_Set: not must_collide_connected
		end

feature {NONE} -- Implementation

	type_internal:NATURAL
			-- Internal type of `Current'
		deferred
		end

invariant
	Body_1_Valid: exists implies {B2D_EXTERNAL}.b2JointDef_bodyA_get(item) ~ body_1.item
	Body_2_Valid: exists implies {B2D_EXTERNAL}.b2JointDef_bodyB_get(item) ~ body_2.item
	Type_Valid: exists implies {B2D_EXTERNAL}.b2jointdef_type_get(item) ~ type_internal
end
