note
	description: "Common ancestor for {B2D_JOINT_DEFINITION} that possess a reference angle."
	author: "Patrick Boucher"
	date: "Fri, 10 Jul 2020 18:33:54 +0000"
	revision: "0.1"

deferred class
	B2D_JOINT_DEFINITION_ANGLE

inherit
	B2D_JOINT_DEFINITION

feature -- Access

	reference_angle:REAL_32
			-- The constraint angle between the bodies.
			-- This should be updated before creating a {B2D_JOINT},
			-- unless the rotation of `body_1' or `body_2' has not changed since the creation of `Current'
			-- or since the last reference angle update.
			-- See `update_reference_angle'.
		require
			Exists: exists
		deferred
		end

	update_reference_angle
			-- Update `reference_angle'. This should be used before creating a {B2D_JOINT},
			-- unless the rotation of `body_1' or `body_2' has not changed since the creation of `Current'
			-- or since the last reference angle update or if you want to keep the old `reference_angle'.
		require
			Exists: exists
		deferred
		ensure
			Is_Assigned: reference_angle ~ body_2.angle - body_1.angle
		end

end
