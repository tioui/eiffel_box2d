note
	description: "A {B2D_JOINT} that can shift it's point of origin."
	author: "Louis Marchand"
	date: "Mon, 21 Feb 2022 15:17:30 +0000"
	revision: "0.2"

deferred class
	B2D_JOINT_SHIFTABLE

inherit
	B2D_JOINT

feature -- Access



	shift_origin(a_x, a_y:REAL_32)
			-- Move the origin of points by `a_x', `a_y' in world coordinate.
		require
			Exists:exists
		local
			l_vector:B2D_VECTOR_2D
		do
			create l_vector.make_with_coordinates (a_x, a_y)
			check l_vector.exists then
				shift_origin_internal(l_vector.item)
			end
		end

	shift_origin_with_vector(a_vector:B2D_VECTOR_2D)
			-- Move the origin of points by `a_vector' in world coordinate
		require
			Exists:exists
			Vector_Exists:a_vector.exists
		do
			shift_origin_internal(a_vector.item)
		end

feature {NONE} -- Implementation

	shift_origin_internal(a_vector:POINTER)
			-- Internal value of `shift_origin'
		require
			Exists: exists
			Vector_Exists: not a_vector.is_default_pointer
		deferred
		end

end
