note
	description: "Structure used to create a {B2D_JOINT_PULLEY}."
	author: "Patrick Boucher"
	date: "Thu, 02 Jul 2020 04:38:23 +0000"
	revision: "0.1"

class
	B2D_JOINT_PULLEY_DEFINITION

inherit
	B2D_JOINT_DEFINITION

create
	make,
	make_with_ratio

feature {NONE} -- Initialization

	make (a_body_1, a_body_2:B2D_BODY; a_ground_anchor_1, a_ground_anchor_2, a_anchor_1, a_anchor_2:B2D_VECTOR_2D)
			-- Initialization of `Current' using `a_body_1' as `body_1' `a_body_2' as `body_2',
			-- `a_ground_anchor_1' as `ground_anchor_1,' and `a_ground_anchor_2' as `ground_anchor_2';
			-- Computing `local_anchor_1', `local_anchor_2', `segment_length_1' and `segment_length_2' from the
			-- world positions `a_anchor_1' and `a_anchor_2';
			-- Gives to `ratio' the default value of 1.0;
		require
			Body_1_Exists: a_body_1.exists
			Body_2_Exists: a_body_2.exists
			Ground_Anchor_1_Exists: a_ground_anchor_1.exists
			Ground_Anchor_2_Exists: a_ground_anchor_2.exists
			Anchor_1_Exists: a_anchor_1.exists
			Anchor_2_Exists: a_anchor_2.exists
			Bodies_Not_Same: a_body_1 /= a_body_2
		do
			make_with_ratio (a_body_1, a_body_2, a_ground_anchor_1, a_ground_anchor_2, a_anchor_1, a_anchor_2, 1.0)
		ensure
			Body_1_Assigned: body_1 ~ a_body_1
			Body_2_Assigned: body_2 ~ a_body_2
		end

	make_with_ratio (a_body_1, a_body_2:B2D_BODY; a_ground_anchor_1, a_ground_anchor_2, a_anchor_1, a_anchor_2:B2D_VECTOR_2D; a_ratio:REAL_32)
			-- Initialization of `Current' using `a_body_1' as `body_1' `a_body_2' as `body_2',
			-- `a_ground_anchor_1' as `ground_anchor_1,', `a_ground_anchor_2' as `ground_anchor_2' and `a_ratio' as `ratio';
			-- Computing `local_anchor_1', `local_anchor_2', `segment_length_1' and `segment_length_2' from the
			-- world positions `a_anchor_1' and `a_anchor_2';
		require
			Body_1_Exists: a_body_1.exists
			Body_2_Exists: a_body_2.exists
			Ground_Anchor_1_Exists: a_ground_anchor_1.exists
			Ground_Anchor_2_Exists: a_ground_anchor_2.exists
			Anchor_1_Exists: a_anchor_1.exists
			Anchor_2_Exists: a_anchor_2.exists
			Ratio_Valid: a_ratio > 0.0
		do
			own_from_item ({B2D_EXTERNAL}.b2pulleyjointdef_new)
			if exists then
				{B2D_EXTERNAL}.b2pulleyjointdef_initialize (
					item, a_body_1.item, a_body_2.item, a_ground_anchor_1.item, a_ground_anchor_2.item, a_anchor_1.item, a_anchor_2.item, a_ratio)
			end
			body_1 := a_body_1
			body_2 := a_body_2
		ensure
			Body_1_Assigned: body_1 ~ a_body_1
			Body_2_Assigned: body_2 ~ a_body_2
		end

feature -- Access

	ground_anchor_1:B2D_VECTOR_2D assign set_ground_anchor_1
			-- The anchor attached to a world point. This is linked to `anchor_1' by a segment.
			-- This point can't be changed inside a {B2D_JOINT_PULLEY}.
		require
			Exists: exists
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2pulleyjointdef_groundanchora_get (item))
		end

	set_ground_anchor_1 (a_world_point:B2D_VECTOR_2D)
			-- Assign `ground_anchor_1' with the value `a_world_point'.
		require
			Exists: exists
			Vector_Exists: a_world_point.exists
		do
			{B2D_EXTERNAL}.b2pulleyjointdef_groundanchora_set (item, a_world_point.item)
			update_internal_segment_lengths
		ensure
			Is_Assigned: ground_anchor_1 ~ a_world_point
		end

	ground_anchor_2:B2D_VECTOR_2D assign set_ground_anchor_2
			-- The anchor attached to a world point. This is linked to `anchor_2' by a segment.
			-- This point can't be changed inside a {B2D_JOINT_PULLEY}.
		require
			Exists: exists
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2pulleyjointdef_groundanchorb_get (item))
		end

	set_ground_anchor_2 (a_world_point:B2D_VECTOR_2D)
			-- Assign `ground_anchor_2' with the value `a_world_point'.
		require
			Exists: exists
			Vector_Exists: a_world_point.exists
		do
			{B2D_EXTERNAL}.b2pulleyjointdef_groundanchorb_set (item, a_world_point.item)
			update_internal_segment_lengths
		ensure
			Is_Assigned: ground_anchor_2 ~ a_world_point
		end

	local_anchor_1:B2D_VECTOR_2D assign set_local_anchor_1
			-- The local anchor attached to `body_1'.
		require
			Exists: exists
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2pulleyjointdef_localanchora_get (item))
		end

	set_local_anchor_1 (a_local_point:B2D_VECTOR_2D)
			-- Assign `local_anchor_1' with the value `a_local_point'.
		require
			Exists: exists
			Vector_Exists: a_local_point.exists
		do
			{B2D_EXTERNAL}.b2pulleyjointdef_localanchora_set (item, a_local_point.item)
			update_internal_segment_lengths
		ensure
			Is_Assigned: local_anchor_1 ~ a_local_point
		end

	set_local_anchor_1_with_world_point (a_world_point:B2D_VECTOR_2D)
			-- Assign `local_anchor_1' with the `body_1''s local point equivalent to `a_world_point'.
		require
			Exists: exists
			Vector_Exists: a_world_point.exists
		local
			l_local_point:B2D_VECTOR_2D
		do
			l_local_point := body_1.world_point_to_local_coordinates_with_vector (a_world_point)
			set_local_anchor_1 (l_local_point)
		ensure
			Is_Assigned: world_anchor_1 ~ a_world_point
		end

	local_anchor_2:B2D_VECTOR_2D assign set_local_anchor_2
			-- The local anchor attached to `body_2'.
		require
			Exists: exists
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2pulleyjointdef_localanchorb_get (item))
		end

	set_local_anchor_2 (a_local_point:B2D_VECTOR_2D)
			-- Assign `local_anchor_2' with the value `a_local_point'.
		require
			Exists: exists
			Vector_Exists: a_local_point.exists
		do
			{B2D_EXTERNAL}.b2pulleyjointdef_localanchorb_set (item, a_local_point.item)
			update_internal_segment_lengths
		ensure
			Is_Assigned: local_anchor_2 ~ a_local_point
		end

	set_local_anchor_2_with_world_point (a_world_point:B2D_VECTOR_2D)
			-- Assign `local_anchor_2' with the `body_2''s local point equivalent to `a_world_point'.
		require
			Exists: exists
			Vector_Exists: a_world_point.exists
		local
			l_local_point:B2D_VECTOR_2D
		do
			l_local_point := body_2.world_point_to_local_coordinates_with_vector (a_world_point)
			set_local_anchor_2 (l_local_point)
		ensure
			Is_Assigned: world_anchor_2 ~ a_world_point
		end

	segment_length_1:REAL_32
			-- The reference length for the segment attached to `body_1'.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2pulleyjointdef_lengtha_get (item)
		end

	segment_length_2:REAL_32
			-- The reference length for the segment attached to `body_2'.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2pulleyjointdef_lengthb_get (item)
		end

	ratio:REAL_32 assign set_ratio
			-- The pulley ratio, used to simulate a block-and-tackle.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2pulleyjointdef_ratio_get (item)
		end

	set_ratio (a_value:REAL_32)
			-- Assign `ratio' with the value `a_value'.
		require
			Exists: exists
			Positive_Value: a_value > 0
		do
			{B2D_EXTERNAL}.b2pulleyjointdef_ratio_set (item, a_value)
		ensure
			Is_Assigned: ratio ~ a_value
		end

feature {NONE} -- Implementation

	world_anchor_1:B2D_VECTOR_2D
			-- World position of `local_anchor_1'.
		do
			Result := body_1.local_point_to_world_coordinates_with_vector (local_anchor_1)
		end

	world_anchor_2:B2D_VECTOR_2D
			-- World position of `local_anchor_2'.
		do
			Result := body_2.local_point_to_world_coordinates_with_vector (local_anchor_2)
		end

	update_internal_segment_lengths
			-- Compute `segment_length_1' and `segment_length_2'
			-- with the distance between `local_anchor_1' and `ground_anchor_1'
			-- and the distance between `local_anchor_2' and `ground_anchor_2', respectively.
		require
			Exists exists
		local
			l_length_1, l_length_2:REAL_32
		do
			l_length_1 := ground_anchor_1.distance (world_anchor_1)
			l_length_2 := ground_anchor_2.distance (world_anchor_2)
			{B2D_EXTERNAL}.b2pulleyjointdef_lengtha_set (item, l_length_1)
			{B2D_EXTERNAL}.b2pulleyjointdef_lengthb_set (item, l_length_2)
		end


	type_internal:NATURAL
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2joint_type_pulleyjoint
		end

	delete
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2pulleyjointdef_delete (item)
		end

invariant
	Segment_Length_1_Valid: exists implies {B2D_COMPARE_REAL}.compare_real_32 (segment_length_1, ground_anchor_1.distance (world_anchor_1))
	Segment_Length_2_Valid: exists implies {B2D_COMPARE_REAL}.compare_real_32 (segment_length_2, ground_anchor_2.distance (world_anchor_2))

end
