note
	description: "Structure that contains information to create a {B2D_JOINT_MOUSE}."
	author: "Patrick Boucher"
	date: "Sat, 27 Jun 2020 04:24:21 +0000"
	revision: "0.1"

class
	B2D_JOINT_MOUSE_DEFINITION

inherit
	B2D_JOINT_DEFINITION

create
	make

feature {NONE} -- Initialization

	make (a_body_1, a_body_2:B2D_BODY)
			-- Initialization of `Current' using `a_body_1' as `body_1' and
			-- `a_body_2' as `body_2'.
			-- Note that `a_body_1' is useless in a {B2D_JOINT_MOUSE} but
			-- must be provided to be conform to {B2D_JOINT}.
			-- Also, be carefull because `must_collide_connected' is `False'
			-- by default.
		require
			Body_1_exists: a_body_1.exists
			Body_2_exists: a_body_2.exists
			Bodies_Not_Same: a_body_1 /= a_body_2
		do
			own_from_item ({B2D_EXTERNAL}.b2mousejointdef_new)
			set_body_1 (a_body_1)
			set_body_2 (a_body_2)
		ensure
			Body_1_Assigned: body_1 ~ a_body_1
			Body_2_Assigned: body_2 ~ a_body_2
		end

feature -- Access

	target_anchor_position:B2D_VECTOR_2D assign set_target_anchor_position
			-- World target point that coincide with the {B2D_BODY} anchor.
		require
			Exists: exists
		do
			create Result.shared_from_item ({B2D_EXTERNAL}.b2mousejointdef_target_get (item), Current)
		end

	set_target_anchor_position (a_world_position:B2D_VECTOR_2D)
			-- Assign `target_anchor_position' with the value `a_world_position'.
			--
			-- `a_world_position`: The body's anchor world position.
		require
			Exists: exists
			Vector_Exists: a_world_position.exists
		do
			{B2D_EXTERNAL}.b2mousejointdef_target_set (item, a_world_position.item)
		ensure
			Is_Assigned: target_anchor_position ~ a_world_position
		end

	maximum_force:REAL_32 assign set_maximum_force
			-- Maximum constraint force that can be exerced to move the {B2D_BODY}.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2mousejointdef_maxforce_get (item)
		end

	set_maximum_force (a_force:REAL_32)
			-- Assign `maximum_force' with the value `a_force'.
			-- Usually express as some multiplier of the weight: multiplier * mass * gravity.
		require
			Exists: exists
			Non_Negative_Force: a_force >= 0
		do
			{B2D_EXTERNAL}.b2mousejointdef_maxforce_set (item, a_force)
		ensure
			Is_Assigned: maximum_force ~ a_force
		end

	set_maximum_force_with_weight_multiplier (a_body:B2D_BODY; a_multiplier:INTEGER_32)
				-- Assign `maximum_force' with the `a_body''s weight multiplied by `a_multiplier'.
		require
			Exists: exists
			Body_Exists: a_body.exists
			Non_Negative_Multiplier: a_multiplier >= 0
		local
			l_force:REAL_32
		do
			if a_multiplier > 0 then
				l_force := a_multiplier * a_body.mass * a_body.world.gravity.y.abs
				if l_force < 0 then
					l_force := 0
				end
			end
			set_maximum_force (l_force)
		end

	frequency:REAL_32 assign set_frequency
			-- The respond speed of a mouse movement in Hz.
			-- Can be used with `damping_ratio' to create a spring/damper effect.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2mousejointdef_frequencyhz_get (item)
		end

	set_frequency (a_frequency:REAL_32)
			-- Assign `frequency' with the value `a_frequency'.
		require
			Exists: exists
			Non_Negative_Frequency: a_frequency >= 0
		do
			{B2D_EXTERNAL}.b2mousejointdef_frequencyhz_set (item, a_frequency)
		ensure
			Is_Assigned: frequency ~ a_frequency
		end

	damping_ratio:REAL_32 assign set_damping_ratio
			-- The damping ratio.
			-- Can be used with `frequency' to create a spring/damper effect.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2mousejointdef_dampingratio_get (item)
		end

	set_damping_ratio (a_ratio:REAL_32)
			-- Assign `damping_ratio' with the value `a_ratio'.
			--
			--`a_ratio`: 0 = no damping, 1 = critical damping
		require
			Exists: exists
			Non_Negative_Ratio: a_ratio >= 0
		do
			{B2D_EXTERNAL}.b2mousejointdef_dampingratio_set (item, a_ratio)
		ensure
			Is_Assigned: damping_ratio ~ a_ratio
		end

feature {NONE} -- Implementation

	type_internal:NATURAL
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2joint_type_mousejoint
		end

	delete
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2mousejointdef_delete (item)
		end

invariant
	Non_Negative_Max_Force: exists implies maximum_force >= 0
	Non_Negative_Frequency: exists implies frequency >= 0
	Non_Negative_Damping_Ratio: exists implies damping_ratio >= 0

end
