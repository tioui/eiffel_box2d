note
	description: "Structure that contains information to create a {B2D_JOINT_PRISMATIC}."
	author: "Patrick Boucher"
	date: "Sun, 28 Jun 2020 23:46:28 +0000"
	revision: "0.1"

class
	B2D_JOINT_PRISMATIC_DEFINITION

inherit
	B2D_JOINT_GEARABLE_DEFINITION
	B2D_JOINT_DEFINITION_ANGLE
	B2D_JOINT_DEFINITION_AXIS

create
	make_with_vector,
	make,
	make_with_vector_and_angle,
	make_with_angle

feature {NONE} -- Initialization

	make(a_body_1, a_body_2:B2D_BODY; a_anchor_x, a_anchor_y, a_axis_x, a_axis_y:REAL_32)
			-- Initialization of `Current' using `a_body_1' as `body_1' and `a_body_2' as `body_2';
			-- computing `local_anchor_1', `local_anchor_2' from the world position
			-- `a_anchor_x`, `a_anchor_y` and computing `local_axis' and `reference_angle' from
			-- the direction translation `a_axis_x`, `a_axis_y`.
		require
			Body_1_Exists: a_body_1.exists
			Body_2_Exists: a_body_2.exists
			Bodies_Not_Same: a_body_1 /= a_body_2
		local
			l_anchor_vector, l_axis_vector:B2D_VECTOR_2D
		do
			create l_anchor_vector.make_with_coordinates (a_anchor_x, a_anchor_y)
			create l_axis_vector.make_with_coordinates (a_axis_x, a_axis_y)
			if l_anchor_vector.exists and l_axis_vector.exists then
				make_with_vector(a_body_1, a_body_2, l_anchor_vector, l_axis_vector)
			else
				body_1 := a_body_1
				body_2 := a_body_2
			end
		ensure
			Body_1_Assigned: body_1 ~ a_body_1
			Body_2_Assigned: body_2 ~ a_body_2
		end

	make_with_vector (a_body_1, a_body_2:B2D_BODY; a_anchor, a_axis:B2D_VECTOR_2D)
			-- Initialization of `Current' using `a_body_1' as `body_1' and `a_body_2' as `body_2';
			-- computing `local_anchor_1', `local_anchor_2' from the world position
			-- `a_anchor` and computing `local_axis' and `reference_angle' from
			-- the direction translation `a_axis`.
		require
			Body_1_Exists: a_body_1.exists
			Body_2_Exists: a_body_2.exists
			Anchor_Exists: a_anchor.exists
			Axis_Exists: a_axis.exists
		do
			own_from_item ({B2D_EXTERNAL}.b2prismaticjointdef_new)
			if exists then
				{B2D_EXTERNAL}.b2prismaticjointdef_initialize (item, a_body_1.item, a_body_2.item, a_anchor.item, a_axis.item)
			end
			body_1 := a_body_1
			body_2 := a_body_2
		ensure
			Body_1_Assigned: body_1 ~ a_body_1
			Body_2_Assigned: body_2 ~ a_body_2
		end

	make_with_vector_and_angle (a_body_1, a_body_2:B2D_BODY; a_anchor:B2D_VECTOR_2D; a_angle:REAL_32)
			-- Initialization of `Current' using `a_body_1' as `body_1' and `a_body_2' as `body_2';
			-- computing `local_anchor_1', `local_anchor_2' from the world position
			-- `a_anchor` and computing `local_axis' and `reference_angle' from
			-- the direction of the translation, in radians `a_angle`.
		require
			Body_1_Exists: a_body_1.exists
			Body_2_Exists: a_body_2.exists
			Anchor_Exists: a_anchor.exists
		do
			make_with_vector (a_body_1, a_body_2, a_anchor, convert_angle_to_vector (a_angle))
		ensure
			Body_1_Assigned: body_1 ~ a_body_1
			Body_2_Assigned: body_2 ~ a_body_2
		end

	make_with_angle (a_body_1, a_body_2:B2D_BODY; a_anchor_x, a_anchor_y, a_angle:REAL_32)
			-- Initialization of `Current' using `a_body_1' as `body_1' and `a_body_2' as `body_2';
			-- computing `local_anchor_1', `local_anchor_2' from the world position
			-- `a_anchor_x`, `a_anchor_y` and computing `local_axis' and `reference_angle' from
			-- the direction of the translation, in radians `a_angle`.
		require
			Body_1_Exists: a_body_1.exists
			Body_2_Exists: a_body_2.exists
		local
			l_anchor_vector:B2D_VECTOR_2D
		do
			create l_anchor_vector.make_with_coordinates (a_anchor_x, a_anchor_y)
			if l_anchor_vector.exists then
				make_with_vector_and_angle(a_body_1, a_body_2, l_anchor_vector, a_angle)
			else
				body_1 := a_body_1
				body_2 := a_body_2
			end
		end

feature -- Access

	set_anchor_with_vector (a_anchor:B2D_VECTOR_2D)
			-- <Precursor>
		local
			l_local_anchor_1, l_local_anchor_2:B2D_VECTOR_2D
		do
			l_local_anchor_1 := body_1.world_point_to_local_coordinates_with_vector (a_anchor)
			{B2D_EXTERNAL}.b2prismaticjointdef_localanchora_set (item, l_local_anchor_1.item)
			l_local_anchor_2 := body_2.world_point_to_local_coordinates_with_vector (a_anchor)
			{B2D_EXTERNAL}.b2prismaticjointdef_localanchorb_set (item, l_local_anchor_2.item)
		end

	local_anchor_1:B2D_VECTOR_2D
			-- <Precursor>
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2prismaticjointdef_localanchora_get (item))
		end

	local_anchor_2:B2D_VECTOR_2D
			-- <Precursor>
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2prismaticjointdef_localanchorb_get (item))
		end

	local_axis:B2D_VECTOR_2D
			-- <Precursor>
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2prismaticjointdef_localaxisa_get (item))
		end

	set_local_axis_with_vector (a_world_axis:B2D_VECTOR_2D)
			-- <Precursor>
		local
			l_rotated:B2D_VECTOR_2D
		do
			l_rotated := body_1.relative_local_point_with_vector (a_world_axis)
			{B2D_EXTERNAL}.b2prismaticjointdef_localaxisa_set (item, l_rotated.item)
		end

	reference_angle:REAL_32
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2prismaticjointdef_referenceangle_get (item)
		end

	update_reference_angle
			-- <Precursor>
		local
			l_angle:REAL_32
		do
			l_angle := body_2.angle - body_1.angle
			{B2D_EXTERNAL}.b2prismaticjointdef_referenceangle_set (item, l_angle)
		end

	is_limit_enabled:BOOLEAN assign set_is_limit_enabled
			-- Is the translation of `Current' limited ?
		do
			Result := {B2D_EXTERNAL}.b2prismaticjointdef_enablelimit_get (item)
		end

	set_is_limit_enabled (a_value:BOOLEAN)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2prismaticjointdef_enablelimit_set (item, a_value)
		end

	lower_limit:REAL_32 assign set_lower_limit
			-- The lower translation limit.
		do
			Result := {B2D_EXTERNAL}.b2prismaticjointdef_lowertranslation_get (item)
		end

	set_lower_limit (a_limit:REAL_32)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2prismaticjointdef_lowertranslation_set (item, a_limit)
		end

	upper_limit:REAL_32 assign set_upper_limit
			-- The upper translation limit.
		do
			Result := {B2D_EXTERNAL}.b2prismaticjointdef_uppertranslation_get (item)
		end

	set_upper_limit (a_limit:REAL_32)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2prismaticjointdef_uppertranslation_set (item, a_limit)
		end

	is_motor_enabled:BOOLEAN assign set_is_motor_enabled
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2prismaticjointdef_enablemotor_get (item)
		end

	set_is_motor_enabled (a_value:BOOLEAN)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2prismaticjointdef_enablemotor_set (item, a_value)
		end




	maximum_motor_force:REAL_32
			-- The maximum motor force in N.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2prismaticjointdef_maxmotorforce_get (item)
		end

	set_maximum_motor_force (a_value:REAL_32)
			-- Assign `maximum_motor_force' with the value `a_value'.
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2prismaticjointdef_maxmotorforce_set (item, a_value)
		ensure
			Is_Assign: maximum_motor_force ~ a_value
		end

	motor_speed:REAL_32 assign set_motor_speed
			-- Translation speed when powered, in m/s.
		do
			Result := {B2D_EXTERNAL}.b2prismaticjointdef_motorspeed_get (item)
		end

	set_motor_speed (a_value:REAL_32)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2prismaticjointdef_motorspeed_set (item, a_value)
		end

feature {NONE} -- Implementation

	type_internal:NATURAL
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2joint_type_prismaticjoint
		end

	delete
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2prismaticjointdef_delete (item)
		end

invariant
	Limits_Valid: exists implies lower_limit <= upper_limit

end
