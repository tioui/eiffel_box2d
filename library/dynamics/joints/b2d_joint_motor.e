note
	description: "[
					Used to control the relative motion between two {B2D_BODY}. 
					A typical usage is to control the movement of a dynamic body
					with respect to the ground.
				]"
	author: "Louis Marchand"
	date: "Mon, 21 Feb 2022 15:17:30 +0000"
	revision: "0.2"

class
	B2D_JOINT_MOTOR

inherit
	B2D_JOINT

create {B2D_ANY}
	make_from_item

feature {NONE} -- Implementation

	anchor_1_internal(a_vector:POINTER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2MotorJoint_GetAnchorA(item, a_vector)
		end

	anchor_2_internal(a_vector:POINTER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2MotorJoint_GetAnchorB(item, a_vector)
		end

	reaction_force_internal(a_inverse_time_step:REAL_32; a_vector:POINTER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2MotorJoint_GetReactionForce(item, a_vector, a_inverse_time_step)
		end

	type_internal:NATURAL
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2Joint_type_motorJoint
		end

	reaction_torque_internal(a_inverse_time_step:REAL_32):REAL_32
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2MotorJoint_GetReactionTorque(item, a_inverse_time_step)
		end

end
