note
	description: "[
			Common ancestor for {B2D_JOINT_DEFINITION} or {B2D_JOINT} that
			have an upper and lower limit.
		]"
	author: "Patrick Boucher and Louis Marchand"
	date: "Fri, 18 Feb 2022 16:21:15 +0000"
	revision: "0.1"

deferred class
	B2D_JOINT_LIMITED

inherit
	B2D_MEMORY_OBJECT

feature -- Access

	is_limit_enabled:BOOLEAN assign set_is_limit_enabled
			-- Is the angle of `Current' limited ?
		require
			Exists: exists
		deferred
		end

	set_is_limit_enabled (a_value:BOOLEAN)
			-- Assign `has_limit' with `a_value'.
		require
			Exists: exists
		deferred
		ensure
			Is_Assigned: is_limit_enabled ~ a_value
		end

	enable_limit
			-- Enable the limit.
		require
			Exists: exists
		do
			set_is_limit_enabled (True)
		ensure
			Has_Limit: is_limit_enabled
		end

	disable_limit
			-- Disable the limit.
		require
			Exists: exists
		do
			set_is_limit_enabled (False)
		ensure
			Has_No_Limit: not is_limit_enabled
		end

	lower_limit:REAL_32
			-- The lowest limit that `Current' can reach.
		require
			Exists: exists
		deferred
		end

	set_lower_limit (a_value:REAL_32)
			-- Assign `lower_limit' with the value `a_value'.
		require
			Exists: exists
			Limit_Valid: a_value <= upper_limit
		deferred
		ensure
			Is_Assigned: lower_limit ~ a_value
		end

	upper_limit:REAL_32
			-- The highest limit that `Current' can reach.
		require
			Exists: exists
		deferred
		end

	set_upper_limit (a_value:REAL_32)
			-- Assign `upper_limit' with the value `a_value'.
			-- It is better to reduce `lower_limit' first.
			-- Use `set_limits' to set both limits.
		require
			Exists: exists
			Limit_Valid: a_value >= lower_limit
		deferred
		ensure
			Is_Assigned: upper_limit ~ a_value
		end

	limits:TUPLE[lower_limit, upper_limit:REAL_32]
			-- The angle limit allowed in `Current' (in radian)
		require
			Exists: exists
		do
			Result := [lower_limit, upper_limit]
		end

	set_limits (a_lower, a_upper:REAL_32)
			-- Assign `lower_limit' and `upper_limit' with the values `a_lower' and `a_upper'.
		require
			Exists: exists
			Limits_Valid: a_lower <= a_upper
		deferred
		ensure
			Lower_Limit_Assigned: limits.lower_limit ~ a_lower
			Upper_Limit_Assigned: limits.upper_limit ~ a_upper
		end


invariant
	Is_Limits_Valid: exists implies lower_limit <= upper_limit and limits.lower_limit <= limits.upper_limit

end
