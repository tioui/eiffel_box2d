note
	description: "Structure used to create a {B2D_JOINT_ROPE}."
	author: "Patrick Boucher"
	date: "Fri, 10 Jul 2020 05:53:26 +0000"
	revision: "0.1"

class
	B2D_JOINT_ROPE_DEFINITION

inherit
	B2D_JOINT_DEFINITION

create
	make,
	make_with_vector

feature {NONE} -- Initialization

	make (a_body_1, a_body_2:B2D_BODY; a_local_anchor_1_x, a_local_anchor_1_y,
				a_local_anchor_2_x, a_local_anchor_2_y, a_max_length:REAL_32)
			-- Initialization of `Current' using `a_body_1' as `body_1', `a_body_2' as `body_2' and
			-- `a_max_length' as `max_length'.
			-- Assign `local_anchor_1' with the values `a_local_anchor_1_x' and `a_local_anchor_1_y'.
			-- Assign `local_anchor_2' with the values `a_local_anchor_2_x' and `a_local_anchor_2_y'.
		require
			Bodies_Exists: a_body_1.exists and a_body_2.exists
			Bodies_Not_Same: a_body_1 /= a_body_2
			Max_Length_Minimum_Value: a_max_length > 0.005
			Max_Length_Is_At_Least_The_Distance_Between_Bodies: a_max_length >= a_body_1.position.distance (a_body_2.position)
		local
			l_anchor_1, l_anchor_2:B2D_VECTOR_2D
		do
			create l_anchor_1.make_with_coordinates (a_local_anchor_1_x, a_local_anchor_1_y)
			create l_anchor_2.make_with_coordinates (a_local_anchor_2_x, a_local_anchor_2_y)
			make_with_vector (a_body_1, a_body_2, l_anchor_1, l_anchor_2, a_max_length)
		ensure
			Body_1_Assigned: body_1 ~ a_body_1
			Body_2_Assigne: body_2 ~ a_body_2
			Local_Anchor_1_Assigned: local_anchor_1.x ~ a_local_anchor_1_x and local_anchor_1.y ~ a_local_anchor_1_y
			Local_Anchor_2_Assigned: local_anchor_2.x ~ a_local_anchor_2_x and local_anchor_2.y ~ a_local_anchor_2_y
			Max_Length_Assigned: max_length ~ a_max_length
		end

	make_with_vector (a_body_1, a_body_2:B2D_BODY; a_local_anchor_1, a_local_anchor_2:B2D_VECTOR_2D; a_max_length:REAL_32)
			-- Initialization of `Current' using `a_body_1' as `body_1', `a_body_2' as `body_2',
			-- `a_local_anchor_1' as `local_anchor_1', `a_local_anchor_2' as `local_anchor_2' and
			-- `a_max_length' as `max_length'.
		require
			Bodies_Exist: a_body_1.exists and a_body_2.exists
			Vectors_Exist: a_local_anchor_1.exists and a_local_anchor_2.exists
			Max_Length_Minimum_Value: a_max_length > 0.005
			Max_Length_Is_At_Least_The_Distance_Between_Bodies: a_max_length >= a_body_1.position.distance (a_body_2.position)
		do
			own_from_item ({B2D_EXTERNAL}.b2ropejointdef_new)
			check exists end
			set_body_1 (a_body_1)
			set_body_2 (a_body_2)
			set_local_anchor_1 (a_local_anchor_1)
			set_local_anchor_2 (a_local_anchor_2)
			set_max_length (a_max_length)
		ensure
			Body_1_Assigned: body_1 ~ a_body_1
			Body_2_Assigne: body_2 ~ a_body_2
			Local_Anchor_1_Assigned: local_anchor_1 ~ a_local_anchor_1
			Local_Anchor_2_Assigned: local_anchor_2 ~ a_local_anchor_2
			Max_Length_Assigned: max_length ~ a_max_length
		end

feature -- Access


	local_anchor_1:B2D_VECTOR_2D assign set_local_anchor_1
			-- The local anchor point relative to the origin of `body_1'.
		require
			Exists: exists
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2ropejointdef_localanchora_get (item))
		end

	set_local_anchor_1 (a_local_point:B2D_VECTOR_2D)
			-- Assign `local_anchor_1' with the value `a_local_point'.
		require
			Exists: exists
			Vector_Exists: a_local_point.exists
		do
			{B2D_EXTERNAL}.b2ropejointdef_localanchora_set (item, a_local_point.item)
		ensure
			Is_Assigned: local_anchor_1 ~ a_local_point
		end

	local_anchor_2:B2D_VECTOR_2D assign set_local_anchor_2
			-- The local anchor point relative to the origin of `body_2'.
		require
			Exists: exists
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2ropejointdef_localanchorb_get (item))
		end

	set_local_anchor_2 (a_local_point:B2D_VECTOR_2D)
			-- Assign `local_anchor_2' with the value `a_local_point'.
		require
			Exists: exists
			Vector_Exists: a_local_point.exists
		do
			{B2D_EXTERNAL}.b2ropejointdef_localanchorb_set (item, a_local_point.item)
		ensure
			Is_Assigned: local_anchor_2 ~ a_local_point
		end

	max_length:REAL_32 assign set_max_length
			-- The maximum length of the rope.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2ropejointdef_maxlength_get (item)
		end

	set_max_length (a_value:REAL_32)
			-- Assign `max_length' with `a_value'.
		require
			Exists: exists
			Minimum_Value: a_value > 0.005
			Is_At_Least_The_Distance_Between_Bodies: a_value >= body_1.position.distance (body_2.position)
		do
			{B2D_EXTERNAL}.b2ropejointdef_maxlength_set (item, a_value)
		ensure
			Is_Assigned: max_length ~ a_value
		end

feature {NONE} -- Implementation

	type_internal:NATURAL
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2joint_type_ropejoint
		end

	delete
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2ropejointdef_delete (item)
		end

end
