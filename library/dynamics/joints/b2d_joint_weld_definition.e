note
	description: "Structure used to create a {B2D_JOINT_WELD}."
	author: "Patrick Boucher"
	date: "Fri, 10 Jul 2020 20:34:44 +0000"
	revision: "0.1"

class
	B2D_JOINT_WELD_DEFINITION

inherit
	B2D_JOINT_DEFINITION_UNIQUE_ANCHOR
	B2D_JOINT_DEFINITION_ANGLE
	B2D_JOINT_DEFINITION_DAMPING

create
	make,
	make_with_vector

feature {NONE} -- Initialization

	make (a_body_1, a_body_2:B2D_BODY; a_anchor_x, a_anchor_y:REAL_32)
			-- Initialization of `Current' using `a_body_1'  as `body_1' and `a_body_2' as `body_2'.
			-- Assign `local_anchor_1' and `local_anchor_2' with the world point `a_anchor_x' and `a_anchor_y' relatively to the
			-- position of `body_1' and `body_2'.
		require
			Body_1_Exists: a_body_1.exists
			Body_2_Exists: a_body_2.exists
			Bodies_Not_Same: a_body_1 /= a_body_2
		local
			l_anchor:B2D_VECTOR_2D
		do
			create l_anchor.make_with_coordinates (a_anchor_x, a_anchor_y)
			check l_anchor.exists then
				make_with_vector (a_body_1, a_body_2, l_anchor)
			end
		ensure
			Body_1_Assigned: body_1 ~ a_body_1
			Body_2_Assigned: body_2 ~ a_body_2
			Anchor_Assigned: local_anchor_1.x ~ body_1.world_point_to_local_coordinates (a_anchor_x, a_anchor_y).x and
							 local_anchor_1.y ~ body_1.world_point_to_local_coordinates (a_anchor_x, a_anchor_y).y and
							 local_anchor_2.x ~ body_2.world_point_to_local_coordinates (a_anchor_x, a_anchor_y).x and
							 local_anchor_2.y ~ body_2.world_point_to_local_coordinates (a_anchor_x, a_anchor_y).y
		end

	make_with_vector (a_body_1, a_body_2:B2D_BODY; a_anchor:B2D_VECTOR_2D)
			-- Initialization of `Current' using `a_body_1'  as `body_1' and `a_body_2' as `body_2'.
			-- Assign `local_anchor_1' and `local_anchor_2' with the world point `a_anchor' relatively to the
			-- position of `body_1' and `body_2'.
		require
			Body_1_Exists: a_body_1.exists
			Body_2_Exists: a_body_2.exists
			Anchor_Exists: a_anchor.exists
			Bodies_Not_Same: a_body_1 /= a_body_2
		do
			own_from_item ({B2D_EXTERNAL}.b2weldjointdef_new)
			check exists then
				{B2D_EXTERNAL}.b2weldjointdef_initialize (item, a_body_1.item, a_body_2.item, a_anchor.item)
			end
			body_1 := a_body_1
			body_2 := a_body_2
		ensure
			Body_1_Assigned: body_1 ~ a_body_1
			Body_2_Assigned: body_2 ~ a_body_2
			Anchor_Assigned: local_anchor_1 ~ body_1.world_point_to_local_coordinates_with_vector (a_anchor)
							 local_anchor_2 ~ body_2.world_point_to_local_coordinates_with_vector (a_anchor)
		end

feature -- Access

	local_anchor_1:B2D_VECTOR_2D
			-- <Precursor>
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2weldjointdef_localanchora_get (item))
		end

	local_anchor_2:B2D_VECTOR_2D
			-- <Precursor>
		do
			create Result.make_from_item ({B2D_EXTERNAL}.b2weldjointdef_localanchorb_get (item))
		end

	set_anchor_with_vector (a_world_point: B2D_VECTOR_2D)
			-- <Precursor>
		local
			l_anchor_1, l_anchor_2:B2D_VECTOR_2D
		do
			l_anchor_1 := body_1.world_point_to_local_coordinates_with_vector (a_world_point)
			l_anchor_2 := body_2.world_point_to_local_coordinates_with_vector (a_world_point)
			{B2D_EXTERNAL}.b2weldjointdef_localanchora_set (item, l_anchor_1.item)
			{B2D_EXTERNAL}.b2weldjointdef_localanchorb_set (item, l_anchor_2.item)
		end

	reference_angle:REAL_32
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2weldjointdef_referenceangle_get (item)
		end

	update_reference_angle
			-- <Precursor>
		local
			l_reference:REAL_32
		do
			l_reference := body_2.angle - body_1.angle
			{B2D_EXTERNAL}.b2weldjointdef_referenceangle_set (item, l_reference)
		end

	frequency:REAL_32 assign set_frequency
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2weldjointdef_frequencyHz_get(item)
		end

	set_frequency(a_value:REAL_32)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2weldjointdef_frequencyHz_set(item, a_value)
		end

	damping_ratio:REAL_32 assign set_damping_ratio
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2weldjointdef_dampingRatio_get(item)
		end

	set_damping_ratio(a_value:REAL_32)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2weldjointdef_dampingRatio_set(item, a_value)
		end

feature {NONE} -- Implementation

	type_internal:NATURAL
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2joint_type_weldjoint
		end

	delete
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2weldjointdef_delete (item)
		end

end
