note
	description: "Use to create {B2D_JOINT_DISTANCE}"
	author: "Louis Marchand"
	date: "Tue, 23 Jun 2020 14:41:24 +0000"
	revision: "0.1"

class
	B2D_JOINT_DISTANCE_DEFINITION

inherit
	B2D_JOINT_DEFINITION
	B2D_JOINT_DEFINITION_DAMPING

create
	make,
	make_with_vector

feature {NONE} -- Initialisation

	make_with_vector(a_body_1, a_body_2:B2D_BODY; a_anchor_1, a_anchor_2:B2D_VECTOR_2D)
			-- Initialisation of `Current' creating keeping `a_anchor_1' of
			-- `a_body_1' at a certain distance from `a_anchor_2' of `a_body_2'.
			-- Note: Anchors are given in World coordinates.
		require
			Body_1_Exists: a_body_1.exists
			Body_2_Exists: a_body_2.exists
			Anchor_1_Exists: a_anchor_1.exists
			Anchor_2_Exists: a_anchor_2.exists
			Bodies_Not_Same: a_body_1 /= a_body_2
		do
			own_from_item ({B2D_EXTERNAL}.b2DistanceJointDef_new)
			if exists then
				{B2D_EXTERNAL}.b2DistanceJointDef_Initialize(item, a_body_1.item, a_body_2.item, a_anchor_1.item, a_anchor_2.item)
			end
			body_1 := a_body_1
			body_2 := a_body_2
		ensure
			Body_1_Assign: body_1 ~ a_body_1
			Body_1_Assign: body_2 ~ a_body_2
			Anchor_1_Assign: local_anchor_1 ~ body_1.world_point_to_local_coordinates_with_vector (a_anchor_1)
			Anchor_2_Assign: local_anchor_2 ~ body_2.world_point_to_local_coordinates_with_vector (a_anchor_2)
		end

	make(a_body_1, a_body_2:B2D_BODY; a_anchor_1_x, a_anchor_1_y, a_anchor_2_x, a_anchor_2_y:REAL_32)
			-- Initialisation of `Current' creating keeping anchor at (`a_anchor_1_x', `a_anchor_1_y') of
			-- `a_body_1' at a certain distance from (`a_anchor_2_x', `a_anchor_2_y') of `a_body_2'.
			-- Note: Anchors are given in World coordinates.
		require
			Body_1_Exists: a_body_1.exists
			Body_2_Exists: a_body_2.exists
		local
			l_anchor_1, l_anchor_2:B2D_VECTOR_2D
		do
			create l_anchor_1.make_with_coordinates (a_anchor_1_x, a_anchor_1_y)
			create l_anchor_2.make_with_coordinates (a_anchor_2_x, a_anchor_2_y)
			if l_anchor_1.exists and l_anchor_2.exists then
				make_with_vector (a_body_1, a_body_2, l_anchor_1, l_anchor_2)
			else
				body_1 := a_body_1
				body_2 := a_body_2
			end
		ensure
			Body_1_Assign: body_1 ~ a_body_1
			Body_1_Assign: body_2 ~ a_body_2
			Anchor_1_Assign: local_anchor_1 ~ body_1.world_point_to_local_coordinates_with_vector (create {B2D_VECTOR_2D}.make_with_coordinates (a_anchor_1_x, a_anchor_1_y))
			Anchor_2_Assign: local_anchor_2 ~ body_2.world_point_to_local_coordinates_with_vector (create {B2D_VECTOR_2D}.make_with_coordinates (a_anchor_2_x, a_anchor_2_y))
		end

feature -- Access

	local_anchor_1:B2D_VECTOR_2D
			-- The local point in `body_1' that `Current' is attached to.
		require
			Exists: exists
		do
			create Result.shared_from_item ({B2D_EXTERNAL}.b2DistanceJointDef_localAnchorA_get(item), Current)
		end

	set_local_anchor_1_with_vector(a_anchor:B2D_VECTOR_2D)
			-- Assign `local_anchor_1' with the value of `a_anchor'.
		require
			Exists: exists
			Local_Anchor_Exists: local_anchor_1.exists
			Anchor_Exists: a_anchor.exists
		do
			local_anchor_1.set_coordinates (a_anchor.x, a_anchor.y)
		ensure
			Is_Assign: local_anchor_1 ~ a_anchor
		end

	set_local_anchor_1(a_anchor_x, a_anchor_y:REAL_32)
			-- Assign `local_anchor_1' with the values `a_anchor_x' and `a_anchor_y'.
		require
			Exists: exists
			Local_Anchor_Exists: local_anchor_1.exists
		do
			local_anchor_1.set_coordinates (a_anchor_x, a_anchor_y)
		ensure
			Is_Assign_X: local_anchor_1.x ~ a_anchor_x
			Is_Assign_Y: local_anchor_1.y ~ a_anchor_y
		end

	set_world_anchor_1_with_vector(a_anchor:B2D_VECTOR_2D)
			-- Assign `local_anchor_1' with the value World point coordinates `a_anchor'.
		require
			Exists: exists
			Local_Anchor_Exists: local_anchor_1.exists
			Anchor_Exists: a_anchor.exists
		local
			l_vector:B2D_VECTOR_2D
		do
			l_vector := body_1.world_point_to_local_coordinates_with_vector (a_anchor)
			local_anchor_1.set_coordinates (l_vector.x, l_vector.y)
		ensure
			Is_Assign: local_anchor_1 ~ body_1.world_point_to_local_coordinates_with_vector (a_anchor)
		end

	set_world_anchor_1(a_anchor_x, a_anchor_y:REAL_32)
			-- Assign `local_anchor_1' with the value World point coordinates `a_anchor_x', `a_anchor_y'.
		require
			Exists: exists
			Local_Anchor_Exists: local_anchor_1.exists
		local
			l_vector:B2D_VECTOR_2D
		do
			l_vector := body_1.world_point_to_local_coordinates_with_vector (create {B2D_VECTOR_2D}.make_with_coordinates (a_anchor_x, a_anchor_y))
			local_anchor_1.set_coordinates (l_vector.x, l_vector.y)
		ensure
			Anchor_1_Assign: local_anchor_1 ~ body_1.world_point_to_local_coordinates_with_vector (create {B2D_VECTOR_2D}.make_with_coordinates (a_anchor_x, a_anchor_y))
		end

	local_anchor_2:B2D_VECTOR_2D
			-- The local point in `body_2' that `Current' is attached to.
		require
			Exists: exists
		do
			create Result.shared_from_item ({B2D_EXTERNAL}.b2DistanceJointDef_localAnchorB_get(item), Current)
		end

	set_local_anchor_2_with_vector(a_anchor:B2D_VECTOR_2D)
			-- Assign `local_anchor_1' with the value of `a_anchor'.
		require
			Exists: exists
			Local_Anchor_Exists: local_anchor_2.exists
			Anchor_Exists: a_anchor.exists
		do
			local_anchor_2.set_coordinates (a_anchor.x, a_anchor.y)
		ensure
			Is_Assign: local_anchor_2 ~ a_anchor
		end

	set_local_anchor_2(a_anchor_x, a_anchor_y:REAL_32)
			-- Assign `local_anchor_2' with the values `a_anchor_x' and `a_anchor_y'.
		require
			Exists: exists
			Local_Anchor_Exists: local_anchor_2.exists
		do
			local_anchor_2.set_coordinates (a_anchor_x, a_anchor_y)
		ensure
			Is_Assign_X: local_anchor_2.x ~ a_anchor_x
			Is_Assign_Y: local_anchor_2.y ~ a_anchor_y
		end

	set_world_anchor_2_with_vector(a_anchor:B2D_VECTOR_2D)
			-- Assign `local_anchor_2' with the value World point coordinates `a_anchor'.
		require
			Exists: exists
			Local_Anchor_Exists: local_anchor_2.exists
			Anchor_Exists: a_anchor.exists
		local
			l_vector:B2D_VECTOR_2D
		do
			l_vector := body_2.world_point_to_local_coordinates_with_vector (a_anchor)
			local_anchor_2.set_coordinates (l_vector.x, l_vector.y)
		ensure
			Is_Assign: local_anchor_2 ~ body_2.world_point_to_local_coordinates_with_vector (a_anchor)
		end

	set_world_anchor_2(a_anchor_x, a_anchor_y:REAL_32)
			-- Assign `local_anchor_2' with the value World point coordinates `a_anchor_x', `a_anchor_y'.
		require
			Exists: exists
			Local_Anchor_Exists: local_anchor_2.exists
		local
			l_vector:B2D_VECTOR_2D
		do
			l_vector := body_2.world_point_to_local_coordinates_with_vector (create {B2D_VECTOR_2D}.make_with_coordinates (a_anchor_x, a_anchor_y))
			local_anchor_2.set_coordinates (l_vector.x, l_vector.y)
		ensure
			Anchor_2_Assign: local_anchor_2 ~ body_2.world_point_to_local_coordinates_with_vector (create {B2D_VECTOR_2D}.make_with_coordinates (a_anchor_x, a_anchor_y))
		end

	length:REAL_32 assign set_length
			-- The natural distance between the `local_anchor_1' and `local_anchor_2'.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2DistanceJointDef_length_get(item)
		end

	set_length(a_length:REAL_32)
			-- Assign `length' with the value of `a_length'
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2DistanceJointDef_length_set(item, a_length)
		ensure
			Is_Assign: length ~ a_length
		end

	frequency:REAL_32 assign set_frequency
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2DistanceJointDef_frequencyHz_get(item)
		end

	set_frequency(a_value:REAL_32)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2DistanceJointDef_frequencyHz_set(item, a_value)
		end

	damping_ratio:REAL_32 assign set_damping_ratio
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2DistanceJointDef_dampingRatio_get(item)
		end

	set_damping_ratio(a_value:REAL_32)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2DistanceJointDef_dampingRatio_set(item, a_value)
		end

feature {NONE} -- Implementation

	type_internal:NATURAL
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2joint_type_distancejoint
		end

	delete
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2DistanceJointDef_delete(item)
		end

end
