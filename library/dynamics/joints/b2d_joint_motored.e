note
	description: "[
			Common ancestor for {B2D_JOINT_DEFINITION} or {B2D_JOINT} that
			have a motor.
		]"
	author: "Patrick Boucher and Louis Marchand"
	date: "Fri, 18 Feb 2022 16:21:15 +0000"
	revision: "0.1"

deferred class
	B2D_JOINT_MOTORED

inherit
	B2D_MEMORY_OBJECT

feature -- Access

	is_motor_enabled:BOOLEAN
			-- Is `Current' powered?
		require
			Exists: exists
		deferred
		end

	set_is_motor_enabled (a_value:BOOLEAN)
			-- Assign `is_motor_enabled' with the value `a_value'.
		require
			Exists: exists
		deferred
		ensure
			Is_Assigned: is_motor_enabled ~ a_value
		end

	enable_motor
			-- Make `Current' powered.
		require
			Exists: exists
		do
			set_is_motor_enabled (True)
		ensure
			Motor_Is_Enabled: is_motor_enabled
		end

	disable_motor
			-- Make `Current' unpowered.
		require
			Exists: exists
		do
			set_is_motor_enabled (False)
		ensure
			Motor_Is_Disabled: not is_motor_enabled
		end

	motor_speed:REAL_32
			-- Motor speed when powered.
		require
			Exists: exists
		deferred
		end

	set_motor_speed (a_value:REAL_32)
			-- Assign `motor_speed' with the value `a_value'.
		require
			Exists: exists
			Non_Negative_Speed: a_value >= 0.0
		deferred
		ensure
			Is_Assigned: motor_speed ~ a_value
		end


end
