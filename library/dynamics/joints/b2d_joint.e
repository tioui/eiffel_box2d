note
	description: "Used to constraint two bodies together in various fashions."
	author: "Louis Marchand"
	date: "Mon, 21 Feb 2022 15:17:30 +0000"
	revision: "0.2"

deferred class
	B2D_JOINT

inherit
	B2D_MEMORY_OBJECT


feature {NONE} -- Initialisation

	make_from_item(a_item:POINTER; a_body_1, a_body_2:B2D_BODY)
			-- Initialisation of `Current' between `a_body_1' and
			-- `a_body_2'
		require
			Body_1_Exists: a_body_1.exists
			Body_2_Exists: a_body_2.exists
		do
			own_from_item(a_item)
			body_1 := a_body_1
			body_2 := a_body_2
		ensure
			Body_1_Assign: body_1 ~ a_body_1
			Body_2_Assign: body_2 ~ a_body_2
		end

feature -- Access

	body_1:B2D_BODY
			-- The first {B2D_BODY} that is connected to `Current'

	body_2:B2D_BODY
			-- The second {B2D_BODY} that is connected to `Current'

	anchor_1:B2D_VECTOR_2D
			-- The anchor of `body_1' in world coorinates
		require
			Exists: exists
		do
			create Result
			if Result.exists then
				anchor_1_internal(Result.item)
			end
		end

	anchor_2:B2D_VECTOR_2D
			-- The anchor of `body_2' in world coorinates
		require
			Exists: exists
		do
			create Result
			if Result.exists then
				anchor_2_internal(Result.item)
			end
		end

	is_active:BOOLEAN
			-- `body_1' and `body_2' are active bodies
		do
			Result := body_1.is_active and body_2.is_active
		end

	reaction_force(a_inverse_time_step:REAL_32):B2D_VECTOR_2D
			-- The reaction force on body_2 at the joint anchor in Newtons.
		require
			Exists: exists
		do
			create Result
			if Result.exists then
				reaction_force_internal(a_inverse_time_step, Result.item)
			end
		end

	reaction_torque(a_inverse_time_step:REAL_32):REAL_32
			-- The reaction torque on `body_2' in N*m
		require
			Exists: exists
		do
			Result := reaction_torque_internal(a_inverse_time_step)
		end

	is_collide_connected:BOOLEAN
			-- `body_1' and `body_2' has collide
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2Joint_GetCollideConnected(item)
		end

feature {NONE} -- Implementation

	anchor_1_internal(a_vector:POINTER)
			-- Internal value of `anchor_1'
		require
			Exists: exists
			Vector_Exists: not a_vector.is_default_pointer
		deferred
		end

	anchor_2_internal(a_vector:POINTER)
			-- Internal value of `anchor_2'
		require
			Exists: exists
			Vector_Exists: not a_vector.is_default_pointer
		deferred
		end

	reaction_force_internal(a_inverse_time_step:REAL_32; a_vector:POINTER)
			-- Internal value of `reaction_force'
		require
			Exists: exists
			Vector_Exists: not a_vector.is_default_pointer
		deferred
		end

	reaction_torque_internal(a_inverse_time_step:REAL_32):REAL_32
			-- Internal value of `reaction_torque'
		require
			Exists: exists
		deferred
		end

	type_internal:NATURAL
			-- Internal type of `Current'
		deferred
		end

	delete
			-- <Precursor>
		do
			check Should_Not_Be_Deleted: False end
		end

invariant
	Body_1_Valid: (exists and body_1.exists) implies {B2D_EXTERNAL}.b2Joint_GetBodyA(item) ~ body_1.item
	Body_2_Valid: (exists and body_2.exists) implies {B2D_EXTERNAL}.b2Joint_GetBodyB(item) ~ body_2.item
	Type_Valid: exists implies {B2D_EXTERNAL}.b2Joint_getType(item) ~ type_internal
end
