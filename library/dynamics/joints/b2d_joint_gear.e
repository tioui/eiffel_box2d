note
	description: "[
					Used to connect two {BD_JOINT} together.
					Either joint can be a {B2D_JOINT_REVOLUTE} or {B2D_JOINT_PRISMATIC}.
					You specify a gear ratio to bind the motions together:
					coordinate1 + ratio * coordinate2 = constant .
					The ratio can be negative or positive. 
					If one joint is a {B2D_JOINT_REVOLUTE} and the other joint is a {B2D_JOINT_PRISMATIC},
					then the ratio will have units of length or units of 1/length.
				]"
	author: "Louis Marchand"
	date: "Mon, 21 Feb 2022 15:17:30 +0000"
	revision: "0.2"

class
	B2D_JOINT_GEAR

inherit
	B2D_JOINT

create {B2D_ANY}
	make

feature {NONE} -- Initialisation

	make(a_item:POINTER; a_joint_1, a_joint_2:B2D_JOINT_GEARABLE)
			-- Initialisation of `Current' between `a_joint_1' and `a_joint_2'
		require
			Joint_1_Exists: a_joint_1.exists
			Joint_2_Exists: a_joint_2.exists
		do
			own_from_item(a_item)
			body_1 := a_joint_1.body_2
			body_2 := a_joint_2.body_2
			joint_1 := a_joint_1
			joint_2 := a_joint_2
		ensure
			Is_Body_1_Valid: body_1 ~a_joint_1.body_2
			Is_Body_2_Valid: body_2 ~a_joint_2.body_2
			Is_Assigned_joint_1: joint_1 ~ a_joint_1
			Is_Assigned_joint_2: joint_2 ~ a_joint_2
		end

feature -- Access

	joint_1:B2D_JOINT_GEARABLE
			-- The first {B2D_JOINT_GEARABLE} that will be attached to `Current'.

	joint_2:B2D_JOINT_GEARABLE
			-- The first {B2D_JOINT_GEARABLE} that will be attached to `Current'.

	ratio:REAL_32 assign set_ratio
			-- The ratio of the {B2D_JOINT_GEAR}.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2GearJoint_GetRatio (item)
		end

	set_ratio (a_value:REAL_32)
			-- Assign `ratio' with the value `a_value'.
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2GearJoint_SetRatio (item, a_value)
		end

feature {NONE} -- Implementation

	anchor_1_internal(a_vector:POINTER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2GearJoint_GetAnchorA(item, a_vector)
		end

	anchor_2_internal(a_vector:POINTER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2GearJoint_GetAnchorB(item, a_vector)
		end

	reaction_force_internal(a_inverse_time_step:REAL_32; a_vector:POINTER)
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2GearJoint_GetReactionForce(item, a_vector, a_inverse_time_step)
		end

	type_internal:NATURAL
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2Joint_type_gearJoint
		end

	reaction_torque_internal(a_inverse_time_step:REAL_32):REAL_32
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2GearJoint_GetReactionTorque(item, a_inverse_time_step)
		end
end
