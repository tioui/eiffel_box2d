note
	description: "Common ancestor for {B2D_JOINT} that can be used in {B2D_JOINT_GEAR}"
	author: "Louis Marchand"
	date: "Mon, 21 Feb 2022 15:17:30 +0000"
	revision: "0.2"

deferred class
	B2D_JOINT_GEARABLE

inherit
	B2D_JOINT_LIMITED
	B2D_JOINT_MOTORED
	B2D_JOINT


feature -- Access

	local_anchor_1:B2D_VECTOR_2D
			-- The local anchor point relative to the origin of `body_1'
		require
			Exists: exists
		deferred
		end

	local_anchor_2:B2D_VECTOR_2D
			-- The local anchor point relative to the origin of `body_2'
		require
			Exists: exists
		deferred
		end

	reference_angle:REAL_32
			-- The angle of the reference of `Current'
		require
			Exists: exists
		deferred
		end

	speed:REAL_32
			-- The speed of `Current'
		require
			Exists: exists
		deferred
		end

	set_lower_limit(a_lower:REAL_32)
			-- Assign `lower_limit' with the value of `a_lower'
		do
			set_limits(a_lower, upper_limit)
		end

	set_upper_limit(a_upper:REAL_32)
			-- Assign `upper_limit' with the value of `a_upper'
		do
			set_limits(lower_limit, a_upper)
		end

end
