note
	description: "Structure that contains information to create a {B2D_JOINT_GEAR}."
	author: "Patrick Boucher and Louis Marchand"
	date: "Wed, 24 Jun 2020 22:34:57 +0000"
	revision: "0.1"

class
	B2D_JOINT_GEAR_DEFINITION

inherit
	B2D_JOINT_DEFINITION
		export
			{NONE} set_body_1, set_body_2
		end

create
	make

feature {NONE} -- Initialization

	make (a_joint_1, a_joint_2:B2D_JOINT_GEARABLE)
			-- Initialization of `Current' with defined {B2D_JOINT}.
		require
			Joint_1_Exists: a_joint_1.exists
			Joint_2_Exists: a_joint_2.exists
		do
			own_from_item ({B2D_EXTERNAL}.b2gearjointdef_new)
			set_joint_1 (a_joint_1)
			set_joint_2 (a_joint_2)
			set_body_1(a_joint_1.body_2)
			set_body_2(a_joint_2.body_2)
		ensure
			Is_Assigned_joint_1: joint_1 ~ a_joint_1
			Is_Assigned_joint_2: joint_2 ~ a_joint_2
			Is_Body_1_Valid: body_1 ~ a_joint_1.body_2
			Is_Body_2_Valid: body_2 ~ a_joint_2.body_2
		end

feature -- Access

	joint_1:B2D_JOINT_GEARABLE assign set_joint_1
			-- The first {B2D_JOINT_GEARABLE} that will be attached to the {B2D_JOINT_GEAR}.


	set_joint_1 (a_joint:B2D_JOINT_GEARABLE)
			-- Assign `joint_1' with the value `a_joint'.
		require
			Exists: exists
			Joint_Exists: a_joint.exists
		do
			{B2D_EXTERNAL}.b2gearjointdef_joint1_set (item, a_joint.item)
			joint_1 := a_joint
			set_body_1(a_joint.body_2)
		ensure
			Is_Assigned: joint_1 ~ a_joint
		end

	joint_2:B2D_JOINT_GEARABLE assign set_joint_2
			-- The second {B2D_JOINT_GEARABLE} that will be attached to the {B2D_JOINT_GEAR}.


	set_joint_2 (a_joint:B2D_JOINT_GEARABLE)
			-- Assign `joint_2' with the value `a_joint'.
		require
			Exists: exists
			Joint_Exists: a_joint.exists
		do
			{B2D_EXTERNAL}.b2gearjointdef_joint2_set (item, a_joint.item)
			joint_2 := a_joint
			set_body_2(a_joint.body_2)
		ensure
			Is_Assigned: joint_2 ~ a_joint
		end

	ratio:REAL_32 assign set_ratio
			-- The ratio of the {B2D_JOINT_GEAR}.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2gearjointdef_ratio_get (item)
		end

	set_ratio (a_value:REAL_32)
			-- Assign `ratio' with the value `a_value'.
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2gearjointdef_ratio_set (item, a_value)
		end

feature {NONE} -- Implementation

	type_internal:NATURAL
			-- <Precursor>
		do
			Result := {B2D_EXTERNAL}.b2joint_type_gearjoint
		end

	delete
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2GearJointDef_delete(item)
		end

invariant
	Body_1_Valid: body_1 ~ joint_1.body_2
	Body_2_Valid: body_2 ~ joint_2.body_2
end
