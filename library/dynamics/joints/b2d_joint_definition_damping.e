note
	description: "Common ancestor for {B2D_JOINT_DEFINITION} that possess a damping effect."
	author: "Patrick Boucher"
	date: "Mon, 22 Jun 2020 20:01:15 +0000"
	revision: "0.1"

deferred class
	B2D_JOINT_DEFINITION_DAMPING

inherit
	B2D_JOINT_DEFINITION

feature -- Access

	frequency:REAL_32 assign set_frequency
			-- The mass-spring-damper frequency in Hertz.
			-- A value of 0 disables softness.
		require
			Exists: exists
		deferred
		end

	set_frequency(a_value:REAL_32)
			-- Assign `frequency' with `a_value'
		require
			Exists: exists
			Non_Negative: a_value >= 0.0
			Fequency_Valid: (a_value < 0.5 / body_1.world.step_time) or {B2D_COMPARE_REAL}.compare_real_32 (a_value, {REAL_32}0.5 / body_1.world.step_time)
		deferred
		ensure
			Is_Assign: frequency ~ a_value
		end

	damping_ratio:REAL_32 assign set_damping_ratio
			-- The ratio of the damping of `Current'.
			-- 0 = no damping, 1 = critical damping.
		require
			Exists: exists
		deferred
		end

	set_damping_ratio(a_value:REAL_32)
			-- Assign `damping_ratio' with `a_value'
		require
			Exists: exists
			Non_Negative: a_value >= 0.0
		deferred
		ensure
			Is_Assign: damping_ratio ~ a_value
		end

invariant
	Non_Negative_Frequency: exists implies frequency >= 0.0
	Non_Negative_Damping_Ratio: exists implies damping_ratio >= 0.0

end
