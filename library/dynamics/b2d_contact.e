note
	description: "When two body are touching."
	author: "Louis Marchand"
	date: "Fri, 05 Jun 2020 16:59:55 +0000"
	revision: "0.1"

class
	B2D_CONTACT

inherit
	B2D_MEMORY_OBJECT

create {B2D_ANY}
	make

feature {NONE} -- Initialization

	make(a_item:POINTER; a_fixture_1, a_fixture_2:B2D_FIXTURE; a_with: B2D_ANY)
			-- Initialisation of `Current' using `a_item' as `item',
			-- `a_fixture_1' as `fixture_1' and `a_fixture_2' as
			-- `fixture_2'. `a_with' is the {B2D_*} object with whom `Current'
			-- shared it's `item'.
		do
			shared_from_item (a_item, a_with)
			fixture_1 := a_fixture_1
			fixture_2 := a_fixture_2
		end

feature -- Access

	manifold:B2D_MANIFOLD
			-- The manifold of `Current'. `world_manifold' is generally more usefull.
		require
			Exists:exists
		do
			create Result.shared_from_item ({B2D_EXTERNAL}.b2Contact_GetManifold(item), Current)
		end

	world_manifold:B2D_WORLD_MANIFOLD
			-- The `manifold' with world coordinates
		require
			Exists:exists
			Manifold_Exists: manifold.exists
		do
			create Result.make_empty (manifold.point_count)
			if Result.exists then
				{B2D_EXTERNAL}.b2Contact_GetWorldManifold(item, Result.item)
			end
		end

	is_touching:BOOLEAN
			-- `Current' has touching contact.
		require
			Exists:exists
		do
			Result := {B2D_EXTERNAL}.b2Contact_IsTouching(item)
		end

	is_enabled:BOOLEAN
			-- `Current' is enabled. Only usefull in the {B2D_WORLD}.`pre_solve_contact_actions'
			-- and is only disabled for one {B2D_WORLD}.`step'.
		require
			Exists:exists
		do
			Result := {B2D_EXTERNAL}.b2Contact_IsEnabled(item)
		end

	set_is_enabled(a_is_enabled:BOOLEAN)
			-- Assign `is_enabled' with the value of `a_is_enabled'
		require
			Exists:exists
		do
			{B2D_EXTERNAL}.b2Contact_SetEnabled(item, a_is_enabled)
		ensure
			Is_Assign: is_enabled ~ a_is_enabled
		end

	enable
			-- Set `is_enabled'
		require
			Exists:exists
		do
			set_is_enabled(True)
		ensure
			Is_Set: is_enabled
		end

	disable
			-- Unset `is_enabled'
		require
			Exists:exists
		do
			set_is_enabled(False)
		ensure
			Is_Set: not is_enabled
		end

	fixture_1:B2D_FIXTURE
			-- The first {B2D_FIXTURE} that `Current' is assign

	fixture_1_child_index:INTEGER
			-- The index of the child of `fixture_1' to which `Current'
			-- applied. Only usefull if `fixture_1' has a {B2D_SHAPE_CHAIN}.
		require
			Exists:exists
		do
			Result := {B2D_EXTERNAL}.b2Contact_GetChildIndexA(item) + 1
		end

	fixture_2:B2D_FIXTURE
			-- The second {B2D_FIXTURE} that `Current' is assign

	fixture_2_child_index:INTEGER
			-- The index of the child of `fixture_2' to which `Current'
			-- applied. Only usefull if `fixture_2' has a {B2D_SHAPE_CHAIN}.
		require
			Exists:exists
		do
			Result := {B2D_EXTERNAL}.b2Contact_GetChildIndexB(item) + 1
		end

	friction:REAL_32 assign set_friction
			-- The friction between the two `fixture_*' when `Current' happen.
		require
			Exists:exists
		do
			Result := {B2D_EXTERNAL}.b2Contact_GetFriction(item)
		end

	set_friction(a_friction:REAL_32)
			-- Assign `frition' wit the value of `a_friction'
		require
			Exists:exists
		do
			{B2D_EXTERNAL}.b2Contact_SetFriction(item, a_friction)
		ensure
			Is_Assign: friction ~ a_friction
		end

	reset_friction
			-- Put back the default value to `friction'
		require
			Exists:exists
		do
			{B2D_EXTERNAL}.b2Contact_ResetFriction(item)
		end

	restitution:REAL_32 assign set_restitution
			-- The restitution of the `fixture_*'
		require
			Exists:exists
		do
			Result := {B2D_EXTERNAL}.b2Contact_GetRestitution(item)
		end

	set_restitution(a_restitution:REAL_32)
			-- Assign `restitution' wit the value of `a_restitution'
		require
			Exists:exists
		do
			{B2D_EXTERNAL}.b2Contact_SetRestitution(item, a_restitution)
		ensure
			Is_Assign: restitution ~ a_restitution
		end

	reset_restitution
			-- Put back the default value to `restitution'
		require
			Exists:exists
		do
			{B2D_EXTERNAL}.b2Contact_ResetRestitution(item)
		end

	tangent_speed:REAL_32 assign set_tangent_speed
			-- The tangent_speed of the `fixture_*'
		require
			Exists:exists
		do
			Result := {B2D_EXTERNAL}.b2Contact_GetTangentSpeed(item)
		end

	set_tangent_speed(a_tangent_speed:REAL_32)
			-- Assign `tangent_speed' wit the value of `a_tangent_speed'
		require
			Exists:exists
		do
			{B2D_EXTERNAL}.b2Contact_SetTangentSpeed(item, a_tangent_speed)
		ensure
			Is_Assign: tangent_speed ~ a_tangent_speed
		end


feature {NONE} -- Implementation

	delete
			-- <Precursor>
		do
			check Should_Not_Be_Deleted: False end
		end


end
