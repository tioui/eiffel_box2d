note
	description: "{B2D_WORLD} profiling data"
	author: "Louis Marchand"
	date: "Thu, 11 Jun 2020 20:29:27 +0000"
	revision: "0.1"

class
	B2D_PROFILE

inherit
	B2D_MEMORY_OBJECT

create {B2D_ANY}
	shared_from_item

feature -- Access

	step:REAL_32
			-- The time of the step in milliseconds
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2Profile_step_get(item)
		end

	collide:REAL_32
			-- The time of the collider in milliseconds
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2Profile_collide_get(item)
		end

	initialisation_solver:REAL_32
			-- The time of the initialisation of the solver in milliseconds
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2Profile_solveInit_get(item)
		end

	velocity_solver:REAL_32
			-- The time of the velocity solver in milliseconds
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2Profile_solveVelocity_get(item)
		end

	solver:REAL_32
			-- The time of the solver in milliseconds
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2Profile_solve_get(item)
		end

	position_solver:REAL_32
			-- The time of the position solver in milliseconds
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2Profile_solvePosition_get(item)
		end

	time_of_impact_solver:REAL_32
			-- The time of the time of impact solver in milliseconds
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2Profile_solveTOI_get(item)
		end

	broad_phase:REAL_32
			-- The time of the broad phase in milliseconds
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2Profile_broadphase_get(item)
		end

feature {NONE} -- Implementation

	delete
			-- <Precursor>
		do
			check Shold_Not_Be_Deleted: False end
		end

end
