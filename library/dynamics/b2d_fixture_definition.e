note
	description: "Definition used to create {B2D_FIXTURE}."
	author: "Louis Marchand"
	date: "Fri, 05 Jun 2020 16:59:55 +0000"
	revision: "0.1"

class
	B2D_FIXTURE_DEFINITION

inherit
	B2D_MEMORY_OBJECT

create
	make

feature {NONE} -- Initialisation

	make(a_shape:B2D_SHAPE)
			-- Initialisation of `Current'
		require
			Shape_Exists: a_shape.exists
			Shape_Valid: a_shape.is_valid
		do
			shape := a_shape
			own_from_item ({B2D_EXTERNAL}.b2FixtureDef_new)
			if exists then
				{B2D_EXTERNAL}.b2FixtureDef_shape_set(item, a_shape.item)
				create filter.shared_from_item ({B2D_EXTERNAL}.b2FixtureDef_filter_get(item), Current)
			else
				create filter.own_from_item (create {POINTER})
			end
		end

feature -- Access

	shape:B2D_SHAPE
			-- The {B2D_SHAPE} that `Current' is assing to.

	friction:REAL_32 assign set_friction
			-- The friction coefficient, usually in the range [0,1].
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2FixtureDef_friction_get(item)
		end

	set_friction(a_friction:REAL_32)
			-- Assign `friction' with the value `a_friction'
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2FixtureDef_friction_set(item, a_friction)
		ensure
			Is_Assign: friction ~ a_friction
		end

	restitution:REAL_32 assign set_restitution
			-- The restitution (elasticity) usually in the range [0,1].
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2FixtureDef_restitution_get(item)
		end

	set_restitution(a_restitution:REAL_32)
			-- Assign `restitution' with the value `a_restitution'
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2FixtureDef_restitution_set(item, a_restitution)
		ensure
			Is_Assign: restitution ~ a_restitution
		end

	density:REAL_32 assign set_density
			-- The density, usually in kg/m^2.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2FixtureDef_density_get(item)
		end

	set_density(a_density:REAL_32)
			-- Assign `density' with the value `a_density'
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2FixtureDef_density_set(item, a_density)
		ensure
			Is_Assign: density ~ a_density
		end

	is_sensor:BOOLEAN assign set_is_sensor
			-- A sensor shape collects contact information but never
			-- generates a collision response.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2FixtureDef_isSensor_get(item)
		end

	set_is_sensor(a_is_sensor:BOOLEAN)
			-- Assign `is_sensor' with the value `a_is_sensor'
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2FixtureDef_isSensor_set(item, a_is_sensor)
		ensure
			Is_Assign: is_sensor ~ a_is_sensor
		end

	enable_sensor
			-- Set `is_sensor' to `True'
		require
			Exists: exists
		do
			set_is_sensor(True)
		ensure
			Is_Set: is_sensor
		end

	disable_sensor
			-- Set `is_sensor' to `False'
		require
			Exists: exists
		do
			set_is_sensor(False)
		ensure
			Is_Set: not is_sensor
		end

	filter:B2D_FILTER
			-- Used to filter collision



feature {NONE} -- Implementation

	delete
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2FixtureDef_delete(item)
		end

invariant
	Shape_Valid: exists and shape.exists implies
					{B2D_EXTERNAL}.b2FixtureDef_shape_get(item) ~ shape.item
end
