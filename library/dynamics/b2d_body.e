note
	description: "A rigid body. These are created via {B2D_WORLD}.create_body."
	author: "Patrick Boucher"
	date: "Thu, 16 Jun 2020 02:30:00"
	revision: "0.1"

class
	B2D_BODY

inherit
	B2D_LIST_HELPER

create {B2D_WORLD}
	make

feature {NONE} -- Initialiation

	make (a_item:POINTER; a_world:B2D_WORLD)
			-- Initialization of `Current'.
			--
			-- `a_item': C pointer of a newly created b2Body.
		require
			Item_Exists: not a_item.is_default_pointer
			World_Exists: a_world.exists
		do
			has_error := False
			shared_from_item (a_item, a_world)
			create {LINKED_LIST[B2D_FIXTURE]} internal_fixtures.make
			world := a_world
		end

feature -- Access

	create_fixture_from_definition (a_fixture_definition: B2D_FIXTURE_DEFINITION)
			-- Create and attach a new fixture to `Current'.
			-- Use this function if you need to set some fixture parameters, like friction.
			-- Otherwise you can create the fixture directly from a shape.
			-- If the density is non-zero, this function automatically updates the mass of the body.
			-- Contacts are not created until the next time step.
			--
			-- `a_fixture_definition`: Definition of the fixture to be created.
		require
			Exists: exists
			Fixture_Definition_Exists: a_fixture_definition.exists
		do
			has_error := False
			create_fixture ({B2D_EXTERNAL}.b2body_createfixture_with_definition (item, a_fixture_definition.item))
		end

	create_fixture_from_shape (a_shape:B2D_SHAPE; a_density:REAL_32)
			-- Creates a fixture from a shape and attach it to this body.
			-- This is a convenience function. Use {B2D_FIXTURE_DEFINITION} if you need to set
			-- parameters like friction, restitution or filtering.
			-- If the density is non-zero, this function automatically updates the mass of the body.
			--
			-- `a_shape`: The shape to be cloned.
			-- `a_density`: The shape density (set to zero for static bodies).
		require
			Exists: exists
			Shape_Exists: a_shape.exists
			Static_Zero_Density: is_static implies a_density = 0.0
		do
			has_error := False
			create_fixture ({B2D_EXTERNAL}.b2body_createfixture_with_shape (item, a_shape.item, a_density))
		end

	destroy_fixture (a_fixture:B2D_FIXTURE)
			-- Remove `a_fixture' from `Current'.
			-- This will automatically adjust the mass of the body if the body is dynamic
			-- and the fixture has positive density.
		require
			Exists: exists
			Fixture_Exists: a_fixture.exists
		local
			l_fixture_item:POINTER
			l_user_data:POINTER
		do
			has_error := False
			if has_fixture (a_fixture) then
				l_user_data := {B2D_EXTERNAL}.b2Fixture_GetUserData(a_fixture.item)
				{B2D_EXTERNAL}.b2Fixture_SetUserData(a_fixture.item, create {POINTER})
				world.remove_from_wean_chain (l_user_data)
				remove_from_internal_list (internal_fixtures, a_fixture)
				if last_fixture = a_fixture then
					if not internal_fixtures.is_empty then
						last_fixture := internal_fixtures.last
					else
						last_fixture := Void
					end
				end
				l_fixture_item := a_fixture.item
				a_fixture.put_null
				{B2D_EXTERNAL}.b2body_destroyfixture (item, l_fixture_item)
			else
				has_error := True
			end
		ensure
			Fixtures_Decreased: not has_error implies internal_fixtures.count < old internal_fixtures.count
			Last_Fixture_Valid: not has_error implies last_fixture /= old a_fixture
		end

	last_fixture: detachable B2D_FIXTURE
			-- The last {B2D_FIXTURE} added to `Current'.

	fixture_count:INTEGER
			-- The number of {B2D_FIXTURE} in `Current'
			-- May have side effect on `has_error'.
		require
			Exists: exists
		do
			has_error := False
			Result := internal_fixtures.count
		end

	fixtures: LIST[B2D_FIXTURE]
			-- List of all {B2D_FIXTURE} attached to `Current'.
		require
			Exists: exists
		do
			has_error := False
			create {ARRAYED_LIST[B2D_FIXTURE]} Result.make_from_iterable (internal_fixtures)
		end

	has_fixture (a_fixture:B2D_FIXTURE): BOOLEAN
			-- `Current' owns `a_fixture'.
		require
			Exists: exists
			Fixture_Exists: a_fixture.exists
		do
			Result := internal_fixtures.has (a_fixture)
		end

	transform: B2D_TRANSFORM assign set_transform_with_transform
			-- Copyof the transform of `Current'.
		require
			Exists: exists
		do
			has_error := False
			create Result.make_from_item ({B2D_EXTERNAL}.b2body_gettransform (item))
		end

	set_transform (a_position_x, a_position_y, a_angle:REAL_32)
			-- Set the position of the `Current's origin and rotation.
		require
			Exists: exists
		local
			l_vector:B2D_VECTOR_2D
		do
			has_error := False
			create l_vector.make_with_coordinates (a_position_x, a_position_y)
			if l_vector.exists then
				set_transform_with_vector(l_vector, a_angle)
			else
				has_error := True
			end
		end

	set_transform_with_vector (a_position:B2D_VECTOR_2D; a_angle:REAL_32)
			-- Set the position of the `Current's origin and rotation.
		require
			Exists: exists
			Vector_Exists: a_position.exists
		do
			has_error := False
			{B2D_EXTERNAL}.b2body_settransform (item, a_position.item, a_angle)
		end

	set_transform_with_transform (a_transform:B2D_TRANSFORM)
			-- Set the transform of `Current' with `a_transform'.
		require
			Exists: exists
			Transform_Exists: a_transform.exists
		do
			set_transform_with_vector (a_transform.position, a_transform.rotation.angle)
		ensure
			Is_Assign: not has_error implies transform ~ a_transform
		end

	position: B2D_VECTOR_2D
			-- World position of `Current's origin.
		require
			Exists: exists
		do
			has_error := False
			create Result.make_from_item ({B2D_EXTERNAL}.b2body_getposition (item))
		end

	angle: REAL_32
			-- Get the angle of `Current' in radians.
		require
			Exists: exists
		do
			has_error := False
			Result := {B2D_EXTERNAL}.b2body_getangle (item)
		end

	center_of_mass: B2D_VECTOR_2D
			-- World position of the center of mass of `Current'.
		require
			Exists: exists
		do
			has_error := False
			create Result.make_from_item ({B2D_EXTERNAL}.b2body_getworldcenter (item))
		end

	center_of_mass_local: B2D_VECTOR_2D
			-- Local position of the center of mass of `Current'.
		require
			Exists: exists
		do
			has_error := False
			create Result.make_from_item ({B2D_EXTERNAL}.b2body_getlocalcenter (item))
		end

	linear_velocity: B2D_VECTOR_2D assign set_linear_velocity_with_vector
			-- Copy of the linear velocity of `Current's center of mass.
		require
			Exists: exists
		do
			has_error := False
			create Result.make_from_item ({B2D_EXTERNAL}.b2body_getlinearvelocity (item))
		end

	set_linear_velocity_with_vector (a_linear_velocity:B2D_VECTOR_2D)
			-- Assign `linear_velocity' with the value `a_linear_velocity'.
		require
			Exists: exists
			Linear_Velocity_Exists: a_linear_velocity.exists
		do
			has_error := False
			{B2D_EXTERNAL}.b2body_setlinearvelocity (item, a_linear_velocity.item)
		ensure
			Is_Assign: linear_velocity ~ a_linear_velocity
		end

	set_linear_velocity (a_linear_velocity_x, a_linear_velocity_y:REAL_32)
			-- Assign `linear_velocity' with the value `a_linear_velocity_x' and `a_linear_velocity_y'.
		require
			Exists: exists
		local
			l_vector:B2D_VECTOR_2D
		do
			has_error := False
			create l_vector.make_with_coordinates (a_linear_velocity_x, a_linear_velocity_y)
			if l_vector.exists then
				set_linear_velocity_with_vector(l_vector)
			else
				has_error := True
			end
		ensure
			Is_Assign_X: not has_error implies linear_velocity.x ~ a_linear_velocity_x
			Is_Assign_Y: not has_error implies linear_velocity.y ~ a_linear_velocity_y
		end

	angular_velocity: REAL_32 assign set_angular_velocity
			-- Linear velocity of `Current'.
		require
			Exists: exists
		do
			has_error := False
			Result := {B2D_EXTERNAL}.b2body_getangularvelocity (item)
		end

	set_angular_velocity (a_angular_velocity:REAL_32)
			-- Assign `angular_velocity' with the value `a_angular_velocity'.
			--
			-- `a_angular_velocity`: Angular velocity in radians/second.
		require
			Exists: exists
		do
			has_error := False
			{B2D_EXTERNAL}.b2body_setangularvelocity (item, a_angular_velocity)
		ensure
			Is_Assign: angular_velocity ~ a_angular_velocity
		end

	apply_force_with_vectors (a_force:B2D_VECTOR_2D; a_world_point:B2D_VECTOR_2D)
			-- Apply a force at a world point. If the force is not applied at the center of mass,
			-- it will generate a torque and affect the angular velocity.
			-- It wakes up the `Current'.
		require
			Exists: exists
			Force_Exists: a_force.exists
			Point_Exists: a_world_point.exists
		do
			has_error := False
			{B2D_EXTERNAL}.b2body_applyforce (item, a_force.item, a_world_point.item, True)
		end

	apply_force (a_force_x, a_force_y, a_world_point_x, a_world_point_y:REAL_32)
			-- Apply a force at a world point. If the force is not applied at the center of mass,
			-- it will generate a torque and affect the angular velocity.
			-- It wakes up the `Current'.
		require
			Exists: exists
		local
			l_force, l_point:B2D_VECTOR_2D
		do
			has_error := False
			create l_force.make_with_coordinates (a_force_x, a_force_y)
			create l_point.make_with_coordinates (a_world_point_x, a_world_point_y)
			if l_force.exists and l_point.exists then
				apply_force_with_vectors(l_force, l_point)
			else
				has_error := True
			end
		end

	apply_force_to_center_with_vector (a_force:B2D_VECTOR_2D)
			-- Apply a force to the center of mass. It wakes up `Current'.
		require
			Exists: exists
			Force_exists: a_force.exists
		do
			has_error := False
			{B2D_EXTERNAL}.b2body_applyforcetocenter (item, a_force.item, True)
		end

	apply_force_to_center (a_force_x, a_force_y:REAL_32)
			-- Apply a force to the center of mass. It wakes up `Current'.
		require
			Exists: exists
		local
			l_force:B2D_VECTOR_2D
		do
			has_error := False
			create l_force.make_with_coordinates (a_force_x, a_force_y)
			if l_force.exists then
				apply_force_to_center_with_vector(l_force)
			else
				has_error := True
			end
		end

	apply_torque (a_torque:REAL_32)
			-- Apply a torque. This affects the angular velocity without affecting
			-- the linear velocity of the center of mass.
			-- It wakes up the body.
		require
			Exists: exists
		do
			has_error := False
			{B2D_EXTERNAL}.b2body_applytorque (item, a_torque, True)
		end

	apply_linear_impulse_with_vectors (a_impulse:B2D_VECTOR_2D; a_world_point:B2D_VECTOR_2D)
			-- Apply an impulse at a point. This immediately modifies the velocity.
			-- It also modifies the angular velocity if the point of application is not at the center of mass.
			-- It wakes up the body.
		require
			Exists: exists
			Impulse_Exists: a_impulse.exists
			Point_Exists: a_world_point.exists
		do
			has_error := False
			{B2D_EXTERNAL}.b2body_applylinearimpulse (item, a_impulse.item, a_world_point.item, True)
		end

	apply_linear_impulse (a_impulse_x, a_impulse_y, a_world_point_x, a_world_point_y:REAL_32)
			-- Apply an impulse at a point. This immediately modifies the velocity.
			-- It also modifies the angular velocity if the point of application is not at the center of mass.
			-- It wakes up the body.
		require
			Exists: exists
		local
			l_impulse, l_point:B2D_VECTOR_2D
		do
			has_error := False
			create l_impulse.make_with_coordinates (a_impulse_x, a_impulse_y)
			create l_point.make_with_coordinates (a_world_point_x, a_world_point_y)
			if l_impulse.exists and l_point.exists then
				apply_linear_impulse_with_vectors(l_impulse, l_point)
			else
				has_error := False
			end
		end

	apply_linear_impulse_to_center_with_vector (a_impulse:B2D_VECTOR_2D)
			-- Apply an impulse at the center of mass. This immediately modifies the velocity.
			-- It also modifies the angular velocity if the point of application is not at the center of mass.
			-- It wakes up the body.
		require
			Exists: exists
			Impulse_Exists: a_impulse.exists
		do
			has_error := False
			apply_linear_impulse_with_vectors (a_impulse, center_of_mass)
		end

	apply_linear_impulse_to_center (a_impulse_x, a_impulse_y:REAL_32)
			-- Apply an impulse at the center of mass. This immediately modifies the velocity.
			-- It also modifies the angular velocity if the point of application is not at the center of mass.
			-- It wakes up the body.
		require
			Exists: exists
		local
			l_impulse:B2D_VECTOR_2D
		do
			has_error := False
			create l_impulse.make_with_coordinates (a_impulse_x, a_impulse_y)
			if l_impulse.exists then
				apply_linear_impulse_to_center_with_vector (l_impulse)
			else
				has_error := True
			end

		end

	apply_angular_impulse (a_impulse:REAL_32)
			-- Apply an angular impulse.
			-- It wakes up the body.
		require
			Exists: exists
		do
			has_error := False
			{B2D_EXTERNAL}.b2body_applyangularimpulse (item, a_impulse, True)
		end

	mass: REAL_32
			-- Total mass of `Current'.
		require
			Exists: exists
		do
			has_error := False
			Result := {B2D_EXTERNAL}.b2body_getmass (item)
		end

	inertia: REAL_32
			-- Get the rotational inertia of `Current' about the local origin.
		require
			Exists: exists
		do
			has_error := False
			Result := {B2D_EXTERNAL}.b2body_getinertia (item)
		end

	mass_data:B2D_MASS
			-- the mass properties to override the mass properties of the fixtures.
			-- Note that this changes the center of mass position.
			-- Note that creating or destroying fixtures can also alter the mass.
			-- This function has no effect if the body isn't dynamic.
		require
			Exists:exists
			Is_Synamic: is_dynamic
		do
			create Result
			if Result.exists then
				{B2D_EXTERNAL}.b2Body_getMassData(item, Result.item)
			end
		end

	set_mass_data(a_mass_data:B2D_MASS_DATA)
			-- Assign `mass_data' with the value of `a_mass_data'
		require
			Exists: exists
			Mass_Data_Exists: a_mass_data.exists
			Is_Synamic: is_dynamic
		do
			{B2D_EXTERNAL}.b2Body_setMassData(item, a_mass_data.item)
		ensure
			Is_Assign: mass_data ~ a_mass_data
		end

	reset_mass_data
			-- resets the mass properties to the sum of the mass properties of the fixtures.
			-- This normally does not need to be called unless you called SetMassData to
			-- override the mass and you later want to reset the mass.
		require
			Exists: exists
		do
			has_error := False
			{B2D_EXTERNAL}.b2Body_resetMassData(item)
		end

	local_point_to_world_coordinates_with_vector (a_local_point:B2D_VECTOR_2D): B2D_VECTOR_2D
			-- World coordinates of a point given the local coordinates.
		require
			Exists: exists
			Local_Point_Exists: a_local_point.exists
		do
			has_error := False
			create Result
			if Result.exists then
				{B2D_EXTERNAL}.b2body_getworldpoint (item, a_local_point.item, Result.item)
			end
		end

	local_point_to_world_coordinates (a_local_point_x, a_local_point_y:REAL_32): B2D_VECTOR_2D
			-- World coordinates of a point given the local coordinates.
		require
			Exists: exists
		local
			l_vector:B2D_VECTOR_2D
		do
			has_error := False
			create l_vector.make_with_coordinates (a_local_point_x, a_local_point_y)
			if l_vector.exists then
				Result := local_point_to_world_coordinates_with_vector(l_vector)
			else
				create Result.own_from_item (create {POINTER})
				has_error := True
			end
		end

	rotated_local_point_with_vector (a_local_point:B2D_VECTOR_2D): B2D_VECTOR_2D
			-- Local coordinates of a point after rotation.
			-- Equivalent to b2body::getWorldVector()
			--
			-- `a_local_point`: Local point on `Current' if the rotation were 0.
		require
			Exists: exists
			Vector_Exists: a_local_point.exists
		do
			has_error := False
			create Result
			if Result.exists then
				{B2D_EXTERNAL}.b2body_getworldvector (item, a_local_point.item, Result.item)
			end
		end

	rotated_local_point (a_local_x, a_local_y:REAL_32): B2D_VECTOR_2D
			-- Local coordinates of a point after rotation.
			-- Equivalent to b2body::getWorldVector()
			--
			-- `a_local_x` and `a_local_y`: Local point on `Current' if the rotation were 0.
		require
			Exists: exists
		local
			l_vector:B2D_VECTOR_2D
		do
			has_error := False
			create l_vector.make_with_coordinates (a_local_x, a_local_y)
			if l_vector.exists then
				Result := rotated_local_point_with_vector(l_vector)
			else
				create Result.own_from_item (create {POINTER})
				has_error := True
			end
		end

	relative_local_point_with_vector (a_local_point:B2D_VECTOR_2D):B2D_VECTOR_2D
			-- Local coordinates of a point as if the X axis were aligned
			-- with the `angle' of `Current'.
			-- Equivalent to b2body::getLocalVector()
			--
			-- `a_local_point`: Local point on `Current' as the x axis is horizontal.
		require
			Exists: exists
		do
			has_error := False
			create Result
			if Result.exists then
				{B2D_EXTERNAL}.b2body_getlocalvector (item, a_local_point.item, Result.item)
			end
		end

	relative_local_point (a_local_x, a_local_y:REAL_32):B2D_VECTOR_2D
			-- Local coordinates of a point as if the X axis were aligned
			-- with the `angle' of `Current'.
			-- Equivalent to b2body::getLocalVector()
			--
			-- `a_local_x` and `a_local_y`: Local point on `Current' as the x axis is horizontal.
		require
			Exists: exists
		local
			l_vector:B2D_VECTOR_2D
		do
			has_error := False
			create l_vector.make_with_coordinates (a_local_x, a_local_y)
			if l_vector.exists then
				Result := relative_local_point_with_vector (l_vector)
			else
				create Result.own_from_item (create {POINTER})
				has_error := True
			end
		end

	world_point_to_local_coordinates_with_vector (a_world_point:B2D_VECTOR_2D): B2D_VECTOR_2D
			-- Local point relative to the body's origin given a world point.
		require
			Exists: exists
			World_Point_Exists: a_world_point.exists
		do
			has_error := False
			create Result
			if Result.exists then
				{B2D_EXTERNAL}.b2body_getlocalpoint (item, a_world_point.item, Result.item)
			end
		end

	world_point_to_local_coordinates (a_world_point_x, a_world_point_y:REAL_32): B2D_VECTOR_2D
			-- Local point relative to the body's origin given a world point.
		require
			Exists: exists
		local
			l_vector:B2D_VECTOR_2D
		do
			has_error := False
			create l_vector.make_with_coordinates (a_world_point_x, a_world_point_y)
			if l_vector.exists then
				Result := world_point_to_local_coordinates_with_vector(l_vector)
			else
				create Result.own_from_item (create {POINTER})
				has_error := True
			end
		end

	linear_velocity_from_world_point_with_vector (a_world_point:B2D_VECTOR_2D): B2D_VECTOR_2D
			-- World linear velocity of a world point attached to this body.
		require
			Exists: exists
			World_Point_Exists: a_world_point.exists
		do
			has_error := False
			create Result
			if Result.exists then
				{B2D_EXTERNAL}.b2body_getlinearvelocityfromworldpoint (item, a_world_point.item, Result.item)
			end
		end

	linear_velocity_from_world_point (a_world_point_x, a_world_point_y:REAL_32): B2D_VECTOR_2D
			-- World linear velocity of a world point attached to this body.
		require
			Exists: exists
		local
			l_vector:B2D_VECTOR_2D
		do
			has_error := False
			create l_vector.make_with_coordinates (a_world_point_x, a_world_point_y)
			if l_vector.exists then
				Result := linear_velocity_from_world_point_with_vector(l_vector)
			else
				create Result.own_from_item (create {POINTER})
				has_error := True
			end
		end

	linear_velocity_from_local_point_with_vector (a_local_point:B2D_VECTOR_2D): B2D_VECTOR_2D
			-- World velocity of a local point.
		require
			Exists: exists
			Local_Point_Exists: a_local_point.exists
		do
			has_error := False
			create Result
			if Result.exists then
				{B2D_EXTERNAL}.b2body_getlinearvelocityfromlocalpoint (item, a_local_point.item, Result.item)
			end
		end

	linear_velocity_from_local_point (a_local_point_x, a_local_point_y:REAL_32): B2D_VECTOR_2D
			-- World velocity of a local point.
		require
			Exists: exists
		local
			l_vector:B2D_VECTOR_2D
		do
			has_error := False
			create l_vector.make_with_coordinates (a_local_point_x, a_local_point_y)
			if l_vector.exists then
				Result := linear_velocity_from_local_point_with_vector(l_vector)
			else
				create Result.own_from_item (create {POINTER})
				has_error := True
			end
		end

	linear_damping: REAL_32 assign set_linear_damping
			-- Linear damping of `Current'.
		require
			Exists: exists
		do
			has_error := False
			Result := {B2D_EXTERNAL}.b2body_getlineardamping (item)
		end

	set_linear_damping (a_linear_damping:REAL_32)
			-- Assign `linear_damping' with the value `a_linear_damping'.
		require
			Exists: exists
		do
			has_error := False
			{B2D_EXTERNAL}.b2body_setlineardamping (item, a_linear_damping)
		ensure
			Is_Assign: linear_damping ~ a_linear_damping
		end

	angular_damping: REAL_32 assign set_angular_damping
			-- Angular damping of `Current'.
		require
			Exists: exists
		do
			has_error := False
			Result := {B2D_EXTERNAL}.b2body_getangulardamping (item)
		end

	set_angular_damping (a_angular_damping:REAL_32)
			-- Assign `angular_damping' with the value `a_angular_damping'.
		require
			Exists: exists
		do
			has_error := False
			{B2D_EXTERNAL}.b2body_setangulardamping (item, a_angular_damping)
		ensure
			Is_Assign: angular_damping ~ a_angular_damping
		end

	gravity_scale: REAL_32 assign set_gravity_scale
			-- Gravity scale of `Current'.
		require
			Exists: exists
		do
			has_error := False
			Result := {B2D_EXTERNAL}.b2body_getgravityscale (item)
		end

	set_gravity_scale (a_gravity_scale:REAL_32)
			-- Assign `gravity_scale' with the value `a_gravity_scale'.
		require
			Exists: exists
		do
			has_error := False
			{B2D_EXTERNAL}.b2body_setgravityscale (item, a_gravity_scale)
		ensure
			Is_Assign: gravity_scale ~ a_gravity_scale
		end

	is_bullet: BOOLEAN assign set_is_bullet
			-- `Current' is treated like a bullet for continuous collision detection.
		require
			Exists: exists
		do
			has_error := False
			Result := {B2D_EXTERNAL}.b2body_isbullet (item)
		end

	set_is_bullet (a_value:BOOLEAN)
			-- Assign `is_bullet' with the value `a_value'.
		require
			Exists: exists
			Is_Dynamic: a_value implies is_dynamic
		do
			has_error := False
			{B2D_EXTERNAL}.b2body_setbullet (item, a_value)
		ensure
			Is_Assign: is_bullet ~ a_value
		end

	enable_bullet
			-- Set `is_bullet' to `True'
		require
			Exists: exists
		do
			set_is_bullet(True)
		ensure
			Is_Set: is_bullet
		end

	disable_bullet
			-- Set `is_bullet' to `False'
		require
			Exists: exists
		do
			set_is_bullet(False)
		ensure
			Is_Set: not is_bullet
		end

	allow_sleep: BOOLEAN assign set_allow_sleep
			-- `Current' is allowed to sleep.
		require
			Exists: exists
		do
			has_error := False
			Result := {B2D_EXTERNAL}.b2body_issleepingallowed (item)
		end

	set_allow_sleep (a_value:BOOLEAN)
			-- Assign `allow_sleep' with the value `a_value'.
		require
			Exists: exists
		do
			has_error := False
			{B2D_EXTERNAL}.b2body_setsleepingallowed (item, a_value)
		ensure
			Is_Assign: allow_sleep ~ a_value
		end

	enable_allow_sleep
			-- Set `allow_sleep' to `True'
		require
			Exists: exists
		do
			set_allow_sleep(True)
		ensure
			Is_Set: allow_sleep
		end

	disable_allow_sleep
			-- Set `allow_sleep' to `False'
		require
			Exists: exists
		do
			set_allow_sleep(False)
		ensure
			Is_Set: not allow_sleep
		end

	is_awake: BOOLEAN assign set_is_awake
			-- `Current' is awake.
		require
			Exists: exists
		do
			has_error := False
			Result := {B2D_EXTERNAL}.b2body_isawake (item)
		end

	set_is_awake (a_value:BOOLEAN)
			-- Assign `is_awake' with the value `a_value'.
		require
			Exits: exists
			Sleeping_Allowed: not a_value implies allow_sleep
		do
			has_error := False
			{B2D_EXTERNAL}.b2body_setawake (item, a_value)
		ensure
			Is_Assign: is_awake ~ a_value
		end

	wake
			-- Set `is_awake' to `True'
		require
			Exists: exists
		do
			set_is_awake(True)
		ensure
			Is_Set: is_awake
		end

	sleep
			-- Set `is_awake' to `False'
		require
			Exists: exists
			Sleeping_Allowed: allow_sleep
		do
			set_is_awake(False)
		ensure
			Is_Set: not is_awake
		end

	is_active: BOOLEAN assign set_is_active
			-- `Current' is active.
		require
			Exists: exists
		do
			has_error := False
			Result := {B2D_EXTERNAL}.b2body_isactive (item)
		end

	set_is_active (a_value:BOOLEAN)
			-- Assign `is_active' with the value `a_value'.
		require
			Exists: exists
		do
			has_error := False
			{B2D_EXTERNAL}.b2body_setactive (item, a_value)
		ensure
			Is_Assign: is_active ~ a_value
		end

	activate
			-- Set `is_active' to `True'
		require
			Exists: exists
		do
			set_is_active(True)
		ensure
			Is_Set: is_active
		end

	deactivate
			-- Set `is_active' to `False'
		require
			Exists: exists
		do
			set_is_active(False)
		ensure
			Is_Set: not is_active
		end

	is_fixed_rotation: BOOLEAN assign set_is_fixed_rotation
			-- Forces do not affect `Current's rotation.
		require
			Exists: exists
		do
			has_error := False
			Result := {B2D_EXTERNAL}.b2body_isfixedrotation (item)
		end

	set_is_fixed_rotation (a_value:BOOLEAN)
			-- Assign `is_fixed_rotation' with the value `a_value'.
		require
			Exists: exists
		do
			has_error := False
			{B2D_EXTERNAL}.b2body_setfixedrotation (item, a_value)
		ensure
			Is_Assign: is_fixed_rotation ~ a_value
		end

	enable_fixed_rotation
			-- Set `is_fixed_rotation' to `True'
		require
			Exists: exists
		do
			set_is_fixed_rotation(True)
		ensure
			Is_Set: is_fixed_rotation
		end

	disable_fixed_rotation
			-- Set `is_fixed_rotation' to `False'
		require
			Exists: exists
		do
			set_is_fixed_rotation(False)
		ensure
			Is_Set: not is_fixed_rotation
		end

	joints: LIST[B2D_JOINT]
			-- Every {B2D_JOINT} attached to `Current'.
		require
			Exists: exists
		do
			has_error := False
			create {ARRAYED_LIST[B2D_JOINT]} Result.make (0)
			across world.joints as la_joints loop
				if la_joints.item.body_1 ~ Current or la_joints.item.body_2 ~ Current then
					Result.extend (la_joints.item)
				end
			end
		end

	contacts: LIST[B2D_CONTACT]
			-- Every {B2D_CONTACT} attached to `Current'.
			-- this list changes during the time step and
			-- you may miss some collisions if you don't use {B2D_CONTACT_LISTENER}.
		require
			Exists: exists
		local
			l_c_list: POINTER
			l_fixture_1, l_fixture_2:B2D_FIXTURE
		do
			has_error := False
			create {LINKED_LIST[B2D_CONTACT]} Result.make
			from
				l_c_list := {B2D_EXTERNAL}.b2body_getcontactlist (item)
			until
				l_c_list.is_default_pointer
			loop
				if attached fixture_with_pointer ({B2D_EXTERNAL}.b2Contact_GetFixtureA(l_c_list)) as la_fixture_1 then
					l_fixture_1 := la_fixture_1
				else
					create l_fixture_1.make (Current, create {POINTER})
				end
				if attached fixture_with_pointer ({B2D_EXTERNAL}.b2Contact_GetFixtureB(l_c_list)) as la_fixture_2 then
					l_fixture_2 := la_fixture_2
				else
					create l_fixture_2.make (Current, create {POINTER})
				end
				Result.extend (create {B2D_CONTACT}.make (l_c_list, l_fixture_1, l_fixture_2, Current))
				l_c_list := {B2D_EXTERNAL}.b2contact_getnext (l_c_list)
			end
		end

	world: B2D_WORLD
			-- The {B2D_WORLD} to which `Current' belongs.

	is_static: BOOLEAN
			-- `Current' is static.
		require
			Exists: exists
		do
			has_error := False
			Result := {B2D_EXTERNAL}.b2body_gettype (item) = {B2D_EXTERNAL}.b2body_type_staticbody
		end

	is_kinematic: BOOLEAN
			-- `Current' is kinematic.
		require
			Exists: exists
		do
			has_error := False
			Result := {B2D_EXTERNAL}.b2body_gettype (item) = {B2D_EXTERNAL}.b2body_type_kinematicbody
		end

	is_dynamic: BOOLEAN
			-- `Current' is dynamic.
		require
			Exists: exists
		do
			has_error := False
			Result := {B2D_EXTERNAL}.b2body_gettype (item) = {B2D_EXTERNAL}.b2body_type_dynamicbody
		end

	attached_data:detachable ANY assign set_attached_data
			-- Usefull to stock anything that the client may want to attach to `Current'

	set_attached_data(a_data:detachable ANY)
			-- Assign `attached_data' with the value of `a_data'
		do
			attached_data := a_data
		ensure
			Is_Assign: attached_data ~ a_data
		end

	has_error: BOOLEAN
			-- An error occurred during the last featured call.

feature {B2D_WORLD}

	fixture_with_pointer(a_item:POINTER):detachable B2D_FIXTURE
			-- Retreive the {B2D_FIXTURE} of `fixtures' with
			-- it's `item' that is `a_item'
		local
			l_cursor:INDEXABLE_ITERATION_CURSOR[B2D_FIXTURE]
		do
			from
				l_cursor := fixtures.new_cursor
			until
				l_cursor.after or attached Result
			loop
				if l_cursor.item.item ~ a_item then
					Result := l_cursor.item
				end
				l_cursor.forth
			end
		end

feature {NONE} -- Implementation

	internal_fixtures: LIST[B2D_FIXTURE]
			-- Internal value of `fixtures'.

	create_fixture (a_fixture_item: POINTER)
			-- Create and add a {B2D_FIXTURE} to `Current'.
			--
			-- `a_fixture_item`: C pointer of a newly created b2Fixture.
		require
			Exists: exists
		local
			l_fixture: B2D_FIXTURE
			l_protected_fixture:POINTER
		do
			create l_fixture.make (Current, a_fixture_item)
			if l_fixture.exists then
				last_fixture := l_fixture
				internal_fixtures.extend (l_fixture)
				l_protected_fixture := {B2D_EXTERNAL}.adopt_eiffel_object(l_fixture)
				{B2D_EXTERNAL}.b2Fixture_SetUserData(l_fixture.item, l_protected_fixture)
				world.add_to_wean_chain(l_protected_fixture)
			else
				has_error := True
			end
		ensure
			Last_Fixture_Has_Changed: not has_error implies last_fixture /= old last_fixture
			Fixtures_Increased: not has_error implies fixtures.count = old fixtures.count + 1
		end

	delete
			-- <Precursor>
		do
			check Should_Not_Be_Deleted: False end
		end

invariant
	Bullet_Valid: exists and is_bullet implies is_dynamic

end
