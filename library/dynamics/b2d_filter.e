note
	description: "Used in {B2D_FIXTURE*} to create collision filter."
	author: "Louis Marchand"
	date: "Tue, 09 Jun 2020 13:27:08 +0000"
	revision: "0.1"

class
	B2D_FILTER

inherit
	B2D_MEMORY_OBJECT
		redefine
			default_create, is_equal
		end

create
	make,
	default_create

create {B2D_ANY}
	shared_from_item,
	own_from_item

feature {NONE} -- Initialisation

	default_create
			-- Initialisation of `Current'
		do
			make
		end

	make
			-- Initialisation of `Current'
		do
			own_from_item ({B2D_EXTERNAL}.b2Filter_new)
		end

feature -- Access

	category_bits:NATURAL_16 assign set_category_bits
			-- The collision category bits. Normally you would just set one bit.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2Filter_categoryBits_get(item)
		end

	set_category_bits(a_category_bits:NATURAL_16)
			-- Assign `category_bits' with the value of `a_category_bits'
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2Filter_categoryBits_set(item, a_category_bits)
		ensure
			Is_Assign: category_bits ~ a_category_bits
		end

	mask_bits:NATURAL_16 assign set_mask_bits
			-- The collision mask bits. This states the categories
			-- that this shape would accept for collision.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2Filter_maskBits_get(item)
		end

	set_mask_bits(a_mask_bits:NATURAL_16)
			-- Assign `mask_bits' with the value of `a_mask_bits'
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2Filter_maskBits_set(item, a_mask_bits)
		ensure
			Is_Assign: mask_bits ~ a_mask_bits
		end

	group_index:INTEGER_16 assign set_group_index
			-- Collision groups allow a certain group of objects to never
			-- collide (negative) or always collide (positive). Zero
			-- means no collision group. Non-zero group filtering always
			-- wins against the mask bits.
		require
			Exists: exists
		do
			Result := {B2D_EXTERNAL}.b2Filter_groupIndex_get(item)
		end

	set_group_index(a_group_index:INTEGER_16)
			-- Assign `group_index' with the value of `a_group_index'
		require
			Exists: exists
		do
			{B2D_EXTERNAL}.b2Filter_groupIndex_set(item, a_group_index)
		ensure
			Is_Assign: group_index ~ a_group_index
		end

	is_equal(a_other:like Current):BOOLEAN
			-- Is `a_other` attached to an object considered
			-- equal to current object?
		do
			if exists and a_other.exists then
				Result := category_bits ~ a_other.category_bits and
							mask_bits ~ a_other.mask_bits and
							group_index ~ a_other.group_index
			else
				Result := exists ~ a_other.exists
			end
		end


feature {NONE} -- Implementation

	delete
			-- <Precursor>
		do
			{B2D_EXTERNAL}.b2Filter_delete(item)
		end

end
